(*pp camlp4r *)
(* vim:ts=4:syntax=ocaml
*)
(*===========================================================================*)
(*                           "domains_config.ml":                            *)
(*                                                                           *)
(*     Types for Global and Per-Domain-Group Configuration of "Gatling*"     *)
(*                                                                           *)
(*               (C) Explar Technologies Ltd, 2003--2006                     *)
(*===========================================================================*)
(*=========================*)
(*  "CONFIG_INTEXT_MODT":  *)
(*=========================*)
module type CONFIG_INTEXT_MODT =
sig
    (* Types for the External Config: *)
    type  group_parm' = 'a;
    type  global_parm'= 'a;

    (* "domain_group_conf'" and "external_conf'" are abstract types here: *)
    type  domain_group_conf' = 'a;
    value get_group_name'           : domain_group_conf'  -> string;
    value get_group_parms'          : domain_group_conf'  -> list group_parm';

    type  external_conf'     = 'a;
    value get_global_parms'         : external_conf' -> list global_parm';
    value get_default_group_parms'  : external_conf' -> list group_parm';
    value get_domain_groups'        : external_conf' -> list domain_group_conf';

    (* Mutable Internal Config, and how to fill it it: *)
    type internal_conf' = 'a;

    value empty_internal_conf'  : unit           -> internal_conf';
    value set_group_name'       : internal_conf' -> string      -> unit;
    value fill_from_global_parm': internal_conf' -> global_parm' -> unit;
    value fill_from_group_parm' : internal_conf' -> group_parm'  -> unit;

    (* Exception which can be raised by the above functions: *)
    exception Config_Error' of string;
end;

(*========================*)
(*  "Mk_Config" Functor:  *)
(*========================*)
(*  Provides a function for bulting internal configs from an external one: *)

module Mk_Config = functor (M: CONFIG_INTEXT_MODT) ->
struct
    (*-------------------------*)
    (*  "mk_internal_confs":   *)
    (*-------------------------*)
    (*  Creates an ARRAY of "internal_conf"s from a given "external_conf":
        The 0th element of the array is the default conf to be applied to
        automatically-generated Singleton Groups, and other elements  are
        confs for explicitly configured Groups:
    *)
    value mk_internal_confs: M.external_conf' -> array M.internal_conf' =
    fun ec ->
        (* Thread Global and Group Parms through Internal Config u/constr: *)
        let fill_from_global_parms:
            M.internal_conf' -> list M.global_parm' -> unit =
        fun ic gps ->
            List.iter (fun gp -> M.fill_from_global_parm' ic gp) gps
        in
        let fill_from_group_parms :
            M.internal_conf' -> list M.group_parm' -> unit =
        fun ic gps ->
            List.iter (fun gp -> M.fill_from_group_parm'  ic gp) gps
        in

        (* Then: *)
        let default_group_name = "DEFAULT" in

        let all_groups_parms  =
            [(M.get_default_group_parms' ec, default_group_name) ::
             (List.map
             (fun gr-> (M.get_group_parms' gr, M.get_group_name' gr))
                  (M.get_domain_groups' ec))
            ]
        in
        let all_confs_list    =
            List.map
            (fun (parms, gr_name) ->
                (* Be careful not to over-write the same "ic" copy here!
                   Create a fresh instance -- we don't have a general "copy"
                   of non-objects in OCaml:
                *)
                let ic = M.empty_internal_conf' ()     in

                (* Memoise the Group Name: *)
                let () = M.set_group_name' ic gr_name  in

                (* Fill "ic" from the Global Parms first: *)
                let () =
                    try  fill_from_global_parms ic (M.get_global_parms' ec)
                    with [M.Config_Error' msg ->
                          Misc_utils.fatal_error ("In Global Parms: "^msg)]
                in
                (* Fill "ic" from the Default Per_Group Parms: *)
                let () =
                    try  fill_from_group_parms ic
                         (M.get_default_group_parms' ec)
                    with [M.Config_Error' msg ->
                          Misc_utils.fatal_error
                         ("In Default Group Parms: "^msg)]
                in
                (* Fill "ic" from the current "parms", overriding the defaults,
                   -- unless it's a "DEFAULT" Group already:
                *)
                let () =
                    if  gr_name <> default_group_name
                    then
                        try fill_from_group_parms   ic parms
                        with [M.Config_Error' msg ->
                              Misc_utils.fatal_error
                              ("In Group Parms for "^gr_name^": "^msg)]
                    else ()
                in
                (* The result: has been filled in: *)
                ic
            )
            all_groups_parms
        in
        (* Convert the result into an array: *)
        Array.of_list all_confs_list;
end;

(*------------------------------*)
(* "compile_domain_selectors":  *)
(*------------------------------*)
(*  Takes a list of GLOB-type regular exprs which act as selectors of domains
    in a group,  and combines them into one PCRE-type regexp  recognising the
    same group of domains.
    Returns the resulting single PCRE regexp as string, and in compiled form:
*)
value compile_domain_selectors: list string -> (string * Pcre.regexp) =
fun glob_res ->
    let str_re =
        String.concat "|"
        (List.map
            (fun gre -> "("^(Misc_utils.pcre_of_glob gre)^")")
            glob_res
        )
    in
    (str_re, Pcre.regexp ~study:True str_re);

(*=========*)
(*  Misc:  *)
(*=========*)
(*  The following return code is used by Monitored sending processes to indi-
    cate a severe error, so they will NOT be re-started by the Monitor:
*)
value no_restart_rc = 255;

