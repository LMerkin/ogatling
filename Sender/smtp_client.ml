(*pp camlp4r *)
(* vim:ts=4:syntax=ocaml
*)
(*===========================================================================*)
(*                              "smtp_client.ml":                            *)
(*      Implementation of the Client (Sending) Side of the SMTP Protocol     *)
(*                  (C) Explar Technologies Ltd, 2003--2006                  *)
(*===========================================================================*)
(*=================*)
(*   Exceptions:   *)
(*=================*)
exception SMTP_LowLevel_Error  of string;
exception SMTP_Transient_Error of string;
exception SMTP_Permanent_Error of string;
exception SMTP_Error_Body_Sent of string;


(*=================*)
(*  SMTP  Agents:  *)
(*=================*)
(*-----------------*)
(*  Configuration: *)
(*-----------------*)
(* The following types are specified in the interface : *)

type sched_call_backs =
{
    reserve_ip:    string -> Unix.inet_addr -> bool;
    release_ip:    string -> Unix.inet_addr -> unit
};

type ip_resolv_mode =
[ IP_Resolv_Lazy   of Dns_client.dns_agent_config
| IP_Resolv_Strict of Dns_client.dns_agent_config
| IP_PreCached
| IP_Scheduled     of sched_call_backs
];

type smtp_agent_config =
{
    conf_ip_resolv_mode     : mutable ip_resolv_mode;
    conf_attempt_timeout_sec: mutable float;
    conf_n_retries          : mutable int;
    conf_server_port        : mutable int;
    conf_sock_buff_size     : mutable int;
    conf_client_ips         : mutable array Unix.inet_addr;
    conf_client_domains     : mutable array string;
    conf_dry_run            : mutable option float;
    conf_trace_file         : mutable option Unix.file_descr;
    conf_use_persistence    : mutable bool;
    conf_use_pipelining     : mutable bool;
    conf_strict_5xx_mode    : mutable bool
};

(* Default config -- every time a fresh instance, as it's mutable, so it's
   a function rather than a constant.
   We always need a valid client domain for HELO/EHLO,  and will therefore
   need to bind to the corresponding local IP, otherwise many servers  may
   refuse to talk to us. If we are unable to set these values properly, we
   don't complain at the moment, as the default settings can be overridden
   at the time of SMTP Agent creation -- but at that point, rigorous check
   will be made.
   XXX: we use Unix.gethostbyname () rather  than our own DNS interface at
   this point -- this function is typically not performance-critical:
*)
value default_local_domain : string = Unix.gethostname ();

value default_local_ips    : array Unix.inet_addr   =
    try let  h = Unix.gethostbyname (default_local_domain) in
        h.Unix.h_addr_list
        (* NB: this array may still be empty! *)
    with
    [ _ ->
        (* "local_domain" lookup error -- must be overridden later: *)
        [| |]
    ];

value default_smtp_agent_config: unit -> smtp_agent_config =
fun sock_buff_size ->
    let dns_conf = Dns_client.default_dns_agent_config ()
    in
    {
        conf_ip_resolv_mode     = IP_Resolv_Strict dns_conf;
        conf_attempt_timeout_sec= 12.0;     (* Inherited from old good time *)
        conf_n_retries          = 1;
        conf_server_port        = 25;
        conf_sock_buff_size     = dns_conf.Dns_client.conf_sock_buff_size;
        conf_client_ips         = default_local_ips;
        conf_client_domains     = [| default_local_domain |];
        conf_dry_run            = None;
        conf_trace_file         = None;
        conf_use_persistence    = True;
        conf_use_pipelining     = False;
        conf_strict_5xx_mode    = False
    };

(*--------------------------------------------*)
(*  Resolver construction and config reading: *)
(*--------------------------------------------*)
(* The last ctor of "Resolver_Scheduled": config, reserved_IP: *)
type sched_interface =
{
    call_backs: sched_call_backs;
    use_ips   : mutable list Unix.inet_addr;
    resv_ip   : mutable Unix.inet_addr
};

type ip_resolver =
[ Resolver_Lazy      of Dns_client.dns_agent
| Resolver_Strict    of Dns_client.dns_agent
| Resolver_PreCached of (ref (list Unix.inet_addr))
| Resolver_Scheduled of sched_interface
];

value mk_resolver: ip_resolv_mode -> ip_resolver =
fun
[ IP_Resolv_Lazy   dns_conf     ->
    Resolver_Lazy   (Dns_client.mk_dns_agent dns_conf)

| IP_Resolv_Strict dns_conf     ->
    Resolver_Strict (Dns_client.mk_dns_agent dns_conf)

| IP_PreCached                  ->
    Resolver_PreCached (ref [])

| IP_Scheduled     call_backs   ->
    Resolver_Scheduled
    {
        call_backs = call_backs;
        use_ips    = [];
        resv_ip    = Unix.inet_addr_any
    }
];

value config_of_resolver: ip_resolver -> ip_resolv_mode =
fun
[
  Resolver_Lazy    dns_agent    ->
    IP_Resolv_Lazy   (Dns_client.config_of_dns_agent dns_agent)

| Resolver_Strict  dns_agent    ->
    IP_Resolv_Strict (Dns_client.config_of_dns_agent dns_agent)

| Resolver_PreCached _          ->
    IP_PreCached

| Resolver_Scheduled sched_inter->
    IP_Scheduled   sched_inter.call_backs
];

(*----------------------*)
(* "smtp_agent" itself: *)
(*----------------------*)
type smtp_agent =
{
    (* The following fields are similar to their counter-parts in the "smtp_
       agent_config". They are all mutable to allow for re-configuration:
    *)
    resolver            : mutable ip_resolver;
    attempt_timeout_sec : mutable float;
    n_retries           : mutable int;
    server_port         : mutable int;
    use_persistence     : mutable bool;
    use_pipelining      : mutable bool;
    strict_5xx_mode     : mutable bool;
    dry_run             : mutable option float;
    trace_file          : mutable option Unix.file_descr;
    client_domains      : mutable array string;
    client_ips          : mutable array Unix.inet_addr;

    (* The user-level receiving buffer --  NB: this is an object type expr: *)
    reader              : Misc_utils.line_reader Tnio.tsock;

    (* The current SMTP connection: Socket, Domain and PipeLining Mode, and
       the currently reserved server IP (if any):
    *)
    sock                : mutable  Tnio.tsock;
    sock_buff_size      : mutable  int;
    ipdi                : mutable  int;    (* Client IP/Domain index to use *)
    conn_domain         : mutable  string;
    in_pipeline         : mutable  bool;

    (* Deadline for the on-going dialogue with the current IP:   *)
    deadline            : mutable  float
};

(*--------------------*)
(*  "mk_smtp_agent":  *)
(*--------------------*)
(*  Creates an SMTP Agent from given SMTP and DNS Agent Configs: *)

value mk_smtp_agent:   smtp_agent_config -> smtp_agent =
fun   smtp_c  ->
    (* Verify the config parms: *)
    if  smtp_c.conf_attempt_timeout_sec <= 0.0 ||
        smtp_c.conf_n_retries           <  0   ||
        smtp_c.conf_server_port         <= 0
    then
        invalid_arg "Smtp_client.mk_smtp_agent"
    else
    {
        (* Agent parms, coming from the SMTP Config. NB: the correspondence
           between the client IPs and Domains is not verified automatically,
           as these settings may be overridden later:
        *)
        resolver            = mk_resolver smtp_c.conf_ip_resolv_mode;
        attempt_timeout_sec = smtp_c.conf_attempt_timeout_sec;
        n_retries           = smtp_c.conf_n_retries;
        server_port         = smtp_c.conf_server_port;
        client_domains      = Array.copy smtp_c.conf_client_domains;
        client_ips          = Array.copy smtp_c.conf_client_ips;
        dry_run             = smtp_c.conf_dry_run;
        trace_file          = smtp_c.conf_trace_file;

        (* Create the SMTP socket: *)
        ipdi                = -1;            (* as yet *)
        sock                = Tnio.invalid_tsock;
        sock_buff_size      = smtp_c.conf_sock_buff_size;
        use_persistence     = smtp_c.conf_use_persistence;
        use_pipelining      = smtp_c.conf_use_pipelining;
        strict_5xx_mode     = smtp_c.conf_strict_5xx_mode;
        conn_domain         = "";            (* Not yet defined    *)
        deadline            = 0.0;           (* Will be set later  *)
        in_pipeline         = False;         (* Not yet there!     *)

        (* Size of the KERNEL sending buffer: *)

        (* The user-level receiving buffer -- is 4K enough? *)
        reader              = new Misc_utils.line_reader Tnio.recv 4096
    };

(*========================================*)
(*  Interaction with the Send Scheduler:  *)
(*========================================*)
(*-----------------*)
(*  "reserve_ip":  *)
(*-----------------*)
value reserve_ip: smtp_agent -> string -> Unix.inet_addr ->
                  ~pre_reserved:bool   -> unit =

fun agent dom ip  ~pre_reserved        ->
    (* Need to reserve this IP. This can only be done if the Agent
       is operating with the Scheduled resolver:
       NB: in "reserve_ip", use "dom" as given  explicitly by the
       arg, not "agent.conn_domain", as the latter may not be set
       until a successful connection:
    *)
    match agent.resolver with
    [ Resolver_Scheduled sched_inter ->
      do {
        if  not pre_reserved
        then
            if  sched_inter.call_backs.reserve_ip dom ip
            then
                () (* OK, reservation confirmed: *)
            else
                raise (SMTP_Transient_Error "IP reservation not confirmed")
        else
            (); (* Don't call reservation again  *)

        (* Memoise the reserved IP: *)
        sched_inter.resv_ip := ip
      }

    | _ ->
        ()  (* Ignore the request XXX or better signal an error? *)
    ];

(*-----------------*)
(*  "release_ip":  *)
(*-----------------*)
(* Like for "reserve_ip", "dom" is passed explicitly as it may not be set
   in the "agent" when this function is invoked:
*)
value release_ip: smtp_agent -> string -> unit =
fun agent dom ->
    match agent.resolver with
    [ Resolver_Scheduled sched_inter ->
        if  sched_inter.resv_ip <> Unix.inet_addr_any
        then
        do {
            sched_inter.call_backs.release_ip dom sched_inter.resv_ip;
            sched_inter.resv_ip := Unix.inet_addr_any
        }
        else
            ()
    | _ ->
        ()  (* Ignore the request XXX or better signal an error? *)
    ];

(*----------------------*)
(*  "get_resv_ip":      *)
(*  "get_conn_domain":  *)
(*----------------------*)
value get_resv_ip    : smtp_agent -> Unix.inet_addr =
fun agent ->
    match agent.resolver with
    [ Resolver_Scheduled sched_inter ->
        sched_inter.resv_ip
    | _ ->
        Unix.inet_addr_any (* XXX: or better signal an error? *)
    ];

value get_conn_domain: smtp_agent -> string =
fun agent ->
    agent.conn_domain;


(*------------------------*)
(*  "set_scheduled_ips":  *)
(*------------------------*)
value set_scheduled_ips: smtp_agent  -> list Unix.inet_addr -> unit =
fun agent use_ips ->
    match agent.resolver with
    [ Resolver_Scheduled sched_inter ->
        sched_inter.use_ips:= use_ips

    | Resolver_PreCached its_ips     ->
        its_ips.val        := use_ips

    | _ -> ()
    ];


(*======================*)
(*  Socket Operations:  *)
(*======================*)
(*------------------*)
(*  "*local_port":  *)
(*------------------*)
value local_port:  smtp_agent -> int =
fun agent ->
    match Tnio.getsockname agent.sock with
    [   Unix.ADDR_INET _ port -> port
      | _                     -> assert False
    ];

value string_of_local_port: smtp_agent -> string =
fun agent ->
    string_of_int (local_port agent);


(*------------*)
(*  "trace":  *)
(*------------*)
value trace: smtp_agent -> string -> unit =
fun   agent  str ->
    match agent.trace_file with
    [   None    -> ()
      | Some fd ->
            try  Misc_utils.write_fstr fd str
            with [_ -> ()]
    ];


(*------------*)
(*  "send*":  *)
(*------------*)
value send: smtp_agent -> string -> unit =
fun   agent str ->
try
    if  str = ""
    then
        () (* Nothing to send *)
    else
    let ()       = trace agent   str in
    let to_send  = String.length str in
    let sent_len =
        (* Try to send all the bytes in the "str": *)
        Misc_utils.strict_io
        Tnio.send agent.sock str 0 to_send agent.deadline
    in
    if  sent_len = to_send
    then
        () (* All done: *)
    else
    do {
        (* This is an error, as we used "strict_io": *)
        assert (sent_len < to_send);
        raise  (SMTP_LowLevel_Error
               (Printf.sprintf "Smtp_client.send: Short Send: %d < %d"
                sent_len to_send))
    }
with
[   Unix.Unix_error _ _ _ as err ->
    raise (SMTP_LowLevel_Error (Misc_utils.print_exn err))
];

(*------------------*)
(*  "compare_rcs":  *)
(*------------------*)
(*  An exception is raised if comparison fails: *)

value compare_rcs: smtp_agent -> string -> int -> int -> unit =
fun   agent  line  rc  exp_rc ->
    if  rc = exp_rc
    then
        (* OK, this is the expected Return Code *)
        ()
    else
        if  (rc     >= 200) &&     (rc <= 299) &&
            (exp_rc >= 200) && (exp_rc <= 299)
        then
            (* Both the real and expected RCs are in the 2XX range --
               consider them to be compatible:  *)
            ()
        else
        (* "5XX" errors are normally permanent, but 503 (and sometimes other
           codes, but that's completely non-standard)  may actually indicate
           an unsupported sequence of commands, so can be retried in another
           mode. However, in the Strict_500 Mode, all 5XX errors are conside-
           red to be permanent:
        *)
        if  (rc >= 500) && (rc <> 503 || agent.strict_5xx_mode)
        then
            (* These errors are considered to be permanent: *)
            raise (SMTP_Permanent_Error line)
        else
            (* All other errors are considered to be transient, e.g.:
               4XX, 503, and inappropriate 1XX, 2XX and 3XX responses
               -- they probably indicate a command sequence error:
            *)
            raise (SMTP_Transient_Error line);


(*----------------------*)
(*  "check_smtp_resp":  *)
(*----------------------*)
(*  Read the server response and match it against the expected one(s): multiple
    responses need to be checked in the pipelining mode. Exception is raised if
    any of the responses is improper. If the "look_pipe" flag is set, this fun-
    ction will also look for the "PIPELINING" capability in the response lines,
    and set it in the "agent" if found:
*)
value rec   check_smtp_resp: smtp_agent -> list int -> bool -> unit =

fun   agent exp_rcs look_pipe ->
match exp_rcs with
[ [] ->
    (* No expected responses, so nothing to do: *)
    ()
| [hrc :: trcs] ->
    (* The generic case. Read the next line in, with a deadline: *)

    let (line, mb_err) =
        try  (agent.reader#recv_line agent.sock agent.deadline, None)
        with [ hmm -> ("", Some hmm) ]
    in
    do {
        (* Trace it if necessary: *)
        trace agent (line^"\r\n");

        (* Any  reading  errors ? *)
        match mb_err with
        [   Some err -> raise err
          | None     -> ()
        ];

        (* The line may only have ' ' or '-' at offset 3: *)
        if  String.length line < 4
        then
            (* Response too short: *)
            raise (SMTP_Transient_Error ("Response Too Short: \""^line^"\""))
        else
        let c3 = line.[3] in
        if  c3 = '-'
        then
        do {(* This is not the final line of this response; we don't need to
               analyse it, EXCEPT when we are looking for the PIPELINING ca-
               pability after EHLO:
            *)
            if  look_pipe && agent.use_pipelining &&
                (ExtString.String.exists (String.uppercase line) "PIPELINING")
            then
            do {
                agent.in_pipeline:= True;
                trace agent "[ENTERED PIPELINE]\r\n"
            }
            else ();

            (* Need more input -- need to get the final line! *)
            check_smtp_resp agent exp_rcs look_pipe
        }
        else
        if  c3 = ' '
        then
        do {(* This is a final line of this response; compare its code with
               the head of expected response codes:
            *)
            let rc =
            try int_of_string (String.sub line 0 3)
            with
                [_ -> raise (SMTP_Transient_Error
                            ("No Response Code in \""^line^"\""))]
            in
            compare_rcs agent line rc hrc;

            (* If there are more expected codes, read more input: *)
            if  trcs <> []
            then
                check_smtp_resp agent trcs look_pipe
            else ();
        }
        else
            (* Invalid char at offset 3: *)
            raise (SMTP_Transient_Error
                  ("Wrong Response Format in \""^line^"\""))
    }
];

(*---------------------*)
(*  "wrap_dns_error":  *)
(*---------------------*)
value wrap_dns_error: exn -> exn =
fun
[   Dns_client.Negative_Result dom ->
        (* Certainly cannot send mail to there! *)
            SMTP_Permanent_Error ("Invalid dest domain: "^dom)

  | other ->
    (* Other DNS error: probably transient: *)
            SMTP_LowLevel_Error (Misc_utils.print_exn  other)
];


(*-----------------------*)
(*  "fold_smtp_errors":  *)
(*-----------------------*)
(*  Folds a list of exceptions (got while sending a msg to different IPs of
    the same dest addr) into a single one:
*)
value fold_smtp_errors: smtp_agent -> list exn -> exn =
fun agent errs ->
    let msg = Misc_utils.joint_err_msg errs in

    (* If a msg body was sent to at least one IP, then it was really sent.
       But we will NOT get such Body_Sent errors at all in the Strict_5XX
       mode:
    *)
    if  List.exists  (fun [SMTP_Error_Body_Sent _ -> True | _ -> False]) errs
    then
        SMTP_Error_Body_Sent msg
    else
    (* If we got a Permanent Error  (in particular, a 5XX response):   in the
       Strict_5XX mode, it will be a Permanent Error;  otherwise, if at least
       one error was Transient, we will produce a Transient one:
    *)
    let
        comb = if  agent.strict_5xx_mode then List.exists else List.for_all
    in
    if  comb (fun [SMTP_Permanent_Error _ -> True | _ -> False]) errs
    then
        SMTP_Permanent_Error msg
    else
        SMTP_Transient_Error msg;

(*=====================*)
(*   SMTP Statistics:  *)
(*=====================*)
(*  "smpt_agent_stats": externally-visible record: *)
type smtp_agent_stats =
{
    total_transacts  : int;
    ok_transacts     : int;
    succ_rate        : float;
    reused_conns     : int;
    reuse_rate       : float;
    av_dns_time_sec  : float;
    av_trans_time_sec: float
};

value empty_smtp_agent_stats: unit -> smtp_agent_stats =
fun () ->
{
    total_transacts  = 0;
    ok_transacts     = 0;
    succ_rate        = 0.0;
    reused_conns     = 0;
    reuse_rate       = 0.0;
    av_dns_time_sec  = 0.0;
    av_trans_time_sec= 0.0
};

(*  "smtp_agent_stats_accum": internal type for accumulating sending
    statistics for a single Agent. All times are in sec:
*)
type smtp_agent_stats_accum =
{
    (* All times are in seconds: *)
    total_transacts' : mutable  int;
    ok_transacts'    : mutable  int;
    reused_conns'    : mutable  int;
    av_dns_time      : mutable  float;
    total_trans_time : mutable  float
};

value empty_smtp_agent_stats_accum: unit -> smtp_agent_stats_accum =
fun () ->
{
    total_transacts' = 0;
    ok_transacts'    = 0;
    reused_conns'    = 0;
    av_dns_time      = 0.0;
    total_trans_time = 0.0
};

(* Stats extraction functions: *)

value update_av_dns_time: smtp_agent -> smtp_agent_stats_accum -> unit =
fun   agent accum    ->
    let mb_dns_agent =
        match agent.resolver with
        [   Resolver_Lazy   agent -> Some agent
        |   Resolver_Strict agent -> Some agent
        |   Resolver_PreCached  _ -> None
        |   Resolver_Scheduled  _ -> None
        ]
    in
    let av_dns_time =
        match mb_dns_agent with
        [   Some agent ->
                let stats = Dns_client.get_dns_stats agent in
                stats.Dns_client.av_resp_time_sec
        |   None ->
                0.0
        ]
    in
    accum.av_dns_time := av_dns_time;

value get_smtp_agent_stats :
      smtp_agent_stats_accum -> smtp_agent_stats =

fun   accum ->
    if  accum.total_transacts' = 0
    then
        empty_smtp_agent_stats ()
    else
    let nf = float_of_int  accum.total_transacts' in
    {
        total_transacts  = accum.total_transacts';
        ok_transacts     = accum.ok_transacts';
        succ_rate        = (float_of_int accum.ok_transacts') /. nf;
        reused_conns     = accum.reused_conns';
        reuse_rate       = (float_of_int accum.reused_conns') /. nf;
        av_dns_time_sec  = accum.av_dns_time;
        av_trans_time_sec= accum.total_trans_time /. nf
    };

(*====================================*)
(*  Low-Level Sending SMTP Dialogue:  *)
(*====================================*)
(*-----------------------------*)
(*  "smtp_dialogue_headless":  *)
(*-----------------------------*)
(*  If pipelining is used, we wait for several responses together at the
    "DATA" synchronisation point.    The "reused" flag indicates for how
    many (it depends on whether the current connection was re-used).
*)
value rec smtp_dialogue_headless:
    smtp_agent -> Mime_xml_types.smtp_msg_final -> bool -> unit =

fun agent fmsg reused->
do {
    (* "MAIL From:" *)
    send agent ("MAIL From:<"^ fmsg.Mime_xml_types.envel_from'' ^">\r\n");

    if   agent.in_pipeline
    then ()
    else
    try
        check_smtp_resp agent [250] False
    with
    [   SMTP_Permanent_Error hmm when (reused && not agent.strict_5xx_mode) ->
            (* This may simply mean that a persistent connection is not
               supported, so that error is likely to be  actually tran-
               sient. Any other errors are propagated as they are. How-
               ever, in the Strict_5XX mode, we propagate all Permanent
               Errors:
            *)
            raise (SMTP_Transient_Error hmm)
    ];

    (* "RCPT To:"   *)
    send agent  ("RCPT To:<" ^ fmsg.Mime_xml_types.to_addr''    ^">\r\n");

    if   agent.in_pipeline
    then ()
    else check_smtp_resp agent [250] False;

    (* "DATA" (unless we are in the "dry-run" mode).  After this point (if
       "DATA" command was accepted), any exceptions are converted into the
       "SMTP_Error_Body_Sent", to prevent data re-sending which could lead
       to multiple delivery of messages. "DATA" is a synchronisation point
       in the pipe-lined mode:
    *)
    match agent.dry_run with
    [ None ->
      do {
        send agent "DATA\r\n";

        (* Get the response(s): *)
        let exp_rcs =
            if   agent.in_pipeline
            then
                if  reused
                then
                    (* Connection was re-used, with pipelining: wait for
                       the responses  to the previous  "." at the end of
                       data, plus "MAIL", plus "RCPT",  plus this "DATA"
                       itself:
                    *)
                    [250; 250; 250; 354]
                else
                    (* Came through  the dialogue head  --  wait for the
                       results as above, but without ".":
                    *)
                    [250; 250; 354]
            else
                (* No pipelining -- wait for the result of "DATA" only *)
                [354]
        in
        check_smtp_resp agent exp_rcs False;

        (* If we got here, send the data in the re-sending-protected mode.
           BE CAREFUL: don't allow "." on individual line, it will cause a
           protocol error!
        *)
        try
        do {
            List.iter
            (fun line ->
                 let cl = if line = ".\r\n" then ". \r\n" else line
                 in send agent cl
            )
            fmsg.Mime_xml_types.all_data'';

            (* Data are done: *)
            send agent ".\r\n";
            if   agent.in_pipeline
            then ()
            else check_smtp_resp agent [250] False;
        }
        with
        (* Any error here will be converted into  "Body_Sent", so that no re-
           tries will be made (even if it's a "554 Transaction failed" error).
           However, in the Strict_5XX mode,   we propagate 5XX errors as they
           appear (there will be no body re-sending on them anyway, as in this
           mode, they are propagated all the way up),  while still converting
           other errors into "Body_Sent":
        *)
        [ (SMTP_Permanent_Error _ as p) when agent.strict_5xx_mode ->
            raise p
        |  hmm ->
            raise (SMTP_Error_Body_Sent (Misc_utils.print_exn hmm))
        ]
      }

    | Some delay ->
      do {
        (* "Dry-run" mode: only need to send "RSET". However, a problem
           occurs if Dry-Run + Persistent Connections + Pipe-Lining are
           used together: we will never get to a synchronisation point,
           as "Dry-Run" eliminates "DATA" and persistent connection eli-
           minates "EHLO"! So we DO need a synchronous response here in
           both modes; it's a good idea, in Dry-Run,  to send "NOOP" as
           well.
           A delay is applied before RSET, to emulate temporal behaviour
           of real sends:
        *)
        if   delay > 0.0
        then Tnio.thread_sleep delay
        else ();

        send agent "RSET\r\n";

        if   agent.in_pipeline
        then
        do {
            send agent "NOOP\r\n";
            (* MUST read the response here, to achieve synchronisation! *)
            check_smtp_resp agent [250] False
        }
        else ();

        check_smtp_resp agent [250] False
      }
    ]
};

(*-------------------------*)
(*  "smtp_dialogue_full":  *)
(*-------------------------*)
value smtp_dialogue_full:  smtp_agent -> Mime_xml_types.smtp_msg_final ->
                           unit =
fun agent fmsg ->
let client_domain = agent.client_domains.(agent.ipdi) in
do {
    (* When we start a new full dialogue, it is always initially assumed that
       there is NO pipelining yet (and we are NOT in a persistent connection,
       just re-assert that:
    *)
    agent.in_pipeline     := False;

    (* So get the prompt synchronously: *)
    check_smtp_resp agent [220] False;

    (* Now try "EHLO" (if pipelining is desired) or "HELO": *)
    if  agent.use_pipelining
    then
        try
        do {
            (* Here we check for the "PIPELINING" capability: *)
            send agent ("EHLO "^client_domain^"\r\n");
            check_smtp_resp agent [250] True
        }
        with
        [   _ ->
            do {
                (* "EHLO" not accepted -- in particular, no pipelining. Do
                   "HELO" then (it may also be connection failure, then we
                   will get another exception:
                *)
                send agent ("HELO "^client_domain ^"\r\n");
                check_smtp_resp agent [250] False
            }
        ]
    else
    do {
        (*  Normal  "HELO" -- pipelining is not requested: *)
        send agent ("HELO "^client_domain ^"\r\n");
        check_smtp_resp agent [250] False
    };

    (* The rest of the dialogue,  with or without pipelining.
       The "reuse" flag is "False", as we entered through the
       dialogue head, after a new connection:
    *)
    smtp_dialogue_headless agent fmsg False
};


(*---------------------*)
(*  "close_smtp_conn": *)
(*---------------------*)
value close_smtp_conn: smtp_agent -> ~polite_quit:bool -> unit =
fun   agent ~polite_quit ->
do {
    if  agent.sock <> Tnio.invalid_tsock
    then
    do {
        if  polite_quit && (agent.conn_domain <> "")
        then
            (* If we are trying to be polite, send "QUIT" to our peer (which is
               non-"", although it may have already disconnected by itself):
            *)
            try do{
                send  agent "QUIT\r\n";
                let exp_rcs =
                    if  agent.in_pipeline
                    then [250; 221]     (* For "." and "QUIT" *)
                    else [221]          (* For "QUIT" only    *)
                in
                check_smtp_resp agent exp_rcs False
            }
            with [_ -> ()]
        else
            ();

        (* Now close the socket. IMPORTANT: use "close", not "shutdown", as
           the latter does not break existing connections!
        *)
        Tnio.close agent.sock;
        agent.sock := Tnio.invalid_tsock;
        agent.ipdi := -1;

        if  agent.conn_domain <> ""
        then trace agent ("[DISCONNECTED from "^agent.conn_domain^"]\r\n")
        else trace agent  "[CLOSED SMTP SOCKET]\r\n";
    }
    else
        ();

    (* If we use a Scheduled Resolver, report disconnection. Also, re-set
       the conn Domain;  the resv IP is re-set automatically if it is re-
       leased. To be on a safe side, we do it EVEN if there is no current
       socket!
    *)
    release_ip agent agent.conn_domain;
    agent.conn_domain := ""
};

(*-----------------------*)
(*  "close_smtp_agent":  *)
(*-----------------------*)
(*  Closes the current connection (as "close_smtp_conn") in the "polite" mode,
    and also closes the DNS Agent (if any) and re-sets the internal fields:
*)
value close_smtp_agent: smtp_agent -> unit =
fun agent ->
do {
    (* Close the last connection: *)
    close_smtp_conn agent ~polite_quit:True;

    (* Close the resolver: *)
    match agent.resolver with
    [
      Resolver_Lazy dns_agent   ->
        Dns_client.close_dns_agent dns_agent

    | Resolver_Strict dns_agent ->
        Dns_client.close_dns_agent dns_agent

    | _ -> ()
    ];
    (* Re-setthe fields: *)
    agent.conn_domain     := "";
    agent.in_pipeline     := False;
    agent.deadline        := 0.0
};

(*===================================*)
(*  Connect-and-Send SMTP Sessions:  *)
(*===================================*)
(*-----------------*)
(*  "send_to_ip":  *)
(*-----------------*)
(*  Connects to a single Mail Exchange IP and runs a full sending dialogue with
    it:
*)
value send_to_ip:
    smtp_agent -> (Unix.inet_addr * bool) -> Mime_xml_types.smtp_msg_final ->
    unit       =
fun agent (ip, need_res) fmsg ->
do {
    (* In case if the socket has not been properly closed before, do it now,
       to prevent leak of socket descriptors,   trying to be polite ("QUIT")
       if there exists a peer:
    *)
    close_smtp_conn agent ~polite_quit:(agent.conn_domain <> "");

    (* Reserve the "ip" in any case  --  what actually is done here, depends
       on the resolver mode and the ~pre_reserved flag:
    *)
    reserve_ip agent fmsg.Mime_xml_types.to_domain''  ip
                          ~pre_reserved: (not need_res);

    trace agent ("[CONNECTING to "^ fmsg.Mime_xml_types.to_domain'' ^
                 " ("^(Unix.string_of_inet_addr ip)^")...\r\n");

    (* (Re)-create and bind the client socket. The local IP to bind is randomly
       selected from the array of allowed IPs:
    *)
    agent.ipdi     := Random.int (Array.length agent.client_ips);

    agent.sock     := Tnio.socket
                            ~kern_buff_size:agent.sock_buff_size
                            Unix.PF_INET Unix.SOCK_STREAM;

    agent.deadline := Unix.gettimeofday() +. agent.attempt_timeout_sec;

    (* Bind the socket to the given IP and any local port: *)
    Tnio.bind  agent.sock (Unix.ADDR_INET agent.client_ips.(agent.ipdi) 0);

    (* Connect to the server: *)
    try Tnio.connect
            agent.sock (Unix.ADDR_INET ip agent.server_port) agent.deadline
    with
    [hmm ->
     do {
        (* If connection failed, release the curr IP immediately.
           Otherwise, it stays reserved until we disconnect:
        *)
        release_ip agent fmsg.Mime_xml_types.to_domain'';
        raise   (SMTP_LowLevel_Error ("Connection failed: "^
                (Misc_utils.print_exn hmm)))
    }];
    (* The domain we are connected to becomes the current one: *)
    agent.conn_domain := fmsg.Mime_xml_types.to_domain'';

    trace agent ("[CONNECTED (local port "^
                (string_of_local_port agent)^")]\r\n");

    (* Re-set the reader: *)
    agent.reader#reset ();

    (* Run the full SMTP dialogue: *)
    smtp_dialogue_full agent fmsg
};

(*-------------------*)
(*  "send_to_ips*":  *)
(*-------------------*)
(*  Tries several MX IPs until a successful transaction or a definite failure
    occurs.  The IPs are paired with flags which show whether they need to be
    reserved before use (if True), or can be used straight away:
*)
value rec send_to_ips':
    smtp_agent -> list (Unix.inet_addr * bool) ->
    Mime_xml_types.smtp_msg_final -> list exn  -> unit =

fun agent ips fmsg prev_errs ->
    match ips with
    [
        [] ->
            (* No more IPs available, so it's an error: *)
            raise (fold_smtp_errors agent prev_errs)

      | [hip ::tips] ->
        try
        do {
            (* Try the head IP ("hip"): *)
            send_to_ip agent hip fmsg

            (* If we got here,  send was successful *)
        }
        with
        [   error ->
            do {
                (* In any case, close the connection without "QUIT": *)
                close_smtp_conn agent ~polite_quit:False;

                (* Classify the error: *)
                match error with
                [
                    SMTP_Error_Body_Sent _ ->
                        (* An exception, but the body of the message has
                           already been sent -- no retry: *)
                        raise error

                  | SMTP_Permanent_Error _ ->
                        (* No point in re-trying: *)
                        raise error

                  | _ ->
                        (* Any other error (e.g. connect failure, time-out):
                           can try more IPs: *)
                        let exc_msg = Misc_utils.print_exn error in
                        do {
                            trace agent  ("[EXCEPTION: "^exc_msg^ "]\r\n");

                            send_to_ips' agent tips fmsg (prev_errs @ [error])
                        }
                ]
            }
        ]
    ];

(* The outer "send_to_ips" contains the retrying policy: *)

value send_to_ips:
    smtp_agent -> list Unix.inet_addr -> Mime_xml_types.smtp_msg_final -> unit =

fun agent ips fmsg ->
    if  ips <> []
    then
        let repl_ips   = Misc_utils.replicate_list ips (agent.n_retries+1) in
        let resv_pairs =
            match agent.resolver with

            [ Resolver_Scheduled _ ->
                (* Need  to reserve  the IPs,  except the head one,  which is
                   already reserved. The rest are not, even the head replicas,
                   as the latter will be released by the time  we come to use
                   them -- the prev IP is always released  before the new one
                   is applied (in "send_to_ip"):
                *)
                [(List.hd  repl_ips, False) ::
                 (List.map (fun ip->(ip, True)) (List.tl repl_ips))]

            | _ ->
                (* No need to reserve anything: *)
                List.map   (fun ip->(ip,False)) repl_ips
            ]
        in
        (* Now try them all: *)
        send_to_ips' agent resv_pairs fmsg []
    else
        (* This can onky happen in some limited circumstances, e.g. if using
           Resolver_PreCached or Resolver_Scheduled with empty IPs -- XXX...
        *)
        raise (SMTP_Transient_Error "No MX IPs");

(*-------------------------*)
(*  "smtp_transact_lazy":  *)
(*  "smtp_transact_cont":  *)
(*-------------------------*)
(* Get IPs for a given Mail Domain in fragmented blocks (possibly more than
   one), and connect to the first successful IP. There is no load balancing
   here, as the IPs will normally be returned in a pre-defined order:
*)
value rec smtp_transact_cont:
    smtp_agent -> Dns_client.dns_agent -> Dns_client.mx_ips_cont -> list exn ->
    Mime_xml_types.smtp_msg_final      -> unit     =

fun agent dns_agent cont_obj prev_errs fmsg ->
try
    (* Perform the DNS request: *)
    let more_ips = Dns_client.more_mx_ips dns_agent cont_obj in

    (* Process the block of IPs we got: *)
    match more_ips with
    [
        [] ->
            (* No more IPs -- but no DNS errors either. Send failed --
               how exactly, depends on the "prev_errs":
            *)
            raise (fold_smtp_errors agent prev_errs)
     | _  ->
            (* OK, got a block of IPs, try them. There is no need to
               reserve the IPs:
            *)
            try send_to_ips agent more_ips fmsg with
            [
                SMTP_Error_Body_Sent _ as error ->
                    (* Don't re-try: *)
                    raise error

              | SMTP_Permanent_Error _ as error ->
                    (* Don't re-try either: *)
                    raise error

              | any_other ->
                (* Any other error: Try other IP blocks, if any: *)
                smtp_transact_cont
                    agent dns_agent cont_obj (prev_errs @ [any_other]) fmsg
            ]
    ]
with
[hmm ->
    (* A DNS error: No point in trying further, as "Dns_client.more_mx_ips"
       had already tried to by-pass the errors encountered:
    *)
    raise (fold_smtp_errors agent (prev_errs @ [wrap_dns_error hmm]))
];

value smtp_transact_lazy:
    smtp_agent -> Dns_client.dns_agent -> Mime_xml_types.smtp_msg_final ->
    unit       =

fun agent dns_agent fmsg ->
    (*  Get the initial block of IPs, plus the Continuation Object,  with
        timing.  Here we have to make the  "cont_obj" optional in case of
        error, not the error itself, since "cont_obj" is of abstract type
        and does not have a suitable default:
    *)
    let (init_ips, cont_obj) =
        try
            Dns_client.init_mx_ips dns_agent fmsg.Mime_xml_types.to_domain''
        with
        [hmm -> raise (wrap_dns_error hmm)]
    in
    do {
        (* Try to run a transaction over the IPs we got. There is no need
           to reserve them:
        *)
        try send_to_ips agent init_ips fmsg with
        [
            SMTP_Error_Body_Sent _ as error ->
                (* Don't re-try: *)
                raise error

          | SMTP_Permanent_Error _ as error ->
                (* Don't re-try either -- permanent failure: *)
                raise error

          | hmm ->
                (* Anything else: as this is NOT a DNS error, the cont obj
                   does exit, and we can try the next block,  but remember
                   the error we got:
                *)
                smtp_transact_cont agent dns_agent cont_obj [hmm] fmsg
        ]
    };

(*---------------------------*)
(*  "smtp_transact_strict":  *)
(*---------------------------*)
(* Performs a "strict" DNS look-up, and then a transaction: *)

value smtp_transact_strict:
      smtp_agent -> Dns_client.dns_agent -> Mime_xml_types.smtp_msg_final ->
      unit       =

fun   agent dns_agent fmsg ->
    (* Get all IPs for this mail domain, in random order: *)
    let to_domain = fmsg.Mime_xml_types.to_domain'' in
    let all_ips   =
        try
            Dns_client.all_mx_ips dns_agent to_domain
        with
        [hmm -> raise (wrap_dns_error hmm)]
    in
    (* "send_to_ips" will contain policies regarding IP reservation and
       replication. It will fail if all_ips=[]:
    *)
    send_to_ips agent  all_ips fmsg;

(*======================*)
(*  "send_final_msg*":  *)
(*======================*)
(*  Top-level Connect-and-Send functions: *)

value rec send_final_msg':
    smtp_agent                    ->
    smtp_agent_stats_accum        ->
    Mime_xml_types.smtp_msg_final ->
    unit                          =

fun agent accum fmsg ->
do {
    trace agent ("\r\n[STARTING SMTP TRANSACTION FOR "  ^
                  fmsg.Mime_xml_types.to_addr''^"]\r\n");

    (* Do we already have a saved connection to that domain?  (In case when
       persistent connections are allowed):
    *)
    if  agent.use_persistence &&
        agent.conn_domain = fmsg.Mime_xml_types.to_domain''
    then
    do {
        (* Assume that we already have a connection. Try to run a "headless"
           sending dialogue over it -- no need to begin with "HELO". We must
           set the "deadline" here, as we go into the dialogue now.   In the
           "smtp_dialogue_headless", the "reuse" flag is "True":
        *)
        trace agent ("[RE-USING CONNECTION TO "^agent.conn_domain^"]\r\n");

        agent.deadline := Unix.gettimeofday() +. agent.attempt_timeout_sec;
        try
        do {
            (* Run the dialogue: *)
            smtp_dialogue_headless agent fmsg True;

            (* Dialogue over re-used connection successful: *)
            accum.reused_conns':= accum.reused_conns' + 1
        }
        with
        [
            err ->
            do {
                (* Got an error; in any case, close the connection without
                   sendting "QUIT". This also re-sets "agent.conn_domain",
                   so the next attempt (if any)  will be  without re-using 
                   the connection:
                *)
                close_smtp_conn agent ~polite_quit:False;

                (* Now examine the error: *)
                match err with
                [
                    SMTP_Error_Body_Sent _ ->
                        (* The msg body has actually been sent -- no retry: *)
                        raise err

                  | SMTP_Permanent_Error _ ->
                        (* The msg has not been delivered,  and there is no
                           point in re-trying it. The persistent connection
                           could still be usable, but we have closed it, in
                           case if the server got into a "dead-end" state:
                        *)
                        raise err

                  | _ ->
                        (* Any other error: The connection failed or become
                           unusable, but from the high-level SMTP  point of
                           view, the destination may still be OK, so re-try
                           it with the persistent connection removed:
                        *)
                        send_final_msg' agent accum fmsg
                ]
            }
        ]
    }
    else
        (* No persistent connection to the "to_domain" found. What we do,
           depends on the configured Resolver:
        *)
        match agent.resolver with
        [
            Resolver_Lazy   dns_agent     ->
                (* Obtain the IPs by a Lazy   DNS look-up: *)
                smtp_transact_lazy      agent dns_agent fmsg

        |   Resolver_Strict dns_agent     ->
                (* Obtain the IPs by a Strict DNS look-up: *)
                smtp_transact_strict    agent dns_agent fmsg

        |   Resolver_PreCached use_ips    ->
                send_to_ips agent use_ips.val fmsg

        |   Resolver_Scheduled sched_inter ->
                (* Obtain the IPs from a Scheduler call-back: *)
                send_to_ips agent sched_inter.use_ips fmsg
        ]
};

(*  "send_final_msg" itself:
    The external wrapper, measuring the (transaction) statistics:
*)
value send_final_msg:
    smtp_agent                    ->
    smtp_agent_stats_accum        ->
    Mime_xml_types.smtp_msg_final ->
    unit                          =

fun agent accum fmsg ->
    (* Do the send, with timimg: *)

    let before = Unix.gettimeofday()         in
    let mb_err =
        try
        do {
            send_final_msg' agent accum fmsg;
            None
        }
        with [ e -> Some e ]
    in
    let after  = Unix.gettimeofday () in
    do {
        accum.total_transacts' := accum.total_transacts' + 1;
        accum.total_trans_time := accum.total_trans_time +. after -. before;
        update_av_dns_time  agent accum;

        match mb_err  with
        [   None   ->
                (* The transaction was successful: *)
                accum.ok_transacts' := accum.ok_transacts' + 1

          | Some e ->
            do {
                (* We got an exception in this transaction: *)
                trace agent ("[EXCEPTION: "^(Misc_utils.print_exn e)^"]\r\n");
                raise e
            }
        ]
    };

(*=========================*)
(*  ReConfiguring Agents:  *)
(*=========================*)
(*---------------------------*)
(*  "config_of_smtp_agent":  *)
(*---------------------------*)
(* Read the config back from an Agent: *)

value config_of_smtp_agent: smtp_agent -> smtp_agent_config =

fun agent ->
{
    conf_ip_resolv_mode     =  config_of_resolver agent.resolver;
    conf_attempt_timeout_sec=  agent.attempt_timeout_sec;
    conf_n_retries          =  agent.n_retries;
    conf_server_port        =  agent.server_port;
    conf_sock_buff_size     =  agent.sock_buff_size;
    conf_client_ips         =  Array.copy agent.client_ips;
    conf_client_domains     =  Array.copy agent.client_domains;
    conf_dry_run            =  agent.dry_run;
    conf_trace_file         =  agent.trace_file;
    conf_use_persistence    =  agent.use_persistence;
    conf_use_pipelining     =  agent.use_pipelining;
    conf_strict_5xx_mode    =  agent.strict_5xx_mode
};

(*--------------------------*)
(* "reconfigure_resolver":  *)
(*--------------------------*)
value reconfigure_resolver: smtp_agent -> ip_resolv_mode -> unit  =
fun agent new_mode ->
    match agent.resolver with
    [
      Resolver_Strict dns_agent ->
        match new_mode with
        [
          IP_Resolv_Lazy   new_conf ->
          do {
            (* Re-confifure the mutable "dns_agent": *)
            Dns_client.reconfigure_dns_agent dns_agent new_conf;

            (* Change the mode: *)
            agent.resolver:= Resolver_Lazy dns_agent
          }

        | IP_Resolv_Strict new_conf ->
            (* Same mode here. Just re-configure the "dns_agent": *)
            Dns_client.reconfigure_dns_agent dns_agent new_conf

        | IP_PreCached | IP_Scheduled _ ->
          do {
            (* The "dns_agent" is discarded, with finalisation: *)
            Dns_client.close_dns_agent dns_agent;

            (* Resolver is re-initialised: *)
            agent.resolver := mk_resolver new_mode
          }
        ]

    | Resolver_Lazy dns_agent ->
        match new_mode with
        [
          IP_Resolv_Lazy   new_conf ->
            (* Same mode here. Just re-configure the "dns_agent": *)
            Dns_client.reconfigure_dns_agent dns_agent new_conf

        | IP_Resolv_Strict new_conf ->
          do {
            (* Re-configure the mutable "dns_agent": *)
            Dns_client.reconfigure_dns_agent dns_agent new_conf;

            (* Change the mode: *)
            agent.resolver:= Resolver_Strict dns_agent
          }

        | IP_PreCached | IP_Scheduled _ ->
          do {
            (* The "dns_agent" is discarded, with finalisation: *)
            Dns_client.close_dns_agent dns_agent;

            (* The resoolver is re-initialised: *)
            agent.resolver:= mk_resolver new_mode
          }
        ]

    | Resolver_PreCached _   ->
        (* The old Resolber object can be safely discarded without
           finalisation. Just do new initialisation from scratch:
        *)
        agent.resolver:= mk_resolver new_mode

    | Resolver_Scheduled _ ->
      do {
        (* Hmm... To be on a safe side, we need to release the curr reserved
           IP if the Resolver changes, and for that we actually need to dis-
           connect from that IP (if we are connected to it):
        *)
        close_smtp_conn agent ~polite_quit:True;
        agent.resolver:= mk_resolver new_mode
      }
    ];

(*-----------------------------*)
(*  "reconfigure_smtp_agent":  *)
(*-----------------------------*)
(* Re-configuring is different from Agent creation: only static fields
   in the Agent are affected.  It is NOT checked whether the fields of
   new config are valid, --  they are assumed to be so,  since the new
   config is normally derived from a "live" one obtained via  "config_
   of_smtp_agent".  In particular, there is again NO automatic verifi-
   cation of the client IPs and Domains.
   IMPORTANT: the Resolver is NOT re-configured by this function;  use
   "reconfigure_resolver" explicitly for that purpose:
*)
value reconfigure_smtp_agent:
    smtp_agent -> smtp_agent_config -> unit =

fun agent c ->
do {
    agent.attempt_timeout_sec   := c.conf_attempt_timeout_sec;
    agent.n_retries             := c.conf_n_retries;
    agent.server_port           := c.conf_server_port;
    agent.client_ips            := Array.copy c.conf_client_ips;
    agent.client_domains        := Array.copy c.conf_client_domains;
    agent.dry_run               := c.conf_dry_run;
    agent.trace_file            := c.conf_trace_file; (* Old file NOT closed! *)
    agent.use_persistence       := c.conf_use_persistence;
    agent.use_pipelining        := c.conf_use_pipelining;
    agent.strict_5xx_mode       := c.conf_strict_5xx_mode
};

(*-------------------------*)
(*  "verify_domains_ips":  *)
(*-------------------------*)
(*  This verification function must be EXPLICITLY invoked when the caller has
    finally set the local IPs and Domains on the Agent:
*)
value verify_domains_ips: array string -> array Unix.inet_addr -> unit =
fun domains ips ->
    let  len_doms = Array.length domains in
    let  len_ips  = Array.length ips     in
    if   len_doms = 0
    then failwith "Smtp_client.verify_domains_ips: Empty local domains"
    else
    if   len_ips  = 0
    then failwith "Smtp_client.verify_domains_ips: Empty local IPs"
    else
    if   len_doms <> len_ips
    then failwith ("Smtp_client.verify_domains_ips: "^
                   "Mismatching lengths of local IPs and Domains")
    else
    try
    do {
        for i = 0 to (len_doms-1)
        do {
            let dom = String.lowercase domains.(i) in
            let ip  = ips.(i)                      in
            do {
                (* First, do a check with FORWARD DNS look-ups: *)

                let h1 = Unix.gethostbyname dom    in
                if  Misc_utils.array_mem ip h1.Unix.h_addr_list
                then
                    () (* OK for now... *)
                else failwith
                    ("Smtp_client.verify_domains_ips: Invalid IP for "^dom^
                     ": "^(Unix.string_of_inet_addr ip));

                (* Then, do a check with REVERSE DNS look-ups:  *)

                let h2 = Unix.gethostbyaddr ip in
                if  String.lowercase h2.Unix.h_name = dom
                then
                    () (* OK for now... *)
                else failwith
                    ("Smtp_client.verify_domains_ips: Invalid Domain for "^
                    (Unix.string_of_inet_addr ip)^": "^dom)
            }
        }
    }
    with
    [ hmm ->
        failwith ("Smtp_client.verify_domains_ips: Error: "^
                 (Misc_utils.print_exn hmm))
    ];

