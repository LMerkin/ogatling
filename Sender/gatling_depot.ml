(*pp camlp4r *)
(* vim:ts=4:syntax=ocaml
*)
(*===========================================================================*)
(*                             "gatling_depot.ml":                           *)
(*                                                                           *)
(*                 Sender for User-Generated E-Mail Messages                 *)
(*                                                                           *)
(*                  (C) Explar Technologies Ltd, 2003--2006                  *)
(*===========================================================================*)
(*==============================*)
(*  Configuration and Start-Up: *)
(*==============================*)
exception ReSendable_Error of string;

value invalid_name     = "";
value invalid_count    = (-1);
value invalid_time     = (-1.0);

value xml_config_file  = ref invalid_name;

(* Some constants -- probably no point to make them configurable: *)
value msg_file_sffx    = ".msg";
value delayed_q_subdir = "Delayed_Messages";
value admin_name       = "PostMaster";
value notif_subj       = "Error Notification";

(*----------------------*)
(*  Command-Line Parms: *)
(*----------------------*)
(* Configurable variables: *)
value version_gen = "Gatling Mail 2.6";

value version =
fun () ->
    do {
        print_endline "\t\tGatling_Depot version 2.6.1,  2006-08-28";
        print_endline "\t\t(C) Explar Technologies Ltd,  2000-2006";
        flush stdout;
        Tnio.exit 0
    };

value usage =
fun rc ->
do {
    print_endline "PARAMETERS: {--version | --help | XML_Config_File}";
    flush stdout;
    Tnio.exit rc
};

if  Array.length Sys.argv <> 2
then usage 1
else
if  Sys.argv.(1) = "--help"
then usage 0
else
if  Sys.argv.(1) = "--version"
then version ()
else
    (* This should be the XML Conf File: *)
    xml_config_file.val := Sys.argv.(1);


(*----------------------------*)
(*  Parse the XML Conf File:  *)
(*----------------------------*)
value ext_conf =
    try Depot_xml_types.consult_external_conf xml_config_file.val
    with
    [ _ ->
        Misc_utils.fatal_error
        ("Invalid XML Config File: \""^xml_config_file.val^"\"" )
    ];

(*  Create the array of INTERNAL confs: *)
value int_confs = Depot_xml_types.mk_internal_confs ext_conf;

assert (Array.length int_confs >= 1);
value conf0 = int_confs.(0);

(*  Some global parms: *)
value licence_file      = conf0.Depot_xml_types.licence_file;
value msg_buff_dir      = conf0.Depot_xml_types.msg_buff_dir;
value msg_buff_dir_lock = conf0.Depot_xml_types.msg_buff_dir_lock;
value log_file          = conf0.Depot_xml_types.log_file;
value pid_file          = conf0.Depot_xml_types.pid_file;
value admin_email       = conf0.Depot_xml_types.admin_email;
value n_threads         = conf0.Depot_xml_types.n_threads;
value thread_stack_k    = conf0.Depot_xml_types.thread_stack_k;
value poll_period       = conf0.Depot_xml_types.poll_period;
value daemon_mode       = conf0.Depot_xml_types.daemon_mode;
value restart_after     = conf0.Depot_xml_types.restart_after;
value max_blocking_time = conf0.Depot_xml_types.max_blocking_time;
value trace_dialogue    = conf0.Depot_xml_types.trace_dialogue;
value tnio_events_mech  = conf0.Depot_xml_types.tnio_events_mech;
value run_as_user       = conf0.Depot_xml_types.run_as_user;
value run_as_group      = conf0.Depot_xml_types.run_as_group;
value use_rt_priority   = conf0.Depot_xml_types.use_rt_priority;
value dns_servers       = conf0.Depot_xml_types.dns_servers;
value dns_cache_size    = conf0.Depot_xml_types.dns_cache_size;
value dns_timeout       = conf0.Depot_xml_types.dns_timeout;
value dns_retries       = conf0.Depot_xml_types.dns_retries;
value sock_buff_size    = conf0.Depot_xml_types.sock_buff_size;
value err_note_file_txt = conf0.Depot_xml_types.err_note_file_txt;
value err_note_file_html= conf0.Depot_xml_types.err_note_file_html;
value our_tz            = conf0.Depot_xml_types.tz_offset;

(*--------------------------*)
(*  Other Init Operations:  *)
(*--------------------------*)
(* Initialise the TNIO RTS.    We currently use TWO priority levels:
   one for the worker threads, the other for the management interface:
*)
Tnio.init
    ~io_events_mech: tnio_events_mech
    ~rt_priority   : use_rt_priority
    (Tnio.Priority_Bands 2);

(*  Verify the Licence.
    We use a default 15-dec time-out for secure Internet time requests:
*)
(*  XXX: Temporarily configured out, as OpenSSL support appeared to be
    non-thread-safe!

Enforcement.verify_licence
    licence_file
    (fun () -> Secure_time.gmtime 15.0);
*)

(* Change Privileges:  *)
try
    Misc_utils.run_as run_as_user run_as_group
with
    [hmm -> Misc_utils.fatal_error (Misc_utils.print_exn hmm)];

(*  Also, construct the regexps corresponding to the "int_confs"; the "0"th
    entry can be any; the rest are taken from external confs:
*)
value domain_sels: array (string * Pcre.regexp) =
    Array.init
    (Array.length int_confs)
    (fun i ->
        if  i = 0
        then
            (* Does not matter: *)
            (".*", Pcre.regexp ".*")
        else
        let gr_i = List.nth ext_conf.Depot_xml_types.domain_groups (i-1)
        in
            Domains_config.compile_domain_selectors
                gr_i.Depot_xml_types.selector_regexps
    );

(*====================*)
(*  File Operations:  *)
(*====================*)
(*-----------------*)
(*  "open_write":  *)
(*-----------------*)
(*  Opens a file for over-writing or appending:  *)

value open_write: string -> bool -> string -> Unix.file_descr =
fun file_name append role ->
    let mbfd =
        Misc_utils.open_write_locked
            ~append_mode :append
            ~lock_fail_rc:Domains_config.no_restart_rc
            file_name
    in
    match mbfd with
    [ None    ->
        Misc_utils.fatal_error ("ERROR: The "^role^" file was not specified")

    | Some fd ->
        fd
    ];

(*----------------*)
(*  "check_dir":  *)
(*----------------*)
value check_dir: string -> unit =
fun dir ->
    try
        if  (Unix.stat dir).Unix.st_kind <> Unix.S_DIR
        then
            failwith ("ERROR: Not a directory: \""^dir^"\"")
        else
            (* Assert access rights: *)
            Unix.access dir [Unix.R_OK; Unix.W_OK; Unix.X_OK]
    with
        [   _ ->
            (* Any exception in "stat" or "access": *)
            failwith
                ("ERROR: Invalid or inaccessible directory: \""^dir^"\"")
        ];


(*-----------------------------------*)
(*  Open/Verify all Files and Dirs:  *)
(*-----------------------------------*)
(* Log File:  *)
value log_fd      = open_write log_file True "Log";
Unix.set_close_on_exec log_fd;

(* Check the Message Buffer Dir: *)
try  check_dir msg_buff_dir
with [Failure hmm -> Misc_utils.fatal_error hmm];

(* Check the Delayed Queue Sub-Dir: it is created if does not exist: *)
value delayed_q_dir =
     Filename.concat msg_buff_dir delayed_q_subdir;

try  check_dir delayed_q_dir
with
[   _ ->
    try Unix.mkdir delayed_q_dir 0o700
    with
    [   hmm ->
            (* This is unlikely, since the parent dir exists and is writable: *)
            Misc_utils.fatal_error
            ("Cannot create the directory \""^delayed_q_dir^"\": "^
            (Misc_utils.print_exn hmm))
    ]
];

(* Message Buffer Lock File -- to lock out concurrent processes: *)
value msg_buff_lock_fd  =
    open_write msg_buff_dir_lock False "Msg Buff Dir Lock";

Unix.set_close_on_exec   msg_buff_lock_fd;

(* Send Counter: *)
value send_counter      = ref 0;
value will_restart      = ref False;


(*----------------*)
(*  "log":        *)
(*  "close_log":  *)
(*----------------*)
(*  Logs the time, destination e-mail adress (if any) on which the condition
    has occurred, and the condition message, In log, the time is LOCAL:
*)
value log: string -> string -> unit =
fun to_addr cond_msg ->
    let curr_time  = Unix.localtime (Unix.time ()) in

    let time_stamp = Printf.sprintf "%04d-%02d-%02d %02d:%02d:%02d"
                           (curr_time.Unix.tm_year + 1900)
                           (curr_time.Unix.tm_mon  + 1)
                            curr_time.Unix.tm_mday  curr_time.Unix.tm_hour
                            curr_time.Unix.tm_min   curr_time.Unix.tm_sec
    in
    let log_line   = Printf.sprintf "%s %s (PID=%d): %s\n\n"
                            time_stamp to_addr (Unix.getpid ()) cond_msg 
    in
    Misc_utils.write_fstr log_fd log_line;


value close_log: unit -> unit =
fun () ->
    Misc_utils.safe_close log_fd;


(*---------------------------*)
(*  Message Buffer Locking:  *)
(*---------------------------*)
(*  Any exception while locking is fatal. The locking mechanism is "lockf",
    not "flock" or "fcntl" (although in Linux "lockf" uses "fcntl") -- XXX:
    what if an external process uses "flock" instead?     Will it still be
    locked out?
*)
value   lock_msg_buff: unit -> unit =
fun () ->
try
do {
    Misc_utils.lock_or_abort
        ~rc:Domains_config.no_restart_rc msg_buff_dir_lock msg_buff_lock_fd;

    (* Write 2 bytes at the beginning of the file,  to update its modification
       time for the "last_activity" function -- this function is invoked peri-
       odically from the "sender_loop", via "pick_user_msg":
    *)
    ignore (Unix.write   msg_buff_lock_fd  "X\n" 0 2);
}
with
[any ->
    (* Error during locking -- no good: *)
    Misc_utils.fatal_error
        ("Could not lock the Msg Buff Dir: "^ Misc_utils.print_exn any)
];


value unlock_msg_buff: unit -> unit =
fun () ->
try
do {
    (* No writing into the lock file is performed here: *)
    ignore  (Unix.lseek msg_buff_lock_fd  0 Unix.SEEK_SET);
             Unix.lockf msg_buff_lock_fd    Unix.F_ULOCK 0;
}
with
[   any ->
        (* Error during unlocking -- no good: *)
        Misc_utils.fatal_error ("Could not unlock the Msg Buff Dir: "^
                               Misc_utils.print_exn any)
];


(*  "critical_section_msg_buff":
    Runs an arbitrary piece of code with "msg_buff" locked. The code is
    abstracted into a function to force late evaluation. All exceptions
    are caught and re-raised after the lock is removed:
*)
value critical_section_msg_buff: (unit -> 'a) -> 'a =
fun f ->
    do {
        lock_msg_buff ();

        let res = try f () with
        [   hmm ->
            do {
                (* Error: remove the lock,   and re-raise the exception: *)
                unlock_msg_buff ();
                raise  hmm
            }
        ]
        in
        do {
            (* Normal completion: remove the lock, and return the "res": *)
            unlock_msg_buff ();
            res
        }
    };


(*========================*)
(*  Agents Configuration: *)
(*========================*)
(* We start from default configurations, and then modify those fields for
   which the command-line options were given. At this point, the validity
   of options is not chcked, -- it will be done when the configs are used:
*)
(*-------------------*)
(* DNS Agent Config: *)
(*-------------------*)
(*  This configuration is global. The values in "conf0" are always valid --
    no extra checks required:
*)
value dns_config:
           Dns_client.dns_agent_config =
           Dns_client.default_dns_agent_config ();

dns_config.Dns_client.conf_timeout_sec   := dns_timeout;
dns_config.Dns_client.conf_n_retries     := dns_retries;
dns_config.Dns_client.conf_cache_size    := dns_cache_size;
dns_config.Dns_client.conf_dns_servers   := dns_servers;
dns_config.Dns_client.conf_sock_buff_size:= sock_buff_size;

(*--------------------*)
(* SMTP Agent Config: *)
(*--------------------*)
(*  Only the globally-configurable fields are set here. The rest are done
    by re-configuring the Agents during sends:
*)
value smtp_config:
    Smtp_client.smtp_agent_config =
    Smtp_client.default_smtp_agent_config ();

smtp_config.Smtp_client.conf_sock_buff_size :=
    sock_buff_size;

smtp_config.Smtp_client.conf_ip_resolv_mode :=
    Smtp_client.IP_Resolv_Strict dns_config;

(*-------------*)
(* Validation: *)
(*-------------*)
(* Traces, if required, are written into the corresp "NNN.tr" files: *)
if  n_threads > 9999
then
    Misc_utils.fatal_error
        ("Too many threads requested: "^(string_of_int n_threads))
else
    ();

(*  Verify the consistency of Domains and IPs for all "int_confs": *)
Array.iter
(fun conf ->
    if  conf.Depot_xml_types.verify_locals
    then
    try
        Smtp_client.verify_domains_ips
        (Array.of_list conf.Depot_xml_types.local_domains)
        (Array.of_list
            (List.map Unix.inet_addr_of_string
            conf.Depot_xml_types.local_ips)
        )
    with
    [ Failure hmm ->
        Misc_utils.fatal_error
        ("ERROR: Inconsistent Local IPs and Domains for Group "^
        conf.Depot_xml_types.from_group_name^": "^hmm)
    ]
    else
        () (* Skip verification *)
)
int_confs;

(* The number of Server IPs per each domain encountered: *)
value n_server_ips: Hashtbl.t string int = Hashtbl.create 100000;


(*---------------------------------*)
(*  Current load on Domain Groups: *)
(*---------------------------------*)
(*  A Domain either belongs to one of the pre-configured Groups, or forms its
    own Singleton Group:
*)
(* Map GroupID => Load:   *)
value curr_group_load: Hashtbl.t string int = Hashtbl.create 100000;

(* Map ThreadID=> GroupID *)
value thread_groups  : Hashtbl.t int string = Hashtbl.create 1000;


(*================================*)
(*  Operations on User Messages:  *)
(*================================*)
(*  See "Depot_xml_types.user_msg": *)

(*--------------------*)
(*  "load_user_msg":  *)
(*--------------------*)
(* "user_msg"s are created by reading the corresp XML files in the Msg
    Buff Dir:
*)
value load_user_msg: string -> Depot_xml_types.user_msg =
fun file_name ->
try
    let cnnl = open_in file_name  in
    try
        let strm = Stream.of_channel cnnl in
        let xstr = IoXML.parse_xml   strm in
        let umsg = Depot_xml_types.xparse_user_msg xstr in
        do {
            close_in cnnl;
            (* Ensure that the file name recorded inside the "umsg" is indeed
               the one it was read from:
            *)
            {(umsg) with Depot_xml_types.umsg_file = file_name}
        }
    with
    [   hmm ->
        do {
            close_in cnnl;
            raise hmm
        }
    ]
with
[ err -> failwith (Misc_utils.print_exn err) ];


(*--------------------*)
(*  "save_user_msg":  *)
(*--------------------*)
(*  Inverse of "load_user_msg". NB: This function does NOT lock the msg dir:
    the CALLER must do that. The file name is randomy generated, so there is
    no need to lock that file against concurrent writing.
    XXX:  while closing the output channel, the corresp "fd" will presumably
    be closed automatically:
*)
value save_user_msg: Depot_xml_types.user_msg -> string -> unit =
fun umsg  msg_kind ->
try
    let cnnl = open_out umsg.Depot_xml_types.umsg_file in
    try
        let form = Format.formatter_of_out_channel cnnl  in
        do {
            IoXML.xprint form "@[%a@]@." Depot_xml_types.xprint_user_msg umsg;
            close_out    cnnl
        }
    with
    [   hmm ->
        do {
            close_out  cnnl;
            raise hmm
        }
    ]
with
[ _ -> failwith ("Cannot Save the "^msg_kind^" File: \""^
                 umsg.Depot_xml_types.umsg_file^"\"") ];


(*---------------------*)
(*  "final_user_msg":  *)
(*---------------------*)
(* A "user_msg" can now be readied.  If it is indeed a message originating from
   an end-user, its contents is assumed to be already fully generated:  no tags
   are used.   If, however, it's an automatically-generated error notification,
   then a number of tags are recognised. Note that the body of the original msg
   is ALREADY inserted into the error notification,  so it is not a tag  (since
   it would be difficult to carry that body along in order to insert it later):
*)
value final_user_msg:
    Depot_xml_types.user_msg      -> Depot_xml_types.internal_conf ->
    Mime_xml_types.smtp_msg_final =

fun umsg conf ->
    (* First, create a "Mime_codec.smtp_msg_info" object. Note that the
       "*_from" addresses are those of the user, not of our admin; this
       may cause problems with the server side suspecting we are a spam
       relay,   but has an advantage of sending "more personal" msgs to
       the users' destinations,  and also relieves us from managing the
       bounced messages.
       NB: GMT is used for SMTP time calculations:
    *)
    let umsg_info =
    {
        Mime_xml_types.strict_lines = conf.Depot_xml_types.strict_lines;
        Mime_xml_types.envel_from   = umsg.Depot_xml_types.from_email;
        Mime_xml_types.body_from    = umsg.Depot_xml_types.from_email;
        Mime_xml_types.msg_to       = umsg.Depot_xml_types.to_email;
        Mime_xml_types.subject      = umsg.Depot_xml_types.subject;
        Mime_xml_types.priority     = umsg.Depot_xml_types.priority;

        Mime_xml_types.date         = Mime_codec.smtp_time_stamp
                                      ~tz_off:our_tz (Unix.time ());

        Mime_xml_types.msg_id       = Printf.sprintf "%08d" send_counter.val;
        Mime_xml_types.mailer_ver   = version_gen;
        Mime_xml_types.tags         = [];
        (* Tags are used for error notifications only,   and substituted
           when the msg body is just created, so there are no tags here!
        *)
        Mime_xml_types.content      = umsg.Depot_xml_types.content
    }
    in
    Mime_codec.mk_final_msg Mime_codec.null_tags_map
                           (Mime_codec.cook_msg umsg_info);


(*=====================*)
(*  Delayed Messages:  *)
(*=====================*)
(*  See the "Depot_xml_types.delayed_msg" type. The Delayed messages are
    organised into an in-memory Queue:
*)
module MQE =
struct
    type t = Depot_xml_types.delayed_msg;

    (* Comparison:
       The messages are primarily sorted wrt the "next_send" time. In case if
       the times of two messages are equal (an unlikely case),    more checks
       are performed:
    *)
    value compare: t -> t -> int =
    fun l r ->
        (* Primary comparison: *)
        let comp1 = compare l.Depot_xml_types.next_send
                            r.Depot_xml_types.next_send in
        if  comp1 <> 0
        then
            comp1
        else
            (* Secondary comparison:  Files are always different if the
               messages are indeed different, so compare the file names:
            *)
            compare l.Depot_xml_types.dmsg_file r.Depot_xml_types.dmsg_file;
end;

(* The Delayed Message Queue itself: *)
module DMQ = Set.Make (MQE);

(* The Delayed Queue is mutable, initially empty: *)
value delayed_msgs  = ref DMQ.empty;


(*-----------------------*)
(*  "load_delayed_msg":  *)
(*-----------------------*)
(*  Delayed Msgs can be loaded/saved from/to XML files in the same way as
    User Msgs:
*)
value load_delayed_msg: string -> Depot_xml_types.delayed_msg =
fun file_name ->
    let cnnl = open_in file_name  in
    try
        let strm = Stream.of_channel  cnnl in
        let xstr = IoXML.parse_xml    strm in
        let dmsg = Depot_xml_types.xparse_delayed_msg xstr in
        do {
            close_in cnnl;
            (* Ensure that the file name recorded inside the "dmsg" is indeed
               the one it was read from:
            *)
            {(dmsg) with Depot_xml_types.dmsg_file = file_name}
        }
    with
    [   hmm ->
        do {
            close_in cnnl;
            raise hmm
        }
    ];


(*---------------------*)
(*  "mk_delayed_msg":  *)
(*---------------------*)
(*  Creates a "delayed_msg" from a "user_msg" when the corresp "final_msg"
    is already known.   It is done either in case of error, or if the max
    allowed load on the destination Domain Group is exceeded:
*)
value mk_delayed_msg:
      Depot_xml_types.user_msg -> Depot_xml_types.internal_conf ->
      string                   -> Mime_xml_types.smtp_msg_final ->
      string                   -> Depot_xml_types.delayed_msg   =

fun umsg conf grp fmsg last_err ->

    (* NB: remove the original directory prefix from "umsg.umsg_file"! *)
    let delayed_file_name =
        Filename.concat   delayed_q_dir
       (Filename.basename umsg.Depot_xml_types.umsg_file)
    in
    (* If "last_error" is "", there was actually no error, just too much
       load on the destination Domain Group:
    *)
    let resend_delay =
        if  last_err = ""
        then
            (* Try to re-send soon: *)
            conf.Depot_xml_types.load_retry_interval
        else
            (* Real error: *)
            conf.Depot_xml_types.delivery_retry_interval
    in
    let prev_errors  =
        if  last_err = ""
        then []     (* None *)
        else [last_err]
    in
    {
        Depot_xml_types.conf        = conf;
        Depot_xml_types.group_id    = grp;
        Depot_xml_types.orig_fmsg   = fmsg;
        Depot_xml_types.orig_umsg   = umsg;
        Depot_xml_types.dmsg_file   = delayed_file_name;
        Depot_xml_types.next_send   = Unix.gettimeofday () +. resend_delay;
        Depot_xml_types.prev_errors = prev_errors
    };


(*-----------------------*)
(*  "save_delayed_msg":  *)
(*-----------------------*)
(*  As in "save_user_msg", the caller is responsible for directory locking: *)

value save_delayed_msg: Depot_xml_types.delayed_msg -> unit =
fun dmsg ->
try
    let fd   = open_write dmsg.Depot_xml_types.dmsg_file False "Delayed Msg" in
    let cnnl = Unix.out_channel_of_descr  fd in
    try
        let form = Format.formatter_of_out_channel cnnl in
        do {
            IoXML.xprint form "@[%a@]@."
                Depot_xml_types.xprint_delayed_msg dmsg;
            (* Again, closing the channel implies closing the "fd": *)
            close_out  cnnl
        }
    with
    [   hmm ->
        do {
            close_out  cnnl;
            raise hmm
        }
    ]
with
[ _ -> failwith ("Cannot Save the Delayed File: \""^
                dmsg.Depot_xml_types.dmsg_file^"\"") ];


(*----------------------------------------------*)
(*  So: Load the Delayed Messages at start-up:  *)
(*----------------------------------------------*)
(*  No locking is required here, as this function is called before the threads
    are created:
*)
value load_delayed_msgs:  unit -> unit =
fun () ->
try
    let ddh = Unix.opendir delayed_q_dir  in

    let rec load_next_entry =
    fun () ->
        let fn = Unix.readdir ddh in
        do {
            if  ExtString.String.ends_with  fn msg_file_sffx
            then
                (* Yes, it's a message file -- load it. Don't forget to
                   use the FULL file name! The file will not be deleted
                   until the message is successfully sent or completely
                   failed:
                *)
                let ffn = Filename.concat delayed_q_dir fn in
                try
                    let dm = load_delayed_msg ffn in
                    delayed_msgs.val := DMQ.add  dm delayed_msgs.val
                with
                [   hmm ->
                    do {
                        log "INIT"
                        ("Invalid Delayed Msg File, removing it: \""^ffn^
                         "\": "^(Misc_utils.print_exn hmm)^
                         "\n\n"^(Misc_utils.read_file ffn));

                        Unix.unlink ffn
                    }
                ]
            else
                (* Skip this entry: *)
                ();

            (* Try to load more: *)
            load_next_entry ()
        }
    in
    try
        load_next_entry ()
    with
        [   End_of_file ->
            Unix.closedir ddh   (* All done! *)
        ]
with
    [   other_err ->
            (* E.g., could not open the dir? *)
            failwith ("Cannot load Delayed Messages: "^
                     (Misc_utils.print_exn other_err))
    ];


(*=======================*)
(*  Error Notifications: *)
(*=======================*)
(*  Combined type for User and Delayed Msgs:  *)
type gen_msg = [ U of Depot_xml_types.user_msg
               | D of Depot_xml_types.delayed_msg];

(*  Tags which may occur in Notifications: *)
value admin_tags =
[
    "[[FROM_NAME]]";  (* Corresponds to "from_name" in the orig msg *)
    "[[TO_EMAIL]]" ;  (* ...            "to_email"  in the orig msg *)
    "[[ADMIN_EMAIL]]";
    "[[ERRORS]]"      (* What actually happened      *)
];

(*-----------------------------------------------*)
(*  Read and Parse the Error Notification Files: *)
(*-----------------------------------------------*)
(* Error Note File (plain text) and its contents. It is parsed into "smtp_data"
   ready for tags substituition:
*)
value rec strip_eol_smtpd:
          Mime_xml_types.smtp_data -> Mime_xml_types.smtp_data =
fun
[   Mime_xml_types.SMTP_Chunk s ->
        Mime_xml_types.SMTP_Chunk (Misc_utils.strip_eol s)

  | Mime_xml_types.SMTP_Tag   t ->
        Mime_xml_types.SMTP_Tag t (* Assume no EOL in the tag anyway! *)

  | Mime_xml_types.SMTP_Line ls ->
        Mime_xml_types.SMTP_Line  (List.map strip_eol_smtpd ls)
];


value parse_err_note:  string -> string -> list Mime_xml_types.smtp_data =
fun file_name mime_type ->
try
    let lines = Misc_utils.read_lines file_name  in
    let body  = Mime_xml_types.Body_Simple
                {
                    Mime_xml_types.mime_type  = mime_type;
                    Mime_xml_types.file_name  = file_name;
                    Mime_xml_types.body_lines = lines
                }
    in
    let cdata = Mime_codec.cook_part_body
                ~is_all_ascii: (Mime_codec.is_all_ascii body)
                ~strict_mode : True
                admin_tags     body
    in
    (* Hmm.. "cook_part_body" installed line terminators which we don't need
       here. So we have to strip off the extraneous "\r\n"s from all strings
       in "cdata":
    *)
    List.map strip_eol_smtpd cdata
with
    [ hmm ->
        Misc_utils.fatal_error
                  ("ERROR: Cannot use the "^mime_type^" ErrNote file (\"" ^
                  file_name ^"\"): "^(Misc_utils.print_exn hmm))
    ];


(* HTML and Plain-Text notifications pre-parsed. They are shared between all
   Domain Groups,  but for some of them,  either HTML or Text  notifications
   can be configured off:
*)
value err_note_data_html = parse_err_note err_note_file_html "text/html";
value err_note_data_txt  = parse_err_note err_note_file_txt  "text/plain";

(*=====================*)
(*  Sending Messages:  *)
(*=====================*)
(*------------------*)
(*  "failed_send":  *)
(*------------------*)
(*  Called  when a total failure occurs (either first time or on re-send):
    no more delivery attempts:
*)
value failed_send:
    gen_msg -> Depot_xml_types.internal_conf -> string -> unit =

fun gmsg conf last_err  ->
    (* Extract the Msg info, depending on its type: *)

    (* NB: For "delayed_msg"s, the calling convention is that  "last_err"
           was not already included into "prev_errors":
    *)
    let all_errors = match gmsg with
        [  U umsg -> [last_err]
         | D dmsg -> dmsg.Depot_xml_types.prev_errors @ [last_err]
        ]
    in
    let to_email   = match gmsg with
        [  U umsg -> umsg.Depot_xml_types.to_email
         | D dmsg -> dmsg.Depot_xml_types.orig_umsg.Depot_xml_types.to_email
        ]
    in
    let from_name  = match gmsg with
        [  U umsg -> umsg.Depot_xml_types.from_name
         | D dmsg -> dmsg.Depot_xml_types.orig_umsg.Depot_xml_types.from_name
        ]
    in
    let from_email = match gmsg with
        [  U umsg -> umsg.Depot_xml_types.from_email
         | D dmsg -> dmsg.Depot_xml_types.orig_umsg.Depot_xml_types.from_email
        ]
    in
    let file_name  = match gmsg with
        [  U umsg -> umsg.Depot_xml_types.umsg_file
         | D dmsg -> dmsg.Depot_xml_types.dmsg_file
        ]
    in
    let orig_cont  = match gmsg with
        [  U umsg -> umsg.Depot_xml_types.content
         | D dmsg -> dmsg.Depot_xml_types.orig_umsg.Depot_xml_types.content
        ]
    in
    (* The "orig_parts" will be grouped together and quoted: *)
    let quot_part  =
    [{
        Mime_xml_types.disposition = Mime_xml_types.Disp_Inline;
        Mime_xml_types.body        = Mime_xml_types.Body_Nested orig_cont
    }]
    in
    (* Now: Log the errors: *)

    let err_msgs   = String.concat ", " all_errors in
    do {
        log to_email ("MAIL DELIVERY FAILED: "^err_msgs);

        (* Notify the sender: for that, simply schedule a notification msg,
           unless the msg that failed was itself a notification one:
        *)
        if  from_email = admin_email
        then
            (* This message was being sent FROM US, so it's an error notif-
               ication by itself: don't produce another notification on it:
            *)
            ()
        else
            (* Original client's message -- do notification.  A temporary file
               is crteated and put into the Msg Buff Dir. The notification msg
               itself is a plain-text/HTML alternative,   with original  parts
               quoted in attachments:
            *)
            let notif_file =
                Misc_utils.uniq_file_name msg_buff_dir "" msg_file_sffx
            in
            (* Prepare the map for tags substituition: *)
            let tags_map =
                let tags_map' = Mime_codec.null_tags_map in
                do {
                    Hashtbl.add tags_map' "[[FROM_NAME]]"   from_name;
                    Hashtbl.add tags_map' "[[TO_EMAIL]]"    to_email;
                    Hashtbl.add tags_map' "[[ADMIN_EMAIL]]" admin_email;
                    Hashtbl.add tags_map' "[[ERRORS]]"      err_msgs;
                    tags_map'
                }
            in
            (* The following function creates HTML and Plain-Text Notification
               Parts:
            *)
            let mk_notif_part: string -> Mime_xml_types.mime_msg_part =
            fun notif_mime_type ->
                let notif_lines =
                    (* Do the substituition to construct the "notif_body": *)
                    List.map
                        (Mime_codec.mk_final_string tags_map)
                        (if   notif_mime_type  = "text/html"
                         then err_note_data_html
                         else err_note_data_txt)
                in
                let notif_body =
                {
                    Mime_xml_types.mime_type   = notif_mime_type;
                    Mime_xml_types.file_name   = "";   (* No need for it! *)
                    Mime_xml_types.body_lines  = notif_lines
                }
                in
                (* Now the full Notification Part: *)
                {
                    Mime_xml_types.disposition =
                        Mime_xml_types.Disp_Inline;

                    Mime_xml_types.body        =
                        Mime_xml_types.Body_Simple notif_body
                }
            in
            (* Make HTML and Plain-Text Notification Parts:  *)
            let notif_part_html =
                if  conf.Depot_xml_types.use_html_err_note
                then
                    [mk_notif_part "text/html"]
                else
                    []
            in
            let notif_part_txt  =
                if  conf.Depot_xml_types.use_txt_err_note
                then
                    [mk_notif_part "text/plain"]
                else
                    []
            in
            (* Assemble the whole Notification Msg: *)
            let notif_umsg =
            {
                Depot_xml_types.umsg_file  = notif_file;
                Depot_xml_types.from_name  = admin_name;
                Depot_xml_types.from_email = admin_email;
                Depot_xml_types.to_email   = from_email;
                Depot_xml_types.subject    = notif_subj;
                Depot_xml_types.priority   = Mime_xml_types.MP_Highest;
                Depot_xml_types.content    =              (* Urgent! *)
                {
                    Mime_xml_types.composition =
                        Mime_xml_types.MultiPart_Mixed;

                    Mime_xml_types.parts       =
                        List.concat
                        [notif_part_html; notif_part_txt; quot_part]
                }
            }
            in
            (* Finally, save the Notification Msg  in the Msg Buff Dir to
               schedule it for sending:
            *)
            critical_section_msg_buff
            (fun () ->
                save_user_msg notif_umsg "Notification Msg"
            );

        (* In any case, delete the original (failed) msg file. In many cases,
           it would already be removed at this point -- so ignore any errors:
        *)
        try Unix.unlink file_name with [_ -> ()]
    };


(*---------------*)
(*  "send_msg":  *)
(*---------------*)
(*  Here the actual SMTP send and errors classification occur: *)

value send_msg :
    Smtp_client.smtp_agent              ->
    Smtp_client.smtp_agent_stats_accum  ->
    Mime_xml_types.smtp_msg_final       ->
    Depot_xml_types.internal_conf       ->
    string                              ->
    unit =

fun agent dummy fmsg conf grp ->
do {
    (*--------------*)
    (* Load Issues: *)
    (*--------------*)
    let thr_id = Tnio.thread_id (Tnio.thread_self ()) in
    let dom    = fmsg.Mime_xml_types.to_domain''      in
    do {
        (* Decrement the load on the previous group serviced by this thread: *)
        try
            let prev_grp  = Hashtbl.find thread_groups   thr_id   in
            let prev_load = Hashtbl.find curr_group_load prev_grp in
            do {
                assert (prev_load >= 1);

                if  prev_load > 1
                then
                    Hashtbl.replace curr_group_load prev_grp (prev_load - 1)
                else
                    Hashtbl.remove  curr_group_load prev_grp;

                (* Also, remove "prev_grp" from the "thread_groups": *)
                Hashtbl.remove thread_groups thr_id
            }
        with
        [Not_found -> (* Don't decrement then! *)
            ()
        ];

        (* Check the current load on the new "grp": *)
        let curr_load  =  if Hashtbl.mem  curr_group_load grp
        then
            let cl = Hashtbl.find curr_group_load grp  in
            do {
                assert (cl >= 1); cl
            }
        else
            0
        in
        (* Compute the maximum allowed load: *)
        let max_load = match conf.Depot_xml_types.group_threads_limit with
                       [ Depot_xml_types.Absolute n      ->
                            n
                       | Depot_xml_types.Per_Server_IP n ->
                            let n_ips = try Hashtbl.find n_server_ips dom with
                                        [Not_found -> 1]
                            in
                            do {
                                assert (n_ips >= 1);
                                n_ips * n
                            }
                        ]
        in
        if  curr_load >= max_load
        then
            (* Produce a fake "error", so the msg will be delayed;
               NB: ">" should not really happen here:
            *)
            raise (ReSendable_Error "")
        else
            (* Increment (or install) the load: *)
            if  curr_load = 0
            then
                Hashtbl.add     curr_group_load grp 1
            else
                Hashtbl.replace curr_group_load grp (curr_load + 1);

        (* Also, memoise the "grp" being dealt with by the current thread: *)
        Hashtbl.add thread_groups thr_id grp
    };
    (*-------------------------*)
    (* Now do the actual send: *)
    (*-------------------------*)
    try
        let ()    = Smtp_client.send_final_msg agent dummy fmsg in

        (* Update the Server IPs statistics: *)
        (*
        let _    = match Smtp_client.get_n_server_ips sres with
        [ Some n_ips ->
            do {
                assert (n_ips >= 1);
                Hashtbl.replace n_server_ips dom n_ips
            }
        | None -> ()
        ]
        in
        *)
        (* Successful send -- increment "send_counter" and evaluate the
           re-start condition:
        *)
        let new_send_counter =
        do {
            send_counter.val := send_counter.val + 1;
            send_counter.val
        }
        in
        if new_send_counter > restart_after
        then
        do {
            (* Request the re-start. However, the actual re-start will only
               occur when all threads terminate gracefully,  otherwise msgs
               currently being sent can be lost:
            *)
            log "SENDER" ("Performed "^(string_of_int restart_after)^
                          " sends, time to re-start");
            will_restart.val := True
        }
        else ()
    with
    [
        Smtp_client.SMTP_Transient_Error _ as err ->
            (* This exception cannot probably propagate to the top-level, but
               if it still does, the msg is probably re-sendable:
            *)
            raise (ReSendable_Error (Misc_utils.print_exn err))

      | other_err ->
            (* E.g.: (SMTP_Permanent_Error _), (SMTP_Error_Body_Sent _):  do
               NOT try to re-send the msg:
            *)
            raise other_err
    ]
};

(*-----------------------------*)
(*  "reconfigure_smtp_agent":  *)
(*-----------------------------*)
(* Re-configures the Agent. DNS Agent is NOT touched: *)

value reconfigure_smtp_agent:
    Smtp_client.smtp_agent -> Depot_xml_types.internal_conf -> unit =

fun agent conf ->
    let agent_conf = Smtp_client.config_of_smtp_agent agent in
    do {
        agent_conf.Smtp_client.conf_client_ips     :=
            Array.of_list
            (List.map
            Unix.inet_addr_of_string conf.Depot_xml_types.local_ips);

        agent_conf.Smtp_client.conf_client_domains :=
            Array.of_list (conf.Depot_xml_types.local_domains);

        agent_conf.Smtp_client.conf_use_persistence:=
            conf.Depot_xml_types.use_persistence;

        agent_conf.Smtp_client.conf_use_pipelining :=
            conf.Depot_xml_types.use_pipelining;

        Smtp_client.reconfigure_smtp_agent agent agent_conf
    };


(*-----------------*)
(*  "find_group":  *)
(*-----------------*)
(*  Try all explicitly-configured Domain Groups. If none of them matches the
    given "dom", select the Default Group (at 0). Returns (Config, GroupID):
*)
value find_group:
    string -> (Depot_xml_types.internal_conf * string) =
fun dom ->
    let i       = ref 1 in
    let conf_id = ref 0 in
    let ()      =
        while i.val < Array.length int_confs
        do {
            let sel = domain_sels.(i.val) in
            if  Pcre.pmatch ~rex: (snd sel) dom
            then
            do {
                (* Found -- break the loop: *)
                conf_id.val := i.val;
                i.val       := Array.length int_confs
            }
            else
                i.val := i.val + 1
        }
    in
    let conf  = int_confs.(conf_id.val) in

    if  conf_id.val = 0
    then
        (* No specific Group found  --  it's a Singleton Group: *)
        (conf, dom)
    else
        (* Specific Group found: return its string regexp as ID:*)
        (conf, fst (domain_sels.(i.val)));


(*--------------------*)
(*  "send_user_msg":  *)
(*--------------------*)
(*  The 1st attempt to send a msg, when it is just taken from the
    Msg Buff Dir:
*)
value send_user_msg:
    Smtp_client.smtp_agent              ->
    Smtp_client.smtp_agent_stats_accum  ->
    Depot_xml_types.user_msg            ->
    unit =

fun agent dummy umsg ->
    (* Determine the Conf, the Domain Group, and re-configure the Agent: *)

    let to_dom      = Misc_utils.get_email_domain umsg.Depot_xml_types.to_email
    in
    let (conf, grp) = find_group  to_dom in
    let ()          = reconfigure_smtp_agent agent conf in

    (* Now try to actually send the msg: *)

    let fmsg = final_user_msg umsg  conf  in
    try
        send_msg agent dummy fmsg conf grp
        (* If we got here: successful send -- nothing more to do! *)
    with
    [
        ReSendable_Error err ->
            (* If re-sending of messages is allowed, put it into the Delayed
               Queues (in both the file system and in memory):
            *)
            if  conf.Depot_xml_types.delivery_attempts > 1
            then
                let dmsg = mk_delayed_msg umsg conf grp fmsg err in
                do {
                    (* Put the file into the Delayed Queue SubDir.  In fact,
                       a separate lock (not the main Msg Buff Dir Lock) can
                       be provided here, but (XXX) we currently don't do it:
                    *)
                    critical_section_msg_buff
                    (fun () ->
                        save_delayed_msg dmsg
                    );
                    (* Put "dmsg" into the in-memory Delayed Queue.    This
                       operations should not fail, but we still protrect it:
                    *)
                    delayed_msgs.val := DMQ.add dmsg delayed_msgs.val
                }
            else
                (*  Re-sending is not allowed: *)
                failed_send (U umsg) conf err

        | hmm ->
            (* Any other error:  not re-sendable:
               Log it and delete the msg:
            *)
            failed_send (U umsg) conf (Misc_utils.print_exn hmm)
    ];


(*-----------------------*)
(*  "send_delayed_msg":  *)
(*-----------------------*)
value send_delayed_msg:
    Smtp_client.smtp_agent              ->
    Smtp_client.smtp_agent_stats_accum  ->
    Depot_xml_types.delayed_msg         ->
    unit                                =

fun  agent dummy dmsg ->
    let conf = dmsg.Depot_xml_types.conf     in
    let grp  = dmsg.Depot_xml_types.group_id in
    try
    do {
        (* Reconfigure the Agent:   *)
        reconfigure_smtp_agent agent conf;

        (* Try to send the message: *)
        send_msg agent dummy dmsg.Depot_xml_types.orig_fmsg conf grp;

        (* If we got here, the send was OK: remove the file: *)
        Unix.unlink    dmsg.Depot_xml_types.dmsg_file
    }
    with
    [
        ReSendable_Error err ->
        do {
            (* Send failed, but we may try again: *)
            dmsg.Depot_xml_types.prev_errors:=
            dmsg.Depot_xml_types.prev_errors @ [err];

            let n_errors = List.length dmsg.Depot_xml_types.prev_errors
            in
            if  n_errors < conf.Depot_xml_types.delivery_attempts
            then
                (* Can still try again. The interval between delivery attempts
                   increases exponentially:
                *)
                let delay = conf.Depot_xml_types.delivery_retry_interval *.
                            (2.0 ** (float_of_int (n_errors - 1)))
                in
                do {
                    (* Put the "dmsg" back into the queue -- it must be locked
                       for this operation (and we protect it from exceptions):
                    *)
                    dmsg.Depot_xml_types.next_send   :=
                    dmsg.Depot_xml_types.next_send +. delay;

                    delayed_msgs.val := DMQ.add dmsg delayed_msgs.val;

                    (* Also, update the stored XML file.  No need to lock the
                       dir for that:
                    *)
                    save_delayed_msg dmsg
                }
            else
                (* No more re-send attempts: absolute failure: *)
                failed_send (D dmsg) conf err
        }

      | hmm ->
        (* Any other (non-resendable) error: absolute failure: *)
        failed_send  (D dmsg) conf (Misc_utils.print_exn hmm)
    ];


(*--------------------*)
(*  "run_delayed_q":  *)
(*--------------------*)
(*  Applies "send_delayed_msg" to all queue entries for which the re-send time
    has come. Note that only the in-memory queue is examined; the resp entries
    in the file system are used ONLY to re-build the in-memory queue (with the
    loss of re-sending times and counts, but that's OK)  if "gatling_depot" is
    to be re-started:
*)
value rec run_delayed_q:
    Smtp_client.smtp_agent             ->
    Smtp_client.smtp_agent_stats_accum ->
    unit                               =

fun agent dummy ->
    (* Examine the head entry in the delayed queue --
       is it time to re-send it?
    *)
    let mb_min_entry =
        if  DMQ.is_empty delayed_msgs.val
        then
            (* Nothing really to do:  *)
            None
        else
            (* Take the leading element: *)
            let min_entry = DMQ.min_elt delayed_msgs.val
            in
            (* Is it time to send it? *)
            if  Unix.gettimeofday () >= min_entry.Depot_xml_types.next_send
            then
            do {
                (* Remove the delayed message from the queue --
                   it may be put back there if send fails:
                *)
                delayed_msgs.val := DMQ.remove min_entry delayed_msgs.val;

                (* Indicate we will send it: *)
                Some min_entry
            }
            else
                (* No, it's not yet time to send it: *)
                None
    in
    match mb_min_entry with
    [ None ->
            ()
    | Some min_entry ->
      do {
            (* Exceptions are trapped by "send_delayed_msg" itself: *)
            send_delayed_msg agent dummy min_entry;

            (* Try other elements: *)
            run_delayed_q agent dummy
    }];

(*--------------------*)
(*  "pick_user_msg":  *)
(*--------------------*)
(*  Picks the first entry of the Msg Buff Dir which has the appropriate suffix
    (if any), and loads a User Msg from it:
*)
value rec pick_user_msg': Unix.dir_handle -> option Depot_xml_types.user_msg =
fun dh ->
try
    let dir_entry = Unix.readdir dh in

    if  ExtString.String.ends_with dir_entry msg_file_sffx
    then
        (* It's a message file!  Try to read it in; we need the full
           file name in order to do that:
        *)
        let file_name = Filename.concat msg_buff_dir  dir_entry  in
        try
            let  omsg = Some (load_user_msg file_name) in
            do {
                (* File successfully loaded -- DELETE IT; if send fails,
                   it will be re-created in the Delayed Q dir:
                *)
                Unix.unlink file_name;
                omsg
            }
        with
        [   hmm ->
            (* This entry was bad  --  remove it, and try more: *)
            do {
                log "SENDER"
                    ("Invalid Msg File, deleting it: \""^file_name^"\": "^
                    (Misc_utils.print_exn hmm)^"\n\n"^
                    (Misc_utils.read_file file_name));

                Unix.unlink    file_name;

                pick_user_msg' dh
            }
        ]
    else
        (* Not a message file -- don't touch it, just try more: *)
        pick_user_msg' dh
with
[   End_of_file ->
        (* No more directory entries: *)
        None
];


value pick_user_msg: unit -> option Depot_xml_types.user_msg =
fun () ->
    do {
        critical_section_msg_buff
        (fun () ->
            let dh = Unix.opendir msg_buff_dir in
            let mb_umsg = pick_user_msg'   dh      in
            do {
                Unix.closedir dh;
                mb_umsg
            }
        )
    };


(*=========================*)
(*  Top-Level Management:  *)
(*=========================*)
(*------------------*)
(*  "sender_loop":  *)
(*------------------*)
(*  XXX: IMPORTANT: Make sure nothing impedes the tail-recursive optimisation
    of this function, otherwise we will run out of stack soon!
*)
value rec sender_loop:
    Smtp_client.smtp_agent             ->
    Smtp_client.smtp_agent_stats_accum ->
    unit                               =

fun agent dummy ->
    (* Re-start requested? -- If so, terminate the thread: *)
    if  will_restart.val
    then
        (* Don't forget to close the Agent before terminating the thread: *)
        Smtp_client.close_smtp_agent agent
    else
    try
    do {
        (* Run the Delayed Queue. It attempts to send ALL delayed messages for
           which the sending time has come:
        *)
        run_delayed_q agent dummy;

        (* Now run the main directory buffer.   At most one "main" message is
           sent per "sender_loop" iteration:
        *)
        match pick_user_msg () with
        [   None      ->
                ()

          | Some umsg ->
                (* Aha! Got it -- try to send it! *)
                send_user_msg agent dummy umsg
        ];

        (* Wait: *)
        Tnio.thread_sleep poll_period;

        (* Tail-recursive call: *)
        sender_loop agent dummy
    }
    with
    [ hmm ->
        do {
            (* Hmm... an uncaught exception propagated  to this level --
               something unexpected happened.  Better shut down (and re-
               start) the whole program, as it may be in an inconsistent
               state:
            *)
            log  "SENDER" ("UNEXPECTED ERROR: "^(Misc_utils.print_exn hmm));

            (* Again, don't forget to close the Agent: *)
            Smtp_client.close_smtp_agent agent;

            (* Tell all other threads that they must re-start gracefully: *)
            will_restart.val := True
        }
    ];

(*---------------------*)
(*  "sender_startup":  *)
(*---------------------*)
(*  Create the SMTP Agent inside the thread: *)

value rec sender_startup: int -> unit =
fun i ->
    let conf_i =
        (* Open the trace file if appropriate: *)
        if  trace_dialogue
        then
            let trace_fd =
                open_write (Printf.sprintf "%04d.tr" i) False "Trace"
            in
            do {
                Unix.set_close_on_exec trace_fd;

                {(smtp_config) with
                               Smtp_client.conf_trace_file=(Some trace_fd)}
            }
        else
            smtp_config
    in
    let agent =
        try Smtp_client.mk_smtp_agent conf_i
        with [any ->
                 Misc_utils.fatal_error
                 ("ERROR: Could not create SMTP Agent: "^
                 Misc_utils.print_exn any)
            ]
    in
    let dummy = Smtp_client.empty_smtp_agent_stats_accum () in

    sender_loop agent dummy;

(*-----------------------------*)
(* Create the Sending Threads: *)
(*-----------------------------*)
(* They never exit. If there is only 1 thread, we do everything sequentially
   in the main process:
*)
value run_threads: unit -> unit =
fun () ->
do {
    (* Load the Delayed Msgs -- this has to be done after the Monitor/Sender
       "fork", as otherwise the originally-loaded Delayed Msgs would persist
       in all Senders forked out in the future:
    *)
    load_delayed_msgs ();

    (* Now really create the threads: *)
    if  n_threads >= 2
    then
    do {
        for i = 0 to (n_threads-1)
        do {
            try
                ignore
                (Tnio.thread_create
                    ~stack_size_k:thread_stack_k ~priority:0
                    (fun () -> sender_startup i)
                )
            with
            [hmm ->
                let msg = Printf.sprintf
                          "Thread %04d creation ERROR: %s\n"
                          i (Misc_utils.print_exn hmm)
                in do{
                    log "SENDER" msg;
                    Misc_utils.fatal_error msg
                }
            ];
            log "SENDER" (Printf.sprintf "Thread %4d created" i);
        };
        (* Wait for all threads to terminate. Normally, they should not
           terminate at all, unless the "restart_after" count  has been
           reached.  If they do terminate, we re-start the application:
        *)
        Tnio.threads_scheduler ()
    }
    else
        sender_startup 0;

    (* If we reach this point, terminate the Sender process (it will be
       terminated anyway, perhaps), so it can be re-started. The return
       code does not really matter:
    *)
    Tnio.exit 2
};

(*--------------------*)
(*  "last_activity":  *)
(*--------------------*)
(*  This call-back is supplied to the Monitor in order to detect deadlocks in
    the Sender. But how often is the lock file updated? --    It is done from
    the "sender_loop", by "pick_user_msg", so it's OK:
*)
value last_activity: unit -> float =
fun () ->
    (Unix.fstat msg_buff_lock_fd).Unix.st_mtime;
    (* Any exceptions are propagated to the caller *)


(*---------------------------------------*)
(*  Enter the Daemon Mode if requested:  *)
(*---------------------------------------*)
value minfo =
{
    Monitor.last_activity     = last_activity;
    Monitor.infinite_proc     = True;
    Monitor.max_blocking_time = max_blocking_time;
    Monitor.log_heartbeats    = False
};

(* NB: here the monitored mode is equivalent to the daemon mode: *)
value rinfo =
{
    Monitor.init_phase        = fun _ -> (fun _ -> ()); (* XXX No clean-up? *)
    Monitor.oper_phase        = run_threads;
    Monitor.daemon_mode       = daemon_mode;
    Monitor.with_monitor      = if daemon_mode then Some minfo else None;
    Monitor.severe_error      = Domains_config.no_restart_rc;
    Monitor.exn_error         = 1;              (* For example    *)
    Monitor.pid_file          = pid_file;
    Monitor.run_as_user       = run_as_user;    (* Already there  *)
    Monitor.run_as_group      = run_as_group;   (* Already there  *)
    Monitor.log               = log "MONITOR"
    (* No Log file locking between the Sender and the Monitor.  This is
       deliberate, to prevent another potential deadlock condtn, at the
       cost of a slight chance of mess-up in the Log file.
    *)
};
Monitor.run_process rinfo;

