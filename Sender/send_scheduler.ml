(*pp camlp4r *)
(* vim:ts=4:syntax=ocaml
*)
(*===========================================================================*)
(*                          "send_scheduler.ml":                             *)
(*                                                                           *)
(*       High-performance scheduler of destination e-mail addresses          *)
(*                for the "gatling_barrage" application                      *)
(*                                                                           *)
(*               (C) Explar Technologies Ltd, 2003--2006                     *)
(*===========================================================================*)
(*=============*)
(* Data Types: *)
(*=============*)
exception Scheduler_Error of string;

(*  "sending_job":
    The data returned by the Scheduler to the Thread from the "get_next_job"
    call:
*)
type sending_job =
{
    sj_ml_rec : array string;   (* Record fields, WITHOUT the extra data *)
    sj_conf_id: int;            (* Index of the resp Group Config        *)
    sj_use_ips: list Unix.inet_addr               (* IPs to use (if any) *)
};

(* "domain_info" for Groups with RegExp selectors  (such Groups typically con-
    tail MANY domains) is the map

    domain_name => (domain_offset, email_count)

    where  "domain_offset" is in bytes from the beginning of the Direct Access
    Mailing List File.   This structure provides efficient membership test and
    per-domain counting of e-mails in the Group.

    Groups with "Str" selectors (Single-Domain Groups) don't require any extra
    info about their domains:
*)
type group_mem_sel = [RegExp_Sel  of Pcre.regexp | Str_Sel of string];

type domain_info   = [RegExp_Info of Hashtbl.t string (int * int) | Str_Info];

(*  "domain_group" is the group under construction or processing. NB: this type
    does not contain GroupID;   the latter is the index of the corresp "domain_
    group" in the "all_groups_arr" array (when applicable):
*)
type domain_group  =
{
    (* The ConfID is the index of this Group in the array of Group Configs,
       which is always constant:
    *)
    conf_id         : int;
    group_name      : string;

    (* RegExp or a single domain name selecting Group members: *)
    members_sel     : group_mem_sel;

    (* Total number of e-mails in this Group: *)
    total_emails    : mutable int;

    (* Offset of this Group from the beginning of the Direct Access Mailing
       List file, in bytes:
    *)
    group_offset    : mutable int;

    (* Domain Info, as above: *)
    domains         : domain_info;

    (* Array of the same domains, sorted in the descending order of e-mail
       counts:
    *)
    domains_sorted  : mutable array (string * int) (* Domain, E-Mail Count *)
};

(*  "domain_info_long":
    to be used in the all-over Domains Dictionary:
*)
type domain_info_long =
{
    (* The Config index for this domain: *)
    dom_conf_id         : int;

    (* The Group this domain belongs to: *)
    dom_group_id        : int;

    (* Offset in bytes from the beginning of the Direct Access File: *)
    dom_offset          : int;

    (* Total E-Mails count for this domain: *)
    dom_total_emails    : mutable int;

    (* The number of e-mails "processed" for this domain; the exact meaning
       of this depends on the operation being performed:
    *)
    dom_done_emails     : mutable int
};


(*  "{groups|domains}_progress":
    For recording and reporting information on send progress:
*)
type domain_progress_stat =
{
    dps_pert_emails: mutable int;
    dps_done_emails: mutable int;
    dps_succ_emails: mutable int
};
type domains_progress_stats = Hashtbl.t string domain_progress_stat;
    (* Key: DomainName *)

type group_progress_stat  =
{
    gps_group_name   : string;
    gps_pert_emails  : mutable int;
    gps_done_emails  : mutable int;
    gps_succ_emails  : mutable int;
    gps_domains_stats: domains_progress_stats
};
type groups_progress_stats  = Hashtbl.t int group_progress_stat;
    (* Key: GroupID *)


(*==================================*)
(*  Global State of the Scheduler:  *)
(*==================================*)
(*  Constructed by the "init_from_*" functions given below. Updated by the
    "get_next_job" etc call-backs from the working threads. Keeps track of
    how many threads are working with each Domain Group.
*)
(*  Descr of the Direct Access File: *)
value dir_file_name = ref "";
value dir_fd        = ref (Tnio.invalid_file_descr);

(*  "abs_threads_load" is a map GroupID => CurrThreadsNumber.  It is used
    for those Groups which require Absolute load limits.
    NB:
    --  GroupID here is the ID of each individual Group, including Single-
        Domain ones; it's NOT the same as the ConfID!  The GroupID is kept
        in each record, and is ONLY used in this data structure to  deter-
        mine the current load on that Group.
    --  The Hash table is mutable by itself, hence no "ref" here.
*)
value abs_threads_load: Hashtbl.t int (ref int)
                      = Hashtbl.create 100000;

(*  "dip_threads_load": a map
    Domain-IP => (CurrThreadsNumber, MaxLoadSoft, MaxLoadHard)
    (just "IP" as the key would not do,  as some IPs can be shared
    between multiple domains).
    Used for those Groups which require Per-Server-IP load limits:
*)
value dip_threads_load: Hashtbl.t string ((ref int) * int * int) =
                        Hashtbl.create 500000;

(*  DNS Cache: used together with "dip_threads_load" :
    map EMail_Domain => Server_IPs:
*)
value dns_cache       : Hashtbl.t string (list Unix.inet_addr) =
                        Hashtbl.create 100000;

(*  Is the DNS Cache actually used? *)
value do_dns_cache    : ref bool =
                        ref False;

(*  "first_free":
    The offset of the first (N) record, used to guide fetching of records:
*)
value first_free  = ref 0;

(*  Total Direct Access File Size:  *)
value total_dir_acc_size = ref 0;

(*  "threads_jobs": a map
    ThreadID => CurrSendingJob
    (threads without the current job are removed from the map):
*)
value threads_jobs : Hashtbl.t int  sending_job
                   = Hashtbl.create 1000;

(*  Progress Info:   *)
value progress_info: groups_progress_stats = Hashtbl.create 100000;

(*  DB-related data: *)
value db_handle    = ref None;

(*  The following data DO NOT REALLY NEED to be global, but we keep them
    here just for convenience:
*)

(*  Format of the record in the Direct Access File. Includes the original
    fields plus the Extra Field (see below). The format is
    [(Offset, Length)] where Offset is the field offset is bytes from the
    beginning of the record.  Fields of length less than the maximum will
    be padded:
*)
value all_fields:   ref (list (int*int)) = ref [];

(*  Indices of the "EMail" and "Crypt" fields: always constant: *)
value email_pos   = Gatling_db.email_fld_pos;
value crypt_pos   = Gatling_db.crypt_fld_pos;

(* Fields separator: also constant: *)
value fields_sep  = String.make 1 Gatling_db.flds_sep;

(*  Total record length and the total number of fields in it (they can be
    derived from "all_fields", but kept here for simplicity):
*)
value dir_rec_len = ref 0;
value n_fields    = ref 0;

(*  Total length of the preamble in the Direct Access File:   *)
value preamble_len= ref 0;

(*  All Internal Confs: *)
value confs_arr   : ref (array Barrage_config.internal_conf)
                  = ref [| |];

(*  The Dictionary and the Array of all Domains: VERY USEFUL: *)
value domains_dict: Hashtbl.t string domain_info_long =
                    Hashtbl.create 100000;

value domains_arr : ref (array string)   =
                    ref [| |];

(*  The Logging Channel: *)
value log_ch      : ref (option out_channel) =
                    ref None;

(*=======================================*)
(*  Groups Infrastructure Construction:  *)
(*=======================================*)
(*  All functions in this section are initialisation ones, so they MUST be
    called from a single-threaded environment:
*)
(*----------------------------------*)
(*  Direct Access Records Support:  *)
(*----------------------------------*)
(*  The Extra Data format:   these data are to be added as an extra field to
    the Mailing List Records when they are stored in a Direct Access File:
    (GroupID, ConfigID, NextGroupOff, Status),
    where "Status" is:
    N -- Not (yet) processed,  G -- in proGress,
    S -- Success,              R -- failure, but can Retry,
    F -- Failure, don't retry, P -- Permanent failure (remove that addr),
    T -- permanent failure in the strictT 5XX mode,
    and "GroupID" is the index in the array of Group Configs of the Group to
    which this record belongs.

    NB:  the "in" and "out" formats are almost the same (up to the isomorphism
    %c <=> %1s at the end):
*)
type  proc_status = [N | G | S | R | F | P | T];

value extra_data_format_in    = format_of_string "%04x%03x%08x%c" ;
value extra_data_format_out   = format_of_string "%04x%03x%08x%1s";
value extra_data_len          = 16;

(*  Preamble  for the Direct Access File:
    (N_Fields, [(FieldOffset, FieldLen)], RoundNo)
*)
value preamble_int_format_in  = format_of_string "%03x";
value preamble_int_format_out = format_of_string "%03x";
value preamble_int_len        = 3;


(*--------------------*)
(*  "write_dir_acc":  *)
(*  "read_dir_acc" :  *)
(*--------------------*)
(*  The reading/writing functions for the Direct Access File: *)

value pad_char = '\000';


value write_dir_acc: ~offset:int -> string -> unit =
fun ~offset data ->
try
do {
    if  offset >= 0
    then
        (* Real offset, do positioning: *)
        ignore (Unix.lseek dir_fd.val offset Unix.SEEK_SET)
    else
        (* Ignore the offset: will read sequentially: *)
        ();

    let data_len = String.length data in

    if  Unix.write dir_fd.val data 0 data_len <> data_len
    then
        failwith "Short write"
    else
        ()
}
with
[ hmm ->
    raise (Scheduler_Error
          ("Direct Access write failed: "^(Misc_utils.print_exn hmm)))
];


value read_dir_acc: ~offset:int -> int -> string =
fun ~offset len ->
do {
    if  offset >= 0
    then
        (* Real offset, do positioning: *)
        ignore (Unix.lseek dir_fd.val offset Unix.SEEK_SET)
    else
        (* Ignore the offset: will write sequentially: *)
        ();

    let data = String.make len pad_char in
    let got  = Unix.read dir_fd.val data 0 len in

    if  got  = 0 && len <> 0
    then
        (* Typically, this means end of file: *)
        raise End_of_file
    else
    if  got <> len
    then
        (* Incomplete read -- system error, or hit end of file
           prematurely:
        *)
        failwith "Short read"
    else
        (* OK: *)
        data
};

(*--------------------*)
(* "status_of_char":  *)
(*     "get_status":  *)
(*     "str_status":  *)
(*  "update_status":  *)
(*--------------------*)
(*  Whatever format of the Extra Data is used, the status byte is the last in
    the record. We convert it into the "proc_status" type:
*)
value status_of_char: char -> proc_status =
fun c ->
    match c with
    [ 'N'-> N | 'G'-> G | 'S'-> S | 'R'-> R | 'F'-> F | 'P'-> P | 'T'-> T
    | _  -> raise (Scheduler_Error
                  (Printf.sprintf "Invalid Status Byte: %c" c))
    ];

value get_status:     int  -> proc_status =
fun rec_off ->
    let c = (read_dir_acc ~offset:(rec_off + dir_rec_len.val - 1) 1).[0]
    in  status_of_char c;


(*  Other way round, convert a "proc_status" value into a 1-char string to be
    written into the DirAcc file:
*)
value str_status: proc_status -> string =
fun status ->
    match status with
        [ N-> "N" | G-> "G" | S-> "S" | R-> "R" | F-> "F" | P-> "P" | T-> "T"];

value update_status: int -> proc_status -> unit =
fun rec_off  status ->
    write_dir_acc ~offset:(rec_off + dir_rec_len.val - 1) (str_status status);


(*--------------------*)
(*  "pack_dir_rec" :  *)
(*  "parse_dir_rec":  *)
(*--------------------*)
(*  Packing/parsing of Direct Access Records: *)

type dir_rec =
{
    rec_fields        : array string;            (* ORIGINAL Fields  *)
    rec_group_id      : int;    (* The Group this record belongs to  *)
    rec_conf_id       : int;    (* The Config index for this record  *)
    rec_next_group_off: int;    (* Absolute offset of the NEXT group *)
    rec_status        : proc_status
};

value pack_dir_rec  : dir_rec -> string =
fun dr ->
    (* NB: the Status must ALWAYS be the last field -- this condition is
       used in many other places throughout this module:
    *)
    let extra_fld =
        Printf.sprintf  extra_data_format_out
                        dr.rec_group_id
                        dr.rec_conf_id
                        dr.rec_next_group_off
                        (str_status dr.rec_status)
    in
    let all_flds =  (Array.to_list dr.rec_fields) @ [extra_fld]  in
    do {
        assert (List.length all_flds = n_fields.val);

        let padded_flds =
            List.map
            (fun (f, (fld_off, fld_len)) ->
            do {
                assert (String.length f <= fld_len);

                if  String.length f < fld_len
                then
                    (* Pad the field: *)
                    f ^ (String.make (fld_len - String.length f) pad_char)
                else
                    f
            })  (List.combine all_flds all_fields.val)
        in
        (* NB: "\n" installed IN FRONT of each record, to facilitate viewing
           of the Direct Access File via a text editor:
        *)
        let out_rec = "\n"^(String.concat "" padded_flds) in
        do {
            assert (String.length out_rec = dir_rec_len.val);
            out_rec
        }
    };


value parse_dir_rec : string  -> dir_rec =
fun s ->
try
do {
    assert (String.length s = dir_rec_len.val);

    let (orig_flds, extra_fld) =
        List.fold_left
        (fun (curr_flds, last_fld) (off, len) ->
            (* The (possibly padded) field: *)
            let f  = String.sub s off len in

            (* Remove the trailing padss, if any: *)
            let act_len = ref len in
            let () =
                while act_len.val > 0 && f.[act_len.val-1] = pad_char
                do {
                    act_len.val := act_len.val - 1
                }
            in
            (* The stripped field: *)
            let g  = String.sub f 0 act_len.val in

            (* Is it the last one? *)
            if  off+len = dir_rec_len.val
            then
                (curr_flds, g)
            else
                (curr_flds @ [g], "")
        )
        ([], "")  all_fields.val
    in
    do {
        (* There must be at least TWO fields: an original one, and
           the extra one:
        *)
        assert ((orig_flds <> []) &&
                (List.length orig_flds   = (n_fields.val - 1)) &&
                (String.length extra_fld = extra_data_len));

        (* Now parse the extra field: *)

        let (group_id, conf_id, next_group_off, status)  =
            Scanf.sscanf extra_fld extra_data_format_in
                         (fun g c n s -> (g, c, n, s))
        in
        (* The resulting record: *)
        {
            rec_fields         = Array.of_list orig_flds;
            rec_group_id       = group_id;
            rec_conf_id        = conf_id;
            rec_next_group_off = next_group_off;
            rec_status         = status_of_char status
        }
    }
}
with
[ _ ->
    raise (Scheduler_Error ("Invalid record: "^(String.escaped s)))
];

(*----------------------------*)
(*  "find_and_update_group":  *)
(*----------------------------*)
(*  Tries to determine to which Group a given "dom" belongs. If such a Group
    is found, its details are updated.  Returns the Bool flag of find/update
    success.  NB: the defaults pseudo-group is of course NOT searched!  This
    is a helper function for "init_from_text_file":
*)
value rec search_regexp_groups:
          list domain_group -> string -> option domain_group =

fun re_groups dom ->
    match re_groups with
    [   [] ->
            (* No more RegExp Groups: *)
            None

      | [hgr :: tgrs] ->
            match hgr.members_sel with
            [   RegExp_Sel re ->
                    if Pcre.pmatch ~rex:re dom
                    then (* Selector found! *)
                        Some hgr

                    else (* Try others...   *)
                        search_regexp_groups tgrs dom

              | Str_Sel s ->
                    (* This must not happen: it's for RegExp Groups only: *)
                    failwith ("Not a RegExp Group: \""^s^"\"")
            ]
    ];

value find_and_update_group:
    list domain_group -> Hashtbl.t string domain_group -> string -> bool =

fun re_groups str_groups dom ->
    (* First, check whether "dom" is matched by the RegExp in any of the
       "re_groups":
    *)
    match search_regexp_groups re_groups dom with
    [   None ->
            (* "dom" not found in RegExp Groups; try Single-Domain Groups: *)
            try
                let sgr = Hashtbl.find str_groups dom in
                do {
                    (* Yes, it's there;  update the "total_emails" for the
                       whole Group   (same as e-mails count for the single
                       domain in that Group):
                    *)
                    sgr.total_emails := sgr.total_emails + 1;

                    (* Data consistency check: *)

                    assert (sgr.members_sel = Str_Sel dom);

                    match sgr.domains  with
                    [   Str_Info      -> ()
                      | RegExp_Info _ -> assert False
                    ];

                    (* "dom" had been found: *)
                    True
                }
            with
            [   Not_found ->
                    (* "dom" not found in "str_groups" either: *)
                    False
            ]

      | Some regr ->
        do {
            (* A RegExp Group yielded the result. Is the "dom" already listed
               in this Group? NB: the domain offsets are  0 (not yet defined)
               below.
               Before all, update the "total_emails" count for the whole Group:
            *)
            regr.total_emails := regr.total_emails + 1;

            match regr.domains with
            [   RegExp_Info rei ->
                try
                    let (_, email_count) =  Hashtbl.find rei dom in
                    (* Yes, it's already there: update "email_count": *)
                    Hashtbl.replace rei dom (0, email_count+1)
                with
                [   Not_found ->
                    (* The "dom" is not listed there yet; do it now:  *)
                    Hashtbl.add  rei dom (0, 1)
                ]

              | Str_Info -> assert False
            ];
            (* Return the "Found" flag: *)
            True
        }
    ];


(*-----------------------*)
(*  "mk_regexp_groups":  *)
(*-----------------------*)
(*  Creates a list of initial "domain_group"s from a "domain_group_conf" list:
*)
value rec mk_regexp_groups':
    list Barrage_config.domain_group_conf -> int ->
    list domain_group -> list domain_group =

fun gr_confs i curr ->
    match gr_confs with
    [ [] ->
        (* All done: *)
        curr

    | [hgrc :: tgrcs] ->
        let ()   = assert (i >= 1 && i < Array.length confs_arr.val)  in

        (*  Convert the "reg_exps" into the PCRE format, and OR them: *)
        let one_re = Domains_config.compile_domain_selectors
                     hgrc.Barrage_config.selector_regexps
        in
        let conf = confs_arr.val.(i) in
        let ()   = assert (hgrc.Barrage_config.group_name =
                           conf.Barrage_config.from_group_name) in
        let res  =
        {
            (* "conf_id" starts from 1, as 0 is reserved  for the Default
               Conf. NB: here we use the NON-TRIVIAL group names, as they
               appear in "Barrage_config":
            *)
            conf_id         = i;
            group_name      = hgrc.Barrage_config.group_name;
            members_sel     = RegExp_Sel (snd one_re);
            total_emails    = 0;
            group_offset    = 0;
            domains         = RegExp_Info (Hashtbl.create 100000);
            (* A group with RegExp-given selector is likely to be LARGE: *)

            domains_sorted  = [| |]  (* Not yet *)
        }
        in
        (* Go further: *)
        mk_regexp_groups' tgrcs (i+1) (curr @ [res])
    ];

value mk_regexp_groups:
    list Barrage_config.domain_group_conf -> list domain_group =
fun gr_confs ->
    mk_regexp_groups' gr_confs 1 [];


(*------------------------*)
(*  "init_sort_domains":  *)
(*------------------------*)
(*  Sorts the Groups and their Domains   in the descending order   of e-mail
    counts. Also computes offsets of Groups and domains in the Direct Access
    File:
*)
value init_sort_domains: array domain_group -> unit =
fun all_groups_arr ->
do {
    Array.sort
        (fun gr1 gr2 -> compare gr2.total_emails gr1.total_emails)
        (* NB: inverse order of comparison! *)
        all_groups_arr;

    (* Determine the offsets for all Groups, in bytes from the beginning of
       the Direct Access File. For RegExp (multi-domain) Groups,  also sort
       the domains in the descending order of e-mail counts,  and determine
       the offset of each domain:
    *)
    ignore (Array.fold_left
    (fun curr_off gr ->
    do {
        gr.group_offset := curr_off;
        let group_len    = gr.total_emails * dir_rec_len.val  in

        match gr.domains with
        [   Str_Info ->
                (* Single-Domain Group: STILL install its single domain!  *)
                gr.domains_sorted := [| (gr.group_name, gr.total_emails) |]

          | RegExp_Info rei ->
                (* Make a list and an array of all domains here: *)
                let domains_list = Hashtbl.fold
                    (fun  dom  (_, email_count) curr_list ->
                        [(dom,  email_count) :: curr_list])
                    rei []
                in
                let domains_arr  = Array.of_list domains_list in

                let ()= Array.sort
                        (fun (_, count1) (_, count2) ->
                            (* Again, inverse order of comparison: *)
                            compare count2 count1
                        )
                        domains_arr
                in
                (* Compute the absolute domain offsets: they start
                   with the Group's offset:
                *)
                let final_off = Array.fold_left
                    (fun this_off (dom, email_count) ->
                    do {
                        Hashtbl.replace rei dom (this_off, email_count);

                        (* The value to return to "fold_left": *)
                        this_off + email_count * dir_rec_len.val
                    })
                    gr.group_offset domains_arr
                in
                (* The final offset on all domains in the Group must
                   be the same as the offset past the whole Group:
                *)
                let () = assert  (final_off = curr_off + group_len)  in

                (* Install the sorted list of domains in the Group: *)
                gr.domains_sorted := domains_arr
        ];
        (* The value to return to "fold_left": *)
        curr_off + group_len
    })
    preamble_len.val all_groups_arr)
    (* NB: the initial offset is "preamble_len", not 0! *)
};


(*-----------------------*)
(*  "print_init_stats":  *)
(*-----------------------*)
(*  Prints e-mail distribution statistics on Groups and Domains.  The last arg
    is the domain size threshold: domains with less e-mails than that fraction
    of the total number, will not be printed:
*)
value print_init_stats:
    array domain_group -> out_channel -> int -> float -> unit =

fun all_groups_arr outch total_emails thresh ->

    let int_thresh = int_of_float ((float_of_int total_emails) *. thresh) in
    do {
        Printf.fprintf outch
            "\n\nSCHEDULER: STATISTICS FOR \"OVER-%f%%\" DOMAINS:\n\n"
            (thresh *. 100.0);

        Array.iter
        (fun gr ->
            match gr.domains with
            [   Str_Info ->
                if  gr.total_emails > int_thresh
                then
                    (* Single domain: *)
                    let dom = match gr.members_sel with
                    [   Str_Sel d    -> d
                      | RegExp_Sel _ -> assert False
                    ]
                    in
                    Printf.fprintf outch
                        "Singleton Group: %s ==> %d\n"
                        dom gr.total_emails
                else ()

              | RegExp_Info rei ->
                do {
                    (* Multi-domain Group: *)

                    Printf.fprintf outch
                        "\nMulti-Domain Group \"%s\": Total of %d e-mails\n"
                        gr.group_name
                        gr.total_emails;

                    Array.iter
                        (fun (dom, email_count) ->
                            if  email_count > int_thresh
                            then
                                Printf.fprintf outch
                                    "\t%s ==> %d\n" dom email_count
                            else ()
                        )
                        gr.domains_sorted;

                    Printf.fprintf outch "\n"
                }
            ]
        )   all_groups_arr;

        Printf.fprintf outch "\n\n";
        flush outch
    };


(*-------------------*)
(*  "mk_doms_dict":  *)
(*-------------------*)
(*  Constructs the Dictionary of all domains, to be used in Pass 2 of "init_
    from_text_file", so that we would not need to test Group membership for
    every domain once again:
        domain => domain_info_long
    The number of domains  is approximately  the same as that of Groups, as
    most Groups are single-domain ones.
    For convenienece, this function also constructs an array of all domains:

    The result comes into global vars "domains_dict" and "domains_arr".
*)
value mk_doms_arr: unit -> unit =
fun () ->
    let domains_list = Misc_utils.hashtbl_keys domains_dict
    in
    domains_arr.val := Array.of_list domains_list;


value mk_doms_dict_arr: array domain_group -> unit =
fun all_groups_arr ->
do {
    (* First, fill in the global "domains_dict": *)
    Array.iteri
    (fun grid gr ->
        match gr.domains with

        [   RegExp_Info rei ->
                (* The domains information is in "rei": *)
                Hashtbl.iter
                    (fun dom_name (offset, emails_count) ->
                        let long_info =
                        {
                            dom_conf_id     = gr.conf_id;
                            dom_group_id    = grid;
                            dom_offset      = offset;
                            dom_total_emails= emails_count;
                            dom_done_emails = 0
                        }
                        in
                        do {
                            (* By construction,   each domain belongs only
                               to one group, so it couldn't be encountered
                               before:
                            *)
                            assert
                            (not (Hashtbl.mem domains_dict dom_name));

                            Hashtbl.add domains_dict dom_name long_info
                        }
                    ) rei

          | Str_Info ->
                (* This is a single-domain Group: *)
                let dom_name  = match gr.members_sel with
                [   Str_Sel dom' -> dom'
                  | RegExp_Sel _ -> assert False
                ]
                in
                let long_info =
                {
                    dom_conf_id     = gr.conf_id;
                    dom_group_id    = grid;
                    dom_offset      = gr.group_offset;
                    dom_total_emails= gr.total_emails;
                    dom_done_emails = 0
                }
                in
                do {
                    (* Again, this domain should not be seen before: *)
                    assert (not (Hashtbl.mem domains_dict dom_name));
                    Hashtbl.add domains_dict dom_name long_info
                }
        ]
        ) all_groups_arr;

    (* Now, convert "domains_dict" into a list and then array: *)
    mk_doms_arr ()
};

(*----------------------*)
(*  "wrap_exceptions":  *)
(*----------------------*)
(* Convert any exception into a "Scheduler_Error": *)

value wrap_exceptions: (unit -> 'a) -> 'a =
fun f ->
    try f ()
    with
    [   Scheduler_Error _ as hmm ->
            raise hmm
      | any_other ->
            raise (Scheduler_Error (Misc_utils.print_exn any_other))
    ];


(*=========================================*)
(*  Initialisation / Finalisation Proper:  *)
(*=========================================*)
(*----------*)
(*  "log":  *)
(*----------*)
value log: string -> unit =
fun msg ->
    match log_ch.val with
    [ None    ->
        ()
    | Some ch ->
      do {
        output_string ch ("Send_scheduler: "^msg^"\n");
        flush ch
      }
    ];

(*---------------------------*)
(*  "check_initialisation":  *)
(*---------------------------*)
value check_initialisation: unit -> unit =
fun () ->
try
    assert (dir_fd.val       <> Tnio.invalid_file_descr &&
            all_fields.val   <> [] &&
            dir_rec_len.val  >  0  &&
            preamble_len.val >  0  &&
            n_fields.val     >= 2  &&
            first_free.val   >= preamble_len.val        &&
            Array.length confs_arr.val >= 1             &&
            total_dir_acc_size.val     > preamble_len.val)
            (*
                We deliberately don't check "domains_dict", "domains_arr.val"
                and "dns_cache" -- they MAY be empty is degenerate cases!
            *)
with
[hmm ->
    raise (Scheduler_Error "Scheduler Not Initialised")
];

(*----------------------*)
(*  "close_scheduler":  *)
(*----------------------*)
(*  Clears all global data structures and closes the Direct Access File: *)

value close_scheduler: unit -> unit =
fun () ->
try
do {
    (* We close and delete the "dir_acc_file" on successful completion:  *)
    Unix.close dir_fd.val;
    dir_fd.val        := Tnio.invalid_file_descr;
    Unix.unlink dir_file_name.val;
    dir_file_name.val := "";

    all_fields.val    := [];
    dir_rec_len.val   := 0;
    n_fields.val      := 0;
    preamble_len.val  := 0;
    first_free.val    := 0;
    confs_arr.val     := [| |];
    total_dir_acc_size.val  := 0;
    Hashtbl.clear      abs_threads_load;
    Hashtbl.clear      dip_threads_load;
    Hashtbl.clear      threads_jobs;
    Hashtbl.clear      domains_dict;
    domains_arr.val   := [| |]
}
with [ _ -> ()];


(*----------------------*)
(*  "get_round_no":     *)
(*  "update_round_no":  *)
(*----------------------*)
(*  The Round Number (managed by the caller, not by the Scheduler itselfs is
    STORED AT THE END OF THE PREAMBLE,  so if the program crashes and is re-
    started, we don't start again from Round 1:
*)
value get_round_no: unit -> int =
fun () ->
do {
    check_initialisation ();

    let s = read_dir_acc
            ~offset:(preamble_len.val - preamble_int_len) preamble_int_len
    in
    Scanf.sscanf s preamble_int_format_in (fun i -> i)
};

value update_round_no: int -> unit =
fun round_no ->
do {
    check_initialisation ();

    let s = Printf.sprintf preamble_int_format_out round_no    in
    write_dir_acc ~offset:(preamble_len.val - preamble_int_len) s
};


(*--------------------------*)
(*  "init_from_text_file":  *)
(*--------------------------*)
(*  The "ml_file" contains Mailing List Records as lines of fields separated
    by the "fields_sep" char. The field No."email_pos" is the email field
    from which domain names are taken.  All domains not listed explicitly in
    the "conf" are considered to be members of their own Group, with the
    corresp default config.
    The side-effect is that a direct-access file containing the Mailing List
    sorted by Domain Groups and Domains  is constructed  (as "direct_file"),
    to facilitate future scheduling of Mailing List distribution:
*)
value init_from_text_file:
    string                                ->
    list Barrage_config.domain_group_conf ->
    float                                 ->
    unit  =

fun mlist_file group_confs thresh ->

    (*------------------*)
    (*  Before Pass 1:  *)
    (*------------------*)
    (* Open the Mailing List file: *)
    let  inch =
    try  open_in mlist_file
    with [hmm -> raise (Scheduler_Error ("Cannot open Mailing List File \""^
                       mlist_file^"\": "^(Misc_utils.print_exn hmm)))]
    in
    (* "get_next_record":
       Produces a list of fields, and extracts the domain name from one of
       them:
    *)
    let get_next_record: unit -> (string * (list string) * int * string) =
    fun () ->
        let line = input_line inch  in
        let flds = ExtString.String.nsplit line fields_sep     in
        let nf   = List.length flds in

        (* Now, determine the Domain Group this record belongs to: *) 
        let email = String.lowercase (List.nth flds email_pos) in
        let dom   = Misc_utils.get_email_domain email          in

        (line, flds, nf, dom)
    in
    (*---------*)
    (* PASS 1: *)
    (*---------*)
    (* Determine the number of fields, the length of each field, the number
       and composition of Domain Groups. Two sets of Groups are maintained:
       one with a RegExp-selectors ("re_groups", a list), and another is an
       external Hash table of Single-Domained Groups ("str_groups"):
    *)
    (* "str_groups" is a map domain_name => domain_group: *)
    let str_groups   = Hashtbl.create 100000 in

    (* "re_groups"  are compiled from the "group_confs":  *)
    let re_groups    = mk_regexp_groups group_confs
    in

    let rec pass1: list int -> int -> ((list int) * int) =
    fun curr_fld_lens total_emails ->

        (* Get and parse the next line: *)
        let next4 = try  Some (get_next_record ())
                    with [End_of_file -> None]
        in
        match next4 with
        [   None ->
            (* All done: Return the accumulated results: *)
            (curr_fld_lens, total_emails)

          | Some (line, flds, nf, dom) ->

            (* Do the actual processing.
               Calculate the max field lengths:
            *)
            if  nf <> n_fields.val - 1
            then
                raise (Scheduler_Error
                      ("Invalid number of Fields in: \""^line^"\""))
            else
            let new_lens = List.map String.length flds  in
            let max_lens = 
                if  curr_fld_lens = []
                then new_lens
                else List.map (fun (o,n) -> max o n)
                          (List.combine curr_fld_lens new_lens)
            in
            (* NB: "find_and_update_group" modifies the Group if it was
               found:
            *)
            do {
                if  find_and_update_group re_groups str_groups dom
                then
                    (* The Group has been found and updated: all done here: *)
                    ()
                else
                    (* Group not found -- create a new Single-Domain Group,
                       just for this "dom":
                    *)
                    let new_gr =
                    {
                        conf_id         = 0;      (* Default *)
                        group_name      = dom;    (* Because Singleton! *)
                        members_sel     = Str_Sel dom;
                        total_emails    = 1;      (* Got one e-mail already *)
                        group_offset    = 0;      (* Not yet known *)
                        domains         = Str_Info;
                        domains_sorted  = [| |]   (* Not yet *)
                    }
                    in
                    (* Insert it into "str_groups": *)
                    Hashtbl.add str_groups dom new_gr;

                (* Recursively process other lines: *)
                pass1 max_lens (total_emails+1)
            }
        ]
    in
    (* End "pass1" definition. SO: DO "pass1": *)

    let (orig_max_lens, total_emails) = pass1 [] 0
    in
    (*-----------------*)
    (*  After Pass 1:  *)
    (*-----------------*)
    (* Empty Mailing Lists are NOT allowed: *)
    let ()= if  total_emails = 0
            then raise (Scheduler_Error "Empty Mailing List")
            else ()
    in
    (* Compose the record structure of the Direct Access File.  Each record
       consists of the original Mailing List fields, of the corresp maximum
       length. Field descriptors are pairs (Offset,Length), where Offset is
       in bytes from the beginning of the record.
       NB: offsets are from the beginning of the record, so they start at 1
       to account for "\n" in front!
    *)
    let (orig_len, orig_flds) =
        List.fold_left
        (fun (curr_off,  curr_lst) curr_len ->
             (curr_off + curr_len, curr_lst @ [(curr_off, curr_len)])
        )
        (1, []) orig_max_lens
    in

    (* Add the Extra Data field: *)
    let () = all_fields.val  := orig_flds @ [(orig_len, extra_data_len)] in

    (* Record length in the Direct Access File: orig_len + extra; "orig_len"
       ALREADY includes the "\n" in front of the record!
    *)
    let () = dir_rec_len.val := orig_len + extra_data_len  in

    (* Sort the Domain Groups obtained in the descending order of the total
       number of e-mails in them. XXX: why this ordering? -- No strict need
       for that, but it will mean that large Groups are treated first.
       So: fisrt merge "re_groups" and "str_groups" into a single list,
       then convert it into an array:
    *)
    let all_groups_list = Hashtbl.fold
        (fun _ sgr curr_list -> [sgr :: curr_list])
        str_groups re_groups
    in
    let all_groups_arr  = Array.of_list all_groups_list in

    (* Sort the Groups and domains inside Groups: *)
    let () = init_sort_domains all_groups_arr in

    (* Output the statistics (if the output channel is igiven): *)
    let () = match log_ch.val with
    [   Some outch ->
            print_init_stats all_groups_arr outch total_emails thresh
      | None ->
            ()
    ]
    in
    (* Initialise "progress_info",   once we got composition of all Groups.
       Groups with less than the threshold number of e-mails in them,  are
       not even included in the hash table:
    *)
    let int_thresh = int_of_float (thresh *. (float_of_int total_emails))
    in
    let () = Array.iteri
        (fun grid gr ->
            (* Do we include this Group? *)
            if  gr.total_emails > int_thresh
            then
                (* Yes:
                   Process all domains of this Group:
                *)
                let dstats = Hashtbl.create 100 in
                let ()     = Array.iter
                    (fun (dname, ecount) ->
                        Hashtbl.add dstats dname
                        {
                            dps_pert_emails = ecount;
                            dps_done_emails = 0;
                            dps_succ_emails = 0
                        }
                    )
                    gr.domains_sorted
                in
                (* Attach the Group itself: *)
                Hashtbl.add progress_info grid
                {
                    gps_group_name    = gr.group_name;
                    gps_pert_emails   = gr.total_emails;
                    gps_done_emails   = 0;
                    gps_succ_emails   = 0;
                    gps_domains_stats = dstats
                }
            else
                (* Small Group: skip it.
                   XXX: there will be no more Groups to include, as they are
                   sorted in the descending order of e-mail counts,  but for
                   now, there is no way to bail out of "Array.iteri":
                *)
                ()
        )
        all_groups_arr
    in
    (* Create the Direct Access File, of byte size
       (preamble_len + dir_rec_len * total_emails),
       and zero it out:
    *)
    let preamble0 = String.make preamble_len.val pad_char in
    let rec0      = String.make dir_rec_len.val  pad_char in

    let ()= total_dir_acc_size.val :=
            preamble_len.val  + dir_rec_len.val * total_emails in
    let ()=
    try
    do {
        (* Do NOT truncate the file before it is locked -- the
           locking function will do this:
        *)
        dir_fd.val  :=
            Option.get
            (Misc_utils.open_write_locked
                ~append_mode :False
                ~lock_fail_rc:Domains_config.no_restart_rc
                dir_file_name.val
            );

        write_dir_acc ~offset:0 preamble0;

        for i = 0 to (total_emails - 1)
        do {
            (* Write sequentially: *)
            write_dir_acc ~offset:(-1) rec0
        }
    }
    with
    [ hmm ->
        raise (Scheduler_Error
              ("Cannot create the Direct Access File \""^
              dir_file_name.val ^"\": "^(Misc_utils.print_exn hmm)))
    ]
    in
    (* Construct a Dictionary of all domains, to make Pass 2 more efficient: *)
    let ()= mk_doms_dict_arr all_groups_arr in

    (* Rewind the input stream back to the beginning: *)
    let ()= seek_in inch 0 in

    (*---------*)
    (* PASS 2: *)
    (*---------*)
    (* Go though the Mailing List File again, pick records and put them into
       the resp slots in the Direct Access File:
    *)
    let rec pass2: unit -> unit =
    fun () ->
        (* Get the record: *)
        let next4 = try  Some (get_next_record ())
                    with [End_of_file -> None]
        in
        match next4 with
        [   None ->
            (* All done: *)
            ()

          | Some (_, flds, _, dom) ->
            (* Do the actual processing.
               Determine the domain info for this record:
            *)
            let dom_info =
                try  Hashtbl.find domains_dict dom
                with [Not_found ->
                     raise (Scheduler_Error ("Missed domain: \""^dom^"\""))]
            in
            (* There MUST still be a slot where to put this record to: *)
            if dom_info.dom_done_emails >= dom_info.dom_total_emails
            then
                raise (Scheduler_Error ("No more slots for \""^dom^"\""))
            else

            (* Make the output record: *)
            let grid = dom_info.dom_group_id in

            (* Offset of the next Group: make it the END OF FILE if there is
               no next Group:
            *)
            let ngro = if  grid = Array.length all_groups_arr - 1
                       then
                            total_dir_acc_size.val
                       else
                            all_groups_arr.(grid+1).group_offset
            in
            let rrec =
            {
                rec_fields          = Array.of_list flds;
                rec_group_id        = dom_info.dom_group_id;
                rec_conf_id         = dom_info.dom_conf_id;
                rec_next_group_off  = ngro;
                rec_status          = N   (* As yet *)
            }
            in
            let out_rec = pack_dir_rec rrec in
            do {
            (* Now write the "out_rec" into the appropriate position: *)
                let out_off = dom_info.dom_offset  +
                              dom_info.dom_done_emails * dir_rec_len.val
                in
                write_dir_acc ~offset:out_off out_rec;

                (* Increment the "dom_done_emails" counter: *)
                dom_info.dom_done_emails := dom_info.dom_done_emails + 1;

                (* Next record: *)
                pass2 ()
            }
        ]

    (* "pass2" definition end. SO: DO "pass2": *)
    in
    let ()= pass2 ()  in

    (*---------------*)
    (* After Pass 2: *)
    (*---------------*)
    (* Put the preamble into the Direct Access File: *)
    (* Number of Fields: *)
    let ()= write_dir_acc
            ~offset:0 (Printf.sprintf preamble_int_format_out n_fields.val)
    in
    (* [(FieldOffset, FieldLen)]: write them sequentially: *)
    let ()= List.iter
            (fun (off, len) ->
            do {
                write_dir_acc   ~offset:(-1)
                                (Printf.sprintf preamble_int_format_out off);

                write_dir_acc   ~offset:(-1)
                                (Printf.sprintf preamble_int_format_out len)
            })
            all_fields.val
    in
    (* RoundNo: initially 1: *)
    let ()= update_round_no 1
    in
    (* Close the input file, but NOT the Direct Access one: *)
    close_in inch;


(*------------------------*)
(*  "adjust_first_free":  *)
(*------------------------*)
(*  Move the "first_free" offset forward if that position was taken: *)

value rec adjust_first_free': int -> unit =
fun off ->
do {
    if  off >= total_dir_acc_size.val
    then
        (* There is no (N) slot anymore -- stop here, beyond the end of
           the file:
        *)
        first_free.val := off
    else
        (* Make a read (give an explicit offset): *)
        let rawr = read_dir_acc ~offset:off dir_rec_len.val in

        (* Got the record. No need to parse it -- the status is the last
           byte in any case:
        *)
        if  status_of_char (rawr.[String.length rawr - 1]) = N
        then
            (* Yes, it's free: *)
            first_free.val := off
        else
            adjust_first_free' (off + dir_rec_len.val)
};

value adjust_first_free: unit -> unit =
fun () ->
    let start_at = first_free.val + dir_rec_len.val in
    if  start_at >= total_dir_acc_size.val
    then
        (* Nowhere to go: *)
        ()
    else
        adjust_first_free' start_at;


(*------------------------*)
(*  "scan_for_progress":  *)
(*------------------------*)
(*  Used by "init_from_direct_file" to initialise the "progress_info". Small
    Groups are excluded AFTER the whole DirectAccessFile has been processed.
    The global "domains_dict" and "domains_arr" are also constructed:
*)
value rec scan_for_progress: float -> int -> unit  =

fun thresh pert_emails ->
    (* Read the next record sequentially: *)
    let rawr = try Some  (read_dir_acc ~offset:(-1) dir_rec_len.val)
               with[End_of_file -> None]
    in
    match rawr with
    [ None ->
        (* No more records; "domains_dict" has been formed. Make the
           "domains_arr" as well:
        *)
        let ()= mk_doms_arr () in

        (* Remove small Groups from the "progress_info". As it seems
           to be unsafe to do removals while iterating over the hash
           table, fisrt accumulate the entries to be removed in a list:
        *)
        let int_thresh = int_of_float (thresh *. (float_of_int pert_emails))
        in
        let to_remove = Hashtbl.fold
            (fun grid gr_stats tr ->
                if  gr_stats.gps_pert_emails > int_thresh
                then  tr            (* Keep it!  *)
                else  [grid :: tr]  (* To remove *)
            )
            progress_info []
        in
        List.iter (fun tr -> Hashtbl.remove progress_info tr) to_remove

    | Some rr ->
        let prec    = parse_dir_rec rr                  in
        let email   = prec.rec_fields.(email_pos)       in
        let dom     = Misc_utils.get_email_domain email in

        (* Include the "dom" into the global "domains_dict":    *)
        let ()=
        try
            let li = Hashtbl.find domains_dict dom      in
            (* If we got here, we have seen this domain before: *)
            li.dom_total_emails:= li.dom_total_emails +1
        with
        [Not_found ->
            (* Get the curr file offset (AFTER realing: *)
            let curr_offset  = Unix.lseek dir_fd.val 0 Unix.SEEK_CUR in

            (* Create a new "domains_dict" entry: *)
            Hashtbl.add domains_dict dom
            {
                dom_conf_id      = prec.rec_conf_id;
                dom_group_id     = prec.rec_group_id;
                dom_offset       = curr_offset - dir_rec_len.val;
                dom_total_emails = 1;
                dom_done_emails  = 0
            }
        ]
        in
        (* Now see whether this e-mail is pertinent to the statistics being
           initialised:
        *)
        let pert_emails' =
            if  List.mem prec.rec_status [S; F; P; T]
            then
                (* Skip this record -- it's not to be sent, and is excluded
                   from statistics:
                *)
                pert_emails
            else
            do {
                (* In all other cases, the status will normally be (N) --
                   transient errors are cleared in between the rounds. The
                   only exception is when a failed program is re-started
                   immediately after crash:
                *)
                try
                    (* Have we already encountered this GroupID? *)
                    let gr_stats  =
                        Hashtbl.find progress_info prec.rec_group_id  in

                    (* Yes, we already have this GroupID.
                       Have we already encountered this domain?
                    *)
                    let gr_ecount = gr_stats.gps_pert_emails   in
                    let dstats    = gr_stats.gps_domains_stats in
                    do {
                        assert (gr_stats.gps_pert_emails >= 1);
                        try
                            let dom_stats  = Hashtbl.find dstats dom   in
                            let dom_ecount = dom_stats.dps_pert_emails in
                            do {
                                (* Yes -- update the "dom" entry: *)
                                assert (dom_ecount >= 1);

                                dom_stats.dps_pert_emails := dom_ecount + 1;
                                dom_stats.dps_done_emails := 0;
                                dom_stats.dps_succ_emails := 0
                            }
                        with
                        [Not_found ->
                            (* No  -- first time we see this "dom": *)
                            Hashtbl.add dstats dom
                            {
                                dps_pert_emails = 1;
                                dps_done_emails = 0;
                                dps_succ_emails = 0
                            }
                        ];

                        (* Now update  the "group_id"  entry:
                           NB: "dstats" was  already  updated:
                        *)
                        gr_stats.gps_pert_emails := gr_ecount + 1;
                        gr_stats.gps_done_emails := 0;
                        gr_stats.gps_succ_emails := 0
                    }
                with
                [Not_found ->
                    (* This "group_id" was not seen yet. Create a new entry: *)

                    let dstats  = Hashtbl.create 100                  in
                    let conf    = confs_arr.val.(prec.rec_conf_id)    in
                    let gr_name = conf.Barrage_config.from_group_name in
                    do {
                        Hashtbl.add dstats dom
                        {
                            dps_pert_emails = 1;
                            dps_done_emails = 0;
                            dps_succ_emails = 0
                        };

                        Hashtbl.add progress_info prec.rec_group_id
                        {
                            (* "group_name" is to be taken as "dom" if the
                               "conf" is the default one:
                            *)
                            gps_group_name    =
                                if  prec.rec_conf_id = 0
                                then dom
                                else gr_name;

                            gps_pert_emails   = 1;
                            gps_done_emails   = 0;
                            gps_succ_emails   = 0;
                            gps_domains_stats = dstats
                        }
                    }
                ];
                (* New "pertinent" e-mail was encountered: *)
                pert_emails + 1
            }
        in
        (* Tail-recursive call: *)
        scan_for_progress thresh pert_emails'
    ];


(*----------------------------*)
(*  "init_from_direct_file":  *)
(*----------------------------*)
(*  If the Direct Access File already exists, there is no need to parse the
    text-based Mailing List again:
*)
value init_from_direct_file: unit -> unit =
fun () ->
    (* Try to open the existing Direct Access File: *)
    let conf0        = confs_arr.val.(0)                    in
    let thresh       = conf0.Barrage_config.stats_threshold in
    try
    do {
        (* We cannot use "Misc_utils.open_write_locked" function here, as the
           latter either appends or over-writes the file. Rather, we open and
           lock it explicitly here:
        *)
        dir_fd.val  := Unix.openfile dir_file_name.val [Unix.O_RDWR] 0o600;

        Misc_utils.lock_or_abort
            ~rc: Domains_config.no_restart_rc
            dir_file_name.val
            dir_fd.val;

        (* Get the fields info from the config and preamble.
           NB: "n_fields" includes  the extra data field.
           The "first_free" value may need adjustment, as the file may have
           already been used:
        *)
        adjust_first_free ();

        (* The following function performs a sequential read. Initial offset
           is 0, as the Direct Access File has just been opened:
        *)
        let get_pre_int: unit -> int =
            fun () -> Scanf.sscanf
                      (read_dir_acc  ~offset:(-1) preamble_int_len)
                      preamble_int_format_in (fun i -> i)
        in
        let n_fields' = get_pre_int () in
        do {
            assert (n_fields' = n_fields.val  &&  n_fields.val >= 2);

            for  i = 1 to n_fields.val
            do {
                let f_off  = get_pre_int () in
                let f_len  = get_pre_int () in
                do {
                    (*  Set "dir_rec_len" and "all_fields":
                        The last field indicates the total record length
                        (the "\n" in front is already accounted for!):
                    *)
                    if   i = n_fields.val
                    then dir_rec_len.val := f_off + f_len
                    else ();

                    all_fields.val := all_fields.val @ [(f_off, f_len)]
                }
            }
        };
        assert (dir_rec_len.val > 0);

        (* Save the file size: *)
        total_dir_acc_size.val := (Unix.stat dir_file_name.val).Unix.st_size;
        assert
        ((total_dir_acc_size.val - preamble_len.val) mod dir_rec_len.val = 0);

        (* Now initialise the "progress_info". This is difficult, as we don't
           have the Groups composition data here -- will have to  go  through
           the file.
           In this process, we also fill in  "domains_dict" and "domains_arr"
           data structures:
        *)
        ignore (Unix.lseek dir_fd.val preamble_len.val Unix.SEEK_SET);

        scan_for_progress thresh 0
    }
    with
    [ hmm ->
        raise (Scheduler_Error
              ("Cannot initialise from the Direct Access File \"" ^
              dir_file_name.val ^"\": "^(Misc_utils.print_exn hmm)))
    ];

(*-------------*)
(*  "mk_dip":  *)
(*-------------*)
value mk_dip: string -> Unix.inet_addr -> string =
fun dom ip ->
    dom^"-"^(Unix.string_of_inet_addr ip);

(*----------------------*)
(*  "dns_cacher_loop":  *)
(*----------------------*)
(* The body of the DNS Cacher Threads: *)

(*  The counter for the domains already resolved: *)
value done_dns = ref 0;

(*  The list of failed domains, worth re-trying : *)
value to_retry = ref [];


value rec dns_cacher_loop:
    Dns_client.dns_agent -> array string -> int -> unit =

fun dns_agent all_domains log_step ->
    (* Get the domain to process: no lock is needed here if the threads
       are not pre-emptable:
    *)
    if  done_dns.val >= Array.length all_domains
    then
        (* All done. Don't forget to close the Agent! *)
        Dns_client.close_dns_agent dns_agent

    else
    (* Do the actual resolution *)

    (* Is it time to print some statistics? *)
    let ()  = if  done_dns.val <> 0 && done_dns.val mod log_step = 0
              then log
                   (Printf.sprintf "DNS: %d domains done\n" done_dns.val)
              else ()
    in
    let dom = all_domains.(done_dns.val)        in
    let ()  = done_dns.val :=  done_dns.val + 1 in

    (* Get the config  for this "dom" -- it MUST already exist: *)
    let li  = Hashtbl.find domains_dict dom     in
    let conf= confs_arr.val.(li.dom_conf_id)    in

    (* Perform the actual DNS look-up: *)
    let mx_ips =
        try  Dns_client.all_mx_ips dns_agent dom
        with
        (* Check whether we can re-try this domain later: *)
        [Dns_client.Negative_Result _ ->
            do {  (* Really negative result, no re-trying *)
                log (Printf.sprintf "\nUNRESOLVED DOMAIN: %s\n" dom);
                []
            }
        | hmm ->
            do {  (* Can possibly re-try: *)
                log (Printf.sprintf
                    "\nWARNING: TRANSIENT DNS ERROR on %s: %s\n"
                    dom (Misc_utils.print_exn hmm));

                to_retry.val := [dom :: to_retry.val];
                []
            }
        ]
    in
    do {
        (* Cache the result, even []. Use "replace" here, as the Cacher can
           run in multiple rounds,   with earlier [] results replaced after
           re-trying:
        *)
        Hashtbl.replace dns_cache dom mx_ips;

        (* What is the maximum load configured for the IPs found? If, for this
           "dom", the limit is "Absolute", we will distribute it evenly across
            all IPs (this limit is not actually used):
        *)
        let (max_conf_load_soft, max_conf_load_hard) =
            match conf.Barrage_config.group_threads_limit with
            [
            Barrage_config.Absolute n ->
                let n_ips  = List.length mx_ips in
                let load   =
                    if  n_ips <> 0
                    then n / n_ips
                    else max_int
                in
                (load, load)

            | Barrage_config.Per_Server_IP soft hard ->
                (soft, hard)
            ]
        in      
        (* Now: for all IPs encountered, set  (curr_load = 0, max_load_soft,
           max_load_hard) in the corresp "dip_threads_load" entries:
        *)
        List.iter
        (fun ip ->
            let dip = mk_dip dom ip
            in
            Hashtbl.replace dip_threads_load dip
                            (ref 0, max_conf_load_soft, max_conf_load_hard)
        )
        mx_ips;

        (* Tail-recursive call:  *)
        dns_cacher_loop dns_agent all_domains log_step
    };

(*-----------------------*)
(*  "dns_cacher_start":  *)
(*-----------------------*)
(*  This initialiser is needed just to create a DNS Agent from within a DNS
    Cacher Thread before going into the "dns_cacher_loop":
*)
value dns_cacher_start:
    Dns_client.dns_agent_config -> array string -> int -> unit =

fun dns_conf all_domains log_step ->
    let dns_agent = Dns_client.mk_dns_agent dns_conf in
    dns_cacher_loop dns_agent all_domains log_step;


(*---------------------*)
(*  "run_dns_cacher":  *)
(*---------------------*)
(*  Creates "dns_cacher" threads, waits for them to complete, analyses the
    results:
*)
value rec run_dns_cacher: array string -> int -> unit =
fun all_domains rem_rounds ->
    if  rem_rounds <= 0
    then
        (* No more invocations. If there are any domains not yet re-tried,
           signal an error on them:
        *)
        List.iter
        (fun dom ->
            log (Printf.sprintf "\nUNRESOLVED DOMAIN: %s\n" dom)
        )
        to_retry.val
    else
    let n_domains   = Array.length all_domains in
    let conf0       = confs_arr.val.(0) in
    let dns_threads = conf0.Barrage_config.dns_threads in
    do {
        (* Create the DNS Threads: *)
        log (Printf.sprintf
            "Filling in the DNS Cache for %d Domains: Creating %d DNS Threads"
             n_domains dns_threads);

        done_dns.val := 0;

        for i = 0 to (dns_threads - 1)
        do {
            try
                let stack_size_k = conf0.Barrage_config.thread_stack_k in
                let dns_conf     = Barrage_config.mk_dns_config  conf0 in
                let log_step     = max 1 (n_domains / 200)             in
                ignore (Tnio.thread_create
                        ~stack_size_k:stack_size_k ~priority:0
                        (fun () ->
                            dns_cacher_start dns_conf all_domains log_step
                       ))
            with
            [ hmm ->
                (* Thread creation failed: BAD: *)
                let msg = Printf.sprintf "DNS Thread %d creation ERROR: %s"
                                         i (Misc_utils.print_exn hmm)
                in do{
                    log msg;
                    Misc_utils.fatal_error msg
                }
            ]
        };
        (* Run the DNS Threads: *)
        log "Running the DNS Threads...\n";
        Tnio.threads_scheduler ();

        (* On exit from the Threads Scheduler, we are done for this round: *)
        log (Printf.sprintf "DNS: %d domains done\n" done_dns.val);

        (* If there are domains to re-try, do a recursive invocation: *)
        if  to_retry.val <> []
        then
            let retry_domains = Array.of_list to_retry.val in
            do {
                to_retry.val := [];
                run_dns_cacher retry_domains (rem_rounds - 1)
            }
        else ()
    };

(*----------------*)
(*  "unload_db":  *)
(*----------------*)
(*  Unloads the contents of an MList DB into an MList File (temporary or
    otherwise), and returns the MList File name to be used by "init". In
    the "Dry-Run" mode, no DB update is done:
*)
value unload_db:
    Barrage_config.mlist_db_config -> bool -> string -> string =

fun dbc dry_run our_envel ->
try
    (* Initialise the DB Logger: *)
    let ()  = Gatling_db.init ~log_fun:log  in

    (* Open a DB connection -- HERE, and save it globally:   *)
    let dbh = Gatling_db.open_db dbc.Barrage_config.access_parms      in
    let ()  = db_handle.val := Some dbh                               in

    let psid= Gatling_db.get_session_id dbh                           in
    let ()  = log (Printf.sprintf "\nPREVIOUS SESSION ID: %d\n" psid) in

    (* The following DB updating opetrations are NOT performed in the
       "Dry-Run" mode:
    *)
    let ()  =
        if dry_run
        then    ()
        else
        do{
            (* Do "manual" removal of requested entries from the DB: *)
            Gatling_db.remove_entries
                dbh
                ~rem_file :dbc.Barrage_config.remove_entries
                ~list_name:dbc.Barrage_config.list_name;

            (* Scan the Bounces File, mark bounced addresses. As there is a
               latency is the arrival of bounces, they are correctly attrib-
               uted to the PREVIOUS session:
            *)
            if  dbc.Barrage_config.mark_bounced
            then
            Gatling_db.mark_bounced
                dbh
                ~sid           :psid
                ~list_name     :dbc.Barrage_config.list_name
                ~file_name     :dbc.Barrage_config.bounces_file
                ~reset_file    :dbc.Barrage_config.reset_bounces_file
                ~check_sessions:dbc.Barrage_config.reset_succ
                ~our_envelope  :our_envel
            else ();

            (* Re-set failure counters on successful entries: *)
            Gatling_db.reset_succ
                dbh
                ~list_name     :dbc.Barrage_config.list_name
                ~check_sessions:dbc.Barrage_config.reset_succ;

            (* Possibly sweep the constantly-failing entries: *)
            Gatling_db.sweep_failed
                dbh
                ~list_name:dbc.Barrage_config.list_name
                ~thresh   :dbc.Barrage_config.sweep_failed
        }
    in
    (* The MList file to be used:    *)
    let mlist_file =
        if   dbc.Barrage_config.buff_file_name <> ""
        then
             dbc.Barrage_config.buff_file_name
        else
            (* NB: if there is no DB unloading, there is no point in gene-
               rating a new buff file name:
            *)
            if  dbc.Barrage_config.unload_db
            then
                Misc_utils.uniq_file_name "/tmp" "MList-" ".txt"
            else
                ""
    in
    do {
        (* Now actually unload the DB, unless requested not to do so: *)
        if dbc.Barrage_config.unload_db
        then
        do {
            try
                Gatling_db.unload_mlist
                    ~extra_flds  : dbc.Barrage_config.extra_fields
                    ~extra_conds : dbc.Barrage_config.extra_conds
                    dbh
                    dbc.Barrage_config.list_name
                    mlist_file
            with
            [hmm ->
                Misc_utils.fatal_error
                    ("Send_scheduler.unload_db: DB unloading failed: "^
                    (Misc_utils.print_exn hmm))
            ];
            (* It's probably a good idea to call a GC after unloading: *)
            Gc.compact ();
        }
        else ();

        (* Return the DB Handle and the MList File name: *)
        mlist_file
    }
with
[hmm ->
    Misc_utils.fatal_error ("Send_scheduler.unload_db: FAILED: "^
                           (Misc_utils.print_exn hmm))
];

(*-----------*)
(*  "init":  *)
(*-----------*)
(*  Initialises the Scheduler from the Direct Access File if it is available
    AND more recent than the Mailing List file (or if the latter is omitted,
    given by "");  otherwise, uses the Mailing List file itself.  The latter
    may also be constructed by the DB unloading. This function may also ini-
    tialise the DNS Cache.
    Returns: (Opional DNS Cache, MList file name, DB tags list):
*)
value init: list   Barrage_config.domain_group_conf ->
            array  Barrage_config.internal_conf     ->
            option out_channel                      ->
            float                                   ->

            (option (Hashtbl.t  string (list Unix.inet_addr)) *
             string * list string) =

fun group_confs int_confs mb_outch thresh ->
wrap_exceptions
(fun () ->
    (* Verify the parms;  "group_confs" are assumed to correspond to
       "int_confs" elements from 1 to the last; int_confs.(0) is for
       default Group parms (not a particular Group):
    *)
    let ()= log_ch.val := mb_outch    in

    let ()= assert (Array.length int_confs  >= 1 &&
                    List.length  group_confs = Array.length int_confs - 1)
    in
    (* If the List Source is a DB, we will need to unload that DB into a text
       file. In general, the last modifcation time  of the DB cannot be deter-
       mined, so the DB unloading will always occur,  unless turned off expli-
       citly by the user:
    *)
    let conf0       = int_confs.(0) in       (* The "conf0" always exists *)
    let dry_run     =
        match conf0.Barrage_config.dry_run with
        [   None   -> False
        |   Some _ -> True
        ]
    in
    let dbc         = conf0.Barrage_config.mlist_src     in
    let our_envel   = conf0.Barrage_config.envelope_from in
    let mlist_file  = unload_db dbc dry_run our_envel    in

    (* Make the parms global: *)

    let db_tags     = Gatling_db.std_flds @ dbc.Barrage_config.extra_fields in
    let orig_n_flds = List.length db_tags        in

    let ()= n_fields.val     := orig_n_flds + 1  in
    (* NB: there is one extra field used by the system -- MessageID  *)

    (* NB: the "RoundNo" field in the Preamble! *)
    let ()= preamble_len.val := preamble_int_len * (1 + 2*n_fields.val + 1) in
    let ()= first_free.val   := preamble_len.val in
    let ()= confs_arr.val    := int_confs        in

    (* Does the Direct Access File already exist? *)

    let ()= dir_file_name.val:= conf0.Barrage_config.dir_acc_file in
    let ()=
    try
    do {
        Unix.access dir_file_name.val [Unix.F_OK];

        (* Yes, the Direct Access File exists - check its modification time: *)

        let dir_modt = (Unix.stat dir_file_name.val).Unix.st_mtime in
        let txt_modt = if  mlist_file = ""
                       then 0.0
                       else   (Unix.stat mlist_file).Unix.st_mtime in

        if  dir_modt >= txt_modt
        then
            (* OK, the Direct Access File is more recent  -- try to use it: *)
        try
        do {
            log "Direct Access File exists -- using it for initialisation";
            init_from_direct_file ();
            (* Success! *)
        }
        with
        [hmm ->
            (* Direct Access File failed -- use the text-based one instead: *)
            Misc_utils.fatal_error
                ("Initialisation from the Direct Access File failed: "
                 ^(Misc_utils.print_exn hmm)^".\n" ^
                 "Remove the Direct Access File if you don't need it,\n"^
                 "and start the program again.")
        ]
        else
            (* The Direct Access File exists but is outdated: *)
            Misc_utils.fatal_error
                ("Direct Access File is older than the Text List.\n"    ^
                 "Remove the Direct Access File if you don't need it,\n"^
                 "and start the program again.")
    }
    with
    [_ ->
        (* No Direct Access File -- use the text-based one, if not "": *)
        if  mlist_file <> ""
        then
        do {
            log
            "No Direct Access File -- using the Text List for initialisation";

            init_from_text_file mlist_file group_confs thresh
         }
        else
            Misc_utils.fatal_error
                ("Direct Access File is not present, "^
                "and the Text List File is not specified")
    ]
    in
    (* List initialisation done, call major GC: *)
    let ()= log "List Initialisation Complete" in
    let ()= Gc.compact ()
    in
    (* No matter whether we did  "init_from_text_file"  or  "init_from_direct_
       file", all global objects except "dns_cache" have now been constructed.

       Now decide whether we need to pre-build the DNS cache:
    *)
    let dns_threads       = conf0.Barrage_config.dns_threads in
    let ()=
        if  dns_threads > 9999
        then
            Misc_utils.fatal_error
            (Printf.sprintf "Too many DNS Threads requested: %d" dns_threads)
        else
            ()
    in
    let have_per_ip_limit = List.exists
        (fun conf ->
            match conf.Barrage_config.group_threads_limit with
            [ Barrage_config.Per_Server_IP _ _ -> True
            | _                                -> False
            ]
        )
        (Array.to_list confs_arr.val)
    in
    (* Set the global "do_dns_cache" var: *)

    let ()= do_dns_cache.val :=
        if  have_per_ip_limit
        then
            (* If we have at least one Per-IP limit, we currently DO NEED a
               pre-built DNS Cache:
            *)
            if  dns_threads = 0
            then
                Misc_utils.fatal_error
                ("Send_scheduler.init: Per-IP Thread Limit(s) requested "^
                 "but DNS Threads not provided")
            else
                True
        else
            (* The DNS Cache is not compulsory, but will be created if DNS
               Threads are non-0:
            *)
            dns_threads <> 0
    in
    (* If the DNS Cache is to be used, fill it in: *)
    let mb_dns_cache  =
        if  do_dns_cache.val
        then
        do {
            (* Will run the resolver.  Initially, the array of domains to
               resolve is given by "domains_arr":
            *)
            run_dns_cacher  domains_arr.val conf0.Barrage_config.dns_rounds;

            (* Return the DNS Cache filled in: *)
            Some dns_cache
        }
        else
            None (* No DNS Cache used *)
    in
    do {
        (* All done, call a major GC: *)
        Gc.compact ();
        (mb_dns_cache, mlist_file, db_tags)
    }
);

(*============================*)
(*  Scheduling Using Groups:  *)
(*============================*)
(*-----------------*)
(*  "sched_info":  *)
(*-----------------*)
(*  This is an "opaque" per-thread data structure which keeps track of the
    current offset of that thread in the Direct Access File. The contents:

    (Current_GroupID, Current_Domain, Next_Offset_Bytes):
*)
type sched_info = (int * string * int);


(*---------------------------*)
(*  "check_load_integrity":  *)
(*---------------------------*)
value check_load_integrity: Unix.inet_addr -> string -> int -> int -> unit =
fun ip dom curr_load max_load ->
    if  (curr_load >= 0 && curr_load <= max_load) || max_load = 0
    then
        () (* OK *)
    else
        log (Printf.sprintf
            "\nDomain=%s, IP=%s: WARNING: Load=%d not in [0-%d]\n" 
            dom (Unix.string_of_inet_addr ip) curr_load max_load);


(*-----------------------------*)
(*  "Smtp_client" call-backs:  *)
(*-----------------------------*)
(*  Used when Per-Server-IP load limits are requested, and consequently
    PreCached Resolver is used by the SMTP module.  The "get_ips" call-
    back is provided not here, but rather by the top-level module.
*)
value reserve_dip: string -> bool =
fun dip ->
try
    (* As all IPs are pre-cached, we must only try to reserve a valid IP: *)
    let (curr_load, _, max_load_hard) = Hashtbl.find dip_threads_load  dip
    in
    (* Use the HARD limit here! *)
    if  curr_load.val < max_load_hard
    then
    do {
        (* Increment the "curr_load" on this (Domain, IP):   *)
        curr_load.val :=  curr_load.val + 1;
        True   (* Green light!  *)
    }
    else
        False  (* Cannot use it *)
with
[Not_found ->
    failwith ("Send_scheduler.reserve_dip: DIP="^dip^" not found")
];


value reserve_ip: string -> Unix.inet_addr -> bool =
fun dom ip ->
    reserve_dip (mk_dip dom ip);


value release_ip: string -> Unix.inet_addr -> unit =
fun dom ip ->
    (* If the client notionally disconnects from a non-existent IP, such
       as Unix.inet_addr_any, it may not exist in the "dip_threads_load":
    *)
    let dip = mk_dip dom ip
    in
    try
        let (curr_load, _, _) = Hashtbl.find dip_threads_load  dip
        in
        if  curr_load.val > 0
        then
            (* Decrement the "curr_load" for this IP: *)
            curr_load.val := curr_load.val - 1
        else
            ()
    with
    [Not_found -> ()];


(*-----------------------*)
(*  "check_group_load":  *)
(*-----------------------*)
(*  Returns a pair
    (Load_OK_Flag (whether this record can be used), IP_List)

    NB: IP_List is to be returned IN ANY CASE if "do_dns_cache" is set, as
    it is then used FOR ALL DOMAINS:
*)
value check_group_load:
    dir_rec -> Barrage_config.internal_conf -> string ->
    (bool * (list Unix.inet_addr)) =

fun psdr conf curr_dip ->
    let (email, dom, all_ips) =
        if  do_dns_cache.val
        then
            let email'   = psdr.rec_fields.(email_pos)        in
            let dom'     = Misc_utils.get_email_domain email' in

            let all_ips' = try  Hashtbl.find dns_cache dom'
                           with [Not_found -> []]  (* It should NOT happen! *)
            in
            (email', dom', all_ips')
        else
            (* They are not to be used: *)
            ("", "", [])
    in
    match conf.Barrage_config.group_threads_limit with
    [
      Barrage_config.Absolute n ->
        (* Absolute limit -- examine "abs_threads_load": *)
        let curr_load =
        try
            let curr_load' =
                Hashtbl.find abs_threads_load psdr.rec_group_id  in
            do {
                assert (curr_load'.val >= 1 && curr_load'.val <= n);
                curr_load'
            }
        with
        [Not_found ->
            (* GroupID not there -- so no load on that Group: *)
            ref 0
        ]
        in
        if  curr_load.val < n
        then
        do {
            (* Yes, can use this record -- so increment (or install) the
               load figure for this Group:
            *)
            if  curr_load.val = 0
            then
                (* This Group was not yet started: *)
                Hashtbl.add  abs_threads_load  psdr.rec_group_id  (ref 1)
            else
                curr_load.val := curr_load.val + 1;

            (* Return positive result, with real or [] "all_ips": *)
            (True,  all_ips)
        }
        else
            (* Just return the negative result: *)
            (False, [])

    | Barrage_config.Per_Server_IP conf_soft conf_hard ->
      do {
        assert do_dns_cache.val;

        (* Per-Server-IP limit. Get all IPs of this domain  which still have
           spare load capacity. As we don't know to which IP the client will
           actually connect, we reserve one of them and return others as op-
           tions.    If the client wishes to use them, they must be reserved
           explicitly.
           The SOFT limit is used here:
        *)
        if  all_ips = []
        then
            (* No MX data for this domain.  STILL, allow this addr to be taken
               for sending with empty list of IPs  --  this is deliberate,  in
               order to trigger an exception in the sender, otherwise the addr
               would be merely skipped:
            *)
            (True, [])
        else
        (* Otherwise, see if there are still loadable IPs: *)
        let good_ips =
            List.fold_left
            (fun curr_lst ip ->
                let dip = mk_dip dom ip
                in
                let (curr_load, max_load_soft, max_load_hard) =

                    try  Hashtbl.find dip_threads_load  dip
                    with [Not_found ->
                            failwith ("Send_scheduler.check_group_load: DIP="^
                            dip^" not found")
                         ]
                in
                (* In some cases, e.g. if the IP is specifically de-configured
                   in the cache, "n" and "max_load'" can be different  -- take
                   the min of them:
                *)
                let max_load = min conf_soft max_load_soft in

                (* Check the Send Scheduler's integrity wrt load on this IP: *)
                let ()= check_load_integrity ip dom curr_load.val max_load_hard
                in
                (* This IP is allowed if it still has spare load capacity,
                   OR if the client is already connected to it:
                *)
                if  (curr_load.val < max_load) || (dip = curr_dip)
                then
                    [(ip, curr_load.val) :: curr_lst]
                else
                    curr_lst
            )
            [] all_ips
        in
        (* If we got more than 1 good IP, which IP should be put at the top?
           The "curr_dip" is not necesserily preferred;   it was used in the
           "filter" above only to calculate the load properly. If the client
           needs to re-connect to it,  it will be exactly the same procedure
           as for other IPs.
           We will select the IP with the HIGHEST (but still valid) load. This
           should facilitate reservation of other IPs should it become necess-
           ary, as they would have lower loads on average:
        *)
        if  good_ips <> []
        then
            let (sel_ip, _)  =
                List.fold_left
                (fun ((_, bld) as best) ((_, ld) as curr) ->
                    if   ld > bld
                    then curr
                    else best
                )
                (List.hd good_ips) (List.tl good_ips)
            in
            (* Other IPs are sorted in the ASCENDING order of loads, as they
               may need to be reserved in the future,    so the IPs with the
               lowest load are more likely to be successful:
            *)
            let (sort_ips, _) =
                 List.split
                (List.sort (fun (_, ld1) (_, ld2) -> compare ld1 ld2) good_ips)
            in
            (* The final list to be used: *)
            let lo_ips  = [sel_ip :: (ExtList.List.remove sort_ips sel_ip)] in
            let hi_ips  = Misc_utils.list_minus all_ips lo_ips       in
            let use_ips = lo_ips @ hi_ips                            in
            do {
                (* Reserve the selected IP, UNLESS it's actually "curr_ip" --
                   then it is already reserved:
                *)
                let sel_dip =  mk_dip dom sel_ip in
                if  sel_dip <> curr_dip
                then assert  (reserve_dip sel_dip)
                else ();

                (* We return the list of IPs with the reserved one  coming 1st,
                   but ALL other IPs, including the currently over-loaded ones,
                   are also returned, as their status can change later:
                *)
                (True, use_ips)
            }
        else
            (False, [])         (* No good IPs  -- already too much load *)
      }
    ];


(*---------------------------*)
(*  "find_suitable_record":  *)
(*---------------------------*)
(*  Used by "init_sched_infos" and "get_next_job": *)

value rec find_suitable_record:
    int   -> ~init_mode:bool  -> string ->
    option (sending_job * sched_info)   =

fun curr_off ~init_mode curr_dip        ->
do {
    assert (curr_off >= preamble_len.val &&
            curr_off <= total_dir_acc_size.val);

    (* NB: it is possible that "curr_off" is greater than the file size: this
       happens during initialisation of the number of threads is greater than
       the list length:
    *)
    if  curr_off >= total_dir_acc_size.val
    then
        None
    else
        let rawr = read_dir_acc ~offset:curr_off dir_rec_len.val  in

        (* Got the record. Examine its Status -- no need to parse the
            record for that, just look at the last byte:
        *)
        if  status_of_char (rawr.[String.length rawr - 1]) <> N
        then
            (* Not a "fresh" record -- try more: *)
            find_suitable_record
                (curr_off + dir_rec_len.val) ~init_mode curr_dip
        else
        (* OK, parse the record and check the load on its Group. If the
           GroupID is the same  as that   of the previously encountered
           Group which is already at full load, just skip it:
           XXX: getting GroupID does not require full parse either, but
           for the moment we do it:
        *)
        (* The config for this record: *)
        let psdr = parse_dir_rec rawr  in
        let cid  = psdr.rec_conf_id    in

        let ()   = assert (cid >= 0    &&
                           cid < Array.length confs_arr.val) in

        let conf = confs_arr.val.(cid) in

        (* Check the load on the Group -- can we proceed with it? *)

        let (load_ok, some_ips) = check_group_load psdr conf curr_dip in
        if   load_ok
        then
        do {
            (* Mark the record found as being "in-proGress": *)
            update_status curr_off G;

            (* Nb: "some_ips" will be [] if DNS Cache is not used at all: *)
            let new_job =
            {
                sj_ml_rec      = psdr.rec_fields;
                sj_conf_id     = cid;
                sj_use_ips     = some_ips
            }
            in
            do {
                if  not init_mode
                then
                    let this_thread_id = Tnio.thread_id (Tnio.thread_self ())
                    in
                    do {
                        (* Modify the "threads_jobs". The previous job, if any,
                           is removed first,   to make sure the hidden data do
                           not build up:
                        *)
                        Hashtbl.remove threads_jobs this_thread_id;
                        Hashtbl.add    threads_jobs this_thread_id new_job;
                    }
                else
                    ();

                (* Modify the "first_free", if it is taken now:  *)
                if  curr_off = first_free.val
                then
                    adjust_first_free ()
                else
                    ();

                (* The data to return. The offset points to the NEXT
                   record for the next fetch attempt:
                *)
                Some (new_job,
                        (psdr.rec_group_id,
                        Misc_utils.get_email_domain
                                   (psdr.rec_fields.(email_pos)),
                        curr_off + dir_rec_len.val)
                    )
            }
        }
        else
            (* Already have maximum load for this Group -- jump to the
               beginning of the next Group:
            *)
            find_suitable_record psdr.rec_next_group_off ~init_mode curr_dip
};


(*-------------------*)
(*  "get_next_job":  *)
(*-------------------*)
(*  Called from a thread when it needs the next job to do.  The offset in the
    "sched_info" passed from the thread is the initial position in the Direct
    Access File  where we try to get a new record for the thread.  Returns an
    array of fields, and a new "sched_info", or None if there is nothing more
    to do (the thread will then terminate).
    No need to check initialisation here, since we already have "sched_info"s:
*)

value get_next_job:
    sched_info  -> string -> Unix.inet_addr ->
    option (sending_job * sched_info)  =

fun (curr_grid, dom, next_off) curr_domain curr_ip ->
wrap_exceptions
(fun () ->
do {
    (* "curr_off" at the end of the file is possible: *)
    assert (next_off >= preamble_len.val);

    let curr_dip  = mk_dip curr_domain curr_ip
    in
    if  next_off >= total_dir_acc_size.val
    then
        None
    else
    do {
        (* Decrement the current load on the given Group, if we use Absolute
           limit -- for a Per-Server-IP limit, this is done when the resp IP
           disconnects:
        *)
        let li   = Hashtbl.find domains_dict dom  in    (* MUST be there! *)
        let conf = confs_arr.val.(li.dom_conf_id) in
        let ()   =
        match conf.Barrage_config.group_threads_limit with
        [ Barrage_config.Absolute _ ->

            let curr_load = Hashtbl.find abs_threads_load curr_grid  in
            if  curr_load.val = 1
            then
                (* Eliminate this Group completely from the load hash: *)
                Hashtbl.remove  abs_threads_load curr_grid
            else
                curr_load.val:= curr_load.val -1

        | Barrage_config.Per_Server_IP _ _ -> ()
        ]
        in
        (* Start searching the Direct Aceess File at the "start_off".  NB:
           "start_off" is no less than "next_off", but it is also adjusted
           to the "first_free" offset for efficiency:
        *)
        let start_off  = max next_off first_free.val in

        find_suitable_record start_off ~init_mode:False curr_dip
    }
});

(*-----------------------*)
(*  "init_sched_infos":  *)
(*-----------------------*)
(*  To avoid a "jam" during initialisation, assign initial offsets to all
    threads in one sweeping move through the Direct Access File:
*)
value init_sched_infos:
    unit -> array (option (sending_job * sched_info)) =
fun () ->
    let ()= check_initialisation () in

    let n_threads = (confs_arr.val.(0)).Barrage_config.n_threads in

    (* Create the array of offsets -- initially trivial: *)
    let from_off  = ref  preamble_len.val                        in
    let res_arr   = Array.init n_threads (fun i -> None)         in
    let i         = ref 0                                        in

    let ()= while i.val < n_threads
    do {
        (* Try to find the FIRST job and the NEXT "sched_info" for the "i"th
           thread:
        *)
        match find_suitable_record from_off.val ~init_mode:True ""
        with
        [None ->
         do {
            (* Nothing found for this thread -- and so will be for the rest,
               so break the loop:
            *)
            i.val := n_threads
        }
        | Some (_, (_, _, next_off)) as i_init ->
          do {
            from_off.val   := next_off;
            res_arr.(i.val):= i_init
          }
        ];
        i.val := i.val + 1
    }
    in
    res_arr;


(*---------------------------*)
(*  "update_progress_info":  *)
(*---------------------------*)
(*  Increments the "done" and "succ" counters for the given "grid" and "dom".
    If we are in between the rounds, we increment the corresp "pert" counters
    instead:
*)
value update_progress_info:
      int -> string -> ~perts:bool -> ~cres:proc_status -> unit =
fun grid dom ~perts ~cres ->
    try
        let gr_stats = Hashtbl.find progress_info grid  in
        try
        do {
            let dstats = Hashtbl.find gr_stats.gps_domains_stats dom in
            if  not perts
            then
            (* In the middle of a round: *)
            do {
                dstats.dps_done_emails  := dstats.dps_done_emails   + 1;
                gr_stats.gps_done_emails:= gr_stats.gps_done_emails + 1;
                if  cres = S
                then
                do {
                    dstats.dps_succ_emails   := dstats.dps_succ_emails   + 1;
                    gr_stats.gps_succ_emails := gr_stats.gps_succ_emails + 1
                }
                else ()
            }
            else
            (* In between the rounds: *)
            do {
                dstats.dps_pert_emails  := dstats.dps_pert_emails   + 1;
                gr_stats.gps_pert_emails:= gr_stats.gps_pert_emails + 1
                (* Success status (~cres) is not used here... *)
            };
            assert (  dstats.dps_succ_emails <=   dstats.dps_done_emails);
            assert (  dstats.dps_done_emails <=   dstats.dps_pert_emails);

            assert (gr_stats.gps_succ_emails <= gr_stats.gps_done_emails);
            assert (gr_stats.gps_done_emails <= gr_stats.gps_pert_emails)
        }
        with
        [Not_found ->
            (* Serious inconsistency: *)
            raise (Scheduler_Error
                  (Printf.sprintf
                   "Domain \"%s\" missing in Group \"%s\" (ID=%d)"
                   dom gr_stats.gps_group_name grid))
        ]
    with
    [Not_found ->
        (* No progress statistics is collected about this Group: *)
        ()
    ];


(*------------------------*)
(*  "curr_threads_jobs":  *)
(*------------------------*)
(*  NB: must return COPIES of the hash table, as the original is subject to
    asynchronous updates:
*)
value curr_threads_jobs: unit -> Hashtbl.t int sending_job =
fun  () ->
    wrap_exceptions
    (fun () ->
        Hashtbl.copy threads_jobs
    );


(*--------------------*)
(*  "last_activity":  *)
(*--------------------*)
(*  This is a call-back provided to the Monitor for deadlock detection.  It
    uses the last modification time on the Direct Access File. NB: the file
    may already be closed by the time when the Monitor  makes the last call
    to this function, so use "stat", not "fstat".     If that file does not
    exist, it has (with all the probability) been already deleted by "close_
    scheduler", so we are alive and terminating -- return the curr time:
*)
value last_activity: unit -> float =
fun () ->
try
    (Unix.stat dir_file_name.val).Unix.st_mtime
with
    [Unix.Unix_error Unix.ENOENT _ _ -> Unix.gettimeofday()];
    (* All other errors are propagated to the Monitor *)

(*----------------------*)
(*  "reconstr_ml_rec":  *)
(*----------------------*)
(* Re-construct the Mailing List record from the Sending Job: *)
value reconstr_ml_rec: array string -> string =
fun fields ->
    String.concat fields_sep (Array.to_list fields);


(*==================================*)
(*  Statistics and Post-Processing: *)
(*==================================*)
(*-------------------------*)
(*  "report_send_result":  *)
(*-------------------------*)
(*  NB:  when this call-back is invoked from a thread,  the offset in "sched_
    info" is that of the NEXT record already  (XXX: what to do if the thread
    violates this calling sequence?),  so we need to decrement it again  for
    "update_status".
*)
type send_result =
[   Success
|   Failed_Try_ReSend
|   Failed_No_ReSend
|   Failed_Purge_It
|   Failed_Strict_5XX
];


value report_send_result: sched_info -> send_result -> unit =
fun (grid, dom, off) sres -> 

wrap_exceptions
(fun () ->
do {
    (* "off" at the end of the file is possible: *)
    assert (off >= preamble_len.val   &&
            off <= total_dir_acc_size.val);

    let cres = match sres with
    [   Success             -> S
    |   Failed_Try_ReSend   -> R
    |   Failed_No_ReSend    -> F
    |   Failed_Purge_It     -> P
    |   Failed_Strict_5XX   -> T
    ]
    in
    do {
        (* Remove the curr job, since it is done/undone: *)
        let this_thread_id = Tnio.thread_id (Tnio.thread_self ()) in
        Hashtbl.remove threads_jobs this_thread_id;

        (* "off" points to the NEXT record: *)
        update_status (off - dir_rec_len.val) cres;

        (* Also, update the progress statistics: *)
        update_progress_info grid dom ~perts:False ~cres:cres
    }
});


(*---------------------*)
(*  "post_send_proc":  *)
(*---------------------*)
(*  To be invoked in the SINGLE-THREADED environment,   after sending has been
    done for the current Round.   Analyses and returns  the statistics  of all
    domains by scanning the Direct Access File.
    It also prepares the Direct Access File for the next sending Round: the (R)
    ("failed but re-sendable") flags are changed to (N)  (so these entries can
    be processed again), and so are the (G) flags (but normally they shouldn't
    occur).
    The records which failed and are NOT re-sendable   (or all failed records,
    if it's the last round) are marked in the DB as failed:
*)
type domain_stats_full =
{
    ds_domain_name: string;
    ds_success    : int;
    ds_resend     : int;
    ds_failed     : int;
    ds_perm_failed: int;
    ds_in_progr   : int;
    ds_not_yet    : int;
    ds_succ_rate  : float
};

value post_send_proc:
    ~last_round  : bool  ->
    ~stats_thresh: float ->
    (int * array domain_stats_full) =

fun ~last_round ~stats_thresh    ->
wrap_exceptions
(fun () ->
do {
    check_initialisation ();

    (* Before the next round, re-set some global data -- but DON'T touch the
       DNS Cache! NB: we completely clear "abs_threads_load",  but we cannot
       do so with "dip_threads_load", as its content is created  at the same
       time as DNS cache, and is supposed to be persistent.    In the latter
       case, re-set load levels to 0 (if they are not 0 already):
    *)
    Hashtbl.clear abs_threads_load;
    Hashtbl.clear threads_jobs;
    first_free.val := preamble_len.val;
    adjust_first_free ();

    Hashtbl.iter
        (fun _ (curr_load, _, _) -> curr_load.val := 0)
        dip_threads_load;

    (* Re-set the statistics in "progress_info" (both "Pertinent" and "Done"
       counters):
    *)
    Hashtbl.iter
        (fun _ gr_stats ->
        do {
            gr_stats.gps_pert_emails := 0;
            gr_stats.gps_done_emails := 0;
            gr_stats.gps_succ_emails := 0;

            Hashtbl.iter
                (fun _ dstats ->
                do {
                    dstats.dps_pert_emails := 0;
                    dstats.dps_done_emails := 0;
                    dstats.dps_succ_emails := 0
                })
                gr_stats.gps_domains_stats
        })
        progress_info;

    (* If the threshold is not appropriate, don't collect the stats at all: *)
    let do_stats    = stats_thresh >= 0.0 && stats_thresh < 1.0 in

    (* Results are initially put into the hash table
       Domain_Name => (N, G, S, R, F, P); SuccRates are not yet computed:
    *)
    let dom_hash    = Hashtbl.create 100000 in
    let total_tried = ref 0 in

    (* Go through all Direct Access records: *)
    let conf0 = confs_arr.val.(0)              in
    let dbc   = conf0.Barrage_config.mlist_src in
    let dbh   = Option.get db_handle.val       in
    let sid   =
        (* If we are in the last round, increment our Session ID, and return
           the new one:
        *)
        if  last_round
        then
        do {
            Gatling_db.incr_session_id dbh;
            Gatling_db.get_session_id  dbh
        }
        else
          Gatling_db.get_session_id  dbh
    in
    let () = log (Printf.sprintf "\nCURRENT SESSION ID: %d\n" sid) in
    (* The Record Scanner itself: *)

    let rec scan_dir_recs: int -> unit =
    fun curr_off ->
        match try  Some (read_dir_acc ~offset:curr_off dir_rec_len.val)
              with [End_of_file -> None]
        with
        [   None ->
            (* All done: *)
            assert (curr_off = total_dir_acc_size.val)

          | Some rr ->
            (* Process the record: *)
            let prec   = parse_dir_rec rr               in
            let dom    = Misc_utils.get_email_domain
                        (prec.rec_fields.(email_pos))   in

            let crypt  = prec.rec_fields.(crypt_pos)    in
            let status = prec.rec_status                in
            do {
                if  do_stats
                then
                do {
                    (* Stats increments:   *)
                    if  status <> N
                    then
                        total_tried.val := total_tried.val + 1
                    else
                        ();

                    let (dn, dg, ds, dr, df, dp, dt) = match status  with
                    [   N -> (1, 0, 0, 0, 0, 0, 0)
                      | G -> (0, 1, 0, 0, 0, 0, 0)
                      | S -> (0, 0, 1, 0, 0, 0, 0)
                      | R -> (0, 0, 0, 1, 0, 0, 0)
                      | F -> (0, 0, 0, 0, 1, 0, 0)
                      | P -> (0, 0, 0, 0, 0, 1, 0)
                      | T -> (0, 0, 0, 0, 0, 0, 1)
                    ]
                    in
                    (* The current stats for this "dom", if any: *)
                    try
                        let (cn,cg,cs,cr,cf,cp,ct) = Hashtbl.find dom_hash dom
                        in  Hashtbl.replace dom_hash
                            dom
                            (cn+dn, cg+dg, cs+ds, cr+dr, cf+df, cp+dp, ct+dt)

                        with
                        [Not_found ->
                            Hashtbl.add dom_hash
                            dom (dn, dg, ds, dr, df, dp, dt)
                        ]
                }
                else
                    (* No detailed statistics collected: *)
                    ();

                (* We still need, in any case,  to update the statistics in
                   the "progress_info" -- but only if the curr address will
                   be used for sending or re-sending in the next round:
                *)
                if  status = N || status = R || status = G
                then
                    update_progress_info
                        prec.rec_group_id dom ~perts:True ~cres:status
                else ();

                (* Re-set the status flag, if the message is re-sendable: *)
                if  status = G || status = R
                then
                    update_status curr_off N
                    (* To do re-send on them in the next round *)
                else ();

                (* NB:  Marking of errors in the DB is done  only during the
                   last round,  as otherwise,  persistent   (F), (P) and (T)
                   errors would be marked multiple times.
                   (P) records will be removed immediately (not just marked)
                   if "remove_perm_fails" is set -- but not (T) records,  as
                   the Strict 5XX mode might indeed be too strict, and would
                   result in unnecessary removals:
                *)
                if  last_round
                then
                let remove_fails = dbc.Barrage_config.remove_perm_fails in
                try
                    if  status = F || status = R || status = T ||
                       (status = P &&  not remove_fails)
                    then
                        Gatling_db.mark_failed
                            dbh
                            ~sid           : sid
                            ~key           : (Gatling_db.Crypt crypt)
                            ~check_sessions: dbc.Barrage_config.reset_succ
                            ~strict_5xx    : (status = T)
                    else
                    if  status = P && remove_fails
                    then
                        Gatling_db.remove_entry dbh (Gatling_db.Crypt crypt)
                    else
                        ()
                with [_ -> ()]
                else ();

                (* Next record:  *)
                scan_dir_recs (curr_off + dir_rec_len.val)
            }
        ]
    in
    (* Apply "scan_dir_recs". Start at the end of the preamble:    *)
    scan_dir_recs preamble_len.val;

    (* If it was the last round, we can now close the "db_handle": *)
    let ()=
        if  last_round
        then
        do {
            Gatling_db.close_db dbh;
            db_handle.val :=   None
        }
        else ()
    in
    (* Now convert the Hash Table into a List, then Array, and compute the
       Success Rates:
    *)
    if  do_stats
    then
        let int_thresh = int_of_float
                         ((float_of_int total_tried.val) *. stats_thresh)
        in
        let dom_list = Hashtbl.fold
            (fun dom (n, g, s, r, f, p, t) curr ->
                let tried = n+g+s+r+f+p+t  in
                if  tried > int_thresh
                then
                    let succ_rate =
                        if   tried <> 0
                        then (float_of_int s) /. (float_of_int tried) *. 100.0
                        else 0.0
                    in
                    [{
                        ds_domain_name  = dom;
                        ds_success      = s;
                        ds_resend       = r;
                        ds_failed       = f;
                        ds_perm_failed  = p+t;
                        ds_in_progr     = g;
                        ds_not_yet      = n;
                        ds_succ_rate    = succ_rate
                    } ::
                    curr]
                else
                    curr
            )
            dom_hash []
        in
        let dom_arr = Array.of_list dom_list in
        do {
            (* Sort the "dom_arr" in the ascending order of Succ Rates: *)
            Array.sort
                (fun ds1 ds2 -> compare ds1.ds_succ_rate ds2.ds_succ_rate)
                dom_arr;

            (* Return the statistics: *)
            (total_tried.val,   dom_arr)
        }
    else
        (* No statistics -- return empty vals: *)
        (0, [| |])
});

(*-------------------------*)
(*  "get_progress_stats":  *)
(*-------------------------*)
(*  Exports the information from "progress_info": *)

type domain_progress = (string * int * int * int);
    (* (DomainName, PertinentEMails, DoneEMails, SuccEMails)  *)

type group_progress  = (string * int * int * int * (list domain_progress));
    (* (GroupName,  PertinentEMails, DoneEMails, SuccEMails,
        PerDomainProgress);
       "PerDomainProgress" is [] for Singleton Groups
    *)

value get_progress_stats: unit -> list group_progress =
fun () ->
do {
    check_initialisation ();

    Hashtbl.fold
    (fun _ gr_stat res1 ->
        let gr_name = gr_stat.gps_group_name  in
        let gr_pert = gr_stat.gps_pert_emails in
        let gr_done = gr_stat.gps_done_emails in
        let gr_succ = gr_stat.gps_succ_emails in

        let dstats  = Hashtbl.fold
            (fun dom   ds  res2 ->
                [(dom, ds.dps_pert_emails,
                       ds.dps_done_emails,
                       ds.dps_succ_emails) :: res2]
            )
            gr_stat.gps_domains_stats []
        in
        (* Check the sums: *)
        let (sum_pert, sum_done, sum_succ) =
            List.fold_left
            (fun (cp, cd, cs) (_, dom_ecount, dom_done, dom_succ) ->
                 (cp + dom_ecount, cd + dom_done,  cs + dom_succ)
            )
            (0,0,0) dstats
        in
        let ()= assert
                (gr_pert = sum_pert && gr_done = sum_done && gr_succ = sum_succ)
        in
        [(gr_name, gr_pert, gr_done, gr_succ, dstats) :: res1]
    )
    progress_info []
};
