(*pp camlp4r *)
(* vim:ts=4:syntax=ocaml
*)
(*===========================================================================*)
(*                           "send_scheduler.mli":                           *)
(*                (C) Explar Technologies Ltd, 2003--2006                    *)
(*===========================================================================*)
(*  "init":
    Must be invoked in the SINGLE-THREADED environment before any other functn
    of this module.
    Args:   (1) configs for all non-default Groups;
            (2) all internal configs: #0 is for Default,   the rest correspond
                to the above Groups;
            (3,4): channel and threshold for printing domain statistics during
                initialisation;

    If the direct access file exists, is valid, and is no less recent than the
    mailing list file (or the latter is given as ""),    the former is used.
    Otherwise, the latter is used, and the direct access file is re-created.

    The mailing list file can also be constructed by DB unloading.

    If the optional pair (output_channel, threshold) are given  as Arg 3,  and
    the direct access file is (re-)created,  some domain statistics is written
    into that channel.

    Return value: (DNS_Cache or None, MList file name, DB Field Names).

    NB: The DNS Cache is None if it has not been built by this function (e.g.,
    it is not required by the configs provided).
*)
value init:
    list  Barrage_config.domain_group_conf ->
    array Barrage_config.internal_conf     ->
    option out_channel                     ->
    float                                  ->
   (option (Hashtbl.t string (list Unix.inet_addr)) * string * list string);

(*  "sched_info":
    The abstract type of per-thread scheduling information.  IMPORTANT: this
    type is NOT mutable, so its objects can be re-used  in a  referentially-
    transparent way:
*)
type sched_info = 'a;


(*  "sending_job":
    The type of the data a thread receives when requests a new job to do:
*)
type sending_job =
{
    (* Mailing List Record Fields: *)
    sj_ml_rec : array string;

    (* Index in the array of "internal_conf"s (as in "init") for the conf
       for the current record:
    *)
    sj_conf_id: int;

    (* If PreCached MX IPs are used,  the HEAD  of "use_ips"  is reserved for
       this job; if it is not taken for whatever reason, it must be released.
       The rest of IPs (if any) are not reserved, but may be used if the pri-
       mary one fails; however, they must be reserved before they can be used.
       The IPs list is already randonly permuted:
    *)
    sj_use_ips: list Unix.inet_addr
};

(*  "init_sched_infos":
    Returns an array of initial jobs and "sched_info" objects for the number
    of threads which has already been configured in "init":
*)
value init_sched_infos:
    unit -> array (option (sending_job * sched_info));

(*  "get_next_job":
    The main scheduling call-back of sender threads.
    Arg1: curr sched_info; Arg2: curr domain; Arg3: curr_ip used:
*)
value get_next_job:
    sched_info -> string -> Unix.inet_addr ->
    option (sending_job * sched_info);

(*  The following call-backs are for statistical purposes, and for managing
    multi-round sends:
*)
type  send_result =
[   Success
|   Failed_Try_ReSend
|   Failed_No_ReSend
|   Failed_Purge_It
|   Failed_Strict_5XX
];

(*  "report_send_result":  *)
value report_send_result:  sched_info -> send_result -> unit;

(*  "reserve_ip": 
    Arg1: Mail_Domain; Arg2: IP
    Returns True iff the reservation has been confirmed:
*)
value reserve_ip: string -> Unix.inet_addr -> bool;

(*  "release_ip":
    Arg1: Mail_Domain; Arg2: IP
*)
value release_ip: string -> Unix.inet_addr -> unit;

(*  "last_activity":
    Used by the Monitor for deadlock prevention:
*)
value last_activity: unit -> float;

(*  Get the statistics of currently working threads, as a map
    ThreadID => SendingJob:
*)
value curr_threads_jobs: unit -> Hashtbl.t int sending_job;

(*  Get the progress info on Domain Groups: *)

(*  Get the post-sending statistics on all domains (NOT THREAD SAFE!), as:
    (Total_EMails_NonSkipped, Domain_Stats array)

    (see below for the "domain_stats" definition). The array is sorted in the
    ASCENDING order of SuccRate.

    The function also prepares  the Direct Access File  for the next round in
    multi-round sends:  it re-sets the status on "failed but re-sendable" and
    "in-progress" entries in that file, so they can be tried again during the
    next round,  and re-sets  the status on  "failed, don't re-send now"  and
    "failed, likely permanently" entries, so they will not interfere with the
    next round's statistics.

    If "stats_thresh" > 0.0,  information will be returned for only those do-
    mails which have more non-skipped e-mail addresses than   (stats_thresh *
    Total_EMails_NonSkipped):
*)
type domain_stats_full =
{
    ds_domain_name: string; (* Domain name *)

    ds_success    : int;    (* Successfully sent e-mails *)
    ds_resend     : int;    (* Failed sends, but re-sending  possible  *)
    ds_failed     : int;    (* Failed sends, don't try to re-send now  *)
    ds_perm_failed: int;    (* Failed sends, likely permanently bad    *)
    ds_in_progr   : int;    (* E-mails currently being sent or aborted *)
    ds_not_yet    : int;    (* Sending not yet started   *)
    ds_succ_rate  : float   (* Percentage of successfull sent e-mails, of
                               the total number EXCLUDING skipped ones *)
};

value post_send_proc:
    ~last_round   : bool  ->
    ~stats_thresh : float ->
    (int * array domain_stats_full);


(*  "reconstr_ml_rec":
    A helper function: merges parsed record fields back together:
*)
value reconstr_ml_rec: array string -> string;


(* "close_scheduler":
    If invoked, the environment must be single-threaded:
*)
value close_scheduler: unit -> unit;


(* "get_round_no" and "update_round_no":
   Memoise the currrent Round Number, needed in case of re-starts:
*)
value get_round_no   : unit -> int;
value update_round_no: int  -> unit;


(*  "{groups|domains}_progress":
    For reporting information on send progress:
*)
type domain_progress = (string * int * int * int);
    (* (DomainName, PertinentEMails, DoneEMails, SuccEMails) *)

type group_progress  = (string * int * int * int * (list domain_progress));
    (* (GroupName,  PertinentEMails, DoneEMails, SuccEMails,
        PerDomainProgress);
       "PerDomainProgress" is [] for Singleton Groups
    *)

value get_progress_stats: unit -> list group_progress;

