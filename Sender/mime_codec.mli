(*pp camlp4r *)
(* vim:ts=4:syntax=ocaml
*)
(*===========================================================================*)
(*                              "mime_codec.mli":                            *)
(*    Interface to Parsing and Generation of MIME-Enabled SMTP Messages      *)
(*                  (C) Explar Technologies Ltd, 2003--2006                  *)
(*===========================================================================*)
(*======================*)
(*  Message Structure:  *)
(*======================*)
(*  Defined in "Mime_xml_types": *)

open Mime_xml_types;

exception MIME_Error of string;


(*===================*)
(*  Main Functions:  *)
(*===================*)
(* "msg_part_from_file":
    Arg1: disposition
    Arg2: mime_type
    Arg3: file_name
*)
value msg_part_from_file:
    mime_part_disp -> string -> string -> mime_msg_part;

(*  "cook_msg":
    Takes an "smtp_msg_info" object and creates a "cooked" one, which is ready-
    to-send (up to tag sybstituitions):
*)
value cook_msg: smtp_msg_info -> smtp_msg_cooked;

(*  "mk_final_string":
    Performs tags substituition into "smtp_data":
*)
type  string_map      = Hashtbl.t string string;
value null_tags_map:    string_map;

value mk_final_string: string_map -> smtp_data -> string;

(*  "mk_final_msg":
    Performs tags substituition into a cooked msg:
*)
value mk_final_msg   : string_map -> smtp_msg_cooked -> smtp_msg_final;


(*=====================*)
(*  Helper Functions:  *)
(*=====================*)
(* Priority encoding/decoding: *)

value priority_of_int: int -> msg_priority;
value int_of_priority: msg_priority -> int;

(*  The following "cook_*" functions are exported just in case: *)

(*  "cook_msg_struct":
    Arg1: strict mode flag
    Arg2: tags
    Arg3: struct to be cooked
*)
value cook_msg_struct: ~strict_mode:bool -> list string -> mime_msg_struct ->
                       list smtp_data;

(*  "cook_msg_part":
    Arg1: multi-part msg flag
    Arg2: strict mode flag
    Arg3: parts separator
    Arg4: tags
    Arg5: part to be cooked
*)
value cook_msg_part:  ~is_multi_part:bool  -> ~strict_mode:bool ->
                      smtp_data -> list string -> mime_msg_part ->
                      list smtp_data;

(*  "cook_part_body":
    Arg1: all-ASCII flag
    Arg2: strict mode flag
    Arg3: tags
    Arg4: body
*)
value cook_part_body: ~is_all_ascii:bool -> ~strict_mode :bool  ->
                      list string         -> mime_part_body     ->
                      list smtp_data;

(*  "cook_text_line":
    Arg1: strict mode flag
    Arg2: install "\r\n" line terminator (normally "True")
    Arg3: tags
    Arg4: line to be cooked
*)
value cook_text_line: ~strict_mode:bool   -> ?with_crlf:bool  ->
                      list string         -> string           ->
                      list smtp_data;

(*  Other Functions: *)

(*  "is_all_ascii":  *)
value is_all_ascii: mime_part_body -> bool;

(*  "is_text_mime":  *)
value is_text_mime: string -> bool;

(*  "extract_smtp_lines":
    Converts "smtp_data" into strings. Line terminators are removed from
    "smtp_data" elements:
*)
value extract_smtp_lines: list smtp_data -> list string;

(*  "smtp_time_stamp":  *)
value smtp_time_stamp: ?tz_off: option (int * int) -> float -> string;

