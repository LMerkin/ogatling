(*pp camlp4r *)
(* vim:ts=4:syntax=ocaml
*)
(*===========================================================================*)
(*                           "gatling_barrage.ml":                           *)
(*               Mailing List Distribution System, Top-Level                 *)
(*                 (C) Explar Technologies Ltd, 2003--2006                   *)
(*===========================================================================*)
(*------------------------*)
(*  Global Configuration: *)
(*------------------------*)
value invalid_name  = "";
value invalid_count = (-1);
value invalid_time  = (-1.0);

(*  NB: "exit_signalled" is used for both receiving signals, and signalling
    other processes ("--graceful-shutdown"):
*)
value exit_signalled= ref False;
value xml_conf_file = ref invalid_name;
value graceful_sig  = Sys.sigusr2;

(*---------------------*)
(* Command-Line Parms: *)
(*---------------------*)
value version_gen = "Gatling Mail 2.6";

value version_info: unit -> unit =
fun () ->
do {
    print_endline "\t\tGatling_Barrage version 2.6.1,  2006-08-27";
    print_endline "\t\t   (C) Explar Technologies Ltd, 2000-2006";
    flush stdout;
    Tnio.exit 0
};

value usage =
fun rc ->
do {
    print_endline
    "PARAMETERS: {--version | -- help | [--graceful-shutdown] XML_Config_File}";
    flush stdout;
    Tnio.exit rc
};

value n_args = Array.length Sys.argv;

if   n_args  = 2 && Sys.argv.(1) = "--help"
then usage 0          (* Print the help msg *)
else
if   n_args  = 2 && Sys.argv.(1) = "--version"
then version_info ()  (* Print version info *)
else
if   n_args   =2 || n_args = 3
then
do{
    (* The last arg should be the XML Conf File:  *)
    xml_conf_file.val := Sys.argv.(n_args-1);

    (* The previous arg could be the exit signal: *)
    if   n_args = 3
    then
        if Sys.argv.(1)  =  "--graceful_shutdown"
        then exit_signalled.val := True
        else usage 1
    else ()
}
else usage 1;

(*----------------------------*)
(*  Parse the XML Conf File:  *)
(*----------------------------*)
value ext_conf =
    try Barrage_config.consult_external_conf xml_conf_file.val
    with
    [ hmm ->
        Misc_utils.fatal_error
            (Printf.sprintf "Invalid XML Config File: \"%s\": %s"
            xml_conf_file.val (Misc_utils.print_exn hmm))
    ];

(*  Create the array of INTERNAL confs: *)
value  int_confs = Barrage_config.mk_internal_confs ext_conf;
assert (Array.length int_confs >= 1);

value  conf0 = int_confs.(0);

(*  The default settings for Client IPs and Domains in "int_confs" above
    may be inconsistent. Thus, check all the confs now:
*)
Array.iter
(fun conf ->
    if  conf.Barrage_config.verify_locals
    then
        let local_domains =
            Array.of_list conf.Barrage_config.local_domains
        in
        let local_ips =
            Array.of_list
            (List.map Unix.inet_addr_of_string conf.Barrage_config.local_ips)
        in
        try
            Smtp_client.verify_domains_ips local_domains local_ips
        with
        [ Failure hmm ->
            Misc_utils.fatal_error
            ("ERROR: Inconsistent Local IPs and Domains for Group "^
            conf.Barrage_config.from_group_name^": "^hmm)
        ]
    else
        () (* Skip verification *)
)
int_confs;

(*---------------------*)
(*  Shutdown Request?  *)
(*---------------------*)
(*  If "exit_signalled" was set, we will look for an already RUNNING instance
    of this program, and send it a graceful shutdown signal:
*)
if  exit_signalled.val
then
do{
    (* Open and read the PID file, if exists: *)
    let pidf_name = conf0.Barrage_config.pid_file in
    let pids_line =
        try Misc_utils.read_file pidf_name
        with
        [hmm -> Misc_utils.fatal_error
                (Printf.sprintf "Cannot read PID file: \"%s\": %s" pidf_name
                (Misc_utils.print_exn hmm))
        ]
    in
    (* The line on the PID File contains 2 or 1 PID(s). The one we need --
       the actual worker process -- is always the first one:
    *)
    let wpid =
        try List.hd (List.map int_of_string
                              (Misc_utils.split_by_whites pids_line))
        with
        [_ -> Misc_utils.fatal_error ("Malformed PID File: "^pids_line)]
    in
    (* Send it a graceful termination signal: *)
    try Unix.kill wpid graceful_sig
    with
    [hmm -> Misc_utils.fatal_error
            (Printf.sprintf "Cannot send a signal to process %d: %s"
            wpid (Misc_utils.print_exn hmm))
    ];
    (* If we  got here,  everything is done : *)
    Tnio.exit 0
}
else ();

(*---------------------------*)
(*  Intialise the TNIO RTS:  *)
(*---------------------------*)
(*  We currently use TWO priority levels: one for the worker threads, the
    other for the management interface:
*)
Tnio.init   ~io_events_mech: conf0.Barrage_config.tnio_events_mech
            ~rt_priority   : conf0.Barrage_config.use_rt_priority
            (Tnio.Priority_Bands 2);

(*----------------------*)
(*  Check the Licence:  *)
(*----------------------*)
(*  We use a default 15-sec time-out for secure Internet time requests: *)
(*  XXX: temporarily de-configured, as OpenSSL support was found to be
    non-thread-safe!
Enforcement.verify_licence
    conf0.Barrage_config.licence_file
    (fun () -> Secure_time.gmtime 15.0);
*)

(*----------------------*)
(*  Change Privileges:  *)
(*----------------------*)
value uname = conf0.Barrage_config.run_as_user;
value gname = conf0.Barrage_config.run_as_group;
try
    Misc_utils.run_as uname gname
with
    [hmm -> Misc_utils.fatal_error (Misc_utils.print_exn hmm)];

(*----------------------*)
(*  Open Output Files:  *)
(*----------------------*)
(* All output files are locked against attempts of multiple programs to use
   them. This guards against a common error when multiple instances of this
   program are launched on the same files.
   NB: This program does NOT run in the daemon mode,   so the locks are NOT
   lost after "fork"ing -- the original (parent) process (whivh will become
   the monitor) will still hold them:
*)
(* Errors and Info Files: *)

value errors_file  = conf0.Barrage_config.errors_file;
value info_file    = conf0.Barrage_config.info_file;

value mb_errors_fd =
    Misc_utils.open_write_locked
    ~append_mode:False ~lock_fail_rc:Domains_config.no_restart_rc errors_file;

value mb_info_fd   =
    Misc_utils.open_write_locked
    ~append_mode:False ~lock_fail_rc:Domains_config.no_restart_rc info_file;

(* Writing into these files -- if they exist: *)

value write_opt: option Unix.file_descr -> string -> unit =
fun   mb_fd line ->
    match mb_fd with
    [   Some fd  -> Misc_utils.write_fstr fd line
      | None     -> ()
    ];

value over_write_fd: Unix.file_descr -> string -> unit =
fun fd line ->
    do {
        ignore (Unix.lseek fd 0 Unix.SEEK_SET);
        Misc_utils.write_fstr fd line
    };

value close_opt: option Unix.file_descr -> unit =
fun   mb_fd ->
    match mb_fd with
    [   Some fd -> Misc_utils.safe_close fd
      | _       -> ()
    ];

value time_stamp:  unit -> string =
fun curr_time ->
    (*  The time is supposed to be local here:  *)
    let curr_time = Unix.localtime (Unix.time ())  in

    Printf.sprintf "%04d-%02d-%02d %02d:%02d:%02d"
                   (curr_time.Unix.tm_year + 1900)
                   (curr_time.Unix.tm_mon  + 1)
                    curr_time.Unix.tm_mday  curr_time.Unix.tm_hour
                    curr_time.Unix.tm_min   curr_time.Unix.tm_sec;

value log: string -> string -> unit =
fun  group lmsg   ->
    (* NB: log time stamp uses LOCAL time! *)
    let ts = time_stamp () in
    let log_line  =
        Printf.sprintf "%s %s (PID=%d): %s\n" ts group (Unix.getpid ()) lmsg 
    in
    try write_opt mb_info_fd log_line with [_ -> ()];

(*--------------------------*)
(*  Set the signal handler  *)
(*--------------------------*)
(*  This is for our own use. We will examine the flag frequently: *)
Tnio.set_sig_handler
    graceful_sig
    ~new_handler_pos: Tnio.Handler_Attach_After
    (Tnio.Sig_Handler_Gen (fun _ -> exit_signalled.val:= True));

(*---------------------------------*)
(* Initialise the Send Scheduler:  *)
(*---------------------------------*)
(* The domain statistics is printed into the Info file, for the domains which
   contain more than the "stats_threshold" of the total number of e-mails:
*)
value stats_thresh = conf0.Barrage_config.stats_threshold;

value info_ch = match mb_info_fd with
    [   None     -> None
      | Some ifd -> Some (Unix.out_channel_of_descr ifd)
    ];

(* "Send_scheduler_init" returns a DNS cache and the call-backs if they were
   constructed, or None.   This is a blocking operation, so examine the exit
   condition afterwards:
*)
value (mb_dns_cache, _, db_flds) =
try Send_scheduler.init
    ext_conf.Barrage_config.domain_groups
    int_confs
    info_ch
    stats_thresh
with
[ _   when exit_signalled.val -> (None, "", [])
| hmm ->
    Misc_utils.fatal_error
    ("Send_scheduler initialisation failed: "^(Misc_utils.print_exn hmm))
];

if  exit_signalled.val
then
do{
    (* Close all files: *)
    log "SENDER" "GRACEFUL EXIT SIGNAL RECEIVED, SHUTTING DOWN...";
    Send_scheduler.close_scheduler ();
    close_opt mb_errors_fd;
    close_opt mb_info_fd;
    Tnio.exit 0
}
else ();

(* No exit -- continue: *)

value n_threads = conf0.Barrage_config.n_threads;

if    n_threads > 9999
then Misc_utils.fatal_error
     ("Too many threads requested: "^(string_of_int n_threads))
else ();

(*------------------------*)
(*  Agents Configuration: *)
(*------------------------*)
(* SMTP Agent Config:
   We only set here those fields which are Globally-configurable;  the per-
   Group ones will be installed later on "live" SMTP Agents. Again, "conf0"
   already contains safe defaults from "Smtp_client":
*)
value smtp_config:
            Smtp_client.smtp_agent_config    =
            Smtp_client.default_smtp_agent_config ();

smtp_config.Smtp_client.conf_sock_buff_size :=
     conf0.Barrage_config.sock_buff_size;

(* Dry Run? *)
smtp_config.Smtp_client.conf_dry_run        :=
     conf0.Barrage_config.dry_run;

(* Ready-made resolvers: *)

(* Call-backs for the Scheduled resolver, if it's used: *)
value sched_call_backs =
{
    Smtp_client.reserve_ip = Send_scheduler.reserve_ip;
    Smtp_client.release_ip = Send_scheduler.release_ip
};

value resolver_strict      = Smtp_client.IP_Resolv_Strict
                            (Barrage_config.mk_dns_config conf0);
value resolver_pre_cached  = Smtp_client.IP_PreCached;
value resolver_scheduled   = Smtp_client.IP_Scheduled sched_call_backs;

smtp_config.Smtp_client.conf_ip_resolv_mode :=
    match mb_dns_cache with
    [ Some _ -> resolver_pre_cached  (* May be alternated later *)
    | None   -> resolver_strict
    ];

(*------------------------------*)
(* Now Create the SMTP Agents:  *)
(*------------------------------*)
(*  If the SMTP Traced Mode is requested, traces are written into the corresp
    "NNNN.tr" files:
*)
value parent_pid = Unix.getpid ();

value agent_stats:
    array Smtp_client.smtp_agent_stats_accum =
    Array.init
        n_threads
        (fun _ -> Smtp_client.empty_smtp_agent_stats_accum ());
    (* NB: BE CAREFUL: Don't use "Array.make" here, as it would put a single
       shared value into all elements!
    *)

<<<<<<< .mine
(*  The default settings for Client IPs and Domains in "agents" may be
    inconsistent -- the actual settings will be taken from "int_confs"
    at sending time. Thus, check all the confs now:
Array.iter
(fun conf ->
    if  conf.Barrage_config.verify_locals
    then
    try
        Smtp_client.verify_domains_ips
        (Array.of_list conf.Barrage_config.local_domains)
        (Array.of_list
            (List.map Unix.inet_addr_of_string conf.Barrage_config.local_ips))
    with
    [ Failure hmm ->
        Misc_utils.fatal_error
        ("ERROR: Inconsistent Local IPs and Domains for Group "^
        conf.Barrage_config.from_group_name^": "^hmm)
    ]
    else
        () (* Skip verification *)
)
int_confs;
*)

=======
>>>>>>> .r373
(*------------------------*)
(*  Prepare the Message:  *)
(*------------------------*)
(*  The "db_flds" directly correspond to the fields of the Mailing List File,
    and to the mgs tags (with the addition of "[[...]]").    Two if the flds
    ("EMail" and "Crypt") are always present, the rest are user-configurable.

    IMPORTANT: message content is configurable on the per-Group basis, so we
    have to prepare a different message for each of the "int_conf"s. The Msg
    ID contains dynamic information,  so it's given by an internal tag:
*)
value email_idx  = Gatling_db.email_fld_pos;
value crypt_idx  = Gatling_db.crypt_fld_pos;

value msg_id_tag = "[[MSG_ID]]";
value db_tags    = List.map (fun t -> "[["^t^"]]") db_flds;
value all_tags   = db_tags @ [msg_id_tag];
value email_tag  = List.nth db_tags email_idx;

(*  "msg_part_from_file":
    A wrapper around the similar "Mime_codec" function:
*)
value msg_part_from_file:
    Mime_xml_types.mime_part_disp -> string -> string ->
    Mime_xml_types.mime_msg_part  =

fun disp mime_type file_name ->
    try Mime_codec.msg_part_from_file disp mime_type file_name
    with
    [ hmm ->
          Misc_utils.fatal_error
                ("ERROR: Cannot read the file \""^ file_name^"\": "^
                (Misc_utils.print_exn hmm))
    ];

(*  "mk_msg_parts":
    Creates two msg parts (the TEXT one, if defined, comes FIRST) from the
    given files:
*)
value mk_msg_parts:  string -> string -> list Mime_xml_types.mime_msg_part =
fun msg_file_html msg_file_txt ->
    let parts_t =
        if  msg_file_html <> invalid_name
        then
            [(msg_part_from_file
              Mime_xml_types.Disp_Inline "text/html"  msg_file_html)]
        else
            []
    in
        if  msg_file_txt  <> invalid_name
        then
            [(msg_part_from_file
              Mime_xml_types.Disp_Inline "text/plain" msg_file_txt)
              :: parts_t]
        else
            parts_t;

(*  "msg_compositn":  *)

value mk_msg_compostn:
    list Mime_xml_types.mime_msg_part -> Mime_xml_types.mime_composition  =
fun 
[
    []  -> Mime_xml_types.SinglePart          (* In fact,  no parts at all! *)
  | [_] -> Mime_xml_types.SinglePart
  | _   -> Mime_xml_types.MultiPart_Alternative
           (* The parts are always assumed to be ALTERNATIVES to each other *)
];

(*  "mk_msg_cooked":
    Composes and "cookes" a message for a given "internal_conf". NB: GMT is
    to be used for all time calculations in SMTP:
*)
value curr_date_time  = Unix.time ();
value our_tz          = conf0.Barrage_config.tz_offset;

value mk_msg_cooked:
    Barrage_config.internal_conf -> Mime_xml_types.smtp_msg_cooked =

fun conf ->
    let to_templ   = conf.Barrage_config.to_templ in
    let dest_templ =
        if  to_templ = ""
        then
            (* There is no specific template for the "To:" field -- use a
               simple template consisting of just the E-Mail Tag:
            *)
            email_tag
        else
            (* If there is a template, it MUST include the E-Mail tag: *)
            if  ExtString.String.exists to_templ email_tag
            then
                to_templ
            else
                Misc_utils.fatal_error ("\"To_Templ\" for \"" ^
                    conf.Barrage_config.from_group_name ^
                    " does not actually provide an e-mail")
    in
    let msg_info =
    {
        Mime_xml_types.strict_lines =  conf.Barrage_config.strict_lines;
        Mime_xml_types.envel_from   =  conf.Barrage_config.envelope_from;
        Mime_xml_types.body_from    =  conf.Barrage_config.body_from;
        Mime_xml_types.msg_to       =  dest_templ;
        Mime_xml_types.subject      =  conf.Barrage_config.subject;
        Mime_xml_types.priority     =
            Mime_codec.priority_of_int conf.Barrage_config.priority;
        Mime_xml_types.msg_id       =  msg_id_tag;

        Mime_xml_types.date         =  Mime_codec.smtp_time_stamp
                                       ~tz_off:our_tz curr_date_time;

        Mime_xml_types.mailer_ver   =  version_gen;
        Mime_xml_types.tags         =  all_tags;
        Mime_xml_types.content      =
            let parts  = mk_msg_parts  conf.Barrage_config.msg_file_html
                                       conf.Barrage_config.msg_file_txt
            in
            {
                Mime_xml_types.composition = mk_msg_compostn parts;
                Mime_xml_types.parts       = parts
            }
    }
    in
    Mime_codec.cook_msg msg_info;


(*  Now "cook" messages for all configs. These messages will be shared between
    threads, but that's OK, since they are read-only.   The 0th element of the
    array constructed is for the Default config   (for automatically-generated
    Singleton Groups), the rest are for explicitly-configured Groups:
*)
value all_msgs =  Array.init
                 (Array.length int_confs)
                 (fun i -> mk_msg_cooked int_confs.(i));

(*-------------*)
(* Statistics: *)
(*-------------*)
value curr_av_stats: unit -> Smtp_client.smtp_agent_stats =
fun () ->
    (* Here "total" means "total for all threads": *)
    let total_transacts = ref 0   in
    let ok_transacts    = ref 0   in
    let reused_conns    = ref 0   in
    let total_dns_time  = ref 0.0 in
    let total_trans_time= ref 0.0 in
    do {
        for i = 0 to n_threads - 1
        do {
            let stats_i = Smtp_client.get_smtp_agent_stats agent_stats.(i) in
            let tf_tr   = float_of_int
                                stats_i.Smtp_client.total_transacts        in
            do {
            total_transacts.val := total_transacts.val +
                                   stats_i.Smtp_client.total_transacts;
            ok_transacts.val    := ok_transacts.val    +
                                   stats_i.Smtp_client.ok_transacts;

            reused_conns.val    := reused_conns.val    +
                                   stats_i.Smtp_client.reused_conns;

            total_dns_time.val  := total_dns_time.val  +.
                                   stats_i.Smtp_client.av_dns_time_sec  *.tf_tr;
            total_trans_time.val:= total_trans_time.val+.
                                   stats_i.Smtp_client.av_trans_time_sec*.tf_tr
            }
        };
        (* Return the "smtp_stats" record averaged over all Agents: *)
        let tf_tr   = float_of_int total_transacts.val in
        let av_time = fun t ->
            if  tf_tr <> 0.0 then t /. tf_tr else 0.0
        in
        {
            Smtp_client.total_transacts  = total_transacts.val;  (* No aver. *)
            Smtp_client.ok_transacts     = ok_transacts.val;     (* No aver. *)
            Smtp_client.reused_conns     = reused_conns.val;     (* No aver. *)

            Smtp_client.succ_rate        =
                if  tf_tr <> 0.0
                then (float_of_int ok_transacts.val) /. tf_tr else 0.0;

            Smtp_client.reuse_rate       =
                if  tf_tr <> 0.0
                then (float_of_int reused_conns.val) /. tf_tr else 0.0;

            Smtp_client.av_dns_time_sec  = av_time total_dns_time.val;
            Smtp_client.av_trans_time_sec= av_time total_trans_time.val
        }
    };

(* Printing statistics into the Info file, if it exists: *)

value print_progr_stats: unit -> unit =
fun () ->
    let progr = Send_scheduler.get_progress_stats () in
    List.iter
    (fun (gr_name, pert_emails, done_emails, succ_emails, _) ->
        let qgroup = "\""^gr_name^"\"" in
        let msg    =
        if  pert_emails = 0
        then
            Printf.sprintf "Group %20s: NOTHING TO DO\n" qgroup
        else
        if  done_emails = 0
        then
            Printf.sprintf "Group %20s: NOT (YET) STARTED\n" qgroup
        else
        if  done_emails = pert_emails
        then
            Printf.sprintf "Group %20s: FINISHED\n" qgroup
        else
            (Printf.sprintf
                "Group %20s: %d of %d e-mails done (%.1f%%); "
                qgroup done_emails pert_emails
                ((float_of_int done_emails) /.
                 (float_of_int pert_emails) *. 100.0)
            ) ^
            (Printf.sprintf
                "succ.rate %.1f%%\n"
                (if  done_emails = 0
                 then 0.0
                 else (float_of_int succ_emails) /.
                      (float_of_int done_emails) *. 100.0
                )
            )
        in
        write_opt mb_info_fd msg
    )
    progr;


value print_curr_av_stats: float -> unit =
fun start_time ->
    let stats = curr_av_stats ()                     in
    do {
        write_opt mb_info_fd (Printf.sprintf
        "Total Transactions: %d\n"      stats.Smtp_client.total_transacts);

        write_opt mb_info_fd (Printf.sprintf
        "Succ  Transactions: %d\n"      stats.Smtp_client.ok_transacts);

        write_opt mb_info_fd (Printf.sprintf
        "Success Rate      : %.2f %%\n" (stats.Smtp_client.succ_rate *. 100.0));

        write_opt mb_info_fd (Printf.sprintf
        "Connections Re-Use: %.2f %%\n" (stats.Smtp_client.reuse_rate*. 100.0));

        write_opt mb_info_fd (Printf.sprintf
        "Av DNS   Time, sec: %.2f\n"    stats.Smtp_client.av_dns_time_sec);

        write_opt mb_info_fd (Printf.sprintf
        "Av Trans Time, sec: %.2f\n"    stats.Smtp_client.av_trans_time_sec);

        write_opt mb_info_fd (Printf.sprintf
        "Av  e-mails / hour: %d\n"
            (int_of_float ((float_of_int stats.Smtp_client.total_transacts) /.
                          ((Unix.gettimeofday ()) -. start_time) *. 3600.0)
            ));

        (* It would also be nice to print the number of live threads and the
           CPU utilisation:
        *)
        write_opt mb_info_fd (Printf.sprintf
        "Live Threads      : %d\n"        (Tnio.threads_count       ()));

        write_opt mb_info_fd (Printf.sprintf
        "Av CPU utilisation: %.1f %%\n\n" (Tnio.get_cpu_utilisation ()));

        (* In addition, display the Progress Stats. XXX: we currently do NOT
           display detailed per-domain information in Groups:
        *)
        print_progr_stats ();

        write_opt mb_info_fd "\n"
    };

(*-----------------------------*)
(*  "reconfigure_smtp_agent":  *)
(*-----------------------------*)
(* Are all load limits Absolute? *)
value all_abs_limits =
    List.for_all
    (fun conf ->
        match conf.Barrage_config.group_threads_limit with
        [ Barrage_config.Absolute _ -> True
        | _                         -> False
        ]
    )
    (Array.to_list int_confs);

(* Now the actual reconfigurator: *)

value reconfigure_smtp_agent:
    Smtp_client.smtp_agent -> Barrage_config.internal_conf -> unit =

fun agent conf ->
    let smtp_conf = Smtp_client.config_of_smtp_agent agent in
    do {
        smtp_conf.Smtp_client.conf_client_ips       :=
            Array.of_list
            (List.map
            Unix.inet_addr_of_string conf.Barrage_config.local_ips);

        smtp_conf.Smtp_client.conf_client_domains   :=
            (Array.of_list conf.Barrage_config.local_domains);

        smtp_conf.Smtp_client.conf_use_persistence  :=
            conf.Barrage_config.use_persistence;

        smtp_conf.Smtp_client.conf_use_pipelining   :=
            conf.Barrage_config.use_pipelining;

        smtp_conf.Smtp_client.conf_attempt_timeout_sec  :=
            conf.Barrage_config.conn_send_timeout;

        smtp_conf.Smtp_client.conf_n_retries        :=
            conf.Barrage_config.conn_send_retries;

        (* Do the actual re-configuration -- don't touch the resolver yet: *)
        Smtp_client.reconfigure_smtp_agent agent smtp_conf;

        (* Now decide whether we need to change the resolver: *)

        match mb_dns_cache with
        [ None   ->
            (* No DNS Cache, hence the default Strict resolver is always used.
               Don't change anything:
            *)
            ()
        | Some _ ->
            if  all_abs_limits
            then
                (* DNS Cache exists, but all limits are absolute -- the deflt
                   PreCached resolver is always used, don't change it:
                *)
                ()
            else
                (* Yes, it might be a PreCached or Scheduled resolver: *)
                let new_resolv_mode =
                    match conf.Barrage_config.group_threads_limit with
                    [ Barrage_config.Absolute _        ->
                        resolver_pre_cached

                    | Barrage_config.Per_Server_IP _ _ ->
                        resolver_scheduled
                    ]
                in
                Smtp_client.reconfigure_resolver agent new_resolv_mode
        ]
    };

(*----------------*)
(*  "mk_msg_id":  *)
(*----------------*)
(*  Dynamic creation of ID of a msg to be sent: *)

value mk_msg_id:
Barrage_config.internal_conf -> Send_scheduler.sending_job -> string =

fun conf sj ->
    let crypt    = sj.Send_scheduler.sj_ml_rec.(crypt_idx) in
    let from_dom = Misc_utils.get_email_domain (conf.Barrage_config.body_from)
    in
    let buff     = Buffer.create 100 in
    do {
        (* Convert the Crypt into Hex representation: *)
        String.iter
        (fun c ->
            Buffer.add_string buff (Printf.sprintf "%0x" (int_of_char c))
        )
        crypt;
        (* The resulting ID: *)
        Printf.sprintf "<%s@%s>" (Buffer.contents buff) from_dom
    };

(*===========================*)
(*  The Sender Thread Body:  *)
(*===========================*)
value ml_count       = ref 0;
value stats_interval = conf0.Barrage_config.stats_interval;
value start_time     = ref 0.0;
(*  Graceful exit by a signal (SIGUSR2) requested? *)

(*----------------*)
(* "sender_loop": *)
(*----------------*)
(*  We MUST GUARANTEE that this function will be optimised as tail-recursive.
    If not, we can run out of stack very soon!
*)
value rec sender_loop:
    int                     ->
    Smtp_client.smtp_agent  ->
    option (Send_scheduler.sending_job * int * Send_scheduler.sched_info) ->
    unit =

fun i agent mb_job ->
    match mb_job with
    [ (* "Normal" case -- got a next job to do, no exit signal: *)
      Some (curr_job, prev_cid, next_sch_info) when not exit_signalled.val   ->

      let cid  = curr_job.Send_scheduler.sj_conf_id in
      let conf = int_confs.(cid) in
      do {
        try
        do {
            (* Maybe it's time to print statistics? *)
            ml_count.val := ml_count.val + 1;

            if (stats_interval > 0) && (ml_count.val mod stats_interval = 0)
            then
                try print_curr_av_stats start_time.val
                with [_ -> ()]
                (* Can also do GC here, but removed for run-time efficiency *)
            else
                ();

            (* Fill in the Tags Map. All tags except the "msg_id_tag" (last
               one) are external,  and their values are taken from the corr
               record fields. The "msg_id_tag" is synthetic:
            *)
            let tags_map = Hashtbl.create 100 in
            let ()       =
            do {
                ignore
                (List.fold_left
                    (fun j tag ->
                    do {
                        Hashtbl.add tags_map
                                    tag  curr_job.Send_scheduler.sj_ml_rec.(j);
                        j+1
                    })
                    0 db_tags);

                Hashtbl.add tags_map msg_id_tag (mk_msg_id conf curr_job);

                (* Make the local Agent modifications according to the new job.
                   The Agent  actually needs to be re-configured   only if the
                   Domain Group Config is changing:
                *)
                if   cid <> prev_cid
                then reconfigure_smtp_agent agent conf
                else ();

                (* Set the IPs we have received from the Scheduler -- they will
                   be ignored if Agent's resolver is not in the  PreCached  or
                   Scheduled mode  (that is, they are not needed if we have no
                   DNS cache -- but setting them is harmless in any case):
                *)
                Smtp_client.set_scheduled_ips agent
                                curr_job.Send_scheduler.sj_use_ips
            }
            in
            (* Make the FINAL Msg: *)
            let fmsg = Mime_codec.mk_final_msg tags_map all_msgs.(cid)  in

            (* Send it up!  *)
            let ()   = Smtp_client.send_final_msg agent agent_stats.(i) fmsg
            in
            (* If we got here, the send was successful: *)
            Send_scheduler.report_send_result next_sch_info
                Send_scheduler.Success
        }
        with
        [ any ->
          do {
            (* Any unexpected exception  AFTER  the Mailing List entry has
               been obtained (so not End_of_file):    Log it in the Errors
               file, but don't exit:
            *)
            try
                write_opt mb_errors_fd
                    (Printf.sprintf "%s\nThread %d: %s\n"
                        (Send_scheduler.reconstr_ml_rec
                            curr_job.Send_scheduler.sj_ml_rec)
                        i (Misc_utils.print_exn any)
                    )
            with [_ -> ()];

            (* Report failure, but which exactly? *)
            let failure = match any with

            [   Smtp_client.SMTP_Permanent_Error _ ->
                if  conf.Barrage_config.strict_5xx_mode
                then
                    Send_scheduler.Failed_Strict_5XX
                else
                    Send_scheduler.Failed_Purge_It

            |   Smtp_client.SMTP_Error_Body_Sent _ ->
                    Send_scheduler.Failed_No_ReSend

            |   _-> Send_scheduler.Failed_Try_ReSend
            ]
            in
            Send_scheduler.report_send_result next_sch_info failure
        } ];

        (* Tail-recursive call with the Next Job. Curr IP and Domain
           are communicated back to the Send Scheduler so it can de-
           cide whether the new IP would need to be reserved:
        *)
        match Send_scheduler.get_next_job
              next_sch_info
              (Smtp_client.get_conn_domain agent)
              (Smtp_client.get_resv_ip     agent)
        with
        [ None ->
            (* Propagate it, to guarantee tail-recursive optimisation: *)
            sender_loop i agent None

        | Some (nj, nnsi) ->
            sender_loop i agent (Some (nj, cid, nnsi))
        ]
      }

    | _ ->
        (* No next job, or stopped by a signal. Terminate the thread. Close
           the Agent, but do NOT re-set its stats yet:
        *)
        Smtp_client.close_smtp_agent agent
    ];

(*-------------------*)
(*  "sender_start":  *)
(*-------------------*)
(*  This is an initialisation function of the sender thread.  It creates the
    in-thread SMTP Agent -- all the sockets MUST be opened within the thread,
    not outside:
*)
value sender_start:
    int                     ->
    option (Send_scheduler.sending_job * int * Send_scheduler.sched_info) ->
    unit =

fun i first_job ->
    (* Get the Agent Config: *)
    let conf_i =
        (* Open the trace file if appropriate. It is NOT to be locked, as the
           trace files are indexed with the PID of the creating process:
        *)
        if   conf0.Barrage_config.trace_dialogue
        then
            let mb_trace_fd =
                Misc_utils.open_write_locked
                    ~append_mode :False
                    ~lock_fail_rc:Domains_config.no_restart_rc
                    (Printf.sprintf "%04d-%d.tr" i parent_pid)
            in
            {(smtp_config) with Smtp_client.conf_trace_file=mb_trace_fd}
        else
            smtp_config
    in
    (* Create the SMTP Agent, incl all TNIO Sockets: *)
    let agent =
        try Smtp_client.mk_smtp_agent conf_i
        with
        [any -> Misc_utils.fatal_error ("ERROR: "^Misc_utils.print_exn any)]
    in
    (* Enter the Sender Loop: *)
    sender_loop i agent first_job;


(*===================*)
(*  Sender Process:  *)
(*===================*)
(*------------------------*)
(*  "print_sched_stats":  *)
(*------------------------*)
(*  Outputs post-send Send Scheduler's statistics for all domains above the
    threshold, sorted by success rate, into the Info file (if exists):
*)
value sched_post_proc: bool -> unit =
fun last_round ->
    (* If we don't plan to print statistics, set the threshold above 1,
       so it would not be collected at all:
    *)
    let (total_tried, stats_arr) =
        Send_scheduler.post_send_proc
            ~last_round  : last_round
            ~stats_thresh: stats_thresh
    in
    match  info_ch with
    [ None ->
        ()
    | Some  inch ->
        let float_tried = float_of_int total_tried  in
        if  float_tried > 0.0
        then
        do {
            Printf.fprintf inch "\n\n%s%f%%\" %s:\n%18s ==> %s %s\n\n"
                "POST-SEND STATISTICS FOR \"OVER-"
                (stats_thresh *. 100.0)  "DOMAINS"
                "Domain"
                "(Success,  Resend, Failed, PermFail, InProgr, Untouched)"
                "SuccRate";

            Array.iter
            (fun ds ->
                Printf.fprintf inch
                    "%18s ==> (%7d, %7d, %7d, %7d, %7d, %7d)\t%.1f%%\n"
                              ds.Send_scheduler.ds_domain_name
                              ds.Send_scheduler.ds_success
                              ds.Send_scheduler.ds_resend
                              ds.Send_scheduler.ds_failed
                              ds.Send_scheduler.ds_perm_failed
                              ds.Send_scheduler.ds_in_progr
                              ds.Send_scheduler.ds_not_yet
                              ds.Send_scheduler.ds_succ_rate
            )
            stats_arr;

            Printf.fprintf inch "\n\n";
            flush inch
        }
        else ()
    ];

(*------------------------*)
(*  Sender Process Body:  *)
(*------------------------*)
(*  Can perform multiple sending rounds. In the Monitored mode, this function
    (and the threads it creates)  is performed by a child process,  hence the
    name:
*)
value stack_size_k = conf0.Barrage_config.thread_stack_k;

value sender_proc: unit -> unit =
fun   () ->
try
do {
    (* Do the Rounds. It might be that the program is re-starting, so the
       initial round (memoised by the Send Scheduler) is already > 1:
    *)
    let n_rounds   = conf0.Barrage_config.n_rounds    in
    let init_round = Send_scheduler.get_round_no   () in

    for round = init_round to n_rounds
    do {
        (* Do not do any more rounds if terminated by a signal: *)
        if  exit_signalled.val
        then ()
        else
        (* Start the Round: *)

        log "SENDER" (Printf.sprintf "STARTING ROUND %d\n" round);
        Gc.compact ();

        Send_scheduler.update_round_no round;

        let init_scheds = Send_scheduler.init_sched_infos () in

        (* Create the sending threads, UNLESS only 1 thread is requested: *)
        if  n_threads >= 2
        then
        do {
            log "SENDER"
                (Printf.sprintf "Creating %d SMTP Threads\n" n_threads);

            for i = 0 to (n_threads-1)
            do {
                (* The initial thread arg. Note: initial "prev_conf_id" is
                   invalid (-1):
                *)
                let arg_i = match init_scheds.(i)  with
                [ None ->
                    None
                | Some (job, next_sch) ->
                    Some (job, -1, next_sch)
                ]
                in
                try
                    (* Actually create the thread: *)
                    ignore (Tnio.thread_create
                            ~stack_size_k:stack_size_k ~priority:0
                            (fun () -> sender_start i arg_i))
                with
                [ hmm ->
                    (* Thread creation error: BAD: *)
                    let msg = Printf.sprintf
                              "Thread %04d creation ERROR: %s\n"
                               i (Misc_utils.print_exn hmm)
                    in
                    do {
                        log "SENDER" msg;
                        Misc_utils.fatal_error msg
                    }
                ]
            };
            log "SENDER" "SMTP Threads starting\n";

            (* Now, enter the Threads Scheduler and wait for all threads to
               terminate. "Start_time" is per round, and starts here. In an
               "exit_signalled" is got  while running the threads  (so that
               it terminates "threads_scheduler" via an exception (e.g., in-
               terrupred syscall) -- threat this condition gracefully,  and
               still enable full post-processing:
            *)
            start_time.val := Unix.gettimeofday ();
            try
                Tnio.threads_scheduler  ()
            with
                [_ when exit_signalled.val   -> ()]
        }
        else
        do {
            (* Do everything in the single thread: *)
            log "SENDER" "Starting the Single Thread...";

            match init_scheds.(0) with
            [ None ->
                ()
            | Some (job, next_sch) ->
                sender_start 0 (Some (job, -1, next_sch))
            ]
        };
        (* When we get here, all threads have terminated: *)
        Gc.compact ();
        let exit_msg1 =
            if  exit_signalled.val
            then "Interrupted By A Signal"
            else "Done"
        in
        log "SENDER" ("Sending "^exit_msg1^", Post-Processing the Results...");

        (* At the end of the round, print the final statistics: *)
        print_curr_av_stats  start_time.val;

        (* Also process the Send Scheduler's Direct Access File. A flag is
           passed indicating whether it's the last round   (also if exited
           via a signal):
        *)
        sched_post_proc (round = n_rounds || exit_signalled.val);

        (* Re-set the Agents to get next round's statistics right -- do it
           ONLY after all threads have terminated,  or "mutual" statistics
           will be incorrect!
        *)
        for i = 0 to n_threads - 1
        do {
            agent_stats.(i) := Smtp_client.empty_smtp_agent_stats_accum ()
        };
        let exit_msg2 =
            if  exit_signalled.val
            then "TERMINATED GRACEFULLY BY A SIGNAL"
            else "COMPLETED"
        in
        log "SENDER" (Printf.sprintf "ROUND %d %s\n" round exit_msg2)
    };

    (* All sending rounds done -- close the Scheduler: *)
    Send_scheduler.close_scheduler ();

    log "SENDER" "ALL DONE, TERMINATING...";
    close_opt mb_errors_fd;
    close_opt mb_info_fd
}
with
[ (* Again, an exception at this level might be caused by the graceful termi-
     nation signal -- if so, it's no error:
  *)
  _ when exit_signalled.val ->
    log "SENDER" "TERMINATED GRACEFULLY (almost) BY A SIGNAL"

| hmm ->
  (* Any other exception: *)
  do {
    log "SENDER" ("Top-Level Exception: "^(Misc_utils.print_exn hmm));
    Tnio.exit 2
  }
];

(*----------*)
(*  FIRE!!! *)
(*----------*)
value minfo =
{
    Monitor.last_activity     = Send_scheduler.last_activity;
    Monitor.infinite_proc     = False;
    Monitor.max_blocking_time = conf0.Barrage_config.max_blocking_time;
    Monitor.log_heartbeats    = False
};

value rinfo =
{
    Monitor.init_phase        = fun _ -> (fun _ -> ()); (* No clean-up *)
    Monitor.oper_phase        = sender_proc;
    Monitor.daemon_mode       = False;
    Monitor.with_monitor      = if  conf0.Barrage_config.with_monitor
                                then Some minfo
                                else None;
    Monitor.severe_error      = Domains_config.no_restart_rc;
    Monitor.exn_error         = 1;      (* For example    *)
    Monitor.pid_file          = conf0.Barrage_config.pid_file;
    Monitor.run_as_user       = uname;  (* Already there! *)
    Monitor.run_as_group      = gname;  (* Already there! *)
    Monitor.log               = log "MONITOR"
    (* NB:  no locking of the log file is done between the Sender and the
            Monitor processes. This is deliberate, in order not to intro-
            duce yet another potential deadlock condition  -- it's better
            just to risk a minor mess-up of the log file!
    *)
};

Monitor.run_process rinfo;

