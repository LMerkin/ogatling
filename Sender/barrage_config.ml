(*pp camlp4r pa_ioXML.cmo *)
(* vim:ts=4:syntax=ocaml
*)
(*===========================================================================*)
(*                           "barrage_config.ml":                            *)
(*     Configuration types for "gatling_barrage", with XML-based I/O:        *)
(*                (C) Explar Technologies Ltd, 2003--2006                    *)
(*===========================================================================*)
exception Config_Error of string;

(*==================*)
(*  External View:  *)
(*==================*)
(*  These types are used in external XML config files;  to avoid unnecessarily
    repeated data in those files, the data are organised as LISTS (rather than
    records) or parms, all of which are syntactically optional.
*)

(*  The following config parms can be specified on the per-Domain-Group basis,
    as well as globally. Some of them are optional, other mandatory, yet other
    depend on each other's combination, so it's not possible to say here which
    of them are optional or otherwise:
*)
type domain_threads_limit =
[
    Absolute      of int
|   Per_Server_IP of int and int  (* Soft, Hard *)
];

type group_parm =
[
    (* (E)SMTP Configuration: *)
    Envelope_From       of string
|   Priority            of int
|   Use_Persistence     of bool
|   Use_Pipelining      of bool
|   Local_IPs           of list string
|   Local_Domains       of list string
|   Verify_Locals       of bool
|   Strict_Lines        of bool
|   Strict_5XX_Mode     of bool

    (* Content Configuration. The "To" field, if specified, would typically be
       template, as the destination obviously needs to be modified dynamically:
    *)
|   Body_From           of string
|   To_Templ            of string
|   Subject             of string
|   Msg_File_HTML       of string
|   Msg_File_Txt        of string

    (* TCP/IP  Configuration: *)
|   ConnSend_TimeOut    of float
|   ConnSend_ReTries    of int

    (* Running Configuration: *)
|   Group_Threads_Limit of domain_threads_limit
];

(*  Mailing List Source: can be a File or a DB: *)

type mlist_db_config   =
{
    (* DB Parms: *)
    access_parms        : Db_types.db_access;
    buff_file_name      : string;       (* MList text file, "" if temporary *)
    list_name           : string;
    extra_fields        : list string;  (* From the "ExtraData Table";
                                           to follow EMail and Crypt
                                        *)
    extra_conds         : string;       (* Extra conditions for selecting
                                           list entries to be used
                                        *)
    (* List Pre-Processing:
       "unload_db":       If "False", the mailer will try to use an
                          existing "buff_file" and/or the Direct Access
                          File;

       "remove_entries":  name of a file which provides a list of "EMail"s
                          and/or "Crypt"s to be explicitly removed from the
                          DB BEFORE a sending session; if the file name is "",
                          no removing is done; this option HAS NO EFFECT if
                          "unload_db" is "False". If an entry to be removed
                          is an EMail (as opposed to a Crypt), it is only re-
                          moved from the above "list_name", not the whole DB;

       "reset_succ":      for all entries in this List which have no failures
                          in the last "n" sessions ("n" is the arg), re-set
                          the "ConsFails" and "LastFail" counters;  no action
                          if n <= 0;

       "sweep_failed":    remove entries from  the DB with  "ConsFails" >=
                          than the value of this parm;  if the value is <= 0,
                          no sweeping is done:
    *)
    unload_db           : bool;
    remove_entries      : string;
    reset_succ          : int;
    sweep_failed        : int;

    (* Marking of Bounced Addresses: *)
    mark_bounced        : bool;
    bounces_file        : string;
    reset_bounces_file  : bool;

    (* List Post-Processing:
       "mark_failed" :    immediate sending errors are to be marked back
                          in the DB by setting/incrementing "ConsFails";

       "remove_perm_fails":
                          remove e-mails marked as "Permanent Failures"  from
                          the DB immediately, rather than marking them in the
                          normal way:
    *)
    mark_failed         : bool;
    remove_perm_fails   : bool
};

(*  The following config parms can only be specified globally. Again, we can't
    say here which of them are optional or mandatory:
*)
type global_parm  =
[
    (* Files Configuration: *)
    Licence_File        of string
|   DirAcc_File         of string
|   Info_File           of string
|   Errors_File         of string
|   PID_File            of string

    (* Mailing List Configuration: *)
|   MList_Src           of mlist_db_config

    (* Running Configuration:  *)
|   N_Rounds            of int
|   N_Threads           of int
|   DNS_Threads         of int
|   DNS_Rounds          of int
|   Thread_Stack_K      of int
|   With_Monitor        of bool
|   Max_Blocking_Time   of float
|   Stats_Interval      of int
|   Stats_Threshold     of float
|   Dry_Run             of float   (* Delay in seconds *)
|   Trace_Dialogue      of bool

    (* Low-Level Mechanisms and Security: *)
|   TNIO_Events_Mech    of Tnio_mechs.io_events_mech
|   Run_As_User         of string
|   Run_As_Group        of string
|   Use_RT_Priority     of bool

    (* DNS Configuration: *)
|   DNS_Servers         of list string
|   DNS_Cache_Size      of int
|   DNS_TimeOut         of float
|   DNS_ReTries         of int  (* NB: These are LOW-LEVEL retries *)

    (* TCP/IP Configuration: *)
|   Sock_Buff_Size_K    of int

    (* Our time zone: *)
|   TZ_Offset           of int and int    (* Hrs, Mins *)
];


(*------------------------*)
(*  "domain_group_conf":  *)
(*------------------------*)
(*  The domains which do not fall into any other (explicitly-configured) Group
    will be encapsulated  into their own  Single-Domain Groups   (NOT into one
    large Group!) with default settings:
*)
type domain_group_conf =
{
    group_name      : string;
        (* For user's convenience only *)

    selector_regexps: list string;
        (* The RegExps are in the SHELL (GLOB) format, and are ORed before
           being applied to domains.
        *)

    group_parms     : list group_parm
        (* Config parms for this Group, overriding global defaults *)
};

(*----------------------------*)
(*  "barrage_external_conf":  *)
(*----------------------------*)
(*  Mailing-List related configuration parms: *)
type barrage_external_conf =
{
    (* Global Parms: *)
    global_parms       : list global_parm;

    (* Default Per-Group Parms: *)
    default_group_parms: list group_parm;

    (* Explicitly-Configured Groups: *)
    domain_groups      : list domain_group_conf
};

(*----------------------------*)
(*  "consult_external_conf":  *)
(*----------------------------*)
value consult_external_conf: string -> barrage_external_conf =

fun conf_xml_file ->
    let cnnl = open_in conf_xml_file  in
    try
        let strm  = Stream.of_channel cnnl    in
        let xstr  = IoXML.parse_xml   strm    in
        let res   = xparse_barrage_external_conf xstr in
        do {
            close_in cnnl;
            res
        }
    with
    [   hmm ->
        do {
            close_in cnnl;
            raise hmm
        }
    ];

(*==================*)
(*  Internal View:  *)
(*==================*)
(*  This is a RECORD representation of the config data. All global and per-
    group parms come together, and are directly accessible.    Fields which 
    have not actually been set, hold default or invalid values.    Multiple
    record configs can be constructed (e.g. for different domain Groups).
    XXX: we don't really need XML representation for this type, but it will
    be constructed automatically anyway:
*)
(*--------------------*)
(*  "internal_conf":  *)
(*--------------------*)
type internal_conf =
{
    (* "from_group_name": it is convenient to keep it: *)
    from_group_name     : mutable string;

    (* (E)SMTP Configuration: *)
    envelope_from       : mutable string;
    priority            : mutable int;
    use_persistence     : mutable bool;
    use_pipelining      : mutable bool;
    local_ips           : mutable list string;
    verify_locals       : mutable bool;
    local_domains       : mutable list string;
    strict_lines        : mutable bool;
    strict_5xx_mode     : mutable bool;

    (* Content Configuration: *)
    body_from           : mutable string;
    to_templ            : mutable string;
    subject             : mutable string;
    msg_file_html       : mutable string;
    msg_file_txt        : mutable string;

    (* Files   Configuration: *)
    licence_file        : mutable string;
    dir_acc_file        : mutable string;
    info_file           : mutable string;
    errors_file         : mutable string;
    pid_file            : mutable string;

    (* Mailing List Source Configuration: *)
    mlist_src           : mutable mlist_db_config;

    (* Running Configuration: *)
    n_rounds            : mutable int;
    n_threads           : mutable int;
    dns_threads         : mutable int;
    dns_rounds          : mutable int;
    thread_stack_k      : mutable int;
    group_threads_limit : mutable domain_threads_limit;
    with_monitor        : mutable bool;
    max_blocking_time   : mutable float;
    stats_interval      : mutable int;
    stats_threshold     : mutable float;
    dry_run             : mutable option float;
    trace_dialogue      : mutable bool;

    (* Low-Level Mechanisms and Security: *)
    tnio_events_mech    : mutable Tnio_mechs.io_events_mech;
    run_as_user         : mutable string;
    run_as_group        : mutable string;
    use_rt_priority     : mutable bool;

    (* DNS    Configuration: *)
    dns_servers         : mutable list string;
    dns_cache_size      : mutable int;
    dns_timeout         : mutable float;
    dns_retries         : mutable int;

    (* TCP/IP Configuration: *)
    conn_send_timeout   : mutable float;
    conn_send_retries   : mutable int;
    sock_buff_size      : mutable int;

    (* Sender's Time Zone:   *)
    tz_offset           : mutable option (int * int)
};

(*--------------------------*)
(*  "empty_internal_conf":  *)
(*--------------------------*)
(*  We don't call it "default" conf, since many fields do not have reasonable
    default values, and are initialised to invalid values.
    NB: for "Dns_client" and "Smtp_client"-related fields,  we  propagate the
    defaults back from those modules, for the sake of consistency.
    NB: this is a function, not a constant value,  for  the value is mutable,
    and we must create different copies of it every time:
*)
value empty_internal_conf: unit -> internal_conf =
fun () ->
    let dns_default  = Dns_client.default_dns_agent_config   () in
    let smtp_default = Smtp_client.default_smtp_agent_config () in
    {
        from_group_name     = "";

        (* (E)SMTP Configuration. By default, the local IP and Domain to bind
           to (and to report in HELO/EHLO) are taken back from the SMTP Agent
           default config:
        *)
        envelope_from       = "";
        priority            = 3;  (* Normal *)
        use_persistence     = smtp_default.Smtp_client.conf_use_persistence;
        use_pipelining      = smtp_default.Smtp_client.conf_use_pipelining;
        strict_lines        = True;
        strict_5xx_mode     = False;

        local_ips           =
            List.map (fun ip -> Unix.string_of_inet_addr ip)
                     (Array.to_list smtp_default.Smtp_client.conf_client_ips);

        local_domains       =
            Array.to_list (smtp_default.Smtp_client.conf_client_domains);

        verify_locals       = True;

        (* Content Configuration: *)
        body_from           = "";
        subject             = "";
        to_templ            = "";
        msg_file_html       = "";
        msg_file_txt        = "";

        (* Files   Configuration: *)
        licence_file        = "";
        dir_acc_file        = "";
        info_file           = "";
        errors_file         = "";
        pid_file            = "";

        (* Mailing List Source Configuration: *)
        mlist_src           =
        {   (* The following default values are clearly invalid: *)
            access_parms    =
            {
                Db_types.dsid   = "";
                Db_types.user   = "";
                Db_types.passwd = ""
            };
            buff_file_name    = "";
            list_name         = "";
            extra_fields      = [];
            extra_conds       = "";
            unload_db         = False;
            remove_entries    = "";
            reset_succ        = 0;
            sweep_failed      = 0;
            mark_bounced      = False;
            bounces_file      = "";
            reset_bounces_file= False;
            mark_failed       = False;
            remove_perm_fails = False
        };

        (* Running  Configuration: *)
        n_rounds            = 1;
        n_threads           = 1;
        dns_threads         = 0;
        dns_rounds          = 1;     (* If used at all *)
        thread_stack_k      = 256;
        group_threads_limit = Per_Server_IP 2 3;
        with_monitor        = True;
        max_blocking_time   = 300.0; (* 5 minutes *)
        stats_interval      = 0;     (* Disabled  *)
        stats_threshold     = 0.01;  (* No statistics for bottom 1% domains *)
        dry_run             = None;
        trace_dialogue      = False;

        (* Low-Level Mechanisms and Security: *)
        tnio_events_mech    = Tnio_mechs.IO_Events_Default;
        run_as_user         = "nobody";
        run_as_group        = "nogroup";
        use_rt_priority     = False;

        (* DNS    Configuration: *)
        dns_servers         = [];    (* Don't use any local servers *)
        dns_cache_size      = dns_default.Dns_client.conf_cache_size;
        dns_timeout         = dns_default.Dns_client.conf_timeout_sec;
        dns_retries         = dns_default.Dns_client.conf_n_retries;

        (* TCP/IP Configuration: *)
        conn_send_timeout   = smtp_default.Smtp_client.conf_attempt_timeout_sec;
        conn_send_retries   = smtp_default.Smtp_client.conf_n_retries;
        sock_buff_size      = max 
                              smtp_default.Smtp_client.conf_sock_buff_size
                                dns_default.Dns_client.conf_sock_buff_size;

        (* Sender's Time Zone Configuration: *)
        tz_offset           = None
};

(*=========================================*)
(*  External |=> Internal Transformations: *)
(*=========================================*)
(*---------------------*)
(*  "set_group_name":  *)
(*---------------------*)
value set_group_name: internal_conf -> string -> unit =
fun ic gr_name ->
    ic.from_group_name := gr_name;


(*----------------------------*)
(*  "fill_from_global_parm":  *)
(*----------------------------*)
(*  Fills in an "internal_conf" structure from a "global_parm". Basic (static)
    validity checks are performed.
*)
value fill_from_global_parm: internal_conf -> global_parm -> unit =

fun ic gp ->
    match gp with
    [
        (* Mailing List Configuration: *)
        MList_Src           x -> ic.mlist_src       := x

        (* Files Configuration: *)

    |   Licence_File        s -> ic.licence_file    := s
    |   DirAcc_File         s -> ic.dir_acc_file    := s
    |   Info_File           s -> ic.info_file       := s
    |   Errors_File         s -> ic.errors_file     := s
    |   PID_File            s -> ic.pid_file        := s

        (* Running Configuration: *)

    |   N_Rounds            i ->
            if  i >= 1
            then ic.n_rounds          := i
            else raise
                 (Config_Error
                 (Printf.sprintf "Invalid Number of Rounds (%d)" i))

    |   N_Threads           i ->
            if  i >= 1
            then ic.n_threads         := i
            else raise
                 (Config_Error
                 (Printf.sprintf "Invalid Number of Threads (%d)" i))

    |   DNS_Threads         i ->
            if  i >= 0
            then ic.dns_threads       := i
            else raise
                (Config_Error
                (Printf.sprintf "Invalid Number of DNS Threads (%d)" i))

    |   DNS_Rounds          i ->
            if  i >= 1
            then ic.dns_rounds        := i
            else raise
                (Config_Error
                (Printf.sprintf "Invalid Number of DNS rounds  (%d)" i))

    |   Thread_Stack_K      i ->
            if  i >= 1
            then ic.thread_stack_k    := i
            else raise
                 (Config_Error
                 (Printf.sprintf "Invalid Thread Stack Size (%d K)" i))

    |   With_Monitor        b ->
            ic.with_monitor           := b

    |   Max_Blocking_Time   f ->
            if  f > 0.0
            then ic.max_blocking_time := f
            else raise
                 (Config_Error
                 (Printf.sprintf "Invalid Max Blocking Time (%f sec)" f))

    |   Stats_Interval      i ->
            if  i >= 0  (* "0" is OK, means no intermediate stats *)
            then ic.stats_interval    := i
            else raise
                 (Config_Error
                 (Printf.sprintf "Invalid Stats Interval  (%d)" i))

    |   Stats_Threshold     f ->
            if  f >= 0.0 && f <= 1.0
            then ic.stats_threshold   := f
            else raise
                 (Config_Error
                 (Printf.sprintf "Invalid Stats Threshold (%f)" f))

    |   Dry_Run             f ->
            if  f >= 0.0
            then ic.dry_run           := Some f
            else raise
                 (Config_Error
                 (Printf.sprintf "Invalid Dry Run Delay (%f sec)" f))

    |   Trace_Dialogue      b -> ic.trace_dialogue  := b

        (* Low-Level Mechanisms and Security: *)

    |   TNIO_Events_Mech    m -> ic.tnio_events_mech  := m
    |   Run_As_User         s -> ic.run_as_user       := s
    |   Run_As_Group        s -> ic.run_as_group      := s
    |   Use_RT_Priority     b -> ic.use_rt_priority   := b

        (* DNS Configuration:    *)

    |   DNS_Servers        ls -> ic.dns_servers     := ls

    |   DNS_Cache_Size      i ->
            if  i >= 1
            then ic.dns_cache_size    := i
            else raise
                 (Config_Error
                 (Printf.sprintf "Invalid DNS Cache Size (%d)" i))

    |   DNS_TimeOut         f ->
            if  f > 0.0
            then ic.dns_timeout       := f
            else raise
                 (Config_Error
                 (Printf.sprintf "Invalid DNS TimeOut (%f)" f))

    |   DNS_ReTries         i ->
            if  i >= 0
            then ic.dns_retries       := i
            else raise
                 (Config_Error
                 (Printf.sprintf "Invalid DNS ReTries Number (%d)" i))

        (* TCP/IP Configuration: *)

    |   Sock_Buff_Size_K    i ->
            if  i >= 1
            then ic.sock_buff_size := i * 1024  (* !!! *)
            else raise
                 (Config_Error
                 (Printf.sprintf "Invalid Send Buff Size (%d K)" i))

        (* Sender's Time Zone Configuration: *)

    |   TZ_Offset           h m ->
            if  h < -23 || h > 23 || m < -59 || m > 59 || h * m < 0
            then raise
                 (Config_Error
                 (Printf.sprintf "Time Zone Offset out of range (%d Hr, %d Min)"
                                 h m))
            else
            ic.tz_offset:= Some (h,m)
    ];

(*---------------------------*)
(*  "fill_from_group_parm":  *)
(*---------------------------*)
(*  Similar to "fill_from_global_parms":  *)

value fill_from_group_parm: internal_conf -> group_parm -> unit =

fun ic gp ->
    match gp with
    [
        (* (E)SMTP Configuration: *)

        Envelope_From       s -> ic.envelope_from   := s
    |   Priority            i ->
            if  i >= 1 && i <= 5
            then ic.priority    := i
            else raise
                 (Config_Error
                 (Printf.sprintf "Invalid Priority (%d)" i))

    |   Use_Persistence     b -> ic.use_persistence := b
    |   Use_Pipelining      b -> ic.use_pipelining  := b

    |   Local_IPs           ls-> ic.local_ips       := ls
    |   Local_Domains       ls-> ic.local_domains   := ls
    |   Verify_Locals       b -> ic.verify_locals   := b
    |   Strict_Lines        b -> ic.strict_lines    := b
    |   Strict_5XX_Mode     b -> ic.strict_5xx_mode := b

        (* Content Configuration: *)

    |   Body_From           s -> ic.body_from       := s
    |   To_Templ            s -> ic.to_templ        := s
    |   Subject             s -> ic.subject         := s
    |   Msg_File_HTML       s -> ic.msg_file_html   := s
    |   Msg_File_Txt        s -> ic.msg_file_txt    := s

        (* TCP/IP  Configuration: *)

    |   ConnSend_TimeOut    f ->
            if  f > 0.0
            then ic.conn_send_timeout:= f
            else raise
                 (Config_Error
                 (Printf.sprintf "Invalid Conn/Send TimeOut (%f)" f))

    |   ConnSend_ReTries    i ->
            if  i >= 0
            then ic.conn_send_retries:= i
            else raise
                 (Config_Error
                 (Printf.sprintf "Invalid Conn/Send ReTries Number (%d)" i))

        (* Running Configuration: *)

    |   Group_Threads_Limit a ->
            let (msg, i, j) = match a with
                [ Absolute      n   ->
                    (Printf.sprintf "Absolute %d"  n, n, n)
                | Per_Server_IP s h ->
                    (Printf.sprintf "Per_Server_IP %d %d" s h, s, h)
                ]
            in
            if i >= 0 && j >= 0 && j >= i
            (* "0" is OK to configure out a given Group *)
            then ic.group_threads_limit := a
            else raise
                 (Config_Error ("Invalid Group Threads Limit: "^msg))
    ];

(*------------------------*)
(*  "mk_internal_confs":  *)
(*------------------------*)
(*  Using the functorial interface of "Domains_config", creates an ARRAY of
    "internal_conf"s from a given "external_conf":
    The 0th element of the array is the default conf to be applied to auto-
    matically-generated Singleton Groups, and other elements  are confs for
    explicitly configured Groups:
*)
module T =
struct
    (* Abstract "group_" and "global_parm"s: *)
    type  group_parm'        = group_parm;
    type  global_parm'       = global_parm;


    (* Abstract "domain_group_conf": *)
    type  domain_group_conf' = domain_group_conf;

    value get_group_name'           : domain_group_conf' -> string =
        fun dgc -> dgc.group_name;

    value get_group_parms'          : domain_group_conf' -> list group_parm'  =
        fun dgc -> dgc.group_parms;


    (* Abstract "external_conf": *)
    type  external_conf'     = barrage_external_conf;

    value get_global_parms'         :
        external_conf' -> list global_parm' =
        fun ec  -> ec.global_parms;

    value get_default_group_parms'  :
        external_conf' -> list group_parm'  =
        fun ec  -> ec.default_group_parms;

    value get_domain_groups'        :
        external_conf' -> list domain_group_conf' =
        fun ec  -> ec.domain_groups;


    (* Abstract "internal_conf": *)
    type  internal_conf'        = internal_conf;

    value empty_internal_conf'  = empty_internal_conf;
    value set_group_name'       = set_group_name;
    value fill_from_global_parm'= fill_from_global_parm;
    value fill_from_group_parm' = fill_from_group_parm;

    (* Exception: *)
    exception Config_Error'     = Config_Error;
end;

module C = Domains_config.Mk_Config T;


value mk_internal_confs: barrage_external_conf -> array internal_conf =
    C.mk_internal_confs;


(*--------------------*)
(*  "mk_dns_config":  *)
(*--------------------*)
value mk_dns_config:
    internal_conf-> Dns_client.dns_agent_config =
fun conf0 ->
    let dns_config = Dns_client.default_dns_agent_config ()
    in
    do {
        dns_config.Dns_client.conf_timeout_sec   := conf0.dns_timeout;
        dns_config.Dns_client.conf_n_retries     := conf0.dns_retries;
        dns_config.Dns_client.conf_cache_size    := conf0.dns_cache_size;
        dns_config.Dns_client.conf_dns_servers   := conf0.dns_servers;
        dns_config.Dns_client.conf_sock_buff_size:= conf0.sock_buff_size;
        (* Return: *)
        dns_config
    };

