(*pp camlp4r pa_ioXML.cmo *)
(* vim:ts=4:syntax=ocaml
*)
(*===========================================================================*)
(*                           "depot_xml_types.ml":                           *)
(*                                                                           *)
(*       Types for "gatling_depot" Configuration and Message Queueing,       *)
(*           with Automatically-Generated XML Parsers and Printers           *)
(*                                                                           *)
(*                (C) Explar Technologies Ltd, 2003--2006                    *)
(*===========================================================================*)
(*=================================*)
(*  Configuration: External View:  *)
(*=================================*)
exception Config_Error of string;

(*  These types are used in external XML config files;  to avoid unnecessarily
    repeated data in those files, the data are organised as LISTS (rather than
    records) or parms, all of which are syntactically optional.
*)

(*  The following config parms can be specified on the per-Domain-Group basis,
    as well as globally. Some of them are optional, other mandatory, yet other
    depend on each other's combination, so it's not possible to say here which
    of them are optional or otherwise:
*)
type domain_threads_limit =
[
    Absolute      of int
|   Per_Server_IP of int
];

type group_parm =
[
    (* (E)SMTP Configuration: *)
    Use_Persistence         of bool
|   Use_Pipelining          of bool
|   Local_IPs               of list string
|   Local_Domains           of list string
|   Verify_Locals           of bool
|   Strict_Lines            of bool
|   Use_Txt_Err_Notes       of bool
|   Use_HTML_Err_Notes      of bool

    (* TCP/IP  Configuration: *)
|   ConnSend_TimeOut        of float
|   ConnSend_ReTries        of int

    (* Running Configuration: *)
|   Delivery_Attempts       of int
|   Delivery_ReTry_Minutes  of float
|   Load_ReTry_Seconds      of float
|   Group_Threads_Limit     of domain_threads_limit
];

(*  The following config parms can only be specified globally. Again, we can't
    say here which of them are optional or mandatory.  NB: some of these parms
    (e.g., "Admin_EMail") could also be specified on the per-Group basis,  but
    that is probably not necessary:
*)
type global_parm =
[
    (* Files Configuration: *)
    Licence_File            of string
|   Msg_Buff_Dir            of string
|   Msg_Buff_Dir_Lock       of string
|   Log_File                of string
|   PID_File                of string
|   Err_Note_File_HTML      of string
|   Err_Note_File_Txt       of string

    (*  (E)SMTP Configuration: *)
|   Admin_EMail             of string

    (* Running Configuration: *)
|   N_Threads               of int
|   Thread_Stack_K          of int
|   Poll_Period             of float
|   Daemon_Mode             of bool
|   Restart_After           of int
|   Max_Blocking_Time       of float
|   Trace_Dialogue          of bool

    (* Low-Level Mechanisms and Security: *)
|   TNIO_Events_Mech        of Tnio_mechs.io_events_mech
|   Run_As_User             of string
|   Run_As_Group            of string
|   Use_RT_Priority         of bool

    (* DNS     Configuration: *)
|   DNS_Servers             of list string
|   DNS_Cache_Size          of int
|   DNS_TimeOut             of float
|   DNS_ReTries             of int

    (* TCP/IP Configuration: *)
|   Sock_Buff_Size_K        of int

    (* Sender's Time Zone  : *)
|   TZ_Offset               of int and int
];


(*------------------------*)
(*  "domain_group_conf":  *)
(*------------------------*)
(*  The domains which do not fall into any other (explicitly-configured) Group
    will be encapsulated  into their own  Single-Domain Groups   (NOT into one
    large Group!) with default settings:
*)
type domain_group_conf =
{
    group_name      : string;
        (* For user's convenience only *)

    selector_regexps: list string;
        (* The RegExps are in the SHELL (GLOB) format, and are ORed before
           being applied to domains.
        *)

    group_parms     : list group_parm
        (* Config parms for this Group, overriding global defaults *)
};

(*--------------------------*)
(*  "depot_external_conf":  *)
(*--------------------------*)
(*  Mailing-List related configuration parms: *)
type depot_external_conf =
{
    (* Global Parms: *)
    global_parms       : list global_parm;

    (* Default Per-Group Parms: *)
    default_group_parms: list group_parm;

    (* Explicitly-Configured Groups: *)
    domain_groups      : list domain_group_conf
};

(*----------------------------*)
(*  "consult_external_conf":  *)
(*----------------------------*)
value consult_external_conf: string -> depot_external_conf =

fun conf_xml_file ->
    let cnnl = open_in conf_xml_file  in
    try
        let strm  = Stream.of_channel cnnl    in
        let xstr  = IoXML.parse_xml   strm    in
        let res   = xparse_depot_external_conf xstr in
        do {
            close_in cnnl;
            res
        }
    with
    [   hmm ->
        do {
            close_in cnnl;
            raise hmm
        }
    ];

(*=================================*)
(*  Configuration: Internal View:  *)
(*=================================*)
(*  This is a RECORD representation of the config data. All global and per-
    group parms come together, and are directly accessible.    Fields which 
    have not actually been set, hold default or invalid values.    Multiple
    record configs can be constructed (e.g. for different domain Groups).
    XXX: we don't really need XML representation for this type, but it will
    be constructed automatically anyway:
*)
(*--------------------*)
(*  "internal_conf":  *)
(*--------------------*)
type internal_conf =
{
    (* "from_group_name": it is convenient to keep it: *)
    from_group_name         : mutable string;

    (* (E)SMTP Configuration: *)
    admin_email             : mutable string;
    use_persistence         : mutable bool;
    use_pipelining          : mutable bool;
    local_ips               : mutable list string;
    local_domains           : mutable list string;
    verify_locals           : mutable bool;
    strict_lines            : mutable bool;

    (* Files   Configuration: *)
    licence_file            : mutable string;
    msg_buff_dir            : mutable string;
    msg_buff_dir_lock       : mutable string;
    err_note_file_html      : mutable string;
    err_note_file_txt       : mutable string;
    use_html_err_note       : mutable bool;
    use_txt_err_note        : mutable bool;
    log_file                : mutable string;
    pid_file                : mutable string;

    (* Running Configuration: *)
    poll_period             : mutable float;
    daemon_mode             : mutable bool;
    restart_after           : mutable int;
    n_threads               : mutable int;
    thread_stack_k          : mutable int;
    group_threads_limit     : mutable domain_threads_limit;
    delivery_attempts       : mutable int;
    delivery_retry_interval : mutable float;
    load_retry_interval     : mutable float;
    max_blocking_time       : mutable float;
    trace_dialogue          : mutable bool;

    (* DNS    Configuration: *)
    dns_servers             : mutable list string;
    dns_cache_size          : mutable int;
    dns_timeout             : mutable float;
    dns_retries             : mutable int;

    (* Low-Level Mechsnisms and Security: *)
    tnio_events_mech        : mutable Tnio_mechs.io_events_mech;
    run_as_user             : mutable string;
    run_as_group            : mutable string;
    use_rt_priority         : mutable bool;

    (* TCP/IP Configuration: *)
    conn_send_timeout       : mutable float;
    conn_send_retries       : mutable int;
    sock_buff_size          : mutable int;

    (* Sender's Time Zone  : *)
    tz_offset               : mutable option (int * int)  (* Hrs, Mins *)
};

(*--------------------------*)
(*  "empty_internal_conf":  *)
(*--------------------------*)
(*  We don't call it "default" conf, since many fields do not have reasonable
    default values, and are initialised to invalid values.
    NB: for "Dns_client" and "Smtp_client"-related fields,  we  propagate the
    defaults back from those modules, for the sake of consistency.
    NB: this is a function, not a constant value,  for  the value is mutable,
    and we must create different copies of it every time:
*)
value empty_internal_conf: unit -> internal_conf =
fun () ->
    let dns_default  = Dns_client.default_dns_agent_config   () in
    let smtp_default = Smtp_client.default_smtp_agent_config () in
    {
        from_group_name     = "";

        (* (E)SMTP Configuration: *)
        admin_email         = "";
        use_persistence     = smtp_default.Smtp_client.conf_use_persistence;
        use_pipelining      = smtp_default.Smtp_client.conf_use_pipelining;
        strict_lines        = True;

        local_ips           =
            List.map (fun ip -> Unix.string_of_inet_addr ip)
                     (Array.to_list smtp_default.Smtp_client.conf_client_ips);

        local_domains       =
            Array.to_list (smtp_default.Smtp_client.conf_client_domains);

        verify_locals       = True;

        (* Files   Configuration: *)
        licence_file        = "";
        msg_buff_dir        = "";
        msg_buff_dir_lock   = "";
        err_note_file_html  = "";
        err_note_file_txt   = "";
        use_html_err_note   = True;
        use_txt_err_note    = True;
        log_file            = "";
        pid_file            = "";

        (* Running Configuration: *)
        poll_period         = 0.1;
        daemon_mode         = True;
        restart_after       = 10000;
        n_threads           = 10;
        thread_stack_k      = 256;
        delivery_attempts   = 3;
        delivery_retry_interval = 120.0 *. 60.0;    (* 2 hours   *)
        load_retry_interval     = 180.0;            (* 3 minutes *)
        group_threads_limit = Per_Server_IP 1;
        max_blocking_time   = 300.0;                (* 5 minutes *)
        trace_dialogue      = False;

        (* Low-Level Mechanisms and Security: *)
        tnio_events_mech    = Tnio_mechs.IO_Events_Default;
        run_as_user         = "nobody";
        run_as_group        = "nogroup";
        use_rt_priority     = False;

        (* DNS    Configuration: *)
        dns_servers         = [];    (* Don't use any local servers *)
        dns_cache_size      = dns_default.Dns_client.conf_cache_size;
        dns_timeout         = dns_default.Dns_client.conf_timeout_sec;
        dns_retries         = dns_default.Dns_client.conf_n_retries;

        (* TCP/IP Configuration: *)
        conn_send_timeout   = smtp_default.Smtp_client.conf_attempt_timeout_sec;
        conn_send_retries   = smtp_default.Smtp_client.conf_n_retries;
        sock_buff_size      = max
                              smtp_default.Smtp_client.conf_sock_buff_size
                                dns_default.Dns_client.conf_sock_buff_size;

        (* Sender's Time Zone Configuration: *)
        tz_offset           = None
};

(*=========================================*)
(*  External |=> Internal Transformations: *)
(*=========================================*)
(*---------------------*)
(*  "set_group_name":  *)
(*---------------------*)
value set_group_name: internal_conf -> string -> unit =
fun ic gr_name ->
    ic.from_group_name := gr_name;


(*----------------------------*)
(*  "fill_from_global_parm":  *)
(*----------------------------*)
(*  Fills in an "internal_conf" structure from a "global_parm". Basic (static)
    validity checks are performed:
*)
value fill_from_global_parm: internal_conf -> global_parm -> unit =

fun ic gp ->
    match gp with
    [
    (* Files   Configuration: *)

        Licence_File        s -> ic.licence_file        := s
    |   Msg_Buff_Dir        s -> ic.msg_buff_dir        := s
    |   Msg_Buff_Dir_Lock   s -> ic.msg_buff_dir_lock   := s
    |   Log_File            s -> ic.log_file            := s
    |   PID_File            s -> ic.pid_file            := s
    |   Err_Note_File_HTML  s -> ic.err_note_file_html  := s
    |   Err_Note_File_Txt   s -> ic.err_note_file_txt   := s

        (* (E)SMTP Configuration: *)

    |   Admin_EMail         s ->
            try
            do {
                ignore (Misc_utils.parse_email_addr s);
                ic.admin_email := s
            }
            with
            [ _ -> raise
                  (Config_Error
                  (Printf.sprintf "Invalid Admin EMail (%s)" s))
            ]

        (* Running Configuration: *)

    |   N_Threads           i ->
            if  i >= 1
            then ic.n_threads         := i
            else raise
                 (Config_Error
                 (Printf.sprintf "Invalid Number of Threads (%d)" i))

    |   Thread_Stack_K      i ->
            if  i >= 1
            then ic.thread_stack_k    := i
            else raise
                 (Config_Error
                 (Printf.sprintf "Invalid Thread Stack Size (%d K)" i))

    |   Poll_Period         f ->
            if  f > 0.0
            then ic.poll_period       := f
            else raise
                 (Config_Error
                 (Printf.sprintf "Invalid Poll_Period (%f)" f))

    |   Restart_After       i ->
            if  i >= 1
            then ic.restart_after     := i
            else raise
                 (Config_Error
                 (Printf.sprintf "Invalid Restart After Interval  (%d)" i))

    |   Max_Blocking_Time   f ->
            if  f > 0.0
            then ic.max_blocking_time := f
            else raise
                 (Config_Error
                 (Printf.sprintf "Invalid Max Blocking Time (%f)" f))

    |   Daemon_Mode         b -> ic.daemon_mode     := b
    |   Trace_Dialogue      b -> ic.trace_dialogue  := b

        (* Low-Level Mechsnisms and Security: *)

    |   TNIO_Events_Mech    m -> ic.tnio_events_mech  := m
    |   Run_As_User         s -> ic.run_as_user       := s
    |   Run_As_Group        s -> ic.run_as_user       := s
    |   Use_RT_Priority     b -> ic.use_rt_priority   := b

        (* DNS    Configuration: *)

    |   DNS_Servers        ls -> ic.dns_servers     := ls

    |   DNS_Cache_Size      i ->
            if  i >= 1
            then ic.dns_cache_size    := i
            else raise
                 (Config_Error
                 (Printf.sprintf "Invalid DNS Cache Size (%d)" i))

    |   DNS_TimeOut         f ->
            if  f > 0.0
            then ic.dns_timeout       := f
            else raise
                 (Config_Error
                 (Printf.sprintf "Invalid DNS TimeOut (%f)" f))

    |   DNS_ReTries         i ->
            if  i >= 0
            then ic.dns_retries       := i
            else raise
                 (Config_Error
                 (Printf.sprintf "Invalid DNS ReTries Number (%d)" i))

        (* TCP/IP Configuration: *)

    |   Sock_Buff_Size_K    i ->
            if  i >= 1
            then ic.sock_buff_size := i * 1024  (* !!! *)
            else raise
                 (Config_Error
                 (Printf.sprintf "Invalid Sock Buff Size (%d K)" i))

    |  TZ_Offset           h m ->
            if  h < -23 || h > 23 || m < -59 || m > 59 || h * m < 0
            then raise
                 (Config_Error
                 (Printf.sprintf "Time Zone Offset out of range (%d Hr, %d Min)"
                                 h m))
            else
            ic.tz_offset:= Some (h,m)
    ];


(*---------------------------*)
(*  "fill_from_group_parm":  *)
(*---------------------------*)
(*  Similar to "fill_from_global_parms":  *)

value fill_from_group_parm: internal_conf -> group_parm -> unit =

fun ic gp ->
    match gp with
    [
        (* (E)SMTP Configuration: *)

        Use_Persistence     b -> ic.use_persistence  := b
    |   Use_Pipelining      b -> ic.use_pipelining   := b
    |   Strict_Lines        b -> ic.strict_lines     := b
    |   Local_IPs           ls-> ic.local_ips        := ls
    |   Local_Domains       ls-> ic.local_domains    := ls
    |   Verify_Locals       b -> ic.verify_locals    := b
    |   Use_Txt_Err_Notes   b -> ic.use_txt_err_note := b
    |   Use_HTML_Err_Notes  b -> ic.use_html_err_note:= b

        (* TCP/IP  Configuration: *)

    |   ConnSend_TimeOut    f ->
            if  f > 0.0
            then ic.conn_send_timeout:= f
            else raise
                 (Config_Error
                 (Printf.sprintf "Invalid Conn TimeOut (%f)" f))

    |   ConnSend_ReTries    i ->
            if  i >= 0
            then ic.conn_send_retries:= i
            else raise
                 (Config_Error
                 (Printf.sprintf "Invalid Conn/Send ReTries Number (%d)" i))

        (* Running Configuration: *)

    |   Group_Threads_Limit a ->
            let (s, i) =  match a with
                [ Absolute      n -> ("Absolute", n)
                | Per_Server_IP n -> ("Per_Server_IP", n)
                ]
            in
            if i >= 1
            then ic.group_threads_limit := a
            else raise
                 (Config_Error
                 (Printf.sprintf "Invalid %s Threads Limit (%d)" s i))

    |   Delivery_Attempts   i ->
            if i >= 0 (* "0" is OK to configure out a given Group *)
            then ic.delivery_attempts   := i
            else raise
                 (Config_Error
                 (Printf.sprintf "Invalid Delivery Attempts Number (%d)" i))

    |   Delivery_ReTry_Minutes  f ->
            if f > 0.0
            then ic.delivery_retry_interval := f *. 60.0
            else raise
                 (Config_Error
                 (Printf.sprintf "Invalid Delivery Retry Minutes (%f)" f))

        |   Load_ReTry_Seconds     f ->
            if  f > 0.0
            then ic.load_retry_interval    := f
            else raise
                 (Config_Error
                 (Printf.sprintf "Invalid Load Retry Seconds (%f)" f))
    ];


(*------------------------*)
(*  "mk_internal_confs":  *)
(*------------------------*)
(*  Using the functorial interface of "domains_config":  *)

module T =
struct
    (* Abstract "group_" and "global_parm"s: *)
    type  group_parm'        = group_parm;
    type  global_parm'       = global_parm;


    (* Abstract "domain_group_conf": *)
    type  domain_group_conf' = domain_group_conf;

    value get_group_name'           : domain_group_conf' -> string =
        fun dgc -> dgc.group_name;

    value get_group_parms'          : domain_group_conf' -> list group_parm'  =
        fun dgc -> dgc.group_parms;


    (* Abstract "external_conf": *)
    type  external_conf'     = depot_external_conf;

    value get_global_parms'         :
        external_conf' -> list global_parm' =
        fun ec  -> ec.global_parms;

    value get_default_group_parms'  :
        external_conf' -> list group_parm'  =
        fun ec  -> ec.default_group_parms;

    value get_domain_groups'        :
        external_conf' -> list domain_group_conf' =
        fun ec  -> ec.domain_groups;


    (* Abstract "internal_conf": *)
    type  internal_conf'        = internal_conf;

    value empty_internal_conf'  = empty_internal_conf;
    value set_group_name'       = set_group_name;
    value fill_from_global_parm'= fill_from_global_parm;
    value fill_from_group_parm' = fill_from_group_parm;

    (* Exception: *)
    exception Config_Error'     = Config_Error;
end;

module C = Domains_config.Mk_Config T;


value mk_internal_confs: depot_external_conf -> array internal_conf =
    C.mk_internal_confs;


(*===============*)
(*   Messages:   *)
(*===============*)
(*---------------*)
(*  "user_msg":  *)
(*---------------*)
(*  This type closely follows "Mime_type.smtp_msg_info", with the differences
    specific to the functionality of "gatling_depot":
*)
type user_msg =
{
    umsg_file  : string;
    from_name  : string;
    from_email : string;
    to_email   : string;
    subject    : string;
    priority   : Mime_xml_types.msg_priority;
    content    : Mime_xml_types.mime_msg_struct
};

(*------------------*)
(*  "delayed_msg":  *)
(*------------------*)
(*  Entry in a Delayed Message Queue and in the file system. It is used for
    both re-sending the message  and  sending the error notification  if it
    finally fails, hence the following structure:
*)
type delayed_msg =
{
    conf       : internal_conf;         (* Yes, we will need it!    *)
    group_id   : string;                (* Unique Group ID          *)
    orig_fmsg  : Mime_xml_types.smtp_msg_final;
                                        (* For re-sends             *)
    orig_umsg  : user_msg;              (* For notifications        *)

    dmsg_file  : string;
    next_send  : mutable float;         (* Seconds since the Epoch  *)
    prev_errors: mutable list string    (* Errors in prev delivery attempts  *)
};


