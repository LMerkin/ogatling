(*pp camlp4r pa_ioXML.cmo *)
(* vim:ts=4:syntax=ocaml
*)
(*===========================================================================*)
(*                           "mime_xml_types.ml":                            *)
(*                                                                           *)
(*          Types for "Mime_codec" with Automatically-Generated              *)
(*                         XML Parsers and Printers                          *)
(*                                                                           *)
(*                 (C) Explar Technologies Ltd, 2003--2006                   *)
(*===========================================================================*)
(*----------------------*)
(*  "Source" Messages:  *)
(*  "smtp_msg_info"  :  *)
(*----------------------*)
(* Composition of a message: if the message really contains more than 1 part,
   composition must  be "MultiPart_*":
*)
type mime_composition = [SinglePart  | MultiPart_Mixed | MultiPart_Alternative];

(* Message part can be "inline" or "attachment": *)
type mime_part_disp   = [Disp_Inline | Disp_Attachment];

(* Full structure of a message part (at the preparation stage).  The "body" is
   either a list of strings, interpreted according to the "mime_type",    or a
   nested sub-structure composed of its own parts. Currently the part encoding
   is decided automatically:
*)
type simple_mime_body =
{
    mime_type : string;
    file_name : string;
    body_lines: list string
};

type mime_part_body =
[   Body_Simple of simple_mime_body
  | Body_Nested of mime_msg_struct
]

and mime_msg_struct =
{
    composition: mime_composition;
    parts      : list mime_msg_part
}

and mime_msg_part   =
{
    disposition: mime_part_disp;
    body       : mime_part_body
};

(* Message Priority. It can be given by the "msg_priority" type or by integers
   1 (Highest) .. 5 (Lowest):
*)
type msg_priority = [MP_Highest | MP_High | MP_Normal | MP_Low | MP_Lowest];


(* Full structure of a message being prepared. If the "msg_to" field is
   not known at that point   (e.g., it varies for elements of a mailing
   list), a TAG is to be installed insted, so it can later be  replaced
   in the message headers by the actual address;

   "tags" can be used in parts bodies as well as in any headers  (e.g.,
   "subject"); they are ignored in those bodies which are not of "text/*"
   MIME type OR contain non-ASCII chars:
*)
type smtp_msg_info =
{
    (* "Header" matter for the whole top-level message. Any strings related
        to headers and body of the message  (but excluding "envel_from", as
        it's a part of the envelope) can contain tags:
    *)
    strict_lines: bool;
    envel_from  : string;
    body_from   : string;
    msg_to      : string;
    subject     : string;
    priority    : msg_priority;
    date        : string;
    msg_id      : string;
    mailer_ver  : string;
    tags        : list string;        (* Tags allowed in this message --
                                         currently NOT per part!      *)
    (* The structured content: *)
    content     : mime_msg_struct
};

(*----------------------*)
(*  "Cooked" Messages:  *)
(*  "smtp_msg_cooked":  *)
(*----------------------*)
(* SMTP data being transmitted can be composed of the following fragments:
   chunks of data, or tags  to be substituted on the per-message basis.
   If strict line mode is required, we need the "SMTP_Line" ctor to group
   elements corresponding to a single line together:
*)
type smtp_data  =
[
    SMTP_Chunk of string         (* Chunk of text or binary *)
  | SMTP_Tag   of string         (* Name of a tag *)
  | SMTP_Line  of list smtp_data (* Multiple segments forming one line *)
];

(* Structure of a message which is prepared and NEARLY  ready to be sent
   (the sender still needs to do tag substitutions on ALL fields of this
   structure!):
*)
type smtp_msg_cooked =
{
    envel_from': smtp_data;   (* Always SMTP_Line *)
    to_addr'   : smtp_data;   (* Always SMTP_Line *)
    all_data'  : list smtp_data
};

(*---------------------*)
(*  "Final" Messages:  *)
(*  "smtp_msg_final":  *)
(*---------------------*)
(* Final message, ABSOLUTELY ready to be sent -- after tags substituition: *)

type smtp_msg_final =
{
    envel_from'': string;
    to_addr''   : string;   (* "to_domain''" is a part of "to_addr''" *)
    to_domain'' : string;
    all_data''  : list string
};

