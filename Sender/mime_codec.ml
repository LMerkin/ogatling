(*pp camlp4r *)
(* vim:ts=4:syntax=ocaml
*)
(*===========================================================================*)
(*                              "mime_codec.ml":                             *)
(*          Parsing and Generation of MIME-Enabled SMTP Messages             *)
(*                 (C) Explar Technologies Ltd, 2003--2006                   *)
(*===========================================================================*)
open      Mime_xml_types;
exception MIME_Error of string;


(*==================*)
(*  Preliminaries:  *)
(*==================*)
(*----------------------*)
(*  "priority_of_int":  *)
(*  "int_of_priority":  *)
(*----------------------*)
value priority_of_int: int -> msg_priority =
fun p ->
    match p with
    [   5 -> MP_Lowest
      | 4 -> MP_Low
      | 3 -> MP_Normal
      | 2 -> MP_High
      | 1 -> MP_Highest
      | _ -> failwith "Invalid Priority"
    ];

value int_of_priority: msg_priority -> int =
fun p ->
    match p with
    [   MP_Lowest  -> 5
      | MP_Low     -> 4
      | MP_Normal  -> 3
      | MP_High    -> 2
      | MP_Highest -> 1
    ];


(*-------------------*)
(*  "is_text_mime":  *)
(*-------------------*)
value is_text_mime: string -> bool =
fun mime_type ->
    ExtString.String.starts_with mime_type "text/";


(*-------------------*)
(*  "is_all_ascii":  *)
(*-------------------*)
value rec is_ascii_str: int -> string -> bool =
fun   p s ->
    if  p = String.length s
    then True                 (* The whole "s" checked successfully *)
    else
    if  (int_of_char s.[p]) >= 128
    then
        False                 (* Non-ASCII char found *)
    else
        is_ascii_str (p+1) s; (* Yes so far, try next... *)


value rec is_all_ascii: mime_part_body -> bool =
fun body ->
    match body with
    [   Body_Simple sb ->
            List.for_all (is_ascii_str 0) sb.body_lines

      | Body_Nested mstr ->
            False   (* Does not matter!: *)
];


(*---------------------*)
(*  "string_of_comp":  *)
(*---------------------*)
value string_of_comp: mime_msg_struct -> string =
fun mstr ->
    match mstr.composition with
    [   MultiPart_Mixed       -> "multipart/mixed"
      | MultiPart_Alternative -> "multipart/alternative"
      | _ -> assert False
    ];


(*-------------------------*)
(*  "msg_part_from_file":  *)
(*-------------------------*)
(*  Useful for composing "smtp_msg_info" objects, which can then be converted
    into "smtp_msg_cooked" ones. Currently, it CANNOT determine the MIME type
    of the file automatically; other attributes also need to be specified. It
    can only produce "Simple" body parts:
*)
value msg_part_from_file:
      mime_part_disp -> string -> string -> mime_msg_part =

fun disp  mime_type file_name  ->
    let body_lines = if  is_text_mime mime_type 
    then
        (* If the MIME type is textual, read the file line-by-line: *)
         Misc_utils.read_lines file_name
    else
        (* Read the whole content at once: *)
        [Misc_utils.read_file  file_name]
    in
    (* The result: *)
    {
        disposition = disp;
        body        = Body_Simple
                      {
                        mime_type = mime_type;
                        file_name = file_name;
                        body_lines= body_lines
                      }
    };

(*---------------------*)
(*  "smtp_time_stamp": *)
(*---------------------*)
(* Default time zone config: offset in (hour, min) from GMT: *)

value tz_offset_str: int -> int -> string =
fun off_hr off_min ->
    (* off_hr and off_min must be of the same sign: *)
    if  off_hr * off_min < 0
    then
        invalid_arg "tz_offset_str: Hr and Min must be of the same sign"
    else
    let sign =
        if  off_hr < 0 || (off_hr = 0 && off_min < 0)
        then '-'
        else '+'
    in
    let abs_hr  = abs off_hr    in
    let abs_min = abs off_min   in
    if  abs_hr > 23 || abs_min > 59
    then
        invalid_arg "tz_offset_str: Hr or Min out of range"
    else
    Printf.sprintf "%c%02d%02d" sign abs_hr abs_min;


value default_tz_offset: (float * string) =
    let gmt     = Unix.gettimeofday ()  in
    let now     = Unix.gmtime    gmt    in

    (* "now" was GMT, but we will convert it back into abs seconds, assuming
       the LOCAL time zone:
    *)
    let loc     = fst (Unix.mktime now) in
    (* The offset in seconds of the local zone will be (gmt-loc): e.g., for
       Eastern longitudes, "loc" will LESS than "gmt", as it's the same no-
       minal clock reading reduced back to GMT, where the curr time is less:
    *)
    let diff_min= (int_of_float (gmt-.loc)) / 60 in  (* Drop seconds, if any *)
    let off_hr  = diff_min    / 60      in
    let off_min = diff_min  mod 60      in           (* OK if   diff_min < 0 *)

    (float_of_int (off_hr * 3600 + off_min * 60),
     tz_offset_str off_hr off_min);


value smtp_time_stamp: ?tz_off: option (int * int) -> float -> string =
fun ?(tz_off = None) abst ->
    let str_tz_off   =
        match tz_off with
        [ None ->
            (* Use the deault string: *)
            snd default_tz_offset
        | Some (off_hr, off_min) ->
            tz_offset_str off_hr off_min
        ]
    in
    (* Reduce "gmt" to the local time is the SUPPLIED time zone: *)
    let sec_off      =
        match tz_off with
        [ None ->
            (* Use the default value: *)
            fst default_tz_offset
        | Some (h,m) ->
            float_of_int (h * 3600 + m * 60)
        ]
    in
    let reduced_time = abst +. sec_off   in     (* Time expressed as local *)
    let lt           = Unix.gmtime reduced_time in

    let days_short   = [| "Sun"; "Mon"; "Tue"; "Wed"; "Thu"; "Fri"; "Sat" |] in

    let months_short = [| "Jan"; "Feb"; "Mar"; "Apr"; "May"; "Jun";
                          "Jul"; "Aug"; "Sep"; "Oct"; "Nov"; "Dec" |] in

    Printf.sprintf "%s, %d %s %d %02d:%02d:%02d %s"
                   days_short  .(lt.Unix.tm_wday)
                   lt.Unix.tm_mday
                   months_short.(lt.Unix.tm_mon )
                   (lt.Unix.tm_year + 1900)
                    lt.Unix.tm_hour
                    lt.Unix.tm_min
                    lt.Unix.tm_sec
                   str_tz_off;

    (* XXX: it does NOT take into account DayLight Saving Time (yet) *)


(*--------------------*)
(*  "base64_encode":  *)
(*--------------------*)
(*  Takes a list of (presumably binary) chunks, and makes a Base64-encoding of
    them, as a single "smtp_data" chunk:
*)
value base64_encode: list string -> bool -> list smtp_data =
fun   chunks strict_mode ->
    (* Calculate the total length of "chunks": *)
    let total_len = List.fold_left \+ 0 (List.map String.length chunks) in

    (* Put all "chunks" into a buffer, so merging them together: *)
    let src_buff = Buffer.create total_len  in
    let ()       = List.iter (Buffer.add_string src_buff) chunks in
    let src      = Buffer.contents src_buff in

    (* Encode the "src" in 57-byte chunks; the encodings are 78-byte long,
       and must be "\r\n"-separated. In the strict line mode, we split the
       result into lines, otherwise keep it as a single chunk:
    *)
    let encoded  = Netencoding.Base64.encode ~linelength:76 ~crlf:True src in
    if  strict_mode
    then
        (* Split the encoded chunk into the lines: *)
        List.map (fun s -> SMTP_Chunk (s^"\r\n"))
                 (Misc_utils.split_by_crlfs encoded)
    else
        [SMTP_Chunk encoded];


(*-------------------*)
(*  "merge_chunks":  *)
(*-------------------*)
(*  After the whole message was constructed, we merge adjacent normal chunks
    (not tags) -- this is probably better  than a highly-fragmented message.
    The results are accumulated in a buffer (the current chunk being merged)
    and the list of all chunks:
*)
value attach_buffer: Buffer.t -> list smtp_data -> list smtp_data =
fun   buff curr_chunks ->
    if  Buffer.length buff <> 0
    then
        let new_chunks = curr_chunks @ [SMTP_Chunk (Buffer.contents buff)] in
        do {
            (* Re-set the "buff", and return "new_chunks": *)
            Buffer.clear buff;
            new_chunks
        }
    else
        curr_chunks;


value rec merge_chunks: list smtp_data -> Buffer.t -> list smtp_data ->
                        list smtp_data =
fun old_chunks curr_buff res_chunks ->
    match old_chunks with
    [   [] ->
            (* All done, just merge in the "curr_buff", if it is non-empty: *)
            attach_buffer curr_buff res_chunks

        | [och::octs] ->
            match och with
            [   SMTP_Chunk s ->
                do {
                    (* "och" is actually a string -- can merge it: *)
                    Buffer.add_string curr_buff s;
                    merge_chunks octs curr_buff res_chunks
                }
              | _ ->
                    (* "och" is a tag -- the current buffer is finished, and
                       will be re-set:
                    *)
                    let new_chunks1 = attach_buffer curr_buff res_chunks in
                    let new_chunks2 = new_chunks1 @ [och] in

                    merge_chunks octs curr_buff new_chunks2
            ]
    ];

(*=======================*)
(*  "Cooking" Messages:  *)
(*=======================*)
(*---------------------*)
(*  "cook_text_line":  *)
(*---------------------*)
(* Takes a text line (assumed to be unterminated), identifies all tags in it,
   splits it into tags  and normal chunks of text,  and installs a terminator
   at the end.  NB: the tags are assumed to be NON-EMPTY and NON-OVERLAPPING.
   Normally, the resulting line   is terminated by "\r\n",  unless requested
   otherwise (for some system fields):
*)
value rec find_tag_occurs: string -> int -> string -> list (string * int) =
fun   line from tag ->
    let lt   =  String.length tag in

    if  from > (String.length line) - lt
    then
        (* No more occurrences: *)
        []
    else
        (* Try to find an occurrence: *)
        let pos    = Misc_utils.contains_from line from tag in
        if  pos >= 0
        then
            (* Found a tag occurrence!
               Find other occurrences recursively -- NOT tail recursion:
            *)
            let others = find_tag_occurs line (pos + lt) tag in
            [(tag, pos) :: others]
        else
            (* The tag was not found: *)
            [];


value cook_text_line:
    ~strict_mode:bool -> ?with_crlf: bool ->
    list string       -> string           -> list smtp_data =

fun ~strict_mode ?(with_crlf=True) tags line ->

    let ll = String.length line in

    (* Find all occurrencies of all "tags" in the "line": *)
    let tags_occurs   = List.concat (List.map (find_tag_occurs line 0) tags) in

    (* Sort them by position indices: *)
    let sorted_occurs = List.sort
        (fun (_, p1) (_, p2) -> compare p1 p2) tags_occurs in

    (* Now split the line according to this list;  "curr_res" is the resulting
       list of both normal chunks and tags being accumulated, and "prev_cp" is
       the position of the normal chunk preceding the current tag:
    *)
    let (last_pos, parsed_line) =
    List.fold_left
        (fun (prev_cp, curr_res) (tag, tag_pos) ->
            (* Advance the curr position: *)
            let next_pos = tag_pos + String.length tag in

            (* If the previous normal chunk was empty (i.e. two tags come in a
               sequence), do not install it:
            *)
            let next_res = if  prev_cp = tag_pos
            then
                curr_res @ [SMTP_Tag tag]
            else
                let prev_chunk = String.sub line prev_cp (tag_pos - prev_cp) in
                curr_res @ [(SMTP_Chunk prev_chunk); (SMTP_Tag tag)]
            in
            (* New state: *)
            (next_pos, next_res)
        )
        (0, []) sorted_occurs
    in
    (* Don't forget to install the last chunk,
       and the possibly the terminator: *)
    let term = if  with_crlf then "\r\n" else "" in

    let   after_all  = if last_pos < ll
    then (String.sub line last_pos  (ll - last_pos)) ^ term
    else term
    in
    let all_frags  = parsed_line @ [SMTP_Chunk after_all] in

    (* NOW: in the non-strict mode, we simply return "all_frags"; in the strict
       mode, they are wrapped together to indicate that they belong to one line:
    *)
    if   strict_mode
    then [SMTP_Line all_frags]
    else all_frags;


(*--------------------*)
(*  "cook_headers":   *)
(*--------------------*)
(*  Merge the headers into a single string, with "\r\n" separators. Tags can
    be identified here as well. The headers are returned line-by-line, which
    is consistent with the Strict Line mode;    in the non-strict mode, they 
    will be coalesced later.
    NB: "cook_text_line" itself inserts "\r\n" at the end of the line!
*)
value cook_headers : bool -> list string -> list (string * string) ->
                     list smtp_data =

fun strict_mode tags headers ->
    List.concat
   (List.map (fun (n,v)->
              cook_text_line ~strict_mode:strict_mode tags (n^": "^v))
              headers);


(*---------------------*)
(*  "cook_part_body":  *)
(*---------------------*)
value rec
cook_part_body: ~is_all_ascii:bool -> ~strict_mode :bool ->
                list string        -> mime_part_body   -> list smtp_data =

fun ~is_all_ascii ~strict_mode tags body ->
    match body with
    [   Body_Nested mstr->
        cook_msg_struct ~strict_mode : strict_mode tags mstr

      | Body_Simple sb  ->
        cook_body_lines ~is_all_ascii: is_all_ascii
                        ~strict_mode : strict_mode
                        tags  sb
    ]

(*----------------------*)
(*  "cook_body_lines":  *)
(*----------------------*)
and
cook_body_lines:  ~is_all_ascii:bool -> ~strict_mode :bool ->
                  list string        -> simple_mime_body   -> list smtp_data =

fun ~is_all_ascii ~strict_mode tags   sb  ->
    if is_text_mime sb.mime_type
    then
        (* The body is text; it is then considered to be a list of lines,
           currently without line terminators. The lines will need to be:
           -- parsed for Tags;
           -- properly terminated.
           If there are no tags, a short-cut implementation is used.
           BUT: we must make sure that "sb.body_lines" do not contain any
                improper new-line symbols inside ("\n", "\r", "\r\n") --
                so make "pure_lines" of them:
        *)
        let pure_lines = List.concat
                        (List.map Misc_utils.split_by_nls sb.body_lines)
        in
        if  tags <> []
        then
            if  is_all_ascii
            then
                (* Text MIME, ASCII lines, with tags: *)
                List.concat
                    (List.map
                    (cook_text_line ~strict_mode:strict_mode tags) pure_lines)
            else
                (* Text MIME, non-ASCII lines, with tags: the tags are ignored,
                   because non-ASCII lines need to be Base64-encoded:
                *)
                base64_encode sb.body_lines strict_mode
        else
            (* Text MIME, any lines, no tags:
               In any case, install line terminators and merge them together:
            *)
            let lts  = List.map (fun line -> line^"\r\n") pure_lines in

            if  is_all_ascii
            then
                (* The lines were actually ASCII: Just wrap them: *)
                List.map (fun line -> SMTP_Chunk line) lts
            else
                (* The lines were not ASCII: use Base64 encoding: *)
                base64_encode sb.body_lines strict_mode
    else
        (* Non-text MIME: the body is a binary (not lines); definitely use
           Base64 encoding:
        *)
        base64_encode sb.body_lines strict_mode


(*--------------------*)
(*  "cook_msg_part":  *)
(*--------------------*)
(*  NB "is_multi_part" flag means that this part belongs to a multi-part msg,
    NOT that its body itself is "Nested"!
*)
and
cook_msg_part: ~is_multi_part:bool  -> ~strict_mode:bool ->
               smtp_data -> list string -> mime_msg_part ->
               list smtp_data =

fun ~is_multi_part ~strict_mode parts_sep tags part ->

    (* (1) Decide the encoding to be used.  If the "part" has a "Simple" body,
           this is done by really scanning the body of the part; note that the
           MIME type might still  be "text/*", but that text is eg full Latin1
           rather than ASCII, so we have to use the Base64 encoding for it. If
           the body is "Nested", we don't produce the encoding header at all:
    *)
    let cont_type = match part.body with
    [
        Body_Simple sb   -> sb.mime_type
      | Body_Nested mstr -> string_of_comp mstr
    ]
    in
    let is_all_ascii = is_all_ascii part.body in
    (* NB: "is_all_ascii" has no meaning if the part is "Nested" *)

    let headers1  = match part.body with
    [
        Body_Simple _ ->
            let encoding   = if is_all_ascii then "7bit" else "base64" in
            [
                ("Content-Type", cont_type^"; charset=\"us-ascii\"");
                ("Content-Transfer-Encoding", encoding)
            ]
      | Body_Nested _ ->
            (* No "content-type" or "encoding" here. The necessary headers
               will be provided by the enclosed "mime_msg_struct":
            *)
            []
    ]
    in
    (* (2) The Disposition:
           Valid for both Simple and Nested Bodies:
    *)
    let headers2  = 
        if  is_multi_part
        then
            let cont_disp1 =
                if   part.disposition = Disp_Attachment
                then "attachment"
                else "inline"
            in
            let cont_disp2 =
                match part.body with
                [
                    Body_Simple sb ->
                        (* Provide "filename" if it's known,, but ONLY for
                           Attachments:
                        *)
                        if  sb.file_name     <> "" &&
                            part.disposition =  Disp_Attachment
                        then
                            cont_disp1^"; filename=\""^sb.file_name^"\""
                        else 
                            cont_disp1

                  | Body_Nested _  ->
                        cont_disp1  (* No "filename" *)
                ]
            in
            headers1 @ [("Content-Disposition", cont_disp2)]
        else
            (* This part is single -- we don't need any extra headers: *)
            headers1
    in
    (* (3) The Headers are ready, and terminated by the empty line,    UNLESS
           the Body is Nested -- in that case, the enclosed "smtp_msg_struct"
           will continue with its own headers:
    *)
    let headers_end = match part.body with
    [   Body_Simple _ -> [SMTP_Chunk "\r\n"]
      | Body_Nested _ -> []
    ]
    in  
    let cooked_headers =
        (cook_headers strict_mode tags headers2) @ headers_end in

    (* (4) Process the Body: *)

    let cooked_body    = cook_part_body
                         ~is_all_ascii:is_all_ascii
                         ~strict_mode :strict_mode
                         tags part.body
    in
    (* (5) Put the Separator (if it's not a Single part),  Headers and the
           Body together:
    *)
    if  is_multi_part
    then
        [parts_sep::cooked_headers] @ cooked_body
    else
        cooked_headers @ cooked_body


(*----------------------*)
(*  "cook_msg_struct":  *)
(*----------------------*)
and
cook_msg_struct: ~strict_mode:bool -> list string ->
                 mime_msg_struct   -> list smtp_data =

fun  ~strict_mode tags mstr ->
    (* Is the message a multi-part one? Any message can be declared "MultiPart"
       (either "_Mixed" or "_Alternative"), even if it has 0 or 1 part(s);  BUT
       if the message has more than 1 part, it cannot be declared "SinglePart":
    *)
    let is_multi_part =
        match mstr.composition with
        [
            MultiPart_Mixed       -> True
          | MultiPart_Alternative -> True
          | SinglePart  ->
                match mstr.parts with
                [
                    []  -> False
                  | [_] -> False
                  | _   -> raise (MIME_Error
                                 "\"SinglePart\" composition, multi-part msg")
                ]
        ]
    in
    (* What happens now, depends on the "is_multi_part" flag: *)
    if is_multi_part
    then
    do {
        (* XXX: we currently require that for the "Alternative" composition,
           all parts must be 'inline':
        *)
        if  (mstr.composition = MultiPart_Alternative) &&
            (List.exists (fun p-> p.disposition=Disp_Attachment) mstr.parts)
        then
            raise (MIME_Error
                  "Sorry, Attachments currently not allowed in Alternatives")
        else ();

        (* Multi-part struct:  need a Boundary to separate the Parts. We thus
           generate a Boundary value itself for the Headers,  the Parts Sepa-
           rator, and the Parts terminator:
        *)
        let boundary  = "------------"^
                        (Misc_utils.random_string "0123456789ABCDEF" 32)
        in
        let parts_sep = SMTP_Chunk ("--"^ boundary ^"\r\n")   in
        let parts_end = SMTP_Chunk ("--"^ boundary ^"--\r\n") in
        let comp      = string_of_comp mstr                   in

        let mstr_headers =
             [("Content-Type", comp^"; boundary=\""^boundary^"\"")]
        in
        (* Put the struct parts together.  The top-level headers are separated
           from the composite body by an empty line.  The "is_multi_part" flag
           is passed to the "cook_msg_part" function, so if the flag is "True"
           but the corresp part apprears to be declared "Single",  an error is
           raised:
        *)
        let top_headers  =
            (cook_headers strict_mode tags mstr_headers) @ [SMTP_Chunk "\r\n"]
        in
        let cooked_parts =
            (List.concat
            (List.map
                (cook_msg_part ~is_multi_part:is_multi_part
                               ~strict_mode  :strict_mode
                               parts_sep     tags)
                mstr.parts))
        in
        top_headers @ cooked_parts @ [parts_end]
    }
    else
        (* Single-part message:
           In this case, it contains extra headers which need to be merged with
           those already cooked, so there is no empty line between them:
        *)
        match mstr.parts with
        [
            []      -> []

          | [spart] -> cook_msg_part
                       ~is_multi_part: False
                       ~strict_mode  : strict_mode
                       (SMTP_Chunk "") tags  spart

          | _       -> assert False
        ];


(*---------------*)
(*  "cook_msg":  *)
(*---------------*)
(*  Takes an "smtp_msg_info" object and creates a "cooked" one, which is
    "almost" ready-to-send (up to tag substituitions):
*)
value cook_msg: smtp_msg_info -> smtp_msg_cooked =
fun   msg_info ->
do {
    (*  (0) Verify the tags:  *)
    List.iter (fun tag ->
                   if tag = "" then failwith "Mime_codec.cook_msg: Empty tag"
                               else ()
              ) msg_info.tags;

    (*  (1) Top Matter: Analyse "envel_from" and "body_from",  at least one of
            them must be non-"" in the "msg_info".  NB: these can be any valid
            e-mail addresses, e.g. including Sender's name:
    *)
    let (envel_from, body_from) =
        match (msg_info.envel_from, msg_info.body_from) with
        [
            ("", "") -> invalid_arg "msg_info: Source undefined"
          | (ef, "") -> (ef, ef)
          | ("", bf) -> (bf, bf)
          | (ef, bf) -> (ef, bf)
        ]
    in
    (* The "to" field must always be set. We parse it to get the domain name
       from it, and if parsing fails,  the "to" addr is assumed  to be a Tag
       for later substituitions, so no error is produced:
    *)
    let rcpt =  if   msg_info.msg_to = ""
                then invalid_arg "msg_info: Destination undefined"
                else msg_info.msg_to
    in
    (* (2) Form the top-level Headers.
           The following Headers are always defined:
           NB: it's OK here if "rcpt" contains Tags:
    *)
    let headers1 = [("From",        body_from);
                    ("To",          rcpt);
                    ("Date",        msg_info.date);
                    ("Message-Id",  msg_info.msg_id);
                    ("X-Mailer",    msg_info.mailer_ver)
                   ]
    in
    (* The "Subject" may or may not be defined. Priority is always defined: *)

    let (imp, prio, x_prio, x_msm_prio) =  match msg_info.priority with
    [
        MP_Lowest -> ("",     "",       "5 (Lowest)",  "Low")
      | MP_Low    -> ("",     "",       "4 (Low)",     "Low")
      | MP_Normal -> ("",     "",       "3 (Normal)",  "Normal")
      | MP_High   -> ("High", "",       "2 (High)",    "High")
      | MP_Highest-> ("High", "Urgent", "1 (Highest)", "High")
    ]
    in
    let headers2 = headers1 @ (List.concat
    [
        if   imp  <> "" then [("Importance", imp )] else [];
        if   prio <> "" then [("Priority",   prio)] else [];
        [("X-Priority",        x_prio)];

        if   msg_info.subject <> ""
        then [("Subject", msg_info.subject)]
        else [];

        [("MIME-Version", "1.0")]
    ])
    in
    let top_headers  = cook_headers msg_info.strict_lines
                                    msg_info.tags
                                    headers2
    in
    (* (3) Do the rest of the message; the "rest" may also include some other
           top-level headers, apart from those already cooked:
    *)
    let rest      = cook_msg_struct msg_info.strict_lines
                                    msg_info.tags
                                    msg_info.content
    in
    let all_data  = top_headers @ rest  in

    (* It may be a good idea to merge the adjacent textual chunks in the
       "all_data" -- but only if we are not in the strict line mode:
    *)
    let all_data' =
        if   msg_info.strict_lines
        then all_data
        else merge_chunks all_data (Buffer.create 16384) []
    in
    (* (4) The final result:
       In "cook_text_line" here,  the strict_mode is deliberately re-set to
       "False", as we use "SMTP_Line" wrapper explicitly:
    *)
    {
        envel_from' =
            SMTP_Line (cook_text_line ~strict_mode:False ~with_crlf:False
                                      msg_info.tags      envel_from);

        to_addr'    =
            SMTP_Line (cook_text_line ~strict_mode:False ~with_crlf:False
                                      msg_info.tags      rcpt);

        all_data'   = all_data'
    }
};

(*=============================================*)
(*  Final Messages: After Tags Substituition:  *)
(*=============================================*)
(*----------------------*)
(*  "mk_final_string":  *)
(*----------------------*)
(*  Converts an "SMTP_Data" object into the actual string to be sent.
    NB: No line terminators are appended for "SMTP_Line"s!
*)
type  string_map = Hashtbl.t string string;

value null_tags_map: string_map = Hashtbl.create 0;


value rec mk_final_string: string_map -> smtp_data -> string =
fun   tags_map  datum ->
    match datum with
    [
        SMTP_Chunk str ->
            (* A plain string chunk.  This is the most frequent case: *)
            str

      | SMTP_Tag   tag ->
            (* Try to substitute this tag. If it is not found in the map,
               the tag name itself is used. XXX: or better signal an error?
            *)
            try Hashtbl.find tags_map tag with [Not_found -> tag]

      | SMTP_Line frags->
            (* Simple concatenation of fragments is probably OK here: *)
            String.concat "" (List.map (mk_final_string tags_map) frags)
    ];


(*-------------------*)
(*  "mk_final_msg":  *)
(*-------------------*)
(*  As "mk_final_string", but for the whole msg. The validity of "from" and
    "to" e-mail addresses is checked, and the names removed -- they may not
    be suitable for the envelope:
*)

value mk_final_msg: string_map -> smtp_msg_cooked -> smtp_msg_final =
fun tags_map   cmsg ->
    let envel_from  =  mk_final_string tags_map cmsg.envel_from' in
    let envel_to    =  mk_final_string tags_map cmsg.to_addr'    in

    let (_, mbox_from, dom_from) = Misc_utils.parse_email_addr envel_from in
    let (_, mbox_to,   dom_to)   = Misc_utils.parse_email_addr envel_to   in
    {
        envel_from''   = mbox_from ^"@"^ dom_from;
        to_addr''      = mbox_to   ^"@"^ dom_to;
        to_domain''    = dom_to;
        all_data''     = List.map (mk_final_string tags_map) cmsg.all_data'
    };


(*==========================*)
(*  MIME Messages Parsing:  *)
(*==========================*)
(* TODO... *)

(*-------------------------*)
(*  "extract_smtp_lines":  *)
(*-------------------------*)
(*  [smtp_data] -> [string] converter. NB: Line terminators are removed: *)

value rec extract_smtp_lines': list smtp_data -> list string -> list string =
fun data curr  ->
    match data with
    [
        [] -> curr

      | [(SMTP_Chunk s)::dt] ->
            extract_smtp_lines' dt (curr @ [Misc_utils.strip_eol s])

      | [(SMTP_Line ds)::dt] ->
            extract_smtp_lines' dt (curr @ (extract_smtp_lines ds))

      | [(SMTP_Tag   t)::dt] ->
            (* No need for "strip_eol" here? *)
            extract_smtp_lines' dt (curr @ [Misc_utils.strip_eol t])
    ]

and extract_smtp_lines: list smtp_data -> list string =
fun data ->
    extract_smtp_lines' data [];


