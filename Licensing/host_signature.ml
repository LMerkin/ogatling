(*pp camlp4r *)
(* vim:ts=4:syntax=ocaml
*)
(*===========================================================================*)
(*                          "host_signature.ml":                             *)
(*          Provides Information for Issuing Node-Locked Licences            *)
(*                   (C) Explar Technologies Ltd, 2004                       *)
(*===========================================================================*)
print_endline (Enforcement.node_checksum_hex ());
exit 0;

