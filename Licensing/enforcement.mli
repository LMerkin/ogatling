(*pp camlp4r *)
(* vim:ts=4:syntax=ocaml
*)
(*===========================================================================*)
(*                            "enforcement.mli":                             *)
(*      Interface to Licence Enforcement Mechanisms for "Gatling Mail"       *)
(*                   (C) Explar Technologies Ltd, 2004                       *)
(*===========================================================================*)
(*----------------------------*)
(*  Cryptographic Machinery:  *)
(*----------------------------*)
(*  Conversion between human-readable (ASCII hex) and binary representation of
    cryptographic data. In "bin_of_list",   each list element must be an octet
    (0..255):
*)
value hex_of_bin : string   -> string;
value bin_of_hex : string   -> string;
value bin_of_list: list int -> string;

(*  Checksums (currently SHA1) for general (seralisable) OCaml objects, and in
    particular licences. For the latter, all fields except the "signature" are
    used in check-summing, so the licence must already be dated ("signed_on").
    Arg1 is the Licence File absolute path:
*)
value checksum_licence : string -> Licences.licence -> string;

(* Checksum of Node-specific data, in ASCII hex format: *)
value node_checksum_hex: unit   -> string;

(*-----------------------------------*)
(*  Top-Level Licence Verification:  *)
(*-----------------------------------*)
(*  First checks the licence authenticity, validity and integrity, then checks
    whether its conditions are satisfied. Currently, the program is terminated
    if any of these checks fail.
    Arg1: licence XML file;
    Arg3: time source function:
*)
value verify_licence: string -> (unit -> float) -> unit;

