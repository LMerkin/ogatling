(*pp camlp4r *)
(* vim:ts=4:syntax=ocaml
*)
(*===========================================================================*)
(*                            "enforcement.ml":                              *)
(*    Implementation of Licence Enforcement Mechanisms for "Gatling Mail"    *)
(*                  (C) Explar Technologies Ltd, 2004                        *)
(*===========================================================================*)
(*============================*)
(*  Cryptographic Machinery:  *)
(*============================*)
(*-----------------*)
(*  "hex_of_bin":  *)
(*-----------------*)
(*  Computes a human-readable ASCII hexadecimal representation of a binary
    string (e.g., of a hash code):
*)
value hex_of_bin: string -> string =
fun bin ->
    let len  = String.length bin       in
    let buff = Buffer.create (2 * len) in
    let hex  =
    do {
        for i = 0 to len-1
        do {
            Buffer.add_string buff (Printf.sprintf "%02X" (Char.code bin.[i]))
        };
        Buffer.contents buff
    }
    in do {
        Buffer.reset buff;
        hex
    };


(*-----------------*)
(*  "int_of_hex":  *)
(*-----------------*)
(*  Converts a single hex char into a binary value -- faster than using
    "scanf":
*)
value int_of_hex: char -> int =
fun
[   '0'->0 | '1'->1 | '2'->2 | '3'->3 | '4'->4 | '5'->5 | '6'->6 | '7'->7
|   '8'->8 | '9'->9 | 'A'->10| 'a'->10| 'B'->11| 'b'->11| 'C'->12| 'c'->12
|   'D'->13| 'd'->13| 'E'->14| 'e'->14| 'F'->15| 'f'->15|
_ -> invalid_arg "Enforcement.int_of_hex: Invalid hex digit"];


(*-----------------*)
(*  "bin_of_hex":  *)
(*-----------------*)
(*  The inverse of "hex_of_bin". The argument string must be of even length,
    consisting of ASCII representations of hexadecimal digits  (both upper-
    and lower-case letters are OK):
*)
value bin_of_hex: string -> string =
fun hex ->
    let len = String.length hex in
    if  len mod 2 <> 0
    then
        invalid_arg
        "Enforcement.bin_of_hex: Even number of hex digits required"
    else
    let rln = len / 2 in
    let res = String.create rln in
    do {
        for i = 0 to rln-1
        do {
            let j  = (i lsl 1) in   (* j = 2*i *)
            (* More significant hex digit is stored at lower string addr: *)
            let hi   = int_of_hex hex.[j]   in
            let lo   = int_of_hex hex.[j+1] in
            let code = (hi lsl 4) + lo      in
            res.[i] := Char.chr code
        };
        res
    };


(*----------------*)
(*  "checksum*":  *)
(*----------------*)
(* For SHA1, "checksum_len" is 20 bytes: *)
value checksum_len = 20;

(*  "string_of_a":
    Stringifies an arbitrary (non-closure, non-recursive) OCaml object:
*)
value string_of_a:   'a -> string =
fun a ->
    Marshal.to_string a [Marshal.No_sharing];

(*  "checksum_file":
    Read the given file in chunks, and feed them into a SHA1 hash. The
    function is invoked for its side-effect (updating the hash):
*)
value checksum_file: Cryptokit.hash -> string -> unit =
fun hash file_name ->
try
    let buff_size = 8192 in
    let buff = String.create buff_size in

    let ch   = open_in file_name       in
    let rec hash_chunks =
    fun () ->
        let  got =
            try  input ch buff 0 buff_size
            with [End_of_file -> 0]
        in
        if  got <= 0
        then ()
        else
        do {
            hash#add_substring buff 0 got;
            hash_chunks ()
        }
    in
    do {
        hash_chunks ();
        close_in ch;
    }
with
[hmm ->
    failwith ("Cannot read file: \""^file_name^": "^(Misc_utils.print_exn hmm))
];

(*  "checksum_licence":
    Extracts all fields from the "licence" except the previous "signature"
    (if any) but including the "signed_on" time, and feeds them into a hash.
*)
value checksum_licence: string -> Licences.licence -> string =
fun lfn lic ->
    let hash = Cryptokit.Hash.sha1 () in
    let dir  =
        if   Filename.is_relative lfn
        then failwith "Licence File Name must be absolute"
        else Filename.dirname     lfn
    in
    do {
        (* Hash the licence fields: *)
        hash#add_string (string_of_int lic.Licences.licence_id);
        hash#add_string (lic.Licences.product_name);
        hash#add_string (string_of_a   lic.Licences.issued_to);
        hash#add_string (string_of_a   lic.Licences.conditions);
        hash#add_string (lic.Licences.comments);
        hash#add_string (lic.Licences.signed_on);

        (* Also,  hash all the files mentioned in the licence. The files'
           locations are relative to that of the licence file, NOT to the
           CWD (if relative):
        *)
        List.iter
            (fun fn0 ->
                let fn1 =
                    if   Filename.is_relative fn0
                    then Filename.concat dir  fn0
                    else fn0
                in
                checksum_file hash fn1
            )
            lic.Licences.files;

        (* Return the hash value: *)
        let res = hash#result in
        do {
            assert (String.length res = checksum_len);
            res
        }
    };

(*------------------*)
(*  "bin_of_list":  *)
(*------------------*)
(*  Converts a list of char codes (0..255) into a single binary string:  *)

value bin_of_list:  list int -> string =
fun lst ->
    let len = List.length   lst in
    let s   = String.create len in
    let ()  =
        ignore
        (
            List.fold_left
            (fun i code ->
            do {
                s.[i] := Char.chr code;
                i+1
            })
            0 lst
        )
    in
    s;

(*------------------------*)
(*  "decrypt_signature":  *)
(*------------------------*)
(*  Decrypts (unwraps) the signature of Licence Conditions using a "public"
    (but still hidden deep inside the code) key. Here "cr_sign" is assumed
    to be in a binary (not ASCII hexadecimal) format.  The result is a bin
    check-sum to be compared with the actual checksum:
*)
value decrypt_signature: string -> string =
fun cr_sign ->
    (* First of all, the public key which will be used for decryption of the
       Licence Signature. It is stored as a list of ints to make it more dif-
       ficult to recognise it inside the program binary code:
    *)
    let key_size = 2048 in
    let key_n    =
        [0x5D; 0x60; 0x24; 0xBA; 0xE1; 0xE2; 0x6B; 0xC8;
         0xB1; 0x48; 0x25; 0x70; 0x77; 0xC5; 0xB4; 0xD3;
         0x7D; 0xFD; 0xB0; 0x10; 0xB3; 0xB2; 0xA1; 0xC4;
         0xDE; 0xD5; 0x05; 0x9A; 0xD2; 0xA0; 0x9A; 0xEB;
         0xA5; 0xD1; 0x45; 0x06; 0x24; 0x4A; 0x43; 0x66;
         0xB6; 0x59; 0xA2; 0x0C; 0xA8; 0x50; 0x3B; 0x58;
         0xF7; 0xE7; 0xF5; 0xA2; 0x87; 0x44; 0x06; 0x8B;
         0x65; 0xF7; 0xA8; 0x30; 0x75; 0x4F; 0x5D; 0xFA;
         0x67; 0xD4; 0x89; 0x5A; 0xD9; 0xC1; 0xCF; 0xF9;
         0x76; 0xC2; 0xD9; 0xE6; 0x02; 0x7B; 0xCA; 0xD8;
         0xDE; 0xB6; 0xCC; 0x68; 0x23; 0x62; 0x79; 0x93;
         0xF7; 0x0D; 0xA5; 0x1B; 0x19; 0xAC; 0x96; 0x8E;
         0xD4; 0x34; 0xAD; 0xD9; 0x26; 0xD4; 0x82; 0x09;
         0x9B; 0xD7; 0x9E; 0x87; 0x87; 0x1D; 0xD6; 0xCF;
         0xCC; 0x72; 0x86; 0x5E; 0x3B; 0xFC; 0x9D; 0xF1;
         0x28; 0x44; 0x38; 0xBF; 0xC2; 0x51; 0xD1; 0xA1;
         0xBF; 0x49; 0xF8; 0x20; 0x6D; 0xBC; 0x70; 0x46;
         0xB8; 0x0F; 0xD7; 0x49; 0xAA; 0x19; 0x39; 0x61;
         0x89; 0x9E; 0x47; 0x7D; 0x8E; 0x0E; 0x97; 0x07;
         0x84; 0x4A; 0x85; 0x07; 0x2A; 0xF9; 0xA8; 0xF5;
         0x35; 0xCA; 0xCB; 0x11; 0x30; 0x2B; 0x28; 0x05;
         0x5E; 0x07; 0x42; 0xDA; 0x87; 0x82; 0x17; 0x0F;
         0x04; 0x06; 0xAC; 0x39; 0xA7; 0x3C; 0x4F; 0x4E;
         0xB5; 0x30; 0x8D; 0x63; 0xFF; 0xD5; 0x25; 0x2C;
         0x7F; 0x10; 0xF9; 0x6F; 0xCD; 0x8B; 0xEF; 0x87;
         0x35; 0x6D; 0x09; 0x7A; 0x54; 0xA1; 0xB1; 0xCC;
         0x51; 0x96; 0x5A; 0x28; 0xDC; 0xE5; 0x90; 0x9F;
         0x48; 0x7A; 0xB0; 0xF6; 0x60; 0x8B; 0x45; 0xA4;
         0x17; 0x7C; 0x10; 0xBE; 0x25; 0x94; 0x8B; 0x53;
         0xA2; 0x17; 0x14; 0x36; 0xB1; 0x5B; 0xD4; 0xEE;
         0x37; 0x84; 0x1E; 0x34; 0xD8; 0x14; 0xFA; 0x04;
         0xEC; 0x94; 0x9D; 0x2B; 0x62; 0xD0; 0x8B; 0x85]
    in
    let key_e =
        [0x16; 0x1F; 0xC0; 0x54; 0x74; 0x65; 0x0F; 0x3E;
         0xC4; 0xCE; 0xCD; 0xFC; 0x6C; 0xF7; 0x75; 0x34;
         0xF1; 0x36; 0xE8; 0x74; 0xA7; 0x49; 0xA7; 0x07;
         0x22; 0x26; 0xB7; 0x0B; 0x03; 0xCB; 0x47; 0x09;
         0xB2; 0x83; 0xEA; 0x2A; 0x5F; 0x8B; 0x51; 0x1F;
         0x70; 0x39; 0x03; 0x0B; 0xB7; 0x0C; 0x46; 0xE9;
         0x38; 0x52; 0x97; 0xE4; 0x8E; 0x50; 0x87; 0xAE;
         0x0B; 0x65; 0x71; 0xCD; 0x9E; 0x3B; 0x55; 0x7A;
         0x37; 0xF7; 0xB2; 0xA6; 0xB9; 0xC9; 0x15; 0xCE;
         0xAB; 0x3C; 0xCC; 0xE7; 0xCB; 0x2A; 0x1E; 0x10;
         0x81; 0xAD; 0xC7; 0x5F; 0x9C; 0x6E; 0xD1; 0xA1;
         0x11; 0x63; 0x35; 0x81; 0x1A; 0x5C; 0x65; 0xEF;
         0xB6; 0x81; 0x53; 0x9B; 0xFC; 0xFA; 0x62; 0x09;
         0x22; 0xC0; 0xD9; 0xA5; 0x73; 0xB5; 0x6A; 0x08;
         0xA0; 0x7E; 0x6A; 0xC9; 0x65; 0x5A; 0x9C; 0x29;
         0x4F; 0x3D; 0xFB; 0xB5; 0x0E; 0x6D; 0x89; 0x45;
         0xE4; 0x4D; 0xAB; 0x8C; 0x94; 0x63; 0x51; 0x72;
         0xA1; 0x2B; 0xBA; 0xBB; 0x2F; 0x94; 0xD9; 0x6B;
         0x2D; 0xD9; 0x2F; 0x74; 0xFB; 0x05; 0xEA; 0x7F;
         0xE2; 0x88; 0x84; 0x38; 0xF6; 0x6F; 0x5E; 0x32;
         0x55; 0x31; 0x48; 0xE1; 0xB9; 0xE5; 0x49; 0xC1;
         0x57; 0x84; 0xEF; 0x67; 0x63; 0xEA; 0xB6; 0x29;
         0x7E; 0xB4; 0xD3; 0x13; 0x1E; 0x15; 0xAC; 0xD0;
         0xA6; 0x7E; 0x30; 0x0D; 0xC3; 0x1A; 0x95; 0x41;
         0x39; 0xAD; 0xB0; 0x83; 0x65; 0xF9; 0xFE; 0x76;
         0x39; 0x7D; 0x91; 0x35; 0x22; 0x2F; 0x70; 0x58;
         0x62; 0x2C; 0x5A; 0xDE; 0x22; 0xEC; 0xE9; 0x71;
         0x30; 0x8A; 0xC6; 0xD2; 0x5B; 0x75; 0x02; 0xA0;
         0x64; 0xB3; 0xFA; 0x50; 0xE9; 0xDC; 0xF3; 0xB2;
         0x26; 0xD1; 0xF9; 0x05; 0x39; 0xDC; 0xC5; 0x00;
         0x4D; 0xDB; 0xA8; 0xD7; 0xBA; 0xC1; 0xEC; 0x26;
         0x6E; 0x8D; 0x5B; 0xC7; 0x0D; 0x4F; 0x1D; 0xA5]
    in
    (* Build the public key from "n" and "e"; private components are left
       empty, as they are not used here:
    *)
    let key =
    {
        Cryptokit.RSA.size = key_size;
        Cryptokit.RSA.n    = bin_of_list key_n;
        Cryptokit.RSA.e    = bin_of_list key_e;
        Cryptokit.RSA.d    = "";
        Cryptokit.RSA.p    = "";
        Cryptokit.RSA.q    = "";
        Cryptokit.RSA.dp   = "";
        Cryptokit.RSA.dq   = "";
        Cryptokit.RSA.qinv = ""
    }
    in
    (* Get the binary value of the signature, and decrypt it: *)
    Cryptokit.RSA.unwrap_signature key cr_sign;


(*=======================*)
(*  Verification Tools:  *)
(*=======================*)
(*  Node info -- platform-specific: *)

external node_info: unit -> 'a =
    "enforcement_node_info";

(*  "node_checksum_hex":
    A check-sum of "low-level" "node_info". The result is immediately
    put into the ASCII hex format for human readability:
*)
value node_checksum_hex: unit -> string =
fun () ->
    let hash = Cryptokit.Hash.sha1 () in
    do {
        hash#add_string (node_info ());
        hex_of_bin      (hash#result)
    };

(*====================================*)
(*  Top-Level Licences Verification:  *)
(*====================================*)
(*----------------*)
(*  "eval_cond":  *)
(*----------------*)
(*  Evaluation of licence conditions: *)

value rec eval_cond: (unit -> float) -> Licences.cond -> bool =
fun time_fun cond ->
    match cond with
    [   (* Atomic Conds first: *)

        Licences.Valid_Since since ->
            let curr_date = time_fun () in
            let from_date = Date_time.parse_http_date since in
            curr_date >= from_date

    |   Licences.Valid_Until until ->
            let curr_date = time_fun () in
            let until_date= Date_time.parse_http_date until in
            curr_date < until_date

    |   Licences.Host_Signature cs ->
            node_checksum_hex () = cs

        (* Now Logical Exprs: *)

    |   Licences.And conds -> List.for_all (eval_cond time_fun) conds
    |   Licences.Or  conds -> List.exists  (eval_cond time_fun) conds
    |   Licences.Not cond  -> not          (eval_cond time_fun  cond)
    ];

(*---------------------*)
(*  "verify_licence":  *)
(*---------------------*)
(*  Verifies the licence integrity via its digital signature, and then checks
    the licence conditions.   The outcome of checking in an error message (if
    any); an action is taken if it is the case.
    The function used to get the current absolute time is given as a parm; it
    should normally be "Secure_time.gmtime" if the time environment cannot be
    trusted, or "Unix.gettimeofday" otherwise:
*)
value verify_licence:  string -> (unit -> float) -> unit =
fun licence_xml_file time_fun ->
    let err_msg =
    try
        (* First of all, read in and parse the "licence_file": *)
        let cnnl    = open_in licence_xml_file   in
        let licence =
        try
            let strm    = Stream.of_channel cnnl in
            let xstr    = IoXML.parse_xml   strm in
            Licences.xparse_licence  xstr
        with
            [hmm ->
            do {
                close_in cnnl;
                raise hmm    (* Propagate the exception *)
            }]
        in
        let () = close_in cnnl in

        (* Compute the check-sum of the Licence File itself, and of all Product
           files mentioned in it.  This ensures that the licence can be applied
           only to the specified product items,   and that those items (such as
           binary executable files) cannot be altered by the user:
        *)
        let chsm1 = checksum_licence licence_xml_file licence  in

        (* Decrypt the check-sum stored in the licence itself: *)
        let chsm2 = decrypt_signature (bin_of_hex licence.Licences.signature) in

        (* This check-sum is padded; remove the padding: *)
        let pad_start = Cryptokit.Padding.length#strip chsm2    in
        let chsm3     = String.sub chsm2
                        (pad_start - checksum_len) checksum_len in

        (* Do they match? *)
        if  chsm1 <> chsm3
        then "Invalid licence signature"
        else
        (* The licence itself seems to be OK, verify its conditions: *)
        if  eval_cond time_fun licence.Licences.conditions
        then
             "" (* Seems to be OK... *)
        else "Mis-applied or expired licence"
    with
    [hmm ->
        (* Any other error during licence verification: *)
        Misc_utils.print_exn hmm
    ]
    in
    (* Verification done -- what to do now? *)
    if  err_msg <> ""
    then
        (* Verification failed. Currently, we just terminate the program: *)
        Misc_utils.fatal_error
        ("LICENCE VERIFICATION FOR \""^licence_xml_file^"\" FAILED: "^err_msg)
    else
        (); (* All done: *)

