(*pp camlp4r *)
(* vim:ts=4:syntax=ocaml
*)
(*===========================================================================*)
(*                              "secure_time.mli":                           *)
(*                     (C) Explar Technologies Ltd, 2004                     *)
(*===========================================================================*)
(*  "gmtime":
    Get the current GMT time (in seconds from the Epoch) from secure public
    Internet sources. A time-out is specified, to be used in these requests
    (in seconds):
*)
value gmtime: float -> float; (* req_timeout -> time_since_epoch *)

