(*pp camlp4r *)
(* vim:ts=4:syntax=ocaml
*)
(*===========================================================================*)
(*                              "secure_time.ml":                            *)
(*                                                                           *)
(*  A reasonably secure, low-precision  (typical error of the order of secs  *)
(*  or mins) time source implementation, suitable for use in enforcing time- *)
(*  restricted software licences.                                            *)
(*                                                                           *)
(*  Does NOT use any local (e.g. "gettimeofday") or NTP mechanisms, as they  *)
(*  are prone to malicious alteration. Rather, it gets time from secure pub- *)
(*  lic HTTP servers via the "Date:" header.                                 *)
(*                                                                           *)
(*                     (C) Explar Technologies Ltd, 2004                     *)
(*===========================================================================*)
(*==================*)
(*  Configuration:  *)
(*==================*)
(* The following servers have been checked manually for reasonable accuracy
   of time supplied via the "Date:" headers. We store them through their IPs
   rather than symbolic names, to make it more difficult to identify them in
   the program code:
*)
value https_time_servers =
[|
    (209, 132,  177, 50);   (* www.redhat.com     *)
    ( 65, 205,  249, 60);   (* www.verisign.com   *)
    ( 64,  12,  187, 22);   (* www.aol.com        *)
    (216,  46,  96, 115);   (* www.equifax.com    *)
    (208, 254,  45, 200);   (* www.geotrust.com   *)
    ( 65, 205, 248, 243);   (* www.thawte.com     *)
    (128,  11, 100, 130);   (* www.visa.com       *)
    (216, 126, 201,  89);   (* www.usertrust.com  *)
    ( 62,  65, 122, 202);   (* www.addtrust.com   *)
    (213, 159, 130,  76);   (* www.baltimore.com  *)
    (193, 194, 157, 129);   (* www.trustcenter.de *)
    ( 47, 249,  48,  20);   (* www.nortel.com     *)

    (216, 109, 117, 107);   (* www.yahoo.com      *)
    (216, 109, 117, 207);
    (216, 109, 118,  65);
    (216, 109, 118,  66);
    (216, 109, 118,  67);
    (216, 109, 118,  73);
    (216, 109, 118,  76);
    (216, 109, 118,  78);

    (207,  68, 173, 245);   (* www.hotmail.com    *)
    (207,  68, 171, 233);
    (207,  68, 172, 239);

    ( 66, 102,  11,  99);   (* www.google.com     *)
    ( 66, 102,  11, 104)
|];

value n_servers = Array.length https_time_servers;

(* Here the results are stored by each HTTPS Client thread: *)
value gmtimes: array float = Array.make (n_servers) 0.0;

(* Setting the GMT Time Zone: *)
external set_gmt_zone: unit -> unit = "set_gmt_zone";

(*==================*)
(*  "time_client":  *)
(*==================*)
(*  The body of each HTTPS client thread.  NB: it still relies on the LOCAL
    clock for time-outs, but there is no point for the user to distort this
    clock: if the secure time mechanism fails, the software will NOT run!
*)
(*----------------*)
(*  "write_ssl":  *)
(*----------------*)
(* Writes a string into an SSL socket: *)

value rec write_ssl_n:
    Ssl.socket   -> string -> int -> float -> unit =
fun ssl str from deadline ->
    (* Try to write data to the end of "str", but the actual number of bytes
       written will be "n":
    *)
    let max_n  = (String.length str) - from             in
    let n      = Ssl.write ssl str from max_n deadline  in
    if  n <= 0 || n > max_n
    then
        failwith "Secure_time.write_ssl_n: SSL write failed"
    else
    if  n = max_n
    then
        () (* All done *)
    else
        write_ssl_n ssl str (from + n) deadline;

value write_ssl  : Ssl.socket -> string -> float -> unit =
fun ssl str deadline ->
    write_ssl_n ssl str 0 deadline;

(*-------------------*)
(*  "get_date_val":  *)
(*-------------------*)
(*  Read the headers, and return the string value of the "Date: " headers if
    exists, or raise an exception otherwise:
*)
value rec get_date_val:
    Ssl.socket -> Misc_utils.line_reader Ssl.socket -> float -> string =

fun ssl reader deadline ->
    (* Use "invalid_deadline" as a placeholder -- the default deadline has
       already been set:
    *)
    let line = reader#recv_line ssl deadline in
    if  line = ""
    then
        (* The required header has not been found: *)
        failwith "Missing \"Date: \" header"
    else
    if  ExtString.String.starts_with line "Date: "
    then
        (* That's what we need: *)
        ExtString.String.strip (String.sub line 6 (String.length line - 6))
    else
        (* Try more... *)
        get_date_val ssl reader deadline;

(*-----------------------*)
(*  Client Thread Body:  *)
(*-----------------------*)
(*  The arg is the number of the server to use, in the "https_time_servers".
    NB: we don't use the standard "Http_client" or "Clients_simple" modules
    here, as we need just a very limited subset of HTTPS functionality:
*)
value time_client: int -> float -> unit =
fun i deadline ->
    let gmt =
    try
        (* Connect to Port 443 (HTTPS) of the resp server: *)
        let serv_ip   = https_time_servers.(i)              in
        let serv_addr = Misc_utils.inet_addr_of_ip4 serv_ip in
        let sock_addr = Unix.ADDR_INET serv_addr  443       in
        let ssl       = Ssl.mk_socket (Ssl.create_client_context Ssl.SSLv23) in
        let ()        = Ssl.open_connection ssl sock_addr deadline in

        let gmt'      =
        try
            (* Create the reader, with a 2K buffer: *)
            let ssl_reader = new Misc_utils.line_reader Ssl.read 2048 in

            (* Start the dialogue:
               Use HTTP/1.0, as we then don't need to send the "Host:" header,
               and thus don't need to store the domain names of the servers --
               for higher security:
            *)
            let () = write_ssl ssl "GET / HTTP/1.0\r\n\r\n" deadline  in
            let dt = get_date_val ssl ssl_reader deadline
            in
            Date_time.parse_http_date dt
        with
        [_ -> 0.0]    (* If any error occurred *)
        in
        do {
            (* Clean-up: close the connection: *)
            Ssl.shutdown_connection ssl deadline;
            gmt'
        }
    with
    [_ -> 0.0] (* Any top-level error: also zero time: *)
    in
    (* Save the "gmt" value in the "gmt_times" array:  *)
    gmtimes.(i) := gmt;

(*-------------*)
(*  "gmtime":  *)
(*-------------*)
(*  The user-visible function: *)
value gmtime: float -> float =
fun timeout ->
do {
    (* Calculate the deadline (using the LOCAL clock) for the HTTPS reqs: *)
    let deadline = Unix.gettimeofday() +. timeout in

    (* Create the HTTPS client threads.   For them, we will use the highest
       priority available with the current type of scheduling:
    *)
    let (_, max_prio) = Tnio.priority_range ()
    in
    for i = 0 to (n_servers-1)
    do {
        ignore (Tnio.thread_create
                ~stack_size_k:128
                ~priority:max_prio
                (fun () -> time_client i deadline))
    };
    (* Start the threads and wait for their completion: *)
    Tnio.threads_scheduler ();

    (* Average all non-zero results: *)
    let sum = ref 0.0 in
    let n   = ref 0   in
    do {
        for i = 0 to n_servers-1
        do {
            if  gmtimes.(i) <> 0.0
            then
            do {
                n.val  := n.val   +  1;
                sum.val:= sum.val +. gmtimes.(i)
            }
            else ();
        };

        (* Have we got at least one real result: *)
        if  n.val = 0
        then
            failwith "Secure_time.gmtime: All servers failed"
        else
            sum.val /. (float_of_int n.val)
    }
};
