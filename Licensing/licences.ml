(*pp camlp4r pa_ioXML.cmo *)
(* vim:ts=4:syntax=ocaml
*)
(*===========================================================================*)
(*                              "licences.ml":                               *)
(*           Licensing Policies and Mechanisms for "Gatling Mail"            *)
(*                    (C) Explar Technologies Ltd, 2004                      *)
(*===========================================================================*)
(* Atomic conditions over which the Licence Terms are built, and any Boolean
   expressions over them:
*)
type address  =
{
    street_address  : string;
    locality        : string;
    state_province  : string;
    post_code       : string;
    country         : string
};

type person =
{
    title           : string;
    first_name      : string;
    middle_initial  : string;
    last_name       : string;
    position        : string;
    contact_phone   : string;
    contact_fax     : string;
    contact_email   : string
};

type licencee =
{
    company_name    : string;
    company_address : address;
    company_contact : person
};

type cond =
[
    (* Absolute GMT Time (in the format "YYYY-MM-DD hh:mm:ss GMT")
       the licence is valid since:
    *)
    Valid_Since    of string

|   (* Similar time, the licence is valid until: *)
    Valid_Until    of string

    (* Locking to the node (host) -- the exact meaning of the Host Signature
       is platform-specific:
    *)
|   Host_Signature of string

    (* Logical exprs -- n-ari "And" and "Or" are provided for convenience: *)

|   And of list cond
|   Or  of list cond
|   Not of      cond
];

(* The licence fields:  NB: "signed_on" and "signature"  are automatically
   generated each time the licence is digitally signed.     In particular, 
   all checksum-protected files (also acting as a collective Product Code)
   are mentioned here. If any file is given by a relative path, it must be
   relative to the location of the licence file:
*)
type licence =
{
    licence_id  : int;
    product_name: string;
    issued_to   : licencee;
    conditions  : cond;
    files       : list string;
    comments    : string;
    signed_on   : string;
    signature   : string
};

