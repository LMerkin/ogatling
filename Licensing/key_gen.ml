(*pp camlp4r *)
(* vim:ts=4:syntax=ocaml
*)
(*===========================================================================*)
(*                               "key_gen.ml":                               *)
(*   RSA Key Generation with OCaml CryptoKit for Explar Techn Ltd Products   *)
(*                    (C) Explar Technologies Ltd, 2004                      *)
(*===========================================================================*)
open Cryptokit.RSA;

(* Generate the key; its components are stored as binary strings, with
   randomly-chosen exponent "e":
*)
value key  = new_key 2048;

(* Print it in a human-readable format, with components as lists: *)
value buff = Buffer.create 4096;

value to_hex bins =
    let () = Buffer.clear buff  in
    let () = Buffer.add_string buff "[" in
    let len= String.length bins in
    do {
        for i = 0 to len-1
        do {
            Buffer.add_string buff
                (Printf.sprintf "0x%02X" (Char.code bins.[i]));
            if  i <> len-1
            then Buffer.add_string buff "; "
            else ()
        };
        Buffer.add_char buff ']';
        Buffer.contents buff
    };

Printf.printf
"size = %d\nn = %s\ne = %s\n\nd = %s\np = %s\nq = %s\ndp= %s\ndq =%s\nqinv=%s\n"
    key.size
    (to_hex key.n)
    (to_hex key.e)
    (to_hex key.d)
    (to_hex key.p)
    (to_hex key.q)
    (to_hex key.dp)
    (to_hex key.dq)
    (to_hex key.qinv);

exit 0;

