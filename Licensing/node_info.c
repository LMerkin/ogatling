// vim:ts=4:syntax=c
//===========================================================================//
//                               "node_info.c":                              //
//       Host (Node) -Specific Information for Licensing Verification        //
//                   (C) Explar Technologies Ltd, 2004                       //
//===========================================================================//
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdio.h>

#include <caml/mlvalues.h>
#include <caml/alloc.h>
#include <caml/fail.h>

value enforcement_node_info (value unit)
{
#   if defined(__i386__) || defined(__x86_64__)
    // PC x86 and Compatible Hardware Platforms:
    // Use BIOS information at addresses F0000-100000 (8K) for host
    // identification:
    const int   bios_addr = 0xFE000;
    const int   bios_len  =  0x2000;
    value res = alloc_string (bios_len);

#   ifdef __linux__
    // Use "/dev/mem" for reading the BIOS data into "res":
    //
    int mfd = open ("/dev/mem", O_RDONLY);
    if (mfd < 0)
        failwith ("Enforcement.node_info: Cannot open \"/dev/mem\"");

    if (lseek (mfd, bios_addr, SEEK_SET)       != bios_addr ||
        read  (mfd, String_val(res), bios_len) != bios_len)
        failwith ("Enforcement.node_info: Cannot read hardware data");

    close (mfd);
#   else
#   error "This OS is not supported yet"
#   endif
    return res;

#   else
#   error "This platform is not supported yet"
#   endif
}
