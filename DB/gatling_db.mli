(*pp camlp4r *)
(* vim:ts=4:syntax=ocaml
*)
(*===========================================================================*)
(*                              "gatling_db.mli":                            *)
(*           Interface to the DataBase SubSystem for Gatling Mail            *)
(*                    (C) Explar Technologies Ltd, 2004                      *)
(*===========================================================================*)
(*=============*)
(*  DB Layout: *)
(*=============*)
(* Table names -- currently NOT user-configurable: See the body of the module
   for the actual names:
*)
value session_table: string;
value mlists_table : string;
value extras_table : string;

(*------------------*)
(*  Session Table:  *)
(*------------------*)
(*  Currently consists of only 1 column and 1 row. Contains the curr SessionID.
    Statistics of past sessions can be added here in the future  -- e.g., per-
    domain failure rates.
*)
value sid_fld : string;

(*-----------------------*)
(*  Mailing Lists Table: *)
(*-----------------------*)
(*  Fixed format -- the field names and types are given by these names: *)

value email_fld     : string;       (* "EMail" and "ListName" pair is a key  *)
value list_name_fld : string;
value crypt_fld     : string;       (* "Crypt" is also a key    *)
value status_fld    : string;       (* 0 when subscription is in progress;
                                       otherwise, can be used for "sublists" *)
value subscr_fld    : string;       (* Date/Time when subscribed             *)
value subscr_ip_fld : string;       (* IP from which subscribed *)
value cfails_fld    : string;       (* Number of consecutive send failures   *)
value last_fail_fld : string;       (* SessionID of the last failure         *)

(* Number and poistions of the standard EMail and Crypt fields: *)
value email_fld_pos : int;
value crypt_fld_pos : int;
value std_flds      : list string;
value flds_sep      : char;

(*----------------*)
(*  Extra Table:  *)
(*----------------*)
(*  Contains the "email_fld" as the primary key. Other fields are user-managed,
    with the names supplied by the user, and containing  (from the OCaml point
    of view) "string" data.
*)

(*-----------*)
(*  "init":  *)
(*-----------*)
(*  Initialises the DB component with a logging function: *)
value init: ~log_fun:(string -> unit) -> unit;

(*=============*)
(*  DB Handle: *)
(*=============*)
(* An implementation-dependent type: *)
type db_handle = 'a;

(*-------------*)
(*  "open_db": *)
(*-------------*)
(*  Creates a DB Handle:  *)
value open_db: Db_types.db_access -> db_handle;

(*---------------*)
(*  "close_db":  *)
(*---------------*)
(* Closes a DB Handle: *)
value close_db: db_handle -> unit;

(*==================*)
(*  Main Functions: *)
(*==================*)
(*------------------*)
(*  "unload_mlist": *)
(*------------------*)
value unload_mlist:
    ?extra_flds : list string ->
    ?extra_conds: string      ->
    db_handle ->
    string    ->
    string    ->
    unit;

(*---------------------*)
(*  "remove_entry"  :  *)
(*  "remove_entries":  *)
(*---------------------*)
(*  The "rem_file" contains entries to be removed, one on each line.   Entries
    can be given by "EMail" (in that case "list_name" matters), and/or "Crypt"
    (then "list_name" is ignored):
*)
type mlist_key =
[ Crypt      of string
| EMail_List of string and string
];

value remove_entry:     db_handle ->  mlist_key        -> unit;

value remove_entries:
    db_handle -> ~rem_file:string -> ~list_name:string -> unit;

(*------------------------*)
(*  SessionID management: *)
(*------------------------*)
value  get_session_id: db_handle -> int;
value incr_session_id: db_handle -> unit;

(*------------------*)
(*  "mark_failed":  *)
(*------------------*)
(*  Arg2: SessionID; arg3: Key.  When this function is invoked, it is assumed
    that the SessionID is the last one  (it is not read automatically as this
    function is normally invoked multiple times as a call-back),   and that a
    failure has occurred during this session on the entry identified by Crypt.
    Depending on whether there was a failure also in the PREVIOUS or THIS ses-
    sion, the fail counter is either incremented or set as below, and the last
    failure time is set to this SessionID. If the failure was not in the prev-
    ious or this session but earlier than (SessionID - check_sessions), it is
    reset to 1, otherwise unchanged:
*)
value mark_failed:
    db_handle        -> ~sid:int -> ~key:mlist_key -> ~check_sessions:int ->
    ~strict_5xx:bool -> unit;

(*-------------------*)
(*  "mark_bounced":  *)
(*-------------------*)
(*  Similar to "marke_failed", but takes e-mail addrs (with a given MList)
    from a Bounces File. The file is locked during the operation, and pos-
    sibly truncated at the end (if "reset_file" is on):
*)
value mark_bounced:
    db_handle -> ~sid:int -> ~list_name :string ->
    ~file_name   :string  -> ~reset_file:bool   -> ~check_sessions:int ->
    ~our_envelope:string  -> unit;

(*-----------------*)
(*  "reset_succ":  *)
(*-----------------*)
(*  Typically invoked before a new session is started (before incrementing the
    SessionID).   All entries which have no recorded failures withing the LAST
    session (either direct or through bounces), are now presumed to be success-
    ful in that session, and their ConsFails LastFail counters are re-set.
    IMPORTANT:   it might be that the typical delivery time of bounces exceeds
    the gap between sessions, in that case,   we can only re-set fail counters
    on entries which have been error-free within the LAST "n" SESSIONS (n >=1,
    and is user-defined; 0 to disable this operation):
*)
value reset_succ:
    db_handle -> ~list_name:string -> ~check_sessions:int -> unit;

(*------------------*)
(*  "sweep_failed": *)
(*------------------*)
(*  All entries within the given mailing list, with ConsFails >= the given
    threshold, are removed from the list. If the threshold is <= 0, no ac-
    tion is taken:
*)
value sweep_failed:
    db_handle -> ~list_name:string -> ~thresh:int -> unit;

