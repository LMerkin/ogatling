(*pp camlp4r pa_ioXML.cmo *)
(* vim:ts=4:syntax=ocaml
*)
(*===========================================================================*)
(*                               "db_types.ml":                              *)
(*                 XML Data Types for Gatling DB SubSystem                   *)
(*                    (C) Explar Technologies Ltd, 2004                      *)
(*===========================================================================*)
(*  Currently supported DBMSs:
    MySQL, PostgreSQL, TDS-based (Sybase and MS SQL), SQLite.
    NB: Oracle support will require a commercial UNIX-ODBC driver.
*)
(*----------------*)
(*  "db_access":  *)
(*----------------*)
type db_access =
{
    dsid     : string;      (* ODBC DataSetID *)
    user     : string;      (* Username for access, "" for SQLite *)
    passwd   : string       (* Passwd   for access, "" for SQLite *)
};

