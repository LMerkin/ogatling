(*pp camlp4r *)
(* vim:ts=4:syntax=ocaml
*)
(*===========================================================================*)
(*                              "gatling_db.ml":                             *)
(*       DataBase SubSystem for Gatling Mail and Related Products:           *)
(*                   (C) Explar Technologies Ltd, 2004--2005                 *)
(*===========================================================================*)
open ExtString; (* Replaces the standard "String" by an "ExtLib" one *)

(*=============*)
(*  DB Layout: *)
(*=============*)
(* This implementation is based on the OCaml binding to UNIX-ODBC. Thus, all
   DB access parms are configured via the ODBC interface,  outside the OCaml
   system.
   Table names -- currently NOT user-configurable, but should be known to the
   user:
*)
value session_table = "Session";
value mlists_table  = "MLists";
value extras_table  = "ExtraData";

(*------------------*)
(*  Session Table:  *)
(*------------------*)
(*  Currently consists of only 1 column and 1 row. Contains the curr SessionID.
    Statistics of past sessions can be added here in the future  -- e.g., per-
    domain failure rates.
*)
value sid_fld       = "SessionID";

(*-----------------------*)
(*  Mailing Lists Table: *)
(*-----------------------*)
(*  Fixed format -- the field names and types are given here:   *)

value email_fld     = "EMail";      (* "EMail" and "ListName" pair is a key  *)
value list_name_fld = "ListName";
value crypt_fld     = "Crypt";      (* "Crypt" is also a key    *)
value status_fld    = "StatusCode"; (* 0 when subscription is in progress;
                                       otherwise, can be used for "sublists" *)
value subscr_fld    = "Subscribed"; (* Date/Time when subscribed             *)
value subscr_ip_fld = "SubscrIP";   (* IP from which subscribed *)
value cfails_fld    = "ConsFails";  (* Number of consecutive send failures   *)
value last_fail_fld = "LastFail";   (* SessionID of the last failure         *)

(* Positions of the EMail and Crypt fields in the text output file -- must be
   known to other modules:
*)
value email_fld_pos = 0;
value crypt_fld_pos = 1;
value std_flds      = [email_fld; crypt_fld];
value status_5xx    = 500;

(* A record in the DB can be identified either by a single key (Crypt), or by
   a combination of the EMail and ListName:
*)
type mlist_key =
[ Crypt      of string
| EMail_List of string and string
];

(* Fields separator in text output files: *)
value flds_sep      = '\t';

(*----------------*)
(*  Extra Table:  *)
(*----------------*)
(*  Contains the "email_fld" as the primary key. Other fields are user-managed,
    with the names supplied by the user, and containing  (from the OCaml point
    of view) "string" data.
*)

(*-----------*)
(*  "init":  *)
(*-----------*)
(*  The module is initilised with a logging function: *)

value log: ref (string -> unit) = ref (fun _ -> ());

value init: ~log_fun:(string -> unit) -> unit =
fun ~log_fun ->
    log.val := log_fun;


(*=============*)
(*  DB Handle: *)
(*=============*)
(*  Currently, all supported DBs are using the UNIX-ODBC interface: *)
type db_handle = Ocamlodbc.connection;

(*-------------*)
(*  "open_db": *)
(*-------------*)
(*  Creates a DB Handle:  *)

value open_db: Db_types.db_access -> db_handle =
fun dba ->
    Ocamlodbc.connect
        dba.Db_types.dsid
        dba.Db_types.user
        dba.Db_types.passwd;

(*---------------*)
(*  "close_db":  *)
(*---------------*)
(* Closes a DB Handle: *)

value close_db: db_handle -> unit = Ocamlodbc.disconnect;

(*---------------*)
(*  "check_rc":  *)
(*---------------*)
value check_rc: string -> string -> int -> unit =
fun fname req rc ->
    if  rc <> 0
    then
        let msg = Printf.sprintf "Gatling_db.%s: SQL ERROR (RC=%d) in \"%s\""
                  fname rc req
        in
        do {
            log.val  msg;
            failwith msg
        }
    else ();

(*==================*)
(*  "unload_mlist": *)
(*==================*)
value unload_mlist:
    ?extra_flds : list string ->
    ?extra_conds: string      ->
    db_handle ->
    string    ->
    string    ->
    unit      =

fun ?(extra_flds=[]) ?(extra_conds="") dbh list_name out_file ->
    (* The generic SQL request. The syntax depends on whether 1 or 2 tables
       are used. If there are "extra_conds" present,  we must always assume
       that we have 2 tables -- XXX:  this can result in less data produced
       if for some entries in "MLists", there are no corr "Extras" entries:
    *)
    (* Fields: *)
    let flds  =
        if  extra_flds = [] && extra_conds = ""
        then
            email_fld^", "^crypt_fld
        else
            String.concat ", "
            (
                [mlists_table^"."^email_fld;
                 mlists_table^"."^crypt_fld]
                @
                (List.map (fun f -> extras_table^"."^f) extra_flds)
            )
    in
    (* Tables: *)
    let tabs  =
        if   extra_flds = [] && extra_conds = ""
        then mlists_table
        else mlists_table^", "^extras_table
    in
    (* Conditions:  *)
    (* Set the list name. Also, fields fith StatusCode=0 are automatically
       ignored, as they correspond to incompleted subscriptions:
    *)
    let conds0 =
        if  extra_flds  = [] && extra_conds = ""
        then
            Printf.sprintf "%s = '%s' and %s <> 0"
                list_name_fld list_name     status_fld
        else
            (* Don't forget to join the tables by EMail! *)
            Printf.sprintf "%s.%s = '%s' and %s.%s <> 0 and %s.%s = %s.%s"
                mlists_table  list_name_fld list_name
                mlists_table  status_fld
                mlists_table  email_fld     extras_table  email_fld
    in
    (* If "extra_conds" are present: install them: *)
    let conds1 =
        if   extra_conds = ""
        then conds0
        else conds0^" and ("^extra_conds^")"
    in
    (* The request: *)
    let req =
        Printf.sprintf "select %s from %s where %s" flds tabs conds1 
    in
    let ()  = log.val ("\nUNLOADING THE MAILING LIST FROM DB:\n"^req^"\n") in

    (* Open the destination file. We want to lock it against concurrent
       writing by mistake:
    *)
    let fd  =
        Option.get (Misc_utils.open_write_locked ~append_mode:False out_file)
    in
    (* The call-back function which is invoked on blocks of records fetched,
       and saves them in the given file:
    *)
    let sep = String.of_char flds_sep in
    let saver =
        fun recs ->
            List.iter
            (fun r -> Misc_utils.write_fstr fd ((String.concat sep r)^"\n"))
            recs
    in
    try
        (* Read the DB:
           Execute the query; the block-size of records to be used is
           highly empirical -- use 100 for the moment:
        *)
        let (rc, _) =
            Ocamlodbc.execute_gen dbh ~get_info:False ~n_rec:100 req saver
        in
        do {
            check_rc "unload_mlist" req rc;
            Unix.close fd
        }
    with
    [hmm ->
     do {
        Unix.close fd;
        raise hmm
    }];

(*========================*)
(*  "Remove" Facilities:  *)
(*========================*)
(*-------------------*)
(*  "remove_entry":  *)
(*-------------------*)
(*  Just removes an individual entry from the List:
*)
value remove_entry: db_handle -> mlist_key -> unit =
fun dbh key ->
    (* Make the request: *)
    let req =
        match key with
        [ Crypt crypt ->
            Printf.sprintf
                "delete from %s where %s='%s'"
                mlists_table crypt_fld crypt

        | EMail_List email mlist ->
            Printf.sprintf
                "delete from %s where %s='%s' and %s='%s'"
                mlists_table email_fld email list_name_fld mlist
        ]
    in
    (* Log and run the request: *)
    let () = log.val req in
    try
        let (rc, _) = Ocamlodbc.execute dbh req in
        check_rc "remove_entries" req rc
    with
    [ hmm ->
      do {
        log.val
        ("Gatling_db.remove_entry: ERROR: "^(Misc_utils.print_exn hmm));
        raise hmm
    } ];

(*---------------------*)
(*  "remove_entries":  *)
(*---------------------*)
(*  Provides a facility for manually removing entries from a DB. The list
    of entrie sto remove is given (line-by-line) in a file   specified by
    Arg1; Arg2 is ListName. The entries may be  "EMail"s and/or "Crypt"s;
    they are distinguished automatically.  For "Crypt"s, the ListName has
    no effect:
*)
value remove_entries:
    db_handle -> ~rem_file:string -> ~list_name:string -> unit =

fun dbh ~rem_file ~list_name ->
    if  rem_file = "" || list_name = ""
    then
        () (* Nothing to do *)
    else
    (* Open the removals list file: *)
    let inch =
        try  open_in rem_file
        with [_ -> failwith
                    ("Gatling_db.remove_entries: Cannot open file \""^
                    rem_file^"\"")
             ]
    in
    let () = log.val "\nREMOVING THE UNSUBSCRIBED ENTRIES:\n" in
    try
        (* Read the file line-by-line: *)
        let rec remove_each: unit -> unit =
        fun () ->
            let (line, eof) =
                try  (String.strip (input_line inch), False)
                with [_ -> ("", True)]
            in
            if  eof
            then
                ()  (* All done *)
            else
            (* Generate a removal SQL request. It depends on whether
               the line represents an EMail or a Crypt.  Currently,
               Crypts are Base64-encoded ("almost"),  so they can't
               contain '@',  hence the way to distinguish them from
               EMails:
            *)
            let is_email =
                try  let _ = String.index line '@' in True
                with [Not_found -> False]
            in
            do {
                try
                    if   is_email
                    then remove_entry dbh (EMail_List line list_name)
                    else remove_entry dbh (Crypt      line)
                with
                [ _ -> () (* Any error: already logged: continue *) ];

                (* Next: *)
                remove_each ()
            }
        in
        do {
            remove_each  ();
            log.val    "\n";
            close_in  inch
        }
    with
    [hmm ->
     do {
        log.val ("Gatling_db.remove_entries: TOP-LEVEL ERROR: "^
            (Misc_utils.print_exn hmm)^"\n");
        close_in inch;
        raise hmm
    }];

(*====================*)
(*  "get_session_id": *)
(* "incr_session_id": *)
(*====================*)
(*  SessionID is used for tracking CONSECUTIVE errors: *)

value  get_session_id: db_handle -> int  =
fun dbh ->
    let req = Printf.sprintf "select %s from %s" sid_fld session_table
    in
    let (rc, res) = Ocamlodbc.execute dbh req in
    do {
        check_rc "get_session_id" req rc;

        (* The ID to return: *)
        match res with
        [ [[sid]] -> int_of_string sid (* Always not a "NULL" *)
        | _       -> assert False]
    };


value incr_session_id: db_handle -> unit =
fun dbh ->
    let req = Printf.sprintf
              "update %s set %s=%s+1" session_table sid_fld sid_fld
    in
    let (rc, _) = Ocamlodbc.execute dbh req in
    check_rc "incr_session_id" req rc;


(*==================*)
(*  "mark_failed":  *)
(*==================*)
(*  For a given SessionID and a Key (Crypt or EMail/ListName), either increment
    the ConsFails counter, or set it to 1, or keep it unchanged,   depending on
    the SessionID of the last failure and the CheckSessions depth:
*)
value mark_failed:
    db_handle        -> ~sid:int  -> ~key:mlist_key -> ~check_sessions:int ->
    ~strict_5xx:bool -> unit      =

fun dbh ~sid ~key ~check_sessions ~strict_5xx ->
    (* Get the curr number of failures and the last fail session ID for this
       entry:
    *)
    let req1 =
        match key with
        [ Crypt crypt ->
            Printf.sprintf "select %s, %s from %s where %s='%s'"
                cfails_fld last_fail_fld mlists_table crypt_fld crypt

        | EMail_List email mlist ->
            Printf.sprintf "select %s, %s from %s where %s='%s' and %s='%s'"
                cfails_fld last_fail_fld mlists_table
                email_fld  email  list_name_fld mlist
        ]
    in
    let (curr_cf, curr_lf) =
        let (rc1, res1) = Ocamlodbc.execute dbh req1 in
        do {
            check_rc "mark_failed-1" req1 rc1;
            try
                match res1 with
                [ [[c;l]] -> (int_of_string c, int_of_string l)
                | _       -> assert False]
            with
            [ _ -> (* Beware of "NULL"s! *)
                (0, 0)
            ]
        }
    in
    (* The new number of cons failures. A failure is deemed consecutive iff it
       occurred in the previous session, or indeed earlier in this session:
    *)
    let new_cf =
        if  curr_cf >= 1 && curr_lf >= sid-1
        then
            (* There was indeed a failure during the LAST (or THIS) session: *)
            curr_cf+1
        else
            (* No failure during the LAST session, yet there was one now. Here
               we the new number of cons failure is to be reset to 1, but only
               if such a reset is allowed,    i.e. we are far enough away from
               the last failure (had at least "check_sessions" error-free ses-
               sions). Otherwise, the number will stay unchanged:
            *)
            if   sid - curr_lf > check_sessions || curr_lf = 0
            then 1
            else curr_cf
    in
    (* Update the failure data. In the Strict_5XX mode, also set StatusCode
       to "status_5xx":
    *)
    let req2 =
        match key with
        [ Crypt crypt ->
            if  strict_5xx
            then
                Printf.sprintf
                   "update %s set %s=%d, %s=%d, %s=%d where %s='%s'"
                    mlists_table
                    cfails_fld    new_cf
                    last_fail_fld sid
                    status_fld    status_5xx
                    crypt_fld     crypt
            else
                Printf.sprintf
                   "update %s set %s=%d, %s=%d where %s='%s'"
                    mlists_table
                    cfails_fld    new_cf
                    last_fail_fld sid
                    crypt_fld     crypt

        | EMail_List email mlist ->
            if  strict_5xx
            then
                Printf.sprintf
                   "update %s set %s=%d, %s=%d, %s=%d where %s='%s' and %s='%s'"
                    mlists_table
                    cfails_fld    new_cf
                    last_fail_fld sid
                    status_fld    status_5xx
                    email_fld     email
                    list_name_fld mlist
            else
                Printf.sprintf
                   "update %s set %s=%d, %s=%d where %s='%s' and %s='%s'"
                    mlists_table
                    cfails_fld    new_cf
                    last_fail_fld sid
                    email_fld     email
                    list_name_fld mlist
        ]
    in
    let ()       = log.val  req2   in
    let (rc2, _) = Ocamlodbc.execute dbh req2 in
    check_rc "mark_failed-2" req2 rc2;


(*=================*)
(*  "reset_succ":  *)
(*=================*)
(*  See the module interface for description: *)

value reset_succ:
    db_handle -> ~list_name:string -> ~check_sessions:int -> unit =

fun dbh ~list_name ~check_sessions ->
    if  check_sessions <= 0
    then (* Nothing to do: *)
        ()
    else
    let last_sid = get_session_id dbh in
    let req      =
        Printf.sprintf
        "update %s set %s=0, %s=0 where %s='%s' and %s <= %d"
                    mlists_table  cfails_fld last_fail_fld
                    list_name_fld list_name  last_fail_fld
                    (last_sid - check_sessions)
    in
    let () = log.val ("\nRESETTING ERRORS ON SUCCESSFUL ENTRIES:\n"^req^"\n")
    in
    (* Run the request: *)
    let (rc, _)   = Ocamlodbc.execute dbh req in
    check_rc "reset_succ" req rc;


(*===================*)
(*  "sweep_failed":  *)
(*===================*)
(*  All entries within the given mailing list, with ConsFails >= the given
    threshold, are removed from the list. If the threshold is <= 0, no ac-
    tion is taken:
*)
value sweep_failed: db_handle -> ~list_name:string -> ~thresh:int -> unit =
fun dbh ~list_name ~thresh    ->
    if  thresh <= 0
    then
        () (* Exit now, otherwise ALL entries will be removed! *)
    else
    let req = Printf.sprintf
              "delete from %s where %s='%s' and %s >= %d"
              mlists_table list_name_fld list_name cfails_fld thresh
    in
    let ()  = log.val ("\nSWEEPING PERSISTENTLY-FAILING ENTRIES:\n"^req^"\n")
    in
    (* Run the request: *)
    let (rc, _) = Ocamlodbc.execute dbh req in
    check_rc "sweep_failed" req rc;


(*===================*)
(*  "mark_bounced":  *)
(*===================*)
(*  Analyses a Bounces File, extracts the bounced e-mail addresses, and marks
    them as failed in the DB:
*)
value mark_bounced:
    db_handle -> ~sid:int -> ~list_name:string ->
    ~file_name   :string  -> ~reset_file:bool  -> ~check_sessions:int ->
    ~our_envelope:string  -> unit =

fun dbh ~sid ~list_name ~file_name ~reset_file ~check_sessions ~our_envelope ->

    let ()       = log.val "\nMARKING THE BOUNCED ENTRIES:\n" in
    let (fd, ch) =
    try
        if not reset_file
        then
            (* If reset is not requested, open the file for reading only, don't
               lock it, and don't use the low-level FD:
            *)
            (Tnio.invalid_file_descr, open_in file_name)
        else
            (* Otherwise: open it for R/W: *)
            let fd = Unix.openfile file_name [Unix.O_RDWR] 0 in
            (* Lock it: *)
            let () = Unix.lockf fd Unix.F_LOCK 0             in
            (* Get a channel out of it:    *)
            (fd, Unix.in_channel_of_descr fd)
    with
    [_ -> failwith
          ("Gatling_db.mark_bounced: Cannot open file: \""^file_name^"\"")
          (* At this moment, there should be no descr leaks:
             an error could only be in "open", not in "lock"...
          *)
    ]
    in
    (* The following function accumulates failed addrs: *)
    let bounced_emails = Hashtbl.create 200000 in

    let rec read_lines:  unit -> unit =
    fun curr_lst ->
        let (line, eof) =
            try  (input_line ch, False)
            with [End_of_file -> ("", True)]
        in
        if   eof
        then ()  (* All input done *)
        else
        do {
            if  ExtString.String.starts_with line "To: " ||
                ExtString.String.starts_with line "X-Failed-Recipients: "
            then
                (* Our own "envelope-from" addr can appear many times in
                   the bounces file -- exclude it:
                *)
                match Misc_utils.extract_email_addr line with
                [ Some email when email <> our_envelope ->
                    Hashtbl.replace bounced_emails email None
                | _ -> 
                    ()
                ]
            else ();
            (* Tail-recursive call: *)
            read_lines  ()
        }
    in
    let () = try do {
        (* Apply the above function -- get all bounced e-mails: *)
        read_lines ();

        (* To reduce locking time, do file operations first, although  if the
           file is truncated and then this program instance crashes, the data
           would be lost:
        *)
        if  reset_file
        then
        do {
            Unix.ftruncate fd 0;
            log.val ("FILE \""^file_name^"\" TRUNCATED\n")
        }
        else ()
    }
    with
        (* Ignore errors here -- they are extremely unlikely,  we just set up
           this try block to exclude any possibility of failure in a critical
           section:
        *)
        [_ ->()]
    in
    do {
        (* Unlock and close the file. Close both the FD and the channel: *)
        if  reset_file
        then
        do {
            (ignore)     (Unix.lseek   fd 0 Unix.SEEK_SET);
            Unix.lockf fd Unix.F_ULOCK 0;
            Unix.close fd;
        }
        else ();
        close_in ch;

        (* Get the last SID -- the errors will be attributed to that session: *)
        let sid = get_session_id dbh in

        (* Mark all "bounced_emails". XXX: at the moment, we don't extract the
           corresp SMTP error codes, so we assume all errors to be NON-permnt,
           and the Strict_5XX mode is Off:
        *)
        Hashtbl.iter
            (fun email _ ->
             try mark_failed
                    dbh sid (EMail_List email list_name) check_sessions False
             with[ _ -> ()] (* Error is logged, so continue *)
            )
            bounced_emails
    };

