(*pp camlp4r *)
(* vim:ts=4:syntax=ocaml
*)
(*===========================================================================*)
(*                             "back_end_gen.mli":                           *)
(*                                                                           *)
(*                 Gatling/Lukol MetaSearch Engine Back-End:                 *)
(*          Interface to General-Purpose and XML-Based Search Engines        *)
(*                                                                           *)
(*                     (C) Explar Technologies Ltd, 2004                     *)
(*===========================================================================*)
(*-------------------*)
(* The Request Type: *)
(*-------------------*)
(*  Common for both kinds of Search Engines, only "extra_info" is ignored for
    XML-Based ones:
*)
type url_data = (string * string * list Http_common.http_cookie);
                (*  (CurrURL, PrevURL (Referer), CurrCookies) *)

type se_req   =
{
    search_terms: string; (* Search terms, currently just a string *)
    n_results   : int;    (* The number of results required *)
    client_ip   : Unix.inet_addr;          (* End-User's IP *)
    extra_info  : Hashtbl.t char url_data  (* Key:  SE_Code *)
};

(*-------------------*)
(*  The Result Type: *)
(*-------------------*)
(*  Individual result type for General-Purpose SEs:
    NB: "from" is [(SearchEngineName, SearchEngineCode. SearchEngineURL)], as
    the same result can come from multiple SEs:
*)
type se_res_gen =
{
    from    : mutable list   (string * string);
                      (* [(SE_Name,SE_URL)] *)
    url     : string; (* Search result URL -- real page location  *)
    title   : string; (* Ditto, title       *)
    descr   : string; (* Ditto, description *)

    (* The following fields are used for sorting the results: *)
    nurl    : string; (* Normalised "url" for uniq comparison *)
    index   : float;  (* Position of this result in this SE's output, from 1 *)
    weight  : float   (* Weight of the corresp SE *)
};

(* Individual result for XML-Based SEs: *)
type se_res_xml =
{
    xtitle  : string; (* Title *)
    xdescr  : string; (* Descr *)
    xurl    : string; (* Real URL of the page found    *)
    xnurl   : string; (* Normalised Real URL           *)
    xclick  : string; (* Click-through URL for the result found *)
    revenue : float   (* Advertising revenue from this result   *)
};

(* Integrated Result Type: *)
type se_ress =
[   (*  Results from a General-Purpose Search Engine: SE name, list of actual
        results, SE code, URL data for future requests:
    *)
    Res_Gen of string and list se_res_gen and char and url_data

|   (*  Results from an XML-Based Search Engine: SE name, list of results,
        list of related Search Terms:
    *)
    Res_XML of string and list se_res_xml and list string
];

(*-------------------------*)
(*  Module initialisation: *)
(*-------------------------*)
(*  To be invoked after "Lukol_config" has been initialised: *)
value init:
    Lukol_config.se_interfaces_gen   ->     (* General-Purpose SE Interfaces *)
    Lukol_config.se_interfaces_xml   ->     (* XML-Based       SE INterfaces *)
    ~be_worker_prio:int              ->     (* Worker thread priority        *)
    (string -> unit) ->                     (* Log function  *)
    unit;

(*  The server type provided -- "Gen_server" operations are available on it.
    Also included is the server's name:
*)
type se_state  = 's;
type be_server = ((Gen_server.server_t se_req se_state se_ress) * string);

(*----------------------*)
(*  Access to Servers:  *)
(*----------------------*)
(*  General-Purpose servers are accessed by their uniq 1-char codes;
    XML-Based servers are all in a single list:
*)
value gen_server_by_code: char -> be_server;
value all_gen_servers:    unit -> list (char * be_server);
value all_xml_servers:    unit -> list be_server;

(*---------*)
(*  Misc:  *)
(*---------*)
(*  Reasonable PCRE match limit: *)
value match_limit: int;

(* "update_engines":
   Fetches new SE Interface data from the Master Site, and saves them in the
   local config files.   The server needs to be restarted for the changes to
   take effect.
   Args: {"Gen" | "XML"}, deadline :
*)
value update_engines: string -> float -> unit;

