(*pp camlp4r *)
(*===========================================================================*)
(*                              "highlight.mli":                             *)
(*                     (C) Explar Technologies Ltd, 2004                     *)
(*===========================================================================*)
(*  "st_high_light":
    A part of the Front-End HTTP server functionality:
*)
value st_high_light:
    Tnio.tsock           ->     (* Client-Connected Socket *)
    Http_server.http_req ->     (* Client Request    *)
    (string -> unit)     ->     (* Log Function      *)
    float                ->     (* Deadline          *)
    unit;

