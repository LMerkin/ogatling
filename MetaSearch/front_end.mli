(*pp camlp4r *)
(* vim:ts=4:syntax=ocaml
*)
(*===========================================================================*)
(*                              "front_end.mli":                             *)
(*                    (C) Explar Technologies Ltd, 2004                      *)
(*===========================================================================*)
(* HTTP and HTTPS server states -- abstract: *)
type fe_state_http  = 'a;
type fe_state_https = 'b;

(*  "init":  *)
value init:
    ~fe_acceptor_prio :int ->           (* Acceptor Priority *)
    ~fe_worker_prio   :int ->           (* HTTP Worker  Prio *)
    ~ssl_acceptor_prio:int ->           (* Admin (SSL) Acceptor Priority *)
    ~ssl_worker_prio  :int ->           (* Admin SSL   Worker   Priority *)
    (string -> unit) ->                 (* Log Function      *)
    ((Tcp_server.server_t Tnio.tsock fe_state_http ) *   (* HTTP  Server *)
     (Tcp_server.server_t Ssl.socket fe_state_https)     (* HTTPS Server *)
    );
