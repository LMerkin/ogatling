(*pp camlp4r pa_ioXML.cmo *)
(* vim:ts=4:syntax=ocaml
*)
(*===========================================================================*)
(*                            "lukol_config.ml":                             *)
(*    Definition of XML Data Types for Gatling/Lukol MetaSearch Engine:      *)
(*                   (C) Explar Technologies Ltd, 2004                       *)
(*===========================================================================*)
(*=======================*)
(*  Config Data Types:   *)
(*=======================*)
(*-----------------------*)
(*  "front_end_config":  *)
(*-----------------------*)
(*  Front-End (our HTTP server) configuration -- much resembling "Http_server.
    server_config":
*)
type front_end_config =
{
    (* TCP parameters: *)
    our_domain:             string;
    listen_on_ip:           string; (* "0.0.0.0" if any IP *)
    listen_on_port:         int;
    listen_queue_len:       int;
    max_connections:        int;
    sock_buff_size:         int;    (* Size of Kernel I/O buffer for RCV/SND *)

    (* HTTP server threads parameters: *)
    max_idle_http_threads:  int;
    min_idle_http_threads:  int;
    init_http_threads:      int;

    (* HTTP parameters:
       "session_timeout"  is the max duration  of any connection  to the HTTP
       server, and the max number of requests which can be channelled through
       it; "search_timeout" applies to processing of each individual request,
       and similar for "highlight_timeout":
    *)
    session_timeout:        float;
    session_max_reqs:       int;
    search_timeout:         float;
    highlight_timeout:      float;

    (* Parameters for the HTTPS (admin) server: *)
    secure_port:            int;
    pub_cert_file:          string;
    priv_key_file:          string;
    admin_passwd:           string;

    (* Page templates for the front page and for results presentation, see the
       sample templates for more details about the tags allowed:
    *)
    front_template_file:    string;
    stats_template_file:    string;
    results_template_file:  string;

    (* How many results per page are displayed:  *)
    page_size:              int;

    (* Increment of pages in the Pages Bar:      *)
    pages_quant:            int;

    (* How many results from XML search engines are displayed (before all): *)
    top_xml_results:        int;

    (* Front page for the Admin's Interface:     *)
    admin_page_file:        string;

    (* Page sent on error (e.g., too many client connections: *)
    error_page_file:        string
};

(*-----------------------*)
(*  "se_interface_gen":  *)
(*-----------------------*)
(*  Interface configuration for a particular Search Engine. This only includes
    "intrinsic" parms pertinent to the Engine itself,  not to how it used in a
    particular client set-up. Thus, it can be updated if necessary (e.g., when
    the output format of a Search Engine changes) without affecting local con-
    figuration:
*)
type se_interface_gen =
{
    (* Common name, e.g. "Google": *)
    gse_name:           string;

    (* Domain to connect to, e.g. "www.google.com": *)
    gse_domain:         string;

    (* The home domain of this Search Engine: *)
    gse_home:           string;

    (* Other hosts which may be required in search (e.g., for the next
       pages --  it's better, though not strictly necessery,  to  know
       them in advance:
    *)
    gse_other_hosts:    list string;

    (* Request path format: XXX: this will be more complex when advanced
       search is introduced; currently it's just a string containing the
       template parms "${st}" (Search_Term) and "${pn}" (Page Number):
    *)
    gse_req_format:     string;

    (* PCRE RegExps to be used for parsing the results. The sub-patterns
       corresponding  to the parts  of the result  must be labelled as:-
       ?P<URL>, ?P<TITLE>, ?P<DESCR>:
    *)
    gse_parser:         string;

    (* How to get the URL of the next page og search results: *)
    gse_next:           string
};

type se_interfaces_gen = list se_interface_gen;

(*-------------------------*)
(*  "se_user_config_gen":  *)
(*-------------------------*)
(*  Specifies how a partucular Search Engine  is used within the MetaSearch
    Engine (or not used at all, if it is present in "se_interfaces_gen" but
    not in the set of "se_user_config_gen"s):
*)
type se_user_config_gen =
{
    (*  The following ref is used to identify this Search Engine in the
        "se_interfaces_gen" list:
    *)
    gse_name_ref:       string;

    (* There is also a 1-letter code uniquely identifying this Search Engine
       in HTTP requests:
    *)
    gse_code:           char;

    (* Weight  (the larger number is the higher priority)  for using the
       results from this Search Engine in the merged result:
    *)
    gse_weight:         float;

    (* Numbers of threads for this Search Engine: *)
    gse_max_threads:        int;

    (* Client IPs to bind to (randomly; ["0.0.0.0"] if any IP is OK): *)
    gse_client_ips:     list string
};

(*-----------------------*)
(*  "se_interface_xml":  *)
(*-----------------------*)
(*  Interface to an XML-based (often "pay-per-click") Search Engine:  *)

type se_interface_xml =
{
    (* XML Search Engine Name:  *)
    xse_name:           string;
    xse_domain:         string;

    (* URL template for making requests. May contain the following parms:
       "${st}"          (Search Terms),
       "${nr}"          (Number of Results),
       "${ip}"          (End-User's IP):
       "${affid}"       (Any extra parms specifying the Affiliate ID of the
                        MetaSearch site)
    *)
    xse_req_format:     string;

    (* Tag names in XML results: *)
    all_results_tag:    string;
    each_result_tag:    string;
    title_tag:          string;
    descr_tag:          string;
    orig_url_tag:       string;
    click_url_tag:      string;
    abs_revenue_tag:    string;
    all_related_tag:    string;
    each_related_tag:   string
};

type se_interfaces_xml = list se_interface_xml;

(*-------------------------*)
(*  "se_user_config_xml":  *)
(*-------------------------*)
(*  User-defined configuration for accessing XML-based Search Engines. Those
    Engines which are present in "se_interfaces_xml"  but not in the  set of
    "se_user_config_xml"s, will not be used:
*)
type se_user_config_xml =
{
    xse_name_ref:       string; (* Refers to an "xse_name" as above    *)
    xse_affid:          string; (* Affiliate ID  for using this XML SE *)
    xse_commission:     float;  (* Affiliate's commission rate payable *)
    xse_max_threads:    int;    (* Number of Back-End threads to use   *)
    xse_client_ips:     list string         (* Client IPs to be used   *)
};

(*----------------------*)
(*  "back_end_config":  *)
(*----------------------*)
(*  Configuration for the "Gen_server" whose threads are interfaced to the
    actual General-Purpose and XML-Based Search Engines:
*)
type back_end_config =
{
    (* How long the pre-fetched results are kept in the global cache  (sec): *)
    cache_ttl:          float;

    (* User-independent "se_interfaces_gen" data: stored in a separate file: *)
    gse_interfs_file:   string;

    (* The Master URL where Gen Interfaces updates can be found: *)
    gse_interfs_src :   string;

    (* User-independent "se_interfaces_xml" data: stored in a separate file: *)
    xse_interfs_file:   string;

    (* The Master URL where XML Interfaces updates can be found: *)
    xse_interfs_src :   string;

    (* Browsers we are emulating (or pretending to be: *)
    emulate_browsers:   list string;

    (* The step (in sec) of response time distribution functions, for perfor-
       mance statistics:
    *)
    resp_distr_step:    float;

    (* User's configs for General-Purpose Search Engines: *)
    gse_uconfs:         list se_user_config_gen;

    (* User's configs for XML-Based       Search Engines: *)
    xse_uconfs:         list se_user_config_xml
};

(*------------------------*)
(*  Self-Testing Config:  *)
(*------------------------*)
type self_test_config =
{
    test_period     : float;  (* Seconds *)
    test_out_file   : string; (* "" if the self-test results are not saved *)
    test_search_term: string  (* Must be non-"" *)
};

(*-------------------------*)
(*  "meta_search_config":  *)
(*-------------------------*)
(*  Over-all configuration of the Lukol MetaSearch Engine: *)

type meta_search_config =
{
    (* Some global parms: *)
    licence_file    : string;
    log_file        : string;
    pid_file        : string;
    run_as_user     : string;
    run_as_group    : string;
    dns_servers     : list string;
    daemon_mode     : bool;
    trace_mode      : bool;

    (* Front-End config: *)
    fe_config       : front_end_config;

    (* Back-End  config: *)
    be_config       : back_end_config;

    (* Self-Test config: *)
    st_config       : self_test_config
};

(*=============================*)
(*  Configuration Management:  *)
(*=============================*)
(*------------------*)
(*  "init_config":  *)
(*------------------*)
(*  Sets up global configuration from an XML file. It's probably a fatal
    error if this cannot be done:
*)
value global_config_ref: ref (option meta_search_config) = ref None;

value init_config: string -> unit =
fun xml_conf ->
try
    let cnnl = open_in xml_conf in
    try
        let strm  = Stream.of_channel cnnl  in
        let xstr  = IoXML.parse_xml   strm  in
        do {
            global_config_ref.val :=  Some (xparse_meta_search_config xstr);
            close_in cnnl
        }
    with
    [ hmm ->
      do {
        close_in cnnl;
        raise hmm
    }]
with
[hmm ->
    Misc_utils.fatal_error
        (Printf.sprintf "ERROR: Invalid XML Config File: \"%s\": %s\n%!"
        xml_conf (Misc_utils.print_exn hmm))
];

(*---------------------------*)
(*  Servicing Config Parms:  *)
(*---------------------------*)
(*  This can be done once "init_config" has been invoked: *)
value get_global_conf: unit -> meta_search_config =
fun () ->
    match global_config_ref.val with
    [ Some c -> c
    | None   ->
        Misc_utils.fatal_error "Global Config not yet initialised"
    ];

