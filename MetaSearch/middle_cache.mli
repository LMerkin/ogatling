(*pp camlp4r *)
(* vim:ts=4:syntax=ocaml
*)
(*===========================================================================*)
(*                            "middle_cache.mli":                            *)
(*  Middle-Tier Session-Based Cache for the Gatling/Lukol MetaSearch Engine  *) 
(*                    (C) Explar Technologies Ltd, 2004                      *)
(*===========================================================================*)
(*------------------*)
(*  "get_results":  *)
(*------------------*)
(*  Obtains the results from the Cache and from the Search Engines, replenish-
    ing the Cache transparently whenever necessary:
*)
type search_res =
{
    gen_page : list Back_end.se_res_gen;    (* General-Purpose results page *)
    gen_total: int;                         (* Total of Gen. results so far *)
    xml_page : list Back_end.se_res_xml;    (* XML-Based results, this page *)
    rel_terms: list string;                 (* Related Search Terms         *)
    s_expir  : float                        (* Abs session expiration time  *)
};

value get_results:
    string          ->  (* Search  Terms   *)
    int             ->  (* Results Page No *)
    Unix.inet_addr  ->  (* Client  IP      *)
    string          ->  (* General-Purpose SE codes *)
    float           ->  (* Deadline        *)
    search_res;

(*-----------*)
(*  "init":  *)
(*-----------*)
(*  Initialises the Middle-Tier Cache:  *)

value init:
    ~purger_prio:int ->                 (* Purger Thread Priority *)
    (string -> unit) ->                 (* Log  Function *)
    unit;

(*---------------*)
(*  Statistics:  *)
(*---------------*)
(*  For each General-Purpose or XML-Based SE, we maintain:
    -- the total number of reqs made to it;
    -- the number of completed  reqs (without an exception);
    -- the number of successful reqs (with non-[] results);
    -- an (N+1)-step distribution density for the response time of all
       completed (but not necesserily successful) reqs; N steps within
       the time-out (N >= 1), the last one for reqs exceeding it:
*)
type se_stats =
{
    se_http_reqs    : mutable int;
    completed_reqs  : mutable int;
    succ_reqs       : mutable int;
    cum_http_time   : mutable float;
    resp_time_distr : array   int
};

type mc_stats =
{
    (* Timestamp (seconds from the Epoch): *)
    stats_begin     : mutable float;

    (* Total number of user search requests, and their cumulative resp
       time:
    *)
    user_reqs       : mutable int;
    cum_resp_time   : mutable float;

    (* The number of General-Purpose and XML-Based requests which can be
       satisfied from the Cache:
    *)
    cached_reqs     : mutable int;

    (* The number of parallel request "salvos" fired at the actual SEs;
       it can even be greater that "user_reqs", as a user-level req w/
       a long result demanded, can yield multiple "salvos". The actual
       number of SEs involved in each "salvo" is user-specified, so it
       may be different:
    *)
    salvos          : mutable int;

    (* Step (in sec) for each Search Engine's resp time distribution: *)
    distr_step      : mutable float;

    (* SE stats: map SE_Name => SE_Stats, single table for both Gen and
       XML servers:
    *)
    se_stats        : Hashtbl.t string se_stats
};

(* Stats-related functions: *)

value get_stats  : unit -> mc_stats;
value reset_stats: unit -> unit;

