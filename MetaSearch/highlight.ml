(*pp camlp4r *)
(* vim:ts=4:syntax=ocaml
*)
(*===========================================================================*)
(*                              "hihglight.ml":                              *)
(*       Search Terms HighLighting for Gatling/Lukol MetaSearch Engine:      *)
(*                    (C) Explar Technologies Ltd, 2004                      *)
(*===========================================================================*)
(*  XXX: HighLighting style is currently non-configurable:  *)

(*--------------------------*)
(*  Search Terms Analysis:  *)
(*--------------------------*)
(*  "space_re" is for parsing of a search terms string into individual Terms.
    When we then build a regexp to recognise there terms, special symbols in
    the terms (those which may conflict with regexp reserved symbols) need to
    be escaped; they are detected and escaped using "special_re":
*)
value space_re   =
    Pcre.regexp ~study:True ~limit:Back_end.match_limit
    "\\s+";

value special_re =
    Pcre.regexp ~study:True ~limit:Back_end.match_limit
    "[\\[\\]$.|()?*+{}^\\\\-]";

(*---------*)
(*  "hl":  *)
(*---------*)
(*  Just highlights an individual word: *)
value hl: string -> string =
fun s ->
    "<b style=\"background-color: #FFFF66; color:#000000\">"^s^"</b>";

(*------------------*)
(*  "mk_preamble":  *)
(*------------------*)
value mk_preamble: list string -> string =
fun terms ->
    let hl_terms = String.concat " " (List.map hl terms) in
    String.concat "\n"
    [
        "<table border=1 width=100%>";
        "<tr><td>";
        "<table border=1 bgcolor=white cellpadding=10 cellspacing=0 ";
        "width=100% color=black>";
        "<tr><td align=center>";
        "<font color=black size=-1 face=arial>";
        "The following search terms have been highlighted: ";
        hl_terms;
        "</font></td></tr></table>";
        "</td></tr></table>"
    ];

(*----------------------*)
(*  "mk_absolute_url":  *)
(*----------------------*)
value mk_absolute_url: string -> Http_client.parsed_url -> string =
fun url purl ->
    let (proto, host, port, path_etc) = purl in
    let phost =
        if  (proto = "http"  && port = 80 ) ||
            (proto = "https" && port = 443)
        then
            (* Standard port, no need to specify: *)
            host
        else
            (* Specify the port expicitly: *)
            Printf.sprintf "%s:%d" host port
    in
    if  List.exists
        (fun pr -> ExtString.String.starts_with url pr)
        ["http://"; "https://"; "ftp://"; "mailto:"]
    then
        (* The URL is already absolute: *)
        url
    else
    try
    (* The URL is relative, and in this case, as it is found in HTML, it is
       probably just a path, without host name. The path itself can be abso-           lute or relative:
    *)
    if  url.[0] = '/'
    then
        (* Absolute path: *)
        Printf.sprintf "%s://%s%s" proto phost url
    else
    (* Relative path.  Convert it into an absolute one using the directory
       part of the current "path" (which MUST contain at least one "/", as
       we made a full HTTP req for it, and must begin with a "/"):
    *)
        let spos = String.rindex path_etc '/'        in
        let dir  = String.sub    path_etc 0 (spos+1) in
        Printf.sprintf "%s://%s%s%s" proto phost dir url
    with
    [_ ->
        (* Don't know how to re-write it, keep it as is! *)
        url
    ];

(*-----------------*)
(*  "within_tag":  *)
(*-----------------*)
(*  Checks whether a given match is inside an HTML tag -- in that case, it
    is not to be highlighted:
*)
value within_tag: Pcre.substrings -> string -> bool =
fun matches whole ->
    let (pos_from, pos_until) = Pcre.get_substring_ofs matches 0 in

    (* Go to the right of "pos_until", and if a '>' is encountered before
       a '<', it's a tag boundary:
    *)
    let len = String.length whole in

    let rec go_right: int -> bool =
    fun p ->
        if   p >= len
        then False
        else
        if   whole.[p] = '>'
        then True
        else
        if   whole.[p] = '<'
        then False
        else
        go_right (p+1)
    in
    (* As HTML can be malformed, do a similar check on the left: *)
    let rec go_left:  int -> bool =
    fun p ->
        if   p < 0
        then False
        else
        if   whole.[p] = '<'
        then True
        else
        if   whole.[p] = '>'
        then False
        else
        go_left  (p-1)
    in
    (* EITHER of conditions would imply a tag: *)
    (go_right pos_until) || (go_left (pos_from-1));


(*--------------------*)
(*  HTML Re-Writing:  *)
(*--------------------*)
(*  URLs (<a href=...> and <... src=...>) need to be re-written if they are
    relative,    otherwise the user receiving our highlighted page will see
    broken links. Also, a preamble is to be installed after the
*)
value body_re = "(<body.*?>)";
value href_re = "((\\s+href=\")(.+?)\")";
value src_re  = "((\\s+src=\")(.+?)\")";
value loc_re  = "((\\.location=\")(.+?)\")";

value tags_re = body_re^"|"^href_re^"|"^src_re^"|"^loc_re;

(*  In the combined "tags_re", match positions are (with "full_match"):
    for BODY => 1;
    for HREF => 2, 3, 4;
    for SRC  => 5, 6, 7;
    for LOC  => 8, 9, 10;
    in the order of opening parenthesis.
*)
type match_context =
{
    whole             : string;
    terms             : list string;
    parsed_url        : Http_client.parsed_url;
    preamble_installed: mutable bool
};

(*--------------------*)
(*  "process_match":  *)
(*--------------------*)
value process_match: match_context -> Pcre.substrings -> string =
fun ctx matches ->
    let marr = Pcre.get_substrings ~full_match:True matches in

    if  Array.length marr < 10
    then
        marr.(0) (* Can it happen? *)
    else
    if  marr.(1) <> ""
    then
    do {
        (* This is a BODY tag, install a preamble after it, and mark this
           fact in the context:
        *)
        ctx.preamble_installed := True;
        marr.(1)^(mk_preamble ctx.terms)
    }
    else
    if  marr.(2) <> ""
    then
    do {(* This is an HREF:  *)
        assert (marr.(3) <> "" && marr.(4) <> "");

        (* Re-write the URL: *)
        marr.(3)^(mk_absolute_url marr.(4) ctx.parsed_url)^"\""
    }
    else
    if  marr.(5) <> ""
    then
    do {(* This is an SRC:   *)
        assert (marr.(6) <> "" && marr.(7) <> "");

        (* Re-write the URL: *)
        marr.(6)^(mk_absolute_url marr.(7) ctx.parsed_url)^"\""
    }
    else
    if  marr.(8) <> ""
    then
    do {(* This is a LOCATION  -- XXX not very useful, as we don't fully
           interpret JavaScript...
        *)
        assert (marr.(9) <> "" && marr.(10) <> "");

        (* Re-write the URL: *)
        marr.(9)^(mk_absolute_url marr.(10) ctx.parsed_url)^"\""
    }
    else
    (* This is a Search Term, but it is NOT to be highlighted if it occurs
       inside an HTML tag. Also, DON'T re-write the HTML header, so do any
       re-writing only AFTER the preamble has been installed:
    *)
    if  (not (within_tag matches ctx.whole)) && ctx.preamble_installed
    then
        hl marr.(0)
    else
        marr.(0);

(*---------------------*)
(*  "st_high_light":   *)
(*---------------------*)
(*  Top-Level Request Handler: *)

value st_high_light:
    Tnio.tsock -> Http_server.http_req -> (string -> unit) ->  float -> unit =

fun socket req log deadline ->
    (* The "st" and "url" parms MUST be defined: *)
    let parms = req.Http_server.query_parms             in
    let st    = Http_common.get_single_val  parms "st"  in
    let url   = Http_common.get_single_val  parms "url" in

    (* Create a Client HTTP Agent. We do NOT make it persistent  (a part of
       the HTTP Worker's state), as it would only be needed for (relatively
       infrequent) Search Terms HighLighting requests, but would tend to use
       up a lot of memory due to a "large" buffer:
    *)
    let emulate_browsers =
       (Lukol_config.get_global_conf()).
        Lukol_config.be_config.Lukol_config.emulate_browsers
    in
    let conf =
    {
        Http_client.conf_client_ips  = [];
        Http_client.conf_use_persist = False;
        Http_client.conf_buff_size   = 4 * Clients_simple.http_buff_size;
        Http_client.conf_user_agents = emulate_browsers           (*OK?*)
    }
    in
    (* Get the page: *)
    let out_page  =
    try
        (* NB: a non-cached DNS resolver is to be used here, to prevent overflow
           of the L2 cache.   As we create an HTTP agent dynamically, we can do
           the same with the DNS agent:
        *)
        let dns_servers=(Lukol_config.get_global_conf()).
                         Lukol_config.dns_servers
        in
        let dns_agent  = Clients_simple.mk_dns_agent dns_servers
        in
        let http_agent = Http_client.mk_http_agent
                        (Clients_simple.dns_resolver dns_agent)
                        (fun () ->  Dns_client.close_dns_agent dns_agent)
                         Clients_simple.http_transport
                        (Some  (Clients_simple.https_transport))
                         conf
        in
        (* Retrieve the content of the given "url". XXX: For the moment, we only
           accept HTML documents for HighLighting:
        *)
        let retrv =
        {
            Http_client.http_method   = "GET";
            Http_client.full_url      = url;
            Http_client.extra_headers = [("Accept", "text/html")];
            Http_client.cookies       = [];
            Http_client.form          = []
        }
        in
        let (mb_res, err) =
            try  (Some (Http_client.http_transaction http_agent retrv deadline),
                  None)
            with [hmm -> (None, Some hmm)]
        in
        (* Close the agents and re-raise the exception: *)
        let () = Dns_client.close_dns_agent dns_agent           in
        let () = Http_client.close_agent   http_agent deadline  in

        (* Check for errors: *)
        let () = match err with
                [ None -> ()
                | Some hmm -> raise hmm]
        in
        let res  = Option.get mb_res     in
        let body = res.Http_client.body  in

        (* Construct the "highlighted" body: *)
        try
            (* Check the type of the response we got: *)
            let cont_type =
                Http_common.get_single_val
                res.Http_client.headers "content-type"
            in
            if  not (ExtString.String.starts_with cont_type "text/html")
            then
                (* Not HTML -- take the page "as is": *)
                body
            else
            (* Get the list of individual search terms.  Currently, there are no
               wild-cards or logical exprs in the search terms, so they are just
               connected by white spaces:
            *)
            let ind_terms = Pcre.split ~rex:space_re st in

            (* Construct a new regexp matching ANY of the "ind_terms",  ignoring
               the case. Any "special" symbols are to be escaped:
            *)
            let esc_terms  =
                List.map
                (fun t ->
                    let t' = Pcre.substitute
                             ~rex:special_re ~subst:(fun s -> "\\"^s) t
                    in
                    "("^t'^")"
                )
                ind_terms
            in
            let any_terms  = String.concat "|" esc_terms in

            (* In front, install the regexps for HTML constructs: *)
            let tags_terms = tags_re ^"|"^ any_terms     in

            (* Compile the combined substitution pattern: *)
            let subst_re   =
                Pcre.regexp ~study:True ~flags:[`CASELESS]
                            ~limit:Back_end.match_limit tags_terms
            in
            (* Apply the "subst_re" to the "body": *)
            let ctx =
            {
                whole              = body;
                terms              = ind_terms;
                parsed_url         = res.Http_client.from_url;
                preamble_installed = False
            }
            in
            (* Finaly, do all the substitutions; *)
            Pcre.substitute_substrings
                ~rex: subst_re ~subst:(process_match ctx) body
        with
        [hmm ->
         do {
            (* Any error during high-lighting -- use the original "body": *)
            log ("HIGH-LIGHTER ERROR (1): "^(Misc_utils.print_exn hmm));
            body
        }]
    with
        [hmm ->
        do {
            (* Any error not caught above, e.g., failure to retrieve the page:
               Try to display the page via the user's browser capabilities
               (without any highlighting):
            *)
            log ("HIGH-LIGHTER ERROR (2): "^(Misc_utils.print_exn hmm));
            String.concat "\r\n"
            [
                "<html><head>";
                "<meta http-equiv=\"Refresh\" content=\"0;URL="^url^"\">";
                "</head></html>"
            ]
        }]
    in
    (* Output the constructed page: *)
    Http_server.display_page out_page Tnio.send socket deadline;

