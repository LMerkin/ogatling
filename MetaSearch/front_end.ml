(*pp camlp4r *)
(* vim:ts=4:syntax=ocaml
*)
(*===========================================================================*)
(*                              "front_end.ml":                              *)
(*      Front-End HTTP Server for the Gatling/Lukol MetaSearch Engine:       *)
(*                  (C) Explar Technologies Ltd, 2004--2005                  *)
(*===========================================================================*)
(*  An "Http_server" receiving requests from end-users, whose worker threads
    act as clients to the Back-End.
*)
(*====================*)
(*  Global Settings:  *)
(*====================*)
(*  Read-Only Access Functions: *)

value is_monitored      : unit -> bool   =
fun () ->
   (Lukol_config.get_global_conf()).Lukol_config.daemon_mode;

value results_tmpl_file : unit -> string =
fun () ->
   (Lukol_config.get_global_conf()).
    Lukol_config.fe_config.Lukol_config.results_template_file;

value front_tmpl_file   : unit -> string =
fun () ->
   (Lukol_config.get_global_conf()).
    Lukol_config.fe_config.Lukol_config.front_template_file;

value stats_tmpl_file   : unit -> string =
fun () ->
   (Lukol_config.get_global_conf()).
    Lukol_config.fe_config.Lukol_config.stats_template_file;

value our_domain        : unit -> string =
fun () ->
   (Lukol_config.get_global_conf()).
    Lukol_config.fe_config.Lukol_config.our_domain;

value log_file          : unit -> string =
fun () ->
   (Lukol_config.get_global_conf()).Lukol_config.log_file;

value error_page_ref = ref "";
value error_page        : unit -> string =
fun () ->
    error_page_ref.val;

value admin_page_ref = ref "";
value admin_page        : unit -> string =
fun () ->
    admin_page_ref.val;

value page_size         : unit -> int    =
fun () ->
   (Lukol_config.get_global_conf()).
    Lukol_config.fe_config.Lukol_config.page_size;

value pages_quant       : unit -> int    =
fun () ->
   (Lukol_config.get_global_conf()).
    Lukol_config.fe_config.Lukol_config.pages_quant;

value admin_passwd      : unit -> string =
fun () ->
   (Lukol_config.get_global_conf()).
    Lukol_config.fe_config.Lukol_config.admin_passwd;


(*  Model for the Front Page -- will be constant: *)
value front_model = Hashtbl.create 20;


(* SSL Server Context: set up by "init": *)
value ssl_context_ref = ref None;

value ssl_context: unit -> Ssl.context =
fun () ->
    Option.get (ssl_context_ref.val);


(*============================*)
(*  Servicing HTTP requests:  *)
(*============================*)
(*-----------------------------------------------*)
(*  HTTP(S) Worker State and its Initialisation: *)
(*-----------------------------------------------*)
type fe_state_http =
{
    log_func_http   : string -> unit
};

value fe_init_http:  (string -> unit) -> unit -> fe_state_http =
fun log_func () ->
{
    log_func_http    = log_func
};

type fe_state_https  =
{
    log_func_https:  string -> unit
};

value fe_init_https:(string -> unit) -> unit -> fe_state_https=
fun log_func () ->
{
    log_func_https   = log_func
};

(* Create the global template cache: *)

value template_cache =  CamlTemplate.Cache.create ();


(*-------------------*)
(*  "mk_pages_bar":  *)
(*-------------------*)
(*  Generates a navigation bar for the user. NB: "st_enc" is the "URL+"-encoded
    string of Search Term(s):
*)
value mk_pages_bar:
    int -> int -> string -> string -> CamlTemplate.Model.tlist =

fun page_no show_pages st_enc gses_codes ->

    (* Encode the list of Search Engines used: *)
    let add_ses =
        let gses_lst = ref [""] in
        (* This is to get an additional "&" in front! *)
        let () =
            String.iter
            (fun c ->
                gses_lst.val :=
                gses_lst.val @ [Printf.sprintf "se=%c" c]
            )
            gses_codes
        in
        String.concat "&" gses_lst.val
    in
    (* List of Pages: *)
    let pages_lst =
        (* There is no prev page to page 1: *)
        if  page_no = 1
        then
            ref []
        else
            ref [("Prev", page_no-1, True)]
    in
    let () =
        (* The curr page is not clickable:  *)
        for i = 1 to show_pages
        do {
            pages_lst.val :=
            pages_lst.val @ [(string_of_int i, i, i <> page_no)]
        }
    in
    let () =
        pages_lst.val :=
        pages_lst.val @ [("Next", page_no+1, True)]
    in
    (* Generate a list of templates: *)
    List.map
        (fun (name, num, click) ->
            let url =
                if   click
                then Printf.sprintf "/search?st=%s&pn=%d%s" st_enc num add_ses
                else ""
            in
            let pinfo = Hashtbl.create 10 in
            do {
                Hashtbl.add pinfo "name"  (CamlTemplate.Model.Tstr name);
                Hashtbl.add pinfo "url"   (CamlTemplate.Model.Tstr url );
                CamlTemplate.Model.Thash  pinfo
            }
        )
        pages_lst.val;


(*---------------------*)
(*  HTTP worker body:  *)
(*---------------------*)
(* XXX: Currently, the front-end only supports HTTP protocol; HTTPS can be
   added at a later stage:
*)
value fe_worker_http:
    Tnio.tsock    -> Http_server.http_req -> fe_state_http -> float ->
    fe_state_http =

fun socket req state deadline->
let out_page =
try
    (* Currently, we only use the "st", and optionally the "pn" and "ses"
       parms:
       "st" : the Search Term(s) as a single string;
       "pn" : Page Number (from 1, default 1);
       "se" : a Search Engine to be used; multiple "se" parms are allowed;
              if none are given, all configured Search Engines are used.
    *)
    let parms = req.Http_server.query_parms in
    let (search_terms, page_no) =
    try
        let st    = Http_common.get_single_val parms "st" in
        let pn    =
            try int_of_string (Http_common.get_single_val parms "pn")
            with [_ -> 1]
        in
        let ()    = assert (st <> "" && pn >= 1) in
        (st, pn)
    with
        [_ -> failwith "Front_end.fe_worker_http: Invalid request parm(s)"]
    in
    (* So which Search Engine servers are to be used? Get the codes here,
       and merge them into a string -- a string is better than a list of
       codes, as it can also be used in a Cache key.  To make the string
       uniquely-defined, sort the codes alphabetically.
       NB: These codes correspond to General-Purpose Search Engines only.
       XML-Based Search Engines are added later automatically:
    *)
    let gses_lst0 =
    try
        Hashtbl.find parms "se"
    with
    [_ ->
        (* No "se" parameters: use all configured General-Purpose Search
           Engines by default:
        *)
        List.map
            (fun (code, _) -> String.make 1 code)
            (Back_end.all_gen_servers ())
    ]
    in
    (* To be on the safe side, remove duplicates from the list of codes,
       and sort it (using the standard comparison function):
    *)
    let gses_lst1 = List.sort compare (ExtList.List.unique gses_lst0)  in

    (* Verify that all codes are exactly 1 character long: *)
    if  List.exists (fun code -> String.length code <> 1) gses_lst1
    then
        failwith ("Front_end.fe_worker_http: Invalid Search Engine Code(s) "^
                  "in the request")
    else
    (* NB: it's theoretically OK to get an empty str here: *)
    let gses_str = String.concat "" gses_lst1 in

    (* NOW: Is this request a continuation of another one? *)
    let client_ip =
        match Tnio.getpeername socket with
        [ Unix.ADDR_INET ip _ -> ip
        | _                   -> assert False
        ]
    in
    (* Get the result pages from the Cache and Search Engines: *)
    let res   =
        Middle_cache.get_results
        search_terms page_no client_ip gses_str deadline
    in
    (* Make the data model to fill in the output template: *)
    let model = Hashtbl.create 20 in

    (* Encode the Search Term(s) back, to be used in URLs: *)
    let st_enc= Netencoding.Url.encode ~plus:True search_terms in

    (* "gress" is a list of "Thash" nodes made from the General-Purpose
       result records:
    *)
    let before= (page_size ()) * (page_no - 1) in
    let gress =
        fst (List.fold_left
        (fun (curr, i) r ->
            let node   = Hashtbl.create 50 in

            (* Node numbers: our enumerator and that of the original Search
               Engine (on its local page):
            *)
            let this_i = string_of_int (i+1+before)                        in
            let orig_i = string_of_int (int_of_float r.Back_end.index) in

            (* URL to be used for highlighting the search terms: *)
            let hl_url = Printf.sprintf "/sthl?st=%s&url=%s" st_enc
                         (Netencoding.Url.encode ~plus:True r.Back_end.url)
            in
            (* The "from" list: *)
            let from_lst =
                List.map
                (fun (se_name, se_url) ->
                    let pair = Hashtbl.create 10 in
                    do {
                        Hashtbl.add pair "se_name"
                            (CamlTemplate.Model.Tstr se_name);
                        Hashtbl.add pair "se_url"
                            (CamlTemplate.Model.Tstr se_url );
                        CamlTemplate.Model.Thash     pair
                    }
                )
                r.Back_end.from
            in
            let () =
            do {
                Hashtbl.add node "i"      (CamlTemplate.Model.Tstr this_i);
                Hashtbl.add node "from"   (CamlTemplate.Model.Tlist from_lst);

                Hashtbl.add node "url"
                    (CamlTemplate.Model.Tstr r.Back_end.url);

                Hashtbl.add node "title"
                    (CamlTemplate.Model.Tstr r.Back_end.title);

                Hashtbl.add node "descr"
                    (CamlTemplate.Model.Tstr r.Back_end.descr);

                Hashtbl.add node "hl_url" (CamlTemplate.Model.Tstr hl_url);
                Hashtbl.add node "orig_i" (CamlTemplate.Model.Tstr orig_i)
            }
            in
            (curr @ [CamlTemplate.Model.Thash node], i+1)
        )
        ([], 0) res.Middle_cache.gen_page)
    in
    (* "xress" is a list of "Thash" nodes made from the XML-Based result
       records:
    *)
    let xress =
        if  page_no = 1
        then
            List.map
            (fun xr ->
                let node = Hashtbl.create 50 in
                do {
                    Hashtbl.add node "go_url"
                        (CamlTemplate.Model.Tstr xr.Back_end.xclick);

                    Hashtbl.add node "title"
                        (CamlTemplate.Model.Tstr xr.Back_end.xtitle);

                    Hashtbl.add node "descr"
                        (CamlTemplate.Model.Tstr xr.Back_end.xdescr);

                    CamlTemplate.Model.Thash node
                }
            )
            res.Middle_cache.xml_page
        else
            []
    in
    (* Related Search Terms: *)
    let related = 
        if  page_no = 1
        then
            List.map
            (fun rt ->
                let et     = Netencoding.Url.encode ~plus:True rt in
                let rentry = Hashtbl.create 10 in
                do {
                    (* The term  itself -- for the title: *)
                    Hashtbl.add rentry "title"   (CamlTemplate.Model.Tstr rt);

                    (* The encoded term -- for further searches: *)
                    Hashtbl.add rentry "encoded" (CamlTemplate.Model.Tstr et);

                    CamlTemplate.Model.Thash rentry
                }
            )
            res.Middle_cache.rel_terms
        else
            []
    in
    (* "pages_bar" is a list of pages for user's navigation:
       [(page.name, page.url)].
       NB: be carefult here.  As result pages may be  shared between different
       users (if their req parms are exactly the same), do not show ALL avai-
       lable pages to the current user (as they may be created by another one,
       who went further ahead; rather, round the pages up to the nearest mult-
       iple of "pages_quant"):
    *)
    let total_pages =
        if   res.Middle_cache.gen_total mod (page_size ()) = 0
        then res.Middle_cache.gen_total  /  (page_size ())
        else res.Middle_cache.gen_total  /  (page_size ()) + 1
    in
    let round_pages = (page_no / pages_quant () + 1) * pages_quant ()  in
    let show_pages  = min  total_pages round_pages                     in

    let pages_bar   = mk_pages_bar page_no show_pages  st_enc gses_str in

    (* Attach the "results", "terms", "pages_bar" to the top-level "model": *)
    let () =
    do {
        Hashtbl.add model "terms"      (CamlTemplate.Model.Tstr  search_terms);
        Hashtbl.add model "results"    (CamlTemplate.Model.Tlist gress);
        Hashtbl.add model "n_results"  (CamlTemplate.Model.Tint
                                       (List.length gress));
        Hashtbl.add model "xresults"   (CamlTemplate.Model.Tlist xress);
        Hashtbl.add model "n_xresults" (CamlTemplate.Model.Tint
                                       (List.length xress));
        Hashtbl.add model "related"    (CamlTemplate.Model.Tlist related);
        Hashtbl.add model "n_related"  (CamlTemplate.Model.Tint
                                       (List.length related));
        Hashtbl.add model "pages"      (CamlTemplate.Model.Tlist pages_bar);
    }
    in
    (* Now get the template: *)
    let tmpl = CamlTemplate.Cache.get_template
               template_cache (results_tmpl_file ())
    in
    (* Fill it in -- XXX: what is the best estimate for the output size? *)
    let buff = Buffer.create 10240 in
    let ()   = CamlTemplate.merge tmpl model buff in

    (* The final output -- without headers yet: *)
    Buffer.contents buff
with
    [hmm ->
    do {
        (* Error: log it, and return the Error Page: *)
        Http_common.log_http_exn state.log_func_http hmm;
        error_page ()
    }]
in
do {
    (* Send the page (data or error) to the user:    *)
    Http_server.display_page out_page Tnio.send socket deadline;
    state
};

(*------------------*)
(*  Pages Display:  *)
(*------------------*)
(*  Front Page, Error Page, ...: *)

(* "display_front":
   The front page is automatically generated from a template:
*)
value display_front:
    Tnio.tsock  -> Http_server.http_req -> fe_state_http -> float ->
    fe_state_http =

fun socket _ state deadline ->
do {
    (* Load the page template: *)
    let tmpl = CamlTemplate.Cache.get_template
               template_cache (front_tmpl_file ())
    in
    (* Fill it in: *)
    let buff = Buffer.create 2048 in
    let ()   = CamlTemplate.merge tmpl front_model buff in

    (* And send it to the client: *)
    let page   = Buffer.contents buff in

    Http_server.display_page page Tnio.send socket deadline;

    (* Return the unchanged state: *)
    state
};

(* "http_error"   : *)

value http_error: Tnio.tsock -> int -> string -> unit =
fun socket _ _ ->
    (* Use adjusted deadline here, as this function may be invoked
       when the main deadline has already expired:
    *)
    let deadline = Unix.gettimeofday() +. 10.0
    in
    Http_server.display_page  ~full_response:True
        (error_page ()) Tnio.send socket deadline;

(* "https_error"  : *)

value https_error: Ssl.socket -> int -> string -> unit =
fun socket _ _ ->
    (* Same as for "http_error" above, only use a secure socket: *)
    let deadline = Unix.gettimeofday() +. 10.0
    in
    Http_server.display_page  ~full_response:True
        (error_page ()) Ssl.write socket deadline;


(*---------------------------*)
(*  Wrapper for HighLighter: *)
(*---------------------------*)
value st_high_light:
    Tnio.tsock    -> Http_server.http_req -> fe_state_http -> float ->
    fe_state_http =

fun socket req state deadline ->
do {
    Highlight.st_high_light
        socket
        req
        state.log_func_http
        deadline;
    state
};

(*-----------------*)
(*  Bar Diagrams:  *)
(*-----------------*)
(*  The "total" is equivalent (somewaht arbitrarily) to 250 pixels: *)

value bar_width: int -> int -> int =
fun count total ->
    if  count = 0 || total = 0
    then 0
    else max 5 ((250 * count) / total);

(* The following is a 1-pixel GIF file, for drawing bars: *)
value blue_pixel =
    "\x47\x49\x46\x38\x37\x61\x01\x00\x01\x00\x80\x01\x00\x00\x00\xFA"^
    "\xFF\xFF\xFF\x2C\x00\x00\x00\x00\x01\x00\x01\x00\x00\x02\x02\x44"^
    "\x01\x00\x3B";

(*-------------------*)
(*  "server_stats":  *)
(*-------------------*)
(*  Obtains and formats the server statistics: *)

value server_stats: string -> string =
fun passwd ->
    (* Get the stats: *)
    let stats  = Middle_cache.get_stats () in

    (* Make the output data model: *)
    let model  = Hashtbl.create 10 in
    let ()     =
    do {
        (* Over-all Parms: *)

        Hashtbl.add model "user_reqs"
            (CamlTemplate.Model.Tint stats.Middle_cache.user_reqs);

        let av_resp_time =
            if   stats.Middle_cache.user_reqs <> 0
            then stats.Middle_cache.cum_resp_time /.
                (float_of_int stats.Middle_cache.user_reqs)
            else 0.0
        in
        Hashtbl.add model "av_resp_time"
            (CamlTemplate.Model.Tstr
            (Printf.sprintf "%.2f sec" av_resp_time));

        let cache_hit_rate =
            if   stats.Middle_cache.user_reqs <> 0
            then (float_of_int stats.Middle_cache.cached_reqs) /.
                 (float_of_int stats.Middle_cache.user_reqs)   *. 100.0
            else 0.0
        in
        Hashtbl.add model "cache_hit_rate"
            (CamlTemplate.Model.Tstr
            (Printf.sprintf "%.2f %%" cache_hit_rate));

        Hashtbl.add model "salvos"
            (CamlTemplate.Model.Tint stats.Middle_cache.salvos);

        Hashtbl.add model "time_now"
            (CamlTemplate.Model.Tstr
            (Date_time.http_time_stamp (Unix.gettimeofday ())));

        Hashtbl.add model "stats_begin"
            (CamlTemplate.Model.Tstr
            (Date_time.http_time_stamp (stats.Middle_cache.stats_begin)));

        (* Per-SE Parms: *)

        let all_ses =
            Hashtbl.fold
            (fun se_name se_stats curr ->
                let se_data = Hashtbl.create 10 in
                do {
                    Hashtbl.add se_data "name"
                        (CamlTemplate.Model.Tstr se_name);

                    Hashtbl.add se_data "total_reqs"
                        (CamlTemplate.Model.Tint
                        (se_stats.Middle_cache.se_http_reqs));

                    Hashtbl.add se_data "compl_reqs"
                        (CamlTemplate.Model.Tint
                        (se_stats.Middle_cache.completed_reqs));

                    Hashtbl.add se_data "succ_reqs"
                        (CamlTemplate.Model.Tint
                        (se_stats.Middle_cache.succ_reqs));

                    let succ_rate =
                        if   se_stats.Middle_cache.se_http_reqs <> 0
                        then
                            (float_of_int se_stats.Middle_cache.succ_reqs)    /.
                            (float_of_int se_stats.Middle_cache.se_http_reqs) *.
                            100.0
                        else  0.0
                    in
                    Hashtbl.add se_data "succ_rate"
                        (CamlTemplate.Model.Tstr
                        (Printf.sprintf "%.2f %%" succ_rate));

                    (* NB: Here "av_resp_time" is calculated wrt the number of
                       COMPLETED reqs:
                    *)
                    let av_resp_time =
                        if   se_stats.Middle_cache.completed_reqs <> 0
                        then se_stats.Middle_cache.cum_http_time /.
                            (float_of_int se_stats.Middle_cache.completed_reqs)
                        else 0.0
                    in
                    Hashtbl.add se_data "av_resp_time"
                        (CamlTemplate.Model.Tstr
                        (Printf.sprintf "%.2f sec" av_resp_time));

                    (* Now the response time distribution: *)
                    let rtd = ref [] in
                    let arr = se_stats.Middle_cache.resp_time_distr in
                    let n   = Array.length arr in
                    do {
                        Array.iteri
                        (fun i count ->
                            let from = (float_of_int i)     *.
                                       (stats.Middle_cache.distr_step)
                            in
                            let upto = (float_of_int (i+1)) *.
                                       (stats.Middle_cache.distr_step)
                                       (* Unless it's the last intterval *)
                            in
                            let range=
                                if   i <> n-1
                                then Printf.sprintf "%5.2f--%5.2f sec" from upto
                                else Printf.sprintf   "Over %5.2f sec" from
                            in
                            let pair = Hashtbl.create 5 in
                            do {
                                Hashtbl.add pair "range"
                                    (CamlTemplate.Model.Tstr range);

                                Hashtbl.add pair "count"
                                    (CamlTemplate.Model.Tint count);

                                (* The width of the graphical bar used  for
                                   representing this distribution group, is
                                   relative to the total number of "salvo"s:
                                *)
                                let bar =
                                    bar_width count stats.Middle_cache.salvos
                                in
                                Hashtbl.add pair "bar"
                                    (CamlTemplate.Model.Tint bar);

                                rtd.val :=
                                    [(CamlTemplate.Model.Thash pair)::rtd.val]
                            }
                        )
                        arr;

                        (* Attach the "rtd" list to "se_data": *)
                        Hashtbl.add se_data "distr"
                            (CamlTemplate.Model.Tlist (List.rev rtd.val))
                    };

                    (* "se_data" has been filled in; attach it to the LIST: *)
                    [(CamlTemplate.Model.Thash se_data) :: curr]
                }
            )
            stats.Middle_cache.se_stats []
        in
        (* Attach the "all_ses" list constructed -- XXX: it's not sorted! *)
        Hashtbl.add model "se_stats" (CamlTemplate.Model.Tlist all_ses);

        (* Also, pwd (same) for the reset option: *)
        Hashtbl.add model "pwd"      (CamlTemplate.Model.Tstr  passwd )
    }
    in
    (* Now get the template: *)

    let tmpl = CamlTemplate.Cache.get_template
               template_cache (stats_tmpl_file ())
    in
    (* Fill it in -- XXX: what is the best estimate for the output size? *)
    let buff = Buffer.create 10240 in
    let ()   = CamlTemplate.merge tmpl model buff in

    (* The final output -- without headers yet: *)
    Buffer.contents buff;


(*--------------------------*)
(*  Re-starting the server: *)
(*--------------------------*)
(*  (E.g., after updating Search Engines Interface Data). Currently, restart
    only works in the monitored mode:
*)
value update_ok_page =
    "<html><head><title>Update Successful</head></title>" ^
    "<body bgcolor=\"white\"><center>" ^
    "THE UPDATE HAS BEEN SUCCESSFUL.<br>You need to re-start the server "^
    "for the changes to take effect."  ^
    "</center></body></html>";

value restart_server: unit -> unit =
fun () ->
    if  is_monitored ()
    then
        (* The server is monitored, so we simply exit (RC=100 is OK), and the
           monitor will restart us:
        *)
        Tnio.exit 100
    else
        failwith
        "Front_end.restart_server: not supported in the single-process mode";

(*-------------------------------*)
(*  Administration (via HTTPS):  *)
(*-------------------------------*)
(*  The same function is used for all admin-relared requests:  *)

value admin_all:
    Ssl.socket -> Http_server.http_req -> fe_state_https -> float ->
    fe_state_https =

fun socket req state deadline ->
let (out_page, mime_type) =
try
    (* If it's a request for GIF, service it: *)
    if  ExtString.String.ends_with req.Http_server.path "Pixel.gif"
    then
        (blue_pixel, "image/gif")
    else
    (* For all other requests, check the passwd -- if not present, only
       the front page can be presented; if the password is invalid,  an
       error is produced:
        *)
    let passwd =
        try  Http_common.get_single_val req.Http_server.query_parms "pwd"
        with [_ -> ""]
    in
    if  passwd = ""
    then
        (* Empty passwds are not allowed in the normal case -- so there
           was no passwd in this case at all:
        *)
        (admin_page (), "text/html")
    else
    if   passwd <> admin_passwd ()
    then
        (* Invalid passwd was given: *)
        (error_page (), "text/html")
    else
    (* The general case -- valid authentication was provided.  Check the
       action requested.   It can be given via the "action" parm, or via
       the URL path:
    *)
    let action =
        try  Http_common.get_single_val req.Http_server.query_parms "action"
        with [_ -> ""]
    in
    if  action = "RESET STATISTICS"
    then
    do {
        (* Reset the statistics, then show it: *)
        Middle_cache.reset_stats ();
        (server_stats passwd, "text/html")
    }
    else
    if  action = "SHOW STATISTICS"
    then
        (server_stats passwd, "text/html")
    else
    if  action = "UPDATE GEN ENGINES"
    then
    do {
        Back_end.update_engines  "Gen" deadline;
       (update_ok_page, "text/html")
    }
    else
    if  action = "UPDATE XML ENGINES"
    then
    do {
        Back_end.update_engines  "XML" deadline;
       (update_ok_page, "text/html")
    }
    else
    if  action = "RESTART SERVER"
    then
    do {
        restart_server ();
        (* The result will not be displayed UNLESS the server was NOT
           restarted, so:
        *)
        (error_page (), "text/html")
    }
    else
    if  action = "SHUT-DOWN SERVER"
    then
        Tnio.exit 0
    else
        (* Invalid request: *)
        (error_page   (), "text/html")
with
    [hmm ->
    do {
        (* Error: log it, and return the Error Page  : *)
        Http_common.log_http_exn state.log_func_https hmm;
        (error_page   (), "text/html")
    }]
in
do {
    (* Send the output (results or error) to the user: *)
    Http_server.display_page
        out_page ~content_type: mime_type Ssl.write socket deadline;
    state
};

(*========================*)
(*  Front-End Top-Level:  *)
(*========================*)
(*---------------------------------*)
(* The Table of Handler Functions: *)
(*---------------------------------*)
value handler_table_http = Hashtbl.create 10;

(* Front Page: *)
Hashtbl.add handler_table_http "/"       display_front;

(* Search: *)
Hashtbl.add handler_table_http "/search" fe_worker_http;

(* Search Terms High-Lighting: *)
Hashtbl.add handler_table_http "/sthl"   st_high_light;

(* "Powered-by-OCaml" image:   *)
Hashtbl.add handler_table_http "/OCamlPowered.jpg"
    (fun ts _ st deadline ->
    do {
        Http_server.display_page
            Ocaml_powered.logo_jpg
            ~content_type:"image/jpeg"
            Tnio.send ts deadline;
        st
    });

(* For different handlers, there may be different time-outs: *)
value req_timeout:
    Lukol_config.front_end_config -> Http_server.http_req -> float =
fun fc req ->
    if  req.Http_server.path = "/sthl"
    then
        fc.Lukol_config.highlight_timeout
    else
        fc.Lukol_config.search_timeout;
        (* Including servicing JPEG! *)

(*---------------------*)
(* HTTP Server Config: *)
(*---------------------*)
value fe_config_http:
    Lukol_config.front_end_config ->
    int                           ->
    int                           ->
    (string -> unit)              ->
    Http_server.server_config Tnio.tsock fe_state_http =

fun fc fe_acceptor_prio fe_worker_prio log_func ->
{
    (* TCP parms: *)
    Http_server.listen_on_ip        = Unix.inet_addr_of_string
                                      fc.Lukol_config.listen_on_ip;
    Http_server.listen_on_port      = fc.Lukol_config.listen_on_port;
    Http_server.listen_queue_len    = fc.Lukol_config.listen_queue_len;
    Http_server.max_connections     = fc.Lukol_config.max_connections;
    Http_server.sock_buff_size      = fc.Lukol_config.sock_buff_size;

    (* Acceptor thread parms: *)
    Http_server.acceptor_priority   = fe_acceptor_prio;
    Http_server.acceptor_stack_k    = 128;

    (* Worker pool parms: *)
    Http_server.max_idle_threads    = fc.Lukol_config.max_idle_http_threads;
    Http_server.min_idle_threads    = fc.Lukol_config.min_idle_http_threads;
    Http_server.init_threads        = fc.Lukol_config.init_http_threads;
    Http_server.worker_priority     = fe_worker_prio;
    Http_server.worker_stack_k      = 128;

    (* State management:
       Plain TNIO sockets do not require any special init/fini:
    *)
    Http_server.init_socket         = fun ts _ _ -> ts;
    Http_server.finalise_socket     = fun _ _ -> ();

    Http_server.init_worker_state   = fe_init_http log_func;
    Http_server.cleanup_state       = fun _ -> ();
    Http_server.reset_state_on_error= False;
    Http_server.log_error           = Http_common.log_http_exn log_func;
    Http_server.reject_conn         = fun _ -> ();

    (* I/O mechanisms: *)
    Http_server.read_func           = Tnio.recv;
    Http_server.write_func          = Tnio.send;

    (* Processing functions and parms: *)
    Http_server.proc_funcs          = handler_table_http;
    Http_server.session_timeout     = fc.Lukol_config.session_timeout;
    Http_server.max_pers_reqs       = fc.Lukol_config.session_max_reqs;
    Http_server.proc_timeout        = req_timeout fc;
    Http_server.http_error          = http_error;

    Http_server.uploaded_dir        = "" (* Not used *)
};

(*-------------------*)
(* SSL Sockets Init: *)
(*-------------------*)
(*  This is how an ordinary  TNIO socket returned by the acceptor, is converted
    into a server-side SSL socket:
*)
value init_ssl_socket: Tnio.tsock -> fe_state_https -> float -> Ssl.socket =
fun ts _ deadline ->
    let ssl = Ssl.embed_socket ts (ssl_context ()) in
    do {
        (* NB: the following is NOT a TCP accept; it's an "accept" for crypto-
           graphic negotiations on a socket which was ALREDY returned by a TCP
           acceptor:
        *)
        Ssl.accept ssl deadline;
        ssl
    };

value fini_ssl_socket: Ssl.socket -> float -> unit = Ssl.shutdown_connection;

(*-----------------*)
(* HTTPS Handlers: *)
(*-----------------*)
(*  Here it is more convenient to de-multiplex requests in the handler itself,
    so only one super-handler is provided:
*)
value handler_table_https = Hashtbl.create 10;

Hashtbl.add handler_table_https ""  admin_all;

(*----------------------*)
(* HTTPS Server Config: *)
(*----------------------*)
(* HTTPS server is currently used for the management interface (secure search
   queries MAY be implemented in the future.   Currently, the HTTPS server is
   configured to accept only a very limited number of connections  (for admin
   purposes only -- number of connections is not configurable):
*)

value fe_config_https:
    Lukol_config.front_end_config ->
    int                           ->
    int                           ->
    (string -> unit)              ->
    Http_server.server_config Ssl.socket fe_state_https =

fun fc ssl_acceptor_prio ssl_worker_prio log_func ->
{
    (* TCP parms: *)
    Http_server.listen_on_ip        = Unix.inet_addr_of_string
                                      fc.Lukol_config.listen_on_ip;
    Http_server.listen_on_port      = fc.Lukol_config.secure_port;
    Http_server.listen_queue_len    = 5;
    Http_server.max_connections     = 5;
    Http_server.sock_buff_size      = fc.Lukol_config.sock_buff_size;

    (* Acceptor thread parms: *)
    Http_server.acceptor_priority   = ssl_acceptor_prio;
    Http_server.acceptor_stack_k    = 128;

    (* Worker pool parms: *)
    Http_server.max_idle_threads    = 5;
    Http_server.min_idle_threads    = 1;
    Http_server.init_threads        = 1;
    Http_server.worker_priority     = ssl_worker_prio;
    Http_server.worker_stack_k      = 128;

    (* State management:
       Plain TNIO sockets do not require any special init/fini:
    *)
    Http_server.init_socket         = init_ssl_socket;
    Http_server.finalise_socket     = fini_ssl_socket;

    Http_server.init_worker_state   = fe_init_https log_func;
    Http_server.cleanup_state       = fun _ -> ();
    Http_server.reset_state_on_error= False;
    Http_server.log_error           = Http_common.log_http_exn log_func;
    Http_server.reject_conn         = fun _ -> ();

    (* I/O mechanisms: *)
    Http_server.read_func           = Ssl.read;
    Http_server.write_func          = Ssl.write;

    (* Processing functions and parms: *)
    Http_server.proc_funcs          = handler_table_https;
    Http_server.session_timeout     = fc.Lukol_config.session_timeout;
    Http_server.max_pers_reqs       = fc.Lukol_config.session_max_reqs;
    Http_server.proc_timeout        = fun _ -> 15.0;
    Http_server.http_error          = https_error;

    Http_server.uploaded_dir        = "" (* Not used *)
};

(*-----------------------------*)
(*  Front-End Initialisation:  *)
(*-----------------------------*)
(*  Returns a pair of server objects created (for HTTP and HTTPS):
*)
value init:
    ~fe_acceptor_prio :int          ->
    ~fe_worker_prio   :int          ->
    ~ssl_acceptor_prio:int          ->
    ~ssl_worker_prio  :int          ->
    (string -> unit)                ->
    ((Tcp_server.server_t Tnio.tsock fe_state_http ) *
     (Tcp_server.server_t Ssl.socket fe_state_https))
=
fun ~fe_acceptor_prio  ~fe_worker_prio
    ~ssl_acceptor_prio ~ssl_worker_prio log_func ->

let c = Lukol_config.get_global_conf () in
try
do {
    (* SSL settings: *)
    ssl_context_ref.val  :=
        Some (Ssl.create_server_context
             Ssl.SSLv23
             c.Lukol_config.fe_config.Lukol_config.pub_cert_file
             c.Lukol_config.fe_config.Lukol_config.priv_key_file);

    (* Check the Admin passwd: *)
    if   admin_passwd () = ""
    then failwith "Admin password MUST NOT be empty"
    else ();

    (* Error and Admin Pages : *)
    error_page_ref.val :=
        Misc_utils.read_file
           (Lukol_config.get_global_conf()).
            Lukol_config.fe_config.Lukol_config.error_page_file;

    admin_page_ref.val :=
        Misc_utils.read_file
           (Lukol_config.get_global_conf()).
            Lukol_config.fe_config.Lukol_config.admin_page_file;

    (* For the Front Page, we can create the model now: *)
    let gses =
        List.map
        (fun se ->
            let se_entry = Hashtbl.create 10 in
            do {
                Hashtbl.add se_entry "code"
                    (CamlTemplate.Model.Tstr
                    (String.make 1 se.Lukol_config.gse_code));

                Hashtbl.add se_entry "name"
                    (CamlTemplate.Model.Tstr (se.Lukol_config.gse_name_ref));

                CamlTemplate.Model.Thash se_entry
            }
        )
        c.Lukol_config.be_config.Lukol_config.gse_uconfs
    in
    (* Initialise the constant "front_model": *)
    let () = Hashtbl.add front_model "gses" (CamlTemplate.Model.Tlist gses)
    in
    (* Create a server for the HTTP front-end, and one for the HTTPS
       (currently admin only)  front-end. NB: the latter server currently
       uses HIGHER acceptor and worker priorities than the main one:
    *)
    let server_conf =
        fe_config_http
            c.Lukol_config.fe_config
            fe_acceptor_prio
            fe_worker_prio
            log_func
    in
    let secure_conf =
        fe_config_https
            c.Lukol_config.fe_config
            ssl_acceptor_prio
            ssl_worker_prio
            log_func
    in
    let http_server = Http_server.create_server server_conf in
    let ()= log_func "INFO: Front-End HTTP Server successfully created" in

    let https_server= Http_server.create_server secure_conf in
    let ()= log_func "INFO: Front-End HTTPS (Admin) Server successfully created"
    in
    (http_server, https_server)
}
with
[hmm ->
    failwith ("Front_end.init: ERROR: "^(Misc_utils.print_exn hmm))
];

