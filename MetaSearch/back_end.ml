(*pp camlp4r *)
(* vim:ts=4:syntax=ocaml
*)
(*===========================================================================*)
(*                              "back_end_gen.ml":                           *)
(*                                                                           *)
(*                A Gatling/Lukol MetaSearch Engine Back-End:                *)
(*         Interface to General-Purpose and XML-Based Search Engines         *)
(*                                                                           *)
(*                      (C) Explar Technologies Ltd, 2004                    *)
(*===========================================================================*)
(*  "Gen_server" whose worker threads act as clients to the actual general-
    purpose Search Engines:
*)
(*======================*)
(*  Trace, Log, Match:  *)
(*======================*)
value trace: unit   -> bool =
fun () ->
    (Lukol_config.get_global_conf()).Lukol_config.trace_mode;

value log_func_ref  = ref (fun _ -> ());

value log  : string -> unit =
fun msg ->
    log_func_ref.val msg;

(*  PCRE Match Limit -- far below the default value, to guard against non-
    matching regexps with extensive backtracking:
*)
value match_limit = 100_000;

(*===================*)
(*  DNS Facilities:  *)
(*===================*)
(*  The resolver used by the Back-End should cache IP addresses of ALL  Gen
    and XML Search Engies, including both front-and and following-on domain
    names:
*)
(*-----------------*)
(*  "se_resolver": *)
(*-----------------*)
value dns_l2_cache: Hashtbl.t string (list Unix.inet_addr) =
                    Hashtbl.create 100;

value se_resolver :
    Dns_client.dns_agent -> (string -> unit) -> string -> list Unix.inet_addr =

fun agent  log_func  dom ->
    let ips =
    try
        (* Look-up the L2 cache first: *)
        Hashtbl.find dns_l2_cache dom
    with
    [Not_found ->
     do {
        (* Not cached -- log a warning, and try direct resolution: *)
        log_func ("WARNING: \""^dom^"\" not found in the L2 DNS Cache");

        let ips = Clients_simple.dns_resolver agent dom in
        do {
            Hashtbl.add dns_l2_cache dom ips;
            ips
        }
    }]
    in
    ips;

(*-----------------------*)
(*  "init_se_resolver":  *)
(*-----------------------*)
(* Pre-fetches IP addresses of all configured "se_domain"s and
   "gse_other_hosts", and fills in the "dns_l2_cache":
*)
value init_se_resolver:
    list string                    ->
    Lukol_config.se_interfaces_gen ->
    Lukol_config.se_interfaces_xml ->
    (string -> unit)               ->
    unit        =

fun dns_servers interfs_gen interfs_xml log_func ->
do {
    (* Create the DNS agent which will be used here:   *)
    let agent = Clients_simple.mk_dns_agent dns_servers in

    (* Compile the list of all domains to be resolved: *)

    (* Domains of General-Purpose Search Engines:      *)
    let domains_gen =
    List.concat
       (List.map
            (fun gsec ->
                [gsec.Lukol_config.gse_domain ::
                 gsec.Lukol_config.gse_other_hosts]
            )
            interfs_gen
        )
    in
    (* Domains of XML-Based Search Engines: *)
    let domains_xml =
        List.map
            (fun xsec -> xsec.Lukol_config.xse_domain)
            interfs_xml
    in
    (* Resolve them all. HOWEVER, do not propagate an exception if some of
       these resolutions failed -- they can then be done dynamically later:
    *)
    let domains_all = domains_gen @ domains_xml  in
    List.iter
        (fun dom ->
         try
            let ips = Clients_simple.dns_resolver agent dom in
            Hashtbl.replace dns_l2_cache dom ips
         with
         [ hmm ->
            log_func (Printf.sprintf 
                     "WARNING: Initial resolution of \"%s faild: %s"
                     dom (Misc_utils.print_exn hmm))
         ]
        )
        domains_all;

    log_func "Search Engines IPs successfully resolved"
};

(*=======================*)
(*  Req/Res/State Types: *)
(*=======================*)
(*--------------*)
(*  "se_req*":  *)
(*--------------*)
(*  Type of Search Engine requests. Currently, the search terms are given just
    by a string; however, for advanced search, it could include Boolean exprs.
    The caller also provides the info,  for all Search Engines to be used,  on
    the Current URL to be enquiried,  the Previous URL (the Referer),  and the
    Cookies to be used in the curr request,  all taken from the Cache  (unless
    this is the first request in the curr Session).    For XML-based SEs, such
    "extra_info" is not used, as we only get one page.
    For the curr request, "url_data" is (CurrURL, PrevURL, CurrCookies);
    for a future request, it becomes    (NextURL, LastURL, LastCookies):
*)
type url_data  = (string * string * list Http_common.http_cookie);
                 (* (CurrURL, PrevURL, CurrCookies) *)

type se_req =
{
    search_terms: string;
    n_results   : int;
    client_ip   : Unix.inet_addr;
    extra_info  : Hashtbl.t char url_data       (* Key: SE_Code *)
};

(*--------------*)
(*  "se_res*":  *)
(*--------------*)
(*  Search results coming from Search Engines are lists of the following
    records. The type depends on the kind of request:
*)
(*  An individual General-Purpose result: *)
type se_res_gen =
{
    from     :  mutable list (string * string);
                (* [(SE_Name, SE_URL)]. This field needs to be mutable for easy
                   merging of similar results from different Search Engines.
                *)
    url      :  string; (* Search result URL -- real location *)
    title    :  string; (* Ditto, title       *)
    descr    :  string; (* Ditto, description *)

    (* XXX: "size" and "last_modified" can also be provided, but they are curr-
       ently omitted, as not supported by all Search Engines.
    *)
    (* The following fields are used for sorting the results: *)
    nurl     :  string; (* "Normalised" "url" -- useful for comparison   *)
    index    :  float;  (* Pos of the result in this SE's output, from 1 *)
    weight   :  float   (* Weight of the corresp SE           *)
};

(*  An individual XML-based result: *)
type se_res_xml =
{
    xtitle  :   string; (* Title *)
    xdescr  :   string; (* Descr *)
    xurl    :   string; (* Real URL of the page found    *)
    xnurl   :   string; (* Normalised real URL           *)
    xclick  :   string; (* Click-through URL for the result found *)
    revenue :   float   (* Advertising revenue from this result   *)
};

(*  The over-all results type: *)
type se_ress =
[   (*  Results from a General-Purpose Search Engine:
        SE name, list of actual results plus the SE Code, URL data
        for future requests:
    *)
    Res_Gen of string and list se_res_gen and char and url_data

|   (* Results from an XML-Based Search Engine:
       SE name, list of results, list of related Search Terms:
    *)
    Res_XML of string and list se_res_xml and list string
];

(*--------------------------*)
(*  Internal Worker state:  *)
(*--------------------------*)

(*  For General-Purpose Search Engines: *)
type se_state_gen =
{
    (* Configuration, from SE Interfaces and User Config: *)
    gse_code        : char;
    gse_name        : string;
    gse_domain      : string;
    gse_home_url    : string;

    (* Request format is as in the config: *)
    gse_req_format  : string;

    (* Compiled regexps for parsing the responses:    *)
    gse_parser      : Pcre.regexp;
    gse_extractor   : bool -> string -> Pcre.substrings -> string;

    (* How to get a link to the next page: *)
    gse_next        : Pcre.regexp;

    (* Weight of this Search Engine: *)
    gse_weight      : float;

    (* DNS agent, used in case if L2 DNS Cache fails. Normally built into
       the resolver closure inside the HTTP agent (see below); only needs
       to be mentioned here in order to be closed properly:
    *)
    gse_dns_agent   : Dns_client.dns_agent;

    (* The HTTP agent to access this Search Engine:   *)
    gse_agent       : Http_client.http_agent Tnio.tsock unit
};

(*  For XML-Based Search Engines: *)
type se_state_xml =
{
    (* Here we put just a copy of the SE Interface, as it does not require
       any processing:
    *)
    xinter          : Lukol_config.se_interface_xml;

    (* Data from the User Config: *)
    xse_affid       : string;
    xse_commission  : float;

    (* DNS agent:  *)
    xse_dns_agent   : Dns_client.dns_agent;

    (* HTTP agent: *)
    xse_agent       : Http_client.http_agent Tnio.tsock unit
};

(*  Integrated State Type: *)
type se_state =
[   State_Gen of se_state_gen
|   State_XML of se_state_xml
];


(*=======================*)
(*  Back_End Operations: *)
(*=======================*)
(*-----------------------*)
(*  HTML/URL management: *)
(*-----------------------*)
(*  "extract_html":
    Extracts data from Search Engines output, removing extraneous HTML
    formatting:
*)
value unwanted_tags = Pcre.regexp ~study:True "<.+?>" ~limit:match_limit;

value extract_html:
    Pcre.regexp -> bool -> string -> Pcre.substrings -> string =

fun re remove_tags key substr ->
try
    let data = Pcre.get_named_substring re key substr in
    if  remove_tags
    then Pcre.replace ~rex:unwanted_tags ~templ:"" data
    else data
with
    [Not_found -> ""];


(*  "fix_url":
    Another form of modifying the HTML output (cf "extract_html" above):
    Wrong symbols are replaced in URLs.  Also, the URL is made absolute:
*)
value special_re =
    Pcre.regexp ~study:True "(&amp;)|(&quot;)" ~limit:match_limit;

value remove_re  =
    Pcre.regexp ~study:True "(\\s+)|(<.*?>)"   ~limit:match_limit;

value fix_url: string -> string -> string =
fun prev_url url0 ->
    (* Replace HTML esc symbols, suppress HTML tags and spaces which SEs
       occasionally put inside visible representation of URLs:
    *)
    let url1 = Pcre.substitute ~rex:special_re
        ~subst:(fun s ->
            if  s="&amp;"  then "&"
            else
            if  s="&quot;" then "\""
            else
            s   (* Impossible *)
        )
        url0
    in
    let url2 = Pcre.replace ~rex:remove_re ~templ:"" url1
    in
    if prev_url <> ""
    then
        (* We consider "url2" to be full OR local within the current Search
           Engine, referenced by the "prev_url". Merge the URLs:
        *)
        Http_common.join_urls prev_url url2
    else
        (* The URL is considered to be NOT on the current Search Engine, as
           we don't have the latter!  We assume that it already begins with
           the domain name; just need a default protocol scheme if missing:
        *)
        if  (ExtString.String.starts_with url2 "http://" ) ||
            (ExtString.String.starts_with url2 "https://")
        then url2
        else "http://"^ url2;


(*  "normalise_url":
    Put a URL in a normal form for equality comparison:
*)
value normalise_url: string -> string =
fun url ->
try
    let (_, host, port, path_etc) = Http_common.parse_url url in
    let host' =
        (* Make sure "nhost" is in lower-case,   and remove its "www." prefix
           if exists -- the result will NOT be used for making requests, only
           for comparison!
        *)
        if   ExtString.String.starts_with host "www."
        then String.sub host 4 (String.length host - 4)
        else host
    in
    (* Also, parse the "path_etc", and sort all query parms in the alphabetical
       order. We only split the query into "&"-separated parts  (not going into
       "name=value", as this is not always the case):
    *)
    let (path, parms, query, frag) = Http_common.parse_urlpath path_etc in
    let qparts = List.sort compare (ExtString.String.nsplit query "&")  in

    (* Re-assemble the "query", "path_etc" and the whole URL: *)
    let query'    = String.concat "&" qparts                            in
    let path_etc' = Http_common.mk_urlpath path parms query' frag       in

    Http_common.mk_url "" host' port path_etc'
with
[_ ->
    (* For various "not-so-well-formed" URLs which fail to normalise:
       Keep them as they are:
    *)
    url
];

(* Requests Forming:
   Prepare the substitution engine for the Search Engine request line template.
   General-Purpose SEs: "${st}", "${nr}" parms;
   XML-Based       SEs: "${st}", "${nr}", "${ip}", "${affid}":
*)
value req_re_gen = Pcre.regexp ~study:True ~limit:match_limit
    "(\\$\\{st\\})|(\\$\\{nr\\})";

value req_re_xml = Pcre.regexp ~study:True ~limit:match_limit
    "(\\$\\{st\\})|(\\$\\{nr\\})|(\\$\\{ip\\})|(\\$\\{affid\\})";


(*--------------------*)
(*  "se_client_gen":  *)
(*--------------------*)
(*  Performs General-Purpose Search Engine requests: *)

value se_client_gen:
    se_req -> se_state_gen -> float -> se_ress =

fun req state deadline ->
try
    (* Do we have the next URL to use by this Session and this Search Engine,
       from the Cache? If so, it becomes the current "req_url". We will also
       get the "prev_url" (the Referer) and the "cookies" to be used:
    *)
    let (prev_url, req_url, cookies) =
    try
        Hashtbl.find req.extra_info state.gse_code 
    with
    [Not_found ->
        (* No pre-recorded URLs -- generate the initial ones: *)

        let req_path  = Pcre.substitute ~rex:req_re_gen
            ~subst:
            (fun ["${st}" -> Netencoding.Url.encode ~plus:True req.search_terms
                 |"${nr}" -> string_of_int   req.n_results
                 |_       -> assert False]
            )
            state.gse_req_format
        in
        (* The Referer is the Home Page of this Search Engine, and there are
           no Cookies:
        *)
        (state.gse_home_url, "http://" ^ state.gse_domain ^ req_path, [])
    ]
    in
    (* If the "req_url" is invalid (as came from the Cache), no more results
       are forthcoming. THIS IS WHY we need an SE name in "Res_Gen" directly,
       not only in results themselves -- if the latter are empty,   we could
       not determine where they came from!
    *)
    if  req_url = Clients_simple.invalid_url
    then
        Res_Gen  state.gse_name [] state.gse_code
                 (Clients_simple.invalid_url, Clients_simple.invalid_url, [])
    else
    (* Try to get the required pages -- it may still be OK  if we got less
       entries than expected, the outer level (combining the outout of all
       Search Engines engaged) will re-iterate the request if necessary:
    *)
    let http_req  =
    {
        Http_client.http_method   = "GET";
        Http_client.full_url      = req_url;
        Http_client.extra_headers = [("Referer", prev_url)];
        Http_client.cookies       = cookies;
        Http_client.form          = []
    }
    in
    let before =
        if   trace ()
        then
            let bf = Unix.gettimeofday () in
            do {
                Printf.fprintf stderr "TRYING %s: %s\n%!"
                    state.gse_name req_url;
                bf
            }
        else 0.0
    in
    let resp =
        Http_client.http_transaction state.gse_agent http_req deadline
    in
    let resp_body = resp.Http_client.body   in

    let () =
        if  trace ()
        then
            let after = Unix.gettimeofday() in
            Printf.fprintf stderr "GOT    %s: %d bytes in %f sec\n%!"
                state.gse_name (String.length resp_body) (after -. before)
        else ()
    in
    (* Parse the response: *)
    let parsed_resp =
        try
            Pcre.exec_all ~rex:state.gse_parser resp_body
        with
        [Not_found -> [||] ]
    in
    let () =
        if   trace ()
        then Printf.fprintf stderr "PARSED %s: %d entries\n%!"
             state.gse_name (Array.length parsed_resp)
        else ()
    in
    (* Form the result: *)
    let res =
        Array.mapi
        (fun i substr ->
            let url    = state.gse_extractor False "URL"   substr in
            let title  = state.gse_extractor True  "TITLE" substr in
            let descr  = state.gse_extractor True  "DESCR" substr in
            let furl   = fix_url "" url                           in
            let ()     =
            if  trace ()
                then Printf.fprintf stderr
                    "\t%s: %s at %s\n%!" state.gse_name title furl
                else  ()
            in
            {
                from   = [(state.gse_name, req_url)];
                url    = furl;
                nurl   = normalise_url furl;
                title  = title;
                descr  = descr;
                index  = (float_of_int i) +. 1.0;
                weight = state.gse_weight
            }
        )
        parsed_resp
    in
    (* Also extract the new cookies: *)
    let cookies' =
        Http_common.get_cookies (Http_common.Client_Side state.gse_name)
        resp.Http_client.headers
    in
    (* Extract the "next_url" -- will be needed in any case: *)
    let next_url =
    try
        fix_url req_url
        (Pcre.extract ~full_match:False ~rex:state.gse_next resp_body).(0)
    with
        [_ -> Clients_simple.invalid_url] (* No next URL! *)
    in
    let () =
        if   trace ()
        then Printf.fprintf stderr "NEXT   %s: %s\n%!" state.gse_name next_url
        else ()
    in
    let future_urls =
        (* This depends on whether we got any results: *)
        if  Array.length res = 0
        then
            (* No more results from this Search Engine -- set an invalid
               "next URL", to mark the end of the search:
            *)
            (req_url, Clients_simple.invalid_url, [])
        else
            (* Got something. Memoise the "req_url", "next_url" and "cookies'",
               in case if more results will be required in the future:
            *)
            (req_url, next_url, cookies')
    in
    (* The result: *)
    Res_Gen state.gse_name (Array.to_list res) state.gse_code future_urls
with
[ hmm ->
  do {
    (* Log and re-raise the error: *)
    log (Printf.sprintf "%s General-Purpose Back-End: ERROR on \"%s\": %s"
        state.gse_name req.search_terms (Misc_utils.print_exn hmm));
    raise hmm
}];

(*--------------------*)
(*  "se_client_xml":  *)
(*--------------------*)
(*  Performs XML-Based Search Engine requests: *)

value se_client_xml:
    se_req -> se_state_xml -> float -> se_ress =

fun req state deadline ->
try
    (* Make the HTTP request: *)
    let req_path  = Pcre.substitute ~rex:req_re_xml
        ~subst:
        (fun ["${st}"   -> Netencoding.Url.encode ~plus:True req.search_terms
             |"${nr}"   -> string_of_int   req.n_results
             |"${ip}"   -> Unix.string_of_inet_addr req.client_ip
             |"${affid}"-> state.xse_affid
             |_         -> assert False]
        )
        state.xinter.Lukol_config.xse_req_format
    in
    let req_host  = state.xinter.Lukol_config.xse_domain in
    let req_url   = "http://"^req_host^req_path          in
    let http_req  =
    {
        Http_client.http_method   = "GET";
        Http_client.full_url      = req_url;
        Http_client.extra_headers = [];
        Http_client.cookies       = [];
        Http_client.form          = []
    }
    in
    (* Submit the request: *)
    let resp =
        Http_client.http_transaction state.xse_agent http_req deadline
    in
    (* Parse the XML body of the response: *)
    let xml_stream = SaXml.of_string resp.Http_client.body in

    (* The following "vars" contain, for each tag name to be looked for,
       a flag indicating whether  this tag is currently being processed,
       and the  current value  of this tag:
    *)
    let vars = Hashtbl.create 20 in
    let ()   =
        List.iter (fun tag -> Hashtbl.add vars tag (ref False, ref ""))
        [
            state.xinter.Lukol_config.all_results_tag;
            state.xinter.Lukol_config.each_result_tag;
            state.xinter.Lukol_config.title_tag;
            state.xinter.Lukol_config.descr_tag;
            state.xinter.Lukol_config.orig_url_tag;
            state.xinter.Lukol_config.click_url_tag;
            state.xinter.Lukol_config.abs_revenue_tag;
            state.xinter.Lukol_config.all_related_tag;
            state.xinter.Lukol_config.each_related_tag
        ]
    in
    (* Short-cuts to some "scope flags" from the above: *)
    let in_all_results  =
        fst (Hashtbl.find vars state.xinter.Lukol_config.all_results_tag)   in
    let in_each_result  =
        fst (Hashtbl.find vars state.xinter.Lukol_config.each_result_tag)   in
    let in_all_related  =
        fst (Hashtbl.find vars state.xinter.Lukol_config.all_related_tag)   in

    (* Result data field names, and short-cuts to their values: *)
    let res_fld_tags    =
        [
            state.xinter.Lukol_config.title_tag;
            state.xinter.Lukol_config.descr_tag;
            state.xinter.Lukol_config.orig_url_tag;
            state.xinter.Lukol_config.click_url_tag;
            state.xinter.Lukol_config.abs_revenue_tag
        ]
    in
    let title =
        Hashtbl.find vars state.xinter.Lukol_config.title_tag       in
    let descr       =
        Hashtbl.find vars state.xinter.Lukol_config.descr_tag       in
    let orig_url    =
        Hashtbl.find vars state.xinter.Lukol_config.orig_url_tag    in
    let click_url   =
        Hashtbl.find vars state.xinter.Lukol_config.click_url_tag   in
    let abs_revenue =
        Hashtbl.find vars state.xinter.Lukol_config.abs_revenue_tag in
    let each_related=
        Hashtbl.find vars state.xinter.Lukol_config.each_related_tag
    in
    (* Updatable fields: also include related entries: *)
    let fields_lst  =
        [title; descr; orig_url; click_url; abs_revenue; each_related]
    in
    (*  Scope checking for flags: *)

    let scope_ok: string -> bool =
    fun tag_name ->
       (tag_name = state.xinter.Lukol_config.each_result_tag  &&
        in_all_results.val)
       ||
       (List.mem tag_name res_fld_tags && in_each_result.val)
       ||
       (tag_name = state.xinter.Lukol_config.each_related_tag &&
                 in_all_related.val)
       ||
       (* These tags provide the outer-most scope: *)
       (tag_name = state.xinter.Lukol_config.all_results_tag) ||
       (tag_name = state.xinter.Lukol_config.all_related_tag)
    in
    (* The list of results will be accumulated here -- the order does not
       matter, as it will later be sorted anyway:
    *)
    let xress   = ref [] in
    let related = ref [] in

    (* Function which assigns data to the "active" field var: *)
    let assign_data: string -> unit =
    fun data_str ->
        match List.filter (fun (flag, _) -> flag.val) fields_lst with
        [  [(_, active_var)] ->
            active_var.val := data_str
        |  _ -> ()

        ]
    in
    (* Function which reads all tokens from the XML stream:   *)
    let rec read_xml_tokens: unit -> unit =
    fun () ->
    try
        let token = Stream.next xml_stream in
        let () = match token with

        [ SaXml.Tag (SaXml.STag start) ->
            (* Opening Tags: set the corresp flag, and re-set the data,
               anticipating the latter's update. Check ths scope first:
            *)
            let tag_name = start.SaXml.tag_name in
            if  scope_ok tag_name
            then
                try let (fvar, dvar) = Hashtbl.find vars tag_name in
                do {
                    fvar.val := True;
                    dvar.val := ""
                }
                with [Not_found -> ()]     (* Ignore this tag *)
            else
                ()                         (* Ignore this tag *)

        | SaXml.Tag (SaXml.ETag tag_name) ->
            (* Closing Tags: re-set the corresp flag, if the scope is OK: *)
            if  scope_ok tag_name
            then
                try let (fvar, dvar) = Hashtbl.find vars tag_name in
                do {
                    fvar.val := False;

                    (* Furthermore, for certain closing tags, form resulting
                       data objects:
                    *)
                    if  tag_name = state.xinter.Lukol_config.each_result_tag
                    then
                        (* Form a new "xress" entry. The real revenue depends
                           on the absolute revenue and the commission rate.
                           NB: "dvar" itself does not matter here:
                        *)
                        let title_fld       = snd title         in
                        let descr_fld       = snd descr         in
                        let orig_url_fld    = snd orig_url      in
                        let click_url_fld   = snd click_url     in
                        let abs_revenue_fld = snd abs_revenue   in

                        let furl = fix_url "" orig_url_fld.val  in
                        let xres =
                        {
                            xtitle  = title_fld.val;
                            xdescr  = descr_fld.val;
                            xurl    = furl;
                            xnurl   = normalise_url furl;
                            xclick  = click_url_fld.val;
                            revenue = (float_of_string abs_revenue_fld.val) *.
                                      state.xse_commission
                        }
                        in
                        do {
                            (* To be on the safe side, re-set the data flds: *)
                            List.iter
                                (fun fld -> fld.val := "")
                                [title_fld; descr_fld; orig_url_fld;
                                 click_url_fld;     abs_revenue_fld];

                            (* Save the newly-formed result: *)
                            xress.val := [xres :: xress.val]
                        }
                    else
                    if  tag_name = state.xinter.Lukol_config.each_related_tag
                    then
                    do {
                        (* Form a new "related" entry: *)
                        related.val := [dvar.val :: related.val]
                    }
                    else
                        () (* No special treatment of this closing tag *)
                }
                with [Not_found -> ()]     (* Ignore this tag *)
            else
                ()                         (* Ignore this tag *)

        | SaXml.Text text ->
            (* A text field, to be assigned according to the currently open
               field -- if any:
            *)
            assign_data (SaXml.string_of_text text)

        | SaXml.Tag (SaXml.CData cdata) ->
            (* Same as "Text": *)
            assign_data cdata

        | _ -> () (* Ignore any other elements *)
        ]
        in
        (* Recursive call: *)
        read_xml_tokens ()
    with
    [Stream.Failure  -> () (* The stream is finished *)]
    in
    (* Apply the parser: *)
    let () = read_xml_tokens () in

    (* Return the SE name and the accumulated results: *)
    Res_XML state.xinter.Lukol_config.xse_name xress.val related.val
with
[ hmm ->
  do {
    (* Log and re-raise the error: *)
    log (Printf.sprintf "%s XML-Based Back-End: ERROR on \"%s\": %s"
        state.xinter.Lukol_config.xse_name
        req.search_terms
        (Misc_utils.print_exn hmm));
    raise hmm
}];

(*----------------------*)
(*  Integrated Worker:  *)
(*----------------------*)
(*  This is the body of the "Gen_server" Worker Threads: *)

value se_client:
    se_req -> se_state -> float -> (se_ress * se_state) =

fun req state deadline ->
    (* Dispatch the request according to the WORKER's kind -- the "req"
       type is the same in both cases. The state remains unchanged:
    *)
    match state with
    [ State_Gen gs -> (se_client_gen req gs deadline, state)
    | State_XML xs -> (se_client_xml req xs deadline, state)
    ];

(*====================*)
(*  State Management: *)
(*====================*)
(*----------------*)
(*  "mk_agents":  *)
(*----------------*)
(*  Creates a DNS and an HTTP agent -- common part for General-Purpose and
    XML-Based worker state initialisation.
    XXX:   Currently, the list of emulated browsers does not depend on the
    destination URL (it would be difficult to do for search term high-lighting,
    anyway):
*)
value mk_agents:
    list string -> (string -> unit) ->
    (Dns_client.dns_agent * (Http_client.http_agent Tnio.tsock unit)) =

fun ips_list log_func ->
    let client_ips= List.map Unix.inet_addr_of_string ips_list
    in
    let emulate_browsers =
       (Lukol_config.get_global_conf()).
        Lukol_config.be_config.Lukol_config.emulate_browsers
    in
    (* HTTP client agent config: *)
    let agent_conf=
    {
        Http_client.conf_client_ips = client_ips;
        Http_client.conf_use_persist= False;  (* As yet *)
        Http_client.conf_buff_size  = Clients_simple.http_buff_size;
        Http_client.conf_user_agents= emulate_browsers
    }
    in
    (* Create the DNS Agent:      *)
    let dns_servers =(Lukol_config.get_global_conf()).Lukol_config.dns_servers
    in
    let dns_agent   = Clients_simple.mk_dns_agent dns_servers in

    (* Now create the HTTP Agent: *)
    let http_agent  =
        Http_client.mk_http_agent
           (se_resolver dns_agent log_func)
           (fun () -> Dns_client.close_dns_agent dns_agent)
            Clients_simple.http_transport
            None  (* No secure transport *)
            agent_conf
    in
    (dns_agent, http_agent);


(*------------------------------------------------*)
(*  General-Purpose Worker State Initialisation:  *)
(*------------------------------------------------*)
value se_state_init_gen:
    Lukol_config.se_user_config_gen ->
    Lukol_config.se_interfaces_gen  ->
    list string     ->
    (string -> unit)->
    unit            ->
    se_state        =

fun uc interfs_gen emulate_browsers log_func ()  ->

    (* Check the weight of this SearchEngine: *)
    let name      = uc.Lukol_config.gse_name_ref in
    let weight    = uc.Lukol_config.gse_weight   in
    if  weight < 0.0
    then
        failwith
        ("Gen_back_end.se_state_init_gen: Invalid weight of \""    ^name^"\"")
    else
    (* Find the "interfs_gen" entry for this "uc": *)
    let interf =
    try
        List.find (fun i -> i.Lukol_config.gse_name = name) interfs_gen
    with
    [Not_found ->
        failwith
        ("Gen_back_end.se_state_init_gen: No interface data for \""^name^"\"")
    ]
    in
    (* Compile the Parser RegExp: *)

    let parser_re = Pcre.regexp ~study:True ~flags:[`DOTALL] ~limit:match_limit
                    interf.Lukol_config.gse_parser
    in
    let extractor = extract_html parser_re
    in
    (* The RegExp to fetch the "next page" URL from this Search Engine's
       output:
    *)
    let next      =
        Pcre.regexp ~study:True interf.Lukol_config.gse_next ~limit:match_limit
    in
    (* Create the Agents: *)
    let (dns_agent,  http_agent) =
        mk_agents uc.Lukol_config.gse_client_ips log_func
    in
    (* The State: *)
    State_Gen
    {
        gse_code        = uc.Lukol_config.gse_code;
        gse_name        = name;
        gse_domain      = interf.Lukol_config.gse_domain;
        gse_home_url    = "http://"^ interf.Lukol_config.gse_home^ "/";
        gse_req_format  = interf.Lukol_config.gse_req_format;
        gse_parser      = parser_re;
        gse_extractor   = extractor;
        gse_next        = next;
        gse_weight      = weight;
        gse_dns_agent   = dns_agent;
        gse_agent       = http_agent
    };

(*-----------------------------------------*)
(*  XML-Based Worker State Initialisation: *)
(*-----------------------------------------*)
value se_state_init_xml:
    Lukol_config.se_user_config_xml ->
    Lukol_config.se_interfaces_xml  ->
    list string         ->
    (string -> unit)    ->
    unit                ->
    se_state            =

fun xc interfs_xml emulate_browsers log_func ()  ->

    (* Find the "interfs_xml" entry for this "xc": *)
    let name   = xc.Lukol_config.xse_name_ref in
    let interf =
    try
        List.find (fun i -> i.Lukol_config.xse_name = name) interfs_xml
    with
    [Not_found ->
        failwith
        ("Gen_back_end.se_state_init_xml: No interface data for \""^name^"\"")
    ]
    in
    (* Create the Agents: *)
    let (dns_agent, http_agent) =
        mk_agents xc.Lukol_config.xse_client_ips log_func
    in
    (* The state: *)
    State_XML
    {
        xinter          = interf;
        xse_affid       = xc.Lukol_config.xse_affid;
        xse_commission  = xc.Lukol_config.xse_commission;
        xse_dns_agent   = dns_agent;
        xse_agent       = http_agent
    };

(*-------------------------*)
(*  Worker State Clean-Up: *)
(*-------------------------*)
(* NB: a deadline is used for closing the Agents, but any errors (including
   deadline-related) are ignored:
*)
value se_state_cleanup: se_state -> unit =
fun state ->
    (* Some reasonable time-out for clean-up: *)
    let deadline = Unix.gettimeofday () +. 10.0 in
    try
        match state with
        [ State_Gen st ->
         do {
            Dns_client.close_dns_agent st.gse_dns_agent;
            Http_client.close_agent    st.gse_agent  deadline
         }

        | State_XML st ->
         do {
            Dns_client.close_dns_agent st.xse_dns_agent;
            Http_client.close_agent    st.xse_agent  deadline
         }
        ]
    with [_ -> ()];

(*=========================*)
(*  Top-Level Integration: *)
(*=========================*)
(*  Integrated configuration type: *)

type integr_conf =
[   Conf_Gen of Lukol_config.se_user_config_gen
            and Lukol_config.se_interfaces_gen

|   Conf_XML of Lukol_config.se_user_config_xml
            and Lukol_config.se_interfaces_xml
];

(*--------------------------------*)
(*  Full Back-End Server Config:  *)
(*--------------------------------*)
value mk_se_config:
    integr_conf         ->
    list string         ->
    (string -> unit)    ->
    ~max_backlog:int    ->
    ~worker_prio:int    ->
    Gen_server.server_config se_req se_state se_ress =

fun ic emulate_browsers log_func ~max_backlog ~worker_prio ->

    (* The state initialiser, as a partially applied function -- it carries the
       data from SE User Config and SE Interfaces into the SE State:
    *)
    let (state_init, n_threads)   =
        match ic with
        [ Conf_Gen uc interfs_gen ->
            (se_state_init_gen uc interfs_gen emulate_browsers log_func,
             uc.Lukol_config.gse_max_threads)

        | Conf_XML xc interfs_xml ->
            (se_state_init_xml xc interfs_xml emulate_browsers log_func,
             xc.Lukol_config.xse_max_threads)
        ]
    in
    (* Now the config -- common for both kinds: *)
    {
        (* All worker threads are created immediately, and do not die: *)
        Gen_server.max_all_threads      = n_threads;
        Gen_server.max_idle_threads     = n_threads;
        Gen_server.min_idle_threads     = 1;
        Gen_server.init_threads         = n_threads;

        (* The LOWEST priority is assigned to the back-end worker threads: *)
        Gen_server.worker_priority      = worker_prio;
        Gen_server.worker_stack_k       = 256;
        Gen_server.max_queue_len        = max_backlog;

        Gen_server.init_worker_state    = state_init;
        Gen_server.cleanup_state        = se_state_cleanup;
        Gen_server.reset_state_on_error = False;
        Gen_server.proc_func            = se_client
    };

(*----------------------------*)
(*  Back-End Initialisation:  *)
(*----------------------------*)
(*  A separate "Gen_server" is created for each Search Engine we are using.
    General-Purpose servers are then organised into a hash table according
    to their 1-char  server codes.    XML-Based servers are just kept in a
    list.   In both cases, for statistical purposes, we also leep the full
    Server Name:
*)
type  be_server = ((Gen_server.server_t se_req se_state se_ress) * string);

value be_servers_gen: Hashtbl.t  char be_server  = Hashtbl.create 100;
value be_servers_xml: ref       (list be_server) = ref [];

value init:
    Lukol_config.se_interfaces_gen   ->
    Lukol_config.se_interfaces_xml   ->
    ~be_worker_prio:int              ->
    (string -> unit) ->
    unit    =
fun interfs_gen interfs_xml ~be_worker_prio log_func ->
let c = Lukol_config.get_global_conf () in
try
do {
    (* Set up the Logger: *)
    log_func_ref.val    := log_func;

    (* Initialise the DNS resolver used for SEs: *)
    init_se_resolver
        c.Lukol_config.dns_servers interfs_gen interfs_xml log_func;

    (* Some Back-End Parms: *)
    let emulate_browsers =
        c.Lukol_config.be_config.Lukol_config.emulate_browsers
    in
    (* NB: The max backlog of requests to each back-end server cannot be
       greater than the max number of simultaneous client connections:
    *)
    let max_backlog =
        c.Lukol_config.fe_config.Lukol_config.max_connections
    in
    do {
        (* Create the General-Purpose Servers, one per each Search Engine
           configured:
        *)
        List.iter
        (fun uc ->
            let code = uc.Lukol_config.gse_code     in
            let name = uc.Lukol_config.gse_name_ref in

            (* Check that the code uniquely identifies the server:    *)
            if  Hashtbl.mem be_servers_gen code
            then
                failwith
                (Printf.sprintf "Duplicate Search Engine Code: \"%c\"" code)
            else
            (* Create the server config. The max backlog cannot be greater than
               the max number of client connections accepted by the whole Meta-
               Search Engine:
            *)
            let sec  =
                mk_se_config
                    (Conf_Gen uc interfs_gen)
                    emulate_browsers
                    log_func
                    ~max_backlog:max_backlog
                    ~worker_prio:be_worker_prio
            in
            (* Create the server itself: *)
            let srv = (Gen_server.create_server sec, name) in

            (* Attach the server to the global hash table: *)
            Hashtbl.add be_servers_gen code srv
        )
        c.Lukol_config.be_config.Lukol_config.gse_uconfs;

        log ("INFO: Back-End (General-Purpose Search Engines Interface) "^
             "successfully created");

        (* Now create the XML-Based servers: *)
        List.iter
        (fun xc ->
            (* Create the server config: *)
            let name = xc.Lukol_config.xse_name_ref in
            let sec  =
                mk_se_config
                    (Conf_XML xc interfs_xml)
                    emulate_browsers
                    log_func
                    ~max_backlog:max_backlog
                    ~worker_prio:be_worker_prio
            in
            (* Create the server itself: *)
            let srv  = (Gen_server.create_server sec, name) in

            (* Attach the server to the global list: *)
            be_servers_xml.val := [srv :: be_servers_xml.val]
        )
        c.Lukol_config.be_config.Lukol_config.xse_uconfs;

        log ("INFO: Back-End (XML-Based Search Engines Interface) " ^
             "successfully created")
    }
}
with
[hmm ->
    failwith ("Back_end.init: ERROR: "^Misc_utils.print_exn hmm)
];

(*--------------------------*)
(*  Accessing the Servers:  *)
(*--------------------------*)
value gen_server_by_code: char -> be_server =
fun code ->
    Hashtbl.find be_servers_gen code;

value all_gen_servers:    unit -> list (char * be_server) =
fun () ->
    Misc_utils.hashtbl_to_list be_servers_gen;

value all_xml_servers:    unit -> list be_server =
fun () ->
    be_servers_xml.val;

(*============================*)
(*  Updating the Interfaces:  *)
(*============================*)
(*  As the SE Interfaces data is embedded into the state of worker threads
    via partially-applied functions, the server will need to be re-started
    after the update:
*)
value update_engines: string -> float -> unit =
fun group deadline ->
    (* The file and URL to be used: *)
    let (file, src) =
        match group with
        [ "Gen" ->
            let interfs_file_gen =
               (Lukol_config.get_global_conf()).
                Lukol_config.be_config.Lukol_config.gse_interfs_file
            in
            let interfs_src_gen  =
               (Lukol_config.get_global_conf()).
                Lukol_config.be_config.Lukol_config.gse_interfs_src
            in
            (interfs_file_gen, interfs_src_gen)
        | "XML" ->
            let interfs_file_xml =
               (Lukol_config.get_global_conf()).
                Lukol_config.be_config.Lukol_config.xse_interfs_file
            in
            let interfs_src_xml  =
               (Lukol_config.get_global_conf()).
                Lukol_config.be_config.Lukol_config.xse_interfs_src
            in
            (interfs_file_xml, interfs_src_xml)
        | _ ->
            invalid_arg ("Back_end.update_engines: "^group)
        ]
    in
    (* Create a 1-time HTTP agent: *)
    let http_agent = snd (mk_agents [] log_func_ref.val) in

    (* Make a request: *)
    let http_req  =
    {
        Http_client.http_method   = "GET";
        Http_client.full_url      = src;
        Http_client.extra_headers = [];
        Http_client.cookies       = [];
        Http_client.form          = []
    }
    in
    (* Carry out the request: *)
    let res = Http_client.http_transaction http_agent http_req deadline in

    (* Save the file retrieved -- do it without locking: *)
    let ch  = open_out file in
    do {
        output_string ch res.Http_client.body;
        close_out ch
    };

