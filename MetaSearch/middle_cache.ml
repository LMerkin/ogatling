(*pp camlp4r *)
(* vim:ts=4:syntax=ocaml
*)
(*===========================================================================*)
(*                              "middle_cache.ml":                           *)
(*  Middle-Tier Session-Based Cache for the Gatling/Lukol MetaSearch Engine  *) 
(*                      (C) Explar Technologies Ltd, 2004                    *)
(*===========================================================================*)
(*---------------------------------------------*)
(*  Global Data and Read-Only Access Methods:  *)
(*---------------------------------------------*)
value page_size: unit -> int =
fun () ->
   (Lukol_config.get_global_conf()).
    Lukol_config.fe_config.Lukol_config.page_size;

value xml_size : unit -> int =
fun () ->
   (Lukol_config.get_global_conf()).
    Lukol_config.fe_config.Lukol_config.top_xml_results;

value cache_ttl: unit -> float =
fun () ->
   (Lukol_config.get_global_conf()).
    Lukol_config.be_config.Lukol_config.cache_ttl;

value trace:     unit -> bool =
fun () ->
   (Lukol_config.get_global_conf()).Lukol_config.trace_mode;


(*---------------*)
(*  The Logger:  *)
(*---------------*)
value log_func_ref  = ref (fun _ -> ());

value log:   string -> unit =
fun msg ->
    log_func_ref.val msg;


(*---------------*)
(*  Statistics:  *)
(*---------------*)
(* Configuration parms for statistics: *)

value n_steps_ref = ref 0; (* Filled in by "init" *)

value n_steps :  unit -> int   =
fun () ->
do {
    assert (n_steps_ref.val > 0);
    n_steps_ref.val;
};

(*  For "se_stats" and "mc_stats" details, see the module interface: *)
type se_stats =
{
    se_http_reqs    : mutable int;
    completed_reqs  : mutable int;
    succ_reqs       : mutable int;
    cum_http_time   : mutable float;
    resp_time_distr : array   int
};

type mc_stats =
{
    stats_begin     : mutable float;
    user_reqs       : mutable int;
    cum_resp_time   : mutable float;
    cached_reqs     : mutable int;
    salvos          : mutable int;
    distr_step      : mutable float;
    se_stats        : Hashtbl.t string se_stats
};

(* The global stats are accumulated here: *)
value stats: mc_stats =
{
    stats_begin     = 0.0; (* Will be set by "init" *)
    user_reqs       = 0;
    cum_resp_time   = 0.0;
    cached_reqs     = 0;
    salvos          = 0;
    distr_step      = 0.0;
    se_stats        = Hashtbl.create 100
};

(*-----------------------------*)
(*  The Global Results Cache:  *)
(*-----------------------------*)
(*  The key for this Cache is a concatenation of "st" and "gses" fields: *)

type cached_ress =
{
    (* Request Details: *)
    st:         string;                 (* Search Terms used             *)
    gses:       string;                 (* General-Purpose SEs enquired  *)
    expir:      mutable float;          (* Abs deadline for this session *)

    (* General-Purpose and XML-Based results are stored separately. For the
       former, we keep the found entries themselves in "gen_ress",  and the
       URL info in Hash tables defined below.    XML-Based results are also
       split into entries and related terms:
    *)
    gen_ress:   mutable list Back_end.se_res_gen;
    xml_ress:   mutable list Back_end.se_res_xml;

    (* URL info from General-Purpose results: *)
    last_urls:  Hashtbl.t char string;  (* Search Engine Code -> URL     *)
    next_urls:  Hashtbl.t char string;  (* Search Engine Code -> URL     *)
                                        (* Search Engine Code -> Cookies *)
    cookies  :  Hashtbl.t char (list Http_common.http_cookie);

    (* Related terms for XML-Based results: *)
    related  :  mutable list string
};

value ress_cache:
    Hashtbl.t string cached_ress = Hashtbl.create 10000;


(*--------------------*)
(*  "new_empty_ress": *)
(*--------------------*)
value new_empty_ress: string -> string -> cached_ress =
fun st gses ->
{
    st        = st;
    gses      = gses;
    expir     = Unix.gettimeofday () +. cache_ttl ();
    gen_ress  = [];
    xml_ress  = [];
    last_urls = Hashtbl.create 100;
    next_urls = Hashtbl.create 100;
    cookies   = Hashtbl.create 100;
    related   = []
};

(*-----------------------*)
(*  "get_cached_pages":  *)
(*-----------------------*)
(*  Getting the result pages from the cache, by SearchTerms and General-Purpose
    Search Engines used.
*)
value get_cached_pages:
    string -> string -> cached_ress =

fun st gses ->
    (* Make the Key and search the Cache: *)
    let key = st^" "^gses in
    try
        let cr1 = Hashtbl.find ress_cache key in
        do {
            (* Got data from the Cache -- extend their life, as they are
               no win use:
            *)
            cr1.expir := Unix.gettimeofday () +. cache_ttl ();
            cr1
        }
    with
    [Not_found ->
        let cr1 = new_empty_ress st gses in
        do {
            (* Create new empty result, and insert it into the Cache. It
               will then receive fresh data from Search Engines, via the
               Cache Handle returned:
            *)
            Hashtbl.replace ress_cache key cr1;
            cr1
        }
    ];


(*----------------------*)
(*  "select_page_gen":  *)
(*----------------------*)
(* Get the General-Purpose results from "cr" for the given page number: *)

value select_page_gen: cached_ress -> int -> list Back_end.se_res_gen =
fun cr page_no ->
    let from_pos  = (page_no-1) * (page_size ()) in
    let until_pos = from_pos    +  page_size ()  in

    let rec skip_and_get:
        list Back_end.se_res_gen -> int -> list Back_end.se_res_gen ->
        list Back_end.se_res_gen =

        fun curr cpos accum ->
            match curr with
            [ [] ->
                (* No more data in the source list, return what we got: *)
                accum

            | [hc::tcs] ->
                if  cpos <  from_pos
                then
                    (* Skip    this "hc": *)
                    skip_and_get tcs (cpos+1)  accum
                else
                if  cpos >= from_pos && cpos < until_pos
                then
                    (* Include this "hc": *)
                    skip_and_get tcs (cpos+1) (accum @ [hc])
                else
                    (* No point in getting any further: *)
                    accum
            ]
    in
    skip_and_get cr.gen_ress 0 [];

(*--------------------*)
(*  "purge_expired":  *)
(*--------------------*)
(*  The body of the thread which periodically removes expired results from the
    Cache:
*)
value rec purge_expired: unit -> unit =
fun () ->
do {
    let now = Unix.gettimeofday () in
    Hashtbl.iter
        (fun k v ->
            if   now > v.expir
            then Hashtbl.remove ress_cache k
            else ()
        )
        ress_cache;

    (* The period of the thread is set to 1/10 of the entries TTL: *)
    Tnio.thread_sleep (cache_ttl () /. 10.0);

    (* Start over: *)
    purge_expired ();
};

(*-----------------------*)
(* Ordering of results:  *)
(*-----------------------*)
(*  For General-Purpose results: *)

value compare_gen: Back_end.se_res_gen -> Back_end.se_res_gen -> int =
fun l r ->
    (* Compare the weights in the reverse order, --  highest-weighed results
       will then come first:
    *)
    let lw = l.Back_end.weight /. l.Back_end.index in
    let rw = r.Back_end.weight /. r.Back_end.index in
    compare rw lw;

(*  For XML-Based results: *)

value compare_xml: Back_end.se_res_xml -> Back_end.se_res_xml -> int =
fun l r ->
    (*  Compare the revenues, in the reverse order: *)
    compare r.Back_end.revenue l.Back_end.revenue;


(*------------------------*)
(* Similarity of results: *)
(*------------------------*)
(*  For General-Purpose results:
    Used in "save_uniq_ress". Currently, the results are considered to be
    similar if they have the same NORMALISED URLs, OR  the same title and
    description. Title and Descr are considered to be "the same", in par-
    ticular, if one of the strings is just the beginning of another.
    Also, the URLs are considered to be the same,  if they just differ by
    the trailing "/":
*)
value similar_gen: Back_end.se_res_gen -> Back_end.se_res_gen -> bool =
fun l r ->
    (l.Back_end.nurl = r.Back_end.nurl)     ||
    (l.Back_end.nurl = r.Back_end.nurl^"/") ||
    (r.Back_end.nurl = l.Back_end.nurl^"/")
    ||
    (((ExtString.String.starts_with l.Back_end.title r.Back_end.title)  ||
      (ExtString.String.starts_with r.Back_end.title l.Back_end.title))
     &&
     ((ExtString.String.starts_with l.Back_end.descr r.Back_end.descr)  ||
      (ExtString.String.starts_with r.Back_end.descr l.Back_end.descr))
    );

(*  For the XML-Based results: same similarity condition: *)

value similar_xml: Back_end.se_res_xml -> Back_end.se_res_xml -> bool =
fun l r ->
    (l.Back_end.xnurl = r.Back_end.xnurl)     ||
    (l.Back_end.xnurl = r.Back_end.xnurl^"/") ||
    (r.Back_end.xnurl = l.Back_end.xnurl^"/")
    ||
    (((ExtString.String.starts_with l.Back_end.xtitle r.Back_end.xtitle)  ||
      (ExtString.String.starts_with r.Back_end.xtitle l.Back_end.xtitle))
     &&
     ((ExtString.String.starts_with l.Back_end.xdescr r.Back_end.xdescr)  ||
      (ExtString.String.starts_with r.Back_end.xdescr l.Back_end.xdescr))
    );

(*------------------*)
(*  Unique results: *)
(*------------------*)
(*  For General-Purpose results: each of the "new_ress" is compared against
    "old_ress", and if not already there, appended to the latter. Otherwise,
    the "new_ress" entry in question is dropped,   its "from" data moved to
    the corresp equal element of "old_ress".  Returns the appended list and
    the number of new entries appended:
*)
value rec uniq_ress_gen':
    list Back_end.se_res_gen -> list Back_end.se_res_gen -> int ->
   (list Back_end.se_res_gen  * int) =

fun new_ress old_ress ucount ->
    match new_ress with
    [ [] ->
        (old_ress, ucount) (* All done *)

    | [hr::trs] ->
        (* Check "hr" for similarity to some entry in "cr.ress_gen":    *)

        let   mb_sim = Misc_utils.list_find (similar_gen hr) old_ress in
        match mb_sim with
        [ None  ->
            (* "hr" is not similar to any of the "old_ress"; append it: *)
            uniq_ress_gen' trs (old_ress @ [hr]) (ucount+1)

        | Some  er ->
          do {
            (* "hr" will be dropped,    its "from" field moved to "er": *)
            List.iter
                (fun hf ->
                    if   List.mem hf er.Back_end.from
                    then ()
                    else er.Back_end.from := er.Back_end.from @ [hf]
                )
                hr.Back_end.from;

            (* Drop "hr" and go ahead: *)
            uniq_ress_gen' trs old_ress ucount
          }
        ]
    ];

(*  "save_uniq_ress_gen":
    The wrapper: installs unique results in the Cache, returns the number of
    installed entries:
*)
value save_uniq_ress_gen:
    list Back_end.se_res_gen -> cached_ress -> int =

fun new_ress cr ->
    let (all_ress, ucount) = uniq_ress_gen' new_ress cr.gen_ress 0 in
    do {
        cr.gen_ress := all_ress;
        ucount
    };

(*  Uniqueness of XML-Based results:
    No need to move "xfrom" fields when a similar result is found. Also, no
    more than "n_xml" results are produced:
*)
value rec uniq_ress_xml':
    list Back_end.se_res_xml -> list Back_end.se_res_xml -> int ->
    list Back_end.se_res_xml =

fun new_ress old_ress rem_count ->
    if  rem_count <= 0
    then
        (* Don't install any more results: *)
        old_ress
    else
        match new_ress with
        [ [] ->
            (* No more new results to install *)
            old_ress

        | [hr::trs] ->
            (* Is "hr" unique? *)
            let   mb_sim = Misc_utils.list_find (similar_xml hr) old_ress in
            match mb_sim with
            [ None  ->
                (* Append this "hr": *)
                uniq_ress_xml' trs (old_ress @ [hr]) (rem_count-1)
            | Some _ ->
                (* Drop   this "hr": *)
                uniq_ress_xml' trs  old_ress rem_count
            ]
        ];

(*  "save_uniq_ress_xml":
    The wrapper. Saves found unique results in the Cache, but does NOT
    overwrite non-empty cached results with empty ones  (when the user
    travels back and forth the pages,  XML-Based results are generated
    for the 1st page only):
*)
value save_uniq_ress_xml:
    list Back_end.se_res_xml -> cached_ress -> int -> unit =

fun new_ress cr n_xml ->
    if   cr.xml_ress <> []
    then ()
    else cr.xml_ress := uniq_ress_xml' new_ress [] n_xml;


(*---------------------*)
(*  "update_stats_*":  *)
(*---------------------*)
(*  Same functions used for both General-Purpose and XML-Based servers: *)

value update_stats_before: string -> unit =
fun server_name ->

    (* Get the current stats on this server: *)
    let s =
    try
        Hashtbl.find stats.se_stats server_name
        (* Found existing statistics *)
    with
    [Not_found ->
        (* No stats yet for this server -- create and attach it now.
           An array for the resp time distr function  is allocated:
        *)
        let new_stats =
            {
                se_http_reqs    = 0;
                completed_reqs  = 0;
                succ_reqs       = 0;
                cum_http_time   = 0.0;
                resp_time_distr = Array.make (n_steps ()) 0
            }
        in
        do {
            Hashtbl.add stats.se_stats server_name new_stats;
            new_stats
        }
    ]
    in
    (* Update the "se_http_reqs" for this SE: *)
    s.se_http_reqs := s.se_http_reqs + 1;


value update_stats_after: string  -> int -> float -> unit =

fun server_name res_len resp_time ->
    (* Here the corresp stats record MUST already exist. NB: a "completed" req
       is NOT considered to be "successful" if 0 entries were received:
    *)
    let s = Hashtbl.find stats.se_stats server_name in
    do {
        s.completed_reqs := s.completed_reqs +  1;
        s.cum_http_time  := s.cum_http_time  +. resp_time;

        if   res_len > 0
        then s.succ_reqs := s.succ_reqs + 1
        else ();

        (* Update the distribution function for the response time: *)
        let i =
            int_of_float (resp_time /. stats.distr_step)
        in
        let n = Array.length s.resp_time_distr in
        let k =
            if   i < n
            then i      (* "The proper slot" *)
            else n-1    (* "All above"...    *)
        in
        s.resp_time_distr.(k) := s.resp_time_distr.(k) + 1
    };

(*---------------------------*)
(*  "from_search_engines*":  *)
(*---------------------------*)
(*  "n_extra" is the number of results which really  need to be fetched from
    the specified General-Purpose Search Engines (only those whose codes are
    in the "gses_str"); "n_xml" is a similar parm for XML-Based Engines. The
    results are installed in the Cache via the "cr" handle:
*)
value dummy_info = Hashtbl.create 0;

value rec from_search_engines':
    int    -> int -> string -> Unix.inet_addr  -> string ->
    list Back_end.be_server -> list Back_end.be_server   -> cached_ress ->
    float  -> unit =

fun n_extra n_xml search_terms client_ip gses_str gen_engines xml_engines
    cr deadline
->
    (* A new "salvo" is being prepared: *)
    let () = stats.salvos:= stats.salvos + 1 in

    (* The requests to the General-Purpose servers: *)
    let gen_reqs =
        if  gen_engines <> []
        then
            (* Form "extra_info" (URL and Cookies info) for all configured
               General-Purpose Search Engines.  This info is obtained from
               the (mutable) "cr" -- apply it before each request:
            *)
            let extra_info = Hashtbl.create 100 in
            let ()         =
                String.iter
                    (fun se_code ->
                    try
                        let last_url = Hashtbl.find cr.last_urls se_code in
                        let next_url = Hashtbl.find cr.next_urls se_code in
                        let cookies  = Hashtbl.find cr.cookies   se_code in

                        Hashtbl.add extra_info se_code
                           (last_url, next_url, cookies)
                    with
                    [Not_found ->
                        () (* Skip this "se_code" if some data are missing *)
                    ])
                    gses_str
            in
            (* "extra_info" is now formed *)
            List.map
                (fun (_, server_name) ->
                do {
                    (* Pre-update  the  statistics: *)
                    update_stats_before server_name;

                    (* The request to be submitted: *)
                    {
                        Back_end.search_terms = search_terms;
                        Back_end.n_results    = n_extra;
                        Back_end.client_ip    = client_ip;
                        Back_end.extra_info   = extra_info
                    }
                })
                gen_engines
        else
            []
    in
    (* The requests to the XML-Based servers: *)
    let xml_reqs =
        if  xml_engines <> []
        then
            (* No "extra_info" is required for XML-Based servers: *)
            List.map
                (fun (_, server_name) ->
                do {
                    (* Pre-update   the statistics; *)
                    update_stats_before server_name;

                    (* The request to be submitted: *)
                    {
                        Back_end.search_terms = search_terms;
                        Back_end.n_results    = n_xml;
                        Back_end.client_ip    = client_ip;
                        Back_end.extra_info   = dummy_info
                    }
                })
                xml_engines
        else
            []
    in
    (* Submit requests asynchronously to the specified Search Engines, and wait
       for the completion of the jobs or the deadline reached. Then collect the
       arrived results together, skipping missing ones.    The order of results
       does not matter here -- they will be sorted later.
       NB:  Both General-Purpose and  XML-Based Search Engines  are enquired in
       parallel at once, but requests to them are different:
    *)
    let all_reqs    = gen_reqs @ xml_reqs                      in

    (* Extract the actual server handles: *)
    let all_engines = List.map fst (gen_engines @ xml_engines) in

    (* SALVO !!! *)
    let all_ress    =
       Gen_server.from_many_servers all_reqs all_engines deadline
    in
    (* Separate General-Purpose and XML-Based results.  For the former,  save
       the latest URL info. For the latter, collect the list of Related Terms:
    *)
    let (gen_ress0, xml_ress0, related0) =
        List.fold_left
        (fun (curr_gen, curr_xml, curr_related) (res, rt)    ->
            match res with
            [ Back_end.Res_Gen gse_name grs se_code
                               (last_url, next_url, cookies) ->
              do {
                (* Save the URL info in the Cache: *)
                Hashtbl.replace cr.last_urls se_code last_url;
                Hashtbl.replace cr.next_urls se_code next_url;
                Hashtbl.replace cr.cookies   se_code cookies;

                (* Update the statistics: *)
                update_stats_after gse_name (List.length grs) rt;

                (* Update the "gen" list: *)
                (curr_gen @ grs, curr_xml, curr_related)
              }
            | Back_end.Res_XML xse_name xrs related ->
              do {
                (* Update the statistics; *)
                update_stats_after xse_name (List.length xrs) rt;

                (* Update the "xml" list: *)
                (curr_gen, curr_xml @ xrs, curr_related @ related)
              }
            ]
        )
        ([], [], []) all_ress
    in
    (* Analyse the XML-Based results first. Sort them in the descending order
       of revenues. XXX: doing sorting before eliminating duplicates is a bit
       inefficient, but simpler:
    *)
    let xml_ress1 = List.sort compare_xml xml_ress0 in

    (* Install at most "n_xml" unique XML-Based results in the Cache.  Do NOT
       override "old good" results with empty ones:
    *)
    let () = save_uniq_ress_xml xml_ress1 cr n_xml   in

    (* Eliminate duplicates from the "related" terms, sort them alphabetically
       and install them in the Cache (if got anything):
    *)
    let related1  = ExtList.List.unique related0     in
    let related2  = List.sort compare   related1     in
    let ()        =
        if   related2 <> []
        then cr.related:= related2
        else ()
    in
    (* NOW, analyse the General-Purpose results. Sort them taking into account
       the weights of the corresp Search Engines,  so that higher-weighed ress
       come first:
    *)
    let got0      = List.length           gen_ress0 in
    let gen_ress1 = List.sort compare_gen gen_ress0 in

    (* Eliminate duplicate  and already cached results  from "gen_ress1".  The
       rest ("got1") are installed in the Cache:
    *)
    let got1      = save_uniq_ress_gen gen_ress1 cr in

    (* So how many results have we got? *)
    if  got0 = 0 || got1 >= n_extra
    then
        (* No results at all (no more data in Search Engines), or on the cont-
           rary, got all the results required, or even more.   In both  cases,
           exit now, as there are no repeated XML-Based requests either:
        *)
        ()
    else
        (* got1 < n_extra, but got0 > 0: this means: got something,  but not
           enough, maybe also because some results have been eliminated. Try
           to get more. However, there are NO repeated XML-Based requests:
        *)
        from_search_engines'
            (n_extra - got1) 0 search_terms client_ip gses_str gen_engines
            [] cr deadline;

(* The wrapper: *)

value from_search_engines:
    int    -> int -> string -> Unix.inet_addr ->  string      ->
    cached_ress   -> float  -> unit =

fun n_extra n_xml search_terms client_ip gses_str cr deadline ->

    (* Which General-Purpose Search Engines? *)
    let gen_engines =
        if  n_extra <= 0
        then
            [] (* No results needed, hence no servers will be used *)
        else
            let gses = ref [] in
            do {
                String.iter
                (fun code ->
                    let se = Back_end.gen_server_by_code code in
                    gses.val := [se :: gses.val]
                )
                gses_str;

                gses.val
            }
    in
    (* Which XML-Based Search Engines? *)
    let xml_engines =
        if  n_xml <= 0
        then
            [] (* No results needed, hence no servers will be used *)
        else
            Back_end.all_xml_servers ()
    in
    if  gen_engines = [] && xml_engines = []
    then
        () (* Nothing really to do *)
    else
    (* Do the real requests: *)
    from_search_engines'
        n_extra n_xml search_terms client_ip gses_str gen_engines
        xml_engines cr deadline;


(*------------------*)
(*  "get_results":  *)
(*------------------*)
(*  Top-level, externally-visible function. Returns search results either from
    the Cache or by enquiring the Search Engines via the Back-End. Updates the
    Cache automatically:
*)
type search_res =
{
    gen_page : list Back_end.se_res_gen;
    gen_total: int;
    xml_page : list Back_end.se_res_xml;
    rel_terms: list string;
    s_expir  : float
};

value get_results:
    string -> int -> Unix.inet_addr -> string -> float -> search_res =

fun search_terms page_no client_ip gses_str deadline ->

    let stime = Unix.gettimeofday () in

    (* A new request has arrived -- increment the total: *)
    let () = stats.user_reqs := stats.user_reqs + 1  in

    (* Is this request a continuation of a previous one? If not, the following
       call creates a new Cache entry:
    *)
    let cr = get_cached_pages search_terms gses_str  in

    (* NB: "cr" is a Cache Handle; if updated, the Cache content will be modi-
       fied as well.
       So: How many extra General-Purpose results do we need to get?
    *)
    let before    = (page_size ()) * (page_no - 1)   in
    let n_extra   =
        max 0  (before + (page_size ()) - (List.length cr.gen_ress))
    in
    (* Do we need to fetch any XML results? They are only required on the 1st
       page, and only if they are not yet in the Cache:
    *)
    let n_xml     =
        if   page_no = 1 && List.length cr.xml_ress <> xml_size ()
        then xml_size ()
        else 0
    in
    let gen_page  =
    do {
        (* Enquire the Search Engines if we need either General or XML
           results:
        *)
        if  n_extra > 0 || n_xml > 0
        then
            (* Actually  get the new results  from the Search Engines  (both
               General-Purpose and XML-Based). The results are automatically
               installed in the Cache (via "cr") rather than just returned:
            *)
            from_search_engines
                n_extra n_xml search_terms client_ip gses_str cr deadline
        else
            (* All the required results are already in the Cache: *)
            stats.cached_reqs := stats.cached_reqs + 1;

        (* Select the results we need from the Cache: *)
        select_page_gen cr page_no
    }
    in
    (* Update the timing parms: *)
    let etime = Unix.gettimeofday () in
    let ()    = stats.cum_resp_time :=
                stats.cum_resp_time +. (etime -. stime)
    in
    let ()    =
        if   trace ()
        then Printf.fprintf stderr
             "\n*** Page %d: needed %d more results, put %d on page ***\n\n%!"
             page_no n_extra (List.length gen_page)
        else ()
    in
    (* The result: *)
    {
        gen_page  = gen_page;
        gen_total = List.length cr.gen_ress;
        xml_page  = cr.xml_ress;
        rel_terms = cr.related;
        s_expir   = cr.expir
    };

(*----------------*)
(*  "get_stats":  *)
(*----------------*)
(*  Returns a deep copy of the "stats" object (as the latter is mutable: *)

value get_stats: unit -> mc_stats =
fun () ->
{
    stats_begin     = stats.stats_begin;
    user_reqs       = stats.user_reqs;
    cum_resp_time   = stats.cum_resp_time;
    cached_reqs     = stats.cached_reqs;
    salvos          = stats.salvos;
    distr_step      = stats.distr_step;
    se_stats        = Hashtbl.copy stats.se_stats
};

(*------------------*)
(*  "reset_stats":  *)
(*------------------*)
value reset_stats: unit -> unit =
fun () ->
do {
    stats.stats_begin   := Unix.gettimeofday ();
    stats.user_reqs     := 0;
    stats.cum_resp_time := 0.0;
    stats.cached_reqs   := 0;
    stats.salvos        := 0;
    (* DON'T TOUCH "distr_step" -- it's a constant! *)
    Hashtbl.clear stats.se_stats
};

(*------------------------------*)
(*  Middle-Tier Initialisation: *)
(*------------------------------*)
value init:
    ~purger_prio:int                ->  (* Purger Thread Priority *)
    (string -> unit)                ->  (* Log  Function *)
    unit    =
fun ~purger_prio log_func ->
let c = Lukol_config.get_global_conf () in
try
do {
    (* Set the global logger: *)
    log_func_ref.val  := log_func;

    (* Reset the statistics, otherwise the starting time would not be
       correctly set if the server re-starts:
    *)
    reset_stats ();

    (* The number of steps in the response time distribution function: *)
    n_steps_ref.val   :=
        int_of_float (ceil (
            c.Lukol_config.fe_config.Lukol_config.search_timeout /.
            c.Lukol_config.be_config.Lukol_config.resp_distr_step
        )) + 1;

    stats.distr_step  :=
              c.Lukol_config.be_config.Lukol_config.resp_distr_step;

    (* Create the Purger Thread: *)
    ignore
        (Tnio.thread_create  purge_expired
        ~stack_size_k:128 ~priority:purger_prio);

    log "INFO: Middle-Tier Cache successfully created"
}
with
[hmm ->
    failwith ("Middle_cache.init: ERROR: "^(Misc_utils.print_exn hmm))
];

