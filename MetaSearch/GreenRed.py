#----------------------------------------------------------------------------#
#	     "GreenRed.py": RedHat-Like Color Text Messages (OK / FAILED)		 #
#----------------------------------------------------------------------------#
import sys

def echo_success():
	print "\x1B[60G[  \x1B[1;32mOK\x1B[0;39m  ]"
	sys.stdout.flush ()

def echo_failure():
	print "\x1B[60G[\x1B[1;31mFAILED\x1B[0;39m]"
	sys.stdout.flush ()

def echo_warning():
	print "\x1B[60G[\x1B[1;33mWARNING\x1B[0;39m]"
	sys.stdout.flush ()

