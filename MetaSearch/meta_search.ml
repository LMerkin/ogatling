(*pp camlp4r *)
(* vim:ts=4:syntax=ocaml
*)
(*===========================================================================*)
(*                              "meta_search.ml":                            *)
(*       Top-Level Integration of the Gatling/Lukol MetaSearch Engine        *)
(*                  (C) Explar Technologies Ltd, 2004--2006                  *)
(*===========================================================================*)
(*====================*)
(*  Global Settings:  *)
(*====================*)
value version = "Gatling MetaSearch Engine version 1.5.0, 2006-08-17\n"^
                "\t(C) Explar Technologies Ltd, 2004--2006";

value no_restart_rc = 255;

(*-------------*)
(* Priorities: *)
(*-------------*)
value be_worker_prio    = 0;
value purger_prio       = 1;
value fe_worker_prio    = 2;
value fe_acceptor_prio  = 3;
value ssl_worker_prio   = 4;
value ssl_acceptor_prio = 5;
value n_bands           = 6;

(*----------------------------*)
(*  Initialise the Run-Time:  *)
(*----------------------------*)
Tnio.init (Tnio.Priority_Bands n_bands);

(*--------------------------*)
(*  Get the Global Config:  *)
(*--------------------------*)
(* Get the command-line parm: *)
if  (Array.length Sys.argv <> 2) ||
    (List.mem Sys.argv.(1) ["-h"; "-help"; "--help"])
then
do {
    Printf.fprintf  stderr "%s\n%!" version;
    Printf.fprintf  stderr "PARAMETER: XML_Config_File\n%!";
    Tnio.exit 2
}
else ();

(* Parse the XML Config File: *)
Lukol_config.init_config Sys.argv.(1);

(*----------------------*)
(*  Check the Licence:  *)
(*----------------------*)
(*  Temporarily configured out, due to discovered non-thread-safety
    of the OpenSSL binding:

Enforcement.verify_licence
    (Lukol_config.get_global_conf()).Lukol_config.licence_file 
    (fun () -> Secure_time.gmtime 15.0);
*)

(*-------------------------*)
(*  SE Interfaces Parsing: *)
(*-------------------------*)
(*  Interfaces to General-Purpose Search Engines: *)

value interfs_file_gen =
   (Lukol_config.get_global_conf()).
    Lukol_config.be_config.Lukol_config.gse_interfs_file;

value mk_interfs_gen: unit -> list Lukol_config.se_interface_gen =
fun () ->
try
    let cnnl  = open_in interfs_file_gen    in
    try
        let strm  = Stream.of_channel cnnl  in
        let xstr  = IoXML.parse_xml   strm  in
        let res   = Lukol_config.xparse_se_interfaces_gen xstr in
        do {
            close_in cnnl;
            res
        }
    with
    [ hmm ->
      do {
        close_in cnnl;
        raise hmm
    }]
with
[hmm ->
    failwith
    (Printf.sprintf "ERROR: Invalid Gen SE Interface File: \"%s\": %s%!"
                    interfs_file_gen (Misc_utils.print_exn hmm)
    )
];

(*  Interfaces to XML-Based Search Engines: *)

value interfs_file_xml =
   (Lukol_config.get_global_conf()).
    Lukol_config.be_config.Lukol_config.xse_interfs_file;

value mk_interfs_xml: unit -> list Lukol_config.se_interface_xml =
fun () ->
try
    let cnnl  = open_in interfs_file_xml    in
    try
        let strm  = Stream.of_channel cnnl  in
        let xstr  = IoXML.parse_xml   strm  in
        let res   = Lukol_config.xparse_se_interfaces_xml xstr in
        do {
            close_in cnnl;
            res
        }
    with
    [ hmm ->
      do {
        close_in cnnl;
        raise hmm
    }]
with
[hmm ->
    failwith
    (Printf.sprintf "ERROR: Invalid XML SE Interface File: \"%s\": %s%!"
                    interfs_file_xml (Misc_utils.print_exn hmm)
    )
];

(*----------------------*)
(* "log" and "trace":   *)
(*----------------------*)
(* Open the Log, and lock it to prevent over-writing by another instance: *)

value log_file = (Lukol_config.get_global_conf()).Lukol_config.log_file;
value logger   =
try
    let fd =
        Option.get
        (Misc_utils.open_write_locked
            ~append_mode :True
            ~lock_fail_rc:no_restart_rc
            log_file
        )
    in
    let ch = Unix.out_channel_of_descr fd in
    do {
        output_string ch "\n";
        ch
    }
with
[hmm ->
    Misc_utils.fatal_error ~rc:no_restart_rc
    (Printf.sprintf
        "ERROR: Cannot open the Log File \"%s\": %s\n%!"
        log_file (Misc_utils.print_exn hmm)
    )
];

(* The logging function, to be used by all components. Messages resulting
   just from a client disconnecting, are not recorded:
*)
value log: string -> unit =
fun msg ->
    try ignore (ExtString.String.find msg " bytes not sent")
        (* If found, nothing to do! *)
    with[_ ->
        (* Not found -- do real logging! *)
        let timestamp = Date_time.http_time_stamp (Unix.gettimeofday ()) in
        let log_line  = Printf.sprintf "[%s] %s\n" timestamp msg         in
        do {
            output_string logger log_line;
            flush logger
        }
    ];

(*  "trace":
    XXX: should it write data on STDERR or in the same LOG file?
*)
value trace =
    (Lukol_config.get_global_conf()).Lukol_config.trace_mode
    && (not
    (Lukol_config.get_global_conf()).Lukol_config.daemon_mode);

(*  External DNS servers (if any) to be used: *)
value dns_servers =
    (Lukol_config.get_global_conf()).Lukol_config.dns_servers;


(*==============================*)
(*  Initialise all components:  *)
(*==============================*)
(*  "init_server" returns a "clean-up" function to be registered with the
    Monitor (if used):
*)
value init_server: unit -> (unit -> unit) =
fun _ ->
try
do {
    (* Initialise all Interafces:    *)
    let interfs_gen = mk_interfs_gen () in
    let interfs_xml = mk_interfs_xml () in
    do {
        (*  Initialise the Back-End: *)
        Back_end.init
            interfs_gen
            interfs_xml
            ~be_worker_prio: be_worker_prio
            log;
    };
    (*  Initialise the Middle-Tier Cache: *)
    Middle_cache.init
        ~purger_prio     : purger_prio
        log;

    (*  Initialise the Front-End:         *)
    let (http_server, https_server) =
    Front_end.init
        ~fe_acceptor_prio : fe_acceptor_prio
        ~fe_worker_prio   : fe_worker_prio
        ~ssl_acceptor_prio: ssl_acceptor_prio
        ~ssl_worker_prio  : ssl_worker_prio
        log
    in
    (* The clean-up function: currently, clean-up is required only for the
       front-end, as it binds to "well-known" sockets:
    *)
    fun _ ->
    do {
        Tcp_server.shutdown_server http_server  Gen_server.Discard_Queue;
        log "Front-end HTTP  server shut down";

        Tcp_server.shutdown_server https_server Gen_server.Discard_Queue;
        log "Front-end HTTPS server shut down"
    }
}
with
[hmm ->
do {
    (* XXX: currently, any init error is fatal: *)
    log ("Meta_serach.init_server: FATAL_ERROR: "^(Misc_utils.print_exn hmm));
    Tnio.exit no_restart_rc
}];

(*========================================*)
(*  Self-Testing: The Monitor Interface:  *)
(*========================================*)
(*  Performs ACTIVE liveness test of the MetaSearch Engine. If the latter is
    found alive, returns the current time. Otherwise, returns a time earlier
    than the max blocking interval, to force Engine re-starting by the Moni-
    tor (if used);
    "test_http_agent" uses pre-configured constant-value resolver to always
    return the IP the MetaSearch Engine is listening on:
*)
value test_http_agent_conf =
{
    Http_client.conf_client_ips = [];
    Http_client.conf_use_persist= False;
    Http_client.conf_buff_size  = Clients_simple.http_buff_size;
    Http_client.conf_user_agents= []
};

value server_dom  =
   (Lukol_config.get_global_conf()).
    Lukol_config.fe_config.Lukol_config.our_domain;

value server_port =
   (Lukol_config.get_global_conf()).
    Lukol_config.fe_config.Lukol_config.listen_on_port;

value test_http_agent =
    Clients_simple.mk_http_agent
   (Clients_simple.mk_dns_agent dns_servers);

(* Must ensure that the Search Term used for self-testing is non-"": *)
value test_term    =
    Netencoding.Url.encode
    ~plus:True
   (Lukol_config.get_global_conf()).
        Lukol_config.st_config.Lukol_config.test_search_term;

if  test_term = ""
then Misc_utils.fatal_error ~rc:no_restart_rc
     "Meta_search: Empty self-test search term"
else ();

(* And: must ensure the temporal self-test integrity, to prevent false
   negative results.   In particular, the period must be longer than 4
   minutes which is the maximum time required by the server to bind to
   the local IP/port:
*)
value test_timeout =
   (Lukol_config.get_global_conf()).
    Lukol_config.fe_config.Lukol_config.search_timeout;

value test_period  =
   (Lukol_config.get_global_conf()).
    Lukol_config.st_config.Lukol_config.test_period;

if  test_period < test_timeout || test_period <= Tnio.bind_wait
then
    Misc_utils.fatal_error ~rc:no_restart_rc
        ("Meta_search: Invalid self-test period, "^
         "must be longer than Search TimeOut, and longer than BindWait")
else
    ();

(* Form the testing request: *)
value test_url = 
    Printf.sprintf "http://%s:%d/search?st=%s"
    server_dom server_port test_term;

(* TODO:
   Perform separate self-requests for each real SearchEngine configured, to
   verify that they are all available.    For now, only one request (to all
   configured Search Engines) is performed:
*)
value test_fn =
   (Lukol_config.get_global_conf()).
    Lukol_config.st_config.Lukol_config.test_out_file;

value last_activity: unit -> float =
fun () ->
    (* Perform a self-request: *)
    let now      = Unix.gettimeofday ()      in
    let deadline = now +. test_timeout       in
    let ancient  = now -. 1.0 -. test_period in
    try
        let res = Clients_simple.http_get test_http_agent test_url deadline in
        do {
            (* Save the result in a file, if specified, but don't propagate an
               error if it fails:
            *)
            if  test_fn <> ""
            then
                try
                    let ch = open_out test_fn in
                    do {
                        output_string ch res.Http_client.body;
                        close_out ch
                    }
                with [_ -> ()]
            else ();
        
            (* Return the "last alive" time stamp:  *)
            if   res.Http_client.body <> ""
            then deadline (* Even later than "now"! *)
            else ancient
        }
    with
    [hmm ->
     do {
        log ("MONITOR: SELF-TEST FAILED: "^(Misc_utils.print_exn hmm));
        ancient
    }];

(*===============*)
(*  Off we go!.. *)
(*===============*)
(* "clean_up":
   Shut-down of the server wjen a SIGHUP is received: Pending queries are
   discarded:
*)
value minfo =
{
    Monitor.last_activity     = last_activity;
    Monitor.infinite_proc     = True;
    Monitor.max_blocking_time = test_period;
    Monitor.log_heartbeats    = False
};

value daemon_mode = (Lukol_config.get_global_conf()).Lukol_config.daemon_mode;
value pid_file    = (Lukol_config.get_global_conf()).Lukol_config.pid_file;
value user        = (Lukol_config.get_global_conf()).Lukol_config.run_as_user;
value group       = (Lukol_config.get_global_conf()).Lukol_config.run_as_group;

(* NB: here "daemon_mode" and "with_monitor" are equivalent: *)
value rinfo =
{
    Monitor.init_phase        = init_server;
    Monitor.oper_phase        = Tnio.threads_scheduler; (* Just that! *)
    Monitor.daemon_mode       = daemon_mode;
    Monitor.with_monitor      = if daemon_mode then Some minfo else None;
    Monitor.severe_error      = no_restart_rc;
    Monitor.exn_error         = 1;
    Monitor.pid_file          = pid_file;
    Monitor.run_as_user       = user;
    Monitor.run_as_group      = group;
    Monitor.log               = log
};

Monitor.run_process rinfo;

