(*pp camlp4r *)
(* vim:ts=4:syntax=ocaml
*)
(*===========================================================================*)
(*                              "http_server.ml":                            *)
(*           HTTP(S) server implementation on top of "Tcp_server"            *)
(*                     (C) Explar Technologies Ltd, 2004                     *)
(*===========================================================================*)
(*===============*)
(*  Data Types:  *)
(*===============*)
(*---------------*)
(*  "http_req":  *)
(*---------------*)
type http_req =
{
    client_ip   :   Unix.inet_addr;
    client_port :   int;

    http_version:   string;
    http_method :   string;
    path        :   string;
    parameters  :   string;
    fragment    :   string;

    query_parms :   Hashtbl.t string (list string);
    headers     :   Hashtbl.t string (list string);
    uploaded_files: Hashtbl.t string (string * string);

    (* Derived from the headers and client status: *)
    keep_alive  :   bool
};
                
(*--------------------*)
(*  "server_config":  *)
(*--------------------*)
(*  Similar to the "Tcp_server.server_config", except the "proc_func": *)

type server_config  'socket 'state =
{
    (* TCP Parameters: *)
    listen_on_ip        : Unix.inet_addr;
    listen_on_port      : int;
    listen_queue_len    : int;
    max_connections     : int;
    sock_buff_size      : int;

    (* Acceptor Parameters: *)
    acceptor_priority   : int;
    acceptor_stack_k    : int;

    (* Worker Pool Parameters: *)
    max_idle_threads    : int;
    min_idle_threads    : int;
    init_threads        : int;
    worker_priority     : int;
    worker_stack_k      : int;

    (* State Management: *)
    init_socket         : Tnio.tsock       -> 'state -> float  -> 'socket;
    finalise_socket     : 'socket -> float -> unit;
    init_worker_state   : unit    -> 'state;
    cleanup_state       : 'state  -> unit;
    reset_state_on_error: bool;
    log_error           : exn     -> unit;
    reject_conn         : Tnio.tsock -> unit;

    (* I/O functions: *)
    read_func           : Misc_utils.io_func 'socket;
    write_func          : Misc_utils.io_func 'socket;

    (* User-Level Processing -- these functions send output to clients, so
       they would typically need the same "write_func"  as in this config,
       supplied via the closure:
    *)
    proc_funcs          : Hashtbl.t  string
                          ('socket -> http_req -> 'state -> float -> 'state);

    session_timeout     : float;
    max_pers_reqs       : int;
    proc_timeout        : http_req -> float;
    http_error          : 'socket  -> int -> string -> unit;
    uploaded_dir        : string;

    (* User and Group for running this server: *)
    run_as_user         : string;
    run_as_group        : string
};

(*====================*)
(*  Reqs Processing:  *)
(*====================*)
(*----------------------*)
(*  "split_inet_addr":  *)
(*----------------------*)
value split_sock_addr: Unix.sockaddr -> (Unix.inet_addr * int) =
fun
[ Unix.ADDR_INET ip port -> (ip, port)
| _ -> failwith "Http_server.split_sock_addr" (* Impossible *)
];

(*---------------------*)
(*  "parse_req_line":  *)
(*---------------------*)
(* Returns (Method, URL, HTTP_Version): *)

value reqline_re = Pcre.regexp ~study:True
                   "^([^\\s]+)\\s+([^\\s]+)\\s+HTTP/([^\\s]+)$";

value parse_req_line: string -> (string * string * string) =
fun rline ->
try
    let parts   = Pcre.extract ~rex:reqline_re ~full_match:False rline in
    let methd   = parts.(0) in
    let urlpath = parts.(1) in
    let versn   = parts.(2) in

    if (methd <> "GET" && methd <> "PUT")
    then
        raise (Http_common.HTTP_Error 501 ("invalid HTTP method: " ^methd))
    else
    if (versn <> "1.0" && versn <> "1.1")
    then
        raise (Http_common.HTTP_Error 505 ("invalid HTTP version: "^versn))
    else
        (methd, urlpath, versn)
with
[Not_found ->
    (* Malformed request line: *)
    raise (Http_common.HTTP_Error 400 ("invalid HTTP request: "^rline))
];

(*-----------------------------*)
(*  "get_multipart_boundary":  *)
(*-----------------------------*)
(* Returns "" if it is NOT a multi-part message: *)

value multipart_re = Pcre.regexp ~study:True ~flags:[`CASELESS]
                     "^multipart/[a-z]+, boundary=(.+)$";

value get_multipart_boundary:
    Hashtbl.t string (list string) -> string =
fun hdrs_dict ->
try
    (* Get the "content-type": *)
    let cont_type =
        try match Hashtbl.find hdrs_dict "content-type" with
            [ [ct] -> ct
            | _    -> raise (Http_common.HTTP_Error 400 "Content-Type Error")
            ]         (* This should not happen *)
        with
        [Not_found -> (* Default: *)
            "application/x-www-form-urlencoded"
        ]
    in
    let parts =
         Pcre.extract ~rex:multipart_re ~full_match:False cont_type
    in
    (* If match is successful: *)
    let bound1= parts.(0) in

    (* Remove the outer quotes if exist: *)
    let len   = String.length bound1 in
    let bound2=
        if   bound1.[0] = '"' && bound1.[len-1] = '"'
        then String.sub bound1 1 (len-2)
        else bound1
    in
    bound2
with
[Not_found -> ""];

(*--------------------------*)
(*  "read_multipart_post":  *)
(*--------------------------*)
(*  This includes file uploading: *)

value read_multipart_post:
    server_config          'socket 'state ->
    Misc_utils.line_reader 'socket        ->
    'socket->
    Hashtbl.t string (list string)        ->
    string ->
    Hashtbl.t string (string * string)    ->
    float  -> unit =

fun conf reader socket query_dict boundary up_files deadline ->
    (* XXX: Not yet implemented: *)
    raise (Http_common.HTTP_Error 501 "Multi-Part POST not implemented yet");

(*------------------------*)
(*  "get_keepalive_hdr":  *)
(*------------------------*)
value get_keepalive_hdr: string -> Hashtbl.t string (list string) -> bool =
fun version headers ->
    try
        (* If there are explicit requirements, use them. If more than one
           "connection" header was given, use the last one:
        *)
        let hs = Hashtbl.find headers "connection"
        in
        let hd = String.lowercase (List.hd (List.rev hs)) in
        hd = "keep-alive"
    with
    [Not_found ->
        (* Otherwise, use the protocol default: *)
        version = "1.1"
    ];

(*--------------------*)
(*  "read_http_req":  *)
(*--------------------*)
(* Reads in and returns a single HTTP request, with a flag indicating
   (iff set) that the client connection is still valid:
*)
value read_http_req:
    server_config  'socket 'state  -> 'socket ->
    Misc_utils.line_reader 'socket -> Unix.sockaddr -> int -> float ->
    http_req =

fun conf socket reader client_addr req_count deadline ->

    (* First of all, the client's TCP/IP parms: *)
    let (client_ip, client_port) = split_sock_addr client_addr   in

    (* Now, read and parse the request line: *)
    let req_line   = reader#recv_line socket deadline in

    let (methd,  urlpath, version)     = parse_req_line req_line in

    let (path, parms, query, fragment) =
        try  Http_common.parse_urlpath urlpath
        with [_ ->
             raise (Http_common.HTTP_Error 400 ("invalid URL Path: "^urlpath))
        ]
    in  
    (* Initialise the query dictionary: *)
    let query_dict = Hashtbl.create  100 in

    (* Further parse the "query" into the "name, value" fields, decode them,
       and put them into the "query_dict":
    *)
    let () =
        try  Http_common.parse_query query query_dict
        with [_ ->
            raise (Http_common.HTTP_Error 400 ("invalid HTTP Query: "^query))
        ]
    in
    (* Read in the headers: *)
    let hdrs_dict  = Http_common.read_headers reader socket deadline in

    (* Now: only if the method is "POST", get extra data  --  but we need a
       (possibly empty) "up_files" dict in any case:
    *)
    let up_files   = Hashtbl.create 100 in

    (* At the end of reading the data, we may or may not still have a valid
       connection with the client.
    *)
    let still_alive=
        if  methd  = "POST"
        then
            (* Is it a file uploading? *)
            let boundary =  get_multipart_boundary hdrs_dict in
            if  boundary <> ""
            then
            do {
                (* Yes, it's a multi-part message, possibly file uploading: *)
                read_multipart_post
                    conf reader socket query_dict boundary up_files deadline;
                Http_common.Still_On
            }
            else
                (* No,  it's a normal "POST" -- read the data until the given
                   size is received, or until the client closes the connectn:
                *)
                let buff =  String.create 32768 in
                let (posted, after_state) =
                    Http_common.read_http_data
                        Http_common.Server_Side
                        conf.read_func reader socket buff hdrs_dict deadline
                in
                do {
                    (* Add query parms to the "query_dict": *)
                    try  Http_common.parse_query posted query_dict
                    with [_ ->
                        raise (Http_common.HTTP_Error 400
                              ("invalid POST:"^posted))
                    ];
                    after_state
                }
        else
            Http_common.Still_On
    in
    (* So: is the curr connection to be kept? *)
    let keep_alive =
        (get_keepalive_hdr version hdrs_dict) &&
        (still_alive = Http_common.Still_On ) &&
        (req_count < conf.max_pers_reqs  - 1)
    in
    (* The resulting request object: *)
    {
        client_ip       = client_ip;
        client_port     = client_port;
        http_version    = version;
        http_method     = methd;
        path            = path;
        parameters      = parms;
        fragment        = fragment;
        query_parms     = query_dict;
        headers         = hdrs_dict;
        uploaded_files  = up_files;
        keep_alive      = keep_alive
    };

(*---------------------*)
(*  "start_response":  *)
(*---------------------*)
value start_response:
    Misc_utils.io_func 'socket -> 'socket -> http_req -> float -> unit =

fun write_func socket req deadline ->
do {
    (* Send the first resp line: *)
    Misc_utils.send_str write_func socket
        (Printf.sprintf "HTTP/%s 200 OK\r\n" req.http_version)  deadline;

    (* Send the "Date:" header:  *)
    Misc_utils.send_str write_func socket
        (Printf.sprintf "Date: %s\r\n"
        (Date_time.http_time_stamp (Unix.gettimeofday ())))     deadline;

    (* Send the connection status: *)
    if  req.keep_alive
    then
        Misc_utils.send_str
        write_func socket "Connection: keep-alive\r\n" deadline
    else
        Misc_utils.send_str
        write_func socket "Connection: close\r\n"      deadline
};

(*-------------------------*)
(*  "simple_err_handler":  *)
(*-------------------------*)
value simple_err_handler:
    Misc_utils.io_func 'socket -> 'socket -> int -> unit =

fun write_func socket err_code ->
    let msg1 = try Hashtbl.find (Http_common.http_err_msgs ()) err_code with
               [Not_found -> "Internal Server Error"]
    in
    let msg2 = Printf.sprintf "HTTP/1.1 %03d %s\r\n\r\n" err_code msg1 in

    (* Allow some 10 seconds to send it... *)
    Misc_utils.send_str write_func socket msg2 (Unix.gettimeofday() +. 10.0);

    (* The user will then do the rest...   *)


(*-----------------------*)
(*  "handle_http_reqs":  *)
(*-----------------------*)
(*  The processing function for the underlying "Tcp_server". Note that this
    function can handle persistent connections, that is,   service multiple
    HTTP requests over a single TCP connection:
*)
value rec handle_http_reqs':
    server_config          'socket 'state  -> 
    Misc_utils.line_reader 'socket         ->
    'socket -> Unix.sockaddr -> 'state -> int -> float -> 'state =

fun conf reader socket client_addr state req_count deadline ->
    (* NB:
       The "deadline" here applies to the whole session, that is, to a sequ-
       ence of requests serviced over a persistent connection. We do not en-
       force this deadline explicitly  at the beginning of each iteartyion;
       rather, we use it when a new request is being read:
    *)
    if   req_count >= conf.max_pers_reqs
    then
        (* No more requests over this connection -- although this should not
           happen, as the connection would be closed earlier by the server:
        *)
        state
    else
    (* Read in the request completely: *)
    let req = read_http_req conf socket reader client_addr req_count deadline
    in
    (* Determine the handler function:
       XXX: currently, this is done on the basis of the URL Path alone.
       With HTTP/1.1, it would be better to use the "host" hdr as well:
    *)
    let handler =
        try  Hashtbl.find conf.proc_funcs req.path
        with
        [Not_found ->
            (* Specific handler for this "req.path" has not been found --
               try a "catch-all" one (""). If that one is not found as
               well, all bets are off:
            *)
            try  Hashtbl.find conf.proc_funcs ""
            with [Not_found ->
                raise (Http_common.HTTP_Error 404
                      ("no handler for this req: "^req.path))
            ]
        ]
    in
    (* Before applying the actual handler, produce the fisrt line and
       common headers of the output  --  the rest will be done by the
       handler itself:
    *)
    let () = start_response conf.write_func socket req  deadline in

    (* Now apply the handler.  This application has its own individual
       deadline, based on "proc_timeout", but it should also be within
       the over-all session deadline:
    *)
    let proc_deadline =
        min (Unix.gettimeofday() +. (conf.proc_timeout req)) deadline
    in
    let state' = handler socket req state proc_deadline in

    (* Service further requests over the same connection?  If the HTTP
       version is "1.1"  OR "keep-alive" was explicitly requested, and
       nothing bad happened so far, then yes:
    *)
    if  req.keep_alive
    then
        (* New iteration, with updated "state'": *)
        handle_http_reqs'
            conf reader socket client_addr state' (req_count+1) deadline
    else
        (* Return the "state'" achieved: *)
        state';


(* The wrapper: *)

value handle_http_reqs:
    server_config 'socket 'state  ->
    'socket    ->  Unix.sockaddr  -> 'state -> float -> 'state =

fun conf socket client_addr  state deadline ->
try
    (* Create the reader: *)
    let reader = new Misc_utils.line_reader conf.read_func 4096 in

    (* Invoke the recursive master handler: *)
    handle_http_reqs'
        conf reader socket client_addr state 0 deadline
with
    [ Http_common.HTTP_Error n msg as hmm ->
    do {
        (* Invoke the default handler, then the user-supplied handler, then
           **still** propagate the exception to close the current session:
        *)
        conf.http_error socket n msg;
        raise hmm
    }];

(*--------------------*)
(*  "create_server":  *)
(*--------------------*)
value create_server  : server_config 'socket 'state ->
            Tcp_server.server_t      'socket 'state =
fun conf ->
    (* Create configuration for the underlying TCP server: *)
    let tcp_conf =
    {
        Tcp_server.listen_on_ip        = conf.listen_on_ip;
        Tcp_server.listen_on_port      = conf.listen_on_port;
        Tcp_server.listen_queue_len    = conf.listen_queue_len;
        Tcp_server.max_connections     = conf.max_connections;
        Tcp_server.sock_buff_size      = conf.sock_buff_size;

        Tcp_server.acceptor_priority   = conf.acceptor_priority;
        Tcp_server.acceptor_stack_k    = conf.acceptor_stack_k;

        Tcp_server.max_idle_threads    = conf.max_idle_threads;
        Tcp_server.min_idle_threads    = conf.min_idle_threads;
        Tcp_server.init_threads        = conf.init_threads;
        Tcp_server.worker_priority     = conf.worker_priority;
        Tcp_server.worker_stack_k      = conf.worker_stack_k;

        Tcp_server.init_socket         = conf.init_socket;
        Tcp_server.finalise_socket     = conf.finalise_socket;
        Tcp_server.init_worker_state   = conf.init_worker_state;
        Tcp_server.cleanup_state       = conf.cleanup_state;
        Tcp_server.reset_state_on_error= conf.reset_state_on_error;

        Tcp_server.after_acceptor_bind = (* Lower the privileges, if any: *)
            fun () ->
                Misc_utils.run_as conf.run_as_user conf.run_as_group;

        Tcp_server.log_error           = conf.log_error;
        Tcp_server.reject_conn         = conf.reject_conn;

        (* Attach the HTTP master handler here. Note that the "session_timeout"
           applies to the whole session, that is, to the master handler:
        *)
        Tcp_server.proc_func           = handle_http_reqs conf;
        Tcp_server.proc_timeout        = conf.session_timeout
    }
    in
    Tcp_server.create_server tcp_conf;


(*-------------------*)
(*  "display_page":  *)
(*-------------------*)
(*  If used in "normal" circumstances, the "full_response" arg must be False,
    as the initial part of the response has already been sent  by the Server
    itself. If used as a stand-alone error handler, "full_response" should be
    True:
*)
value display_page:
    string -> ?full_response:bool -> ?content_type:string  ->
    Misc_utils.io_func 'socket    -> 'socket ->     float  -> unit =

fun page ?(full_response=False) ?(content_type="text/html")
    write_func  socket  deadline  ->
do {
    (* The content to be sent: *)
    let resp0 =
    [
        Printf.sprintf "Content-type: %s\r\n" content_type;
        "Content-length: "^(string_of_int (String.length page))^"\r\n";
        "\r\n";
        page
    ]
    (* Do we need the initial lines of the response? If yes, it's probably for
       error handling, so keep them simple:
    *)
    in
    let resp1 =
        if  full_response
        then
            ["HTTP/1.0 200 OK\r\n" :: resp0]
        else
            resp0
    in
    (* Send the response out: *)
    List.iter
        (fun str -> Misc_utils.send_str write_func socket str deadline)
        resp1
};

