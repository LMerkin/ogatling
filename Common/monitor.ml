(*pp camlp4r *)
(* vim:ts=4:syntax=ocaml
*)
(*===========================================================================*)
(*                               "monitor.ml":                               *)
(*         Starts a process and guards it against crash or dead-lock         *)
(*                  (C) Explar Technologies Ltd, 2003-2005                   *)
(*===========================================================================*)
(* Flag for graceful exit on SIGHUP: *)
value graceful_exit  = ref False;
value any_exit       = ref False;
value block_debug    = ref False;
value monitor_pid    = ref (-1);
value worker_pid     = ref (-1);
value worker_clean_up= ref (fun () -> ());

(*-------------------*)
(*  "monitor_info":  *)
(*      "run_info":  *)
(*-------------------*)
(*  Documented in the module interface: *)

type monitor_info =
{
    last_activity    : unit -> float;
    infinite_proc    : bool;
    max_blocking_time: float;
    log_heartbeats   : bool
};

type run_info  =
{
    init_phase       : unit -> (unit -> unit);
    oper_phase       : unit -> unit;
    daemon_mode      : bool;
    with_monitor     : option monitor_info;
    severe_error     : int;
    exn_error        : int;
    pid_file         : string;
    run_as_user      : string;
    run_as_group     : string;
    log              : string -> unit
};

(*-----------------------*)
(*  "become_pg_leader":  *)
(*-----------------------*)
(*  Make the current process the leader in its process group. In particular,
    disconnect it from the controlling terminal:
*)
value become_pg_leader: unit -> unit =
fun () ->
do {
    Misc_utils.safe_close Unix.stdin;
    Misc_utils.safe_close Unix.stdout;
    Misc_utils.safe_close Unix.stderr;

    ignore (Unix.setsid ())
};

(*--------------*)
(*  "kill_pg":  *)
(*--------------*)
(*  Send a KILL signal to the whole process group of a given process  (any
    errors, e.g. if the process does not exist, are ignored):
*)
value kill_pg: int -> unit =
fun pid ->
do {
    (* In "wait*" below, we would need to know whether there was indeed at
       least one child which was killed, and needs to be waited for.  How-
       ever, we cannot used the exception in "Unix.kill()" for that purpo-
       se, for the following reasons:
       (1) the process can already be a zomby: "kill" will fail, but "wait"
           IS needed;
       (2) other way round is also possible: we kill the whole group, not
           only the direct children, but only the latter (if any) need to
           be waited for.
       Thus, any exception in "kill" is ignored -- no useful info from it.
       IMPORTANT:
       We need a proper clean-up for the process before it exits, so issue
       SIGHUP, not SIGKILL:
    *)
    try Unix.kill (-pid) Sys.sighup with [_ -> ()];

    (* Wait for all children (if any) to terminate, in order not to create
       zombies, or there could be problems on re-start  (e.g. with binding
       to server sockets):
    *)
    let rec collect_children: unit -> unit =
    fun () ->
        (* NB: It's OK NOT to use the WNOHANG flag here: if there were any
           children of our process  within  the given process group,  they
           have been killed,  so we would not block indefinitely here.  On
           the contrary, using the WNOHANG flag may create race condition:
           a child process has been killed, but not yet terminated...
        *)
        let rc = try fst (Unix.waitpid [] (-pid)) with [_ -> -1] in
        if  rc < 0
        then
            () (* No (more) unwaited-for children *)
        else
            (* More children within the group?    *)
            collect_children ()
    in
    (* Apply it: *)
    collect_children ()
};

(*-------------------*)
(*  "monitor_loop":  *)
(*-------------------*)
(*  Monitor the process's status periodically.
    Arg1: Monitor's config;
    Arg2: PID of the monitored process;
    Arg3: next time of status checking:
*)

type  proc_status   =
    [ Proc_Completed
    | Proc_Crashed of int
    | Proc_Blocked];


value rec monitor_loop:
    run_info -> monitor_info -> int -> float -> proc_status =

fun rinfo minfo pid next_check ->
do {
    (* NB: To prevent a race condition between the Monitor and the proc  which
           is just starting  (so it could not yet produce any "last_activity"),
           the Monitor must sleep BEFORE each iteration, not after.    However,
           the SIGCHLD signal must be enabled to interrupt this sleep,  other-
           wise we may not notice that the child has already terminated.
           On the other hand, if sleep is  interruped by anything  BUT SIGCHLD,
           we will go back to sleep (in the next iteration):
           The sleeping time is rounded UP, to guarantee that sleeping will end
           PAST the "next_check" time, unless an interrupt occurs:
    *)
    if  not any_exit.val
    then
        let  sleep_sec = int_of_float
                         (ceil (next_check -. Unix.gettimeofday ())) in
        Unix.sleep sleep_sec
    else
        (* The child has already terminated,-- there will be no more signals.
           We don't really need to wait, but still do it, to prevent a busy
           loop in this function -- just in case:
        *)
        Unix.sleep 1;

    (* Check (in a non-blocking way) whether  any child  has terminated or got
       blocked. It can in fact be a child from the previous incarnation of the
       monitored process, killed by us:
    *)
    let (term_pid,  term_stat) =
         Unix.waitpid [Unix.WNOHANG; Unix.WUNTRACED] (-1) in

    if   term_pid = pid
    then
        match term_stat with
        [   Unix.WEXITED 0   ->
                (* "Successful" (?) completion *)
                Proc_Completed

          | Unix.WEXITED rc  ->
                (* "Unsuccessful" completion: Consider it to be a crash? *)
                Proc_Crashed rc

          | Unix.WSIGNALED rc ->
                (* Killed by a signal: *)
                Proc_Crashed (-rc)   (* Return negated real POSIX SigNo! *)

          | Unix.WSTOPPED  _ ->
                Proc_Blocked      (* Presumably... *)
        ]
    else
        (* It's a previous instance of the child process (???) which has just
           terminated, or (term_pid=0) there was no termination at all. Thus,
           the current process has NOT terminated; check the activity time on
           it. BUT:
           First of all, check whether we have received a "block_debug" cond;
           in this case, return "Proc_Blocked" even if  it's not time yet to
           check the child's status:
        *)
        if  block_debug.val
        then
            Proc_Blocked
        else
        let now = Unix.gettimeofday () in
        if  now < next_check
        then
            (* Monitor sleeping was intrerrupted by e.g. an I/O signal. It's
               too early to check the status  of the monitored process -- go
               back to sleep until that given time:
            *)
            monitor_loop rinfo minfo pid next_check
        else
            (* The sleeping time has expired -- check the status of the moni-
               tored process through its "last_activity":
            *)
            if  now -. minfo.last_activity () > minfo.max_blocking_time
            then
                Proc_Blocked
            else
            do {
                (* The process seems to be running fine -- produce a log of
                   its "liveness" if required:
                *)
                if   minfo.log_heartbeats
                then rinfo.log
                    ("MONITOR (PID="^(string_of_int (Unix.getpid()))^
                    "): WORKER PROCESS (PID="^(string_of_int pid)^
                    ") IS ALIVE!")
                else ();

                (* Next iteartion: with full sleep time: *)
                monitor_loop rinfo minfo pid (now +. minfo.max_blocking_time)
            }
};

(*-------------------*)
(* "fork_and_guard": *)
(*-------------------*)
value rec fork_and_guard:
    run_info -> monitor_info -> (unit -> unit) -> unit =

fun rinfo minfo worker_body  ->
let mpid = string_of_int monitor_pid.val  in
do {
    (* This is indeed a Monitor: *)
    assert (Unix.getpid() = monitor_pid.val);

    (* Do a little wait -- just in order to prevent a busy loop in this
       function if anything goes wrong:
    *)
    Unix.sleep 1;
    rinfo.log ("MONITOR (PID="^mpid^"): FORK AND GUARD...");

    (* Enable the SIGCHLD signal, to interrupt sleeping in the (future) parent
       when the child terminates.    This handler needs to be installed BEFORE
       "fork", or a race condition occurs if the child terminates immediately.
       For the same reason, the handler must set the "any_exit" flag. Any exi-
       sting handler is preserved:
    *)
    graceful_exit.val := False;
    any_exit.val      := False;

    Tnio.set_sig_handler
        Sys.sigchld
        ~new_handler_pos:Tnio.Handler_Attach_After
        (Tnio.Sig_Handler_Gen (fun _ -> any_exit.val := True));

    (* Also enable the SIGUSR1 signal, used for debugging purposes to emulate
       the blocking condition within the monitored process. This signal is to
       be delivered to the MONITOR only;  for safety (so it will not kill the
       child accidentially),   we set up this handler for both the parent and
       the child:
    *)
    block_debug.val   := False;

    Tnio.set_sig_handler
        Sys.sigusr1
        ~new_handler_pos:Tnio.Handler_Attach_After
        (Tnio.Sig_Handler_Gen (fun _ -> block_debug.val := True));

    (* Fork out the monitored process: *)

    let pid = Unix.fork   () in
    if  pid = 0
    then
    do {
        (* Child: Run the proc as a Leader in its Process Group: *)
        become_pg_leader  ();
        worker_pid.val:=  Unix.getpid ();

        rinfo.log ("WORKER PROCESS (PID="^(string_of_int (Unix.getpid()))^
                  ") HAS BEEN ACTIVATED");
        (* Invoke the actual Worker.     Exception handling is done by  the
           "worker_body" itself; if any exception still propagates  to this
           level, the child process is terminated with the error set by the
           OCaml RTS; the Monitor will regard it as "severe" or "exn" depen-
           ding on the values of those parms (and the error):
        *)
        worker_body       ()
    }
    else
    let wpid = string_of_int pid in
    do {
        (* "pid" is that of the Worker Process; PID of the Monitor remains
           the same as it was before "fork":
        *)
        worker_pid.val:= pid;

        (* Run the Monitor Loop: *)
        rinfo.log ("MONITOR (PID="^mpid^
                  "): WORKER PROCESS (PID="^wpid^") STARTED");
        try
            let first_check = Unix.gettimeofday () +. minfo.max_blocking_time
            in
            let proc_res    = monitor_loop rinfo minfo pid first_check in
            do {
                (* If we got here, the proc has terminated  (one way or another)
                   or got blocked. In any case, we first of all do the clean-up:
                   kill the whole group of the monitored process  (which is the
                   leader in that group).   This is especially necessary if the
                   monitored process is blocked, or created any children:
                *)
                kill_pg pid;

                (* The rest depends on what actually has happened: *)

                match proc_res with
                [Proc_Completed ->
                    (* If the process is "infinite" -- re-start it, UNLESS the
                       "graceful_exit" flag is set.   XXX: there may be a race
                       condition in setting this flag -- in the parent, it may
                       be set later than in the child due to  a later delivery
                       of SIGHUP -- so wait a bit first:
                    *)
                    let () = Unix.sleep 5
                    in
                    if  minfo.infinite_proc && (not graceful_exit.val)
                    then
                    do {
                        rinfo.log
                            ("MONITOR (PID="^mpid^
                            "): WORKER PROCESS (PID="^wpid^
                            ") UNEXPECTEDLY EXITED, RE-STARTING IT...");
                        fork_and_guard rinfo minfo worker_body
                    }
                    else
                    do {
                        (* Successful completion, all done: *)
                        rinfo.log
                            ("MONITOR (PID="^mpid^
                            "): WORKER PROCESS (PID="^wpid^
                            ") SUCCESSFULLY COMPLETED, EXITING NOW!");
                        Tnio.exit 0
                    }

                | Proc_Blocked ->
                    do {
                        rinfo.log
                            ("MONITOR (PID="^mpid^
                            "): WORKER PROCESS (PID="^wpid^
                            ") BLOCKED, RE-STARTING IT...");
                        (* Re-start the process: *)
                        fork_and_guard rinfo minfo worker_body
                    }

                | Proc_Crashed rc ->
                    if  rc <>  rinfo.severe_error
                    then
                    do {
                        rinfo.log
                            ("MONITOR (PID="^mpid^
                            "): WORKER PROCESS (PID="^wpid^
                            ") CRASHED (CODE="^(string_of_int rc)^
                            "), RE-STARTING IT...");
                        (* Re-start the process: *)
                        fork_and_guard rinfo minfo worker_body
                    }
                    else
                    do {
                        rinfo.log
                            ("MONITOR (PID="^mpid^
                            "): WORKER PROCESS (PID="^wpid^
                            ") CRASHED WITH NO-RESTART CODE ("^
                            (string_of_int rc)^"), EXITING NOW!");
                        (* Unsuccessful completion, don't run "a_the_end": *)
                        Tnio.exit rc
                    }
                ]
            }
        with [hmm ->
        do {
            (* "monitor_loop" propagated an exception, which should not
               normally happen:
            *)
            rinfo.log
                ("MONITOR (PID="^mpid^"): WARNING: MONITOR_LOOP EXCEPTION: "^
                (Misc_utils.print_exn hmm));

            (* Do clean-up -- the child(ren) may still be running: *)
            kill_pg pid;

            (* Still, try to go ahead: *)
            fork_and_guard rinfo minfo worker_body
        }]
    }
};

(*------------------*)
(*  "run_process":  *)
(*------------------*)
(*  A "high-level" function: first creates a daemon (if requested)  out of the
    current process,  then splits the process into the monitor  and the actual
    worker process (if requested).  PID(s) of these processes are saved in the
    specified "pid_file" for management by external tools  (such as start/stop
    scripts), unless the file name is "". SIGHUP handler may be set to provide
    graceful exit of the worker process:
*)
value run_process: run_info -> unit =
fun rinfo ->
    (* The body of the worker process: *)

    let worker_body: unit -> unit =
    fun () ->
    let wpid = string_of_int (Unix.getpid ()) in
    do {
        (* Save the Worker's (and possibly the Monitor's) PID.
           XXX A big question is what to do if this file already exists:

           (a) just over-write it and start the new process(es);
           (b) first kill the process(es) listed in the PID file;
           (c) do not proceed if the listed processes are still running.

           Each strategy has its disadvantages:

           (a) possibility of multiple instances created;
           (b) killing of valid instances (or even ourselves)  or other valid
               processes (due to PID re-use);
           (c) failure to start due to a stale PID file.

           The strategy currently implemented:
           (b) kill the listed process(es) (unless it's ourself / our parent),
               then start a new instance.
        *)
        if  rinfo.pid_file <> ""
        then
            let (old_pid, old_ppid) =
            try
                (* Try to read this file. Format: [PID] or [PID PPID]: *)
                let cont = ExtString.String.strip
                          (Misc_utils.read_file rinfo.pid_file)
                in
                match Misc_utils.split_by_whites cont with
                [ [p] ->
                    (* It's only the PID, no PPID: *)
                    (int_of_string p, -1)
                | [p; pp] ->
                    (* Got both: *)
                    (int_of_string p, int_of_string pp)
                | _ ->
                    (* Invalid format -- have not got anything: *)
                    failwith ("Invalid format: "^cont)
                ]
            with [hmm ->
                (* This is normal  if the PID file  is missing: *)
                (-1, -1)
            ]
            in
            (* If "old_pid" is not our own PID (that must not happen anyway,
               but just take a pre-caution),   then kill the whole group of
               that process:
            *)
            let () =
            try
            do {
                if   (old_pid  <> Unix.getpid  ()) && (old_pid  <> -1)
                then kill_pg old_pid
                else ();

                if   (old_ppid <> Unix.getppid ()) && (old_ppid <> -1)
                then kill_pg old_ppid
                else ()   (* Don't even produce a warning here  -- having the
                             same parent running is normal when the child got
                             a crash or blocking, and is being re-started
                          *)
            }
            with[_ -> ()] (* Ignore errors (e.g., non-existent processes) *)
            in
            (* We can now save the new PID / PPID: *)
            try
                let pidch = open_out rinfo.pid_file in
                do {
                    match rinfo.with_monitor with
                    [ None ->
                        (* No monitor, hence no parent PID: *)
                        Printf.fprintf pidch "%d\n" (Unix.getpid ())

                    | _    ->
                        (* Save both our PID and PPID: *)
                        Printf.fprintf pidch "%d %d\n"
                            (Unix.getpid ()) (Unix.getppid ())
                    ];
                    close_out pidch
                }
            with [hmm ->
            do {
                rinfo.log
                  ("WORKER PROCESS (PID="^wpid^"): FATAL INIT ERROR: "^
                  (Printf.sprintf "Cannot save PID in \"%s\": %s"
                      rinfo.pid_file (Misc_utils.print_exn hmm))
                  );
                Tnio.exit rinfo.severe_error
            }]
        else ();  (* Don't save PID(s) at all -- nowhere *)

        (* Detach the TNIO RTS from that of the parent, if monitoring is on: *)
        match rinfo.with_monitor with
        [None -> ()
        |_    -> Tnio.detach_child ()
        ];

        (* Run the init phase. It returns a new SIGHUP clean-up function: *)
        try  worker_clean_up.val := rinfo.init_phase ()
        with [hmm ->
        do {
            rinfo.log ("WOKER PROCESS (PID="^wpid^"): FATAL INIT INIT ERROR: "^
                      (Misc_utils.print_exn hmm));
            Tnio.exit rinfo.severe_error
        }];

        (* Set the privilege level: *)
        try  Misc_utils.run_as rinfo.run_as_user rinfo.run_as_group
        with [hmm ->
        do {
            rinfo.log
                ("WORKER PROCESS (PID="^wpid^"): FATAL INIT ERROR: "^
                (Printf.sprintf "Cannot run as \"%s.%s\": %s"
                    rinfo.run_as_user rinfo.run_as_group
                    (Misc_utils.print_exn hmm))
                );
            Tnio.exit rinfo.severe_error
        }];

        (* Run the operational phase -- any uncaught exceptions are logged
           and converted into "non-severe" errors:
        *)
        try  rinfo.oper_phase ()
        with [hmm ->
        do {
            rinfo.log
                ("WORKER PROCESS (PID="^wpid^"): UnCaught Exception: "^
                (Misc_utils.print_exn hmm)^", EXITING FOR RE-START...");
            Tnio.exit rinfo.exn_error
        }];

        (* If we got here, assume successful completion: *)
        rinfo.log
            ("WORKER PROCESS (PID="^wpid^
            "): SUCCESSFULLY COMPLETED, EXITING NOW!");
        Tnio.exit 0;
    }
    in
    do {
        (* Should we run as a daemon? *)
        if  rinfo.daemon_mode
        then
        do {
            (* Create a daemon: "fork", exit the parent, retain the child. The
               latter becomes a child of the "init" process:
            *)
            let dpid = Unix.fork () in
            if  dpid <> 0
            then
                (* It's the parent process -- terminate it: *)
                Tnio.exit 0
            else ();
            rinfo.log ("DAEMON (PID="^(string_of_int (Unix.getpid()))^
                      ") CREATED");

            (* It is the child process -- disconnect from the controlling ter-
               minal:
            *)
            become_pg_leader ()
        }
        else ();

        (* Set up the SIGHUP handler, no matter whether we are going to "fork"
           a monitor afterwards or not, If we are, the handler must be set in
           BOTH the parent and the child, so do it here.
           Also, set the PIDs of the Monitor and the Worker -- for the moment,
           they are the same, and they are only used  in the following signal
           handler.
           The clean-up function to be invoked  when the Worker Process termi-
           nated by SIGHUP was initially set to "do-nothing"; "init_phase" of
           the Worker may return a new clean-up handler:
        *)
        monitor_pid.val := Unix.getpid  ();
        worker_pid.val  := monitor_pid.val;

        Tnio.set_sig_handler
            Sys.sighup
            ~new_handler_pos:Tnio.Handler_Attach_After
            (Tnio.Sig_Handler_Exit
                (fun _ ->
                do {
                    graceful_exit.val := True;  (* Exiting gracefully! *)
                    any_exit.val      := True;  (* Exiting anyway!     *)
                    let pid = Unix.getpid () in
                    (* NB: On receiving this signal,  only the Worker Process
                       should exit immediately; the Monitor should do so only
                       after finalising the child properly, otherwise a zomby
                       could be created, which is in general undesirable:
                    *)
                    if  pid = worker_pid.val
                    then (* We are inside the Worker Process *)
                    do {
                        worker_clean_up.val ();        (* IMPORTANT! *)
                        rinfo.log
                            ("WORKER PROCESS (PID="^(string_of_int pid)^
                            "): SIGHUP RECEIVED, EXITING GRACEFULLY...");
                    }
                    else ()
                })
            (* ExitCode *) 0);

        (* Now: no matter whether we run as a daemon on not, check for
           monitoring:
        *)
        match  rinfo.with_monitor with
        [ Some minfo ->
            (* Run under a monitor: *)
            fork_and_guard rinfo minfo worker_body

        | None ->
            (* Run stand-alone: *)
            worker_body ()
        ]
    };

