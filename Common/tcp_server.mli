(*pp camlp4r *)
(* vim:ts=4:syntax=ocaml
*)
(*===========================================================================*)
(*                             "tcp_server.mli":                             *)
(*             Interface to the Generic Fault-Tolerant TCP Server            *)
(*                    (C) Explar Technologies Ltd, 2004                      *)
(*===========================================================================*)
(*--------------------*)
(*  "server_config":  *)
(*--------------------*)
(*  'socket is the type of the communication sockets used
            (e.g., Tnio.tsock or Ssl.socket);
    'state  is the type of the intrernal state of worker threads:
*)
type server_config 'socket 'state =
{
    (* TCP Parameters: *)
    listen_on_ip        : Unix.inet_addr;
    listen_on_port      : int;
    listen_queue_len    : int;
    max_connections     : int;
    sock_buff_size      : int;

    (* Acceptor Thread Parameters: *)
    acceptor_priority   : int;
    acceptor_stack_k    : int;

    (* Worker Pool Parameters -- same as in "Gen_server.server_config": *)
    max_idle_threads    : int;
    min_idle_threads    : int;
    init_threads        : int;
    worker_priority     : int;
    worker_stack_k      : int;

    (* "init_socket":
       Converts the raw client communication socket (Tnio.tsock) returned by
       the acceptor, into the "'socket" type (needed if e.g. SSL is used).
       NB: a deadline is used:
    *)
    init_socket         : Tnio.tsock -> 'state -> float -> 'socket;

    (* "finalise_socket":
       Called before the socket is closed -- e.g., to finalise SSL-realted
       structures -- with a deadline:
    *)
    finalise_socket     : 'socket -> float -> unit;

    (* The following parms are the same as in "Gen_server.server_config": *)
    init_worker_state   : unit   -> 'state;
    cleanup_state       : 'state -> unit;
    reset_state_on_error: bool;

    (* Call-back invoked by the acceptor after it has been bound to the
       acceptor socket; can be used e.g. to lower the process priorities:
    *)
    after_acceptor_bind : unit   -> unit;

    (* Call-back invoked on errors, typically for logging: *)
    log_error           : exn    -> unit;

    (* Call-back invoked when there are too many open connections, before
       the curr connection is closed -- for that reason:
    *)
    reject_conn         : Tnio.tsock -> unit;

    (* The actual function  dealing with the client connections -- invoked
       within a worker thread:
       Arg1:   client communication socket created by "accept";
       Arg2:   client address;
       Arg3:   worker thread state;
       Arg4:   deadline for the dialogue with the client;
       Result: updated state of the worker thread:

       The user DOES NOT need to make sure  that the "socket" and "state"
       are properly finalised when this function exits (either normally or
       by exception) -- this is done automatically:
    *)
    proc_func           : 'socket -> Unix.sockaddr -> 'state -> float ->
                          'state;
    (* Time-out for dealing with each client, used in automatic calculation
       the deadline supplied to "proc_fun". This time-out is only advisory:
       the TCP server does NOT use it by itself to enforce disconnection of
       clients:
    *)
    proc_timeout        : float
};

(*-----------------*)
(*  "server_t":    *)
(*  "server_info": *)
(*-----------------*)
type server_t    'socket 'state = 'x;  (* Private type *)

type server_info 'socket 'state =
{
    server_config    : server_config 'socket 'state;
    curr_connections : int;
    curr_all_threads : int;
    curr_idle_threads: int
};

(*----------------------*)
(*  Server Management:  *)
(*----------------------*)
value create_server  : server_config 'socket 'state ->
                       server_t      'socket 'state;

value get_server_info: server_t      'socket 'state ->
                       server_info   'socket 'state;

value shutdown_server: server_t      'socket 'state ->
                       Gen_server.shutdown_mode     -> unit;

