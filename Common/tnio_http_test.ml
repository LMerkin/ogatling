(*pp camlp4r *)
(* vim:ts=4:syntax=ocaml
*)
(*===========================================================================*)
(*                           "tnio_http_test.ml":                            *)
(*                 TNIO RTS test, as a simple HTTP(S) Client                 *)
(*                  (C) Explar Technologies Ltd, 2003--2006                  *)
(*===========================================================================*)
(* Get the command-line parms: *)

value (server, n_threads, n_iters) =
try
    (Sys.argv.(1),
     int_of_string  Sys.argv.(2),
     int_of_string  Sys.argv.(3)
    )
with [_ ->
do {
    prerr_endline
        "PARAMETERS: <URL> <N_Threads> <N_Iters_Per_Thread>";
    exit 1
}];

(* Initialise the RTS: *)
Tnio.init (Tnio.Priority_Bands 5);

(* Create an HTTP client in a given thread, then start operating loop.
   NB: It is important that the client is created within the thread
   itself, not in the calling main thread, as passing of open sockets
   across threads is forbidden:
*)
value rec tfn_loop: Clients_simple.http_agent -> int -> int -> unit =
fun agent given_id n ->
    let deadline = Unix.gettimeofday() +. 15.0 in
    let self     = Tnio.thread_self ()         in
    let id       = Tnio.thread_id   self       in
    let ()       = if id <> given_id
                   then failwith (Printf.sprintf "id=%d, given_id=%d\n"
                                                 id given_id)
                   else ()
    in
    if  n <= 0
    then
    do{
        (* All done! *)
        Http_client.close_agent agent deadline;
        Printf.printf "THREAD %d IS COMPLETED\n" id
    }
    else
    do {
        try
        do {
            let resp     =
                Clients_simple.http_get agent Sys.argv.(1) deadline
            in
            print_endline  resp.Http_client.body
        }
        with [hmm -> prerr_endline (Misc_utils.print_exn hmm ) ];

        print_endline "=================================================\n";

        (* Force GC, to check against memory errors: *)
        Gc.compact ();

        (* Wait a bit, then next iteration: *)
        Tnio.thread_sleep 1.0;
        tfn_loop agent given_id (n-1)
    };

value tfn_init: int -> int -> unit =
fun given_id n ->
    let agent =
        Clients_simple.mk_http_agent (Clients_simple.mk_dns_agent [])
    in
    tfn_loop agent given_id n;


prerr_endline "ROUND 1:\n";

for i = 0 to n_threads-1
do {
    ignore (Tnio.thread_create
        ~priority:(i mod 5) (fun () -> tfn_init i n_iters))
};

Tnio.threads_scheduler ();

prerr_endline "ROUND 2:\n";

for i = 0 to n_threads-1
do {
    ignore (Tnio.thread_create
        ~priority:(i mod 5) (fun () -> tfn_init i n_iters))
};

Tnio.threads_scheduler ();
Tnio.exit 0;

