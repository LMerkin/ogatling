(*pp camlp4r *)
(* vim:ts=4:syntax=ocaml
*)
(*===========================================================================*)
(*                              "http_common.ml":                            *)
(*      Implementation of Functions Common to HTTP(S) Client and Server      *)
(*                     (C) Explar Technologies Ltd, 2004                     *)
(*===========================================================================*)
(*  For server-side operation only: (HTTP_Err_Code, Comment): *)
exception HTTP_Error of int and string;

(*---------------*)
(*  HTTP Cookie: *)
(*---------------*)
type http_side = [Server_Side | Client_Side of string];
                                (* Connected domain *)
type http_cookie =
{
    cookie_name:    string;
    cookie_value:   string;
    cookie_domain:  string;
    cookie_path:    string;
    cookie_expires: float
};

(*-------------------*)
(*  "dict_install":  *)
(*-------------------*)
(* Installs (name, vlue) in the "dict", re-combining them with the existing
   content if applicable:
*)
value dict_install: Hashtbl.t 'a (list 'b) -> 'a -> 'b -> unit =
fun   dict name vlue ->
try
    (* There are aleady bindings for this "name" -- recombine them: *)
    let old = Hashtbl.find dict name in
    Hashtbl.replace   dict name [vlue::old]
with
[Not_found ->
    (* No bindings for this "name" yet: *)
    Hashtbl.add dict name [vlue]
];

(*-------------------*)
(*  "read_headers":  *)
(*-------------------*)
(* Read all the HTTP headers, and return them as a dictionary: *)

value hdr_re = Pcre.regexp ~study:True "^(.+?):\\s+(.+)$";

value rec read_headers':
    Misc_utils.line_reader 'socket -> 'socket ->
    Hashtbl.t string (list string) -> float   -> unit =

fun reader socket dict deadline ->
    let line = reader#recv_line socket deadline in
    if  line = ""
    then
        () (* All done! *)
    else
    do {
        let parts = Pcre.extract ~rex:hdr_re ~full_match:False line in

        (* Conver the "name" into the lower-case: *)
        let name  = String.lowercase parts.(0) in
        let vlue  = parts.(1)                  in
        dict_install dict name vlue;

        read_headers' reader socket dict deadline
    };

value read_headers:
    Misc_utils.line_reader 'socket -> 'socket -> float ->
    Hashtbl.t string (list string) =

fun reader socket deadline ->
    let dict = Hashtbl.create 100 in
    do {
        read_headers' reader socket dict deadline;
        dict
    };

(*----------*)
(*  Errors: *)
(*----------*)
value http_err_msgs_lst: list (int * string) =
[
    (100, "Continue");
    (101, "Switching Protocols");
    (200, "OK");
    (201, "Created");
    (202, "Accepted");
    (203, "Non-Authoritative Information");
    (204, "No Content");
    (205, "Reset Content");
    (206, "Partial Content");
    (300, "Multiple Choices");
    (301, "Moved Permanently");
    (302, "Found");
    (303, "See Other");
    (304, "Not Modified");
    (305, "Use Proxy");
    (307, "Temporary Redirect");
    (400, "Bad Request");
    (401, "Unauthorized");
    (402, "Payment Required");
    (403, "Forbidden");
    (404, "Not Found");
    (405, "Method Not Allowed");
    (406, "Not Acceptable");
    (407, "Proxy Authentication Required");
    (408, "Request Timeout");
    (409, "Conflict");
    (410, "Gone");
    (411, "Length Required");
    (412, "Precondition Failed");
    (413, "Request Entity Too Large");
    (414, "Request-URI Too Long");
    (415, "Unsupported Media Type");
    (416, "Requested Range Not Satisfiable");
    (417, "Expectation Failed");
    (500, "Internal Server Error");
    (501, "Not Implemented");
    (502, "Bad Gateway");
    (503, "Service Unavailable");
    (504, "Gateway Timeout");
    (505, "HTTP Version Not Supported")
];

(* "http_err_msgs" is a function, not just a Hashtbl object, as the latter is
   always mutable, so a global object would be modifyable by the clients:
*)
value http_err_msgs: unit -> Hashtbl.t int string  =
fun () ->
    let ht = Hashtbl.create 50 in
    do {
        List.iter
            (fun (code, msg) -> Hashtbl.add ht code msg) http_err_msgs_lst;
        ht
    };

(* Static object for internal use: *)
value http_err_msgs_static = http_err_msgs ();

(*  "http_error":
    Generates different exceptions depending on the caller's side
    (client or server):
*)
value http_error: http_side -> int -> string -> 'a =
fun side code msg ->
    match side with
    [ Client_Side dom ->
        failwith ("Http_common: CLIENT ERROR on "^dom^": "^msg)

    | Server_Side   ->
        raise (HTTP_Error code msg)
    ];

(*---------------------*)
(*  "get_single_val":  *)
(*---------------------*)
value get_single_val: Hashtbl.t 'string (list 'a) -> string -> 'a =
fun ht pname ->
try
    match Hashtbl.find ht pname with
    [ [v] -> v
    | _   -> failwith
             ("Http_common.get_single_val: Multi- or []-value for "^pname)
    ]
with
[ Not_found ->
    failwith ("Http_common.get_single_val: No such key: "^pname)
];

(*---------------*)
(*  "parse_url": *)
(*---------------*)
(* Returns (protocol, host, port, path_etc): *)

value url_re = Pcre.regexp ~study:True ~flags:[`CASELESS]
    "^(([a-z]+)://)?([a-z0-9.-]+)?(:([0-9]+))?(.*)$";

value parse_url: string  -> (string * string * int * string)    =
fun url ->
try
    if  url   =   "" (* Not allowed! *)
    then failwith "" (* Exception re-thrown at the end... *)
    else
    let mres  = Pcre.extract ~rex:url_re ~full_match:False url in
    let ()    = assert (Array.length mres = 6)                 in

    (* First of all, detect local (absolute or relative) URLs  -- they only
       contain "path_etc" -- this is how we distinguish it from a "host" in
       this case: "host" always requires "proto":
    *)
    if  mres.(0) = ""
    then
        (* Don't make any assumptions on the "proto", "host" and "port" --
           in this case, they depend on where we are connected to:
        *)
        ("", "", 0, url)
    else
    (* Full URL: *)
    let proto = String.lowercase mres.(1) in

    (* XXX: can the following restriction be relaxed? *)
    if  proto <> "http" && proto <> "https"
    then
        failwith ("Http_common.parse_url: Invalid protocol: \""^proto^"\"")
    else
    let host  = String.lowercase mres.(2) in
    let port  =
        if  mres.(4) <> ""
        then
            int_of_string mres.(4)
        else
        if  proto = "http"
        then 80  (* Default HTTP  port *)
        else 443 (* Default HTTPS port *)
    in
    let rest     = mres.(5) in
    let path_etc =
        if  rest = ""
        then "/"
        else rest (* NB: "rest" may be relative! *)
    in
    (proto, host, port, path_etc)
with
[_ ->
    failwith ("Http_common.parse_url: Invalid URL: \""^url^"\"")
];
 
(*--------------------*)
(*  "parse_urlpath":  *)
(*--------------------*)
(* Parses Path_Etc, returns (Path, Parms, Query, Fragment): *)

value url_re = Pcre.regexp ~study:True
               "^([^;?#]*)(;([^;?#]*))?(\\?([^;?#]*))?(#([^;?#]*))?$";

value parse_urlpath: string -> (string * string * string * string) =
fun urlpath ->
try
    let parts = Pcre.extract ~rex:url_re ~full_match:False urlpath in
    (parts.(0), parts.(2), parts.(4), parts.(6))
with
[Not_found ->
    (* Malformed URL: *)
    failwith ("Http_common.parse_urlpath: Malformed URL path: "^urlpath)
];

(*------------------*)
(*  "parse_query":  *)
(*------------------*)
(*  Fills in the "query_dict" by (name, value) pairs extracted and decoded
    from the "query" string:
*)
value parse_query: string -> Hashtbl.t string (list string) -> unit =
fun query dict ->
    (* Hmm... Need to test "query" for "" explicitly, or "nsplit" will
       raise an exception on empty query:
    *)
    if  query = ""
    then ()
    else
    let pairs = ExtString.String.nsplit query "&"  in
    List.iter
        (fun nv ->
            let len   = String.length nv in
            let e_pos =
                (* "nv" must be a "name=value" pair, and the "name" must
                   not be empty (but the "value" may be):
                *)
                try String.index nv '=' with [Not_found -> -1]
            in
            if  e_pos <= 0
            then failwith
                 ("Http_common.parse_query: Invalid query: "^query)
            else
            let name =  Netencoding.Url.decode ~plus:True
                        (String.sub nv 0  e_pos)
            in
            let vlue =  Netencoding.Url.decode ~plus:True
                        (String.sub nv   (e_pos+1) (len-e_pos-1))
            in
            dict_install dict name vlue
        )
        pairs;

(*-------------*)
(*  "mk_url":  *)
(*-------------*)
(*  Inverse of "parse_url":  *)

value mk_url: string -> string -> int -> string -> string =
fun proto host port path_etc   ->
    let proto' =
        if   proto <> ""
        then proto^"://"
        else "http://"  (* Default *)
    in
    let port' =
        if  proto'="http://"  && port = 80 ||
            proto'="https://" && port = 443
        then
            ""  (* No need to specify explicitly *)
        else
            ":"^(string_of_int port)
    in
    proto'^host^port'^path_etc;

(*----------------*)
(*  "join_urls":  *)
(*----------------*)
value join_urls: string -> string -> string =
fun url1 url2 ->
    (* Is "url2" full? *)
    let (proto2, host2, port2, path_etc2) = parse_url url2   in
    let ()    =  assert (path_etc2 <> "") (* In any case! *) in
    if   proto2 <> ""
    then
    do {
        (* Yes, it is full -- return it as is:  *)
        assert (host2 <> "" && port2 > 0);
        url2
    }
    else
    (* "url2" is local -- need to parse "url1"; it
       is REQUIRED to be full in this case:
    *)
    let (proto1, host1, port1, path_etc1) = parse_url  url1
    in
    if  (proto1 = "" || host1 = "" || port1 <= 0 || path_etc1 = "" ||
        path_etc1.[0] <> '/')
    then
        failwith  ("Http_common.join_urls: Invalid base URL: "^url1)
    else
    if  path_etc2.[0] =  '/'
    then
        (* "url2" is local but absolute; re-construct it: *)
        mk_url proto1 host1 port1 path_etc2
    else
        (* "url2" is relative -- use elements of "path_etc1" to make
           it absolute:
        *)
        let p          = String.rindex path_etc1 '/'     in
        let base_path1 = String.sub    path_etc1 0 (p+1) in
        let abs_path2  = base_path1 ^  path_etc2         in
        mk_url proto1 host1 port1 abs_path2;

(*-----------------*)
(*  "mk_urlpath":  *)
(*-----------------*)
(*  Inverse of "parse_urlpath":  *)

value mk_urlpath:   string -> string -> string -> string -> string =
fun path parms query fragm ->
    let parms' =
        if  parms <> ""
        then ";"^parms
        else ""
    in
    let query' =
        if  query <> ""
        then "?"^query
        else ""
    in
    let fragm' =
        if  fragm <> ""
        then "#"^fragm
        else ""
    in
    path^parms'^query'^fragm';

(*---------------*)
(*  "mk_query":  *)
(*---------------*)
(*  Inverse of "parse_query":  *)

value mk_query: Hashtbl.t string (list string) -> string =
fun qdict ->
    String.concat "&"
    (Hashtbl.fold
        (fun qn qvs curr ->
            let pairs =
                String.concat "&"
                (List.map
                    (fun qv ->
                        let  qv' = Netencoding.Url.encode ~plus:True qv in
                        qn^"="^qv'
                    )
                    qvs
                )
            in
            [pairs::curr]
        )
        qdict []
    );

(*--------------------*)
(*  "parse_cookies":  *)
(*--------------------*)
(*  Parses the value of a "cookie" or "set-cookie" header, extracting all the
    cookie data from it:
*)
value sep_re    = Pcre.regexp ~study:True ";\\s+";
value nev_re    = Pcre.regexp ~study:True "(.+?)=(.*)";

value parse_cookies: string -> string -> list http_cookie =
fun dom hval ->
try
    (* Initial parsing: *)
    let parts  = Pcre.split ~rex:sep_re hval in

    (* The attributes to fill in: *)
    let cnvs   = ref []  in
    let cdom   = ref dom in
    let cpath  = ref "/" in
    let cexpir = ref (Date_time.epoch_end) in

    (* Go through the "parts": they may define multiple cookies, that is,
       multiple (name, value) combinations, but their attributes must all
       be the same:
    *)
    let () =
        List.iter
        (fun part ->
            let lpart = String.lowercase part in
            if  lpart = "secure" || lpart = "httponly"
            then
                () (* XXX: Ignore them for now *)
            else
            let nv    = Pcre.extract ~rex:nev_re ~full_match:False part in
            let cname = nv.(0) in
            let cval  = nv.(1) in
            if  cname = "domain"
            then
                (* This is actually a "domain" attr:  *)
                if   cdom.val <> dom  (* Already set? *)
                then failwith "duplicate domain"
                else cdom.val := cval
            else
            if  cname = "path"
            then
                (* This is actually a "path" attr:    *)
                if   cpath.val <> "/" (* Already set? *)
                then failwith "duplicate path"
                else cpath.val := cval
            else
            if  cname = "expires"
            then
                (* This is actually an "expires" attr:    *)
                if   cexpir.val <> Date_time.epoch_end
                then failwith "duplicate expiration"
                else cexpir.val := Date_time.parse_http_date cval
            else
            (* This is are normal name=value cookie data: *)
            cnvs.val := cnvs.val @ [(cname, cval)]
        )
        parts
    in
    (* The Cookie objects: *)
    List.map
        (fun (cname, cval) ->
        {
            cookie_name   = cname;
            cookie_value  = cval;
            cookie_domain = cdom.val;
            cookie_path   = cpath.val;
            cookie_expires= cexpir.val
        })
        cnvs.val
with
[hmm ->
    failwith ("Http_common.parse_cookies: Invalid cookie: "^hval^": "^
             (Misc_utils.print_exn hmm))
];

(*------------------*)
(*  "get_cookies":  *)
(*------------------*)
value get_cookies:
    http_side -> Hashtbl.t string (list string) -> list http_cookie =

fun side headers  ->
    let (dom, hdr_name) =
        match side with
        [ Server_Side      -> ("",   "cookie")
        | Client_Side dom' -> (dom', "set-cookie")
        ]
    in
    let hdr_vals =
        try  Hashtbl.find headers hdr_name
        with [_ -> []]
    in
    List.concat (List.map (parse_cookies dom) hdr_vals);


(*--------------------*)
(*  "print_headers":  *)
(*--------------------*)
value print_headers: Hashtbl.t string (list string) -> out_channel -> unit =
fun headers outch ->
    Hashtbl.iter
    (fun n vs ->
        List.iter (fun v -> Printf.fprintf outch "%s: %s\n%!" n v) vs
    )
    headers;

(*---------------------*)
(*  "read_http_data":  *)
(*---------------------*)
(*  "get_content_size":
    If the "Content-length" header is present, its valud is returned.
    If the "chunked" mode is used, (-1) is returned.
    Otherwise, the maximum allowed value  ("buff_size")  is returned:
*)
value get_content_size:
    http_side    -> Hashtbl.t string (list string) -> int -> int =
fun side headers buff_size ->
    try
        match Hashtbl.find headers  "content-length"   with
        [ [cl]   ->  int_of_string  cl        (* Explicit size *)
        | _      ->  http_error side 400 "invalid Content-Length"
        ]
    with
    [ Not_found  ->
    try
        match Hashtbl.find headers "transfer-encoding" with
        [ [te] ->
            if  String.lowercase te = "chunked"
            then -1                  (* Chunked mode *)
            else buff_size           (* Default size *)
        | _    ->
            http_error side 400 "invalid Transfer-Encoding"
    ]
    with
    [ Not_found ->
        buff_size (* Default size *)
    ]];


(*  "read_chunk":
    Returns the length of the data read into the "buff"  (starting from the
    "from" position) out of the requested "need_size" bytes, and the socket
    status after reading:
*)
type after_read = [EOF | Still_On];

value read_chunk:
    http_side                       ->
    Misc_utils.io_func     'socket  ->
    Misc_utils.line_reader 'socket  ->
    'socket                         ->
    string                          ->
    int                             ->
    int                             ->
    float                           ->
    (int * after_read) =

fun side read_func line_reader socket buff from need_size deadline ->

    (* The maximum size which will still fit in the "buff": *)
    let max_size = String.length buff - from  in
    if  max_size < need_size
    then
        http_error side 500
            (Printf.sprintf "buffer too small (%d bytes, need %d)"
            max_size need_size)
    else
    (* NB: be careful to recover any data left in the line reader, but
       no more than "need_size" -- the rest of data, if any, are to be
       left in the reader:
    *)
    let left = line_reader#leftover_data need_size in
    let llen = String.length left         in
    let ()   = assert (llen <= need_size) in

    (* Copy "left" into the "buff" at "from": *)
    let ()   = String.blit left 0 buff from llen in
    if  llen = need_size
    then
        (* The required data are already in:  *)
        (llen, Still_On)
    else
    (* 0 <= llen < need_size <= max_size: *)

    (* Do  the actual reading into the free part of the "buff": *)
    let to_get = need_size - llen in
    let got  =
        try Misc_utils.strict_io
            read_func socket buff (from+llen) to_get deadline
        with
        [_ -> 0] (* The client has alreday shut down their sending socket?  *)
    in
    let total =  llen + got in
    if  total <> need_size
    then
    do {
        assert (total < need_size);
        if  need_size = max_size
        then
            (* It may be OK that we got less than the maximum, but
               the socket is no longer alive:
            *)
            (total, EOF)
        else
            (* Required content size not received -- peer error! *)
            http_error side 400 "data size not reached"
    }
    else
        (* All "need_size" bytes obtained:  *)
        if  total = max_size
        then
            (* Input too large, as it reached the end of the "buff": *)
            http_error side 413 "data too large"
        else
            (* OK: *)
            (total, Still_On);


(*  "read_http_data":
    The externally-visible function:
*)
value read_http_data:
    http_side                       ->
    Misc_utils.io_func     'socket  ->
    Misc_utils.line_reader 'socket  ->
    'socket                         ->
    string                          ->
    Hashtbl.t string (list string)  ->
    float                           ->
    (string * after_read) =

fun side read_func line_reader socket buff headers deadline ->

    (* How much do we need, and how much can we store? *)
    let buff_size = String.length buff in
    let need_size = get_content_size side headers buff_size in

    let (total, status) =
        if  need_size = -1
        then
            (* Chunked mode: *)
            let rec read_chunks: int -> (int * after_read) =
            fun from ->
                (* Get the next chunk's size: *)
                let chunk_size =
                    try
                        let sl = line_reader#recv_line socket deadline   in
                        Scanf.sscanf sl "%x" (fun l -> l)
                    with
                    [_ -> http_error side 400 "chunk size error"]
                in
                if  chunk_size = 0
                then
                    (* No more chunks -- return the curr "buff" length. The
                       socket is presumably still alive:
                    *)
                    (from, Still_On)
                else                
                let (got_size, after_read) =
                    read_chunk
                    side read_func line_reader socket buff from chunk_size
                    deadline
                in
                (* The sizes MUST match, and the socket MUST still be alive: *)
                if not (got_size = chunk_size && after_read = Still_On)
                then
                    http_error side 400 "invalid chunk size received"
                else
                (* Read in the empty line: *)
                let empty =  line_reader#recv_line socket deadline in
                if  empty <> ""
                then
                    http_error side 400 "invalid chunk delimiter"
                else
                (* Try to get more chunks: *)
                read_chunks (from + chunk_size)
            in
            (* Do the reading: *)
            read_chunks 0
        else
            (* Reading the given number of bytes, or until EOF: *)
            read_chunk
            side read_func line_reader socket buff 0 need_size deadline
    in
    (String.sub buff 0 total, status);


(*-------------------*)
(*  "log_http_exn":  *)
(*-------------------*)
(*  Logging Front-End exceptions -- trivial exceptions such as "client
    disconnected" are NOT logged:
*)
value log_http_exn: (string -> unit) -> exn -> unit =
fun   log_func hmm ->
    match hmm with
    [ Failure msg when
     (ExtString.String.starts_with msg
        "Misc_utils.line_reader: Read failed"       ||
      ExtString.String.starts_with msg
        "Http_common.send_str: Client disconnected")->
        (* The user has disconnected:     *)
        ()
    | HTTP_Error rc msg ->
        (* Invalid request from the user: *)
        log_func ("HTTP ERROR "^(string_of_int rc)^": "^msg)
    | _ ->
        log_func ("HTTP ERROR: "^(Misc_utils.print_exn hmm))
    ];

