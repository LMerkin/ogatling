(*pp camlp4r *)
(* vim:ts=4:syntax=ocaml
*)
(*===========================================================================*)
(*                              "tcp_server.ml":                             *)
(*    Generic TCP server implemented on top of the abstract "Gen_server"     *)
(*                    (C) Explar Technologies Ltd, 2004                      *)
(*===========================================================================*)
(*===============*)
(*  Data Types:  *)
(*===============*)
(*------------------*)
(* "server_config": *)
(*------------------*)
(* "'socket" is normally either "Tnio.tsock", or "Ssl.socket": *)

type server_config 'socket 'state =
{
    (* TCP Parameters: *)
    listen_on_ip        : Unix.inet_addr;
    listen_on_port      : int;
    listen_queue_len    : int;
    max_connections     : int;
    sock_buff_size      : int;

    (* Acceptor Parameters: *)
    acceptor_priority   : int;
    acceptor_stack_k    : int;

    (* Worker Pool Parameters: *)
    max_idle_threads    : int;
    min_idle_threads    : int;
    init_threads        : int;
    worker_priority     : int;
    worker_stack_k      : int;

    (* State Management: *)
    init_socket         : Tnio.tsock -> 'state -> float  -> 'socket;
    finalise_socket     : 'socket -> float     -> unit;
    init_worker_state   : unit    -> 'state;
    cleanup_state       : 'state  -> unit;
    reset_state_on_error: bool;
    after_acceptor_bind : unit    -> unit;
    log_error           : exn     -> unit;
    reject_conn         : Tnio.tsock -> unit;

    (* User-Level Processing: *)
    proc_func           : 'socket -> Unix.sockaddr -> 'state -> float -> 'state;
    proc_timeout        : float
};

(*------------------*)
(*  "server_info":  *)
(*------------------*)
type server_info 'socket 'state =
{
    server_config    : server_config 'socket 'state;
    curr_connections : int;
    curr_all_threads : int;
    curr_idle_threads: int
};

(*---------------*)
(*  "server_t":  *)
(*---------------*)
type server_t 'socket 'state =
{
    (* Configuration of this server: *)
    config       : server_config 'socket 'state;

    (* The Acceptor Thread: *)
    acceptor_sock: mutable Tnio.tsock;
    acceptor     : mutable Tnio.thread_t;

    (* The underlying "Gen_server" which provides worker threads: *)
    worker_pool  :
    (Gen_server.server_t
        (Tnio.tsock * Tnio.thread_t * Unix.sockaddr * (ref int))) 'state unit;

    (* The current number of connections accepted: *)
    curr_conns   : ref int
};

(*====================*)
(*  Acceptor Thread:  *)
(*====================*)
(*  Acts as a CLIENT to the pool of worker threads: *)

(*------------------------*)
(*  "acceptor_body_loop": *)
(*------------------------*)
value rec acceptor_body_loop: server_t 'socket 'state -> unit =
fun server ->
do {
    try
        (* Accept  a connection from the client.
           Currently, there is no deadline here:
        *)
        let (cc, client_addr) =
            Tnio.accept ~kern_buff_size:server.config.sock_buff_size
                        server.acceptor_sock infinity
        in
        (* However, close the connection if there are already
           too many connections open:
        *)
        if  server.curr_conns.val >= server.config.max_connections
        then
        do {
            (* Invoke the call-back, and close the socket: *)
            server.config.reject_conn cc;
            Tnio.close cc
        }
        else
        do {
            server.curr_conns.val := server.curr_conns.val + 1;

            (* Pass it to the worker pool -- asynchronously.   The thread will
               not wait for the result, as the latter is communicated directly
               to the network client. NB: The acceptor thread object is passed
               to the worker thread, in order to transfer the "cc" socket from
               the former to the latter:
            *)
            ignore
            (Gen_server.call_server_async
                server.worker_pool
                (cc,  server.acceptor, client_addr, server.curr_conns)
                (Unix.gettimeofday () +. server.config.proc_timeout)
            )
        }
    with
    [hmm ->
    do{
        (* Tolerate acceptor errors: *)
        server.config.log_error hmm
    }
    ];
    (* Recursive invocation: *)
    acceptor_body_loop server
};

(*----------------------------*)
(*  "acceptor_body_startup":  *)
(*----------------------------*)
(*  Opens and binds the acceptor socket. After that, invokes a call-back
    (e.g., to lower the the privileges of the calling process), and then
    enters the "acceptor_body_loop":
*)
value acceptor_body_startup: server_t 'socket 'state -> unit =
fun server ->
    try
    do{
        (* Create the acceptor socket -- already within the thread:   *)
        server.acceptor_sock := Tnio.socket  Unix.PF_INET Unix.SOCK_STREAM;

        (* Bind the acceptor socket -- try for up to 4 minutes or so: *)
        let () = Tnio.bind_retry
                 server.acceptor_sock
                 (Unix.ADDR_INET
                    server.config.listen_on_ip
                    server.config.listen_on_port)
        in
        (* Create a listening queue on the acceptor socket: *)
        let () = Tnio.listen
                    server.acceptor_sock
                    server.config.listen_queue_len
        in
        (* Invoke the call-back:    *)
        let () = server.config.after_acceptor_bind () in

        (* Enter the acceptor loop: *)
        acceptor_body_loop server
    }
    with
    [hmm ->
     do {
        (* Close the "acceptor_sock" and propagate the exception: *)
        Tnio.close server.acceptor_sock;
        server.acceptor_sock := Tnio.invalid_tsock;
        raise hmm
    }];

(*======================*)
(*  Server Management:  *)
(*======================*)
(*----------------------*)
(*  "shutdown_server":  *)
(*----------------------*)
(* The same shutdown modes as in "Gen_server" are used: *)

value shutdown_server: server_t 'socket 'state  ->
                       Gen_server.shutdown_mode -> unit =
fun server mode ->
do {
    (* Cancel the acceptor thread  and close the acceptor socket: *)
    Tnio.thread_kill server.acceptor;
    Tnio.close       server.acceptor_sock;
    server.acceptor_sock := Tnio.invalid_tsock;

    (* Shutdown the worker threads according to the "mode" given: *)
    Gen_server.shutdown_server server.worker_pool mode
};

(*--------------------*)
(*  "create_server":  *)
(*--------------------*)
(*  The server is created, and starts the acceptor thread: *)

value create_server: server_config 'socket 'state ->
                     server_t      'socket 'state =
fun conf ->
    (* Create the processing function to be invoked by the worker threads.
       The type of this function must conform to the "Gen_server" spec. It
       also makes sure there are no socket leaks in any circumstances:
    *)
    let proc_conn   =
        fun (rsocket, acceptor_thread, client_addr, conns_count)
            state  deadline ->

            (* "rsocket" is the "raw" socket from the acceptor, not yet used.
               FIRST OF ALL, we must transfer it to the current thread, or
               an error will ensure:
            *)
            let ()     = Tnio.pass_tsock rsocket acceptor_thread in
            let state' =
            try
                (* Initialise the socket (e.g. SSL): *)
                let socket = conf.init_socket rsocket     state deadline in
                let state' =
                    (* The actual user-level processing function:    *)
                    try conf.proc_func socket client_addr state deadline
                    with
                    [hmm ->
                    do {
                        (* Always make sure the socket is finalised: *)
                        conf.finalise_socket socket deadline;
                        raise hmm
                    }]
                in do {
                    (* Normal completion: *)
                    conf.finalise_socket socket deadline;
                    state'
                }
            with
            [hmm ->
             do {
                conf.log_error hmm;
                (* State change on error again depends on the policy: *)
                if   conf.reset_state_on_error
                then conf.init_worker_state ()
                else state
            }]
            in
            do {
                (* HERE is the counter of open connections decremented.
                   The socket  may have already been implicitly closed
                   by "finalise", but we never know:
                *)
                Tnio.close rsocket;
                conns_count.val := conns_count.val - 1;
                ((), state')
            }
    in
    (* Create a config for the worker pool: *)
    let workers_conf=
    {
        Gen_server.max_all_threads      = conf.max_connections +
                                          conf.max_idle_threads;
        Gen_server.max_idle_threads     = conf.max_idle_threads;
        Gen_server.min_idle_threads     = conf.min_idle_threads;
        Gen_server.init_threads         = conf.init_threads;
        Gen_server.worker_priority      = conf.worker_priority;
        Gen_server.worker_stack_k       = conf.worker_stack_k;

        (* The maximum length of the queue of accepted but yet unporocessed
           connections cannot, obviously,  exceed the over-all limit on the
           number of connections:
        *)
        Gen_server.max_queue_len        = conf.max_connections;
        Gen_server.init_worker_state    = conf.init_worker_state;
        Gen_server.cleanup_state        = conf.cleanup_state;
        Gen_server.reset_state_on_error = conf.reset_state_on_error;
        Gen_server.proc_func            = proc_conn
    }
    in
    (* "acceptor" and "server" are mutually recursive, so create the latter
       with a placeholder acceptor thraed and socket first.
       NB:  But why is it actually required  to have an "acceptor" field in
       the "server" object,  thus creating  a mutually-recursive dependency
       between them? -- For "shutdown_server" only!
       Also NB: for the "acceptor_sock", we currently don't specify (probab-
       ly not necessary) the "kern_buff_size":
    *)
    let server =
    {
        config        = conf;
        acceptor_sock = Tnio.invalid_tsock;
        acceptor      = Tnio.invalid_thread;
        worker_pool   = Gen_server.create_server workers_conf;
        curr_conns    = ref 0
    }
    in
    (* Now actually create the acceptor thread and attach it to the
       "server" object:
    *)
    let acceptor_thr=
        Tnio.thread_create
            ~stack_size_k:conf.acceptor_stack_k
            ~priority    :conf.acceptor_priority
            (fun () -> acceptor_body_startup server)
    in
    do {
        server.acceptor := acceptor_thr;
        (* It is possible that the acceptor thread may have already
           may have already terminated at this point, but its ID is
           still valid in any case:
        *)
        server
    };

(*----------------------*)
(*  "get_server_info":  *)
(*----------------------*)
value get_server_info: server_t    'socket 'state ->
                       server_info 'socket 'state =
fun server ->
    let workers_info = Gen_server.get_server_info server.worker_pool in
    {
        (* The Acceptor is included in the total threads count, but not
           into the idle count:
        *)
        server_config    = server.config;
        curr_connections = server.curr_conns.val;
        curr_all_threads = workers_info.Gen_server.curr_all_threads + 1;
        curr_idle_threads= workers_info.Gen_server.curr_idle_threads
    };


