(*pp camlp4r *)
(* vim:ts=4:syntax=ocaml
*)
(*===========================================================================*)
(*                              "dns_client.mli":                            *)
(*                    Interface  to the  DNS Client in OCaml                 *)
(*                    (C) Explar Technologies Ltd, 2003-2004                 *)
(*===========================================================================*)
(* Exceptions which can be raised by functions of this  module: *)

exception Negative_Result of string;    (* E.g. non-existent Domain Name    *)
exception DNS_Error       of string;    (* Other error (with Error Message) *)

(* The abstract type of DNS Agent: *)
type dns_agent = 'a;

(* The types of DNS requests: only a sub-set of possible types is provided by
   this module:
*)
type dns_type =
[   T_A
  | T_NS
  | T_CNAME
  | T_PTR
  | T_MX
];

type ip_or_domain =
[   IP  of Unix.inet_addr
  | Dom of string
];

(* The result of DNS enquiries: IP addresses or domain names: *)
type dns_enquiry_res =
[   IPs  of list Unix.inet_addr
  | Doms of list string
];

(* "set_debug_stream": *)
(* Sets an optional channel for printing debugging information.  If set to
   None (as initially), no debugging information is printed:
*)
value set_debug_stream: dns_agent -> option out_channel -> unit;


(* "mk_dns_agent": *)
(* Uses the "dns_agent_config" record as an argument. The "dns_agent_config"
   fields are mutable for the ease of management:
*)
type dns_agent_config =
{
    (* Time-out on individual server enquiries: *)
    conf_timeout_sec   : mutable float;

    (* Number of re-tries on failed server requests, >= 0: *)
    conf_n_retries     : mutable int;

    (* Maximum depth of sub-enquiries (e.g. 50) *)
    conf_max_depth     : mutable int;

    (* Maximum Cache size: *)
    conf_cache_size    : mutable int;

    (* UDP socket kenel buffer size: *)
    conf_sock_buff_size: mutable int;

    (* The DNS Servers to be used.  Normally, this list should be empty, as
       the module will automatically iterate over the DNS servers,  staring
       from the Root ones. However, if we are behind a firewall  which dis-
       allows UDP traffic, specify  LOCAL  DNS servers here,  which will be
       used in the recursive mode. The values here are dotted IPs:
    *)
    conf_dns_servers   : mutable list string
};

value mk_dns_agent: dns_agent_config -> dns_agent;

(* "default_dns_agent_config":  *)
(* some reasonable parms settings.
   NB: this is a function, not a constant value, as its fields are mutable,
   and we must create a fresh copy of it every time:
*)
value default_dns_agent_config: unit -> dns_agent_config;

(* "config_of_dns_agent": *)
(* Reads the current configuration from a "live" DNS Agent: *)
value config_of_dns_agent:   dns_agent -> dns_agent_config;

(* "reconfigure_dns_agent":  *)
(* Changes the static config fields in a given DNS Agent to new values given
   by "config":
*)
value reconfigure_dns_agent: dns_agent -> dns_agent_config -> unit;

(* "close_dns_agent": *)
(* Must be used to close the agent's UDP socket and free other resources: *)
value close_dns_agent    : dns_agent -> unit;


(* "dns_enquiry": *)
(* Iterative DNS enquiry, not relying on any particular DNS server.
   Arg1: DNS agent;
   Arg2: the request type;
   Arg3: the domain name or IP addr which is the subject of query;
         if IP addr, the request type must be T_PTR:
*)
value dns_enquiry : dns_agent -> dns_type -> ip_or_domain -> dns_enquiry_res;


(* "init_mx_ips": *)
(* "more_mx_ips": *)
(* An all-in-one enquiry for the IPs of Mail Exchanges for a given domain;
   "init_mx_ips" always returns a non-[] list of IPs  and  a Continiation
   Object, or raises an exception;
   "more_mx_ips" modifies the Continuation Object,  and returns IPs which
   were NOT returned before, or [] (when there are no new IPs):
*)
type  mx_ips_cont = 'a;

value init_mx_ips: dns_agent -> string -> ((list Unix.inet_addr)*mx_ips_cont);
value more_mx_ips: dns_agent -> mx_ips_cont ->   list Unix.inet_addr;

(* "all_mx_ips": *)
(* In contrast to "init_mx_ips"/"more_mx_ips",   this function returns ALL IPs
   of Mail Exchanges for a given domain, in random order, or raises an exceptn
   if no IPs were found. Thus, if no exception is raised, the result is always
   non-[]:
*)
value all_mx_ips : dns_agent -> string -> list Unix.inet_addr;

(*  "get_dns_stats":  *)
(*  Returns statistics for using a given Agent since it was created: *)
type dns_stats =
{
    (* Request stats. Average values are wrt "total_enquiries": *)
    total_enquiries     : int;
    succ_enquiries      : int;
    av_wasted_sub_reqs  : float;
    av_resp_time_sec    : float;

    (* Errors distribution:  *)
    errors              : Hashtbl.t string int;
    (* DNS cache statistics: *)
    cache_stats         : Cache_simple.cache_stats
};

value get_dns_stats     : dns_agent -> dns_stats;

