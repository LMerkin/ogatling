// vim:ts=4:syntax=c
//===========================================================================//
//                              "tnio_mwd.c":                                //
//                                                                           //
//             Portable Timed Network I/O implementation using               //
//                 User-Level Threads ("Mad Whistle Dance")                  //
//                                                                           //
//                 (C) Explar Technologies Ltd,  2003--2006                  //
//===========================================================================//
//=========================//
//  Module Configuration:  //
//=========================//
#ifdef __linux__
    //  Linux (the latest 2.6 kernel) has "select", "poll"  and "epoll":
    //  We assume that they are all present on our COMPILATION platform;
    //  at run time, the mechanism will be chosen according to the user
    //  configuration and the kernel version:
#   define HAVE_IO_EVENTS_SELECT
#   define HAVE_IO_EVENTS_POLL
#   define HAVE_IO_EVENTS_EPOLL
#   define HAVE_CONTEXT_POSIX

#else
#   error "This OS is not supported yet"
#endif

#include <assert.h>
#include <float.h>
#include <fcntl.h>
#include <errno.h>
#include <sched.h>
#include <time.h>
#include <stdlib.h>
#include <stdio.h>
#include <limits.h>
#include <signal.h>
#include <string.h>
#include <unistd.h>
#include <utime.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <sys/socket.h>
#include <sys/utsname.h>
#include <sys/ioctl.h>
#include <net/if.h>

//  Extra includes for the I/O Events mechanisms available:
//
#ifdef HAVE_IO_EVENTS_SELECT
//  Nothing extra for "select"...
#endif

#ifdef HAVE_IO_EVENTS_POLL
#include <sys/poll.h>
#endif

#ifdef HAVE_IO_EVENTS_EPOLL
#include <sys/epoll.h>
#endif

// Extra includes for the Context mechanisms available:
//
#ifdef HAVE_CONTEXT_POSIX
#include <ucontext.h>
#endif

//  Includes for the OCaml interface:
//
#include <caml/mlvalues.h>
#include <caml/fail.h>
#include <caml/callback.h>
#include <caml/memory.h>
#include <caml/alloc.h>
#include <caml/custom.h>
#include "stack.h"
#include "roots.h"
#include "unixsupport.h"
#include "socketaddr.h"
#include "caml_signals.h"
#include "printexc.h"

//-----------------------//
//  Global definitions:  //
//-----------------------//
#define MAX_TSOCKS  65536
#define MAX_THREADS 32768

// I/O event mask. This is a universal definition, useful for all I/O Events
// mechanisms:
typedef __uint32_t  event_mask_t;

// The Thread Control Block:
//
#define INIT_STATUS       0
#define RUNNABLE_STATUS   1
#define BLOCKED_STATUS    2
#define TERMINATED_STATUS 3

#ifdef  TNIO_DEBUG
#define TCB_MAGIC   4342612
// Little-endian {'T','C','B','\0'}
#endif

typedef struct
{
#   ifdef TNIO_DEBUG
    // Magic:
    int magic;
#   endif

    // Thread id, same as its position in the "all_threads" array:
    int id;

    // The OCaml closure which is the thread body is referenced here:
    value  body_closure;

    // Thread priority -- currently it can be any integer value,   larger
    // values correspond to higher priorities:
    int    priority;

    // The return context is stored here  when the thread  yields the CPU.
    // The initial context to start the thread is also stored here:
    // Multiple context implementations are supported, and can be selected
    // at run time:
#   ifdef HAVE_CONTEXT_POSIX
    ucontext_t posix_ctx;
#   endif

    // Thread status (as above):
    int    status;

    // Position of the thread in the Runnable queue (if it's runnable):
    int    rpos;

    // Now: a thread can be blocked for the following reasons:
    // (1) waiting for I/O event(s);
    // (2) sleeping (suspended itself for some time);
    // (3) suspended itself on a condition variable ;
    // (4) was suspended by another thread (via "Tnio.thread_stop").
    //
    // In case (1), the thread can only be resumed by the Scheduler when the
    // latter receives the required I/O event(s).
    // In case (2), the thread is resumed by the Scheduler when the sleeping
    // time elapses.
    // In case (3), the thread can be resumed by any other thread which sig-
    // nals that cond var, via "Tnio.thread_resume".
    // In case (4), the thread can also be resumed by any other thread,  not
    // necesserily the one which stopped it, via "Tnio.thread_resume".
    // The following flag indicates whether we are in cases (3) or (4), so
    // that another thread can resume the current thread:
    int external_resume_ok;

    // Currently, a thread is allowed to wait for I/O events on no more than
    // one socket (FD), although it can create and own more than one socket;
    // waiting on multiple sockets ("select"-like) may be implemented later,
    // but it is probably unnecessary,  as the user can instead create extra
    // threads, each blocking on one resp FD.  (And conversely, sockets must
    // not be shared between threads -- see the "ET" struct below).
    // The thread waits for EITHER  of the events specified by the following
    // mask.    The set of all FDs owned by the thread is also stored in the
    // TCB, so they can be properly  closed when the thread exits:
    fd_set       FDs;
    event_mask_t wait_for;

    // The absolute deadline for waiting, in seconds from the Epoch:
    double deadline;

    // Any errors encountered while waiting for the I/O (e.g. time-out);
    // rc==0 if no errors:
    int wrc;
    int werrno;

#   ifdef TNIO_DEBUG
    // Stack usage debugging info:
    char * stack_base;
    int    stack_size;
#   endif

    // Reference count for this TCB -- how many times it is referenced from
    // OCaml-leevel visible wrapper objects:
    int ref_count;

    // Some global data of the OCaml RTS must be saved on per-thread basis:
    char  *                     bottom_of_stack;
    unsigned long               last_retaddr;
    value *                     gc_regs;
    char  *                     exception_pointer;
    struct caml__roots_block *  local_roots;
}
TCB;

// I/O events and threads waiting for them. As each FD MUST be local to the
// thread in which it was created, only one thread can be suspended waiting
// for I/O on a given FD.  The actual socket on which waiting is performed,
// is given as the index of this structure in the "all_events" array below,
// and can be verified for debugging purposes:
typedef struct
{
    int          live;  // The resp FD is actually opened
    event_mask_t got_events;
    TCB *        waiting_thread;
#   ifdef TNIO_DEBUG
    int          ts;
#   endif
}
ET;

static int initialised = 0;

static ET  all_events [MAX_TSOCKS];

// The highest currently opened file descriptor:
static int max_fd    = -1;

// Abstract representation of events:
static int EVENT_IN  = 0;
static int EVENT_OUT = 0;
static int EVENT_ERR = 0;
static int EVENT_HUP = 0;

#ifdef HAVE_IO_EVENTS_SELECT
// Global data for the "select"-based I/O Events implementation:
// Data sets containing all currently registered sockets:
static fd_set in_fds;
static fd_set out_fds;
static fd_set exc_fds;

// Modifiable temporary data sets -- it's more efficient to declare
// them globally:
static fd_set in_tmp;
static fd_set out_tmp;
static fd_set exc_tmp;
#endif

#ifdef HAVE_IO_EVENTS_POLL
// Global data for the "poll"-based I/O Events implementation:
static struct pollfd  poll_buff [MAX_TSOCKS];
#endif

#ifdef HAVE_IO_EVENTS_EPOLL
// Global data for the "epoll"-based I/O Events implementation:
// The buffer for "epoll_wait":
static struct epoll_event epoll_buff [MAX_TSOCKS];

// The Master EPoll FD:
static int    epoll_fd   = -1;
#endif

// Pointers to the TCBs of all runnable threads. They can be organised into
// bands, each band containing threads of the same priority.    If there is 
// only one band, it may be organised into a heap to provide a single queue
// for multiple priorities:
typedef struct
{
    TCB** tcbs;
    int   n_runnable;
}
prio_band;

static prio_band * runnable_threads = NULL;
static int  n_bands  = 0;
static int  use_heap = 0;

// The current running thread:
static TCB * curr_thread = NULL;

// Ptrs to TCBs of all threads:
static TCB * all_threads  [MAX_THREADS];

// "last_thread"  is the maximum thread ID which exists currently, though
// some intermediate thread IDs may not exist;
// "live_threads" is the number of all existing threads except terminated:
static int last_thread   = -1;
static int live_threads  =  0;

// The notional TCB of the Scheduler itself. In fact, only the context and
// saved global RTS data are used from it:
static TCB sched_tcb;

// Operations on the OCaml Custom blocks encapsulating the TCB ptr:
static struct custom_operations tcb_ops;

// "/proc/loadavg" FD:
// This is only available on Linux:
#ifdef __linux__
static int load_fd = -1;
#endif

// EXTERNALLY-VISIBLE "value"s:

// Invalid Unix.file_descr -- unboxed value, so don't need to declare it to
// be a GC root:
value tnio_invalid_file_descr = Val_int (-1);

// Invalid deadline, to be used as a placeholder:
#define INVALID_DEADLINE  (- DBL_MAX)
value tnio_invalid_deadline   = Val_unit;

// Deadline for non-blocking operations:
#define ZERO_DEADLINE     0.0

// Deadline for unbounded blocking operations:
#define INFINITE_DEADLINE ((double) INT_MAX)

//-----------------------//
//  Raising exceptions:  //
//-----------------------//
static void terror (char* comment)
{
    uerror (comment, Nothing);
}

static void tnio_error (int errcode, char* comment)
{
    errno = errcode;
    unix_error  (errcode, comment, Nothing);
}

static void horror (char * msg)
{
    // This is a fatal error which results in termination of the  whole
    // program. STILL, use "exit(3)" rather than "_exit(2)", as "atexit"
    // handlers may really need to be invoked:
    fprintf (stderr, "FATAL ERROR: %s\n", msg);
    fflush  (stderr);
    exit    (255);
}

//----------------//
//  "CHECK_INIT": //
//----------------//
#define CHECK_INIT(where) \
    if  (! initialised)   \
        horror (where ": Tnio module not initialised");

//----------------//
//  "CHECK_TCB":  //
//----------------//
//  Checks some invariants:
//
#ifdef  TNIO_DEBUG
#define CHECK_TCB(tcb) \
{ \
    assert  ((tcb) != NULL); \
    assert  ((tcb)->magic == TCB_MAGIC); \
    int id = (tcb)->id; \
    assert (id >= 0); \
    assert (id <= last_thread); \
    assert (last_thread  >= 0 && last_thread  <  MAX_THREADS  ); \
    assert (live_threads >= 0 && live_threads <= last_thread+1); \
    assert (all_threads [id] == (tcb));  \
    if  ((tcb)->status == RUNNABLE_STATUS) \
    { \
        int rpos = (tcb)->rpos; \
        int b    = get_prio_band (tcb); \
        prio_band* band = runnable_threads + b; \
        assert (rpos >= 0); \
        assert (rpos < band->n_runnable); \
        assert (band->tcbs[rpos] == (tcb)); \
    } \
    if  ((tcb)->bottom_of_stack != NULL) \
    { \
        assert ((tcb)->bottom_of_stack >= (tcb)->stack_base); \
        assert ((tcb)->bottom_of_stack <= (tcb)->stack_base + \
                                          (tcb)->stack_size); \
    } \
}
#else
#define CHECK_TCB(tcb)
#endif

//---------------------//
//  "CHECK_RUNNABLE":  //
//---------------------//
#define CHECK_RUNNABLE(tcb) \
{ \
    CHECK_TCB (tcb) \
    assert    (tcb->status == RUNNABLE_STATUS); \
}

//---------------//
//  "CHECK_TS":  //
//---------------//
//  Check the validity of a socket descr. In particular, if "curr_thread"
//  is not NULL, "ts" must be in the FDs set of that thread.  This means:
//  passing socket descrs between threads (incl the main thread)  is NOT
//  allowed (unless done peroperly via "tnio_pass_socket").
//  Also note that "CHECK_TS" is always invoked from an active thread or
//  from th emain thread. In both cases the calling thread owns the socket
//  being checked, so there must be NO-ONE waiting on this socket:
//
#ifdef  TNIO_DEBUG
#define ASSERT_ET_TS(ts) assert (all_events[ts].ts == (ts));
#else
#define ASSERT_ET_TS(ts) // Always OK, as cannot check!
#endif

#define CHECK_TS(where, ts) \
    if  ((ts) < 0 || (ts) >= MAX_TSOCKS || (ts) > max_fd || \
        (! all_events[ts].live) || \
        (curr_thread != NULL && ! FD_ISSET(ts, &curr_thread->FDs)) || \
        (all_events[ts].waiting_thread != NULL) \
        ) \
        tnio_error (EBADF, where); \
    ASSERT_ET_TS (ts);

//--------------//
//  "TCB_PTR":  //
//--------------//
//  Gets an (lvalue) of the TCB ptr stored inside an OCaml custom block:
#define TCB_PTR(vtcb) \
    * ((TCB**) (Data_custom_val(vtcb)))

//  Maximum number of TCBs not yet processed by the GC  -- note that some
//  of these TCBs may still be needed, as the corresp threads would still
//  run in the background,   so put it at 10 times the reasonable average
//  number of threads we would create at a time:
//
#define MAX_UNCLAIMED_TCBS 10000

//--------------------//
//  "get_prio_band":  //
//--------------------//
//  The priority band of the current thread. The arg must be a valid ptr to
//  a real TCB (not checked, as this function itself is used inside "CHECK_
//  TCB"):
//
int get_prio_band (TCB * tcb)
{
    if  (use_heap)
        // There is only one queue shared by all priorities:
        return 0;

    // Otherwise: There are bands from 0 to (n_bands-1); the priority level
    // of the arg TCB MUST fall within them:
    int b = tcb->priority;
    assert (b >= 0);
    assert (b < n_bands);
    return  b;
}

//------------------//
//  "custom_wrap":  //
//------------------//
//  Wrap a TCB ptr into an OCaml Custom block.  NB: this function performs
//  allocation from the OCaml heap, so use it with care -- protect all the
//  needed values initialised before it, from the GC:
//
static value custom_wrap (TCB * tcb)
{
    // We would rather not have more than  MAX_UNCLAIMED_TCBS  of the ter-
    // minated threads, so set the GC parms in the "alloc_custom" accordingly:
    //
    value res = alloc_custom
                (& tcb_ops, sizeof(tcb), 1,
                (tcb==NULL) ? 1: MAX_UNCLAIMED_TCBS);

    if  (tcb != NULL)
    {
        // Increment the ref count, unless "tcb" is NULL ("invalid_thread"):
        tcb->ref_count ++;
        CHECK_TCB (tcb)
    }

    // Store  the "tcb" (a ptr by itself) in the block allocated:
    TCB_PTR   (res) = tcb;
    return     res;
}

//----------------------//
//  "close_owned_fds":  //
//----------------------//
//  Close all sockets (FDs) owned by a particular thread (normally before the
//  thread itself is finalised):
//
static void do_close (TCB * tcb, int ts, int withTCBcheck);

static void close_owned_fds (TCB *  tcb, int withTCBcheck)
{
    assert (tcb != NULL);

    // Since we can't iterate directly over an "fd_set", try all FDs up to
    // "max_fd", which may by itself be decremented in the process:
    int ts;
    for (ts = 0; ts <= max_fd;  ts ++)
        if (FD_ISSET (ts, &tcb->FDs))
            do_close (tcb, ts, withTCBcheck);
            // In particular, this removes "ts" from "tcb->FDs"
}

//-------------------//
//  "finalise_tcb":  //
//-------------------//
//  Invoked when a Custom wrapper block ("vtcb") around a TCB ptr is about to
//  be finalised;  this decrements the ref counter stored in the TCB itself.
//  The TCB itself is de-allocated if it is not referenced from anywhere any-
//  more, AND the corresp thread has already terminated:
//
static void remove_thread(TCB * tcb, int async);

static void finalise_tcb (value vtcb)
{
    TCB * tcb = TCB_PTR  (vtcb);

    // Don't CHECK_TCB, as it has already been removed from "all_threads", etc:
    // Furthermore, it's possible that the wrapper of a synthetic TCB is being
    // finalised:
    if  (tcb == NULL)
        return;

    // To make sure, re-set the TCB ptr in "vtcb" -- although the latter is go-
    // ing away anyway:
    TCB_PTR (vtcb) = NULL;

    // Decrement the ref count:
    tcb->ref_count --;

    if  (tcb->status == TERMINATED_STATUS && tcb->ref_count <= 0)
        // This call is completely asynchronous, so we run "remove_thread" in a
        // restricted version:
        remove_thread (tcb, 1);
}

//==========================//
//  Module Initialisation:  //
//==========================//
//-------------------//
//  "*scan_roots*":  //
//-------------------//
//  Implementation of this function is borrowed from
//      "otherlibs/systhreads/posix.c"
//  of the OCaml distribution. It is to be attached to the "scan_roots_hook"
//  of the OCaml RTS, in order to take the threads stacks into account while
//  computing the roots of reachible data by the GC.
//
static void (* orig_scan_roots_hook) (scanning_action) = NULL;

static void tnio_scan_roots   (scanning_action action)
{
    // For all registered threads except the "curr_thread" (if this function
    // is invoked from within a thread), scan their stacks. The "curr_thread"
    // is excluded because its stack would be scanned automatically:
    int i;
    for (i = 0; i <= last_thread;  i ++)
    {
        TCB* tcb = all_threads [i];

        if  (tcb == NULL || tcb == curr_thread  ||
             tcb->bottom_of_stack == NULL)
            continue;
            // The "bottom_of_stack" cond means: don't scan stacks of those
            // threads which have not run yet!

        CHECK_TCB (tcb)
        do_local_roots (action,  tcb->bottom_of_stack,  tcb->last_retaddr,
                        tcb->gc_regs, tcb->local_roots);
    }
    // Probably need to scan the stack of the original Main Thread as well,
    // if called from within a client thread:
    if  (curr_thread != NULL)
        // "sched_tcb" MUST already contain saved data -- XXX need assertion:
        do_local_roots (action, sched_tcb.bottom_of_stack,
                        sched_tcb.last_retaddr, sched_tcb.gc_regs,
                        sched_tcb.local_roots);

    // Also, invoke the original hook:
    if  ( orig_scan_roots_hook != NULL)
        (*orig_scan_roots_hook) (action);
}

//---------------------//
//  "get_active_tcb":  //
//---------------------//
//  Returns the TCB ptr of either the curr thread, or the scheduler:
//
static TCB * get_active_tcb ()
{
    TCB * tcb;
    if  (curr_thread != NULL)
    {
        tcb = curr_thread;
        CHECK_TCB (tcb)
    }
    else
        tcb = & sched_tcb;
    return tcb;
}

//--------------------//
//  "save_rts_data":  //
//--------------------//
//  Save the current global OCaml RTS data in the TCB of the current thread
//  or that of the Scheduler, before yieding the CPU.
//  Acts an an "enter_blocking_section" hook:
//
static void (* orig_enter_blocking_section_hook) () = NULL;

static void save_rts_data ()
{
    TCB * tcb = get_active_tcb ();

    tcb->bottom_of_stack     = caml_bottom_of_stack;
    tcb->last_retaddr        = caml_last_return_address;
    tcb->gc_regs             = caml_gc_regs;
    tcb->exception_pointer   = caml_exception_pointer;
    tcb->local_roots         = local_roots;
}

//-----------------------//
//  "restore_rts_data":  //
//-----------------------//
//  Restore the global OCaml RTS data, just upon re-gaining the CPU.
//  Acts as a "leave_blocking_section" hook:
//
static void (* orig_leave_blocking_section_hook) () = NULL;

static void restore_rts_data ()
{
    TCB * tcb = get_active_tcb ();

    caml_bottom_of_stack     = tcb->bottom_of_stack;
    caml_last_return_address = tcb->last_retaddr;
    caml_gc_regs             = tcb->gc_regs;
    caml_exception_pointer   = tcb->exception_pointer;
    local_roots              = tcb->local_roots;
}

//---------------------------//
//  "init_master_epoll_fd":  //
//---------------------------//
//  For EPoll mechanism under Linux: Do this at "tnio_init" and
//  "tnio_detach_child":
//
#ifdef __linux__
#ifdef HAVE_IO_EVENTS_EPOLL
static void init_master_epoll_fd ()
{
    epoll_fd = epoll_create (MAX_TSOCKS);
    if (epoll_fd < 0)
        horror ("Tnio.init: epoll_create");

    int flags = fcntl (epoll_fd, F_GETFD, 0);
    if (flags == -1 || fcntl (epoll_fd, F_SETFD, flags | FD_CLOEXEC) == -1)
        horror ("Tnio.init: EPoll FD: fcntl-FD");
}
#endif
#endif

//--------------//
// "tnio_init": //
//--------------//
// IMPORTANT: The following values must correspond to the OCaml declarations
// of the "IO_Events_Impl" and "Context_Impl" types:
//
#define USE_IO_EVENTS_DEFAULT 0
#define USE_IO_EVENTS_SELECT  1
#define USE_IO_EVENTS_POLL    2
#define USE_IO_EVENTS_EPOLL   3

// The following are the actual index for the IO Events mech chosen according
// to user's selection and the availability. It is numbered from 0, and equal
// to the corresp values above minus 1:
static int IOEM = 0;

value tnio_init
    (value events_mech, value rt_priority,
     value n_bands_val, value use_heap_val)
{
    if  (initialised)
        // The module is already initialised -- don't do anything, EVEN IF
        // the config parms have changed:
        return Val_unit;

    int i;
    int flags;

    // Initialise the priority bands. The correct settings for "n_bands" and
    // "use_heap" must already be verified by the OCaml caller:
    n_bands  = Int_val  (n_bands_val );
    use_heap = Bool_val (use_heap_val);

    if  (use_heap)
        assert (n_bands == 1);
    else
        assert (n_bands >= 1);

    // Create all bands -- their internal structure (heap or random array) is
    // not created at this point -- that's done dynamically:
    runnable_threads = calloc (n_bands, sizeof(prio_band));

    for (i = 0; i < n_bands; i ++)
    {
        // Initialise the "i"th band. The "tcbs" array is created for the max
        // possible number of threads:
        prio_band * band = runnable_threads + i;
        band->n_runnable = 0;
        band->tcbs       = calloc (MAX_THREADS, sizeof(TCB *));
    }

    // Initialise "all_events":
    for (i = 0; i < MAX_TSOCKS;  i ++)
    {
        ET * et = all_events + i;
        et->live           = 0;
        et->got_events     = 0;
        et->waiting_thread = NULL;
#       ifdef TNIO_DEBUG
        et->ts             = i;
#       endif
    }

    // Initialise the hooks:
    orig_scan_roots_hook   = scan_roots_hook;
    scan_roots_hook        = tnio_scan_roots;

    orig_enter_blocking_section_hook = enter_blocking_section_hook;
    enter_blocking_section_hook      = save_rts_data;

    orig_leave_blocking_section_hook = leave_blocking_section_hook;
    leave_blocking_section_hook      = restore_rts_data;

    // Which IO Events mechanism will be used?
    int ioem = Int_val (events_mech);

#   ifdef __linux__
    switch (ioem)
    {
        case USE_IO_EVENTS_DEFAULT:
        {
            // The user has requested the Default mechanism. On Linux,  it's
            // "epoll" if the kernel version is 2.6 or above, otherwise it's
            // "poll":
            struct utsname un;
            uname  (& un);

            assert (un.release[1] == '.' && un.release[3] == '.');
            int major  = un.release[0];
            int middle = un.release[2];

            if (major >= 2 && middle >= 6)
                IOEM   = USE_IO_EVENTS_EPOLL;
            else
                IOEM   = USE_IO_EVENTS_POLL ;
            break;
        }

        case USE_IO_EVENTS_SELECT:
        case USE_IO_EVENTS_POLL  :
        case USE_IO_EVENTS_EPOLL :
            IOEM = ioem;
            break;

        default:
            horror ("Tnio.init: Invalid I/O Events mechanism specified");
    }
#   else
#   error "This OS is not supported yet"
#   endif

    // Initialise the chosen mechanism. Protect unavailable options by
    // conditional compilation, in a platform-independent way:
    switch (IOEM)
    {
        case USE_IO_EVENTS_SELECT:
#           ifdef HAVE_IO_EVENTS_SELECT
            // "select"-based implementation: Zero-out all event sets:
            FD_ZERO (& in_fds );
            FD_ZERO (& out_fds);
            FD_ZERO (& exc_fds);

            // Define the values of abstract events. In this case, they
            // are chosen by us, as "select" itself does not have them:
            EVENT_IN  = 1;
            EVENT_OUT = 2;
            EVENT_ERR = 4;
            EVENT_HUP = 0;  // Not used
#           else
            horror ("Tnio.init: Select I/O Events mechanism is not available");
#           endif
            break;

        case USE_IO_EVENTS_POLL:
#           ifdef HAVE_IO_EVENTS_POLL
            // "poll"-based implementation:
            // Just re-set the events buffer before it used:
            for (i = 0; i < MAX_TSOCKS; i ++)
            {
                poll_buff[i].fd     = -1;
                poll_buff[i].events =  0;
                poll_buff[i].revents=  0;
            }
            //  Abstract representation of events:
            EVENT_IN  = (POLLIN | POLLPRI);
            EVENT_OUT = POLLOUT;
            EVENT_ERR = POLLERR;
            EVENT_HUP = POLLHUP;
#           else
            horror ("Tnio.init: Poll I/O Events mechanism is not available");
#           endif
            break;

        case USE_IO_EVENTS_EPOLL:
#           ifdef HAVE_IO_EVENTS_EPOLL
            // "epoll"-based implementation:
            //
            if  (epoll_fd != -1)
                // Already initialised:
                return Val_unit;

            // Create the Master EPoll FD, with "close-on-exec" flag set:
            init_master_epoll_fd ();
                    
            //  Abstract representation of events:
            EVENT_IN  = EPOLLIN;
            EVENT_OUT = EPOLLOUT;
            EVENT_ERR = EPOLLERR;
            EVENT_HUP = EPOLLHUP;
#           else
            horror ("Tnio.init: EPoll I/O Events mechanism is not available");
#           endif
            break;

        default:
            assert (0);
    }

#   ifdef __linux__
    // Open "/proc/loadavg" -- required for CPU utilisation info, also in
    // "close-on-exec" mode -- Linux ONLY:
    //
    load_fd = open ("/proc/loadavg", O_RDONLY);
    if  (load_fd < 0)
        horror ("Tnio.init: open /proc/loadavg");

    flags = fcntl (load_fd, F_GETFD, 0);
    if (flags == -1 ||
        fcntl  (load_fd, F_SETFD, flags | FD_CLOEXEC) == -1)
        horror ("Tnio.init: LoadAvg FD: fcntl-FD");
#   endif

    // Also, try to put the whole process into the highest priority level,
    // if requested.    This is only possible in POSIX systems, and if we
    // have EUID=0:
    if  (Bool_val (rt_priority))
    {
        struct sched_param sp;
        sp.sched_priority = sched_get_priority_max (SCHED_FIFO);
        if  (sched_setscheduler (getpid (), SCHED_FIFO, & sp) < 0 ||
            (nice (-20), errno) != 0)
        {
            // Produce only a warning, not an error:
            fprintf (stderr, "WARNING: Tnio.init: Cannot set the maximum "
                             "priority level:\n%s\n",  strerror (errno));
            fflush  (stderr);
        }
    }

    // Also, set up the default next deadline for the C API.  The value set is
    // deliberately invalid, so the clients would not forget setting their own
    // proper deadlines:
    sched_tcb.deadline = INVALID_DEADLINE;

    // Also, initialise the "tnio_invalid_deadline", and make it a global root:
    tnio_invalid_deadline = copy_double (INVALID_DEADLINE);
    register_global_root (& tnio_invalid_deadline);

    // Also, initialise "tcb_ops":
    // NB: The only finalisation operation are provided for this custom block,
    //     not even comparison for equality;  for the latter, Thread IDs have
    //     to be compared:
    tcb_ops.identifier = NULL;
    tcb_ops.finalize   = finalise_tcb;
    tcb_ops.compare    = custom_compare_default;
    tcb_ops.hash       = custom_hash_default;
    tcb_ops.serialize  = custom_serialize_default;
    tcb_ops.deserialize= custom_deserialize_default;

    //  We are now initialised:
    initialised = 1;
    return Val_unit;
}

//----------------------//
// "close_all_sockets": //
//----------------------//
// Sockets are system-wide resources, so they must be properly closed when a
// thread terminates,   or when the whole process is about to be terminated.
// (In the latter case, one can think they will be closed automatically, but
// in Linux at least,  there is a  problem when a server socket is caught in
// the FIN_WAIT1 state when a process terminates --  the corresp port cannot
// be re-used by another process even after the TIME_WAIT period!).
// So:
// The following function  MUST  be invoked when the process is being exited
// by catching a signal (but unfortunately, SIGKILL cannot be caught,  so in
// that case, this mechanism would not help), or via "exit":
//
static void close_all_sockets ()
{
    int ts;
    for (ts = 0; ts <= max_fd; ts ++)
        close (ts);
#   ifdef HAVE_IO_EVENTS_EPOLL
    close (epoll_fd);
#   endif
}

//---------------------//
// "tnio_exit_setup":  //
//---------------------//
value tnio_exit_setup (value unit)
{
    // Install "close_all_sockets" for "exit(3)". XXX:  it will STILL not help
    // for _exit(2); and for exit(3),    it might be unnecessary if the latter
    // EXPLICITLY closes all FDs belonging to the process (rather than relying
    // on th ekernel to do that).     XXX: we ignore the situation if "atexit"
    // fails:
    atexit (close_all_sockets);

    // Now a more complex task:  invoke "close_all_sockets" if we about to be
    // terminated by a signal. The problem is that we don't know whether a sig-
    // nal is deadly or not, since its handler can be modified at any time in
    // the future.
    // So (still a bad way -- and does not help with SIGKILL):
    // ignore all signals, and require the user to write explicit handlers (to
    // use exit(3)) if they want to re-install terminating behaviour of any
    // signal. This function is invoked automatically at start-up (even before
    // "tnio_init"); so if any signal handlers were set before that, they will
    // NOT work -- they will be over-written.
    // NB: This does not apply to the debugging mode, in which we need e.g. to
    // be terminated by a signal and get the core:
#   ifndef TNIO_DEBUG
    int s;
    for (s = 0; s <= SIGRTMAX; s++)
        if  (s != SIGSTOP && s != SIGKILL)
        signal (s, SIG_IGN);
#   endif
    return Val_unit;
}

//-------------------//
//  "tnio_cleanup":  //
//-------------------//
//  Called from the Threads Scheduler just before it returns to OCaml.  The
//  module remains initialised and ready for more Scheduler invocations (in
//  particular, the Master EPoll FD is NOT closed if "epoll" is used,   but
//  all sockets owned by all threads ARE closed):
//
static void tnio_cleanup ()
{
    if  (IOEM == USE_IO_EVENTS_SELECT)
    {
#       ifdef HAVE_IO_EVENTS_SELECT
        // "select"-based implementation: zero out the sets:
        FD_ZERO (&  in_fds);
        FD_ZERO (& out_fds);
        FD_ZERO (& exc_fds);
#       else
        assert  (0);
#       endif
    }

    int i;

    // Clean-up "all_events" (but keep their "ts" self-indices, if present!):
    //
    for (i = 0; i < MAX_TSOCKS;  i ++)
    {
        ET * et = all_events + i;
        et->live           = 0;
        et->got_events     = 0;
        et->waiting_thread = NULL;

        // Implementation-specific clean-up actions:
        switch (IOEM)
        {
            case USE_IO_EVENTS_SELECT:
                // Nothing more to do:
#               ifndef HAVE_IO_EVENTS_SELECT
                assert (0);
#               endif
                break;

            case USE_IO_EVENTS_POLL:
#               ifdef HAVE_IO_EVENTS_POLL
                // "poll"-based implementation:
                poll_buff[i].fd      =-1;
                poll_buff[i].events  = 0;
                poll_buff[i].revents = 0;
#               else
                assert (0);
#               endif
                break;

            case USE_IO_EVENTS_EPOLL:
#               ifdef HAVE_IO_EVENTS_EPOLL
                // "epoll"-based implementation:
                epoll_buff[i].events  = 0;
                epoll_buff[i].data.fd =-1;
#               else
                assert (0);
#               endif
                break;

            default:
                assert (0);
        }
    }

    // Clean-up "runnable_threads":
    //
    for (i = 0; i < n_bands; i ++)
    {
        int nr = runnable_threads[i].n_runnable;
        if  (nr != 0)
        {   // This MUST NOT happen:
            fprintf (stderr,
                    "Tnio.cleanup: WARNING: Still %d runnable threads "
                    "in band %d\n", nr, i);
            fflush  (stderr);
            runnable_threads[i].n_runnable = 0;
        }
        int   j;
        TCB** tcbs = runnable_threads[i].tcbs;
        
        // Just to be sure...
        for (j = 0; j < MAX_THREADS; j++)
            tcbs[j] = NULL;
    }

    // Clean-up "all_threads". Note that this function is called upon exit
    // from the Scheduler, when all threads have terminated,  but they may
    // still have non-NULL TCBs:
    //
    for (i = 0; i < MAX_THREADS; i ++)
    {
        if  (all_threads [i]== NULL)
            continue;

        TCB * tcb = all_threads [i];

        fprintf (stderr,
                "Tnio.cleanup: WARNING: Non-NULL thread entry %d: TCB=%p\n",
                i, tcb);
        fflush  (stderr);

        // Force clean-up:
        remove_thread (tcb, 0);
    }
    last_thread  = -1;
    live_threads =  0;
    curr_thread  = NULL;
}

//------------------------//
//  "tnio_detach_child":  //
//------------------------//
//  If "tnio_init"  was invoked before a process "fork"ed, both the parent and
//  the child may share the TNIO internals.  E.g., if the "epoll" mechanism is
//  used, the Master Socket will be used by both processes,  which can lead to
//  unpredictable results. The following function must be called by EACH CHILD
//  after the "fork", to detach its TNIO RTS from that of the parent:
//
value tnio_detach_child (value unit)
{
#   ifdef __linux__
#   ifdef HAVE_IO_EVENTS_EPOLL
    // Open a new Master EPoll Socket:
    init_master_epoll_fd ();
#   else
    // Currently nothing to do!..
#   endif
#   else
#   error "This OS is not supported yet"
#   endif
    // In any case, it's probably better to shut down all existing threads and
    // clear the pending events:
    tnio_cleanup ();

    return Val_unit;
}

//-----------------------//
//  "get_socket_error":  //
//-----------------------//
//  Returns the "errno" associated with the latest socket error: //
//
static int get_socket_error (int ts)
{
    int      status;
    unsigned len = sizeof (status);

    if  (getsockopt  (ts, SOL_SOCKET, SO_ERROR, (void*) &status, &len) != 0)
        terror ("Tnio.get_socket_error: getsockopt");

    return status;
}

//==============================//
//  Priority Queue Management:  //
//==============================//
//  Used for priority-based threads scheduling.  Larger priority values corresp
//  to higher thread priorities; the "runnable_threads" array is then organised
//  into a heap with the highest-priority thread at 0:

//--------------------//
//  Index Arithmetic: //
//--------------------//
#define PARENT_INDEX(curr) (((curr)+1)/2 - 1)
#define CHILD1_INDEX(curr) (((curr)+1)*2 - 1)
#define CHILD2_INDEX(curr)  ((curr)+1)*2

// Heap Invariant:
// item->priority >= child1->priority &&
// item->priority >= child2->priority

//--------------------//
//  "prio_q_insert":  //
//--------------------//
//  Insert the "item" into its proper position in the "runnable_threads[0]"
//  band, as if this function is used, there is only a single queue of run-
//  nable TCBs:
//
static void prio_q_insert (TCB * item)
{
    // DON'T CHECK_TCB (item), as initally "rpos"es may not be set correctly!

    // First, put the "item" at the end of the array:
    assert (n_bands == 1);
    prio_band  * band = runnable_threads + 0;

    assert (band->n_runnable < MAX_THREADS);
    assert (band->tcbs != NULL);

    item->rpos = band-> n_runnable;
    band->tcbs  [item->rpos] = item;
    band->n_runnable ++;

    // Move the "item" into the correct position -- shift it UP the tree as
    // far as possible,  while it's still larger than the parent,  or until
    // the root is reached:
    //
    int   curr      = item->rpos;
    int   item_prio = item->priority;

    while (curr!= 0)
    {
        int   parent = PARENT_INDEX (curr);
        TCB * pitem  = band->tcbs [parent];
        CHECK_TCB      (pitem)

        if  (pitem->priority >= item_prio)
            break;

        // Otherwise, swap the items.  DON'T FORGET to re-adjust "rpos"es!
        band->tcbs [parent] = item;
        band->tcbs [curr]   = pitem;

        item ->rpos = parent;
        pitem->rpos = curr;
        curr        = parent;

        // Still, "curr" is the index of the "item" in the queue:
        CHECK_TCB (item)
        CHECK_TCB (pitem)
    }
}

//-----------------------//
//  "prio_q_delete_at":  //
//-----------------------//
//  Remove the given item (not necesserily the head!) from the "runnable_
//  threads[0]" heap:
//
static void prio_q_delete_at (int at)
{
    assert (at >= 0);
    assert (n_bands == 1);
    prio_band  * band = runnable_threads + 0;

    assert (band->n_runnable < MAX_THREADS);
    assert (band->tcbs != NULL);
    assert (at  < band->n_runnable);

    // If this is the last item anyway, just truncate the queue:
    if  (at == band->n_runnable-1)
    {
        band->n_runnable --;
        return;
    }
    // OTHERWISE:
    // Move the last item to the "at" position, and truncate the length:
    //
    TCB * item = band->tcbs [band->n_runnable-1];
    CHECK_TCB    (item) 

    band->n_runnable --;
    int   item_prio  = item->priority;

    band->tcbs  [at] = item;
    item->rpos = at;

    int curr   = at;

    // The "item"'s priority can be larger or smaller than that of its parent
    // (if exists). In the former case, we need to move UP the tree:
    //
    int moved_up = 0;

    while (curr != 0)
    {
        // If we got here, the parent exists:

        int   parent = PARENT_INDEX (curr);
        TCB *  pitem = band->tcbs   [parent];
        CHECK_TCB      (pitem)

        if  (item_prio <= pitem->priority)
        {
            // The "item"  is already in a correct position wrt to the parent.
            // If we've actually moved at least one step up, then "item"s pri-
            // ority is greater than that of the last swapped node,  and thus
            // greater than that of the original "at" children -- all done!
            // Otherwise, we may still need to move DOWN the tree:
            if  (moved_up)
                return;
            else
                break;
        }
        // OTHERWISE:
        // The "item" appears to have larger priority than its parent, so move
        // it UP the tree -- REMEMBER "rpos"es:
        //
        band->tcbs [parent] = item;
        band->tcbs [curr]   = pitem;

        item ->rpos = parent;
        pitem->rpos = curr;

        CHECK_TCB (item)
        CHECK_TCB (pitem)

        curr     = parent;
        moved_up = 1;
    }
    // If we got here, the "item" was correctly positioned wrt its parent
    // (or there was no parent at all), -- now need to check its position
    // wrt its children:
    assert (! moved_up);

    // Now shift the "item"  DOWN the tree along the path of HIGHER-priority
    // children, so that the highest of two children will always come on top,
    // and the other one and the moving "item" come below it:
    while (1)
    {
        int ch1 = CHILD1_INDEX (curr);
        int ch2 = CHILD2_INDEX (curr);
        assert (ch2 == ch1+1);

        int   ch_hi;
        TCB * item_hi;

        if  (ch2 < band->n_runnable)
        {
            // "ch2" really exists, and thus "ch1" as well. Find the highest
            // priority of the two children:
            TCB * item1 = band->tcbs [ch1];
            TCB * item2 = band->tcbs [ch2];

            CHECK_TCB (item1)
            CHECK_TCB (item2)

            if  (item1->priority > item2->priority)
            {
                ch_hi   = ch1;
                item_hi = item1;
            }
            else
            {
                ch_hi   = ch2;
                item_hi = item2;
            }
        }
        else
        if  (ch1 < band->n_runnable)
        {
            // "ch2" does not exist, but "ch1" still does. NB: the inverse
            // is not possible, as the tree is built from the left:
            ch_hi   = ch1;
            item_hi = band->tcbs [ch_hi];
            CHECK_TCB (item_hi)
        }
        else
            // No children at all for the "curr" node -- exit:
            break;

        // Compare the highest-priority child with the "item":
        //
        if  (item_prio < item_hi->priority)
        {
            // "item" has a lower priority than necessary to remain on top,
            // so swap it with "item_hi" -- REMEMBER "rpos"es:
            //
            band->tcbs [ch_hi] = item;
            band->tcbs [curr]  = item_hi;

            item   ->rpos  = ch_hi;
            item_hi->rpos  = curr;

            CHECK_TCB (item)
            CHECK_TCB (item_hi)
            
            curr = ch_hi;
        }
        else
            // The "item" is already properly positioned:
            break;
    }
}


//======================//
//  Thread Operations:  //
//======================//
//--------------------//
//  "from_runnable":  //
//--------------------//
//  Remove the given thread from the runnable queue, either when it gets
//  blocked, or when it terminates:
//
static void from_runnable (TCB * tcb)
{
    CHECK_TCB (tcb)
    assert    (tcb->status == RUNNABLE_STATUS  ||
               tcb->status == TERMINATED_STATUS);

    int rpos = tcb->rpos;
    // XXX: Could it be that this function is invoked several times on a
    // thread which had terminated? If there is no valid "rpos", do
    // nothing:
    if (rpos == INT_MIN)
        return;

    // Remove this "tcb" from the "band->tcbs".  The latter is either an
    // unsorted array of same-priority TCB ptrs (use_heap==0), or a heap
    // of mixed-priority priority ones (use_heap==1):
    //
    if  (use_heap)
        // Delete the entry from the heap:
        prio_q_delete_at (rpos);
    else
    {
        prio_band * band = runnable_threads + get_prio_band (tcb);
        assert     (band->tcbs       != NULL);
        assert     (band->n_runnable >= 1);
        // At least the given "tcb" was still runnable...

        if (rpos != band->n_runnable-1)
        {
            // Just "close the hole" by swapping the last runnable thread
            // and the given "tcb", which will no longer be runnable:
            band->tcbs [rpos]       = band->tcbs [band->n_runnable-1];
            band->tcbs [rpos]->rpos = rpos;
        }
        band->n_runnable --;
    }
    // In any case, re-set "rpos" in the removed TCB:
    tcb->rpos  = INT_MIN;
}

//-----------------//
//  "to_runnable": //
//-----------------//
static void to_runnable (TCB * tcb)
{
    CHECK_TCB (tcb)
    assert (tcb->status == INIT_STATUS || tcb->status == BLOCKED_STATUS);

    // Mark the thread as runnable:
    tcb->status     = RUNNABLE_STATUS;
    tcb->wait_for   = 0;
    // NB: Don't touch (tcb->deadline), as it may be set from the C API!

    // Insert "tcb" into its priority band:

    if  (use_heap)
        // Put "tcb" into the queue according to its priority:
        prio_q_insert (tcb);
    else
    {
        // Put "tcb" at the end of the runnable queue for this band:
        prio_band * band = runnable_threads + get_prio_band (tcb);

        assert     (band->tcbs       != NULL);
        assert     (band->n_runnable >= 0);
        assert     (band->n_runnable < MAX_THREADS);

        tcb->rpos = band->n_runnable;
        band->tcbs [tcb->rpos] = tcb;
        band->n_runnable ++;
    }
    CHECK_RUNNABLE (tcb);
}

//-------------------------//
//  "yield_to_scheduler":  //
//-------------------------//
//  Context switch to the Scheduler:
//
static void yield_to_scheduler ()
{
    CHECK_TCB (curr_thread)
    // NB: the thread may already be blocked...

    // Save the current env:
    enter_blocking_section ();

    // Jump to the Scheduler. NB: test the results of context switch -- we
    // need to know whether we had a switch and returned successfully,  or
    // failed to switch at all:
    if  (swapcontext (&  (curr_thread->posix_ctx),
                      & sched_tcb.posix_ctx) != 0)
        horror ("Tnio.yield_to_scheduler: POSIX context swap failed");

    // On return, restore the env:
    leave_blocking_section ();

    // Check our TCB on return:
    CHECK_RUNNABLE (curr_thread)
}

//-----------------//
//  "run_thread":  //
//-----------------//
//  The thread being run is the current one, so we don't need to pass its TCB.
//  This function accepts a dummy parm, for compatibility with GNU Pth:
//
typedef void (* thread_body_t) ();

static  void run_thread (void * dummy)
{
    CHECK_RUNNABLE (curr_thread)

#   if defined(TNIO_DEBUG) && TNIO_DEBUG >= 2
    fprintf (stderr, "Running Thread %d, TCB=%p\n",
             curr_thread->id, curr_thread);
    fflush  (stderr);
#   endif

    // Set the initial OCaml RTS data for this thread:
    leave_blocking_section ();

    // Apply the closure. It normally returns () -- we catch any exceptions
    // here. XXX: will the exception object be properly garbage-collected?-
    // Probably yes, as it lives in the same shared heap:
    //
    value res = callback_exn (curr_thread->body_closure, Val_unit);

#   if defined(TNIO_DEBUG) && TNIO_DEBUG >= 2
    fprintf (stderr, "\t... %d: Back from OCaml body, TCB=%p\n",
            curr_thread->id, curr_thread);
    fflush  (stderr);
#   endif

    if  (Is_exception_result (res))
    {
        // Print a message on stderr, if it is available:
        value exn = Extract_exception (res);

        fprintf(stderr, "Thread %d killed on uncaught exception: %s\n",
                        curr_thread->id,  caml_format_exception (exn));
        fflush (stderr);
    }

    // Mark the thread as terminated -- the Scheduler will do the clean-up:
    curr_thread->status = TERMINATED_STATUS;
    live_threads --;

    // Return to the Scheduler. This can be done in a slightly more effici-
    // ent was as we know that the thread will not be resumed, so we don't
    // need to save its context, but never mind:
    //
    yield_to_scheduler ();
}

//-------------------------//
//  "tnio_thread_create":  //
//-------------------------//
//  A new TCB is created, and made runnable. This function can be called from
//  the main thread before it enters the Scheduler, or from any client thread.
//
value tnio_thread_create (value closure, value stack_size_k, value priority)
{
    CHECK_INIT ("Tnio.thread_create")

    // Check that the priority requested is consistent with the general schedu-
    // ling mode:
    int prio = Int_val (priority);
    if  (! use_heap && (prio < 0 || prio >= n_bands))
        // The priority must be within the bands available:
        tnio_error (EINVAL, "Tnio.thread_create: Invalid priority");

    // Create a TCB -- it's OK to allocate it by "malloc". Do NOT allocate it
    // from OCaml heap (e.g. as a String or a Custom block), as it would then
    // be very difficult to protect it against the GC:
    //
    TCB * tcb = malloc (sizeof (TCB));

    if  (tcb == NULL)
        tnio_error (ENOMEM, "Tnio.thread_create: TCB");

    // Find the thread ID. The ID of a terminated thread can be re-used --
    // XXX but this involves traversing "all_threads":
    int id;
    for (id = 0;  id <= last_thread;  id++)
        if  (all_threads[id] == NULL)
            break;

    if  (id == last_thread+1)
    {
        // No re-usable entry was found, create a new one:
        if  (id >= MAX_THREADS)
            tnio_error (EAGAIN, "Tnio.thread_create: Too many threads");
        last_thread ++;
    }
    // The created thread is live, as it has not yet terminated, so
    // increment the live counter:
    live_threads ++;

    tcb->id           = id;
    tcb->priority     = prio;
    tcb->body_closure = closure;
    tcb->status       = INIT_STATUS;
    tcb->deadline     = INVALID_DEADLINE;
    tcb->external_resume_ok = 0;
    tcb->ref_count    = 0;  // Before wrapping

    // IMPORTANT:
    // As "body_closure" will be stored in a global-like data structure inside
    // the Treads Scheduler, we need to protect it from being GCed:
    register_global_root (& (tcb->body_closure));

    // Put it into the "all_threads" array:
    all_threads [id]  = tcb;

    //  Allocate the stack to be used by the thread.   Like for TCB itself, do
    //  NOT allocate it from OCaml heap:
    //
    int    stsz  = Int_val (stack_size_k) * 1024;
    char * stk   = calloc  (stsz, 1);

    // Create the initial context for the new thread. The function to invoke
    // is an "envelope" C function, which in turn calls the closure. We also
    // create the stack for the thread:

    // POSIX Ctx Switch implementation:
    stack_t st;
    st.ss_sp    = stk;
    if  (st.ss_sp == NULL)
        tnio_error (ENOMEM, "Tnio.thread_create: Stack");
    st.ss_flags = 0;
    st.ss_size  = stsz;

    // The thread context is not linked to anything;
    // return from the thread is explicitly managed:
    if  (getcontext  (& (tcb->posix_ctx)) != 0)
        terror ("Tnio.thread_create: Cannot get POSIX context");

    tcb->posix_ctx.uc_link  = NULL;
    tcb->posix_ctx.uc_stack = st;
    sigemptyset (& (tcb->posix_ctx.uc_sigmask));

    makecontext (& (tcb->posix_ctx), (thread_body_t) run_thread, 1, NULL);

    // Initialise the RTS fields:
    tcb->bottom_of_stack   = NULL;
    tcb->exception_pointer = NULL;
    tcb->local_roots       = NULL;
    tcb->gc_regs           = NULL;
    tcb->last_retaddr      = 0;

#   ifdef TNIO_DEBUG
    // Initialise the debugging fields:
    tcb->magic        = TCB_MAGIC;
    tcb->stack_base   = stk;
    tcb->stack_size   = stsz;
#   endif

    // There are no FDs yet owned by this thread:
    FD_ZERO (& tcb->FDs);

    // Make the thread runnable: this re-sets all waiting-related TCB fields:
    to_runnable    (tcb);
    CHECK_RUNNABLE (tcb)

    // Nothing happens until control  is returned to the Scheduler in due
    // course. The TCB ptr is returned to OCaml encapsulated into a small
    // Custom block,   so that we can de-allocate the TCB when the thread
    // terminates, AND it is no longer accessible from OCaml.
    // NB: while calling "alloc_custom" below, we don't protect any other
    // values from the GC: "closure" is already registered as global, and
    // "stack_size_k" is no longer needed:
    //
    return custom_wrap (tcb);
}

//--------------------//
//  "remove_thread":  //
//--------------------//
//  Clean-up after the given thread has terminated or has been killed, OR
//  when it is no longer accessible from OCaml (async==1):
//
static void remove_thread (TCB * tcb, int async)
{
    assert    (tcb->status == TERMINATED_STATUS);
    if (! async)
    {
        CHECK_TCB (tcb);

        // Remove it from the runnable queue, if it is still there:
        from_runnable (tcb);

        // Close the FDs owned by this thread -- it will not be able to use
        // them anymore. No point in re-checking the TCB, hence "0" arg:
        close_owned_fds (tcb, 0);

        // Clean up the "all_threads" entry:
        all_threads [tcb->id] = NULL;

        // Decrement the counters -- only after this ID was removed:
        if  (tcb->id == last_thread)
            last_thread --;
        assert (last_thread >= -1);
    }
    // In any case: de-allocate the resources, if they are still allocated:

    // POSIX Ctx Switch implementation: De-allocate the stack:
    free (tcb->posix_ctx.uc_stack.ss_sp);
    tcb->posix_ctx.uc_stack.ss_sp   = NULL;
    tcb->posix_ctx.uc_stack.ss_size = 0;

    // Unregister the closure (OCaml thread body),
    // so it can be removed by the GC. XXX: is it OK if we do it multiple
    // times?
    remove_global_root (& (tcb->body_closure));

    // De-allocate the TCB itself ONLY  if it is  no longer referenced  from
    // OCaml, i.e. all the wrapper objects have been finalised.   NB: we can
    // NOT skip this step and rely on "finalise_tcb" alone, as if the thread
    // terminates AFTER all its wrappers have gone out of scope,  "finalise_
    // tcb" will not be invoked anymore, so the memory will not be freed:
    //
    if  (tcb->ref_count <= 0)
        free (tcb);
}

//----------------------//
//  "tnio_thread_self": //
//----------------------//
value tnio_thread_self  (value unit)
{
    CHECK_INIT  ("Tnio.thread_self");

    return custom_wrap (curr_thread);
}

//----------------------//
//  "tnio_*thread_id":  //
//----------------------//
//  Returns integer ID of a given thread (EVEN if it has terminated):
//
#define INVALID_THREAD_ID -1

value tnio_invalid_thread_id (value unit)
{
    return Val_int (INVALID_THREAD_ID);
}

value tnio_thread_id (value vtcb)
{
    CHECK_INIT ("Tnio.thread_id")

    TCB * tcb = TCB_PTR    (vtcb);

    if  (tcb == NULL)
        // The main thread -- return an invalid value:
        return tnio_invalid_thread_id (Val_unit);
    else
    {
        // A real thread, of ANY status:
        CHECK_TCB  (tcb)
        return   Val_int (tcb->id);
    }
}

// The following function is a part of CAPI, but is placed here for simplicity:
int TNIO_CAPI_get_curr_thread_id ()
{
    if (curr_thread == NULL)
        return INVALID_THREAD_ID;

    CHECK_TCB  (curr_thread);
    return (curr_thread->id);
}

//--------------------------//
//  "tnio_invalid_thread":  //
//--------------------------//
//  Generates a synthetic "thread_t" wrapper for the Main Thread. Note that
//  the TCB ptr wrapped in NULL, NOT (&sched_tcb),   so the main thread can
//  always be easily identified, and the result is consistent with settings
//  for "curr_thread":
//
value tnio_invalid_thread (value unit)
{
    return custom_wrap (NULL);
}

//-----------------------//
//  "tnio_thread_by_id": //
//-----------------------//
//  The inverse of "tnio_thread_id":
//
value tnio_thread_by_id (value tid)
{
    CHECK_INIT ("Tnio.thread_by_id")

    int id = Int_val (tid);

    if (id == INVALID_THREAD_ID)
        // This is the ID of the main thread:
        return tnio_invalid_thread (Val_unit);
    else
    if (id >= 0 && id <= last_thread)
    {
        // A potentially valid Thread ID  -- get its TCB, but it may still
        // be NULL -- the space of thereads is not guaranteed to be contig's:
        TCB * tcb = all_threads [id];
        if  (tcb == NULL)
            return tnio_invalid_thread (Val_unit);

        // Otherwise: should be a valid thread:
        CHECK_TCB (tcb);
        return custom_wrap (tcb);
    }
    else
    {
        // Invalid ID -- exception:
        tnio_error (EINVAL, "Tnio.thread_by_id");
        return Val_unit;
    }
}

//------------------------//
//  "process_io_events":  //
//------------------------//
//  Normally (in multi-threaded mode), called from within the Threads Scheduler.
//  In the single-threaded mode, called from the events polling loop:
//
static void process_io_events (int ms)
{
    // Get the incoming I/O events, waiting for up to "ms" milliseconds:
    int i, active = -1, scan_up_to = -1;

    switch (IOEM)
    {
        case USE_IO_EVENTS_SELECT:
        {
#           ifdef HAVE_IO_EVENTS_SELECT
            // "select"-based implementation:
            // Copy the global descr sets into the tmp ("expendable") ones:
            in_tmp  =  in_fds;
            out_tmp = out_fds;
            exc_tmp = exc_fds;

            struct timeval timeout;
            timeout.tv_sec = ms / 1000;         // Seconds
            timeout.tv_usec=(ms % 1000) * 1000; // MicroSeconds

            active     =
                select (max_fd+1, & in_tmp,  & out_tmp,
                                  & exc_tmp, & timeout);
            if (active < 0)
                terror  ("Tnio.process_io_events: select");

            scan_up_to = max_fd;
#           else
            assert (0);
#           endif
            break;
        }

        case USE_IO_EVENTS_POLL:
#           ifdef HAVE_IO_EVENTS_POLL
            // "poll"-based implementation:
            // The total number of FDs to "poll" is "max_fd+1" (as they are
            // numbered from 0),   but only some of them are really "live":
            //
            active     = poll (poll_buff, max_fd+1, ms);
            if (active < 0)
                terror  ("Tnio.process_io_events: poll");

            scan_up_to = max_fd;
#           else
            assert (0);
#           endif
            break;

        case USE_IO_EVENTS_EPOLL:
#           ifdef HAVE_IO_EVENTS_EPOLL
            // "epoll"-based implementation:
            // "epoll_wait" returns the total number of active descrs which
            // were put into the "epoll_buff".
            // The actual bound here for the number of file descrs is
            // "max_fd + 1", but it does not matter for "epoll_wait":
            //
            active     = epoll_wait (epoll_fd, epoll_buff, MAX_TSOCKS, ms);
            if (active < 0)
                terror  ("Tnio.process_io_events: epoll_wait");

            scan_up_to = active-1;
#           else
            assert (0);
#           endif
            break;

        default:
            assert (0);
    }

    // So what have we got?

    if  (active <= 0 || scan_up_to < 0)
        // E.g. waiting interrupted by a signal -- never mind!
        return;

    // Go through all events received, and make the threads waiting for those
    // events, runnable:
    for (i = 0;  i <= scan_up_to;  i ++)
    {
        //  Get "new_events" from the underlying I/O Events mechanism),  and
        //  put them into the "abstract" "all_events" structure:
        int     ts              = INT_MIN;
        event_mask_t new_events = 0;

        switch (IOEM)
        {
            case USE_IO_EVENTS_SELECT:
#               ifdef HAVE_IO_EVENTS_SELECT
                // "select"-based implementation:
                // Check whether this descr is present in any of the sets
                // returned by the "select" call:
                //
                new_events = 0;

                if  (! all_events[i].live)
                    continue;
                ts = i;

                if  (FD_ISSET (i, &  in_tmp))
                    new_events |= EVENT_IN;

                if  (FD_ISSET (i, & out_tmp))
                    new_events |= EVENT_OUT;

                if  (FD_ISSET (i, & exc_tmp))
                    new_events |= EVENT_ERR;        // XXX: is it correct?
#               else
                assert (0);
#               endif
                break;

            case USE_IO_EVENTS_POLL:
#               ifdef HAVE_IO_EVENTS_POLL
                // "poll"-based implementation:
                ts         =  poll_buff[i].fd;
                new_events =  poll_buff[i].revents;

                // Skip invalid descrs:
                if  (ts != i || (new_events & POLLNVAL) != 0)
                {
                    // An invalid descr should not be marked as "live":
                    assert (! all_events[i].live);
                    continue;
                }
                assert (all_events[ts].live);  // ts==i
#               else
                assert (0);
#               endif
                break;

            case USE_IO_EVENTS_EPOLL:
#               ifdef HAVE_IO_EVENTS_EPOLL
                // "epoll"-based implementation:
                ts   =  epoll_buff[i].data.fd;
                assert (all_events[ts].live);
        
                new_events = epoll_buff[i].events;
#               else
                assert (0);
#               endif
                break;

            default:
                assert (0);
        }

        // Once again, check the socket we got:
        assert (ts >= 0);
        assert (ts <  MAX_TSOCKS);
        assert (ts <= max_fd);
        assert (all_events[ts].live);
        ASSERT_ET_TS  (ts);

        // The events structure for the descr in question:
        if  (new_events == 0)
            // No events -- nothing to do:
            continue;

        // Otherwise, update "all_events":
        ET * et = all_events + ts;
        et->got_events |= new_events;

        // Is anyone waiting for these events? -- If not, ignore them:
        TCB * tcb = et->waiting_thread;

        if  (tcb == NULL)
            continue;
        CHECK_TCB (tcb)

        // The "ts" socket MUST actually be owned by this thread:
        assert (FD_ISSET (ts, & tcb->FDs));

        // NB: it is possible that the thread is still hooked to
        // et->waiting_thread,  but is in fact already runnable.
        // This is because it is made runnable by the Scheduler,
        // but unhooks ITSELF when it actually resumes.   Ignore
        // such threads.
        // In particvular, the thread CAN already be terminated,
        // but not yet removed:
        //
        if  (tcb->status != BLOCKED_STATUS)
            continue;

        //  Check for error conds on "ts". As a special exception,
        //  HUPs are ignored if the client thread is waiting on "connect":
        if  ((new_events & EVENT_ERR) != 0 ||
            ((new_events & EVENT_HUP) != 0 &&
               tcb->wait_for != (EVENT_IN | EVENT_OUT)))
        {
            tcb->wrc    = -1;
            tcb->werrno = get_socket_error (ts);

            // Release the waiting thread on error:
            to_runnable (tcb);
        }
        else
        if  ((new_events & tcb->wait_for) != 0)
        {
            // Got an event we were waiting for, and no errors. Also release
            // the waiting thread. To make sure, re-set the error fields:
            tcb->wrc    = 0;
            tcb->werrno = 0;
            to_runnable  (tcb);
        }
    }
}

//------------------//
//  "double_time":  //
//------------------//
//  "timeval" to "double" conversion:
//
static double double_time (struct timeval * tv)
{
    assert (tv != NULL);
    return ((double) tv->tv_sec) + ((double) tv->tv_usec) / 1e6;
}

//-------------------//
//  "get_deadline":  //
//-------------------//
//  Extracts the deadline from the explicitly given arg, if valid, OR from
//  the TCB of the current (or the main) thread, if it is already there:
//
static double get_deadline (value deadline)
{
    double dl = Double_val (deadline);
    if  (dl > 0.0)
        //  The explicitly given deadline seems to be valid:
        return dl;

    // Otherwise: use the one already stored in the TCB:
    TCB * tcb = get_active_tcb ();
    dl  = tcb->deadline;
    return  dl;
}

//------------------------//
//  "wait_for_io_event":  //
//------------------------//
//  Waits for EITHER of the events specified  by the "wait_for" mask.  This
//  function is called from a thread, yields control to the Scheduler, then
//  regains control.
//  This function can also be called from the main thread, in which case it
//  just polls the events.  Raises exception if an error (incl time-out) is
//  received:
//
static void wait_for_io_event
    (ET * et,  event_mask_t wait_for,  double deadline,  char* err_msg)
{
    // If this function is invoked, the deadline must be real:
    assert (deadline > 0.0);

    // Either there is no socket descr at all, or it must be a real one:
    if  (et != NULL)
        assert (et->live);

    if  (curr_thread == NULL)
    {
        // Called from the main thread -- will poll the events until the dead-
        // line expires, with a 10ms interval. If, furthermore, there is no
        // socket descr to wait for events, it will simply sleep:
        //
        struct timeval now;
        gettimeofday (&now, NULL);

        if  (et == NULL)
        {
            // Just sleep for "us" microseconds:
            int us = (int) ((deadline - double_time (& now)) * 1e6);
            usleep   (us);
            return;
        }
        else
        {
            // Poll for events every 10 milliseconds:
            int ms   = (int) ((deadline - double_time (& now)) * 1000.0);
            int step = 10;
            int t;
            for (t = 0; t < ms;  t += step)
            {
                process_io_events    (step);

                if  ((et->got_events & wait_for) != 0)
                    // Event received:
                    return;
            }
            // Waiting finished unsuccessfully:
            tnio_error (ETIMEDOUT, err_msg);
        }
        // We MUST NOT get here: we either return, or raise an error:
        assert (0);
    }

    // Otherwise: called from a real client thread:
    // Suspend the thread on the "et" entry (but et==NULL is OK):
    CHECK_RUNNABLE (curr_thread)

    // Mark the "curr_thread" as blocked and remove it from the runnable queue;
    // NB: there is no separate "blocked" queue:
    from_runnable (curr_thread);
    curr_thread->status = BLOCKED_STATUS;

    // Re-set the error conditions before waiting:
    curr_thread->wait_for   = wait_for;
    curr_thread->deadline   = deadline;
    curr_thread->wrc        = 0;
    curr_thread->werrno     = 0;

    // NB: et==NULL is possible if the thread is to sleep,  without any I/O
    // events. Otherwise, as sockets are not shared between threads, no-one
    // should be waiting on this socket already.  Also check the socket FD:
    if (et != NULL)
    {
        assert (et->live && et->waiting_thread == NULL);
        et->waiting_thread = curr_thread;

#       ifdef TNIO_DEBUG
        int ts = et-> ts;
        ASSERT_ET_TS (ts);
        assert (FD_ISSET (ts, & curr_thread->FDs));
#       endif
    }

    // CONTEXT SWITCH: The current context is saved, so the thread can
    // be resumed later:
    yield_to_scheduler ();

    // Here we emerge from waiting when released by the Scheduler, but haven't
    // yet been removed from et->waiting_thread, so do it now:
    if  (et != NULL)
    {
        assert (et->waiting_thread == curr_thread);
        et->waiting_thread = NULL;
    }
    // The scheduler has made us runnable again:
    CHECK_RUNNABLE (curr_thread)

    // Check for errors (e.g. time-out) received while waiting:
    if  (curr_thread->wrc != 0)
        tnio_error (curr_thread->werrno, err_msg);
}

//------------------------//
//  "tnio_thread_yield":  //
//------------------------//
//  "curr_thread" just returns control to the Scheduler, but remains runnable:
//
value tnio_thread_yield (value unit)
{
    CHECK_INIT ("Tnio.thread_yield")

    if  (curr_thread != NULL)
    {
        // Check before:
        CHECK_RUNNABLE (curr_thread)

        yield_to_scheduler  ();

        // Check after:
        CHECK_RUNNABLE (curr_thread)
    }

    // It returns () when regains the CPU:
    return Val_unit;
}

//------------------------//
//  "tnio_thread_sleep":  //
//------------------------//
//  The thread is suspended, but it is NOT waiting for an I/O event on any
//  socket:
//
value tnio_thread_sleep (value sleep_sec)
{
    CHECK_INIT ("Tnio.thread_sleep")

    double tmo = get_deadline (sleep_sec);

    if  (curr_thread == NULL)
    {
        // Single-threaded mode -- suspend the whole process:
        int     usec = (int) (tmo * 1e6);
        usleep (usec);
    }
    else
    {
        CHECK_RUNNABLE (curr_thread)

        // Calculate the absolute deadline:
        struct timeval now;
        gettimeofday (&now, NULL);

        double dl  = double_time (& now) + tmo;

        // We will in fact be waiting for time-out expiration,   not for an I/O
        // event. NB: "sleep_sec" value can be destroyed if GC occurs after the
        // context switch here, but it's no longer needed:
        //
        wait_for_io_event (NULL, 0, dl, "Tnio.thread_sleep");
    }
    return  Val_unit;
}

//-----------------------//
//  "tnio_thread_stop":  //
//-----------------------//
//  A thread stops another thread:
//  Arg1: TCB of the thread to be stopped;
//  Arg2: Deadline for that thread to wake up; an error is generated if it is
//        not resumed by the deadline:
//
value tnio_thread_stop (value vtcb, value deadline)
{
    CHECK_INIT ("Tnio.thread_stop")

    TCB * tcb = TCB_PTR  (vtcb);

    if  (tcb == NULL)
    {
        // This is the main thread -- cannot stop it:
        tnio_error (EINVAL, "Tnio.thread_stop: Cannot stop the main thread");
        return Val_unit;
    }

    // Otherwise: A real thread (maybe the current one) is to be stopped:
    CHECK_TCB  (tcb)

    // If the thread is blocked already, but NOT in such a way which wllows an
    // external resume, we cannot stop it,  as we would then be unable to tell
    // whether we can resume it (if, for example, it is sleeping by itself):
    switch (tcb->status)
    {
    case BLOCKED_STATUS:
        if  (tcb->external_resume_ok)
            // Already stopped -- nothing to do:
            return Val_unit;
        else
            // Blocked on I/O or sleeping -- can't stop it:
            tnio_error (ECANCELED, "Tnio.thread_stop: Thread already blocked");

    case TERMINATED_STATUS:
        // Also fails:
        tnio_error (ECANCELED, "Tnio.thread_stop: Thread already terminated");

    case RUNNABLE_STATUS:
        // First, remove it from the runnable queue:
        from_runnable (tcb);

    case INIT_STATUS:
        // The rest is common for "RUNNABLE_" and "INIT_" status:
        tcb->status = BLOCKED_STATUS;

        // Re-set the error conditions before waiting: NB: we don't wait for
        // any I/O events,  but allow external resume:
        tcb->wait_for   = 0;
        tcb->deadline   = Double_val (deadline);
        tcb->wrc        = 0;
        tcb->werrno     = 0;
        tcb->external_resume_ok = 1;

        // Now: if it's the current thread which is to be stopped, yield the
        // control to the Scheduler:
        if  (tcb == curr_thread)
            yield_to_scheduler();

        return Val_unit; // Also when returned from the sceduler!
    }
    // This point is actually never reached:
    assert (0);
    return Val_unit;
}

//-------------------------//
//  "tnio_thread_resume":  //
//-------------------------//
value tnio_thread_resume (value vtcb)
{
    CHECK_INIT ("Tnio.thread_resume")

    TCB *tcb = TCB_PTR (vtcb);
    if  (tcb == NULL || tcb == curr_thread)
        // This is the main thread or the current thread, already running --
        // nothing to do:
        return Val_unit;

    // Otherwise: Another thread is to be resumed:
    CHECK_TCB  (tcb)

    switch (tcb->status)
    {
    case BLOCKED_STATUS:
        // Is external resume allowed?
        if  (tcb->external_resume_ok)
        {
            // Yes, release the thread -- no errors encountered.
            // NB: Don't forget to re-set the "external_resume_ok" flag!
            tcb->wrc                = 0;
            tcb->werrno             = 0;
            tcb->external_resume_ok = 0;
            to_runnable  (tcb);
            return    Val_unit;
        }
        else
            // Not allowed to release it:
            tnio_error (ECANCELED,
                       "Tnio.thread_resume: Not allowed to resume that thread");

    case TERMINATED_STATUS:
        tnio_error (ECANCELED,
                   "Tnio.thread_resume: Thread already terminated");

    case INIT_STATUS:
        // Hmm... What to do in this case?
        // Just meke the thread runnable:
        to_runnable (tcb);
        return   Val_unit;

    case RUNNABLE_STATUS:
        // The thread is already runnable -- someone else has waken it up.
        // That's OK, just do nothing:
        return   Val_unit;
    }
    // This point is actually never reached:
    assert (0);
    return Val_unit;
}

//-----------------------//
//  "tnio_thread_kill":  //
//-----------------------//
value tnio_thread_kill (value vtcb)
{
    TCB * tcb = TCB_PTR  (vtcb);

    if  (tcb == NULL)
        // This is the main thread -- will NOT kill it:
        tnio_error (EINVAL, "Tnio.thread_kill: Cannot kill the main thread");
    else
    {
        // Otherwise: A real thread (maybe the current one) is to be killed:
        CHECK_TCB  (tcb)

        // Mark the TCB that the thread has terminated:
        tcb->status = TERMINATED_STATUS;
        live_threads --;

        // If "tcb" is NOT that of the current thread, the killed thread will
        // then be removed by the Scheduler.   Otherwise, we have killed our-
        // selves; yield control to the Scheduler now:
        if  (tcb == curr_thread)
            yield_to_scheduler ();
    }
    return Val_unit;
}

//=============================//
//  "tnio_threads_scheduler":  //
//=============================//
//------------------//
//  "min_wait_ms":  //
//------------------//
//  This function computes the min wait time (ms) for all blocked threads. It
//  iterates over the whole array of threads, which is XXX not the best idea,
//  but this is done at the time when there are no runnable threads anyway,
//  so this strategy should not adversely affect the real-time performance:
//
static int min_wait_ms ()
{
    // Find the minimum deadline. At the same time, for integrity testing, we
    // verify the "live_threads" value:
    double md = DBL_MAX;
    int i;
    int counted_live_threads = 0;

    for (i = 0; i <= last_thread;  i ++)
    {
        TCB * tcb = all_threads [i];
        if  ( tcb == NULL)
            continue;

        CHECK_TCB (tcb);

        if  (tcb->status != TERMINATED_STATUS)
            counted_live_threads ++;

        if  (tcb->status != BLOCKED_STATUS)
            continue;

        if  (tcb->deadline  < md)
            md = tcb->deadline;
    }
    assert (counted_live_threads == live_threads);

    if  (md == DBL_MAX)
        // This means that all threads have infinite deadlines -- that's OK:
        return INT_MAX;

    // Offset in ms from "now", as most of the waiting syscalls ("select",
    // "poll", "epoll_wait") use milliseconds.  Unfortunately, they don't
    // use absolute deadlines, so we need an extra "gettimeofday" call:
    //
    struct timeval now;
    gettimeofday (&now, NULL);
    
    int ms = (int) ((md - double_time (& now)) * 1000.0);
    if (ms < 0)
        ms = 0;
    return ms;
}

//----------------------//
//  "process_expired":  //
//----------------------//
//  Sets error flags in threads with expired deadlines,  and releases them from
//  waiting (they will get an exception when resumed). Also, removes terminated
//  threads which were killed asynchronously while waiting:
//
static void process_expired ()
{
    struct timeval now;
    gettimeofday (&now, NULL);

    double dnow = double_time (& now);

    // Go through all blocked threads and check their deadlines. This is OK
    // since this operation occurs ONLY  when there are no runnable threads
    // anyway:
    int i;
    for (i = 0;  i <= last_thread;  i ++)
    {
        TCB* tcb = all_threads [i];
        if  (tcb == NULL)
            continue;

        CHECK_TCB (tcb);

        // Only blocked and terminated threads are of interest here:
        if  (tcb->status == TERMINATED_STATUS)
        {
            remove_thread (tcb, 0); // Not async mode!
            continue;
        }
        if  (tcb->status != BLOCKED_STATUS)
            continue;

        if  (tcb->deadline < dnow)
        {
            // The deadline has expired, and the client thread should be
            // released. If it was voluntarily sleeping, it's OK.  If it
            // was waiting (unsuccessfully) for an I/O event, set error:

            if (tcb->wait_for != 0)
            {
                tcb->wrc    = -1;
                tcb->werrno = ETIMEDOUT;
            }
            to_runnable (tcb);
        }
    }
}

//-------------------------//
//  The Scheduler Itself:  //
//-------------------------//
//  Externally-visible -- the main thread enters it. The Scheduler exits when
//  all scheduled threads terminate:
//
value tnio_threads_scheduler (value unit)
{
    CHECK_INIT ("Tnio.threads_scheduler")

    // When the Scheduler gets the CPU, it always starts from this point
    // (more precisely, from the point just after saving the context, as
    // that context is restored when we return to the Scheduler).

    // At first-time entry, save the Scheduler's RTS data:
    save_rts_data ();

    if  (getcontext (& sched_tcb.posix_ctx) != 0)
        horror ("Tnio.threads_scheduler: Cannot get POSIX context");

#   if defined(TNIO_DEBUG) && TNIO_DEBUG >= 2
    if  (curr_thread != NULL)
        fprintf (stderr, "Scheduler: Entered from Thread %d, TCB=%p\n",
                curr_thread->id, curr_thread);
    else
        fprintf (stderr, "Scheduler: Entered from Main Thread\n");
    fflush  (stderr);
#   endif

    if  (curr_thread != NULL)
    {
        TCB * prev_thread = curr_thread;
        CHECK_TCB (prev_thread);

        curr_thread = NULL;

        // We probably don't need to restore the Scheduler's RTS data at this
        // point, as this C function does not use them.

        // What happened to the "prev_thread"? Blocked or terminated?
        if  (prev_thread->status == TERMINATED_STATUS)
        {
            //  Clean-up -- not async mode:
            remove_thread (prev_thread, 0);

            //  If all threads have TERMINATED_STATUS, the Scheduler terminates
            //  as well:
            if  (live_threads == 0)
            {
                // Restore the original OCaml context before returning from
                // the Scheduler:
                tnio_cleanup     ();
                restore_rts_data ();
                return Val_unit;
            }

#           if defined(TNIO_DEBUG) && TNIO_DEBUG >= 2
            fprintf (stderr, "Scheduler: Thread %d terminated, TCB=%p\n",
                    prev_thread->id, prev_thread);
            fflush  (stderr);
#           endif
        }
        else
            assert (prev_thread->status == BLOCKED_STATUS);
    }
    //  Now: iterate until we get a runnable thread. Until then, there is no
    //  "curr_thread":
    assert (curr_thread == NULL);

    while (1)
    {
        // Receiving of new I/O events and sweeping of expired threads is done
        // AFTER a wait (if no runnable threads were available at some point):
        //
        // Go through all bands, in the descending order of priorities, and de-
        // termine the first runnable TCB:
        //
        TCB * next_run  = NULL;
        prio_band * band;

        if  (use_heap)
        {
            // Select the highest-priority runnable thread; it is located
            // at the head of the "runnable_threads[0]" band:
            band = runnable_threads + 0;
            if  (band->n_runnable  >= 1)
                next_run = band->tcbs[0];
        }
        else
        {
            // Go through all priority bands, top-down:
            int i;
            assert  (n_bands >= 1);

            for (i = n_bands-1; i >= 0; i --)
            {
                band = runnable_threads + i;
                if  (band->n_runnable >= 1)
                {
                    // Yes, there are runnable threads in this band:
                    int rand_run = random () % band->n_runnable;
                    next_run     = band->tcbs [rand_run];
                }
            }
        }

        // So: have we got a runnable thread to be run next?
        if  (next_run != NULL)
        {
            // Yes, got it!
            curr_thread = next_run;

#           if defined(TNIO_DEBUG) && TNIO_DEBUG >= 2
            fprintf (stderr,
                    "Scheduler: will run Thread %d, TCB=%p, Prio=%d, RPos=%d\n",
                    curr_thread->id, curr_thread, curr_thread->priority,
                    curr_thread->rpos);
            fflush  (stderr);
#           endif
            CHECK_RUNNABLE (curr_thread)

            // Run it -- off we go!..
            setcontext (& (curr_thread->posix_ctx));

            // This point is never reached:
            assert (0);
        }

        // If we got here, there are no runnable threads at all:

        // Will have to wait  until an I/O event is received,  or until
        // the nearest thread time-out expires:
        int ms = min_wait_ms ();

#       ifdef TNIO_DEBUG
        fprintf(stderr, "No runnable threads; waiting for I/O for %d ms\n", ms);
        fflush (stderr);
#       endif
        // Get the new I/O events.  Blocking for at most "ms" millisecs
        // can occur here:
        process_io_events    (ms);

        // Send timeout signals to expired threads -- do it AFTER "pro-
        // cess_io_events", as more threads are likely to expire at once
        // then,  while those  which got their events would be runnable
        // again:
        process_expired      ();

        // And go back to scanning for runnable threads...
    }
}

//-------------------------//
//  "tnio_threads_count":  //
//-------------------------//
value tnio_threads_count (value unit)
{
    CHECK_INIT ("Tnio.threads_count")
    return Val_int (live_threads);
}

//----------------//
//  "tnio_exit":  //
//----------------//
value tnio_exit (value rc)
{
    // OF COURSE, DON'T do "CHECK_INIT" here!
    // Terminate the program and all of its threads. Again, use more graceful
    // "exit(3)" rather than "_exit(2)":
    exit (Int_val (rc));

    // No return in fact:
    return Val_unit;
}

//-------------------------------//
//  "tnio_get_cpu_utilisation":  //
//-------------------------------//
//  Returns the average figure for the last minute:
//
value tnio_get_cpu_utilisation (value unit)
{
    CHECK_INIT ("Tnio.get_cpu_utilisation")

#   ifdef __linux__
    char buff [128];
    if  (lseek (load_fd, 0, SEEK_SET) != 0)
        terror ("Tnio.get_cpu_utilisation: lseek");

    int  n = read (load_fd, buff, 128);
    if  (n <= 0 || n == 128)
        terror ("Tnio.get_cpu_utilisation: read" );

    buff [n+1] = '\0';

    double u;
    if  (sscanf (buff, "%lf", &u) != 1)
        terror ("Tnio.get_cpu_utilisation: sscanf");

    return copy_double (u * 100.0);
#   else
    // For non-Linux systems, we currently don't know how to get the CPU
    // utilisation:
    return copy_double (0.0);
#   endif
}

//=======================================//
//  Networking Syscalls Implementation:  //
//=======================================//
//  Needless to say, time-outs are provided for blocking operations only:

//----------------------//
//  "register_socket":  //
//----------------------//
//  Attaches a new socket (created by "socket" or "accept") to the I/O
//  Events mechanism:
//
static void register_socket (int ts, int type)
{
    // First of all, save the socket in the TCB of the calling thread:
    if  (curr_thread != NULL)
        FD_SET (ts, & curr_thread->FDs);

    // Make the socket non-blocking:
    //
    int flags = fcntl (ts, F_GETFL, 0);
    if (flags == -1 ||
        fcntl (ts, F_SETFL, flags | O_NONBLOCK) == -1)
    {
        close  (ts);
        terror ("Tnio.register_socket: fcntl-FL");
    }

    flags = fcntl (ts, F_GETFD, 0);
    if (flags == -1 ||
        fcntl (ts, F_SETFD, flags | FD_CLOEXEC) == -1)
    {
        close  (ts);
        terror ("Tnio.register_socket: fcntl-FD");
    }

    switch (IOEM)
    {
        case USE_IO_EVENTS_SELECT:
#           ifdef HAVE_IO_EVENTS_SELECT
            // "select"-based implementation:
            // Add the socket to all global sets to watch:
            FD_SET (ts, &  in_fds);
            FD_SET (ts, & out_fds);
            FD_SET (ts, & exc_fds);
#           else
            assert (0);
#           endif
            break;

        case USE_IO_EVENTS_POLL:
#           ifdef HAVE_IO_EVENTS_POLL
            // "poll"-based implementation:
            // Put "ts" into the corresp position of the "poll_buff". There
            // is no point in tegistering interest in  POLLERR, POLLHUP  or
            // POLLINVAL.
            // XXX: is it OK to register both IN and OUT simultaneously?..
            //
            poll_buff[ts].fd     = ts;
            poll_buff[ts].events = POLLIN | POLLPRI | POLLOUT;
            poll_buff[ts].revents= 0;
#           else
            assert (0);
#           endif
            break;

        case USE_IO_EVENTS_EPOLL:
        {
#           ifdef HAVE_IO_EVENTS_EPOLL
            // "epoll"-based implementation:
            // Add the socket to the EPoll system.   We want to receive error
            // conditions, and to have EDGE-triggered events (EPOLLET).   The
            // socket will be registered for both reading and  writing events
            // -- this is OK in the edge-triggered case. The "ts" in question
            // is stored in the event data,  and returned with each pertinent
            // event.
            // XXX: we probably don't need to register  EPOLLERR and EPOLLHUP
            // explicitly:
            //
            struct epoll_event eevent;
            epoll_data_t       edata;
            eevent.events = EPOLLIN | EPOLLOUT | EPOLLERR | EPOLLHUP | EPOLLET;
            edata.fd      = ts;
            eevent.data   = edata;

            // Do "epoll_ctl":
            if  (epoll_ctl (epoll_fd, EPOLL_CTL_ADD, ts, &eevent) != 0)
            {
                close  (ts);
                terror ("Tnio.register_socket: epoll_ctl add error:");
            }
#           else
            assert (0);
#           endif
            break;
        }

        default:
            assert (0);
    }
    // A UDP socket is assumed to be writable from the beginning; a TCP
    // one is not (until "connect" or so):
    ET * et = all_events + ts;

    if  (type == SOCK_DGRAM)
        et -> got_events = EVENT_OUT;
    else
        et -> got_events = 0;

    // No-one is waiting on the socket just created:
    assert   (et->waiting_thread == NULL);

    // Declare it to be "live", adjust "max_fd", and check all once again:
    et->live = 1;

    if  (ts > max_fd)
        max_fd  = ts;

    CHECK_TS ("Tnio.register_socket", ts);
}

//-------------------------//
//  "set_sock_buff_size":  //
//-------------------------//
//  Sets a kernel buffer size (the same for RECV and SEND) on a given socket:
//
#ifdef __linux__
// Error processing on "/proc" operations in Linux:
static void  proc_fs_error (FILE* fh, int ts, char* msg)
{
    if  (fh != NULL) fclose (fh);
    close  (ts);
    terror (msg);
}
// The pseudo-files we will need:
#   define RMEM_MAX "/proc/sys/net/core/rmem_max"
#   define WMEM_MAX "/proc/sys/net/core/wmem_max"
#endif

static int  global_buff_size_max = 0; // Initialised by the first call below...

static void set_sock_buff_size (int ts, int kbs)
{
    // Set the size of the kernel I/O buffer for "ts".
    if  (kbs <= 0)
        return; // Nothing to do

    // Get and possibly change the maximum allowed value -- XXX:currently this
    // is for Linux only. The global maximum values may need to be adjusted if
    // they are not large enough for "kbs":
    if  (global_buff_size_max <= 0)
    {
        // This means that this value is not yet known -- so get it!
#       ifdef  __linux__
        FILE* fh =   NULL;
        int   max_r, max_w;

        // Get the current "rmem_max" value:
        if  ((fh  = fopen (RMEM_MAX, "r")) == NULL ||
            fscanf (fh, "%d", & max_r) != 1)
            proc_fs_error
                (fh, ts, "Tnio.set_sock_buff_size: Cannot read " RMEM_MAX);
        fclose (fh);
        fh   = NULL;

        // Get the current "wmem_max" value:
        if  ((fh  = fopen (WMEM_MAX, "r")) == NULL ||
            fscanf (fh, "%d", & max_w) != 1)
            proc_fs_error
                (fh, ts, "Tnio.set_sock_buff_size: Cannot read " WMEM_MAX);
        fclose (fh);

        // NB: "min" here!
        global_buff_size_max = max_r < max_w ? max_r : max_w;
        assert (global_buff_size_max > 0);
#       else
        // For non-Linux systems, we don't yet know what to do...
#       endif
    }
    // NB: for some systems, "global_buff_size_max" may be undefined, hence
    // the following check:
    if  (global_buff_size_max > 0 && global_buff_size_max < kbs)
    {
        // Need to adjust the maximum kernel value(s). XXX: slight inefficiency:
        // we do so for both RCV and SND buffers,  whereas only one of them may
        // actually need adjustment. But this is rarely the case (they are nor-
        // mally of the same size initially,  and remain of the same size after
        // our adjustments).
#   ifdef __linux__
        FILE* fh = NULL;
        // Adjust "rmrm_max":
        if  ((fh = fopen (RMEM_MAX, "w")) == NULL ||
            fprintf (fh, "%d\n", kbs) <= 0)
            proc_fs_error
                (fh, ts,
                "Tnio.set_sock_buff_size: Cannot write to "  RMEM_MAX);
        fclose  (fh);
        fh = NULL;

        // Adjust "wmem_max":
        if  ((fh = fopen (WMEM_MAX, "w")) == NULL ||
            fprintf (fh, "%d\n", kbs) <= 0)
            proc_fs_error
                (fh, ts,
                "Tnio.set_sock_buff_size: Cannot write to "  WMEM_MAX);
        fclose  (fh);

        // Don't forget to change the stored global values:
        global_buff_size_max = kbs;
#   else
    //  For non-Linux systems, we don't yet know whether - and how - to do it...
#   endif
    }
    //  Now: set the buffer size for THIS "ts".
    //  This is done in a more portable way, using "setsockopt":
    //
    if  (setsockopt (ts, SOL_SOCKET, SO_RCVBUF, & kbs, sizeof(kbs)) < 0)
    {
        close  (ts);
        terror ("Tnio.set_sock_buff_size: setsockopt-RCV failed");
    }

    if  (setsockopt (ts, SOL_SOCKET, SO_SNDBUF, & kbs, sizeof(kbs)) < 0)
    {
        close  (ts);
        terror ("Tnio.set_sock_buff_size: setsockopt-SND failed");
    }
}

static value tnio_socket_impl   (int kbs, int d, int t, int p)
{
    CHECK_INIT ("Tnio.socket")

    // Create the socket:
    int ts = socket (d, t, p);
    if (ts < 0 || ts >= MAX_TSOCKS)
    {
        if  (ts >=0) close (ts);
        terror ("Tnio.socket: socket");
    }

    // Attach the socket to the I/O Events mechanism:
    register_socket    (ts, t);

    // Set the kernel buffer size:
    set_sock_buff_size (ts, kbs);

    return  Val_int (ts);
}

//-------------------//
//  "tnio_socket*":  //
//-------------------//
// Creates an asyncronous socket and registers it with the events mechanism:
//
extern int socket_domain_table [];
extern int socket_type_table   [];

value tnio_socket3  (value kern_buff_size, value domain, value type)
{
    // Here "domain" and "type" are OCaml-encoded, and "protocol" is always 0:
    // XXX: no boundary checks possible here!
    int kbs= Int_val (kern_buff_size);
    int d  = socket_domain_table [Int_val (domain)];
    int t  = socket_type_table   [Int_val (type)];

    return tnio_socket_impl (kbs, d, t, 0);
}

value tnio_socket_c
    (value kern_buff_size, value domain, value type, value proto)
{
    return tnio_socket_impl
           (Int_val(kern_buff_size), Int_val (domain),
            Int_val (type),          Int_val (proto));
}

//----------------------//
//  "tnio_socketpair":  //
//----------------------//
static value tnio_socketpair_impl (int kbs, int d, int t, int p)
{
    CHECK_INIT ("Tnio.socketpair")

    // Create the socket and put it into non-blocking mode. Also, make it
    // "close-on-exec":
    int tss [2];
    if (socketpair (d, t, p, tss) < 0)
        terror ("Tnio.socketpair: socketpair");

    // Attach the sockets to the I/O Events mechanism:
    register_socket    (tss [0], t);
    register_socket    (tss [1], t);

    // Set the size of the kernel I/O buffer on the sockets:
    set_sock_buff_size (tss [0], kbs);
    set_sock_buff_size (tss [1], kbs);

    // Allocate and return the pair of sockets:
    value    res = alloc_small (2, 0);
    Field(res,0) = Val_int (tss[0]);
    Field(res,1) = Val_int (tss[1]);

    return res;
}

value tnio_socketpair3  (value kbs, value domain, value type)
{
    // Here "domain" and "type" are OCaml-encoded, and "protocol" is always 0:
    // XXX: no boundary checks possible here!
    int d  = socket_domain_table [Int_val (domain)];
    int t  = socket_type_table   [Int_val (type)];

    return tnio_socketpair_impl  (kbs, d, t, 0);
}

value tnio_socketpair_c
    (value kern_buff_size, value domain, value type, value proto)
{
    // Here the args are C-encoded. Kernel buffer size not set here:
    return tnio_socketpair_impl
           (Int_val (kern_buff_size), Int_val (domain),
            Int_val (type),           Int_val (proto));
}


//---------------//
//  "do_close":  //
//---------------//
//  Invoked from "tnio_close" (user-level) or from "close_owned_fds". In
//  the latter case, it is invoked from the Scheduler, so there is no
//  "curr_thread":
static void do_close (TCB * tcb, int ts, int withTCBcheck)
{
    assert (tcb != NULL && ts >= 0 && ts <= max_fd);
    if (withTCBcheck)
        CHECK_TCB (tcb);

    // Really close the socket, IGNORING the result:
    (void) close (ts);

    // The socket is no longer live. Also re-set its events:
    all_events[ts].live       = 0;
    all_events[ts].got_events = 0;

    // Remove it from the "tcb" -- it MUST be there. Note that "tcb" may not
    // be the same as "curr_thread":
    // (1) this function may be invoked by the Scheduler which finalises a
    //     terminated thread, in which case curr_thread==NULL, but as asserted
    //     above, tcb!=NULL;
    // (2) this function may be invoked by GC (on the terminated "tcb") which
    //     was in turn triggered by an operation of curr_thread!=NULL:
    assert  (FD_ISSET(ts, &tcb->FDs));
    FD_CLR  (ts, &tcb->FDs);

    if (curr_thread != tcb)
        // Either Scheduler, or another thread which triggered GC, is
        // finalising "tcb" -- but in that case it must be terminated:
        assert (tcb->status == TERMINATED_STATUS);

    // Is it possible that any thread is waiting on "ts"? Theoretically yes,
    // if a waiting thread was killed before emerging from waiting. But in
    // that case, again, it must be the terminated "tcb":
    TCB * wtcb = all_events[ts].waiting_thread;
    if (wtcb != NULL)
        assert ((wtcb == tcb) || (tcb->status == TERMINATED_STATUS));

    // In any case, now clear the entry:
    all_events[ts].waiting_thread = NULL;

    // Decrement "max_fd" until we get another live socket:
    if  (ts == max_fd)
    {
        int i;
        for (i = max_fd-1; i >= 0;  i--)
            if  (all_events[i].live)
                break;
        max_fd = i;
    }
    // Low-level-specifiv clean-up:
    switch (IOEM)
    {
        case USE_IO_EVENTS_SELECT:
#           ifdef HAVE_IO_EVENTS_SELECT
            // "select"-based implementation:
            // remove "ts" from the global sets:
            FD_CLR (ts, &  in_fds);
            FD_CLR (ts, & out_fds);
            FD_CLR (ts, & exc_fds);
#           else
            assert (0);
#           endif
            break;

        case USE_IO_EVENTS_POLL:
#           ifdef HAVE_IO_EVENTS_POLL
            // "poll"-based implementation:
            // invalidate the corresp "poll" entry:
            poll_buff[ts].fd       = -1;
            poll_buff[ts].events = 0;
            poll_buff[ts].revents= 0;
#           else
            assert (0);
#           endif
            break;

        case USE_IO_EVENTS_EPOLL:
            // Nothing specific:
#           ifndef HAVE_IO_EVENTS_EPOLL
            assert (0);
#           endif
            break;

        default:
            assert (0);
    }
}

//------------------//
//  "tnio_close":   //
//------------------//
value tnio_close (value socket)
{
    CHECK_INIT ("Tnio.close")

    // Close the socket, ignoring any errors. It is automatically removed
    // from the EPoll system:
    int  ts = Int_val (socket);

    // Do NOT propagate an error if the socket is invalid, i.e. already
    // closed:
    if  (ts < 0 || ts > max_fd || (! all_events[ts].live))
        return Val_unit;

    // But if it is valid, check it fully. In particular, the socket must
    // be owned by the current thread, and since this thread is active now,
    // non-one should be waiting on this socket:
    CHECK_TS ("Tnio.close", ts);

    // Do internal close, with TCB check:
    do_close (curr_thread, ts, 1);
    return   (Val_unit);
}

//-------------------//
//  "tnio_connect":  //
//-------------------//
extern void get_sockaddr
        (value, union sock_addr_union *, socklen_param_type *);

value tnio_connect (value socket, value address, value deadline)
{
    CHECK_INIT ("Tnio.connect")

    union sock_addr_union addr;
    socklen_param_type addr_len;

    int    ts = Int_val      (socket);
    double dl = get_deadline (deadline);

    CHECK_TS ("Tnio.connect", ts);

    get_sockaddr(address, &addr, &addr_len);

    // Re-set both "IN" and "OUT" flags on the corresp socket before going
    // into "connect", to prevent false positive connection results. Also,
    // re-set the "HUP" flag, as it may produce a false negative result:
    //
    ET * et   = all_events + ts;
    et->got_events &= ~(EVENT_IN | EVENT_OUT | EVENT_HUP);

    // Perform non-blocking "connect". It's perfectly OK for this call to
    // return "EINPROGRESS":
    //
    int rc = connect(ts, &addr.s_gen, addr_len);
    if (rc != 0 && errno != EINPROGRESS)
        terror ("Tnio.connect: connect failed-1");

    // Wait for completion of "connect":

    wait_for_io_event (et, EVENT_IN | EVENT_OUT, dl,
                      "Tnio.connect: wait for connect failed");

    // If we got here,   it's still no guarantee that there were no connection
    // errors -- the "connect" operation may have been completed (no time-out),
    // but with connection attempt rejected. So check the socket status:
    //
    int  status = get_socket_error (ts);
    if  (status != 0)
        tnio_error (status, "Tnio.connect: connect failed-2");

    // On successful connection, the socket always becomes writable -- indicate
    // that explicitly if we have not got it already:
    et->got_events |= EVENT_OUT;

    // All done:
    return Val_unit;
}

//------------------//
//  "tnio_accept":  //
//------------------//
value tnio_accept3 (value kern_buff_size, value socket, value deadline)
{
    CHECK_INIT ("Tnio.accept")

    int   ts = Int_val   (socket );
    int  kbs = Int_val   (kern_buff_size);
    CHECK_TS   ("Tnio.accept", ts);

    // Wait for readability on this socket -- this means incoming connection,
    // unless we are in non-blocking mode:
    double dl = get_deadline (deadline);

    if  (dl != ZERO_DEADLINE)
        wait_for_io_event (all_events + ts, EVENT_IN, dl,
                          "Tnio.accept: wait for incoming connect failed");

    // Now call "accept" -- it should succeed:
    union sock_addr_union addr;
    socklen_param_type    addr_len = sizeof(addr);

    int ns = accept (ts, &addr.s_gen, &addr_len);
    if (ns < 0)
        terror ("Tnio.accept");

    // We are NOT going to read from / write to the acceptor socket ("ts"), so
    // better remove all flags from it:
    all_events[ts].got_events = 0;

    // Now: the new socket "ns" must be registered with the I/O Event mech.
    // We know that the socket is of TCP type:
    register_socket    (ns, SOCK_STREAM);

    // Set the kernel buff size on the newly-created socket:
    set_sock_buff_size (ns, kbs);

    // Form the result:
    value res = Val_unit;
    value a   = alloc_sockaddr (&addr, addr_len, ns);   // Close "ns" on error

    Begin_root (a)
    res = alloc_small (2, 0);
    Field(res,0) = Val_int (ns);
    Field(res,1) = a;
    End_roots ()

    // All done:
    return res;
}

//----------------------//
//  "tnio_pass_tsock":  //
//----------------------//
value tnio_pass_tsock (value socket, value vtcb)
{
    CHECK_INIT ("Tnio.pass_tsock")

    TCB * tcb = TCB_PTR (vtcb);
    // The thread FROM which we pass "socket" -- main thread is allowed

    if (tcb == curr_thread)
        // Passing it to ourselves -- nothing to do:
        return Val_unit;

    int ts    = Int_val (socket);
    // Do NOT run "CHECK_TS" here, as "ts" does not yet belong to the
    // calling thread. Do a simplified check first:
    //
    if  (ts < 0 || ts > max_fd || (! all_events[ts].live))
        tnio_error (EBADF, "Tnio.pass_tsock: Socket is dead");

    // No-one must be waiting on this socket at the moment:
    TCB* wtcb = all_events[ts].waiting_thread;
    if  (wtcb != NULL)
    {   // If such a waiting thread exists, it could only be the
        // previous owner:
        if  (wtcb != tcb)
            tnio_error (EBADF, "Tnio.pass_tsock: Owned by 3rd-party thread");
        else
            // Otherwise, it's still an error, but a different one:
            tnio_error (EBUSY, "Tnio.pass_tsock: Still used by prev owner");
    }
    if  (tcb != NULL)
    {
        // Verify the prev owner -- it was not the main thread:
        CHECK_TCB (tcb)

        // Detach the socket from the I/O system of that thread:
        FD_CLR (ts, &(tcb->FDs));
    }
    if  (curr_thread != NULL)
    {
        // Verify the new  owner -- it is  not the main thread:
        CHECK_TCB (curr_thread);

        // Attach the socket to  the I/I system of this thraed:
        FD_SET (ts, &(curr_thread->FDs));
    }
    // All done: It must now be in a consistent state:
    CHECK_TS ("Tnio.pass_tsock", ts);
    return Val_unit;
}

//--------------------//
//  "tnio_read":      //
//  "tnio_recv":      //
//  "tnio_recvfrom":  //
//--------------------//
// Wait for readability, then read all data immediately available. "Read" and
// "recvfrom" are for for UDP sockets, "recv" is for TCB;
// "buf_prop" < 0 iff "buf" is an OCaml string (with built-in length), other-
// wise it's the length of "buf" (raw C memory):
//
#define READ_OP     0
#define RECV_OP     1
#define RECVFROM_OP 2

static value get_data
    (value socket,   value buf, int buf_prop, value from_or_flags, value len,
     value deadline, int op)
{
    CHECK_INIT ("Tnio.get_data")

    int   ts  = Int_val (socket);
    CHECK_TS   ("Tnio.get_data", ts);

    double dl = get_deadline (deadline);

    char* into;
    int   buf_len;
    int   flags;
    int   from_pos;

    if  (buf_prop < 0)
    {
        // OCaml API:
        // "buf" is an OCaml string, "from_or_flags" is "from":
        //
        into     = String_val    (buf);
        buf_len  = string_length (buf);
        flags    = MSG_NOSIGNAL; // Default flags
        from_pos = Int_val (from_or_flags);
    }
    else
    {
        // C API:
        // "buf" is a raw C memory, "buf_prop" is its length, 
        // "from_or_flags" is "flags", "from" is always 0:
        //
        into     = (char *) buf;
        buf_len  = buf_prop;
        flags    = Int_val (from_or_flags);
        from_pos = 0;
    }

    int max_read = Int_val (len);

    if (max_read <= 0)
    {
        // Nothing really to do. If "recvfrom" is requested, it would fail:
        if  (op != RECVFROM_OP)
            return Val_int (0);
        else
            tnio_error (EFAULT, "Tnio.get_data: zero recvfrom requested");
    }

    // Check against over-writing the buffer:
    if (from_pos + max_read > buf_len)
        tnio_error (EFAULT, "Tnio.get_data: invalid buffer position");

    // Set the actual starting position:
    into += from_pos;

    // The following are for "recvfrom" only:
    union   sock_addr_union src_addr;
    socklen_param_type      src_addr_len = sizeof(src_addr);

    // Now read the data. We stop after a successful read, not waiting for
    // EAGAIN. Making just one read is essential for both TCP and UDP: UDP
    // needs to preserve  datagram boundaries and senders' addresses,  and
    // TCP needs to detect peer disconnection conditions. Thus, the reada-
    // bility conditions are nor re-set after a successful operation, this
    // is why the next read may yield EAGAIN.   We need to allow for that:
    // wait and re-try after the first EAGAIN:

    int got = 0;
    ET * et = all_events + ts;
    int  a;

    //  THE READING LOOP:

    for (a = 0; a < 2; a++)
    {
        // If the socket is not readable immediately, wait until it becomes
        // readable -- unless ZERO_DEADLINE is given. While we are waiting,
        // protect the "buf" against GC triggered by another thread (unless
        // "buf" is a raw C memory):
        //
        if  (! ((et->got_events & EVENT_IN) || (dl == ZERO_DEADLINE)))
        {
            if  (buf_prop < 0)
            {   // OCaml API:
                Begin_root (buf)
                wait_for_io_event (et, EVENT_IN, dl,
                    "Tnio.get_data: wait for readability failed-1");
                End_roots()

                // IMPORTANT: re-initialise "into", as the "buf" may have
                // moved during GC!
                into = String_val (buf) + from_pos;
            }
            else // C API:
                wait_for_io_event (et, EVENT_IN, dl,
                    "Tnio.get_data: wait for readability failed-2");
        }
        // Do the actual read:
        switch (op)
        {
        case READ_OP:
            got = read     (ts, into, max_read);
            break;
        case RECV_OP:
            got = recv     (ts, into, max_read, flags);
            break;
        case RECVFROM_OP:
            got = recvfrom (ts, into, max_read, flags,
                            &src_addr.s_gen, &src_addr_len);
            break;
        default:
            assert (0);
        }
        // Analyse the result:
        //
        if (got <= 0)
        {
            if  (got == 0 && op == RECV_OP)
                // TCP: connection closed by peer:
                break;

            if  (got < 0  && errno == EAGAIN)
            {
                // Re-set the readability flag:
                et->got_events &= ~EVENT_IN;

                // Try again (will cause waiting for readability):
                continue;
            }
            else 
                // Some general error:
                terror ("Tnio.get_data: read failed");
        }
        else
            // Some data have been read successfully. Stop here:
            break;
    }
    // NB: got < 0 is impossible at this point; got==0 (e.g., peer disconnect)
    // is OK:
    assert (got >= 0);

    // All done -- return the result:
    if  (op == RECVFROM_OP)
    {
        // Return the tuple (got, src_addr) -- close "ts" on error here:
        value res = Val_unit;
        value vadr= alloc_sockaddr(&src_addr, src_addr_len, ts);

        Begin_root (vadr)
        res  = alloc_small (2, 0);
        Field (res, 0) = Val_int(got);
        Field (res, 1) = vadr;
        End_roots  ()
        return   res;
    }
    else
        return Val_int (got);
}

//
//  Externally-visible OCaml functions:
//
value tnio_read (value socket, value buf, value from, value len, value deadline)
{
    return (get_data (socket, buf, -1, from, len, deadline, READ_OP));
}

value tnio_recv (value socket, value buf, value from, value len, value deadline)
{
    return (get_data (socket, buf, -1, from, len, deadline, RECV_OP));
}

value tnio_recvfrom
                (value socket, value buf, value from, value len, value deadline)
{
    return (get_data (socket, buf, -1, from, len, deadline, RECVFROM_OP));
}

//
//  C API functions -- simplified:
//  NB:   but need to check that the "buf" size is correctly specified (>=0),
//  otherwise there would be confusion in "get_data" with the case when "buf"
//  is an OCaml string.
//  The "tnio_invalid_deadline" is detected by "get_deadline", and results in
//  usingthe deadline previously stored in the TCB by the caller:
//
value tnio_read_c (value socket, value buf, value len)
{
    int buf_len = Int_val (len);
    if (buf_len < 0)
    {
        errno = EFAULT;
        return Val_int(-1);
    }
    return get_data
           (socket, buf, buf_len, 0, len, tnio_invalid_deadline, READ_OP);
}

value tnio_recv_c
    (value socket, value buf, value len, value flags)
{
    int buf_len = Int_val (len);
    if (buf_len < 0)
    {
        errno = EFAULT;
        return Val_int(-1);
    }
    return get_data
           (socket, buf, buf_len, flags, len, tnio_invalid_deadline, RECV_OP);
}

value tnio_recvfrom_c
    (value socket, value buf, value len, value flags)
{
    int buf_len = Int_val (len);
    if (buf_len < 0)
    {
        errno = EFAULT;
        return Val_int(-1);
    }
    return get_data
           (socket, buf, buf_len, flags, len, tnio_invalid_deadline,
            RECVFROM_OP);
}

//-----------------//
//  "tnio_send":   //
//  "tnio_sendto": //
//-----------------//
//  "Send" is for TCP (don't use "write", as we need to ignore SIGPIPE), and
//  "sendto" is for UDP. UDP could use "write" as well if the socket is pre-
//  connected, but this is not currently supported by Tnio: our "connect" is
//  for TCP only.
//  Unless an error or time-out occurs, this function does not return  until
//  all the data are sent:
//
#define WRITE_OP  4
#define SEND_OP   5
#define SENDTO_OP 6

static value put_data
    (value socket,   value buf, int   buf_prop, value from_or_flags, value len,
     value deadline, int op,    value dest)
{
    CHECK_INIT ("Tnio.put_data")

    int    ts = Int_val (socket);
    CHECK_TS   ("Tnio.put_data", ts);

    double dl = get_deadline (deadline);

    char* bptr;
    int   buf_len;
    int   flags;
    int   from_pos;

    if  (buf_prop < 0)
    {
        // OCaml API:
        // "buf" is an OCaml string, "from_or_flags" is "from":
        //
        bptr     = String_val    (buf);
        buf_len  = string_length (buf);
        flags    = MSG_NOSIGNAL; // Default flags
        from_pos = Int_val (from_or_flags);
    }
    else
    {
        // C API:
        // "buf" is a raw C memory, "buf_prop" is its length, 
        // "from_or_flags" is "flags", "from" is always 0:
        //
        bptr     = (char *) buf;
        buf_len  = buf_prop;
        flags    = Int_val (from_or_flags);
        from_pos = 0;
    }

    int  to_send = Int_val  (len);

    if (to_send <= 0)
        // Nothing really to do:
        return Val_int (0);

    // Check that we don't attempt to send data from beyond the buffer:
    if (from_pos + to_send > buf_len)
        tnio_error (EFAULT, "Tnio.put_data: invalid buffer position");

    // The actual start of data to be written:
    bptr += from_pos;

    // The following data are for "sendto" only:
    union sock_addr_union dest_addr;
    socklen_param_type    dest_addr_len;

    ET * et         = all_events + ts;
    int  total_sent = 0;

    // THE WRITING LOOP:
    
    while  (to_send > 0)
    {
        // If the socket is not writable immediately, wait until it becomes
        // writable.  While waiting, protect the "buf" against GC triggered
        // by another thread (unless "buf" is raw C memory):
        //
        if  (! ((et->got_events & EVENT_OUT) || (dl == ZERO_DEADLINE)))
        {
            if  (buf_prop < 0)
            {
                Begin_root (buf)
                wait_for_io_event (et, EVENT_OUT, dl,
                              "Tnio.put_data: wait for writability failed");
                End_roots  ()

                // IMPORTANT: re-initialise "bptr", as the "buf" may have
                // moved during GC!
                bptr = String_val (buf) + from_pos;
            }
            else
                wait_for_io_event (et, EVENT_OUT, dl,
                              "Tnio.put_data: wait for writability failed");
        }
        int    sent = 0;

        switch (op)
        {
        case WRITE_OP:
            sent = write  (ts, bptr+total_sent, to_send);
            break;

        case SEND_OP:
            sent = send   (ts, bptr+total_sent, to_send, flags);
            break;
        case SENDTO_OP:
            get_sockaddr  (dest,  &dest_addr, &dest_addr_len);

            sent = sendto (ts, bptr+total_sent, to_send, flags, 
                             & dest_addr.s_gen, dest_addr_len);
            break;
        default:
            assert (0);
        }
        // Analyse the result.
        //
        // For UDP, we must write the whole chunk at once; any other result
        // is erroneous, even EAGAIN (if partial data were sent):
        //
        if  (op == SENDTO_OP)
        {
            if  (sent == to_send)
            {
                // Success:
                total_sent = to_send;
                break;
            }
            else
                terror ("Tnio.put_data: Short UDP send");
        }

        // TCP: 0 bytes written is "closed window" (tolerated), EAGAIN
        // is tolerated as well (although it should not occur directly
        // after we have waited for writability), other negative results
        // indicate an error:
        if  (sent <= 0)
        {
            if  (errno == EAGAIN)
            {
                // XXX: should we re-set the writability flag in case of
                // sent==0?  What if no writability events would follow?
                // Still, it's better to wait and eventually get a time-
                // out than to try re-send in an infinite loop  when the
                // window is closed:
                et->got_events &= ~EVENT_OUT;
                continue;
            }
            else
                terror ("Tnio.put_data: UnKnown error"); // General error
        }
        // Otherwise, success: sent some data: continue:
        to_send    -= sent;
        total_sent += sent;
    }
    // If we got here, all data should be sent:
    return Val_int (total_sent);
}

//
//  Externally-visible OCaml functions. As "sendto" takes more than 5 args, we
//  need a separate version for the byte-code interpreter --  but we currently
//  do not support byte code with Tnio, anyway:
//
value tnio_write
    (value socket, value buf, value from, value len, value deadline)
{
    return put_data (socket, buf, -1, from, len, deadline, WRITE_OP, Val_unit);
}

value tnio_send
    (value socket, value buf, value from, value len, value deadline)
{
    return put_data (socket, buf, -1, from, len, deadline, SEND_OP,  Val_unit);
}

value tnio_sendto_native
    (value socket, value buf, value from, value len, value deadline, value dest)
{
    return put_data (socket, buf, -1, from, len, deadline, SENDTO_OP, dest);
}

value tnio_sendto_bytecode (value argv[], int argc)
{
    horror ("Tnio functionality is not available in the byte-code mode");
    return Val_unit;  // No return in fact!
}

//
//  C API functions. Again, as "put_data" uses the sign of "len" to distinguish
//  between the cases of C and OCaml buffers represented by "buf", we must make
//  sure that "len" holds a valid value here; "tnio_invalid_deadline" is recog-
//  nised by the "get_deadline" function,  so the deadline previously stored in
//  the TCB by the caller is used:
//
value tnio_write_c
    (value socket, value buf, value len)
{
    int buf_len = Int_val (len);
    if (buf_len < 0)
    {
        errno = EFAULT;
        return  Val_int(-1);
    }
    return  // NB: No "flags" here:
    put_data
        (socket,  buf, buf_len, 0, len, tnio_invalid_deadline,
        WRITE_OP, Val_unit);
}

value tnio_send_c
    (value socket, value buf, value len, value flags)
{
    int buf_len = Int_val (len);
    if (buf_len < 0)
    {
        errno = EFAULT;
        return  Val_int(-1);
    }
    return
    put_data
        (socket, buf, buf_len, flags, len, tnio_invalid_deadline,
        SEND_OP, Val_unit);
}

value tnio_sendto_c
    (value socket, value buf, value len, value flags, value dest)
{
    int buf_len = Int_val (len);
    if (buf_len < 0)
    {
        errno = EFAULT;
        return  Val_int (-1);
    }
    return
    put_data
        (socket, buf, buf_len, flags, len, tnio_invalid_deadline,
        SENDTO_OP, dest);
}

//----------------------------------//
//  Other Non-Blocking Operations:  //
//----------------------------------//
//  They are currently implemented at the OCaml level by converting "tsock"
//  into "Unix.file_descr". Their internal representation is the same: int:
//
value tnio_descr_of_tsock (value socket, value comment)
{
    int ts = Int_val (socket);
    //  If "ts" is invalid, convert it without checks, otherwise do checks:
    if (ts >= 0)
    {
        CHECK_INIT ("Tnio.descr_of_tsock");
        CHECK_TS   (String_val  (comment), ts);
    }
    return socket;
}

value tnio_join_multicast (value socket, value iface_ip, value mcast_ip)
{
    CHECK_INIT ("Tnio.join_multicast");
    int ts = Int_val (socket);
    CHECK_TS   ("Tnio.join_multicast", ts);

    // Internal representation of "Unix.inet_addr" is just a 4-byte string,
    // with bytes already in the network (big-endian) byte order:
    //
    in_addr_t  * iface_addr, * mcast_addr;

    iface_addr = (in_addr_t*) (String_val (iface_ip));
    mcast_addr = (in_addr_t*) (String_val (mcast_ip));

    // Check that the multicast addr is indeed valid -- XXX we don't know
    // whether this will be checked automatically:
    unsigned char* mbytes= (unsigned char*) mcast_addr;

    if  (mbytes[0] < 224 || mbytes[0] > 239 ||
        (mbytes[0]== 224 && mbytes[1]== 0   && mbytes[3]== 0))
        terror ("Tnio.joint_multicast: Invalid MCast IP");

    // OK: join the group:
    struct ip_mreq mreq;
    mreq.imr_interface.s_addr = * iface_addr;
    mreq.imr_multiaddr.s_addr = * mcast_addr;

    if  (setsockopt
        (ts, IPPROTO_IP, IP_ADD_MEMBERSHIP, &mreq, sizeof(mreq)) != 0)
        terror ("Tnio.join_multicast: SetSockOpt IP_ADD_MEMBERSHIP failed");

    // Delivery of multicast messages back to self is disabled:
    unsigned char loop = 0;
    if  (setsockopt
        (ts, IPPROTO_IP, IP_MULTICAST_LOOP, &loop, sizeof(loop)) != 0)
        terror ("Tnio.join_multicast: SetSockOpt IP_MULTICAST_LOOP failed");

    // Out-bound multicast messages go through the same interface:
    if  (setsockopt
        (ts, IPPROTO_IP, IP_MULTICAST_IF,   &mreq.imr_interface,
         sizeof(mreq.imr_interface)) != 0)
        terror ("Tnio.join_multicast: SetSockOpt IP_MULTICAST_IF failed");

    return Val_unit;
}

value tnio_set_multicast_ttl (value socket, value ttl)
{
    CHECK_INIT ("Tnio.set_multicast_ttl");
    int ts   = Int_val (socket);
    CHECK_TS   ("Tnio.set_multicast_ttl", ts);

    int ttli = Int_val (ttl);
    if (ttli < 0 || ttli > 255)
        terror ("Tnio.set_multicast_ttl: Invalid TTL");

    unsigned char ttlc = (unsigned char) ttli;

    if (setsockopt
       (ts, IPPROTO_IP, IP_MULTICAST_TTL, &ttlc, sizeof(ttlc)) != 0)
       terror ("Tnio.set_multicast_ttl: SetSockOpt failed");

    return Val_unit;
}

value tnio_get_local_ips  (value unit)
{
    // Create some socket -- not under TNIO control:
    int  ts   = socket (PF_INET, SOCK_DGRAM, IPPROTO_IP);
    if  (ts < 0)
        terror ("Tnio.get_local_ips: Cannot create a socket");

    // Extract info for up to 256 IPs (!) associated to this host:
    struct ifreq  buff [256];
    struct ifconf ifc;
    ifc.ifc_len = sizeof (buff);
    ifc.ifc_req = buff;
    
    int rc = ioctl (ts, SIOCGIFCONF, (char*) (& ifc));
    close (ts);
    if (rc != 0)
        terror ("Tnio.get_local_ips: ioctl SIOCGIFCONF failed");

    // The number of interfaces we got:
    int n = ifc.ifc_len / (sizeof (struct ifreq));
    if  (ifc.ifc_len    % (sizeof (struct ifreq)) != 0 || n > 256)
        terror ("Tnio.get_local_ips: Invalid ioctl result");

    // Make a list of results:
    value res = Val_unit;
    Begin_root (res);

    int i;
    for (i = 0; i < n; i ++)
    {
        struct sockaddr_in * s =  (struct sockaddr_in *) (& buff[i].ifr_addr);
        value ip       = alloc_inet_addr (& s->sin_addr);
        value node     = alloc_small (2, 0);
        Field(node, 0) = ip;
        Field(node, 1) = res;
        res            = node;
    }
    End_roots ();
    return   res;
}

//------------------------------//
//  C API Deadline Management:  //
//------------------------------//
//  These functions are not declared in any way in the OCaml API; they are
//  intended to be used directly with the C API:
//
value tnio_set_next_deadline (value deadline)
{
    double dl = Double_val  (deadline);
    if  (dl <= 0.0)
        terror ("Tnio.set_next_deadline: Invalid deadline");

    // Save "dl" directly in the active TCB:
    TCB * tcb = get_active_tcb ();

    tcb->deadline = dl;
    return    Val_unit;
}

value tnio_get_next_deadline (value unit)
{
    // Get the deadline saved in the active TCB:
    TCB * tcb = get_active_tcb ();
    return copy_double (tcb->deadline);
}

// We also need to export the well-known deadline values to OCaml -- DON'T
//   use the static values here, as they are only set after the module has
//   been initialised,    and the OCaml/C API cannot access non-functional
//   "external" values:
//
value tnio_get_invalid_deadline  (value unit)
{
    return copy_double (INVALID_DEADLINE);
}

value tnio_get_zero_deadline     (value unit)
{
    return copy_double   (ZERO_DEADLINE);
}

value tnio_get_infinite_deadline (value unit)
{
    return copy_double   (INFINITE_DEADLINE);
}

//=========//
//  Misc:  //
//=========//
//----------------------//
//  "tnio_touch_file":  //
//----------------------//
value tnio_touch_file (value filename)
{
    char * fn  = String_val (filename);

    if  (utime (fn, NULL) < 0)
        terror ("Tnio.touch_file");

    return Val_unit;
}

