(*pp camlp4r *)
(* vim:ts=4:syntax=ocaml
*)
(*===========================================================================*)
(*                               "http_client.mli":                          *)
(*                    Interface to the HTTP/HTTPS Client Module:             *)
(*                        (C) Explar Technologies Ltd, 2004                  *)
(*===========================================================================*)
(*===============*)
(*  HTTP Agents: *)
(*===============*)
(*  The under-lying "transport" API, parameterised by the socket type ('s).
    There are no UNIX socket type and domain parms here -- they are always
    assumed to be STREAM INET.  NB: in general,  both "socket" and "close"
    may be blocking -- hence the "float" time-out parm:
*)
type transport 's =
{
    socket:         float -> 's;
    invalid_socket: 's;
    getsockname:    's -> Unix.sockaddr;
    bind:           's -> Unix.sockaddr -> unit;
    connect:        's -> Unix.sockaddr -> float -> unit;
    read:           Misc_utils.io_func 's;
    write:          Misc_utils.io_func 's;
    close:          's -> float -> unit
};

(*  User-specified HTTP agent configuration: *)

type http_agent_config =
{
    conf_client_ips : mutable list Unix.inet_addr;
    conf_use_persist: mutable bool;
    conf_buff_size  : mutable int;
    conf_user_agents: mutable list string
};

value default_http_agent_config: unit -> http_agent_config;

(*  HTTP Agent itself is an abstract type: *)
type http_agent 's 'x = 'a;

(* Factory for HTTP Agents,   and reverse.
   The basic "transport" arg is for ordinary HTTP requests; the optional
   "transport" is a secure one (for HTTPS);    "'x" is the type of HTTPS
   sockets, which can be any(e.g., "unit")    if secure transport is not
   actually used:
*)
value mk_http_agent:
    (string -> list Unix.inet_addr) -> (* DNS   resolver  *)
    (unit   -> unit)                -> (* Close resolver  *)
    transport  's                   -> (* HTTP  transport *)
    option (transport 'x)           -> (* HTTPS transport -- optional *)
    http_agent_config               -> (* Config  proper  *)
    http_agent 's 'x;

value get_all_config:
    http_agent 's 'x
    ->
    ((string -> list Unix.inet_addr) *
     (unit   -> unit)                *
     (transport 's)                  *
     (option (transport 'x))         *
     http_agent_config
    );

(*  Agents re-configuration:
    NB: it currently does not provide change of transport or resolver. It does
    assume a deadline for re-configuration:
*)
value reconfigure_http_agent:
    http_agent 's 'x -> http_agent_config -> float -> unit;


(*====================*)
(*  HTTP/1.1 Client:  *)
(*====================*)
(* HTTP requests:
   "GET" and "POST" (form only, not file uploading) are currently supported:
*)
type http_req  =
{
    http_method  : string;  (* "GET" or "POST" *)
    full_url     : string;  (* Full URL of the request *)

    (* Non-standard headers to be sent: [(Name,Val)]: *)
    extra_headers: list (string * string);

    (* Cookies to be sent -- provided separately from
       "extra_headers" for convenience:
    *)
    cookies      : list Http_common.http_cookie;

    (* Form to be submitted -- "POST" only;   for "GET", the form parms are
       supposed to be already encoded in the "full_url":
    *)
    form         : list (string * string)
};


(*  HTTP results:
    The following type represents the ultimate (successful) result,  not any
    intermediate responses (such as re-directions etc). It also provides the
    parsed URL to which this response actually corresponds, after all re-di-
    rections, as  (protocol, host, port, path_etc):
*)
type parsed_url = (string * string * int * string);

type http_res =
{
    headers : Hashtbl.t string (list string);
    body    : string;
    from_url: parsed_url
};

(* The main HTTP client function: transaction with a deadline: *)
value http_transaction: http_agent 's 'x -> http_req -> float -> http_res;

(* Closing the agent: *)
value close_agent     : http_agent 's 'x -> float -> unit;

