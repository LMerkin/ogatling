(*pp camlp4r *)
(* vim:ts=4:syntax=ocaml
*)
(*===========================================================================*)
(*                                "dirq.mli":                                *)
(*            FIFO Queues with Direct Removal from the Middle:               *)
(*                  L.A.Merkin, IMPARTIAL Project, 2005                      *)
(*===========================================================================*)
(* Msg queue: must allow for fast removal of msgs from the middle, as well
   as general FIFO properties:
*)
type t 'a = 'q;

value empty: unit -> t 'a;

(*  "append":
    Appends a new element at the tail. The queue is modified in place:
*)
value append: t 'a -> 'a -> unit;

(*  "take":
    Traverses the queue, and removes and returns the first element (if any)
    which satisfies the given criterion. The queue is modified in place:
*)
value take : t 'a -> ('a -> bool) -> option 'a;

