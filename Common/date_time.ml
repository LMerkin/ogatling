(*pp camlp4r *)
(* vim:ts=4:syntax=ocaml
*)
(*===========================================================================*)
(*                              "date_time.ml":                              *)
(*     Implementation of Date/Time-Related Functions for "Gatling Mail"      *)
(*                    (C) Explar Technologies Ltd, 2004                      *)
(*===========================================================================*)
(*----------------*)
(*  Common Data:  *)
(*----------------*)
value months_short = [| "Jan"; "Feb"; "Mar"; "Apr"; "May"; "Jun";
                        "Jul"; "Aug"; "Sep"; "Oct"; "Nov"; "Dec" |];

value days_short   = [| "Sun"; "Mon"; "Tue"; "Wed"; "Thu"; "Fri"; "Sat" |];


(*-----------------------*)
(*  "rfc822_time_stamp": *)
(*-----------------------*)
(* Default time zone config: offset in (hour, min) from GMT: *)

(* "tz_offset_str":
   Returns the given Time Zone offset in the format "[+-]hhmm":
*)
value tz_offset_str: int -> int -> string =
fun off_hr off_min ->
    (* off_hr and off_min must be of the same sign: *)
    if  off_hr * off_min < 0
    then
        invalid_arg "tz_offset_str: Hr and Min must be of the same sign"
    else
    let sign =
        if  off_hr < 0 || (off_hr = 0 && off_min < 0)
        then '-'
        else '+'
    in
    let abs_hr  = abs off_hr    in
    let abs_min = abs off_min   in
    if  abs_hr > 23 || abs_min > 59
    then
        invalid_arg "tz_offset_str: Hr or Min out of range"
    else
    Printf.sprintf "%c%02d%02d" sign abs_hr abs_min;

(* "tz_offset":
    Combines the above two functions:
*)
value tz_offset: int -> int -> (string * float) =
fun off_hr off_min ->
    (tz_offset_str off_hr off_min,
     float_of_int (off_hr * 3600 + off_min * 60));

(*  "default_tz_offset":
    the value attempting to represent the Time Zone offset for the current
    host machine:
*)
value default_tz_offset: (string * float) =
    let gmt     = Unix.gettimeofday ()  in
    let now     = Unix.gmtime    gmt    in

    (* "now" was GMT, but we will convert it back into abs seconds, assuming
       the LOCAL time zone:
    *)
    let loc     = fst (Unix.mktime now) in
    (* The offset in seconds of the local zone will be (gmt-loc): e.g., for
       Eastern longitudes, "loc" will LESS than "gmt", as it's the same no-
       minal clock reading reduced back to GMT, where the curr time is less:
    *)
    let diff_min= (int_of_float (gmt-.loc)) / 60 in  (* Drop seconds, if any *)
    let off_hr  = diff_min    / 60      in
    let off_min = diff_min  mod 60      in           (* OK if   diff_min < 0 *)

    tz_offset off_hr off_min;

(*  There are time format variations depending on the actual protocol: *)
type protocol_tz =
[ HTTP                         (* Time is always GMT *)
| SMTP of option (int * int)   (* TZ offset (hours, mins) may be given *)
];

value rfc822_time_stamp: protocol_tz -> float -> string =
fun proto_tz abst  ->
    let (str_tz_off, sec_off) =
        match proto_tz with
        [ HTTP ->
            ("GMT", 0.0)

        | SMTP tz_off ->
            match tz_off with
            [ None        -> default_tz_offset
            | Some (h, m) -> tz_offset h m]
        ]
    in
    (* Reduce "gmt" to the local time in the SUPPLIED time zone: *)

    let reduced_time = abst +. sec_off   in
    (* Time expressed as local *)
    let lt           = Unix.gmtime reduced_time in

    Printf.sprintf "%s, %d %s %d %02d:%02d:%02d %s"
                   days_short  .(lt.Unix.tm_wday)
                   lt.Unix.tm_mday
                   months_short.(lt.Unix.tm_mon )
                   (lt.Unix.tm_year + 1900)
                    lt.Unix.tm_hour
                    lt.Unix.tm_min
                    lt.Unix.tm_sec
                   str_tz_off;
    (* XXX: it does NOT take into account DayLight Saving Time (yet) *)


(*----------------------*)
(*  "smtp_time_stamp":  *)
(*  "http_time_stamp":  *)
(*----------------------*)
value smtp_time_stamp: ?tz_off: option (int * int) -> float -> string =
fun ?(tz_off = None) abst ->
    rfc822_time_stamp (SMTP tz_off) abst;

value http_time_stamp: float -> string =
    rfc822_time_stamp HTTP;

(*----------------------*)
(*  "parse_http_date":  *)
(*----------------------*)
(* Formats accepted:
    "Day, DD Mth YYYY hh:mm:ss GMT"
    "Day, DD-Mth-YYYY hh:mm:ss GMT"
*)
value rec get_month': string -> int -> int =
fun mon i ->
    if  i >= 0 && i < Array.length months_short
    then
        if   months_short.(i) = mon
        then i (* Months are counted from 0 *)
        else get_month' mon (i+1)
    else
        failwith ("Invalid Month: "^mon);

value get_month : string -> int =
fun mon ->
    get_month' mon 0;

value parse_http_date: string -> float =
fun dstr ->

    let get_comps =
        fun wday  dd  mth  yyyy  hh  mm  ss ->
           (wday, dd, mth, yyyy, hh, mm, ss)
    in
    let (_,  dd, mth, yyyy, hh, mm, ss) =
        try
            Scanf.sscanf dstr "%3s, %02d-%3s-%04d %02d:%02d:%02d GMT" get_comps
        with
        [_ ->
            Scanf.sscanf dstr "%3s, %02d %3s %04d %02d:%02d:%02d GMT" get_comps
        ]
    in
    let tm = 
    {
        Unix.tm_sec  = ss;
        Unix.tm_min  = mm;
        Unix.tm_hour = hh;
        Unix.tm_mday = dd;
        Unix.tm_mon  = get_month mth;
        Unix.tm_year = yyyy - 1900;
        Unix.tm_wday = -1;    (* Ignored *)
        Unix.tm_yday = -1;    (* Ignored *)
        Unix.tm_isdst= False  (* Ignored *)
    }
    in
    do {
        (* We set the environment so that "tm" is indeed interpreted as GMT:
           do it JUST BEFORE the conversion.  For that, set the envvar "TZ":
        *)
        Unix.putenv "TZ" "GMT+00:00";
        fst (Unix.mktime tm)
    };

(*----------------*)
(*  "epoch_end":  *)
(*----------------*)
(*  2^31-1 seconds in a floating-point format: 68+ years since the Epoch: *)
value epoch_end = 2147483647.0;

