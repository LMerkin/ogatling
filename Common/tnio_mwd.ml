(*pp camlp4r *)
(* vim:ts=4:syntax=ocaml
*)
(*===========================================================================*)
(*                               "tnio_mwd.ml":                              *)
(*  Implementation of Timed Network I/O Functions using User-Level Threads   *)
(*                            L.A.Merkin, 2003--2005                         *)
(*===========================================================================*)
(*===================*)
(*  Initialisation:  *)
(*===================*)
type scheduling_mode =
[   Priority_Bands of int (* Number of bands *)
|   Single_Queue
];

(*---------*)
(* "init": *)
(*---------*)
external init_intern:
    Tnio_mechs.io_events_mech -> bool -> int -> bool -> unit =
    "tnio_init";

(* Here the initialisation parms are memoised: *)
value init_io_events_mech = ref None;
value init_rt_priority    = ref None;
value init_sched_mode     = ref None;

value init:
    ?io_events_mech  : Tnio_mechs.io_events_mech ->
    ?rt_priority     : bool                      ->
    scheduling_mode                              ->
    unit =
fun
    ?(io_events_mech = Tnio_mechs.IO_Events_Default)
    ?(rt_priority    = False)
    sched_mode       ->

    let (n_bands, use_heap) =
        match sched_mode with
        [ Priority_Bands n ->
            if  n <= 0
            then invalid_arg "Tnio.init: Number of Priority Bands must be >= 1"
            else (n, False)   (* Each priority level is separate -- no heap *)

        | Single_Queue ->
            (1, True)         (* 1 queue for all priority levels *)
        ]
    in
    do {
        (* The actual internal initialisation: *)
        init_intern io_events_mech rt_priority n_bands use_heap;

        (* Memoise the parms. NB: setting the RT priority may fail with a
           warning, yet we still record the original parm here:
        *)
        init_io_events_mech.val := Some io_events_mech;
        init_rt_priority.val    := Some rt_priority;
        init_sched_mode.val     := Some sched_mode
    };

(*---------------*)
(* "get_config": *)
(*---------------*)
(*  Returns the memoised "init" parms:  *)

value get_config:
    unit -> (Tnio_mechs.io_events_mech * bool * scheduling_mode) =

fun () ->
try
(
    Option.get init_io_events_mech.val,
    Option.get init_rt_priority.val,
    Option.get init_sched_mode.val
)
with
[_ ->
    failwith "Tnio.get_config: Not yet initialised"
];

(*---------------------*)
(*  "priority_range":  *)
(*---------------------*)
value priority_range: unit -> (int * int) =
fun () ->
try
    let   sched_mode = Option.get init_sched_mode.val in
    match sched_mode with
    [ Priority_Bands n -> (0, n-1)
    | Single_Queue     -> (min_int, max_int)
    ]
with
[_ ->
    failwith "Tnio.priority_range: Not yet initialised"
];

(*------------------*)
(*  "detach_child": *)
(*------------------*)
external detach_child: unit -> unit = "tnio_detach_child";


(*=====================*)
(*  Timed Socket I/O:  *)
(*=====================*)
(* The "tsock" type: *)
type  tsock             = int;
value invalid_tsock     = -1;

(*--------------------------------*)
(*  Timed and related functions:  *)
(*--------------------------------*)
(*  They are also registered as C call-backs, so they can be used with a
    C API:
*)
(* "socket":     *)
external socket3   :
        int -> Unix.socket_domain -> Unix.socket_type->  tsock =
        "tnio_socket3";

value    socket    :
        ?kern_buff_size:int       ->
        Unix.socket_domain        -> Unix.socket_type->  tsock  =
    fun ?(kern_buff_size=0)  d t  ->
        socket3 kern_buff_size d t;

(* "socketpair": *)
external socketpair3:
        int -> Unix.socket_domain -> Unix.socket_type-> (tsock * tsock) =
        "tnio_socketpair3";

value    socketpair :
        ?kern_buff_size:int       ->
        Unix.socket_domain        -> Unix.socket_type-> (tsock * tsock) =
    fun ?(kern_buff_size=0) d t   ->
        socketpair3 kern_buff_size d t;

(* Versions of "socket" and "socketpair" which use the C (rather than OCaml)
   encoding for domain and type, plus the 4th ("protocol") parm:
*)
external socket_c    : int -> int -> int -> int ->  tsock =
        "tnio_socket_c";
Callback.register "tnio_socket_c"     socket_c;

external socketpair_c: int -> int -> int -> int -> (tsock * tsock)   =
        "tnio_socketpair_c";
Callback.register "tnio_socketpair_c" socketpair_c;

(* "close": *)
external close   : tsock -> unit =
        "tnio_close";
Callback.register "tnio_close"   close;

(* "connect": *)
external connect : tsock -> Unix.sockaddr -> float -> unit   =
        "tnio_connect";
Callback.register "tnio_connect" connect;

(* "accept":
   Also has an optional parm - "kern_buff_size" for the socket returned:
*)
external accept3 : int -> tsock -> float -> (tsock * Unix.sockaddr) =
        "tnio_accept3";
Callback.register "tnio_accept3" accept3;

value    accept  :
         ?kern_buff_size:int   ->
         tsock -> float -> (tsock * Unix.sockaddr) =
    fun ?(kern_buff_size=0) ts deadline ->
        accept3 kern_buff_size ts deadline;

(* "read" (UDP), "recv" (TCP), "recvfrom" (UDP);
   common parms: tsock, buff, from_pos, max_len, abs_deadline_sec:
*)
external read    :   tsock -> string -> int -> int -> float -> int =
        "tnio_read";

external recv    :   tsock -> string -> int -> int -> float -> int =
        "tnio_recv";

external recvfrom:   tsock -> string -> int -> int -> float ->
        (int * Unix.sockaddr) =
        "tnio_recvfrom";

(* "write" (mostly UDP), "send" (TCP), "sendto" (UDP);
    similar to "read" et al declared above:
*)
external write   :   tsock -> string -> int -> int -> float -> int =
        "tnio_send";

external send    :   tsock -> string -> int -> int -> float -> int =
        "tnio_send";

external sendto  :   tsock -> string -> int -> int -> float ->
                     Unix.sockaddr   -> int =
        "tnio_sendto_bytecode" "tnio_sendto_native";
        (* XXX: this is only a formality. Byte-code version of this module
           is not yet supported!
        *)

(*------------------*)
(* Async Interface: *)
(*------------------*)
external get_zero_deadline    : unit -> float = "tnio_get_zero_deadline";

value zero_deadline    : float = get_zero_deadline     ();

external get_infinite_deadline: unit -> float = "tnio_get_infinite_deadline";

value infinite_deadline: float = get_infinite_deadline ();

value accept_now       : tsock ->(tsock * Unix.sockaddr) =
fun ts ->
    accept ts zero_deadline;

value read_now         : tsock -> string -> int -> int -> int =
fun ts buff from len ->
    read ts buff from len zero_deadline;

value recv_now         : tsock -> string -> int -> int -> int =
fun ts buff from len ->
    recv ts buff from len zero_deadline;

value recvfrom_now     : tsock -> string -> int -> int -> (int*Unix.sockaddr) =
fun ts buff from len ->
    recvfrom  ts buff from len zero_deadline;

value write_now        : tsock -> string -> int -> int -> int =
fun ts buff from len ->
    write     ts buff from len zero_deadline;

value send_now         : tsock -> string -> int -> int -> int =
fun ts buff from len ->
    send ts buff from len zero_deadline;

value sendto_now       : tsock -> string -> int -> int -> Unix.sockaddr -> int =
fun ts buff from len to_addr   ->
    sendto    ts buff from len zero_deadline to_addr;

(*------------------*)
(* C API Functions: *)
(*------------------*)
(* Same "read/write" functions with the C API:
   common parms: tsock, buff (raw C ptr), max_len, abs_deadline_sec.
   NB: These functions have no explicit deadline args  -- deadlines
   are managed as described  in the "Deadlines Management" section.
   In particular, "zero_deadline" can be pre-set for them,  to make
   operations non-blocking:
*)
external read_c  :   tsock -> 'a -> int -> int =
        "tnio_read_c";
Callback.register "tnio_read_c" read_c;

external recv_c  :   tsock -> 'a -> int -> int =
        "tnio_recv_c";
Callback.register "tnio_recv_c" recv_c;

external recvfrom_c: tsock -> 'a -> int -> (int * Unix.sockaddr) =
         "tnio_recvfrom_c";
Callback.register "tnio_recvfrom_c" recvfrom_c;

external write_c :   tsock -> 'a -> int -> int =
        "tnio_write_c";
Callback.register "tnio_write_c" write_c;

external send_c  :   tsock -> 'a -> int -> int =
        "tnio_send_c";
Callback.register "tnio_send_c" send_c;

external sendto_c:   tsock -> 'a -> int -> Unix.sockaddr -> int =
        "tnio_sendto_c";
Callback.register "tnio_sendto_c" sendto_c;

(*-----------------------*)
(* Deadlines Management: *)
(*-----------------------*)
(* "*invalid_deadline":  *)
external get_invalid_deadline: unit -> float =
        "tnio_get_invalid_deadline";
value invalid_deadline  : float = get_invalid_deadline ();

(* The following two functions are only called from C, but  need to be
   registered by name if the C code needs to intercept exceptions from
   them:
*)
external set_next_deadline: float -> unit =
        "tnio_set_next_deadline";
Callback.register "tnio_set_next_deadline" set_next_deadline;

external get_next_deadline: unit -> float =
        "tnio_get_next_deadline";
Callback.register "tnio_get_next_feadline" get_next_deadline;

(*---------------------------*)
(*  Non-blocking functions:  *)
(*---------------------------*)
(* They are just lifted from the "Unix" module. No dealines are needed: *)

external to_unix: tsock -> string -> Unix.file_descr =
    "tnio_descr_of_tsock";

value invalid_file_descr: Unix.file_descr = to_unix invalid_tsock "";

(* "bind": *)
value bind      : tsock -> Unix.sockaddr -> unit =
fun ts addr ->
    Unix.bind (to_unix ts "Tnio.bind") addr;

(* "listen":  *)
value listen    : tsock -> int -> unit  =
fun ts ->
    Unix.listen (to_unix ts "Tnio.listen");

(* "shutdown"   : *)
value shutdown      : tsock -> Unix.shutdown_command -> unit =
fun ts ->
    Unix.shutdown    (to_unix ts "Tnio.shutdown");

(* "getsockname": *)
value getsockname   : tsock -> Unix.sockaddr =
fun ts ->
    Unix.getsockname (to_unix ts "Tnio.getsockname");

(* "getpeername": *)
value getpeername   : tsock -> Unix.sockaddr =
fun ts ->
    Unix.getpeername (to_unix ts "Tnio.getpeername");

(* "getsockopt" : *)
value getsockopt    : tsock -> Unix.socket_bool_option -> bool =
fun ts opt_id ->
    Unix.getsockopt  (to_unix ts "Tnio.getsockopt") opt_id;

(* "setsockopt" : *)
value setsockopt    : tsock -> Unix.socket_bool_option -> bool -> unit =
fun ts opt_id opt_val ->
    Unix.setsockopt  (to_unix ts "Tnio.setsockopt") opt_id opt_val;

(* "getsockopt_int": *)
value getsockopt_int   : tsock -> Unix.socket_int_option -> int =
fun ts opt_id ->
    Unix.getsockopt_int (to_unix ts "Tnio.getsockopt_int") opt_id;

(* "setsockopt_int": *)
value setsockopt_int   : tsock -> Unix.socket_int_option -> int -> unit   =
fun ts opt_id opt_val ->
    Unix.setsockopt_int (to_unix ts "Tnio.setsockopt_int") opt_id opt_val;

(* "getsockopt_optint": *)
value getsockopt_optint: tsock -> Unix.socket_optint_option -> option int =
fun ts opt_id ->
    Unix.getsockopt_optint (to_unix ts "Tnio.getsockopt_optint") opt_id;

(* "setsockopt_optint": *)
value setsockopt_optint: tsock -> Unix.socket_optint_option -> option int ->
                         unit  =
fun ts opt_id opt_val ->
    Unix.setsockopt_optint (to_unix ts "Tnio.setsockopt_optint")
                            opt_id  opt_val;

(* "getsockopt_float" : *)
value getsockopt_float : tsock -> Unix.socket_float_option  -> float =
fun ts opt_id ->
    Unix.getsockopt_float  (to_unix ts "Tnio.getsockopt_float") opt_id;

(* "setsockopt_float" : *)
value setsockopt_float : tsock -> Unix.socket_float_option  -> float -> unit =
fun ts opt_id opt_val ->
    Unix.setsockopt_float  (to_unix ts "Tnio.setsockopt_float")
                            opt_id  opt_val;

(* "join_multicast"   : *)
external join_multicast:
    tsock-> ~iface_ip:Unix.inet_addr -> ~mcast_ip:Unix.inet_addr  -> unit  =
    "tnio_join_multicast";
Callback.register "tnio_join_multicast"     join_multicast;

(* "set_multicast_ttl": *)
external set_multicast_ttl: tsock -> int -> unit =
    "tnio_set_multicast_ttl";
Callback.register "tnio_set_multicast_ttl"  set_multicast_ttl;

(* "get_local_ips":     *)
external get_local_ips    : unit  -> list Unix.inet_addr =
    "tnio_get_local_ips";
Callback.register "tnio_get_local_ips"      get_local_ips;


(*============*)
(* Threading: *)
(*============*)
(* The abstract thread type: *)
type thread_t = 'a;

(* "thread_create": *)
external thread_create_intern : (unit -> unit) -> int -> int -> thread_t =
        "tnio_thread_create";

value thread_create:
    ?stack_size_k:int -> ?priority:int -> (unit -> unit) -> thread_t =

fun ?(stack_size_k=256) ?(priority=0) thr_body ->
    thread_create_intern thr_body stack_size_k priority;

(* "thread_self": *)
external thread_self : unit -> thread_t =
        "tnio_thread_self";

(* "thread_id":   *)
external thread_id   : thread_t -> int  =
        "tnio_thread_id";

(* "thread_by_id":   *)
external thread_by_id: int -> thread_t  =
        "tnio_thread_by_id";

(* "invalid_thread": *)
external get_invalid_thread: unit -> thread_t =
        "tnio_invalid_thread";

value invalid_thread: thread_t = get_invalid_thread ();

(* "invalid_thread_id": *)
external get_invalid_thread_id: unit -> int =
        "tnio_invalid_thread_id";

value invalid_thread_id:   int = get_invalid_thread_id ();

(* "thread_yield": *)
external thread_yield: unit -> unit =
        "tnio_thread_yield";

(* "thread_sleep": *)
external thread_sleep: float-> unit =
        "tnio_thread_sleep";

(* "thread_stop":  *)
external thread_stop :  thread_t -> float -> unit =
        "tnio_thread_stop";

(* "thread_resume": *)
external thread_resume: thread_t -> unit =
        "tnio_thread_resume";

(* "thread_kill":
   If we kill ourselves (i.e., "exit"), the result is of any type; otherwise,
   it's unit:
*)
external thread_kill_intern: thread_t -> 'a =
        "tnio_thread_kill";

value thread_kill: thread_t -> unit =
fun t ->
    ignore (thread_kill_intern t);

value thread_exit  : unit -> 'a =
fun () ->
    thread_kill_intern (thread_self ());

(* Running the Scheduer: *)
external threads_scheduler: unit -> unit =
        "tnio_threads_scheduler";

(* "threads_cound": *)
external threads_count: unit -> int =
        "tnio_threads_count";

(* "exit" from the whole program and all its threads: *)
external exit : int  -> 'a = "tnio_exit";

(* XXX The rest of Threads API still needs to be implemented! *)

(* CPU utilisation statistics: *)
external get_cpu_utilisation : unit -> float = "tnio_get_cpu_utilisation";


(*========================*)
(*  Condition Variables:  *)
(*========================*)
(*  A cond var is simply a (mutable) list of waiting threads: *)
type  cond_var_t   =
{
    n_waiting: mutable int;
    queue    : mutable list thread_t
    (* Invariant: List.length queue = n_waiting *)
};

(*------------------*)
(* "cond_var_init": *)
(*------------------*)
value cond_var_init: unit -> cond_var_t =
fun () ->
{
    n_waiting = 0;
    queue     = []
};

(*------------------*)
(* "cond_var_wait": *)
(*------------------*)
value cond_var_wait: cond_var_t -> float -> unit =
fun c deadline ->
    let t = thread_self () in
    do {
        (* Add the thread to the waiting queue FIRST: *)
        c.n_waiting := c.n_waiting+1;
        c.queue     := c.queue @ [t];

        (* Now stop ourselves: *)
        thread_stop t deadline
    };

(*--------------------*)
(* "cond_var_signal": *)
(*--------------------*)
value rec cond_var_signal: ?n_threads:int -> cond_var_t -> unit =
fun ?(n_threads=max_int) c ->
try
    if  n_threads <= 0 || c.queue = []
    then
        ()  (* Nothing to do *)
    else
        (* Remove the head thread from the waiting list, and resume it: *)
        let hd_thread = List.hd c.queue in
        let others    = List.tl c.queue in
        do {
            c.queue     := others;
            c.n_waiting := c.n_waiting - 1;
            thread_resume  hd_thread;

            (* Do the rest: *)
            cond_var_signal ~n_threads:(n_threads-1) c
        }
with
[Queue.Empty ->
    () (* Ignore this condition in any case *)
];

(*------------------*)
(* "cond_var_info": *)
(*------------------*)
value cond_var_info: cond_var_t -> (int * (list thread_t)) =
fun c ->
    (c.n_waiting, c.queue);

(*===============*)
(*  Semaphores:  *)
(*===============*)
type sem_t =
{
    sem_owner:  mutable int; (* ID of the thread which locked this sem, or
                                invalid value (-1) if not locked by anyone *)
    sem_sync :  cond_var_t;
    sem_id   :  int          (* Assigned by the user; for debugging only   *)
};

(*---------------*)
(*  "sem_init":  *)
(*---------------*)
value sem_init: int -> sem_t =
fun id ->
{
    sem_owner = invalid_thread_id;
    sem_sync  = cond_var_init ();
    sem_id    = id
};

(*---------------*)
(*  "sem_lock":  *)
(*---------------*)
(*  Non-pre-emptive TNIO scheduling implies that we don't need any speacial
    means to ensure the atomicity of the locking operation:
*)
value rec sem_lock: sem_t -> unit =
fun sem ->
    let self_id = thread_id (thread_self ()) in
    if  self_id = invalid_thread_id
    then
        failwith "Tnio.sem_lock: Cannot be called from the main thread"
    else
    if  sem.sem_owner = invalid_thread_id
    then
    do{
        (* The semaphore is unlocked, so lock it and proceed. It is also
           allowed to lock it twice by the same thread:
        *)
        sem.sem_owner:= self_id;
        Printf.printf "Thread %d successfully locked semaphore %d\n"
                      self_id sem.sem_id;
        flush stdout
    }
    else
    if  sem.sem_owner = self_id
    then
    do{
        Printf.printf "Thread %d AGAIN locked semaphore %d\n"
                      self_id sem.sem_id;
        flush stdout;
        ()
    }
    else
    do{ (* The semaphore is locked; suspend ourselves on the cod var,
            waiting for the current owner to unlock it: *)
        cond_var_wait sem.sem_sync infinite_deadline;

        (* Once we are released, try to lock it again -- tail-recursive call: *)
        sem_lock sem
    };

(*-----------------*)
(*  "sem_unlock":  *)
(*-----------------*)
(*  "force" allows the caller to unlock a semaphore which it does not own --
    DANGEROUS!
*)
value sem_unlock: ?force:bool -> sem_t -> unit =
fun ?(force=False) sem ->
    let self_id = thread_id (thread_self ()) in
    if  self_id = invalid_thread_id
    then
        failwith "Tnio.sem_unlock: Cannot be called from the main thread"
    else
    if  sem.sem_owner = invalid_thread_id
    then
        ()  (* The semaphore is not locked, it's OK *)
    else
    (* The semaphore is locked. Only the thread which owns it, can unlock it,
       unless the "force" mode is set:
    *)
    if (not force) && (sem.sem_owner <> self_id)
    then
        failwith (Printf.sprintf
                 "Tnio.sem_unlock: Cannot unlock %d: caller_id=%d, owner_id=%d"
                 sem.sem_id self_id sem.sem_owner)
    else
    do{     (* Remove the lock, and wake up 1 waiting thread (if any) *)
        
        sem.sem_owner := invalid_thread_id;
        cond_var_signal ~n_threads:1 sem.sem_sync;
        Printf.printf "Thread %d UNLOCKED semaphore %d\n"
                    self_id sem.sem_id
    };

(*=========*)
(*  Misc:  *)
(*=========*)
(*  "pass_tsock": *)
external pass_tsock: tsock -> thread_t -> unit =
        "tnio_pass_tsock";

(*  "touch_file": *)
external touch_file: string -> unit =
        "tnio_touch_file";

(*-----------------*)
(*  "bind_retry":  *)
(*-----------------*)
(*  If an attempt to bind to a given addr fails with "EADDRINUSE", this may
    be due to the socket being in "close-wait" state-- retry for as long as
    20 minutes before giving up.
    Normally, a socket can be in the "TIME_WAIT" state for no longer than 4
    minutes (1 minute on Linux);  but there is a bug in various versions of
    the Linux kernel,  a socket can also be stuck in the "FIN_WAIT1" state,
    and for much longer -- 5-10-15 minutes; so we use 20 minutes as a "safe"
    upper boundary, although it is by far too much:
*)
value bind_wait     = 20.0 *. 60.0;
value bind_interval = 5.0;

value bind_retry: tsock -> Unix.sockaddr -> unit =
fun sock addr ->
    let rec bind_rec =
    fun rem_time ->
        let ok =
            try
                do {bind sock addr; True}
            with
            [ Unix.Unix_error Unix.EADDRINUSE _ _ ->
                if  rem_time > 0.0
                then False    (* Will cause re-iteartion  *)
                else
                do {
                    (* Try to bind forcefully,  but this will still not work
                       if another process is bound to the socket -- it would
                       only do if the socket is (still, somehow) in the TIME_
                       WAIT state:
                    *)
                    setsockopt sock Unix.SO_REUSEADDR True;
                    bind sock addr;
                    True      (* All done, if we get here *)
                }
            ]
        in
        if   ok
        then ()
        else
        do {
            thread_sleep bind_interval;
            bind_rec    (rem_time -. bind_interval)
        }
    in
    (* Apply it: *)
    bind_rec bind_wait;

(*--------------------*)
(* "set_sig_handler": *)
(*--------------------*)
(* See the module interface for details: *)
type  sig_handler_pos =
    [ Handler_Replace | Handler_Attach_Before | Handler_Attach_After ];

type  sig_behaviour =
    [ Sig_Ignore
    | Sig_Default
    | Sig_Handler_Gen  of (int -> unit)         (* Handler *)
    | Sig_Handler_Exit of (int -> unit) and int (* Handler, Exit Code *)
    ];

(* "wrap_sig_behaviour": *)

value wrap_sig_behaviour: sig_behaviour -> Sys.signal_behavior =
fun
    [ Sig_Ignore            -> Sys.Signal_ignore
    | Sig_Default           -> Sys.Signal_default
    | Sig_Handler_Gen  f    -> Sys.Signal_handle f
    | Sig_Handler_Exit f rc -> Sys.Signal_handle
                               (fun sn -> do {f sn; exit rc})
    ];

(* The following table stores information on all signal handlers set  via the
   TNIO interface. NB: it will get mixed-up if a handler is set directly (via
   Sys). Initially, all signals are IGNORED --  "exit_setup" below does this.
   We do NOT initialise the "expl_handlers" table here,  as (1) user-visible
   signal numbers are different from the internal ones (1..SIGRTMAX), and (2)
   we only need to keep information on explicitly-set handlers ("Handler_Gen"
   and "Handler_Exit"), anyway. So the table will be initialised dynamically:
*)
value expl_handlers: Hashtbl.t int sig_behaviour = Hashtbl.create 100;

external exit_setup: unit -> unit = "tnio_exit_setup";
(* Invoke it right now:    *)
exit_setup ();

(* "combine_sig_handlers": *)
value combine_sig_handlers:
    (int -> unit) -> (int -> unit) -> sig_handler_pos -> (int -> unit) =
fun oldh newh pos ->
    match pos with
    [ Handler_Replace ->
        newh
    | Handler_Attach_Before ->
        fun sn -> do{ newh sn; oldh sn }

    | Handler_Attach_After  ->
        fun sn -> do{ oldh sn; newh sn }
    ];

(* "combine_sig_behaviours".
   Here "new_beh" can only be "Sig_Handler_Gen" or "Sig_Handler_Exit":
*)
value combine_sig_behaviours:
    int -> sig_behaviour -> sig_handler_pos -> sig_behaviour =

fun signo new_beh pos    ->
    let curr_beh =
        try Hashtbl.find expl_handlers signo
        with
        [ Not_found -> Sig_Ignore ] (* This will cause an error below *)
    in
    match curr_beh with
    [ Sig_Ignore
    | Sig_Default ->
        (* The corresp "Sys.signal" call returned an explicit handler, but here
           we get Ignore or Default. Thus, that handle was set directly via Sys,
           by-passing the TNIO mechanism: this is an error:
        *)
        failwith
        "Tnio.combine_sig_behaviours: Inconsistent direct signal set detected"

    (* XXX: the following logic is questionable, but for now, it may be OK.
       If any of the handlers is of the Exit kind, we do Exit:
    *)
    | Sig_Handler_Gen f ->
        match new_beh with
        [ Sig_Handler_Gen  g    ->
            Sig_Handler_Gen  (combine_sig_handlers f g pos)

        | Sig_Handler_Exit g rc ->
            Sig_Handler_Exit (combine_sig_handlers f g pos) rc

        | _ -> assert False
        ]
    | Sig_Handler_Exit f rcf ->
        match new_beh with
        [ Sig_Handler_Gen  g ->
            Sig_Handler_Exit
                (combine_sig_handlers f g pos) rcf

        | Sig_Handler_Exit g rcg ->
            Sig_Handler_Exit
                (combine_sig_handlers f g pos)
                (if pos <> Handler_Attach_Before then rcf else rcg)

        | _ -> assert False
        ]
    ];

(* "set_sig_handler" itself: *)

value set_sig_handler:
    int -> ?new_handler_pos:sig_handler_pos  -> sig_behaviour -> unit =

fun signo ?(new_handler_pos=Handler_Replace) new_beh ->
    let comb_beh =
        match new_beh with
        [ Sig_Ignore
        | Sig_Default ->
            (* Such behaviours can only replace the existing one (any);
               but we DON'T signal an error if other "pos" was requested:
            *)
            new_beh

        | _ ->
            (* Handlers can be combined with an existing handle (if any),
               if requested:
            *)
            let   curr_sys_beh  = Sys.signal signo Sys.Signal_ignore in
            match curr_sys_beh with
            [ Sys.Signal_handle _ ->
                (* Yes, both curr and new behaviours are explicit handlers
                   -- combine them using the info in "expl_handlers":
                *)
                combine_sig_behaviours signo new_beh new_handler_pos
            | _ ->
                (* Other kinds of "curr_beh" cannot be combined with the
                   "new_beh", so just wrap up the latter.   It is NOT an
                   error if other "pos" is specified:
                *)
                new_beh
            ]
        ]
    in
    do {
        (* Memoise the setting: *)
        Hashtbl.replace expl_handlers signo comb_beh;

        (* Set the handler, and, just in case if the signal is blocked,
           unblock it:
        *)
        Sys.set_signal signo (wrap_sig_behaviour comb_beh);
        ignore (Unix.sigprocmask Unix.SIG_UNBLOCK [signo]);
    };

