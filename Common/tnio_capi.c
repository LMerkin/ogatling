// vim:ts=4:syntax=c
//===========================================================================//
//                              "tnio_capi.c":                               //
//                                                                           //
//    C API wrapping around the OCaml "Tnio" module. Although the latter     //
//    module is itself implemented mostly in C, it uses OCaml exceptions,    //
//    so we can't directly link C programs to its internals:                 //
//                                                                           //
//                   (C) Explar Technologies Ltd, 2004                       //
//===========================================================================//
//  This C file is to be compiled into a library which can be used as a "drop-
//  in" substitute for the standard Socket API calls.  Only those calls which
//  have a non-trivial "Tnio" implementation are provided here; for all other
//  Socket API calls, their standard implementation can be used directly:
//
//  NB: These functions can be called  ONLY AFTER "Tnio" was initialised!

#include <caml/mlvalues.h>
#include <caml/callback.h>
#include <caml/alloc.h>
#include "socketaddr.h"
#include "tnio_capi.h"

#include <stdio.h>

//-----------------------//
//  Deadline in seconds: //
//-----------------------//
//  Once set, it applies to all subsequent blocking operations within the
//  current thread -- until the deadline expires.
//
extern value  tnio_invalid_deadline;

//  The "set_next_deadline" and "get_next_deadline" functions are analogous
//  to "tnio_set_next_deadline" and "tnio_get_next_deadline",  but they use
//  the C parameters/result types, whereas the former use OCaml types.  The
//  deadline is actually stored in the TCP of the calling thread:
//
int TNIO_CAPI_set_next_deadline (double deadline)
{
    value res = callback_exn
                (* caml_named_value ("tnio_set_next_deadline"),
                copy_double (deadline));

    if  (Is_exception_result (res))
        return -1;
    return 0;
}

double TNIO_CAPI_get_next_deadline ()
{
    value res = callback_exn
                (* caml_named_value ("tnio_get_next_deadline"),
                 Val_unit);

    if  (Is_exception_result (res))
        return 0.0;
    return Double_val(res);
}

//-----------------------------//
//  Default kernel buff size:  //
//-----------------------------//
//  If set, it will be used for subsequent "socket" and "accept" calls. Unlike
//  the deadline (see above) which is written directly into the calling thread's
//  TCP, the deafult kernel buff size for a socket to be created is stored in a
//  GLOBAL var, and then used by a subsequent "socket" or "accept" call -- non-
//  pre-emptive scheduling guarantees that this is OK.  The client can directly
//  manupulate this global var:
//
int TNIO_CAPI_kern_buff_size = 0;

//---------------------//
// "TNIO_CAPI_socket": //
//---------------------//
int TNIO_CAPI_socket (int domain, int type, int proto)
{
    value args[4] =
    {
         Val_int (TNIO_CAPI_kern_buff_size),
         Val_int (domain),
         Val_int (type),
         Val_int (proto)
    };
    value res = callbackN_exn (*caml_named_value ("tnio_socket_c"), 4, args);

    if  (Is_exception_result (res))
        return -1;
    return Int_val(res);
}

//-------------------------//
// "TNIO_CAPI_socketpair": //
//-------------------------//
int TNIO_CAPI_socketpair (int domain, int type, int proto, int socks[2])
{
    value args[4] =
    {
         Val_int (TNIO_CAPI_kern_buff_size),
         Val_int (domain),
         Val_int (type),
         Val_int (proto)
    };
    value res = callbackN_exn
                (* caml_named_value ("tnio_socketpair_c"), 4, args);

    if  (Is_exception_result (res))
    {
        socks[0] = -1;
        socks[1] = -1;
        return -1;
    }
    socks[0] = Int_val (Field (res, 0));
    socks[1] = Int_val (Field (res, 1));
    return 0;
}

//--------------------//
// "TNIO_CAPI_close": //
//--------------------//
int TNIO_CAPI_close (int ts)
{
    value res = callback_exn
                (* caml_named_value ("tnio_close"), Val_int (ts));

    if  (Is_exception_result (res))
        return -1;
    return 0;
}

//----------------------//
// "TNIO_CAPI_connect": //
//----------------------//
int TNIO_CAPI_connect (int ts, const struct sockaddr * saddr, socklen_t alen)
{
    // For simplicity of re-iusing the OCaml functionality, we will pass the
    // address to connect to as an OCaml value. NB: close "ts" on error here:
    value mladdr= alloc_sockaddr ((union sock_addr_union *) saddr, alen, ts);

    // The deadline must already be set, so here we only pass a placeholder:
    value res = callback3_exn
                (* caml_named_value ("tnio_connect"),
                 Val_int (ts),
                 mladdr,
                 tnio_invalid_deadline);

    if  (Is_exception_result (res))
        return -1;
    return 0;
}

//---------------------//
// "TNIO_CAPI_accept": //
//---------------------//
int TNIO_CAPI_accept (int ts, struct sockaddr * saddr, socklen_t * alen)
{
    // The deadline must already be set, so here we only pass a placeholder:
    value res = callback3_exn
                (* caml_named_value ("tnio_accept3"),
                 Val_int (TNIO_CAPI_kern_buff_size),
                 Val_int (ts),
                 tnio_invalid_deadline);

    if  (Is_exception_result (res))
        return -1;

    value ns     = Field (res, 0);
    value mladdr = Field (res, 1);

    get_sockaddr   (mladdr, (union sock_addr_union *) saddr, alen);
    return Int_val (ns);
}

// In the following we assume that "buff" is a raw C memory. To pass it to
// OCaml, we can safely cast it into a "value", but have to use C-specific
// API functions:

//-------------------//
// "TNIO_CAPI_read": //
//-------------------//
ssize_t TNIO_CAPI_read (int ts, void * buff, size_t len)
{
    // NB: "tnio_next_deadline" is applied implicitly by the callee!
    value     res = callback3_exn
                    (* caml_named_value ("tnio_read_c"),
                     Val_int (ts),
                     (value) buff,
                     Val_int(len));

    if  (Is_exception_result (res))
        return -1;
    return (ssize_t) (Int_val (res));
}

//-------------------//
// "TNIO_CAPI_recv": //
//-------------------//
ssize_t TNIO_CAPI_recv (int ts, void * buff, size_t len, int flags)
{
    // NB: "tnio_next_deadline" is applied implicitly by the callee!
    value args[4] = {Val_int (ts),
                     (value) buff,
                     Val_int(len),
                     Val_int(flags)};

    value     res = callbackN_exn
                    (* caml_named_value ("tnio_recv_c"), 4, args);

    if  (Is_exception_result (res))
        return -1;
    return (ssize_t) (Int_val (res));
}

//-----------------------//
// "TNIO_CAPI_recvfrom": //
//-----------------------//
ssize_t TNIO_CAPI_recvfrom
    (int ts,    void * buff,  size_t len,  int flags,
     struct sockaddr * saddr, socklen_t * alen)
{
    value args[4] = {Val_int (ts),
                     (value) buff,
                     Val_int(len),
                     Val_int(flags)};

    value     res = callbackN_exn
                    (* caml_named_value ("tnio_recvfrom_c"), 4, args);

    if  (Is_exception_result (res))
        return -1;

    value got    = Field (res, 0);
    value mladdr = Field (res, 1);

    get_sockaddr   (mladdr, (union sock_addr_union *) saddr, alen);
    return (ssize_t) Int_val(got);
}

//--------------------//
// "TNIO_CAPI_write": //
//--------------------//
ssize_t TNIO_CAPI_write (int ts, void * buff, size_t len)
{
    value     res = callback3_exn
                    (* caml_named_value ("tnio_write_c"),
                     Val_int (ts),
                     (value) buff,
                     Val_int(len));

    if  (Is_exception_result (res))
        return -1;
    return (ssize_t) (Int_val (res));
}

//-------------------//
// "TNIO_CAPI_send": //
//-------------------//
ssize_t TNIO_CAPI_send (int ts, void * buff, size_t len, int flags)
{
    value args[4] = {Val_int (ts),
                     (value) buff,
                     Val_int(len),
                     Val_int(flags)};

    value     res = callbackN_exn
                    (* caml_named_value ("tnio_send_c"), 4, args);

    if  (Is_exception_result (res))
        return -1;
    return (ssize_t) (Int_val (res));
}

//---------------------//
// "TNIO_CAPI_sendto": //
//---------------------//
ssize_t TNIO_CAPI_sendto
    (int ts, void * buff, size_t len, int flags,
     const struct sockaddr *   saddr, socklen_t alen)
{
    // For simplicity of re-iusing the OCaml functionality, we will pass the
    // address to connect to as an OCaml value. NB: close "ts" on error here:
    value mladdr  = alloc_sockaddr ((union sock_addr_union *) saddr, alen, ts);

    value args[5] = {Val_int (ts),
                     (value) buff,
                     Val_int(len),
                     Val_int(flags),
                     mladdr};

    value     res = callbackN_exn
                    (* caml_named_value ("tnio_sendto_c"), 5, args);

    if  (Is_exception_result (res))
        return -1;
    return (ssize_t)  Int_val(res);
}
