(*pp camlp4r *)
(* vim:ts=4:syntax=ocaml
*)
(*===========================================================================*)
(*                               "gen_server.ml:"                            *)
(*           Implements the "Workers Pool" Concurrent Design Pattern         *)
(*                      (C) Explar Technologies Ltd, 2004                    *)
(*===========================================================================*)
(* BUT: There is no dedicated "supervisor" thread here, workers supervise them-
   selves and each other!
*)
(*===============*)
(*  Data Types:  *)
(*===============*)
(*--------------------*)
(*  "server_config":  *)
(*--------------------*)
(*  Documented in more detail in the module interface:  *)

type server_config 'j 's 'r =
{
    (* Configuration for worker threads: *)
    max_all_threads     : int;
    max_idle_threads    : int;
    min_idle_threads    : int;
    init_threads        : int;
    worker_priority     : int;
    worker_stack_k      : int;

    (* Configuration for the queue of client requests: *)
    max_queue_len       : int;

    (* State Management: *)
    init_worker_state   : unit -> 's;
    cleanup_state       : 's   -> unit;
    reset_state_on_error: bool;

    (* The actual processing function invoked by worker threads. It also up-
       dates the worker's state.   A deadline for completing the job is made
       known to the "proc_func":
    *)
    proc_func           : 'j   -> 's -> float -> ('r * 's)
};

(*------------------*)
(*  "server_info":  *)
(*------------------*)
(*  The server status, as returned by "get_server_info": *)

type server_info 'j 's 'r =
{
    server_config    : server_config 'j 's 'r;
    curr_all_threads : int;
    curr_idle_threads: int;
    curr_queue_len   : int
};

(*------------*)
(*  "job_t":  *)
(*------------*)
type job_t 'j 'r =
[
    Do_Job of 'j and Tnio.cond_var_t and (ref (option 'r))
           and float and (ref float)

    (* In this case, the client waits on the cond var  for the result
       to be computed, and receives the result  via the ref provided.
       Also contains a deadline for the worker, and a variable for its
       internal response time:
    *)
|   Terminate
    (* Nothing more to do! *)
];

(*------------------*)
(*  "Threads_Set":  *)
(*------------------*)
module Threads_Ord =
struct
    type  t = Tnio.thread_t;

    value compare: t -> t -> int =
    fun l r ->
        compare (Tnio.thread_id l) (Tnio.thread_id r);
end;
module Threads_Set = Set.Make (Threads_Ord);

(*---------------*)
(*  "server_t":  *)
(*---------------*)
type server_t 'j 's 'r =
{
    (* Configuration of this server: *)
    config       : mutable server_config 'j 's 'r;

    (* All the worker threads (idle & active) are registered here: *)
    all_workers  : mutable Threads_Set.t;

    (* The cond var the idle threads are waiting on: *)
    jobs_barrier : Tnio.cond_var_t;

    (* The queue of incoming jobs. If the client wants a value to
       be returned as a result, it also provides a variable where
       to put it:
    *)
    jobs_queue   : Queue.t (job_t 'j 'r)
};

(*=============================*)
(*  Server-Side Functionality: *)
(*=============================*)
(*--------------------*)
(*  "create_worker":  *)
(*--------------------*)
(*  It can be called from the main thread or from any other thread: *)

value rec create_worker: server_t 'j 's 'r -> unit =
fun server ->
    if  Threads_Set.cardinal server.all_workers >=
        server.config.max_all_threads
    then
        (* Don't create anything -- there are already too many threads! *)
        ()
    else
        (* Create a new thread: *)
        let new_thread = Tnio.thread_create
                         ~stack_size_k:server.config.worker_stack_k
                         ~priority    :server.config.worker_priority
                         (fun ()    -> worker_body_startup  server)
        in
        (* Save it in the "all_workers" set: *)
        server.all_workers := Threads_Set.add new_thread server.all_workers


(*--------------*)
(*  "get_job":  *)
(*--------------*)
(*  Invoked by worker threads: *)

and get_job: server_t 'j 's 'r -> job_t 'j 'r =
fun server ->
    let idle_threads  = fst (Tnio.cond_var_info server.jobs_barrier)
    in
    (*  Check whether a job is available now:  *)
    if  Queue.is_empty server.jobs_queue
    then
        (* No Jobs: *)
        if  idle_threads >= server.config.max_idle_threads
        then
        do {
            (* The thread terminates instead of waiting if there would be
               too many idle threads:
            *)
            Terminate
        }
        else
        do {
            (* Wait on a cond var, and try again -- if there are no false
               wake-ups, a job should be available after the 1st waiting.
               In this case, there is no finite deadline for waiting:
            *)
            Tnio.cond_var_wait server.jobs_barrier infinity;

            get_job server;
        }
    else
    do {
        (* Yes, a job is readily available -- take it. However, this may
           reduce the number of idle threads below the limit --  in that
           case, we create more worker threads    (but not exceeding the
           over-all "max_all_threads" limit) -- THAT IS WHY we have mutu-
           al recursion here!
        *)
        for i = idle_threads+1 to server.config.min_idle_threads
        do {
            create_worker server;
        };
        Queue.pop server.jobs_queue;
    }

(*--------------------------*)
(*  "worker_body_startup":  *)
(*--------------------------*)
(*  Creates the state WITHIN the thread, then enters "worker_body_loop": *)
and worker_body_startup: server_t 'j 's 'r -> unit =
fun server ->
    let init_state = server.config.init_worker_state () in
    worker_body_loop server init_state

(*-----------------------*)
(*  "worker_body_loop":  *)
(*-----------------------*)
(* Iterative function executed by each worker thread  associated with the
   given server.    Note that the worker thread carries its state through
   the loop of operation:
*)
and worker_body_loop: server_t 'j 's 'r -> 's -> unit =
fun server state ->
    let (new_state, cont_flag) =
    try
        (* Get and process the next job: *)
        match get_job server with
        [ Terminate ->
            (* Nothing to do for this thread: *)
            (state, False)

        | Do_Job j c res deadline rt ->
          let before = Unix.gettimeofday () in
          let (state', err) =
          try
            (* Process the arg, put the result into the output location,  and
               signal the completion to the client. If an error occurs during
               processing, a signal is still sent to the client,  but it will
               receive no result in that case. The error is then re-raised in
               the server -- XXX if we implement Ada-style rendezvous, it will
               also propagate to the client:
            *)
            let (r, st) = server.config.proc_func j state deadline in
            do {
                res.val := Some r;
                (Some st,  None)
            }
          with
            [hmm -> (None, Some hmm)]
          in
          let after = Unix.gettimeofday () in
          do {
            (* Record the internal response time: *)
            rt.val := after -. before;

            (* Signal completion: *)
            Tnio.cond_var_signal ~n_threads:1 c;

            match err with
            [None     -> (Option.get state', True)
            |Some hmm -> raise hmm]
          }
        ]
    with
    [_ ->
        (* Any exceptions here are tolerated, and new working iteration
           will be started -- but with which state?
        *)
        if  server.config.reset_state_on_error
        then
        do {
            (* Re-set the current state: *)
            server.config.cleanup_state state;

           (server.config.init_worker_state(), True)
        }
        else
            (* Keep the previous state: *)
            (state, True)
    ]
    in
    if  cont_flag
    then
        (* New iteration: *)
        worker_body_loop server new_state
    else
    do {
        (* Do a clean-up, and terminate now. Don't forget to remove this
           thread from the "all_workers" set:
        *)
        try  server.config.cleanup_state state
        with [_ -> ()];

        server.all_workers :=
            Threads_Set.remove (Tnio.thread_self()) server.all_workers
    };

(*======================*)
(*  Server Management:  *)
(*======================*)
(*--------------------*)
(*  "create_server":  *)
(*--------------------*)
(*  Creates a "server_t" object, including all the worker threads.  If the
    Threads Scheduler is already running (so that this function was itself
    invoked from within a running thread), the server threads will be star-
    ted by the Scheduler in a normal way:
*)
value create_server: server_config 'j 's 'r -> server_t 'j 's 'r =
fun conf ->
    (* Check the water-marks. Note that there MUST be at least 1 spare
       thread at any time, as the worker threads  actually create each
       other; if none exists, the server dies:
    *)
    if  not (conf.min_idle_threads >= 1 &&
             conf.min_idle_threads <= conf.init_threads     &&
             conf.init_threads     <= conf.max_idle_threads &&
             conf.max_idle_threads <= conf.max_all_threads  &&
             conf.max_queue_len    >= 1)
    then
        invalid_arg "Gen_server.create_server: Invalid water-marks"
    else
    (* The initial "server" object: *)
    let server =
    {
        config       = conf;
        all_workers  = Threads_Set.empty;
        jobs_barrier = Tnio.cond_var_init ();
        jobs_queue   = Queue.create ()
    }
    in
    do {
        (* Create the initial set of the worker threads: *)
        for i=1 to conf.init_threads
        do {
            create_worker server;
        };
        (* Return the server object: *)
        server
    };

(*----------------------*)
(*  "get_server_info":  *)
(*----------------------*)
value get_server_info: server_t 'j 's 'r -> server_info 'j 's 'r =
fun server ->
{
    server_config    = server.config;
    curr_all_threads = Threads_Set.cardinal server.all_workers;
    curr_idle_threads= fst   (Tnio.cond_var_info server.jobs_barrier);
    curr_queue_len   = Queue.length server.jobs_queue
};

(*----------------------*)
(*  "shutdown_server":  *)
(*----------------------*)
type shutdown_mode =
[   Graceful
|   Discard_Queue
|   Emergency
];

(* NB: shutdown itself is currently ASYNCHRONOUS: the caller of this
   function does not wait until all threads actually terminate. Also
   note that the server object cannot be re-activated after shutdown:
*)
value shutdown_server: server_t 'j 's 'r -> shutdown_mode -> unit =
fun server mode ->
do {
    (* In any case, re-set the following water-marks: *)
    let dead_conf =
    {(server.config) with
        min_idle_threads = 0;
        max_idle_threads = 0;
        max_queue_len    = 0
    }
    in
    server.config := dead_conf;

    (* After that, no new jobs will be taken, and the threads finishing
       their current jobs will terminate rather than stay idle. We also
       need to cancel all threads  currently waiting on "jobs_barrier",
       otherwise they would be blocked indefinitely.
    *)
    List.iter
        (fun thr ->
        do {
            Tnio.thread_kill thr;
            server.all_workers:= Threads_Set.remove thr server.all_workers
        })
        (snd (Tnio.cond_var_info server.jobs_barrier));

    (* This is enough for the "Graceful" mode. In other modes:- *)
    if  mode <> Graceful
    then
    do {
        (* Discard the queue, notifying the clients by releasing them
           while their response entries are still "None":
        *)
        Queue.iter
        (fun
            [ Do_Job _ c _ _ _ -> Tnio.cond_var_signal ~n_threads:1 c
            | _ -> ()]
        )
        server.jobs_queue;

        Queue.clear server.jobs_queue;

        (* In the "Emergency" mode, also kill all the worker threads,
           but be careful not to kill ourselves  if this function is
           invoked from one of the workers:
        *)
        if  mode = Emergency
        then
            let our_id     = Tnio.thread_id (Tnio.thread_self ()) in
            let kill_self  = ref Tnio.invalid_thread_id in
            let () =
                Threads_Set.iter
                (fun thr ->
                    let id = Tnio.thread_id thr in
                    if  id = our_id
                    then
                        (* Kill ourselves, but later: *)
                        kill_self.val := id
                    else
                        (* Kill that thread now: *)
                        Tnio.thread_kill thr
                )
                server.all_workers
            in
            if  kill_self.val =  our_id &&
                kill_self.val <> Tnio.invalid_thread_id
            then
                Tnio.thread_kill (Tnio.thread_self ())
            else ()
        else ()
    }
    else ()
};

(*=============================*)
(*  Client-Side Functionality: *)
(*=============================*)
(*-----------------*)
(*  "submit_job":  *)
(*-----------------*)
exception Jobs_Queue_Full;
exception Job_Discarded;

value submit_job: server_t 'j 's 'r -> job_t 'j 'r -> unit =
fun   server job ->
do {
    (* Put the job into the queue: *)
    if  Queue.length server.jobs_queue >= server.config.max_queue_len
    then
        raise Jobs_Queue_Full
    else
        Queue.add job server.jobs_queue;

    (* Notify the waiting threads -- actually, just the head one: *)
    Tnio.cond_var_signal ~n_threads:1 server.jobs_barrier
};

(*-----------------------*)
(*  "call_server_sync":  *)
(*-----------------------*)
(*  The client submits a job and waits for its completion. The function
    returns the computed result and the internal worker's resp time;
    NB: the client can also measure the resp time  from their own pers-
    pective (by timimg invocations of this whole function):
*)
value call_server_sync:
    server_t 'j 's 'r -> 'j -> float -> ('r * float) =

fun server j deadline ->
    (* Create a cond var to wait on for the completion of the job: *)
    let c   = Tnio.cond_var_init () in

    (* Create the output area for the result -- "None" is used to avoid ini-
       tialising it to some default value:
    *)
    let res = ref None in
    let rt  = ref 0.0  in
    do {
        (* Submit the job: *)
        submit_job server (Do_Job j c res deadline rt);

        (* Wait for the completion:  *)
        Tnio.cond_var_wait c deadline;

        (* The result is NOT "None", unless the job was discarded on server
           shutdown:
        *)
        match res.val with
        [ None   -> raise Job_Discarded
        | Some r -> (r, rt.val)]
    };

(*------------------------*)
(*  "call_server_async":  *)
(*------------------------*)
(*  The client submits a job, but does not (yet) wait for its completion. The
    function returns the buffer variable and the cond var  which can later be
    used for waiting for the result.
    NB: unlike the synchronous case above, the resp time var returned here is
    important, as this time is difficult to measure from the client's perspec-
    tive alone:
*)
value call_server_async:
    server_t 'j 's 'r -> 'j -> float ->
    ((ref (option 'r)) * Tnio.cond_var_t * (ref float)) =

fun server j deadline ->
    (* The output area and the syncronisation var are as in the sync case: *)
    let c   = Tnio.cond_var_init () in
    let res = ref None in
    let rt  = ref 0.0  in
    do {
        submit_job server (Do_Job j c res deadline rt);

        (* Return immediately: *)
        (res, c, rt)
    };

(*------------------------*)
(*  "from_many_servers":  *)
(*------------------------*)
(*  A concurrent design pattern: submit requests  to multiple servers
    asynchrounously, and wait for all of them  to complete (or fail).
    Individual errors are ignored, and successful results then merged
    into a single list.
    NB: similar to the asynchronous case above, the "wait" primitive
    used in this function affects the resp time measured from the cli-
    ent's perspective, so the internal resp time info returned here is
    important:
*)
value from_many_servers:
    list 'j -> list (server_t 'j 's 'r) -> float -> list ('r * float) =

fun jobs servers deadline    ->
    let js =
        try  List.combine jobs servers
        with [_ -> invalid_arg ("Gen_server.from_many_servers: "^
                                "Different number of jobs and servers")]
    in
    (* Submit jobs to their resp servers: *)
    let subms =
        List.map
        (fun (job, server) -> call_server_async server job deadline)
        js
    in
    (* Wait for the completion of the jobs or the deadline reached: *)
    let rec wait_for_servers:
        list ((ref (option 'r)) * Tnio.cond_var_t * (ref float)) ->
        list ('r * float) ->
        list ('r * float) =

    fun sms ress ->
        match sms with
        [ [] ->
            ress (* Nothing more to wait for *)

        | [(res, cond, rt) :: ths] ->
          do {
            (* Maybe it has already completed? *)
            match res.val with
            [ Some _ ->
                () (* Yes, done!  *)

            | None ->
                (* No, must wait: *)
                try  Tnio.cond_var_wait cond deadline
                with [_ -> ()]
                (* E.g., ignore individual deadline over-runs *)
            ];
            (* If we got a successful result, attach it to the list,
               and wait for the rest:
            *)
            let ress' =
                match res.val with
                [ Some r -> [(r, rt.val) :: ress]
                | None   -> ress
                ]
            in
            wait_for_servers ths ress'
          }
        ]
    in
    (* Wait for all servers: *)
    wait_for_servers subms [];

