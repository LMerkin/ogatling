(*pp camlp4r *)
(* vim:ts=4:syntax=ocaml
*)
(*============================================================================*)
(*                             "cache_simple.mli":                            *)
(*              Cache with "relatively simple" clean-up policies              *)
(*                   (C) Explar Technologies Ltd, 2003--2004                  *)
(*============================================================================*)

(* The mutable type of Cache objects, parametrised by the Key and Val types:  *)
type cache_t 'k 'v  = 'c;

(* Configuration for creating a Cache: *)
type history_interp_t     = [LastAcc | TotalAccs];

type cache_config_t 'k 'v =
{
    (* Cache clean-up control:  when the maximum capacity w.r.t. the number of
       stored Keys ("conf_max_size") is reached, clean-up procedure is invoked
       automatically. It traverses all Keys, and removed those of them  (along
       with their values) which have rank <= conf_cl_threshold * max_rank. The
       rank is a  historically-accumulated  integer  which is either  the last
       look-up "time" (where the "time" is the access cycle, not a clock)  for
       a particular Key OR the total look-ups count for that Key, depending on
       the "conf_history_how" parameter  ("LastAcc" or "TotalAccs", resp):
    *)
    conf_max_size       : int;               (* Must be >= 0 *)
    conf_history_how    : history_interp_t;
    conf_cl_threshold   : float;             (* Must be in [0.0 .. 1.0] *)

    (* The following functions are optional: *)
    conf_string_of_key  : option ('k -> string);
    conf_finaliser      : option ('v -> unit)
};

exception Not_Enough_Space;

value create:   cache_config_t 'k 'v -> cache_t 'k 'v;

(* Store a (Key, Val) pair in the Cache. Multiple Vals can be kept for
   the same Key. Can raise a "Not_Enough_Space" exception:
*)
value store :       cache_t 'k 'v -> 'k -> 'v -> unit;

(* Return the list of Vals for a given Key: *)
value lookup:       cache_t 'k 'v -> 'k -> list 'v;

(* Remove a Key and all its Vals (finalised first if appropriate):   *)
value remove_key:   cache_t 'k 'v -> 'k -> unit;

(* Remove a particular Val (and the Key if that Val was the only one
   for its Key),  with Val finalisation if appropriate:
*)
value remove_val:   cache_t 'k 'v -> 'k -> 'v -> unit;

(* Remove all Cache contents (finalising them first if appropriate): *)
value remove_all:   cache_t 'k 'v -> unit;

(* Export the Cache contents as [(Key, [Val])]: *)
value export:       cache_t 'k 'v -> list ('k * (list 'v));

(* Export the Cache statistics: *)
type cache_stats =
{
    stat_curr_keys: int;
    stat_curr_vals: int;
    stat_hit_rate : float;
    stat_clean_ups: int
};
value get_stats:    cache_t 'k 'v -> cache_stats;

(* Export the Cache configuration used: *)
value get_config:   cache_t 'k 'v -> cache_config_t 'k 'v;

(* Verify the Cache integrity: raises an exception if check fails,
   otherwise returns ():
*)
value verify_integrity: cache_t 'k 'v -> unit;

