// vim:ts=4:syntax=c
//===========================================================================//
//                             "tnio_capi.h":                                //
//                 Specification of the minimum TNIO C API:                  //
//                 (C) Explar Technologies Ltd, 2004--2006                   //
//===========================================================================//
// NB: These C API functions still require linking via the OCaml compiler.

#ifndef __TNIO_C_API__
#define __TNIO_C_API__

#include <sys/socket.h>
#include <sys/types.h>

// Use "TNIO_ZERO_DEADLINE" to indicate a non-blocking operation.
// XXX: explicit definition here is BAD, as it must be cahnged if
// we change the main "Tnio" implementation:
//
#define TNIO_ZERO_DEADLINE 0.0

extern int     TNIO_CAPI_kern_buff_size;
extern int     TNIO_CAPI_set_next_deadline (double deadline);
extern double  TNIO_CAPI_get_next_deadline ();

extern int     TNIO_CAPI_socket (int domain, int type, int proto);
extern int     TNIO_CAPI_socketpair
                                (int domain, int type, int proto, int socks[2]);
extern int     TNIO_CAPI_close  (int ts);

extern int     TNIO_CAPI_connect(int ts, const struct sockaddr * saddr,
                                 socklen_t alen);

extern int     TNIO_CAPI_accept (int ts, struct sockaddr * saddr,
                                 socklen_t * alen);

extern ssize_t TNIO_CAPI_read   (int ts, void * buff, size_t len);
extern ssize_t TNIO_CAPI_recv   (int ts, void * buff, size_t len, int flags);
extern ssize_t TNIO_CAPI_recvfrom
                                (int ts, void * buff, size_t len, int flags,
                                 struct sockaddr * saddr, socklen_t * alen);

extern ssize_t TNIO_CAPI_write  (int ts, void * buff, size_t len);
extern ssize_t TNIO_CAPI_send   (int ts, void * buff, size_t len, int flags);
extern ssize_t TNIO_CAPI_sendto
                                (int ts, void * buff, size_t len, int flags,
                                 const struct sockaddr * saddr, socklen_t alen);

extern int     TNIO_CAPI_get_curr_thread_id ();
#endif
