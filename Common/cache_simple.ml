(*pp camlp4r *)
(* vim:ts=4:syntax=ocaml
*)
(*============================================================================*)
(*                               "cache_simple.ml":                           *)
(*              A cache with "relatively simple" clean-up policies            *)
(*                   (C) Explar Technologies Ltd, 2003--2004                  *)
(*============================================================================*)
(*==============*)
(*  "cache_t":  *)
(*==============*)
(* The Cache is a map Key => cached_data_t, so that multiple (but non-equal)
    Vals can be stored for a given Key. For that, an imperative "Hashtbl" is
    used. The stored data also include the per-Key "rank" which is interpre-
    ted according to the "history_how" parameter:
*)
type cached_data_t 'v =
{
    vals: mutable list 'v;
    rank: mutable int
};

type history_interp_t = [LastAcc | TotalAccs];

type cache_t 'k 'v    =
{
    data_storage : Hashtbl.t 'k (cached_data_t 'v);

    (* Clean-up-related parms: *)
    max_size     : int;
    history_how  : history_interp_t;
    cl_threshold : float;

    (* The current capacity of the Cache: *)
    curr_keys    : mutable int;
    curr_vals    : mutable int;

    (* Cache statistics: *)
    total_lookups: mutable int;
    ok_lookups   : mutable int;
    max_rank     : mutable int;
    clean_ups    : mutable int;

    (* Helper functions: *)
    string_of_key: option  ('k -> string);
    finaliser    : option  ('v -> unit)
};

(*=====================*)
(*  Cache Operations:  *)
(*=====================*)
(*---------------------*)
(*  "string_of_key":   *)
(*---------------------*)
(* Converts the given Key to a String, if the converstion function was
   supplied when the Cache instance was created:
*)
value string_of_key:  cache_t 'k 'v -> 'k -> string =
fun cache k ->
    match cache.string_of_key with
    [
        Some f -> f k
      | None   -> "<UNPRINTABLE>"
    ];

(*--------------------*)
(* "fail_multi_ver":  *)
(*--------------------*)
value fail_multi_ver:
      cache_t 'k 'v -> 'k -> list (cached_data_t 'v) -> 'a =

fun cache k hmm ->
    failwith (Printf.sprintf
             "Cache: %d versions of data in the DataStorage for Key %S"
             (List.length hmm) (string_of_key cache k));

(*------------*)
(*  "create": *)
(*------------*)
(* Create a new Cache object from a given Config: *)

type cache_config_t 'k 'v =
{
    conf_max_size       : int;
    conf_history_how    : history_interp_t;
    conf_cl_threshold   : float;
    conf_string_of_key  : option ('k -> string);
    conf_finaliser      : option ('v -> unit)
};

value create: (cache_config_t 'k 'v) -> (cache_t 'k 'v) =
fun c ->
    if  c.conf_max_size < 0 || c.conf_cl_threshold <= 0.0 ||
        c.conf_cl_threshold >= 1.0
    then
        invalid_arg "Cache_simple.create: Invalid config parm(s)"
    else
    {
        data_storage   =  Hashtbl.create c.conf_max_size;
        max_size       =  c.conf_max_size;
        history_how    =  c.conf_history_how;
        cl_threshold   =  c.conf_cl_threshold;
        curr_keys      =  0;
        curr_vals      =  0;
        total_lookups  =  0;
        ok_lookups     =  0;
        max_rank       =  0;
        clean_ups      =  0;
        string_of_key  =  c.conf_string_of_key;
        finaliser      =  c.conf_finaliser
    };

(*---------------*)
(*  "clean_up":  *)
(*---------------*)
exception Not_Enough_Space;

value clean_up: cache_t 'k 'v -> unit =
fun cache ->
    (* Traverse all the entries, removing those which have rank below the
       threshold.   XXX: it is apparently OK to modify a Hash Table while
       iterating over it:
    *)
    let new_max_rank = ref 0 in
    let thr_rank     = int_of_float (cache.cl_threshold *.
                                     float_of_int (cache.max_rank)) in
    do {
        Hashtbl.iter
        (fun key data ->
            if   data.rank <= thr_rank
            then
            do {
                (* Finalise the "vals" if appropriate: *)
                match cache.finaliser with
                [
                    Some f -> List.iter f data.vals
                  | None   -> ()
                ];
                (* Remove this "key": *)
                Hashtbl.remove cache.data_storage key;

                (* Modify the Cache parms: *)
                cache.curr_keys:= cache.curr_keys  - 1;
                cache.curr_vals:= cache.curr_vals  - (List.length data.vals)
            }
            else
                (* This key remains -- use it to adjust the max_rank: *)
                if  data.rank > new_max_rank.val
                then
                    new_max_rank.val := data.rank
                else
                    ()
        )
        cache.data_storage;

        (* Modify the parms: *)
        cache.max_rank := new_max_rank.val;
        cache.clean_ups:= cache.clean_ups + 1;

        (* Have we released at least one slot? *)
        if  cache.curr_keys = cache.max_size
        then
            raise Not_Enough_Space
        else
            ()
    };

(*------------*)
(*  "store":  *)
(*------------*)
(*  Stores a new (Key, Val) pair in the Cache:  *)

value store: cache_t 'k 'v -> 'k -> 'v -> unit =
fun cache k  v ->
do {
    if  cache.curr_keys = cache.max_size
    then
        (* Need to clean-up the Cache first. An exception is raised if the
           clean-up does not reclaim at least one slot:
        *)
        clean_up cache
    else
        ();

    (* We can now do the actual insertion: *)

    match Hashtbl.find_all cache.data_storage k with
    [
        [] ->
        (* The Key is not in the Cache yet, so it will be created: *)
            let new_data =
            {
                vals = [v];
                rank = 0
            }
            in
            do {
                Hashtbl.add cache.data_storage k new_data;

                cache.curr_keys := cache.curr_keys + 1;
                cache.curr_vals := cache.curr_vals + 1;
            }

      | [old_data] ->
            (* The Key is already there;  check whether the Val is there
               as well:
            *)
            if   List.mem v old_data.vals
            then
                (* Nothing to do: *)
                ()
            else
            do {
                (* Store the "v", preserving the order of insertions;
                   the Cache contents is updated in-place:
                *)
                old_data.vals   := old_data.vals @ [v];
                cache.curr_vals := cache.curr_vals + 1
            }

      | hmm ->
        fail_multi_ver cache k hmm
    ]
};

(*-------------*)
(*  "lookup":  *)
(*-------------*)
(* Returns the list (possibly empty) of Vals for a given Key: *)

value lookup: cache_t 'k 'v -> 'k -> list 'v =
fun cache k ->
do {
    cache.total_lookups := cache.total_lookups + 1;

    match Hashtbl.find_all cache.data_storage k with
    [
        []     -> []

      | [data] ->
                do{
                    (* Modify the statistics. The Cache contents are updated
                       in-place:
                    *)
                    cache.ok_lookups:= cache.ok_lookups + 1;

                    if  cache.history_how = LastAcc
                    then
                        data.rank := cache.total_lookups
                    else
                        data.rank := data.rank + 1;

                    if  data.rank > cache.max_rank
                    then
                        cache.max_rank := data.rank
                    else
                        ();

                    (* Return the vals extracted: *)
                    assert   (List.length data.vals > 0);
                    data.vals
                }

      | hmm    -> fail_multi_ver cache k hmm
    ]
};

(*-----------------*)
(*  "remove_val":  *)
(*-----------------*)
(* Removes a particular Val for a given Key from the Cache.  If the Val
   removed was the only one associated with its Key, the latter is also
   removed.  If the specified Key and/or Val is not found in the Cache,
   no action is taken:
*)
value remove_val: cache_t 'k 'v -> 'k -> 'v -> unit =
fun cache  k  v ->
    match Hashtbl.find_all cache.data_storage k with
    [
        [] ->
            (* This Key is not in the Cache at all -- ignore it: *)
            ()

      | [data] ->
            (* The Key is there, check for the Val:
               All Vals equal to "v" (in fact there can be no more than
               one such Val) are to be finalised and removed:
            *)
            let delete_this_val =
                match cache.finaliser with
                [
                    Some f ->
                        (* Equality test and possible finalisation: *)
                        fun v' ->
                            if  v' = v  then do{f v'; False} else True

                  | None   ->
                        (* Equality test only: *)
                        fun v' -> v' <> v
                ]
            in
            let vals'= List.filter  delete_this_val  data.vals  in

            let l    = List.length  data.vals in
            let l'   = List.length  vals'     in

            if  List.length data.vals < List.length vals'
            then
            do {
                (* Yes, the Val to be removed has been found, and already
                   finalised (if appropriate).    Since equal Vals in the
                   Cache are not allowed by construction, length of "data.
                   vals" must differ from that of "vals'" by 1:
                *)
                assert (l' = l-1);
                cache.curr_vals := cache.curr_vals - 1;

                if  l' = 0
                then
                do {
                    (* The Key is to be removed as well: *)
                    Hashtbl.remove  cache.data_storage k;

                    cache.curr_keys := cache.curr_keys -1
                }
                else
                    (* Modify "data.vals", but no need to write the "data"
                       back into the Cache:
                    *)
                    data.vals := vals'
            }
            else
                (* No appropriate Val to remove -- ignore it: *)
                ()

      | hmm ->
            (* This must not happen: *)
            fail_multi_ver cache k hmm
    ];

(*-----------------*)
(*  "remove_key":  *)
(*-----------------*)
(* Remove the given Key and all associated Vals from the Cache. The Vals
   to be removed are finalised first.  If the specified Key is not found
   in the Cache, no action is taken:
*)
value remove_key: cache_t 'k 'v -> 'k -> unit =
fun cache k->

    match Hashtbl.find_all cache.data_storage k with
    [
        [] ->
            (* This Key is not in the Cache -- ignore it: *)
            ()

      | [data] ->
        do {
            (* Finalise (if appropriate) and remove all "vals": *)
            match cache.finaliser with
            [
                Some f -> List.iter f data.vals
              | None   -> ()
            ];
            Hashtbl.remove    cache.data_storage k;

            cache.curr_keys:= cache.curr_keys  - 1;
            cache.curr_vals:= cache.curr_vals  - (List.length data.vals)

            (* NB: XXX: "cache.max_rank" is NOT decremented even if the
               highest-rank entry has been removed!
            *)
        }

      | hmm ->
            (* This must not happen: *)
            fail_multi_ver cache k hmm
    ];

(*-----------------*)
(*  "remove_all":  *)
(*-----------------*)
value remove_all: cache_t 'k 'v -> unit =
fun cache ->
do {
    (* Finalise (if appropriate) all "vals": *)
    match cache.finaliser with
    [
        Some f -> Hashtbl.iter
                  (fun k  data -> List.iter f data.vals) cache.data_storage
      | None   -> ()
    
    ];
    (* Clear the whole storage: *)
    Hashtbl.clear cache.data_storage;

    (* Re-set all the parms:    *)
    cache.curr_keys      := 0;
    cache.curr_vals      := 0;
    cache.total_lookups  := 0;
    cache.ok_lookups     := 0;
    cache.max_rank       := 0;
};

(*-------------*)
(*  "export":  *)
(*-------------*)
(* The Keys are exported in no particular order, but the Vals for each
   Key come in the order of insertion:
*)
value export: cache_t 'k 'v -> list ('k * (list 'v)) =
fun cache ->
    Hashtbl.fold (fun k data curr_exps -> [(k, data.vals) :: curr_exps])
                 cache.data_storage [];

(*----------------*)
(*  "get_stats":  *)
(*----------------*)
type cache_stats =
{
    stat_curr_keys: int;
    stat_curr_vals: int;
    stat_hit_rate : float;
    stat_clean_ups: int
};

value get_stats: cache_t 'k 'v -> cache_stats =
fun cache ->
    let
        hit_rate = if cache.total_lookups = 0
                   then 0.0
                   else float  (cache.ok_lookups) /.
                        float  (cache.total_lookups)
    in
    {
        stat_curr_keys = cache.curr_keys;
        stat_curr_vals = cache.curr_vals;
        stat_hit_rate  = hit_rate;
        stat_clean_ups = cache.clean_ups
    };

(*----------------*)
(*  "get_config": *)
(*----------------*)
(* Returns the config parms with which the Cache was created: *)

value get_config: cache_t 'k 'v -> cache_config_t 'k 'v =
fun cache ->
{
    conf_max_size       = cache.max_size;
    conf_history_how    = cache.history_how;
    conf_cl_threshold   = cache.cl_threshold;
    conf_string_of_key  = cache.string_of_key;
    conf_finaliser      = cache.finaliser
};

(*-----------------------*)
(*  "verify_integrity":  *)
(*-----------------------*)
value verify_integrity: cache_t 'k 'v -> unit =
fun cache ->
    (* Check the counts of Keys and Vals.  In addition, there must be no
       hidden bindings for the Keys, and each Key must be bound to a non-
       empty list of Vals:
    *)
    let (n_keys, n_vals) =
        Hashtbl.fold

        (fun k data (c_keys, c_vals) ->
            match Hashtbl.find_all  cache.data_storage k with
            [
                ([_::_] as hmm) ->
                    (* Hidden bindings for this Key: *)
                    fail_multi_ver cache k hmm

              | _ ->
                (
                    (* Check the "vals": *)
                    match data.vals with 
                    [
                        [] -> 
                            (* Error -- empty "vals": *)
                            failwith
                            (Printf.sprintf "Cache: Vals=[] for Key=%s"
                            (string_of_key cache k))

                      | _  ->
                            (* OK, increments the counts: *)
                            (c_keys + 1, c_vals + (List.length data.vals))
                    ]
                )
            ]
        )
        cache.data_storage  (0, 0)
    in
        if  (n_keys <> cache.curr_keys) || (n_vals <> cache.curr_vals)
        then
            failwith
            (Printf.sprintf
            "Cache: Declared (Keys, Vals): (%d, %d); Actually: (%d, %d)"
             cache.curr_keys cache.curr_vals n_keys n_vals)
        else
            ();

