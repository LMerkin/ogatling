(*pp camlp4r *)
(* vim:ts=4:syntax=ocaml
*)
(*===========================================================================*)
(*                               "http_client.ml":                           *)
(*                  Implementation of a Simple HTTP(S) Client:               *)
(*                      (C) Explar Technologies Ltd, 2004                    *)
(*===========================================================================*)
(*==========================*)
(*  HTTP Agents Management: *)
(*==========================*)
(*---------------------*)
(*  The Transport API: *)
(*---------------------*)
type transport 's =
{
    socket:         float -> 's;
    invalid_socket: 's;
    getsockname:    's -> Unix.sockaddr;
    bind:           's -> Unix.sockaddr -> unit;
    connect:        's -> Unix.sockaddr -> float -> unit;
    read:           Misc_utils.io_func 's;
    write:          Misc_utils.io_func 's;
    close:          's -> float -> unit
};

(*------------------------*)
(*  "http_agent_config":  *)
(*------------------------*)
type http_agent_config =
{
    conf_client_ips : mutable list Unix.inet_addr;
    conf_use_persist: mutable bool;
    conf_buff_size  : mutable int;
    conf_user_agents: mutable list string
};

(*------------------------------*)
(* "default_http_agent_config": *)
(*------------------------------*)
value default_http_agent_config: unit -> http_agent_config =
fun () ->
{
    conf_client_ips = [];
    conf_use_persist= True;
    conf_buff_size  = 256 * 1024;
    conf_user_agents= ["Gatling_Browser/0.1"]
};

(*----------------*)
(*  "http_agent": *)
(*----------------*)
type http_agent 's 'x =
{
    resolver    : mutable string -> list Unix.inet_addr;
    close_res   : mutable unit   -> unit;
    trans_http  : transport 's;                      (* For now, NOT mutable *)
    trans_https : option (transport 'x);             (* Ditto *)
    reader_http : Misc_utils.line_reader 's;         (* Ditto *)
    reader_https: option (Misc_utils.line_reader 'x);(* Ditto *)
    sock_http   : mutable 's;
    sock_https  : mutable option 'x;
    buff        : mutable string;
    use_persist : mutable bool;
    user_agents : mutable array string;
    client_ips  : mutable array Unix.inet_addr;
    curr_domain : mutable string;      (* The domain we are now connected to *)
    curr_secure : mutable bool         (* The current connection is secure   *)
};

(*------------------*)
(* "mk_http_agent": *)
(*------------------*)
value mk_http_agent:
    (string -> list Unix.inet_addr) -> (unit -> unit)        ->
    transport 's                    -> option (transport 'x) ->
    http_agent_config               ->
    http_agent 's 'x  =

fun reslvr close_resolvr tr_http tr_https  c ->
{
    resolver    = reslvr;
    close_res   = close_resolvr;
    trans_http  = tr_http;
    trans_https = tr_https;
    reader_http = new Misc_utils.line_reader tr_http.read 2048;
    reader_https=
        match tr_https with
        [ Some trs -> Some (new Misc_utils.line_reader trs.read 2048)
        | None     -> None
        ];
    buff        = String.create c.conf_buff_size;
    sock_http   = tr_http.invalid_socket;
    sock_https  =
        match tr_https with
        [ Some trs -> Some trs.invalid_socket
        | None     -> None
        ];
    use_persist = c.conf_use_persist;
    user_agents = Array.of_list c.conf_user_agents;
    client_ips  = Array.of_list c.conf_client_ips;
    curr_domain = "";
    curr_secure = False
};

(*-------------------*)
(* "get_all_config": *)
(*-------------------*)
(*  Actually returns:
    (resolver, close_res, transport_http, option_transport_https,
     config_proper):
*)
value get_all_config:
    http_agent 's 'x ->
    ((string -> list Unix.inet_addr) * (unit -> unit) * (transport 's) *
     (option (transport 'x))         * http_agent_config) =
fun agent ->
    let conf =
    {
        conf_client_ips = Array.to_list agent.client_ips;
        conf_use_persist= agent.use_persist;
        conf_buff_size  = String.length agent.buff;
        conf_user_agents= Array.to_list agent.user_agents
    }
    in
    (agent.resolver, agent.close_res, agent.trans_http, agent.trans_https,
     conf
    );

(*----------------*)
(* "close_agent": *)
(*----------------*)
(*  NB: actually, only one connection (either HTTPS or HTTPS) can be active at
    a time, but we close both sockets (if available) just to make sure.
*)
value close_agent: http_agent 's 'x -> float -> unit =
fun agent deadline ->
do {
    (* Close the Resolver  :  *)
    agent.close_res ();

    (* Close the HTTP socket: *)
    agent.trans_http.close   agent.sock_http deadline;
    agent.sock_http       := agent.trans_http.invalid_socket;

    (* Close the HTTPS socket, if available: *)
    match (agent.trans_https, agent.sock_https) with
    [ (None, None)       ->
        ()
    | (Some tr, Some so) ->
      do {
        tr.close so deadline;
        agent.sock_https  := Some tr.invalid_socket
      }
    | _ ->
        assert False
    ];
    (* Re-set the "curr_domain": *)
    agent.curr_domain := "";
    agent.curr_secure := False
};

(*---------------------------*)
(* "reconfigure_http_agent": *)
(*---------------------------*)
value get_client_ip: http_agent 's 'x -> Unix.inet_addr =
fun agent ->
try
    let saddr =
        if agent.curr_secure
        then
            (* Currently have an HTTPS connection: *)
            let tr = Option.get agent.trans_https in
            let so = Option.get agent.sock_https  in
            do {
                assert (so <> tr.invalid_socket);
                tr.getsockname so
            }
        else
            do {
                (* Currently have an HTTP connection: *)
                assert (agent.sock_http <> agent.trans_http.invalid_socket);

                agent.trans_http.getsockname agent.sock_http
            }
    in
    match saddr with
    [ Unix.ADDR_INET ip _ -> ip
    | _ -> assert False]
with
[_ -> Unix.inet_addr_any];


value reconfigure_http_agent:
    http_agent 's 'x -> http_agent_config -> float -> unit =
fun agent c deadline ->
do {
    (* If the "agent" holds a persistent connection, the latter may or may not
       need to be closed during re-configuration:
    *)
    let is_connected = agent.use_persist && agent.curr_domain <> "" in
    if  is_connected
    then
        (* Client IPs to be changed, and the curr IP disappears, or there will
           be no persistent connections anymore:
        *)
        let client_ip = get_client_ip agent in
        let ip_gone   =
            not (List.mem client_ip          c.conf_client_ips) &&
            not (List.mem Unix.inet_addr_any c.conf_client_ips) &&
            (c.conf_client_ips <> [])
        in
        if  ip_gone || (not c.conf_use_persist)
        then
            (* Yes, close the curr connection: *)
            close_agent agent deadline
        else
            ()
    else
        ();
    (* Now the new settings -- resolver and transport are fixed: *)

    if   String.length  agent.buff <> c.conf_buff_size
    then agent.buff  := String.create c.conf_buff_size
    else ();

    agent.user_agents:= Array.of_list c.conf_user_agents;
    agent.use_persist:= c.conf_use_persist;
    agent.client_ips := Array.of_list c.conf_client_ips
};

(*================================*)
(*  Very Simple HTTP/1.1 Client:  *)
(*================================*)
(*----------------------------*)
(*  Request and Result Types: *)
(*----------------------------*)
type http_req  =
{
    http_method  : string;
    full_url     : string;
    extra_headers: list (string * string);
    cookies      : list Http_common.http_cookie;
    form         : list (string * string)
};

type parsed_url = (string * string * int * string);
                  (* Proto, host,  port,   path_etc *)
type http_res   =
{
    headers:    Hashtbl.t string   (list string);
    body:       string;
    from_url:   parsed_url
};

(*------------------------*)
(* Response line parsing: *)
(*------------------------*)
value resp_re = Pcre.regexp ~study:True "^HTTP/1.[01] +([0-9]+) +.*$";

value get_http_rc: string -> int =
fun line1 ->
    let mres = Pcre.extract ~rex:resp_re ~full_match:False line1 in
    int_of_string mres.(0);

(*------------------*)
(*  "mk_post_data": *)
(*------------------*)
value mk_post_data: http_req -> string =
fun req ->
    if  req.form <> []
    then
        if  req.http_method <> "POST"
        then
            failwith "Http_client.mk_post_data: POST method required"
        else
        let nvs =
            List.map
            (fun (n,v) ->
                (Netencoding.Url.encode ~plus:True n) ^"="^
                (Netencoding.Url.encode ~plus:True v)
            )
            req.form
        in
        String.concat "&" nvs
    else
        "";

(*--------------------*)
(*  "http_dialogue":  *)
(*--------------------*)
value rec http_dialogue:
    http_agent 's 'x -> http_req -> parsed_url -> float -> http_res =

fun agent req ((proto, host, port, path_etc) as purl) deadline   ->
try
    let before = Unix.gettimeofday () in

    (* The sending function: depends on the mode: *)
    let send: string -> unit =
        if  agent.curr_secure
        then
            (* HTTPS send: *)
            let tr = Option.get agent.trans_https in
            let so = Option.get agent.sock_https  in
            fun str ->
                Misc_utils.send_str tr.write so str deadline
        else
            (* HTTP send:  *)
            fun str ->
                Misc_utils.send_str
                    agent.trans_http.write agent.sock_http str deadline
    in
    (* Use persistent connections? *)
    let conn_hdr =
        if  agent.use_persist
        then "Connection: keep-alive\r\n"
        else "Connection: close\r\n"
    in
    (* Prepare the "POST" data (or do nothing if the method is not "POST": *)
    let data   = mk_post_data req in

    (* Select the "user_agent" randomly from the configured list, or skip it
       if no agent names are specified:
    *)
    let user_agent =
        if   agent.user_agents <> [||]
        then Misc_utils.random_entry agent.user_agents
        else ""
    in
    (* Now actually send the data to the server : *)
    do {
        (* Send the request and standard headers: *)
        List.iter send
        [
            Printf.sprintf "%s %s HTTP/1.1\r\n" req.http_method path_etc;
            "Host: "      ^host^"\r\n";
            conn_hdr
        ];
        (* Possibly send up the "user-agent" hdr: *)
        if   user_agent <> ""
        then send ("User-agent: "^user_agent^"\r\n")
        else ();

        (* Send "extra_headers": *)
        List.iter send
            (List.map (fun (n,v) -> n^": "^v^"\r\n") req.extra_headers);

        (* Send up the cookies appropriate for this host/path: *)
        let ok_cookies =
            List.filter
            (fun c ->
                (ExtString.String.ends_with   host
                    c.Http_common.cookie_domain)        &&
                (ExtString.String.starts_with path_etc
                    c.Http_common.cookie_path)          &&
                (before < c.Http_common.cookie_expires)
            )
            req.cookies
        in
        List.iter send
            (List.map
                (fun c ->
                    Printf.sprintf "Cookie: %s=%s\r\n"
                        c.Http_common.cookie_name
                        c.Http_common.cookie_value
                )
                ok_cookies
            );
        (* If the method is "POST", send extra headers: *)
        if  data <> ""
        then
        do {
            send "Content-type: application/x-www-form\r\n";
            send (Printf.sprintf   "Content-length: %d\r\n"
                 (String.length data))
        }
        else ();

        (* Finish with the headers: *)
        send "\r\n";

        (* For "POST", send the form data: *)
        if   data <> ""
        then send data
        else ();

        (* Start getting the response: *)
        let (line1, headers) =
            if   agent.curr_secure
            then
                (* HTTPS: *)
                let rd = Option.get agent.reader_https in
                let so = Option.get agent.sock_https   in
                do {
                    (* Re-set the reader: *)
                    rd#reset ();

                    (* Get the 1st resp line: *)
                    let l1 = rd#recv_line so deadline in

                    (* Get the headers:   *)
                    let hs = Http_common.read_headers rd so deadline in
                    (l1, hs)
                }
            else
            do {
                (* HTTP: *)
                (* Re-set the reader: *)
                agent.reader_http#reset ();

                (* Get the 1st resp line: *)
                let l1 = agent.reader_http#recv_line agent.sock_http deadline in

                (* Get the headers: *)
                let hs = Http_common.read_headers
                        agent.reader_http agent.sock_http deadline in
                (l1, hs)
            }
        in
        (* Now analyse the response: *)

        let rc = get_http_rc line1 in
        if  rc >= 400
        then
            (* Received an error: *)
            failwith (string_of_int rc)
        else
        if  rc >= 300 && rc <= 399
        then
            (* A re-direction: *)
            let location =
            try
                let l = Http_common.get_single_val headers "location" in
                do {
                    assert (l <> "");
                    (* If "location" begins with "/", it's a re-direct within
                       the same domain:
                    *)
                    if   l.[0] = '/'
                    then Http_common.mk_url proto host port l
                    else l
                }
            with [_ -> failwith ("Http_client.http_dialogue: "^
                                 "Undefined re-direction location")]
            in
            (* A full new transaction. We may need to use cookies from the
               curr transaction in the new one.
            *)
            let cookies = Http_common.get_cookies
                         (Http_common.Client_Side host) headers
            in
            let new_req = {(req) with full_url=location; cookies=cookies} in

            http_transaction agent new_req deadline
        else
        (* The curr response is OK. Read the rest of the response: *)
        let (body, after_cond) =
            if  agent.curr_secure
            then
                (* HTTPS read: *)
                let tr = Option.get agent.trans_https  in
                let rd = Option.get agent.reader_https in
                let so = Option.get agent.sock_https   in

                Http_common.read_http_data
                    (Http_common.Client_Side host) tr.read rd so agent.buff
                    headers deadline
            else
                (* HTTP read: *)
                Http_common.read_http_data
                    (Http_common.Client_Side host)
                    agent.trans_http.read agent.reader_http agent.sock_http
                    agent.buff  headers   deadline
        in
        (* What to do with the current connection? *)
        let () =
            if   (not agent.use_persist) || (after_cond = Http_common.EOF)
            then close_agent agent deadline
            else ()
        in
        (* Result: *)
        {
            headers  = headers;
            body     = body;
            from_url = purl
        }
    }
with
[hmm ->
 do {
    (* Close the socket and re-raise the exception: *)
    close_agent agent deadline;
    failwith (Printf.sprintf "HTTP error on %s%s: %s"
              host path_etc (Misc_utils.print_exn hmm))
}]

(*-----------------*)
(*  "http_to_ip":  *)
(*-----------------*)
(*  Attempts an HTTP dialogue over a single IP connection: *)

and http_to_ip:
    Unix.inet_addr -> http_agent 's 'x -> http_req -> bool -> parsed_url ->
    float -> http_res =

fun ip agent req secure ((_, host, port, _) as purl) deadline ->
do {
    (* To prevent sockets leak, explicitly close the client socket(s) before
       re-creating any of them:
    *)
    close_agent agent deadline;

    (* Open the client socket: *)
    if  secure
    then
        let tr = Option.get agent.trans_https  in
        agent.sock_https := Some (tr.socket deadline)
    else
        agent.sock_http  :=
            agent.trans_http.socket deadline;

    (* Bind, connect and run the dialogue:
       NB: If the following function ("bind_conn_dial")  is given a type
           annotation like  "transport 'z -> 'z -> http_res", this would
           result in unification between the 's and 'x type parms!
           The automatically inferred type is (???)
           forall 'z. transport 'z -> 'z -> http_res :
    *)
    let bind_conn_dial =
    fun tr so ->
        try
        do {
            (* Bind the socket randomly to a local IP, if any is given: *)
            if  agent.client_ips <> [| |]
            then
                let lip = Misc_utils.random_entry agent.client_ips  in
                tr.bind so (Unix.ADDR_INET lip 0)
            else
                ();

            (* Connect to the server: *)
            tr.connect so (Unix.ADDR_INET ip port) deadline;

            (* Successful connection: *)
            agent.curr_domain:= host;
            agent.curr_secure:= secure;

            (* Run the dialogue:      *)
            http_dialogue agent req purl deadline
        }
        with
        [hmm ->
         do {
            (* Any exception: close the socket and propagate the error: *)
            close_agent agent deadline;
            raise hmm
        }]
    in
    if  secure
    then
        bind_conn_dial  (Option.get agent.trans_https)
                        (Option.get agent.sock_https )
    else
        bind_conn_dial  agent.trans_http agent.sock_http
}

(*----------------------*)
(*  "http_transaction": *)
(*----------------------*)
(*  Top-level function for both "GET" and "POST" methods. Tries to connect to
    different server IPs (if more than 1) until the request succeeds:
*)
and http_transaction: http_agent 's 'x -> http_req -> float -> http_res =
fun agent req deadline ->
    (* Get and parse the URL: *)

    let purl = Http_common.parse_url req.full_url in
    let (proto, host, port, _)         = purl     in

    let secure = proto = "https" in
    if  secure && agent.trans_https = None
    then
        failwith "Http_client.http_transaction: HTTPS protocol not supported"
    else
    (* Are we already connected to that host WITH THE SAME SECURITY MODE? *)

    let (pers_res, pers_err) =
        if agent.curr_domain = host && agent.curr_secure = secure
        then
            (* Yes, try the dialogue -- HTTP or HTTPS: *)
            try  (Some   (http_dialogue agent req purl deadline), None)
            with [hmm -> (None, Some hmm)]
        else
            (* Not connected to "host": No result here, but no error either: *)
            (None, None)
    in
    match pers_res with
    [ Some res ->
        (* Got the result from a persistent connection:    *)
        res
    | None ->
        (* Will need a new connection. Resolve the "host": *)
        let host_ips = agent.resolver host    in

        (* Try all server IPs -- each ONCE: *)
        let rec try_ips: list Unix.inet_addr -> list string -> http_res =
        fun rem_ips errs ->
            match rem_ips with
            [ [] ->
                (* No more IPs: *)
                failwith
                    ("Http_client.http_transaction: "  ^
                    (String.concat "," (List.rev errs)))

            | [hip::tips] ->
                (* Try the next IP, if time still allows: *)
                if  Unix.gettimeofday () > deadline
                then
                    failwith "Http_client.http_transaction: Deadline over-run"
                else
                let (mb_res, mb_err) =
                    try
                    (   Some (http_to_ip hip agent req secure purl deadline),
                        None
                    )
                    with [hmm->(None, Some hmm)]
                in
                match mb_res with
                [ Some res ->
                    res (* Got it! *)
                | None     ->
                    (* Try other IPs, memoising the error: *)
                    let new_err = Misc_utils.print_exn (Option.get mb_err)
                    in
                    try_ips tips [new_err::errs]
                ]
            ]
        in
        (* The initial application of "try_ips" may carry an error from an
           attempted "persistent" dialogue:
        *)
        let init_err =
            match pers_err with
            [ Some hmm -> [Misc_utils.print_exn hmm]
            | None     -> []
            ]
        in
        try_ips host_ips init_err
    ];

