(*pp camlp4r *)
(* vim:ts=4:syntax=ocaml
*)
(*===========================================================================*)
(*                                 "tnio.mli":                               *)
(*     Interface to Timed Network I/O and Light-Weight Multi-Threading       *)
(*                          L.A.Merkin, 2003--2005                           *)
(*===========================================================================*)
(*------------------------*)
(* Module Initialisation: *)
(*------------------------*)
(* The user can explicitly specify the desirable mechanisms for I/O Events
   and Context Management.   If a particular mechanism is not available, a
   run-time exception will be thrown:
*)
type scheduling_mode =
[   Priority_Bands of int
    (* This mode is the best when the number of different priority levels
       (bands, as given by the arg, must be >=1) is relatively small, but
       there may be a large number of threads in each band. Prioritie can
       range from 0 (lowest) to (arg-1) (highest).
    *)

|   Single_Queue
    (* This mode is the best when all or almost all threads have different
       priorities. In this case, any priorities are allowed. It uses more
       CPU time but less space for scheduling purposes than the "Priority_
       Bands" mode.
    *)
];

(* "init":
   This function  MUST be invoked before any other function of this module.
   Arg1: I/O Events Mechanism to use
   Arg2: Context Management Mechanism to use
   Arg3: Whether to try the Real-Time priority level for the whole process
   Arg4: Scheduling mode for the threads
*)
value init: ?io_events_mech: Tnio_mechs.io_events_mech ->
            ?rt_priority   : bool                      ->
            scheduling_mode                            ->
            unit;

(* "get_config":
   Returns the settings used by "init":
*)
value get_config:
            unit -> (Tnio_mechs.io_events_mech * bool * scheduling_mode);

(* "priority_range":
   Returns the range of priorities available with the type of scheduling to
   which this module has been initialised (min_prio, max_prio):
*)
value priority_range: unit -> (int * int);

(* "detach_child":
   To be called by a "fork"ed child process,    if the TNIO system has already
   been initialised by the parent, and the child wants to use it independently:
*)
value detach_child:   unit -> unit;

(*-------------------*)
(* Timed Socket I/O: *)
(*-------------------*)
(* The type of "timed sockets". NB: "socket" and "socketpair" functions have
   an optional arg  which sets up the size of the kernel I/O  buffer for the
   corresp socket(s);  if a non-default value is specified and it is greater
   than the curr kernel-defined maximum,  root privileges may be required by
   this function to set the buffer size; the buffer size is set for both RCV
   and SND operations:
*)
type  tsock = 'a;

value socket    : ?kern_buff_size:int ->
                  Unix.socket_domain  -> Unix.socket_type ->  tsock;

value socketpair: ?kern_buff_size:int ->
                  Unix.socket_domain  -> Unix.socket_type -> (tsock*tsock);

value close     : tsock -> unit;

value invalid_tsock     : tsock;
value invalid_file_descr: Unix.file_descr;
value invalid_deadline  : float;

(* Timed syscalls. All network calls are protected against SIGPIPE.
   Args: buff,  from, len, ABS_DEADLINE, ...;
   return val : number of bytes read / written.
   The absolute deadlines are in seconds from the Epoch, in the sense of
   "Unix.gettimeofday()".
   Also, "accept" has  an optional parm which  is the kernel buffer size
   for the socket created by it:
*)
value connect : tsock -> Unix.sockaddr  -> float -> unit;

value accept  : ?kern_buff_size:int     ->
                tsock -> float  -> (tsock * Unix.sockaddr);

value read    : tsock -> string -> int -> int -> float -> int;  (* UDP *)
value recv    : tsock -> string -> int -> int -> float -> int;  (* TCP *)
value recvfrom: tsock -> string -> int -> int -> float ->
                (int * Unix.sockaddr);                          (* UDP *)

value write   : tsock -> string -> int -> int -> float -> int;  (* UDP/TCP *)
value send    : tsock -> string -> int -> int -> float -> int;  (* TCP *)
value sendto  : tsock -> string -> int -> int -> float -> Unix.sockaddr ->
                int;                                            (* UDP *)

value zero_deadline    : float;
value infinite_deadline: float;

(* Asynchronous (non-blocking) version of the above calls. Currently, a thread
   cannot wait  (in a "select" style) for events on multiple "tsock"s, so only
   a single asynchronous operation is allowed at any time.
*)
(*
   When readability is established: the following "_now" functions are equiva-
   lent to their "non-now" counterparts valled with the "zero_deadline" arg:
*)
value accept_now       : tsock ->(tsock * Unix.sockaddr);
value recv_now         : tsock -> string -> int -> int -> int;
value read_now         : tsock -> string -> int -> int -> int;
value recvfrom_now     : tsock -> string -> int -> int ->(int * Unix.sockaddr);

(* When writability is established: similar to the above group: *)
value write_now        : tsock -> string -> int -> int -> int;
value send_now         : tsock -> string -> int -> int -> int;
value sendto_now       : tsock -> string -> int -> int -> Unix.sockaddr -> int;

(* Functions lifted from the "Unix" module -- not timed, as they are
   non-blocking in any case.
   NB: "bind" performs a single bind attempt; "bind_retry" tries to bind to the
   given address periodically until the "bind_wait" time-out expires  (normally
   240.0 seconds):
*)
value bind             : tsock -> Unix.sockaddr -> unit;
value bind_wait        : float;
value bind_retry       : tsock -> Unix.sockaddr -> unit;

value listen           : tsock -> int -> unit;
value shutdown         : tsock -> Unix.shutdown_command -> unit;

value getsockname      : tsock -> Unix.sockaddr;
value getpeername      : tsock -> Unix.sockaddr;

value getsockopt       : tsock -> Unix.socket_bool_option   -> bool;
value setsockopt       : tsock -> Unix.socket_bool_option   -> bool -> unit;

value getsockopt_int   : tsock -> Unix.socket_int_option    -> int;
value setsockopt_int   : tsock -> Unix.socket_int_option    -> int -> unit;

value getsockopt_optint: tsock -> Unix.socket_optint_option -> option int;
value setsockopt_optint: tsock -> Unix.socket_optint_option -> option int ->
                         unit;

value getsockopt_float : tsock -> Unix.socket_float_option  -> float;
value setsockopt_float : tsock -> Unix.socket_float_option  -> float -> unit;

(* MultiCast Functions : *)

(* "join_multicast": the host joins the multicast group  with the specified
   "mcast_ip" over the interface "iface_ip". On sending, multicast messages
   will be routed through the same IP. Loop-back delivery of multicast msgs
   to the sender is by default disabled:
*)
value join_multicast   : tsock -> ~iface_ip:Unix.inet_addr  ->
                                  ~mcast_ip:Unix.inet_addr  -> unit;

(* In the following, the int TTL value must be in the range 0..255: *)
value set_multicast_ttl: tsock -> int -> unit;

(* Useful function: returns a list of IPs of the local host: *)
value get_local_ips    : unit  -> list Unix.inet_addr;


(*------------*)
(* Threading: *)
(*------------*)
type  thread_t = 'a;

(* "thread_create":
    Non-optional arg: thread body closure.
    Priorities      : larger value -> higher priority:
*)
value thread_create:
    ?stack_size_k:int  -> ?priority:int -> (unit -> unit) -> thread_t;

(* "thread_self": the calling thread: *)
value thread_self : unit -> thread_t;

(* Numerical ID of a "thread_t" object or of the calling thread -- even if the
   thread had terminated:
*)
value thread_id   : thread_t -> int;

(* "thread_by_id": reverse of "thread_id": *)
value thread_by_id: int -> thread_t;

(* NB: "invalid_thread_id" is one which does not correspond to any of the
   real thread, but it MAY notionally correspond to the main thread.   It
   does correspond to the "invalid_thread" object:
*)
value invalid_thread   : thread_t;
value invalid_thread_id: int;

(* The calling thread voluntarily relinquishes the CPU: *)
value thread_yield : unit -> unit;

(* The calling thread sleeps  until the given deadline: *)
value thread_sleep : float-> unit;

(* Stopping another thread,  with a deadline  for being waken up via
   "thread_resume". If the thread is not resumed within the deadline,
   the stopped thread receives an exception at the blocking point.
   NB: A thread can stop itself this way, and that is different from
   "thread_sleep", as in the latter case the thread CAN'T be resumed
   by another one:
*)
value thread_stop  : thread_t -> float -> unit;

(* Resuming another thread: *)
value thread_resume: thread_t -> unit;

(* Kill another thread (or indeed, ourselves -- "exit"): *)
value thread_kill  : thread_t -> unit;
value thread_exit  : unit     -> 'a;

(* The Scheduler which the main thread enters. The arg determines whether
   thread priorities will actually be used for scheduling, or ignored  --
   in the latter case, the scheduling overhead is slightly lower,  but it
   should be low anyway:
*)
value threads_scheduler: unit -> unit;

(* The count of all currently existing threads, excluding terminated: *)
value threads_count: unit -> int;

(* Terminate the WHOLE PROGRAM *)
value exit    : int -> 'a;

(* CPU utilisation statistics:  result in %%s: *)
value get_cpu_utilisation: unit -> float;

(*------------------------*)
(*  Condition Variables:  *)
(*------------------------*)
(*  By themselves, they do not contain any user-visible values. They just
    allow the user to create queues of threads waiting for a condition to
    be signalled:
*)
type  cond_var_t = 'a;

value cond_var_init: unit -> cond_var_t;

(*  "cond_var_wait":
    Suspend the calling thread waiting for a signal on a given cond var (Arg1).
    Arg2 is the deadline for waking up normally  (via "cond_var_signal");  if
    no wake-up signal occurs within the deadline, this function raises an exn:
*)
value cond_var_wait: cond_var_t -> float -> unit;

(*  "cond_var_signal":
    Send a wake-up signal to the threads  suspended on a given cond var.
    If the "n_threads" arg is omitted, all waiting threads are waken up;
    other-wise, only the given number of earliest-blocked threads receive
    a wake-up signal:
*)
value cond_var_signal: ?n_threads:int -> cond_var_t -> unit;

(*  "waiting_threads":
    Returns the number and the list of all threads waiting on a cond var:
*)
value cond_var_info  : cond_var_t -> (int * (list thread_t));

(*---------------*)
(*  Semaphores:  *)
(*---------------*)
(*  With non-pre-emptive scheduling, they are rarely used.  Internally
    "sem_t" contains a state var and a cond var. The latter provides a
    queue of thereads waiting for the semaphore to open. When it opens,
    the waiting threads proceed in the FIFO order:
*)
type  sem_t = 'a;

value sem_init: int -> sem_t;       (* Initially in the "open" state *)

(*  "sem_lock":
    The calling thread locks the semaphore. If it is already locked,
    the calling thread suspends itself on the cond var within the sem:
*)
value sem_lock:   sem_t -> unit;

(*  "sem_unlock":
    Pre-condition: the calling thread must have previously locked this
    semaphore (otherwise an exception occurs, unless the "force" flag
    is set). A signal is posted on the cond var of this sem, so the head
    of the queued threads (if any) can lock it in its turn, and proceed:
*)
value sem_unlock: ?force:bool -> sem_t -> unit;

(*---------*)
(*  Misc:  *)
(*---------*)
(*  "pass_tsock":
    passes an open TNIO socket (Arg1) from the thread given by Arg2 to the
    calling thread (which can be the main thread).
    NB: we cannot do it other way around (pass a socket INTO a thread given
    by Arg2), as this would create a race condition: once the target thread
    exists, it could already be running -- too late to pass the socket into
    it.
    Without this function, a socket can only be used in a thread where it was
    created -- INTEGRITY CONSTRAINT. This function is normally not needed, as
    we open a socket from within a thread where it will be used. However, it
    is ABSOLUTELY RQUIRED in some cases, e.g. passing a socket from the acce-
    ptor thread to a handler thread:
*)
value pass_tsock: tsock -> thread_t -> unit;

(*  "touch_file":
    sets the access and modification time stamps of the given file to the
    current value:
*)
value touch_file: string -> unit;

(*  "set_sig_handler":

    Sets a new signal handler. If there was already a non-default, non-ignore
    handler set for this signal, and the new handler is also non-default, non-
    ignore, and the "new_handler_pos" flag is set to *"Attach_Before"/"Attach_
    After", then a combined handler is set (with the new one executing before
    / after the new one, resp.).
*)
type  sig_handler_pos =
    [ Handler_Replace | Handler_Attach_Before | Handler_Attach_After ];

(*  The "signal_behaviour" type is similar  to "Sys.signal_behavior",   but it
    assumes that the "Sig_Handler_Gen" is a handler which does not immediately
    cause process termination (ie, does not contain "exit"); and "Sig_Handler_
    Exit" is to apply the given handler  (again without explicit "exit"),  and
    then automatically exit the process.  These behaviours are taken into acc-
    ount when the handlers are combined according to their position:
*)
type  sig_behaviour =
    [ Sig_Ignore
    | Sig_Default
    | Sig_Handler_Gen  of (int -> unit)         (* Handler           *)
    | Sig_Handler_Exit of (int -> unit) and int (* Handler, ExitCode *)
    ];

(*  NB: After the handler is set, the function UNBLOCKS the signal. The deflt
    "new_handler_pos" is "Handler_Replace":
*)
value set_sig_handler:
    int -> ?new_handler_pos:sig_handler_pos -> sig_behaviour -> unit;

