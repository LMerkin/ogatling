(*pp camlp4r *)
(* vim:ts=4:syntax=ocaml
*)
(*===========================================================================*)
(*                                "dirq.ml":                                 *)
(*            FIFO Queues with Direct Removal from the Middle:               *)
(*                  L.A.Merkin, IMPARTIAL Project, 2005                      *)
(*===========================================================================*)
(* Msg queue: must allow for fast removal of msgs from the middle, as well
   as general FIFO properties:
*)
type qel  'a =  (* Queue element:   *)
{
    data: 'a;
    next: mutable option (qel 'a)
};

type t    'a =  (* Queue top-level: *)
{
    head: mutable option (qel 'a);
    tail: mutable option (qel 'a)
};

(*  "empty":  *)
value empty: unit -> t 'a =
fun () ->
{
    head = None;
    tail = None
};

(*  "append":
    Appends a new element at the tail. The queue is modified in place:
*)
value append: t 'a -> 'a -> unit =
fun mq a ->
    (* Create a new node: *)
    let node = Some
    {
        data = a;
        next = None
    }
    in
    match mq.tail with
    [ None ->
      do{
        (* There is no tail yet, so no head either: *)
        assert (mq.head = None);
        mq.head := node;
        mq.tail := node
      }

    | Some tl ->
        (* There exists a tail already: *)
        tl.next := node
    ];

(*  "take":
    Traverses the queue, and removes and returns the first element (if any)
    which satisfies the given criterion. The queue is modified in place:
*)
value take : t 'a -> ('a -> bool) -> option 'a =
fun mq test ->
    let rec traverse: option (qel 'a) -> option (qel 'a) -> option 'a =
    fun prev curr ->
        match curr with
        [ None    ->
            (* curr = None: at the end of the queue, nothing found: *)
            None
        | Some cr ->
            if  test cr.data
            then
            do {
                (* Yes, an appropriate element has been found. By-pass it in
                   the queue:
                *)
                match prev with
                [ None    ->
                  do{
                    (* We are at the beginning of the queue: *)
                    assert (mq.head = curr);
                    mq.head  := cr.next
                  }
                | Some pr ->
                    (* There was a prev element: *)
                    pr.next  := cr.next
                ];
                (* Also, re-set the tail if we remove it:    *)
                if  mq.tail  = curr
                then
                    mq.tail := prev
                else
                    ();
                (* The result to return: *)
                Some cr.data
            }
            else
            (* This node is NOT to be removed, try more: *)
            traverse curr cr.next
        ]
    in
    traverse None mq.head;

