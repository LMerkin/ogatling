(*pp camlp4r *)
(* vim:ts=4:syntax=ocaml
*)
(*===========================================================================*)
(*                              "communic.ml":                               *)
(*    DNS and Transport Facilities for the Gatling/Lukol MetaSearch Engine   *)
(*                  (C) Explar Technologies Ltd, 2004                        *)
(*===========================================================================*)
(*========*)
(*  DNS:  *)
(*========*)
(*  For the Search Engines interface, all IP addresses of configure Search
    Engines are normally pre-resolved. If Search Engines behaviour changes
    (e.g., new DNS records, new re-directions, etc), an error is generated,
    and the list of configured Search Engine domains may need to be updated
    manually.
    For the Front-End HTTP servers which need to fetch 3rd-party pages for
    search terms highlighting, DNS resolvers may be created on demand,  or
    put into the state of the worker thraeds:
*)
(*------------------*)
(*  "mk_dns_agent": *)
(*------------------*)
(* Creates a DNS agent with some default configuration. Local DNS servers
   can be specified in Arg 1; if empty, the internal direct resolver will
   be used:
*)

value mk_dns_agent: list string -> Dns_client.dns_agent =
fun dns_servers ->
    let dns_conf  = Dns_client.default_dns_agent_config ()      in
    let _         = dns_conf.Dns_client.conf_dns_servers := dns_servers in
    let _         = dns_conf.Dns_client.conf_n_retries   := 2   in
    let _         = dns_conf.Dns_client.conf_timeout_sec := 2.0 in

    Dns_client.mk_dns_agent dns_conf;

(*---------------------*)
(* "default_resolver": *)
(*---------------------*)
(*  Used by the Front-End for resolving HTTP domains for search terms high-
    lighting, and for pre-resolving of Back-End Search Engines:
*)
value n_attempts = 2;

value default_resolver:
    Dns_client.dns_agent -> string -> list Unix.inet_addr =

fun agent dom ->
    let rec try_resolve: int -> list string -> list Unix.inet_addr =
    fun n_rem prev_errs ->
        if  n_rem <= 0
        then
            failwith ("Communic.default_resolver: DNS error(s) for \""^
                      dom^"\": "^(String.concat ", " prev_errs))
        else
            let (mb_res, mb_err) =
            try
                let dns_res = Dns_client.dns_enquiry
                              agent
                              Dns_client.T_A
                             (Dns_client.Dom dom)
                in
                let got_ips =
                    match dns_res with
                    [ Dns_client.IPs ips when ips <> []     -> ips
                    | _ -> failwith ("Invalid or empty DNS response")
                    ]
                in
                (Some got_ips, None)
            with
                [hmm -> (None, Some (Misc_utils.print_exn hmm))]
            in
            match mb_res with
            [ Some res ->
                res (* Got it! *)
            | None ->
              do {
                Tnio.thread_sleep 2.0;
                try_resolve (n_rem-1) (prev_errs @ [Option.get mb_err])
              }
            ]
    in
    (* Try serveral times: *)
    try_resolve n_attempts [];


(*-----------------*)
(*  "se_resolver": *)
(*-----------------*)
(* For Search Engines, the L2 DNS cache is used;  for other DNS resolutions
   (eg, syntax highlighting on external sites), it is NOT, to prevent cache
   overflow:
*)
value dns_l2_cache: Hashtbl.t string (list Unix.inet_addr) =
                    Hashtbl.create 100;

value se_resolver :
    Dns_client.dns_agent -> (string -> unit) -> string -> list Unix.inet_addr =

fun agent  log_func  dom ->
    let ips =
    try
        (* Look-up the L2 cache first: *)
        Hashtbl.find dns_l2_cache dom
    with
    [Not_found ->
     do {
        (* Not cached -- log a warning, and try direct resolution: *)
        log_func ("WARNING: \""^dom^"\" not found in the L2 DNS Cache");

        let ips = default_resolver agent dom in
        do {
            Hashtbl.add dns_l2_cache dom ips;
            ips
        }
    }]
    in
    ips;

(*-----------------------*)
(*  "init_se_resolver":  *)
(*-----------------------*)
(* Pre-fetches IP addresses of all configured "se_domain"s and
   "gse_other_hosts", and fills in the "dns_l2_cache":
*)
value init_se_resolver:
    list string                    ->
    Lukol_config.se_interfaces_gen ->
    Lukol_config.se_interfaces_xml ->
    (string -> unit)               ->
    unit        =

fun dns_servers interfs_gen interfs_xml log_func ->
do {
    (* Create the DNS agent which will be used here:   *)
    let agent = mk_dns_agent dns_servers in

    (* Compile the list of all domains to be resolved: *)

    (* Domains of General-Purpose Search Engines:      *)
    let domains_gen =
    List.concat
       (List.map
            (fun gsec ->
                [gsec.Lukol_config.gse_domain ::
                 gsec.Lukol_config.gse_other_hosts]
            )
            interfs_gen
        )
    in
    (* Domains of XML-Based Search Engines: *)
    let domains_xml =
        List.map
            (fun xsec -> xsec.Lukol_config.xse_domain)
            interfs_xml
    in
    (* Resolve them all: *)
    let domains_all = domains_gen @ domains_xml  in
    List.iter
        (fun dom ->
            let ips = default_resolver agent dom in
            Hashtbl.replace dns_l2_cache dom ips
        )
        domains_all;

    log_func "Search Engines IPs successfully resolved"
};

(*=========*)
(*  HTTP:  *)
(*=========*)
value http_transport =
{
    Http_client.socket          = Tnio.socket;
    Http_client.invalid_socket  = Tnio.invalid_tsock;
    Http_client.getsockname     = Tnio.getsockname;
    Http_client.bind            = Tnio.bind;
    Http_client.connect         = Tnio.connect;
    Http_client.read            = Tnio.recv;
    Http_client.write           = Tnio.send;
    Http_client.close           = Tnio.close
};

(* Invalid URL, a placeholder: *)
value invalid_url    = "";

(* Standard buffer size for HTTP clients: *)
value http_buff_size = 256 * 1024;

