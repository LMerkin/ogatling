// vim:ts=4:syntax=c
//===========================================================================//
//                              "gnutls_stubs.c":                            //
//          OCaml Binding to the GNU TLS Library, C Interface Code           //
//                     (C) Explar Technologies Ltd, 2004                     //
//===========================================================================//
#include <caml/mlvalues.h>
#include <caml/fail.h>
#include <gcrypt.h>
#include <gnutls/gnutls.h>

//------------------------//
//  "ogtls_global_init":  //
//------------------------//
//  Module initialisation -- invoked internally once. No need to make it
//  thread-safe:
//
static gnutls_certificate_credentials creds;

value ogtls_global_init (value cas_file, value crl_file, value our_key_files)
{
    // Initialise threading and blocking operations within the GCrypt lib? The
    // GCrypt library allows the user to specify the following call-backs:
    // "accept", "connect", "read", "recvmsg", "select", "sendmsg",  "waitpid"
    // and "write" implementations, but NONE of them appear to be actually
    // used by either GCrypt or its clients. The call-backs which ARE actually
    // used include:   "mutex_init", "mutex_destroy", "mutex_lock", "mutex_un-
    // lock" and (possibly) "init"  (initialising user-specified global data).
    // HOWEVER, TNIO scheduling is non-preemptive -- and apparently, critical
    // sections in GCrypt do not contain  any of the above-mentioned blocking
    // operations, so a thread CANNOT yield control within a critical section.
    // Thus, we argue that mutex operations are not required either!..

    // So: ititialise GNU TLS:
    if  (gnutls_global_init () != 0)
        failwith ("Gnutls.init: Global init failed");

    // Allocate the Credentials structure:
    if  (gnutls_certificate_allocate_credentials (& creds) != 0)
        failwith ("Gnutls.init: Cannot allocate credentials");

    // Load the list of Trusted CAs, if any. We assume that if this file is
    // specified, it must contain at least one certificate:
    char* fn1, *fn2;
    fn1 = String_val (cas_file);
    if  (*fn != '\0')
    {
        if  (gnutls_certificate_set_x509_trust_file
                (creds, fn1, GNUTLS_X509_FMT_PEM) <= 0)
            failwith ("Gnutls.init: Cannot load the Trusted CAs list");
    }

    // Load the Certificate Revocation List, if any. Unlike the Trusted CAs
    // file, we allow this list to be empty even if it is specified:
    fn1 = String_val (crl_file);
    if  (*fn != '\0')
    {
        if  (gnutls_certificate_set_x509_crl_file
                (creds, fn1, GNUTLS_X509_FMT_PEM) <  0)
            failwith ("Gnutls.init: Cannot load the Cert Revocation List");
    }

    // Load our own Public Certificate and Private Key (mostly for servers),
    // if BOTH files are specified:
    fn1 = String_val (Field (our_key_files, 0));
    fn2 = String_val (Field (our_key_files, 1));

    if  ((*fn1 != '\0') && (*fn2 != '\0'))
    {
        // XXX: Is the following function supposed to return 0 on success?
        if  (gnutls_certificate_set_x509_key_file
                (creds, fn1, fn2, GNUTLS_X509_FMT_PEM) != 0)
            failwith ("Gnutls.init: Cannot load our PublicCert / PrivKey");
    }
    else
    if  ((*fn1 != '\0') || (*fn2 != '\0'))
        failwith ("Gnutls.init: Both PublicCert and PrivKey must be specified");

    // NB: we currently DO NOT support Diffie-Hellman key exchange protocol,
    // so no DH parms are generated here.
    return Val_unit;
}

//-------------------//
//  "init_session":  //
//-------------------//
// Certificates we will accept: only RSA:
static const int cert_type_priority[2] = {GNUTLS_CRT_X509, 0};

value ogtls_init_session (value ts, value server)
{
    // Initialise the session:
    gnutls_session_t session;

    int sf = (Bool_val (server)) ? GNUTLS_SERVER : GNUTLS_CLIENT;

    if  (gnutls_init   (&session, sf))
        failwith ("Gnutls.init_session: Cannot create a session");

    if  (gnutls_set_default_priority   (session))
        failwith ("Gnutls.init_session: Cannot set methods priority");

    if  (gnutls_certificate_type_set_priority (session, cert_type_priority))
        failwith ("Gnutls.init_session: Cannot set certs priority");

    // Put all the credentials into the current session:
    // RC=0 means OK?
    if  (gnutls_credentials_set (session, GNUTLS_CRD_CERTIFICATE, creds) != 0)
        failwith ("Gnutls.init_session: Cannot set session credentials");

    // We embed "ts" into the "session" even though "ts" is probably still
    // unconnected in the client case:
    gnutls_transport_set_ptr  (session,
                              (gnutls_transport_ptr_t) (Int_val (ts)));

    // We DON't do any Diffie-Hellman settings for the server.
    // Also, in the server case, we currently DON'T request  client certs.

    // The "sessions" is actually just a ptr to the C heap,   so it can be
    // returned unwrapped. We do NOT automatically finalise sessions, like
    // we don't automatically close sockets which are no longer accessible:
    return (value) session;
}

//----------------------//
//  "invalid_session":  //
//----------------------//
//  Simply a NULL ptr exported to OCaml:

value ogtls_get_invalid_session (value unit)
{
    return (value) NULL;
}

//--------------//
//  "connect":  //
//--------------//
value ogtls_connect (value host, value session, value addr, value deadline)
{
    // Extract the embedded socket:
    int ts = (int)  (gnutls_transport_get_ptr (session));
    if (ts < 0)
        failwith ("Gnutls.connect: Invalid TNIO descr");

    // Do the connection -- straight via the OCaml TNIO interface. NB: this
    // operation is blocking, so it may result in a GC while we are de-sche-
    // duled.  Protect the "deadline" and "hostname" against that,  as they
    // are used in the following:
    Begin_roots2 (deadline, hostname);
    tnio_connect (Val_int (ts), addr, deadline);
    End_roots    ();

    // Set the deadline for all subsequent operations:
    (void)   tnio_set_next_deadline (deadline);

    // Perform the handshake:
    if  (gnutls_handshake (session) < 0)
        failwith ("Gnutls.connect: Handshake error");

    // Verify the server's certificate:
    unsigned int status = 0;
    if  (gnutls_certificate_verify_peers2 (session, &status) < 0)
        failwith ("Gnutls.connect: Server cert verification failed");

    if  (status & GNUTLS_CERT_INVALID)
        failwith ("Gnutls.connect: Server cert not trusted");

    if  (status & GNUTLS_CERT_SIGNER_NOT_FOUND)
        failwith ("Gnutls.connect: Server cert has no known issuer");

    if  (status & GNUTLS_CERT_REVOKED)
        failwith ("Gnutls.connect: Server cert has been revoked");

    // OK, the server has been authenticated -- but is it really the one
    // we were connecting to? Do the following:
    const gnutls_datum_t* cert_list = NULL;
    int   cert_list_size            = 0;
    gnutls_x509_crt_t     cert;

    if (gnutls_certificate_type_get (session) != GNUTLS_CRT_X509)
        failwith ("Gnutls.connect: Must be X509 certs");

    // Get all the peer's certs -- but we will examine only the 1st of them:
    cert_list = gnutls_certificate_get_peers (session, &cert_list_size);
    if  (cert_list == NULL && cert_list_size <= 0)
        failwith ("Gnutls.connect: Invalid peers cert list");

    // Copy the 1st cert for examination:
    if  (gnutls_x509_crt_init   (&cert) < 0 ||
         gnutls_x509_crt_import (cert, &cert_list[0], GNUTLS_X509_FMT_DER) < 0)
        failwith ("Gnutls.connect: Error while importing the chain leader");

    time_t now = time (NULL);
    if  (gnutls_x509_crt_get_expiration_time (cert) > now)
        failwith ("Gnutls.connect: Server cert expired");

    if  (gnutls_x509_crt_get_activation_time (cert) < now)
        failwith ("Gnutls.connect: Server cert not yet activated");

    char* hn = String_val (hostname);
    if  (gnutls_x509_crt_check_hostname (cert, hn) != 0)
        failwith ("Gnutls.connect: server is an IMPOSTOR!");

    // All checks done?
    return Val_unit;
}

//-----------//
//  "send":  //
//-----------//

//-----------//
//  "recv":  //
//-----------//

//------------//
//  "close":  //
//------------//

