(* vim:ts=4:syntax=ocaml
*)
(*===========================================================================*)
(*                                "ssl.mli":                                 *)
(*    Adapted by Explar Technologies Ltd, 2004, for use with the TNIO RTS    *)
(*===========================================================================*)
(*
   Copyright 2003 Samuel Mimram

   This file is part of Ocaml-ssl.
   
   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*)

(**
  Bindings for ssl.

  @author Samuel Mimram
*)

(** A context. *)
type context

(** A certificate. *)
type certificate

(** A SSL abstract socket. *)
type socket

(** Invalid socket is only a placeholder. Can only be used in equality tests. *)
val invalid_socket: socket

(** The SSL method could not be initalized. *)
exception Method_error
(** The SSL server certificate could not be initialized. *)
exception Certificate_error
(** The SSL server private key could not be intialized. *)
exception Private_key_error
(** The SSL private key does not match the certificate public key. *)
exception Unmatching_keys
(** The given socket is invalid. *)
exception Invalid_socket
(** The SSL handler could not be initialized. *)
exception Handler_error
(** The connection could not be made with the SSL service. *)
exception Connection_error
(** Failed to accept an SSL connection. *)
exception Accept_error
(** An error occured while reading data. *)
exception Read_error
(** An error occured while writing data. *)
exception Write_error

(** Initialize SSL functions. Should be called before calling any other function. *)
val init : unit -> unit

(** Create a context for a client connection. *)
val create_client_context : unit -> context

(** [create_server_context cert privkey] creates a context for a server connection with [cert] as certificate's file name and [privkey] as private key file name. *)
val create_server_context : string -> string -> context

(** Embed a Tnio socket into a SSL socket. *)
val embed_socket : Tnio.tsock -> context -> socket

(** Get the cipher used by a socket. *)
val get_cipher : socket -> string

(** Get the certificate used by a socket. *)
val get_certificate : socket -> certificate

(** Get the issuer of a cerstificate. *)
val get_issuer : certificate -> string

(** Get the subject of a certificate. *)
val get_subject : certificate -> string

(** Get the file descriptor associated with a socket (warning: you should not write or read on it). It is primarly useful for [select]ing on it. *)
val file_descr_of_socket : socket -> Tnio.tsock

(* In the following, the "float" arg is the abs deadline. "Tnio.zero_deadline"
   can be used for non-blocking operation:
*)

(** Send data over a connected SSL socket. *)
val write : socket -> string -> int -> int -> float -> int

(** Receive data from a connected SSL socket. *)
val read : socket -> string -> int -> int -> float -> int

(** Write a string on a SSL socket. *)
val output_string : socket -> string -> float -> unit

(** Write a char on a SSL socket. *)
val output_char : socket -> char -> float -> unit

(** Write an integer on a SSL socket. *)
val output_int : socket -> int -> float -> unit

val input_string : socket -> float -> string

val input_char : socket -> float -> char

val input_int : socket -> float -> int

(** Connect an SSL socket. *)
val connect : socket -> float -> unit

(** Accept a SSL connection. *)
val accept : socket -> float -> unit

(** Close an SSL connection. *)
val shutdown : socket -> unit

(** Open a SSL connection. *)
val open_connection : Unix.sockaddr -> float -> socket

(** Close an opened SSL connection. *)
val shutdown_connection : socket -> unit
