/* 
   Copyright 2003 Samuel Mimram
   Modified by Explar Technologies Ltd, 2004, to use with the TNIO RTS

   This file is part of Ocaml-ssl.

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

*/

/**
 * Libssl bindings for OCaml.
 *
 * @author Samuel Mimram
 *
 */

/* $Id: ssl_stubs.c,v 1.10 2004/01/12 15:53:29 smimram Exp $ */

#include <caml/alloc.h>
#include <caml/callback.h>
#include <caml/fail.h>
#include <caml/memory.h>
#include <caml/misc.h>
#include <caml/mlvalues.h>

#include <openssl/ssl.h>
#include <openssl/err.h>
#include <stdio.h>

#include "tnio_capi.h"
// Unfortunately, the original exceptions thrown by this module produced, for
// some reason, invalid exception values which resulted in segfault when prin-
// ted by "Printex". For this reason, exceptions are converted into simple
// "Failure"s. They are converted back into original exceptions by the OCaml
// part of this module:

struct ssl_socket__t
{
  SSL *handler;
  int fd;
};

typedef struct ssl_socket__t ssl_socket_t;

static ssl_socket_t* ssl_socket_of_block(value block)
{
  return (ssl_socket_t*)Field(block, 1);
}

static void finalize_ssl_socket(value block)
{
  ssl_socket_t *s = ssl_socket_of_block(block);
  SSL_free(s->handler);
  free(s);
}

static void finalize_ctx(value block)
{
  SSL_CTX *ctx = (SSL_CTX*)Field(block, 1);
  SSL_CTX_free(ctx);
}

CAMLprim value ocaml_ssl_create_client_context()
{
  CAMLparam0();
  SSL_METHOD *method;
  SSL_CTX *ctx;
  
  if ((method = SSLv23_client_method()) == NULL)
    raise_constant(*caml_named_value("ssl_exn_method_error"));
  if ((ctx = SSL_CTX_new(method)) == NULL)
    raise_constant(*caml_named_value("ssl_exn_certificate_error"));

  CAMLlocal1(block);
  block = alloc_final(2, finalize_ctx, sizeof(SSL_CTX*), sizeof(SSL_CTX*));
  Field(block, 1) = (value)ctx;
  CAMLreturn(block);
}

CAMLprim value ocaml_ssl_create_server_context(value cert, value privkey)
{
  CAMLparam2(cert, privkey);
  char *cert_name = String_val(cert);
  char *privkey_name = String_val(privkey);
  SSL_METHOD *method;
  SSL_CTX *ctx;
  
  if ((method = SSLv23_server_method()) == NULL)
    raise_constant(*caml_named_value("ssl_exn_method_error"));
  if ((ctx = SSL_CTX_new(method)) == NULL)
    raise_constant(*caml_named_value("ssl_exn_certificate_error"));
  if (SSL_CTX_use_certificate_file(ctx, cert_name, SSL_FILETYPE_PEM) <= 0)
    {
      SSL_CTX_free(ctx);
      raise_constant(*caml_named_value("ssl_exn_certificate_error"));
    }
  if (SSL_CTX_use_PrivateKey_file(ctx, privkey_name, SSL_FILETYPE_PEM) <= 0)
    {
      SSL_CTX_free(ctx);
      raise_constant(*caml_named_value("ssl_exn_private_key_error"));
    }
  if (!SSL_CTX_check_private_key(ctx))
    {
      SSL_CTX_free(ctx);
      raise_constant(*caml_named_value("ssl_exn_unmatching_keys"));
    }

  CAMLlocal1(block);
  block = alloc_final(2, finalize_ctx, sizeof(SSL_CTX*), sizeof(SSL_CTX*));
  Field(block, 1) = (value)ctx;
  CAMLreturn(block);
}

CAMLprim value ocaml_ssl_get_cipher(value socket)
{
  CAMLparam1(socket);

  char* cipher = (char*)SSL_get_cipher(ssl_socket_of_block(socket)->handler);
  if (cipher == NULL)
    raise_constant(*caml_named_value("ocaml_exn_not_found"));

  CAMLreturn(copy_string(cipher));
}

static void finalize_cert(value block)
{
  X509 *cert = (X509*)Field(block, 1);
  X509_free(cert);
}

CAMLprim value ocaml_ssl_get_certificate(value socket)
{
  CAMLparam1(socket);

  X509 *cert = SSL_get_peer_certificate(ssl_socket_of_block(socket)->handler);
  
  CAMLlocal1(block);
  block = alloc_final(2, finalize_cert, sizeof(X509*), sizeof(X509*));
  Field(block, 1) = (value)cert;
  CAMLreturn(block);
}

CAMLprim value ocaml_ssl_get_issuer(value certificate)
{
  CAMLparam1(certificate);

  X509 *cert = (X509*)Field(certificate, 1);
  char *issuer = X509_NAME_oneline(X509_get_issuer_name(cert), 0, 0);
  if (issuer == NULL)
    raise_constant(*caml_named_value("ocaml_exn_not_found"));

  CAMLreturn(copy_string(issuer));
}

CAMLprim value ocaml_ssl_get_subject(value certificate)
{
  CAMLparam1(certificate);

  X509 *cert = (X509*)Field(certificate, 1);
  char *subject = X509_NAME_oneline(X509_get_subject_name(cert), 0, 0);
  if (subject == NULL)
    raise_constant(*caml_named_value("ocaml_exn_not_found"));

  CAMLreturn(copy_string(subject));
}

CAMLprim value ocaml_ssl_init()
{
  CAMLparam0();

  SSLeay_add_ssl_algorithms();
  SSL_load_error_strings();

  CAMLreturn(Val_unit);
}

CAMLprim value ocaml_ssl_get_file_descr(value socket)
{
  CAMLparam1(socket);

  CAMLreturn(Val_int(ssl_socket_of_block(socket)->fd));
}

CAMLprim value ocaml_ssl_embed_socket(value socket_, value context)
{
  CAMLparam2(socket_, context);
  int socket = Int_val(socket_);
  SSL_CTX *ctx = (SSL_CTX*)Field(context, 1);

  ssl_socket_t *ret = malloc(sizeof(ssl_socket_t));

  if (socket < 0)
    raise_constant(*caml_named_value("ssl_exn_invalid_socket"));
  if ((ret->handler = SSL_new(ctx)) == NULL)
    raise_constant(*caml_named_value("ssl_exn_handler_error"));
  SSL_set_fd(ret->handler, socket);
  ret->fd = socket;

  CAMLlocal1(block);
  block = alloc_final(2, finalize_ssl_socket, sizeof(ssl_socket_t), sizeof(ssl_socket_t));
  Field(block, 1) = (value)ret;
  CAMLreturn(block);
}

CAMLprim value ocaml_ssl_invalid_socket ()
{
  CAMLparam0();
  CAMLlocal1(block);
  block = alloc_final(2, finalize_ssl_socket, sizeof(ssl_socket_t), sizeof(ssl_socket_t));
  Field(block, 1) = (value)NULL;
  CAMLreturn(block);
}

extern value tnio_set_next_deadline  (value);

CAMLprim value ocaml_ssl_connect(value socket, value deadline)
{
  CAMLparam1(socket);
  (void) tnio_set_next_deadline (deadline);

  int ret = SSL_connect(ssl_socket_of_block(socket)->handler);
  if (ret < 0)
    raise_constant(*caml_named_value("ssl_exn_connection_error"));

  CAMLreturn(Val_unit);
}

CAMLprim value ocaml_ssl_write(value socket, value buffer, value start, value length, value deadline)
{
  CAMLparam4(socket, buffer, start, length);
  tnio_set_next_deadline (deadline);

  int ret = SSL_write(ssl_socket_of_block(socket)->handler, ((char*)String_val(buffer))+Int_val(start), Int_val(length));
  int err = SSL_get_error(ssl_socket_of_block(socket)->handler, ret);
  if (err != SSL_ERROR_NONE)
      raise_constant(*caml_named_value("ssl_exn_write_error"));

  CAMLreturn(Val_int(ret));
}

CAMLprim value ocaml_ssl_read(value socket, value buffer, value start, value length, value deadline)
{
  CAMLparam4(socket, buffer, start, length);
  tnio_set_next_deadline (deadline);

  int ret = SSL_read(ssl_socket_of_block(socket)->handler, ((char*)String_val(buffer))+Int_val(start), Int_val(length));
  int err = SSL_get_error(ssl_socket_of_block(socket)->handler, ret);
  if (err != SSL_ERROR_NONE)
    raise_constant(*caml_named_value("ssl_exn_read_error"));

  CAMLreturn(Val_int(ret));
}

CAMLprim value ocaml_ssl_accept(value socket, value deadline)
{
  CAMLparam1(socket);
  tnio_set_next_deadline (deadline);

  int ret = SSL_accept(ssl_socket_of_block(socket)->handler);
  if (ret < 0)
    raise_constant(*caml_named_value("ssl_exn_accept_error"));

  CAMLreturn(Val_unit);
}

CAMLprim value ocaml_ssl_shutdown(value socket)
{
  CAMLparam1(socket);

  ssl_socket_t *ssl = ssl_socket_of_block(socket);

  SSL_shutdown(ssl->handler);
  TNIO_CAPI_close(ssl->fd);

  CAMLreturn(Val_unit);
}
