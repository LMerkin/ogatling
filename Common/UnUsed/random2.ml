(***********************************************************************)
(*                                                                     *)
(*                           Objective Caml                            *)
(*                                                                     *)
(*              Damien Doligez, projet Para, INRIA Rocquencourt        *)
(*                                                                     *)
(*  Copyright 1996 Institut National de Recherche en Informatique et   *)
(*  en Automatique.  All rights reserved.  This file is distributed    *)
(*  under the terms of the GNU Library General Public License, with    *)
(*  the special exception on linking described in file ../LICENSE.     *)
(*                                                                     *)
(***********************************************************************)
(*=====================================================================*)
(*    Modified by LT, Explar Technologies Ltd, "Gatling" Project:    *)
(*      Added explicit state management for thread safety      *)
(*         (C) Explar Technologies Ltd, 2003--2004         *)
(*=====================================================================*)

(* "Linear feedback shift register" random number generator. *)
(* References: Robert Sedgewick, "Algorithms", Addison-Wesley *)

(* The PRNG is a linear feedback shift register.
   It is seeded by a MD5-based PRNG.
*)

type rng_state = (int array) * (int ref) (* State, Index *)

(* This is the state you get with [init 27182818] on a 32-bit machine. *)
let static_init = ([|
  561073064; 1051173471; 764306064; 9858203; 1023641486; 615350359;
  552627506; 486882977; 147054819; 951240904; 869261341; 71648846; 848741663;
  337696531; 66770770; 473370118; 998499212; 477485839; 814302728; 281896889;
  206134737; 796925167; 762624501; 971004788; 878960411; 233350272;
  965168955; 933858406; 572927557; 708896334; 32881167; 462134267; 868098973;
  768795410; 567327260; 4136554; 268309077; 804670393; 854580894; 781847598;
  310632349; 22990936; 187230644; 714526560; 146577263; 979459837; 514922558;
  414383108; 21528564; 896816596; 33747835; 180326017; 414576093; 124177607;
  440266690
|],
ref 0)

(* Returns 30 random bits as an integer 0 <= x < 1073741824 *)
let bits (state, index) =
  index := (!index + 1) mod 55;
  let newval =
    state.((!index + 24) mod 55) + state.(!index) in
  state.(!index) <- newval;
  newval land 0x3FFFFFFF

(* Returns a float 0 <= x < 1 with at most 90 bits of precision. *)
let rawfloat rgs =
  let scale = 1073741824.0
  and r0 = float (bits rgs)
  and r1 = float (bits rgs)
  and r2 = float (bits rgs)
  in ((r0 /. scale +. r1) /. scale +. r2) /. scale

let rec intaux rgs n =
  let r = bits rgs in
  if r >= n then intaux rgs n else r
let int rgs bound =
  if bound > 0x3FFFFFFF || bound <= 0
  then invalid_arg "Random2.int"
  else (intaux rgs (0x3FFFFFFF / bound * bound)) mod bound

let float rgs bound = (rawfloat rgs) *. bound

let bool rgs = ((bits rgs) land 1 = 0);;

(* Simple initialisation.  The seed is an integer. *)
let init seed =
  let mdg i =
    let d = Digest.string (string_of_int i ^ string_of_int seed) in
    (Char.code d.[0] + (Char.code d.[1] lsl 8) + (Char.code d.[2] lsl 16))
    lxor (Char.code d.[3] lsl 22)
  in
  let state = Array.init 55 mdg  (* state.(i) <- mdg i *)
  in
  (state, ref 0)

(* Full initialisation.  The seed is an array of integers. *)
let full_init seed =
  let (state, index) = init 27182818
  in
  for i = 0 to Array.length (seed) - 1 do
    let j = i mod 55 in
    state.(j) <- state.(j) + seed.(i)
  done;
  (state, index)

(* Low-entropy system-dependent initialisation. *)

external sys_random_seed: unit -> int = "sys_random_seed";;

let self_init () = init (sys_random_seed());;


(****************************************)
