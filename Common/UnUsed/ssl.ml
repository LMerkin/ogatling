(* vim:ts=4:syntax=ocaml
*)
(*===========================================================================*)
(*                                  "ssl.ml":                                *)
(*      Adapted by Explar Technologies Ltd, 2004, for use with TNIO RTS      *)
(*===========================================================================*)
(* 
   Copyright 2003 Samuel Mimram

   This file is part of Ocaml-ssl.

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *)

type context

type certificate

type socket

exception Method_error
exception Certificate_error
exception Private_key_error
exception Unmatching_keys
exception Invalid_socket
exception Handler_error
exception Connection_error
exception Accept_error
exception Read_error
exception Write_error

let _ =
  Callback.register_exception "ocaml_exn_not_found" Not_found;
  Callback.register_exception "ssl_exn_method_error" Method_error;
  Callback.register_exception "ssl_exn_certificate_error" Certificate_error;
  Callback.register_exception "ssl_exn_private_key_error" Private_key_error;
  Callback.register_exception "ssl_exn_unmatching_keys" Unmatching_keys;
  Callback.register_exception "ssl_exn_invalid_socket" Invalid_socket;
  Callback.register_exception "ssl_exn_handler_error" Handler_error;
  Callback.register_exception "ssl_exn_connection_error" Connection_error;
  Callback.register_exception "ssl_exn_accept_error" Accept_error;
  Callback.register_exception "ssl_exn_read_error" Read_error;
  Callback.register_exception "ssl_exn_write_error" Write_error

external init : unit -> unit = "ocaml_ssl_init"

external create_client_context : unit -> context = "ocaml_ssl_create_client_context"

external create_server_context : string -> string -> context = "ocaml_ssl_create_server_context"

external embed_socket : Tnio.tsock -> context -> socket = "ocaml_ssl_embed_socket"

external get_invalid_socket: unit -> socket = "ocaml_ssl_invalid_socket"

let invalid_socket = get_invalid_socket ()

external get_cipher : socket -> string = "ocaml_ssl_get_cipher"

external get_certificate : socket -> certificate = "ocaml_ssl_get_certificate"

external get_issuer : certificate -> string = "ocaml_ssl_get_issuer"

external get_subject : certificate -> string = "ocaml_ssl_get_subject"

external file_descr_of_socket : socket -> Tnio.tsock = "ocaml_ssl_get_file_descr"

external connect : socket -> float -> unit = "ocaml_ssl_connect"

external write  : socket -> string -> int -> int -> float -> int = "ocaml_ssl_write"

external read   : socket -> string -> int -> int -> float -> int = "ocaml_ssl_read"

external accept : socket -> float -> unit = "ocaml_ssl_accept"

external shutdown : socket -> unit = "ocaml_ssl_shutdown"

let open_connection sockaddr deadline =
  let domain =
    match sockaddr with
      | Unix.ADDR_UNIX _ -> Unix.PF_UNIX
      | Unix.ADDR_INET(_, _) -> Unix.PF_INET
  in
  let sock =
    Tnio.socket domain Unix.SOCK_STREAM in
    try
      let _   = Tnio.connect sock sockaddr  deadline         in
      let ssl = embed_socket sock (create_client_context ()) in
    connect ssl deadline; ssl
    with
      | exn -> Tnio.close sock; raise exn

let shutdown_connection = shutdown

let output_string ssl s deadline =
  let len = String.length s in
  if  write ssl s 0 len deadline <> len
  then raise Write_error
  else ()

let output_char ssl c deadline =
  let tmp = String.create 1 in
    tmp.[0] <- c;
    if write ssl tmp 0 1 deadline <> 1
    then raise Write_error
    else ()

let output_int ssl i deadline =
  let tmp = String.create 4 in
    tmp.[0] <- char_of_int (i lsr 24);
    tmp.[1] <- char_of_int ((i lsr 16) land 0xff);
    tmp.[2] <- char_of_int ((i lsr 8) land 0xff);
    tmp.[3] <- char_of_int (i land 0xff);
    if  write ssl tmp 0 4 deadline <> 4
    then raise Write_error
    else ()

let input_string ssl deadline =
  let bufsize = 1024 in
  let buf = String.create bufsize in
  let ret = ref "" in
  let r = ref 1 in
    while !r > 0
    do
      r := read ssl buf 0 bufsize deadline;
      ret := !ret ^ (String.sub buf 0 !r)
    done;
    !ret

let input_char ssl deadline =
  let tmp = String.create 1 in
    (if read ssl tmp 0 1 deadline <> 1
     then raise Read_error
     else ()
    ); tmp.[0]

let input_int ssl deadline =
  let i = ref 0 in
  let tmp = String.create 4 in
    if  read ssl tmp 0 4 deadline <> 4
    then raise Read_error
    else ();
    i := int_of_char (tmp.[0]);
    i := (!i lsl 8) + int_of_char (tmp.[1]);
    i := (!i lsl 8) + int_of_char (tmp.[2]);
    i := (!i lsl 8) + int_of_char (tmp.[3]);
    !i

