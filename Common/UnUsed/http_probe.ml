(*========================================*)
(*  Self-Testing: The Monitor Interface:  *)
(*========================================*)
(*  Performs ACTIVE liveness test of the MetaSearch Engine. If the latter is
  found alive, returns the current time. Otherwise, returns a time earlier
  than the max blocking interval, to force Engine re-starting by the Moni-
  tor (if used);
  "test_http_agent" uses pre-configured constant-value resolver to always
  return the IP the MetaSearch Engine is listening on:
*)
value test_http_agent_conf =
{
  Http_client.conf_client_ips = [];
  Http_client.conf_use_persist= False;
  Http_client.conf_buff_size  = Communic.http_buff_size;
  Http_client.conf_user_agents= []
};

value server_dom  =
   (Lukol_config.get_global_conf()).
  Lukol_config.fe_config.Lukol_config.our_domain;

value server_port =
   (Lukol_config.get_global_conf()).
  Lukol_config.fe_config.Lukol_config.listen_on_port;

value test_dns_agent  = Communic.mk_dns_agent dns_servers;

value test_http_agent:  Http_client.http_agent Tnio.tsock unit =
  Http_client.mk_http_agent
     (Communic.default_resolver test_dns_agent)
    Communic.http_transport
    None
    test_http_agent_conf;

(* Must ensure that the Search Term used for self-testing is non-"": *)
value test_term    =
  Netencoding.Url.encode
  ~plus:True
   (Lukol_config.get_global_conf()).
    Lukol_config.st_config.Lukol_config.test_search_term;

if  test_term = ""
then Misc_utils.fatal_error "Meta_search: Empty self-test search term"
else ();

(* And: must ensure the temporal self-test integrity, to prevent false
   negative results.   In particular, the period must be longer than 4
   minutes which is the maximum time required by the server to bind to
   the local IP/port:
*)
value test_timeout =
   (Lukol_config.get_global_conf()).
  Lukol_config.fe_config.Lukol_config.search_timeout;

value test_period  =
   (Lukol_config.get_global_conf()).
  Lukol_config.st_config.Lukol_config.test_period;

if  test_period < test_timeout || test_period <= Tnio.bind_wait
then
  Misc_utils.fatal_error
    ("Meta_search: Invalid self-test period, "^
     "must be longer than search time-out, and longer than 4 minutes")
else
  ();

(* Form the testing request: *)
value test_http_req   =
{
  Http_client.http_method = "GET";

  Http_client.full_url    =
    Printf.sprintf "http://%s:%d/search?st=%s"
    server_dom server_port test_term;

  Http_client.extra_headers = [];
  Http_client.cookies     = [];
  Http_client.form      = []
};

(* TODO:
   Perform separate self-requests for each real SearchEngine configured, to
   verify that they are all available.    For now, only one request (to all
   configured Search Engines) is performed:
*)
value test_fn =
   (Lukol_config.get_global_conf()).
  Lukol_config.st_config.Lukol_config.test_out_file;

value last_activity: unit -> float =
fun () ->
  (* Perform a self-request: *)
  let now   = Unix.gettimeofday ()    in
  let ancient = now -. 1.0 -. test_period in
  try
    let res = Http_client.http_transaction test_http_agent test_http_req
         (now +. test_timeout)
    in
    do {
      (* Save the result in a file, if specified, but don't propagate an
         error if it fails:
      *)
      if  test_fn <> ""
      then
        try
          let ch = open_out test_fn in
          do {
            output_string ch res.Http_client.body;
            close_out ch
          }
        with [_ -> ()]
      else ();
    
      (* Return the "last alive" time stamp: *)
      if   res.Http_client.body <> ""
      then Unix.gettimeofday ()   (* Even later than "now"! *)
      else ancient
    }
  with
  [hmm ->
   do {
    log ("MONITOR: SELF-TEST FAILED: "^(Misc_utils.print_exn hmm));
    ancient
  }];

