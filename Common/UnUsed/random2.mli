(***********************************************************************)
(*                                                                     *)
(*                           Objective Caml                            *)
(*                                                                     *)
(*              Damien Doligez, projet Para, INRIA Rocquencourt        *)
(*                                                                     *)
(*  Copyright 1996 Institut National de Recherche en Informatique et   *)
(*  en Automatique.  All rights reserved.  This file is distributed    *)
(*  under the terms of the GNU Library General Public License, with    *)
(*  the special exception on linking described in file ../LICENSE.     *)
(*                                                                     *)
(***********************************************************************)
(*=====================================================================*)
(*     Modified by LT, Explar Technologies Ltd, "Gatling" Project:     *)
(*          Added explicit state management for thread safety          *)
(*               (C) Explar Technologies Ltd, 2003--2004               *)
(*=====================================================================*)

(** Pseudo-random number generator (PRNG). *)

type rng_state
(** Values of this type are used to store the current state of the
   generator. *)

val static_init: rng_state
(** Static default initial state, no seed required **)

val init : int -> rng_state
(** Initialize the generator, using the argument as a seed.
     The same seed will always yield the same sequence of numbers. *)

val full_init : int array -> rng_state
(** Same as {!Random2.init} but takes more data as seed. *)

val self_init : unit -> rng_state
(** Initialize the generator with a more-or-less random seed chosen
   in a system-dependent way. *)

val bits : rng_state -> int
(** Return 30 random bits in a nonnegative integer. *)

val int : rng_state -> int -> int
(** [Random2.int rgs bound] returns a random integer between 0 (inclusive)
     and [bound] (exclusive).  [bound] must be more than 0 and less
     than 2{^30}. *)

val float : rng_state -> float -> float
(** [Random2.float rgs bound] returns a random floating-point number
   between 0 (inclusive) and [bound] (exclusive).  If [bound] is
   negative, the result is negative.  If [bound] is 0, the result
   is 0. *)

val bool : rng_state  -> bool
(** [Random2.bool rgs] returns [true] or [false] with probability 0.5 each. *)

val sys_random_seed: unit -> int

