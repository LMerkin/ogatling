(*pp ca,lp4r *)
(* vim:ts=4:syntax=ocaml
*)
(*===========================================================================*)
(*                                "gnutls.ml":                               *)
(*                   OCaml Binding to the GNU TLS Library                    *)
(*                     (C) Explar Technologies Ltd, 2004                     *)
(*===========================================================================*)
(*-----------*)
(*  "init":  *)
(*-----------*)
(* Global initialisation of the package.  The args provide the names of the:-
   trusted CAs file, revocation list file,   and a pair of files for our own
   (public_certificate, private_key).   If a filename is "", it is not used.
   The files must currently be in the PEM format:
*)
external init:
    ~trusted_cas_file: string           ->
    ~crl_file        : string           ->
    ~our_key_files   :(string * string) -> unit =
    "ogtls_global_init";

(*----------------*)
(*  "mk_session": *)
(*----------------*)
(* The TLS session type is directly implemented by the C library; "mk_session"
   opens a TNIO socket and wraps in into a session. This function is only used
   on the client side (on the server side, "accept" is used):
*)
type     session = 'a;

(* "invalid_session" is for equality comparison only: *)
external get_invalid_session: unit -> session = "ogtls_get_invalid_session";
value    invalid_session:  session =  get_invalid_session ();

(* "init_session" is used internally on both client and server sides: *)
external init_session: Tnio.tsock -> ~server:bool -> session =
    "ogtls_init_session";

(* So: *)
value mk_session: unit -> session =
fun ~server ->
    (* Only STREAM INET sockets are supported: *)
    let ts = Tnio.socket Unix.PF_INET Unix.SOCK_STREAM in
    init_session ts ~server:False;

(*--------------*)
(*  "connect":  *)
(*--------------*)
(*  For client sessions only; with an absolute deadline. Unlike UNIX sockets or
    TNIO-like "connect",   this function also requires the SYMBOLIC domain name
    of the server, in order to match it against the certificate received:
*)
external connect: string -> session -> Unix.sockaddr -> float -> unit =
    "ogtls_connect";

(*--------------*)
(*  "accept":   *)
(*--------------*)
value accept    : tsock -> float -> (session * Unix.sockaddr) -
fun master deadline ->
    (* Accept a connection and build a TLS session over the socket returned: *)
    let (ts, peer) = Tnio.accept master deadline in
    let session    = mk_session ~server:True  ts in

    
