(*pp camlp4r *)
(* vim:ts=4:syntax=ocaml
*)
(*===========================================================================*)
(*                              "communic.mli":                              *)
(*    DNS and Transport Facilities for the Gatling/Lukol MetaSearch Engine   *)
(*                     (C) Explar Technologies Ltd, 2004                     *)
(*===========================================================================*)
(*========*)
(*  DNS:  *)
(*========*)
(*  "mk_dns_agent":
    Creates a DNS Agent with some reasonable default configuration. Arg 1 is
    a list of DNS servers to be used (if empty, the internal direct resolver
    is used):
*)
value mk_dns_agent: list string -> Dns_client.dns_agent;

(*  "default_resolver":
    A DNS resolver which can be used by HTTP Front-End servers for resolving
    3rd-party domains for search term highlighting, etc. Not recommended for
    the back-end servers interfaced to the Search Engines:
*)
value default_resolver:
    Dns_client.dns_agent -> string -> list Unix.inet_addr;

(*  "se_resolver":
    Must be initialised before use (see "init_se_resolver"). Uses an L2 DNS
    cache for efficient resolution of all pre-configured Search Engine domains.
    Arg2 is a log function:
*)
value se_resolver:
    Dns_client.dns_agent -> (string -> unit) -> string -> list Unix.inet_addr;

(*  "init_se_resolver":
    Fills in the L2 DNS Cache:
    Arg 1: List of DNS servers (if empty, internal direct resolver is used);
    Arg 2: Gen SE Interfaces
    Arg 3: XML SE Interfaces
    Arg 4: Log function
*)
value init_se_resolver:
    list string                    ->
    Lukol_config.se_interfaces_gen ->
    Lukol_config.se_interfaces_xml ->
    (string -> unit)               ->
    unit;

(*=========*)
(*  HTTP:  *)
(*=========*)
(*  Default (non-SSL) transport to be used by all HTTP clients configured: *)
value http_transport: Http_client.transport Tnio.tsock;

(*  Misc: HTTP client settings: *)
value invalid_url:        string;

(*  Standard HTTP buffer size (for SE requests): *)
value http_buff_size:     int;

