(*pp camlp4r *)
(* vim:ts=4:syntax=ocaml
*)
(*===========================================================================*)
(*                           "tnio_https_test.ml":                           *)
(*                  TNIO RTS test, as a simple HTTPS Client                  *)
(*                  (C) Explar Technologies Ltd, 2003--2004                  *)
(*===========================================================================*)
let (server, n_threads, n_iters) =
try
    (Unix.inet_addr_of_string Sys.argv.(1),
     int_of_string            Sys.argv.(2),
     int_of_string            Sys.argv,(3)
    )
with[_ ->
do {
    prerr_endline
        "PARAMETERS: <Server_IP> <N_Threads> <N_Iters_Per_Thread>";
    exit 1
}];

Tnio.init (Tnio.Priority_Bands 5);

value rec read_lines: Ssl.socket -> string -> float -> unit =
fun s buff deadline ->
    let (got, err) =
        try  (Ssl.read s buff 0 10240 deadline, None)
        with [hmm -> (0, Some hmm)]
    in
    if  got = 0
    then
        Ssl.shutdown_connection s deadline
        (* All done! *)
    else
    do {
        match err with
        [ None     -> ()
        | Some hmm -> raise hmm];

        ignore (Unix.write Unix.stdout buff 0 got);
        read_lines s buff deadline
    };

value rec tfn: int -> unit =
fun n ->
if  n <= 0
then ()
else
do {
try
    let deadline = Unix.gettimeofday() +. 10.0 in
    let buff = String.make 10240 ' ' in

    let s = Ssl.mk_socket (Ssl.create_client_context Ssl.SSLv23) in

    let _ = Ssl.open_connection s
        (Unix.ADDR_INET (Unix.inet_addr_of_string server) 443) deadline
    in
    let _ = Ssl.write s "GET / HTTP/1.1\r\n"         0 16 deadline in
    let _ = Ssl.write s "Host: www.verisign.com\r\n" 0 24 deadline in
    let _ = Ssl.write s "Connection: close\r\n"      0 19 deadline in
    let _ = Ssl.write s "\r\n" 0 2 deadline in


    let _ = read_lines s buff deadline in
    Tnio.thread_sleep 1.0
with
    [ hmm ->
        let msg = Misc_utils.print_exn hmm in
        ignore (Unix.write Unix.stdout msg 0 (String.length msg))
    ];
ignore (Unix.write
        Unix.stdout "\n=================================================\n"
        0 50);
Gc.compact ();
tfn (n-1)
};

for i = 0 to n_threads-1
do {
    ignore (Tnio.thread_create tfn n_iters ~stack_size_k:256 ~priority:(i mod 5));
};

Tnio.threads_scheduler ();

for i = 0 to n_threads-1
do {
    ignore (Tnio.thread_create tfn n_iters ~stack_size_k:256 ~priority:(i mod 5));
};

Tnio.threads_scheduler ();
Tnio.exit 0;

