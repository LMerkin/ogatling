(*pp camlp4r *)
(* vim:ts=4:syntax=ocaml
*)
(*===========================================================================*)
(*                           "tnio_http_test.ml":                            *)
(*                 TNIO RTS test, as a simple HTTP(S) Client                 *)
(*                  (C) Explar Technologies Ltd, 2003--2004                  *)
(*===========================================================================*)
let (server, n_threads, n_iters) =
try
    (Unix.inet_addr_of_string Sys.argv.(1),
     int_of_string            Sys.argv.(2),
     int_of_string            Sys.argv,(3)
    )
with[_ ->
do {
    prerr_endline
        "PARAMETERS: <Server> <N_Threads> <N_Iters_Per_Thread>";
    exit 1
}];

Tnio.init (Tnio.Priority_Bands 5);

value rec read_lines: Tnio.tsock -> string -> float -> unit =
fun s buff deadline ->
    let (got, err) =
        try  (Tnio.recv s buff 0 10240 deadline, None)
        with [hmm -> (0, Some hmm)]
    in
    if  got = 0
    then
        Tnio.close s
        (* All done! *)
    else
    do {
        match err with
        [ None     -> ()
        | Some hmm -> raise hmm];

        ignore (Unix.write Unix.stdout buff 0 got);
        read_lines s buff deadline
    };

value rec tfn: int -> unit =
fun n ->
    if  n <= 0
    then ()
    else
    do {
    try
        let deadline = Unix.gettimeofday() +. 10.0 in
        let buff     = String.make 10240 ' '       in
        let s        = Tnio.socket Unix.PF_INET Unix.SOCK_STREAM in
        do {
            Tnio.connect s
            (Unix.ADDR_INET (Unix.inet_addr_of_string server) 80) deadline;

            Misc_utils.send_str Tnio.send s "GET / HTTP/1.0\r\n"  deadline;
            Misc_utils.send_str Tnio.send s "\r\n"                deadline;

            read_lines s buff deadline;
            Tnio.thread_sleep 1.0
        }
    with
        [ hmm ->
            let msg = Misc_utils.print_exn hmm in
            ignore (Unix.write Unix.stdout msg 0 (String.length msg))
        ];
            Unix.stdout "=================================================\n"
        0 50);
Gc.compact ();
tfn (n-1)
};

for i = 0 to n_threads-1
do {
    ignore (Tnio.thread_create tfn n_iters ~stack_size_k:256 ~priority:(i mod 5));
};

Tnio.threads_scheduler ();

for i = 0 to n_threads-1
do {
    ignore (Tnio.thread_create tfn n_iters ~stack_size_k:256 ~priority:(i mod 5));
};

Tnio.threads_scheduler ();
Tnio.exit 0;

