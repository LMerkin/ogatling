(* vim:ts=4:syntax=ocaml
*)
(*===========================================================================*)
(*                                  "ssl.ml":                                *)
(*      Adapted by Explar Technologies Ltd, 2004, for use with TNIO RTS      *)
(*===========================================================================*)
(* 
   Copyright 2003 Samuel Mimram

   This file is part of Ocaml-ssl.

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *)

type protocol =
  | SSLv2
  | SSLv23
  | SSLv3
  | TLSv1

type context

type certificate

type socket

type ssl_error =
  | Error_none
  | Error_ssl
  | Error_want_read
  | Error_want_write
  | Error_want_x509_lookup
  | Error_syscall
  | Error_zero_return
  | Error_want_connect
  | Error_want_accept

type verify_error =
  | Error_v_unable_to_get_issuer_cert (** Tthe issuer certificate could not be found: this occurs if the issuer certificate of an untrusted certificate cannot be found.*)
  | Error_v_unable_to_get_ctl (** The CRL of a certificate could not be found. Unused. *)
  | Error_v_unable_to_decrypt_cert_signature (** The certificate signature could not be decrypted. This means that the actual signature value could not be determined rather than it not matching the expected value, this is only meaningful for RSA keys.*)
  | Error_v_unable_to_decrypt_CRL_signature (** The CRL signature could not be decrypted: this means that the actual signature value could not be determined rather than it not matching the expected value. Unused. *)
  | Error_v_unable_to_decode_issuer_public_key (** The public key in the certificate SubjectPublicKeyInfo could not be read. *)
  | Error_v_cert_signature_failure (** The signature of the certificate is invalid. *)
  | Error_v_CRL_signature_failure (** The signature of the certificate is invalid. Unused. *)
  | Error_v_cert_not_yet_valid (** The certificate is not yet valid: the notBefore date is after the current time. *)
  | Error_v_cert_has_expired (** The certificate has expired: that is the notAfter date is before the current time. *)
  | Error_v_CRL_not_yet_valid (** The CRL is not yet valid. Unused. *)
  | Error_v_CRL_has_expired (** The CRL has expired. Unused. *)
  | Error_v_error_in_cert_not_before_field (** The certificate notBefore field contains an invalid time. *)
  | Error_v_error_in_cert_not_after_field (** The certificate notAfter field contains an invalid time. *)
  | Error_v_error_in_CRL_last_update_field (** The CRL lastUpdate field contains an invalid time. Unused. *)
  | Error_v_error_in_CRL_next_update_field (** The CRL nextUpdate field contains an invalid time. Unused. *)
  | Error_v_out_of_mem (** An error occurred trying to allocate memory. This should never happen. *)
  | Error_v_depth_zero_self_signed_cert (** The passed certificate is self signed and the same certificate cannot be found in the list of trusted certificates. *)
  | Error_v_self_signed_cert_in_chain (** The certificate chain could be built up using the untrusted certificates but the root could not be found locally. *)
  | Error_v_unable_to_get_issuer_cert_locally (** The issuer certificate of a locally looked up certificate could not be found. This normally means the list of trusted certificates is not complete. *)
  | Error_v_unable_to_verify_leaf_signature (** No signatures could be verified because the chain contains only one certificate and it is not self signed. *)
  | Error_v_cert_chain_too_long (** The certificate chain length is greater than the supplied maximum depth. Unused. *)
  | Error_v_cert_revoked (** The certificate has been revoked. Unused. *)
  | Error_v_invalid_CA (** A CA certificate is invalid. Either it is not a CA or its extensions are not consistent with the supplied purpose. *)
  | Error_v_path_length_exceeded (** The basicConstraints pathlength parameter has been exceeded. *)
  | Error_v_invalid_purpose (** The supplied certificate cannot be used for the specified purpose. *)
  | Error_v_cert_untrusted (** The root CA is not marked as trusted for the specified purpose. *)
  | Error_v_cert_rejected (** The root CA is marked to reject the specified purpose. *)
  | Error_v_subject_issuer_mismatch (** The current candidate issuer certificate was rejected because its subject name did not match the issuer name of the current certificate. *)
  | Error_v_akid_skid_mismatch (** The current candidate issuer certificate was rejected because its subject key identifier was present and did not match the authority key identifier current certificate. *)
  | Error_v_akid_issuer_serial_mismatch (** The current candidate issuer certificate was rejected because its issuer name and serial number was present and did not match the authority key identifier of the current certificate. *)
  | Error_v_keyusage_no_certsign (** The current candidate issuer certificate was rejected because its keyUsage extension does not permit certificate signing. *)
  | Error_v_application_verification (** An application specific error. Unused. *)

exception Method_error
exception Certificate_error
exception Cipher_error
exception Private_key_error
exception Unmatching_keys
exception Invalid_socket
exception Handler_error
exception Connection_error of ssl_error
exception Accept_error     of ssl_error
exception Read_error       of ssl_error
exception Write_error      of ssl_error
exception Verify_error     of verify_error

let () =
  Callback.register_exception "ssl_exn_method_error" Method_error;
  Callback.register_exception "ssl_exn_certificate_error" Certificate_error;
  Callback.register_exception "ssl_exn_cipher_error" Cipher_error;
  Callback.register_exception "ssl_exn_private_key_error" Private_key_error;
  Callback.register_exception "ssl_exn_unmatching_keys" Unmatching_keys;
  Callback.register_exception "ssl_exn_invalid_socket" Invalid_socket;
  Callback.register_exception "ssl_exn_handler_error" Handler_error;
  Callback.register_exception "ssl_exn_connection_error" (Connection_error Error_none);
  Callback.register_exception "ssl_exn_accept_error" (Accept_error Error_none);
  Callback.register_exception "ssl_exn_read_error"   (Read_error   Error_none);
  Callback.register_exception "ssl_exn_write_error"  (Write_error  Error_none);
  Callback.register_exception "ssl_exn_verify_error" (Verify_error Error_v_application_verification)

external init : unit -> unit = "ocaml_ssl_init"
(* So apply it right now! -- LT: *)
let () = init ()

external create_client_context : protocol -> context = "ocaml_ssl_create_client_context"

external create_server_context : protocol -> string -> string -> context = "ocaml_ssl_create_server_context"

external create_context : protocol -> string -> string -> context = "ocaml_ssl_create_context"

external embed_socket : Tnio.tsock -> context -> socket = "ocaml_ssl_embed_socket"

(* Extra by LT: *)
external get_invalid_socket: unit -> socket = "ocaml_ssl_invalid_socket"
let invalid_socket = get_invalid_socket ()


external set_cipher_list : context -> string -> unit = "ocaml_ssl_ctx_set_cipher_list"

external load_verify_locations : context -> string -> string -> unit = "ocaml_ssl_ctx_load_verify_locations"

type verify_mode =
  | Verify_peer
  | Verify_fail_if_no_peer_cert
  | Verify_client_once

type verify_callback

external get_client_verify_callback_ptr : unit -> verify_callback = "ocaml_ssl_get_client_verify_callback_ptr"

let client_verify_callback = get_client_verify_callback_ptr ()

external set_verify : context -> verify_mode list -> verify_callback option -> unit = "ocaml_ssl_ctx_set_verify"

external set_verify_depth : context -> int -> unit = "ocaml_ssl_ctx_set_verify_depth"

external set_client_CA_list_from_file : context -> string -> unit = "ocaml_ssl_ctx_set_client_CA_list_from_file"

type cipher

external get_cipher : socket -> cipher = "ocaml_ssl_get_current_cipher"

external get_cipher_description : cipher -> string = "ocaml_ssl_get_cipher_description"

(* TODO: get_cipher_bits *)

external get_cipher_name : cipher -> string = "ocaml_ssl_get_cipher_name"

external get_cipher_version : cipher -> string = "ocaml_ssl_get_cipher_version"

external get_certificate : socket -> certificate = "ocaml_ssl_get_certificate"

external read_certificate : string -> certificate = "ocaml_ssl_read_certificate"

external get_issuer : certificate -> string = "ocaml_ssl_get_issuer"

external get_subject : certificate -> string = "ocaml_ssl_get_subject"

external file_descr_of_socket : socket -> Tnio.tsock = "ocaml_ssl_get_file_descr"

external connect : socket -> float -> unit = "ocaml_ssl_connect"

external verify : socket -> unit = "ocaml_ssl_verify"

external write : socket -> string -> int -> int -> float -> int = "ocaml_ssl_write"

external read : socket -> string -> int -> int -> float -> int = "ocaml_ssl_read"

external accept : socket -> float -> unit = "ocaml_ssl_accept"

external flush : socket -> float -> unit = "ocaml_ssl_flush"

external shutdown_connection : socket -> float -> unit = "ocaml_ssl_shutdown"

let mk_socket ?(kern_buff_size=0) context =
  let ts = Tnio.socket
           ~kern_buff_size:kern_buff_size Unix.PF_INET Unix.SOCK_STREAM
  in
  embed_socket ts context

let open_connection ssl sockaddr deadline =
(
  (match sockaddr with
    | Unix.ADDR_INET(_, _) -> ()
    | _ -> failwith "Ssl.open_connection: not an INET address"
  );
  let ts = file_descr_of_socket ssl in
  try
  (
    Tnio.connect ts sockaddr deadline;
    connect ssl deadline
  )
  with
    | exn -> (Tnio.close ts; raise exn)
)

