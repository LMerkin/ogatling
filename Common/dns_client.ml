(*pp camlp4r *)
(* vim:ts=4:syntax=ocaml
*)
(*===========================================================================*)
(*                               "dns_client.ml":                            *)
(*      Iterative autonomous DNS resolver, with caching of all results       *)
(*                   (C) Explar Technologies Ltd, 2003--2006                 *)
(*===========================================================================*)
(*  NB:
    All the functions of this module are THREAD-SAFE provided that "dns_agent"
    instances are not shared between threads.
*)
(*======================================*)
(*  Data structures involved: borrowed  *)
(*  from the Erlang DNS implementation: *)
(*======================================*)
value buff_size = 8192;    (* Max size of UDP DNS datagrams -- is it enough? *)


(*-------------------------------------*)
(*  DNS Request and Response Messages: *)
(*-------------------------------------*)
(*  DMS Type: We only support a limited sub-set of possible types: *)
type dns_type  =
    [T_A | T_NS | T_CNAME | T_PTR | T_MX];


(*  DNS Query: The Class is not given -- always supposed to be the Internet: *)
(*  The Header is incorporated here, and there is only one question:         *)
type dns_query_msg =
{
    q_id    :   int;    (* Query identification number *)
    opcode  :   int;    (* Kind of the request *)
    rec_desr:   bool;   (* Recursion   desired *)

    q_domain:   string;
    q_type  :   dns_type
};


(*  DNS Resource: *)
(*  Used in "dns_rr" (response) records, see below: *)
type dns_resource =
[
    R_A     of Unix.inet_addr   (* IPv4 address *)
  | R_NS    of string           (* Name of an authoritative server *)
  | R_CNAME of string           (* Canon.  name *)
  | R_MX    of string and int   (* Mail exchange, with priority *)
  | R_PTR   of string           (* Dom name from a "reverse" query *)
];


(* DNS Response Record -- a Resource for a given Domain,  so it just includes
   a "dns_resource" object. We only provide Response Records for the Internet
   Class:
*)
type dns_rr =
{
    r_domain:   string;             (* Resource domain *)
    data    :   dns_resource;       (* The Resource itself,  for that domain *)
    expires :   float               (* Expir time in Seconds since the Epoch *)
};


(*  DNS Header for Responses: *)
type dns_resp_header =
{
    r_id    :   int;    (* Query / Response identification number *)
    auth_ans:   bool;   (* Authoritative   answer *)
    trunc   :   bool;   (* Truncated message   *)
    rec_avl :   bool;   (* Recursion available *)
    rcode   :   int     (* Response code *)
};


(*  Full DNS Response Message: *)
type dns_resp_msg =
{
    r_header :  dns_resp_header;
    answers  :  list dns_rr;
    auth_srvs:  list dns_rr;
    extras   :  list dns_rr
};


(*  High-level DNS Enquiry Result (can be assembled from multiple DNS
    responses): depending on the request type,  returns a list of IPs
    or a list of domain names:
*)
type dns_enquiry_res =
[
    IPs     of list Unix.inet_addr
  | Doms    of list string
];


(*  A "host" (e.g. a DNS server) can be specified by its IP addr or a
    Domain Name:
*)
type ip_or_domain =
[   IP  of Unix.inet_addr
  | Dom of string
];

(*  Special representation for "cooked-up" NS records:
    (Optional_NS_record, Server):
*)
type ns_rec    = ((option dns_rr) * ip_or_domain);
type ns_recs   = list ns_rec;


(*-----------------*)
(*  "get_ip":      *)
(*  "get_domain":  *)
(*-----------------*)
value get_ip: ip_or_domain -> Unix.inet_addr =
fun data ->
    match  data with
    [   IP ip -> ip
      | _     -> failwith "DNS_Cache.get_ip: Not an IP"
    ];


value get_domain: ip_or_domain -> string =
fun data ->
    match  data with
    [   Dom  dm -> dm
      | _       -> failwith "DNS_Cache.get_domain: Not a domain name"
    ];


(*---------------*)
(*  Exceptions:  *)
(*---------------*)
exception Negative_Result of string;    (* Confirmed negative result,
                                           e.g. non-existent domain  *)
exception No_More_Servers of string;    (* Dead  end   in resolving  *)
exception LowLevel_Error  of string;    (* Time-out or I/O error     *)
exception Recursion_Limit;              (* Max  recursion depth  hit *)
exception DNS_Error       of string;    (* Other kinds of DNS Errors *)


value is_transient: exn -> bool =
fun
[  LowLevel_Error _ -> True
|  _                -> False
];


(*---------------------*)
(*  Usage Statistics:  *)
(*---------------------*)
(*  The type for Exported Per-Agent Statistics.  Cache statistics is not
    included, as there may be more than one cache, and anyway, such sta-
    tistics is less important for the caching algorithms currently used:
*)
type dns_stats =
{
    total_enquiries   : int;
    succ_enquiries    : int;
    av_wasted_sub_reqs: float;
    av_resp_time_sec  : float;
    errors            : Hashtbl.t string int;
    cache_stats       : Cache_simple.cache_stats
};


(*=================================*)
(*  Initialisation / Finalisation: *)
(*=================================*)
(*-------------*)
(*  DNS Cache: *)
(*-------------*)
(*  The caching Keys are pairs (DNS_Type, Domain_Name), and the cached Values
    are pairs (IP_or_Domain, ExpirTime):
*)
type key_t = (dns_type     * string);
type val_t = (ip_or_domain * float );


value string_of_dns_type: dns_type -> string =
fun
[   T_A     -> "A"
  | T_NS    -> "NS"
  | T_CNAME -> "CNAME"
  | T_MX    -> "MX"
  | T_PTR   -> "PTR"
];


value string_of_ip_or_domain:  ip_or_domain -> string =
fun
[   IP  ip  -> Unix.string_of_inet_addr ip
  | Dom dom -> dom
];


(* "string_of_dns_key": *)
(* Used when DNS Cache objects are created: *)

value string_of_dns_key: key_t -> string =
    fun (dnst, dom) ->
    Printf.sprintf "(%s,%s)" (string_of_dns_type dnst) dom;


(*----------------*)
(*  "dns_agent":  *)
(*----------------*)
type dns_agent =
{
    sock       : mutable Tnio.tsock;(* Client UDP socket, timed      *)
    io_buff    : string;            (* Socket I/O buffer             *)
    enc_buff   : Buffer.t;          (* Requests are formed here      *)

    (* The following fields are mutable, to allow re-configuration:  *)

    n_retries  : mutable int;       (* Number of retries on each UDP request *)
    timeout_sec: mutable float;     (* Time-out  on each UDP request *)

    max_depth  : mutable int;
    rem_depth  : mutable int;       (* Remaining depth of recursive resolutions
                                       (to protect against inf. recursion)   *)

    (* "cache", however small, is always required for the operation of the
        resolution algorithm:
    *)
    cache      : Cache_simple.cache_t  key_t  val_t;

    (* Internal statistics: *)
    m_total_enquiries       : mutable int;
    m_succ_enquiries        : mutable int;
    m_total_wasted_sub_reqs : mutable int;
    m_total_resp_time_sec   : mutable float;

    (* The "resolution history" is maintained, in order to detect possible loops
       early and not run the full way down to the recursion barrier. The history
       elements are triples (DNS_Type, Subject_Domain, Server):
    *)
    res_history : mutable list (dns_type * string * ip_or_domain);

    (* Distribution of various errors encountered: *)
    errs_distrib: Hashtbl.t    string   int;

    (* Local recursive DNS servers to be used, if any: *)
    dns_servers : mutable ns_recs;

    (*  Debugging Stream:
        If it is set to non-None, debugging info is printed into that stream:
    *)
    debug_stream: mutable option out_channel
};


(*-----------------------*)
(*  "set_debug_stream":  *)
(*-----------------------*)
value set_debug_stream: dns_agent -> option out_channel -> unit =
fun agent opt_stream ->
    agent.debug_stream:= opt_stream;


(*-----------------*)
(*  "print_debug": *)
(*-----------------*)
value print_debug: dns_agent -> string -> unit =
fun agent msg ->
    match agent.debug_stream with
    [   Some stream -> do {output_string stream msg; flush stream}
      | None        -> ()
    ];


(*------------------------*)
(*  "string_of_history":  *)
(*------------------------*)
value string_of_history: dns_type -> string -> ip_or_domain -> string =
fun dnst domain server ->
    Printf.sprintf  "(%s, %s, %s)"
                    (string_of_dns_type dnst)
                    domain
                    (string_of_ip_or_domain server);


(*-------------------*)
(*  "mk_dns_agent":  *)
(*-------------------*)
(* Creates a new DNS Agent, including an open UDP client socket: *)

type dns_agent_config =
{
    (* Time-out on individual server enquiries: *)
    conf_timeout_sec   : mutable float;

    (* Number of re-tries on failed server requests, >= 0: *)
    conf_n_retries     : mutable int;

    (* Maximum depth of sub-enquiries (e.g. 50) *)
    conf_max_depth     : mutable int;

    (* Max DNS cache size: *)
    conf_cache_size    : mutable int;

    (* UDP socket kenel buffer size: *)
    conf_sock_buff_size: mutable int;

    (* Local recursive DNS servers to be used:  *)
    conf_dns_servers   : mutable list string
};

value default_dns_agent_config: unit -> dns_agent_config =
fun () ->
{
    conf_timeout_sec   = 2.0;
    conf_n_retries     = 1;
    conf_max_depth     =  50;
    conf_cache_size    = 256;
    conf_sock_buff_size= 128*1024;
    conf_dns_servers   = []
};

value mk_dns_servers: list string -> ns_recs =
fun ip_strs ->
    List.map
       (fun ip -> (None, IP (Unix.inet_addr_of_string ip)))
       ip_strs;


value mk_dns_agent: dns_agent_config -> dns_agent =
fun c ->
do {
    if  c.conf_timeout_sec < 0.0 || c.conf_n_retries < 0 ||
        c.conf_max_depth   < 0
    then
        invalid_arg "Dns_client.mk_dns_agent"
    else
        ();

    (* In Cache config, using the "TotalAcc" history mode is more sensible: *)
    let cache_conf  =
    {
        Cache_simple.conf_max_size      = c.conf_cache_size;
        Cache_simple.conf_history_how   = Cache_simple.TotalAccs;
        Cache_simple.conf_cl_threshold  = 0.5;
        Cache_simple.conf_string_of_key = Some string_of_dns_key;
        Cache_simple.conf_finaliser     = None
    }
    in
    {
        (* Create the client DNS socket: *)
        sock         = Tnio.socket
                       ~kern_buff_size:c.conf_sock_buff_size
                       Unix.PF_INET Unix.SOCK_DGRAM;

        io_buff      = String.create buff_size;
        enc_buff     = Buffer.create buff_size;
        n_retries    = c.conf_n_retries;
        timeout_sec  = c.conf_timeout_sec;
        max_depth    = c.conf_max_depth;
        rem_depth    = c.conf_max_depth;

        (* Create the Cache: *)
        cache        = Cache_simple.create cache_conf;

        m_total_enquiries       = 0;
        m_succ_enquiries        = 0;
        m_total_wasted_sub_reqs = 0;
        m_total_resp_time_sec   = 0.0;

        res_history  = [];

        (* The number of different errors is small: *)
        errs_distrib = Hashtbl.create 10;

        (* Local recursive DNS servers to be used : *)
        dns_servers  = mk_dns_servers c.conf_dns_servers;

        (* Debugging stream: initially None: *)
        debug_stream = None
    }
};

(*--------------------------*)
(*  Re-Configuring Agents:  *)
(*--------------------------*)
(*  NB: the "cache_size" is only the initial estimate of the cache size, so
    it cannot be re-configured. When read from the Agent, the actual  cache
    size is returned; when we re-configure teh Agent, the new cache size is
    ignored:
*)
value config_of_dns_agent : dns_agent -> dns_agent_config =
fun agent ->
{
    conf_timeout_sec   = agent.timeout_sec;
    conf_n_retries     = agent.n_retries;
    conf_max_depth     = agent.max_depth;

    conf_cache_size    =
        (Cache_simple.get_stats agent.cache).Cache_simple.stat_curr_keys;

    (* NB: the MAXIMUM of RCV and SND values is returned (normally they are
       equal, though):
    *)
    conf_sock_buff_size=
        max (Tnio.getsockopt_int agent.sock Unix.SO_RCVBUF)
            (Tnio.getsockopt_int agent.sock Unix.SO_SNDBUF);

    conf_dns_servers   = List.map
        (fun (_, ns)  -> Unix.string_of_inet_addr (get_ip ns))
        agent.dns_servers
};

value reconfigure_dns_agent: dns_agent -> dns_agent_config -> unit =
fun agent c ->
do {
    agent.timeout_sec := c.conf_timeout_sec;
    agent.n_retries   := c.conf_n_retries;
    agent.max_depth   := c.conf_max_depth;
    agent.dns_servers := mk_dns_servers c.conf_dns_servers
};


(*----------------------*)
(*  "close_dns_agent":  *)
(*----------------------*)
(*  Closes the client UDP socket: *)

value close_dns_agent: dns_agent -> unit =
fun agent ->
do{
    Tnio.close agent.sock;
    agent.sock := Tnio.invalid_tsock
};


(*=======================*)
(*  Static Information:  *)
(*=======================*)
(*  In UNIX-like systems, if the file "/etc/hosts" exists, we read in and parse
    its contents.   It will be used for both A- and MX-resolutions, with higher
    priority than any DNS servers.  Static DNS info is a small read-only cache:
    XXX: YP (NIS)- and NetInfo-based systems are not yet supported:
*)
value static_hosts: Hashtbl.t string Unix.inet_addr = Hashtbl.create 256;
try
    let host_lines = Misc_utils.read_lines "/etc/hosts" in
    List.iter
        (fun line ->
            (* Drop comments and surrounding spaces: *)
            let dline =
                try
                    let n = String.index line '#' in
                    String.sub line 0 n
                with
                [ Not_found -> line ]
            in
            let sline = ExtString.String.strip dline    in
            if  sline = ""
            then
                (* Ignore it: *)
                ()
            else
                (* Parse the "sline". The 1st field is the IP, the rest are
                   the host name and its aliases. Lines which yield parsing
                   errors are ignored:
                *)
                let flds = Misc_utils.split_by_whites sline in
                try
                    let ip     = Unix.inet_addr_of_string (List.hd flds) in
                    let hnames = List.tl flds in

                    (* Store the parsed info in the hash table: *)
                    List.iter
                        (fun hn ->
                            Hashtbl.add static_hosts (String.lowercase hn) ip)
                        hnames
                with
                    [ _ -> ()]
        )
        host_lines
with
    [ _ -> ()];


(*========================*)
(*  Auxiliary Functions:  *)
(*========================*)
(*-----------------------*)
(*  "normalise_domain":  *)
(*-----------------------*)
value normalise_domain: string -> string =
fun nm ->
    String.lowercase nm;


(*--------------------*)
(*  "get_resp_ipdom": *)
(*  " get_resp_type":  *)
(*--------------------*)
value get_resp_ipdom: dns_rr -> ip_or_domain =
fun rr ->
    (* Extracts  an IP or a Domain Name  from a DNS Response Record: *)
    match rr.data with
    [
        R_A     ip   -> IP  ip
      | R_NS    nm   -> Dom nm
      | R_CNAME nm   -> Dom nm
      | R_MX    nm _ -> Dom nm
      | R_PTR   nm   -> Dom nm
    ];


value get_resp_type: dns_rr -> dns_type =
fun rr ->
    (* Determines the type of a Response Record: *)
    match rr.data with
    [
        R_A       _  -> T_A
      | R_NS      _  -> T_NS
      | R_CNAME   _  -> T_CNAME
      | R_MX    _ _  -> T_MX
      | R_PTR     _  -> T_PTR
    ];


(*--------------------------*)
(*  "homogenise_dns_data":  *)
(*--------------------------*)
(*  Construct an "IPs" or "Doms" object from a list of "ip_or_domain"s which
    we KNOW contains only IPs or only Domain Names:
*)
value homogenise_dns_data: list ip_or_domain -> dns_type -> dns_enquiry_res =
fun data  dnst ->
    if  dnst = T_A
    then IPs  (List.map get_ip     data)
    else Doms (List.map get_domain data);


(*---------------------*)
(*  "label_dns_data":  *)
(*---------------------*)
(*  The inverse of "homogenise_dns_data": assigns individual labels (IP or Dom
    ctors) to each element of a "dns_enquiry_res":
*)
value label_dns_data: dns_enquiry_res -> list ip_or_domain =
fun
[   IPs  ips  -> List.map (fun ip -> IP  ip)  ips
  | Doms doms -> List.map (fun dom-> Dom dom) doms
];


(*---------------------------*)
(*  "dns_cache_insert_rr":   *)
(*---------------------------*)
(*  Saves the contents of an individual Response Record in the Cache(es).
    NB: NS-records are currently NOT cached:
*)
value dns_cache_insert_rr: dns_agent -> dns_rr -> unit =

fun   agent rr ->
    let dnst = get_resp_type rr in

    (* Although this function is not supposed to be called on NS records at
       all, stil lcheck that they did not get through somehow. Also, do NOT
       cache this record if it has zero TTL:
    *)
    if  dnst = T_NS || rr.expires = 0.0
    then
        ()
    else
        let r_dom= rr.r_domain      in
        let k    = (dnst,  r_dom)   in
        let v    = (get_resp_ipdom  rr, rr.expires) in

        (* Store it in the Cache: *)
        Cache_simple.store agent.cache k v;


(*---------------------------*)
(*  "dns_cache_insert_msg":  *)
(*---------------------------*)
(*  Saves all useful information from a "dns_resp_msg" in the DNS Cache. As
    we don't cache the NS-records, we SKIP the "auth_srvs" section -- NS re-
    cords are inserted only when they are verified  AFTER a successful reso-
    lution:
*)
value dns_cache_insert_msg: dns_agent -> dns_resp_msg -> unit =
fun agent msg ->
do {
    List.iter (dns_cache_insert_rr agent) msg.answers;
    List.iter (dns_cache_insert_rr agent) msg.extras
};


(*-----------------------*)
(*  "dns_cache_lookup":  *)
(*-----------------------*)
(*  Search for the required (dns_type, domain) key  in the Cache: *)

value dns_cache_lookup:
      dns_agent -> dns_type -> string ->  (dns_type * list ip_or_domain) =

fun agent dnst domain ->
    let vals = Cache_simple.lookup agent.cache (dnst, domain) in

    (* IMPORTANT: If "vals" appeared to be empty, all may still not
       be lost: we can have a "CNAME" record for ths same domain in
       the cache, rather than the type we are looking for:
    *)
    let (dnst', vals') =
        if  vals = [] && dnst <> T_CNAME
        then
            (T_CNAME, Cache_simple.lookup agent.cache (T_CNAME, domain))
        else
            (* Accept the result as it is: *)
            (dnst, vals)
    in
    (* Also, filter out the expired items. The order of results does not
       matter:
    *)
    let now = Unix.gettimeofday () in
    let res =
        List.fold_left
            (fun curr (v, expir) ->
                if  expir >= now
                then  [v ::curr]
                else  curr
            )
            [] vals'
    in
    (dnst', res);

(*===========================================*)
(*  Low-Level Encoding/Decoding Operations:  *)
(*===========================================*)
value c_in = 1;   (* The Internet Class code *)


(*=============*)
(*  Encoding:  *)
(*=============*)
(*-------------*)
(*  "write1":  *)
(*-------------*)
(*  Writes a 1-byte integer (0..255) into a Buffer: *)

value write1: Buffer.t -> int -> unit =
fun buff b ->
    Buffer.add_char buff (char_of_int b);


(*-------------*)
(*  "write2":  *)
(*-------------*)
(* Writes a 2-byte integer (0..65535) into a Buffer,
   in the Big-Endian order:
*)
value write2: Buffer.t -> int -> unit =
fun buff s ->
do {
    assert  ((s >= 0) && (s <= 65535));
    write1  buff (s lsr  8);
    write1  buff (s land 255)
};


(*---------------*)
(*  "write_lb":  *)
(*---------------*)
(*  Writes a Label (a string prefixed by a 1-byte length) into a Buffer: *)

value write_lb: Buffer.t -> string -> unit =
fun buff  lb ->
    let len = String.length lb  in

    if  len >= 64
    then
        invalid_arg "Dns_client.write_lb: Label too long"
    else
    do {
        write1 buff len;
        Buffer.add_string buff lb
    };


(*----------------*)
(*  "write_dom":  *)
(*----------------*)
(*  Writes a Domain Name into a Buffer, as a sequence of Labels: *)

value write_dom: Buffer.t -> string -> unit =
fun buff  dom  ->
    let dcomps = ExtString.String.nsplit dom "." in
    do {
        if  List.length dcomps > 63
        then
            invalid_arg "Dns_client.write_dom: Too many domain components"
        else
            List.iter (write_lb buff) dcomps;

        write1 buff 0
    };


(*---------------------*)
(* "encode_dns_query": *)
(*---------------------*)
(*  Builds an DNS request datagram from a "dns_query_msg" object: *)

value encode_dns_query: dns_agent -> dns_query_msg -> string =
fun agent msg ->
    let flags= (msg.opcode lsl 11)   +
               (if  msg.rec_desr  then (1 lsl 8) else 0)  in
    do {
        (* Clear the buffer before all: *)
        Buffer.clear agent.enc_buff;

        (* Encode the Header first: *)

        write2 agent.enc_buff msg.q_id;
        write2 agent.enc_buff flags;
        write2 agent.enc_buff 1;        (* Questions Count *)
        write2 agent.enc_buff 0;        (* Answers   Count *)
        write2 agent.enc_buff 0;        (* NS recs   Count *)
        write2 agent.enc_buff 0;        (* Extras    Count *)

        (* Questions Section: *)

        (* Domain: *)
        write_dom agent.enc_buff msg.q_domain;

        (* Type:   *)
        write2    agent.enc_buff
            (match msg.q_type with
            [
                T_A     -> 1
              | T_NS    -> 2
              | T_CNAME -> 5
              | T_PTR   -> 12
              | T_MX    -> 15
            ]);

        (* Class, always C_IN: *)
        write2    agent.enc_buff   c_in;

        (* Return the result: *)
        Buffer.contents agent.enc_buff;
    };


(*============*)
(*  Decoding: *)
(*============*)
(* NB: all "read*" functions take the current buffer position as the 2nd arg,
   and return (result, new_buffer_position):
*)
(*------------*)
(*  "read1":  *)
(*------------*)
(*  Reads a 1-byte integer from a Buffer: *)
value read1: string -> int -> (int*int) =
fun buff p ->
    (int_of_char (buff.[p]), p+1);


(*------------*)
(*  "read2":  *)
(*------------*)
(* Reads a 2-byte integer from a Buffer, in the Big Endian format: *)

value read2:  string -> int -> (int*int) =
fun buff p0 ->
    let (byte1, p1) = read1 buff p0 in
    let (byte2, p2) = read1 buff p1 in

    ((byte1 lsl  8) + byte2, p2);


(*------------*)
(*  "read4":  *)
(*------------*)
(* Reads a 4-byte integer from a Buffer, in the Big Endian format: *)

value read4:  string -> int -> (int*int) =
fun buff p0 ->
    let (shrt1, p1) = read2 buff p0 in
    let (shrt2, p2) = read2 buff p1 in

    ((shrt1 lsl 16) + shrt2, p2);


(*---------------*)
(*  "read_dom":  *)
(*---------------*)
(* Reads a (possibly compressed) Domain Name from a Buffer: *)

value rec read_dom_labels: string -> int -> string -> (string * int) =

fun buff p0  curr_dom ->
    (* Get the length of the current label: *)
    let (ll, p1) = read1 buff p0  in

    if   ll = 0
    then
        (* All done! *)
        (normalise_domain curr_dom,  p1)
    else
        if  (ll land 0xC0) <> 0
        then
            (* It's a "pointer", not a length! Dereference it: *)
            let  hi      = ll land  0x3F    in
            let (lo, p2) = read1 buff p1    in
            let  p3      = (hi lsl 8) + lo  in

            (* Read the entire rest of the domain name at "p3",
               but since it was a jump, ignore the resulting offset:
            *)
            let (rest,_) = read_dom buff p3 in

            let new_dom  = if   curr_dom = ""
                           then rest
                           else curr_dom ^ "." ^ rest
            in
            (new_dom, p2)

        else
            (* Just a normal label of length "ll": *)

            let lbl = String.sub buff p1 ll in
            let pn  = p1 + ll               in

            let new_dom = if   curr_dom = ""
                          then lbl
                          else curr_dom ^ "." ^ lbl
            in
            (* Read other labels of this domain name: *)
            read_dom_labels    buff pn  new_dom


and read_dom: string -> int -> (string * int) =
fun buff p0 ->
    (* Don't forget to normalise the domain name read before returning it: *)
    let (dom, p1) = read_dom_labels buff p0 ""  in
    (normalise_domain dom, p1);


(*-------------------------*)
(*  "decode_resp_header":  *)
(*-------------------------*)
(* Constructs a Response Header and lengths of all response sections: *)

value decode_resp_header: string ->
                         (dns_resp_header * int * int * int * int * int)  =
fun buff ->
    (* Get the Response ID: *)
    let (rid,  p1) = read2 buff  0 in

    (* Get the Flags: *)
    let (flags,p2) = read2 buff p1 in

    let _ =
        if   (flags land (1 lsl 15)) =  0
        then raise
             (DNS_Error "Dns_client.decode_resp_header: Not a Response")
        else True
    in

    let aa = (flags land (1 lsl 10)) <> 0  in
    let tc = (flags land (1 lsl  9)) <> 0  in
    let ra = (flags land (1 lsl  7)) <> 0  in

    (* Get the Return Code: *)
    let rc = (flags land 15) in

    (* Get the Counters: *)
    let (q_count,  p3) = read2 buff p2 in
    let (a_count,  p4) = read2 buff p3 in
    let (ns_count, p5) = read2 buff p4 in
    let (e_count,  p6) = read2 buff p5 in

    (* Make the Header : *)
    let hdr =
    {
        r_id     = rid;
        auth_ans = aa;
        trunc    = tc;
        rec_avl  = ra;
        rcode    = rc
    }   in

    (* The final result: *)
    (hdr, q_count, a_count, ns_count, e_count, p6);


(*-------------------------*)
(*  "decode_resp_section": *)
(*-------------------------*)
(*  Constructs a single "dns_rr" object which is a part of a DNS response:  *)

value decode_resp_section: string -> int -> ((option dns_rr) * int) =
fun buff p0 ->

    (* Get the Domain Name: *)
    let (dom, p1)       = read_dom buff p0  in

    (* Get the Type. It's an "option" here, as unknown types are tolerated for
       now, since we need to parse the section to the end:
    *)
    let (type_code, p2) = read2    buff p1  in

    let dnsto = match type_code with
    [
        1  -> Some T_A
      | 2  -> Some T_NS
      | 5  -> Some T_CNAME
      | 12 -> Some T_PTR
      | 15 -> Some T_MX
      | _  -> None
    ]
    in

    (* Verify the Class -- it must be Internet in any case: *)
    let p3 =
    (
        let (cl, p3') = read2 buff p2 in
        if   cl <> c_in
        then raise (DNS_Error
                   ("Dns_client.decode_resp_section: Invalid DNS Class: "^
                   (string_of_int cl)))
        else p3'
    )
    in

    (* Get the Time-To-Live for this record, and compute its
       expiration time in Seconds from the Epoch:
    *)
    let (ttl,  p4) = read4 buff p3  in

    (* Get the length of the following data, and the data itself: *)
    let (dlen, p5) = read2 buff p4  in

    (* Get and interpret the data: *)
    let res =
        match dnsto with
        [
          Some T_A ->
          do {
            (* The data are 4 bytes of the IP addr: *)
            if  dlen <> 4
            then raise (DNS_Error
                       ("Dns_client.decode_resp_section: IP Len <> 4: "^
                       (string_of_int dlen)))
            else ();

            let ipd= String.sub  buff p5 dlen  in

            let a0 = int_of_char ipd.[0] in
            let a1 = int_of_char ipd.[1] in
            let a2 = int_of_char ipd.[2] in
            let a3 = int_of_char ipd.[3] in

            (* This is not the most efficient conversion, but the only
               one available in OCaml,   since the "inet_addr" type is
               abstract, and can only be constructed from a string:
            *)
            let ip_str = Printf.sprintf "%d.%d.%d.%d" a0 a1 a2 a3   in

            (* Result: A-Record: *)
            Some (R_A (Unix.inet_addr_of_string ip_str))
          }

        | Some T_NS | Some T_CNAME | Some T_PTR ->
            (* The data are a just a Domain Name: *)
            let (jdom, p6) = read_dom buff p5  in

            if  p6 <> p5 + dlen
            then raise
                 (DNS_Error (Printf.sprintf
                 "Dns_client.decode_resp_section: Domain Len: %d <> %d"
                 (p6 - p5) dlen))
            else
                (* Result: depens on the type: *)
                match dnsto with
                [
                    Some T_NS    -> Some (R_NS    jdom)
                  | Some T_CNAME -> Some (R_CNAME jdom)
                  | Some T_PTR   -> Some (R_PTR   jdom)
                  | _            -> assert False
                ]

        | Some T_MX ->
            (* The data are (Preference, ExchangeDomain): *)

            let (pref,  p6) = read2    buff p5  in
            let (mxdom, p7) = read_dom buff p6  in

            if  p7 <> p5 + dlen
            then raise
                 (DNS_Error
                 (Printf.sprintf
                 "Dns_client.decode_resp_section: MX Record Len: %d <> %d"
                 (p7 - p5) dlen))
            else
                (* Result: MX-record: *)
                Some (R_MX mxdom pref)


        | None ->
            (* Cannot parse the data, as we do not recognise the resp type.
               Insert synthetic invalid result here:
            *)
            None
        ]
    in
    (* Construct the resulting "dns_rr" object, if possible: *)
    let rro = match res with
    [
        None         -> None

      | Some res_data-> Some
        {
            r_domain = dom;

            expires  =
                if   ttl = 0
                then 0.0   (* Will not be cached *)
                else (Unix.time ()) +. (float_of_int ttl);

            data     = res_data
        }
    ]
    in
    (*  The final result: *)
    (rro, p5 + dlen);


(*--------------------*)
(*  "sort_sections":  *)
(*--------------------*)
(*  Sorts R_MX records according to the Preference, leaving the order of all
    other sections unchanged.  Normally, the list to be sorted would contain
    either ONLY R_MX records or none of them, but we can't guarantee that in
    all cases, so use the general stable sort:
*)
value sort_sections: list dns_rr -> list dns_rr =
fun ress ->
    List.stable_sort
    (fun l r ->
        match (l.data,  r.data) with
        [   (R_MX _ pl, R_MX _ pr) -> compare pl pr
          | _ ->  0  (* Considered equal *)
        ]
    )
    ress;

(*-------------------------*)
(*  "decode_n_sections":   *)
(*-------------------------*)
(*  NB: This function is not tail-recursive, but that should be OK as the
    number of sections in a DNS response is normally small. Errors due to
    "UnExpected_DNS_Type" are TOLERATED here,  as other records may still
    be OK:
*)
value rec   decode_n_sections: string -> int -> int -> (list dns_rr * int) =
fun buff p0 n_sections ->
    if   n_sections <=  0
    then
        ([], p0)
    else
        match decode_resp_section buff p0 with
        [
            (None, p1) ->
                decode_n_sections buff p1 (n_sections-1)

          | (Some sectn, p1) ->
                let (others, pn) = decode_n_sections buff p1 (n_sections-1)
                in
                (* NB: The order of sections must be preserved here: *)
                let all_sectns   = [sectn::others]  in

                (* However, for MX records, the sections must be sorted
                   in the increasing order of Preferences. In all other
                   cases, the order will be unchanged:
                *)
                (sort_sections all_sectns, pn)
        ];


(*------------------------*)
(*  "skip_q_section(s)":  *)
(*------------------------*)
(*  Skip the given number of Question Sections: *)

value skip_q_section:  string -> int -> int =
fun buff p0 ->
    (* Here comes a domain name: *)
    let (_, p1) = read_dom buff p0  in

    (* After that, 2-byte Type and Class: *)
    p1 + 4;


(* NB: this function is also not tail-recursive: *)
value rec  skip_q_sections: string -> int -> int -> int =
fun buff p0 q_count  ->
    if   q_count <= 0
    then
        p0
    else
        let  p1 = skip_q_section  buff p0 in

        skip_q_sections  buff p1 (q_count-1);


(*---------------------*)
(*  "decode_dns_resp": *)
(*---------------------*)
value decode_dns_resp: string -> int -> dns_resp_msg =
fun buff dgram_size ->
try
    (* Decode the Header: *)
    let (hdr,  q_count,  a_count,  ns_count,  e_count,  p0) =
        decode_resp_header buff
    in

    (* Skip the Questions sections: *)
    let p1            = skip_q_sections   buff p0 q_count  in

    (* Decode the Answers sections: *)
    let (answers, p2) = decode_n_sections buff p1 a_count  in

    (* Decode the NS      sections: *)
    let (ns, p3)      = decode_n_sections buff p2 ns_count in

    (* Decode the Extra   sections: *)
    let (extras,  p4) = decode_n_sections buff p3 e_count  in

    if  p4 = dgram_size
    then
        (* The result: *)
        {
            r_header  = hdr;
            answers   = answers;
            auth_srvs = ns;
            extras    = extras
        }
    else
        raise (DNS_Error "")
with
[_ ->
    raise (DNS_Error
          ("Dns_client.decode_dns_resp: Malformed DNS response, size "^
          (string_of_int dgram_size)))
];


(*=====================*)
(*  UDP Communications:*)
(*=====================*)
(*--------------------------*)
(*  "get_dns_server_resp":  *)
(*--------------------------*)
(*  Wait for the response datagram which matches   any of the given Query IDs,
    with a time-out. The "LowLevel_Error" exception is raised if the time-out
    expires:
*)
value rec get_dns_server_resp:  dns_agent -> int -> float -> dns_resp_msg =
fun agent qid deadline ->
do {
    try
        let dgram_size = Tnio.read agent.sock agent.io_buff 0
                             (String.length agent.io_buff) deadline in

        let ()  = assert (dgram_size > 0 && dgram_size < buff_size) in

        let msg = decode_dns_resp agent.io_buff dgram_size in
        let hdr = msg.r_header in

        if  hdr.r_id = qid
        then
        do {
            (* Yes, this is a response we were waiting for.  We only cache the
               contents of the response in  this case, to prevent cache "pois-
               oning" by irrelevant data:
            *)
            dns_cache_insert_msg agent msg;
            msg
        }
        else
            (* Improper response:  try again, if the same "deadline" allows: *)
            get_dns_server_resp agent qid deadline

    with
    [hmm ->
        (* "read" error -- may be worth re-trying later *)
        raise (LowLevel_Error
              ("Dns_client.get_dns_server_resp: "^(Misc_utils.print_exn hmm)))
    ]
};


(*==========================*)
(*  DNS Servers management: *)
(*==========================*)
(*-----------------------*)
(*  "root_dns_servers":  *)
(*-----------------------*)
value root_dns_servers: ns_recs =
    List.map
    (fun s -> (None, IP (Unix.inet_addr_of_string s)))
    [
    "198.41.0.4";     (* A.Root-Servers.Net *)
    "128.9.0.107";    (* B.Root-Servers.Net *)
    "192.33.4.12";    (* C.Root-Servers.Net *)
    "128.8.10.90";    (* D.Root-Servers.Net *)
    "192.203.230.10"; (* E.Root-Servers.Net *)
    "192.5.5.241";    (* F.Root-Servers.Net *)
    "192.112.36.4";   (* G.Root-Servers.Net *)
    "128.63.2.53";    (* H.Root-Servers.Net *)
    "192.36.148.17";  (* I.Root-Servers.Net *)
    "198.41.0.10";    (* J.Root-Servers.Net *)
    "193.0.14.129";   (* K.Root-Servers.Net *)
    "198.32.64.12";   (* L.Root-Servers.Net *)
    "202.12.27.33"    (* M.Root-Servers.Net *)
    ];


(*-----------------------*)
(*  "find_dns_servers":  *)
(*-----------------------*)
(* Finds suitable NS records, if any, in the Cache for a given Domain or any of
   its enclosing Domains. If there are no such records at all, Root DNS Servers
   are returned. The actual Key=(NS,Domain)  of records found in the Cache  (if
   any) is returned as well,  so if the resolution process then fails on such a
   record, it would be removed from the Cache.
   If local DNS servers are specified,  it is assumed  that the global ones are
   not accessible (e.g. outside a firewall), so inly the local ones are used in
   that case:
*)
value rec find_dns_servers: dns_agent -> string -> ns_recs =
fun agent dom  ->
    match agent.dns_servers with
    [ [] ->
        (* No local servers, try the global ones: *)

        match dns_cache_lookup agent T_NS dom with
        [
            (T_NS, ns_doms) when ns_doms <> [] ->
                (* Yes, we got NS records from the Cache for this domain, but
                   in the response,  we don't need to re-construct the actual
                   NS recs:
                *)
                List.map (fun nsd -> (None, nsd)) ns_doms

        |   _ ->
                (* No NS records in the Cache -- try to chop off the leading
                   component of the "dom", and look for a servers for a more
                   general domain first -- THIS IS THE IDEA OF THE ITERATIVE
                   DNS RESOLUTION ALGORITHM.
                   NB: we do NOT accept CNAME records instead of NS ones!
                *)
                try
                    let dot_pos = String.index  dom  '.'            in
                    let gen_dom = String.sub    dom  (dot_pos + 1)
                                 (String.length dom - dot_pos - 1)  in

                    find_dns_servers agent  gen_dom
                with
                [
                    Not_found ->
                    (* No '.' in "dom"--we can only use the Root Servers: *)
                    root_dns_servers
                ]
        ]

      | _->
        (* Yes, there are local DNS servers to be used. For them (but NOT for
           global servers,  which always have some pre-defined order of prio-
           rity), we perform load balancing:
        *)
        Misc_utils.random_permutation agent.dns_servers
    ];


(*-------------------------*)
(*  "extract_dns_servers": *)
(*-------------------------*)
(* Extracts names of DNS servers to which we are re-directed, from "auth_srvs"
   section of a DNS response:
*)
value extract_dns_servers: string -> dns_resp_msg -> ns_recs =
fun domain  resp ->
    List.fold_left
    (fun curr_nsrs rr ->
        (* It must indeed be an NS-record, for a domain which is a "parent" of
           our given "domain":
        *)
        if  (ExtString.String.ends_with domain rr.r_domain)
        then
            match rr.data with
            [   R_NS ns_serv->
                (* The order is important: append the new item at the end:  *)
                curr_nsrs @ [(Some rr, Dom ns_serv)]

              | _ ->
                curr_nsrs
            ]
        else
            curr_nsrs
    )
    []  resp.auth_srvs;


(*-----------------*)
(*  "mark_error":  *)
(*-----------------*)
value mark_error:  dns_agent -> exn -> unit =
fun  agent err ->
do {
    (* Increment the count of "wasted" sub-requests: *)
    agent.m_total_wasted_sub_reqs := agent.m_total_wasted_sub_reqs + 1;

    (* Update the errors distribution: *)
    let err_key = match err with
    [
        DNS_Error _       -> "DNS_Server_Error"
      | Recursion_Limit   -> "Recursion_Limit"
      | No_More_Servers _ -> "No_More_Servers"
      | LowLevel_Error  _ -> "Low_Level_Error"
      | _                 -> "Other_Errors"
    ]
    in
    match Hashtbl.find_all  agent.errs_distrib err_key with
    [
        []      ->
            (* The first occurrence of this error: *)
            Hashtbl.add     agent.errs_distrib err_key 1

      | [count] ->
            (* There were already such errors *)
            Hashtbl.replace agent.errs_distrib err_key (count+1)

      | _       ->
            (* This cannot happen: *)
            assert False
    ]
};


(*-----------------------*)
(*  "wrong_dns_server":  *)
(*-----------------------*)
(* Invokes "mark_error", and possibly removes that server from the Cache: *)

value wrong_dns_server: dns_agent -> ip_or_domain -> exn -> unit
=
fun agent _ err ->
    mark_error agent err;


(*------------------------*)
(*  Exceptions Wrapping:  *)
(*------------------------*)
(*  The following functions map internal exceptions to externally-visible
    ones:
*)
value wrap_dns_exception: exn -> 'a =
fun err ->
    match err with
    [   Negative_Result _ | DNS_Error _ ->
            (* Re-raise it: *)
            raise err

      | other ->
        (* Map it to "DNS_Error": *)
        raise (DNS_Error (Misc_utils.print_exn other))
    ];


value wrap_dns_errors: string -> list exn -> 'a =
fun domain errs ->
    (* If ALL "errs" are "Negative_Result"s, make a "Negative_Result" for the
       given  "domain":
    *)
    if  List.for_all (fun [Negative_Result _ -> True | _ -> False]) errs
    then
        raise (Negative_Result domain)
    else
        raise (DNS_Error (domain^": "^Misc_utils.joint_err_msg errs));
                        

(*=========================================*)
(*  The Resolution Algorithm, High-Level:  *)
(*=========================================*)
(*  A number of mutually-recursive functions. Each of them returns an actual
    result paired with the Remaining Depth of enquiries.  The latter is used
    for preventing infinite recursion:
*)

(*-----------------------*)
(*  "dns_enquiry_iter":  *)
(*-----------------------*)
(*  Performs an iterative DNS enquiry (possibly including Cache look-ups).
    Returns a list of "dns_enquiry_res".    The subject of the enqury is 
    either a domain name or an IP address; in the latter case the result
    is a domain name, and the type must be T_PTR:
*)
value rec dns_enquiry_iter:
          dns_agent -> dns_type -> ip_or_domain -> ns_recs ->
          list (ip_or_domain * ns_recs) =

fun agent dnst subject ns_path  ->
do {
    (* Check the current recursion depth first: *)
    if  agent.rem_depth < 0
    then
        raise Recursion_Limit
    else
        agent.rem_depth := agent.rem_depth - 1;

    (* Construct the subject domain: *)
    let n_domain = match subject with
    [
        Dom dom ->
            (* This is nominally OK with any request type; however for the
               T_PTR type, "dom" would typically end with ".in-addr.arpa".
               The "dom" can be user-supplied, so normalise it:
            *)
            normalise_domain dom

      | IP  ip  ->
            (* The type MUST be T_PTR: *)
            if  dnst = T_PTR
            then
                let ip_comps = List.rev (ExtString.String.nsplit
                               (Unix.string_of_inet_addr ip) ".")  in

                (* Already normalised: *)
                (String.concat "." ip_comps)^ ".in-addr.arpa"
            else
                invalid_arg ("Dns_client.dns_enquiry_iter: "^
                             "Inverse query: the Type must be T_PTR")
    ]
    in
    (* Look-up the Cache first: *)
    match dns_cache_lookup agent dnst n_domain with
    [
        (_, []) ->
            (* The result is not in the Cache at all, so do a direct request
               to the DNS servers we can find (e.g., the Root Servers):
            *)
            let dns_servs = find_dns_servers agent n_domain  in

            try_dns_servers agent  dns_servs dnst  n_domain  ns_path  False  []

      | (got_type, cress) ->
            (* The result is available straight from the Cache,  with no up-
               dates to the "ns_path". BUT: it may be a "CNAME" rather than
               the type we are looking for, so we need to post-process  the
               result. This will allow for recursive resiolution of  CNAMEs
               under the recursion control of "dns_enquiry_iter":
            *)
            let all_ress = List.map
                (fun cres ->
                    process_dns_answer agent got_type cres dnst ns_path
                )
                cress
            in
            (* Unify the results: *)
            ExtList.List.unique (List.concat all_ress)
    ]
}

(*----------------------*)
(*  "try_dns_servers":  *)
(*----------------------*)
(*  Try to send a given request to each of the specified servers in turn,
        until a success or a definite failure occurs. The servers can be
        given by their names or IPs.   The FAULT TOLERANCE mechanisms of
        the whole resolution process are encapsulated into this function.
    NB: "refs" arg is a list of servers (as "ns_rec"s) to be used, where-
        as the second  "ns_recs"-type  arg is the accumulated resolution
        path ("ns_path"):
*)
and try_dns_servers:
    dns_agent -> ns_recs -> dns_type ->
    string    -> ns_recs -> bool     -> list string  ->
    list (ip_or_domain * ns_recs)    =

fun agent refs dnst domain ns_path aliased prev_errs ->
do {
    (*  Recursion control: also required here, as this function can be invoked
        by-passing the "dns_enquiry*":
    *)
    if  agent.rem_depth < 0
    then
        raise Recursion_Limit
    else
        ();

    (* Verify that we actually have servers available: *)
    if  refs = []
    then
        raise (No_More_Servers (String.concat ", " prev_errs))
    else
        ();

    let hd_ref      = List.hd refs  in
    let server      = snd  hd_ref   in
    let other_refs  = List.tl refs  in
    let hist        = (dnst, domain, server) in

    (* Check that the combination of (dnst, domain, server) has not been previ-
       ously encountered in this request, and that the server we are trying to
       use is not the same as the subject of A-type query (in the latter case,
       we would certainly run into cyclic history in the future):
    *)
    if  (List.mem hist agent.res_history) ||
        ((server = Dom domain) && (dnst = T_A))
    then
        (* Just skip this server -- we already tried it!  *)
        try_dns_servers agent other_refs dnst domain ns_path aliased prev_errs
    else
    do {
        (* Try the head Server. In general, we got it as a result of another
           DNS look-up, so propagate the corresp "ns_rec" object:
        *)
        agent.rem_depth   := agent.rem_depth - 1;
        agent.res_history := [hist::agent.res_history];
        try
            dns_server_enquiry agent server dnst domain [hd_ref::ns_path]
        with
        [
            (Negative_Result d) as err ->
                (* Make sure that the unresolved domain  is indeed the one
                   we've asked about (e.g. not that of the DNS server used):
                *)
                if  d = domain
                then
                    (* Yes, really, THIS domain is unresolvable: *)
                    raise err
                else
                do {
                    (* Process this error and try other servers: *)
                    wrong_dns_server agent server err;

                    try_dns_servers
                        agent other_refs dnst domain ns_path aliased
                        [(Misc_utils.print_exn err) :: prev_errs]
                }

          | Recursion_Limit as err ->
            do {
                (* Definitely no point in any re-trying.  Should we mark this
                   server as "wrong"? -- Probably not, as the error may  have
                   originated much earlier:
                *)
                mark_error agent err;
                raise err
            }

          | (LowLevel_Error _ | DNS_Error _ | No_More_Servers _)  as err ->
            do {
                (* These errors are tolerated (incl a failure to resolve the
                   name server itself, if it's given by name).  So count it,
                   and try other servers. However, if all the servers we are
                   trying are actually different IPs of the same one (so the
                   "aliased" flag is "True"), there is probably  no point in
                   trying the rest of them:
                *)
                wrong_dns_server  agent server err;

                if  (not aliased) || (is_transient err)
                then
                    try_dns_servers
                        agent other_refs dnst domain ns_path aliased
                        [(Misc_utils.print_exn err) :: prev_errs]
                else
                    raise err
            }

          | hmm ->
            do {
                (* But do not mask any other errors, such as assert failures--
                   print them, and abort the current request.   In particular,
                   "Failure"s (by "failwith") are NOT intended to be tolerated:
                *)
                print_debug  agent
                (
                    Printf.sprintf
                    "ERROR: Type=%s, Domain=%s, Server=%s: %s\n\n"
                    (string_of_dns_type dnst)
                    domain
                    (string_of_ip_or_domain server)
                    (Printexc.to_string hmm)
                );
                raise hmm
            }
        ]
    }
}

(*-------------------------*)
(*  "dns_server_enquiry":  *)
(*-------------------------*)
(*  Sends a non-recursive DNS request of the specified Type to a given Server.
    The "domain" arg is guaranteed to be already normalised. In case of succ-
    ess, this function returns a "dns_enquiry_res" object.
    It is important that this function  does not make direct recursive calls,
    but always does so through "try_dns_servers" which is responsible for his-
    tory analysis:
*)
and dns_server_enquiry:
    dns_agent -> ip_or_domain -> dns_type -> string -> ns_recs ->
    list (ip_or_domain * ns_recs) =

fun agent server dnst domain ns_path ->
do {
    print_debug agent
                (Printf.sprintf "DNS ENQUIRY: -s %s -q %s %s\n"
                (string_of_ip_or_domain   server)
                (string_of_dns_type dnst) domain);

    (* The "server" can be given by its name or by an IP addr: *)
    match   server  with
    [
    Dom _ ->
        (* We need to resolve the server name first.  In most cases, it would
           result in just a cache look-up, because the corresp A-records were
           already supplied  in the re-directing response  along with the NS-
           records.  For that reason,  we drop the "ns_paths"  accumulated in
           the following sub-request  --  most likely, there would be nothing
           new in them:
        *)
        let serv_res = dns_enquiry_iter agent T_A server ns_path in

        let all_ips  = List.fold_left
            (fun curr_ips  (ipdom, _) ->
                match ipdom with
                [   IP _ ->
                        (* Make a new "ns_rec" object, preserve  the order of
                           servers. There are actually no new NS records here,
                           only new IP addresses, hence "None" below:
                        *)
                        curr_ips @ [(None, ipdom)]
                  | _ ->
                        (* "T_A" request, but got something different from an
                           IP: error or continue? Let's continue if we can:
                        *)
                        curr_ips
                ]
            ) [] serv_res
        in
        (* Try all the IPs we have found, BUT note that they are indeed just
           different IPs of conceptually the same servers,  so some re-tries
           are not implemented in this case ("alised=True" flag).  NB: there
           are no new NS records used here, hence "None" is "all_ips" above:
        *)
        try_dns_servers  agent all_ips dnst domain ns_path True []

    | IP serv_ip ->
        (* OK, the server is already known by its IP -- can make a direct
           request:
        *)
        let qid  = Random.int 65536 (* Result in [0..65535] *)
        in
        let query_msg  =
        {
            q_id     = qid;
            opcode   = 0;       (* Standard query *)
            q_domain = domain;
            q_type   = dnst;

            (* The enquiry is recursive IFF the local DNS servers are used: *)
            rec_desr = agent.dns_servers <> []
        }
        in
        let query_dgram = encode_dns_query agent query_msg in

        (* Make request and wait for the response, possibly with re-tries: *)
        let resp_msg = dns_server_req_resps
                       agent serv_ip qid query_dgram agent.n_retries
        in
        let rhdr     = resp_msg.r_header  in
        do {
            (* Analyse the response: *)
            (* First of all, the return code: *)
            match rhdr.rcode with
            [
                0 ->
                    (* No error: *)
                    ()

              | 2 ->
                    (* Server failure: Which exception should we raise here:
                       "LowLevel_Error" or "DNS_Error"? - If the former, the
                       server will be considered re-tryable, otherwise  not.
                       We think in most cases it is NOT re-triable (XXX):
                    *)
                    raise (DNS_Error
                          "Dns_client.dns_server_enquiry: Server Failure")

              | 3 ->
                    (* No such domain: *)
                    raise (Negative_Result domain)

              | _ ->
                    (* Other kinds of errors: *)
                    raise (DNS_Error
                          ("Dns_client.dns_server_enquiry: RC=" ^
                          (string_of_int rhdr.rcode)))
            ];

            (* Now: have we really got an answer, or a re-direction to a "more
               authoritative" DNS server?
            *)
            match resp_msg.answers with
            [
            [] ->
                (* No answers, so try re-direction(s), and remember the NS recs
                   being used:
                *)
                let new_servs = extract_dns_servers domain resp_msg in
                if  new_servs = []
                then
                    (* There are no re-directs either:  signal  an error here,
                       without going into (abortive anyway) recursion:
                    *)
                    raise (DNS_Error "Empty server response")
                else
                    (* Yes, we got "more authoritative" servers to try.  They
                       are given by Domain names-- the callee will convert them
                       into IPs:
                    *)
                    try_dns_servers
                        agent new_servs dnst domain ns_path False []

            | _ ->
                (* Yes, got some answers -- process them.  If an answer is not
                   of a requested type, eg CNAME instead of A or MX,  "process_
                   dns_answer" will take further action:
                *)
                let actual_anss =
                    List.concat (List.map
                        (fun ans ->
                            process_dns_answer
                                agent
                                (get_resp_type  ans)
                                (get_resp_ipdom ans)
                                dnst
                                ns_path
                        )
                        resp_msg.answers)
                in
                    match actual_anss with
                    [
                    []  ->
                        (* None of the answers appeared to be OK: *)
                        raise (DNS_Error
                        "Dns_client.dns_server_enquiry: No valid answers")

                    | _ ->
                        (* Answers OK: *)
                        actual_anss
                    ]
            ]
        }
    ]
}

(*---------------------------*)
(*  "dns_server_req_resps":  *)
(*---------------------------*)
(* Request/response cycle for the DNS server, with re-tries. In re-tries,
   requests are sent up with the same QID as the original one; the first
   response received with the matching QID is returned:
*)
and  dns_server_req_resps:
     dns_agent -> Unix.inet_addr -> int -> string -> int -> dns_resp_msg =

fun  agent serv_ip qid query_dgram rem_retries ->

    let query_len = String.length query_dgram in
    let deadline  = (Unix.gettimeofday ()) +. agent.timeout_sec in
    do {
        (* Send the request via UDP: *)
        (* The  server port  is 53 : *)

        if (Tnio.sendto agent.sock query_dgram 0 query_len deadline
                        (Unix.ADDR_INET serv_ip 53)) = query_len
        then ()
        else
            raise (LowLevel_Error
                  ("Dns_client.dns_server_req_resps: Short send"));

        (* Wait for the response to come: *)

        try get_dns_server_resp agent qid deadline with
        [
            LowLevel_Error _ as err ->
                (* No valid response received: time-out or communication error.
                   Possibly re-try sending (using the same datagram, in parti-
                   cular, the same QID), and wait for any response to come. The
                   minimum interval between sendings should be 2.0 sec (as from
                   RFC...):
                *)
                if  rem_retries > 0
                then
                do {
                    if  agent.timeout_sec < 2.0
                    then
                        Tnio.thread_sleep (ceil (2.0 -. agent.timeout_sec))
                    else
                        ();
                    dns_server_req_resps
                        agent serv_ip qid query_dgram (rem_retries-1)
                }
                else
                (* No more attempts, propagate the error: *)
                    raise err
        ]
    }


(*-------------------------*)
(*  "process_dns_answer":  *)
(*-------------------------*)
(*  Verifies that the "ans" is of "expected_type"; if not, we may need to do
        some further resolving:
*)
and process_dns_answer:
    dns_agent -> dns_type -> ip_or_domain -> dns_type -> ns_recs ->
    list (ip_or_domain * ns_recs) =

fun agent  got_type  got_data  expected_type ns_path ->
    match (got_type, got_data) with
    [
      (any, _) when any = expected_type ->
        (* Type OK, return the data paired with the successful "ns_path": *)
        [(got_data, ns_path)]

    | (T_CNAME, Dom _) ->
        (* The types do not match.  This may still be OK  if the response is
           of the CNAME type, which we resolve further.  In this case, "got_
           data" must be a domain:
        *)
        dns_enquiry_iter agent expected_type got_data ns_path

    | _ ->
        (* All other cases: erroneous result: *)
        raise (DNS_Error
              ("Dns_client.process_dns_answer: Unexpected DNS Type: "^
              (string_of_dns_type got_type)))
    ];

(*-------------------------*)
(*  "static_dns_enquiry":  *)
(*-------------------------*)
(*  Tries to obtain static DNS info if available: *)

value static_dns_enquiry: dns_type -> ip_or_domain -> list ip_or_domain =

fun dnst subject ->
    match dnst with
    [
        T_NS | T_CNAME ->
            (* For these types, static info is probably not applicable: *)
            []

      | T_A ->
            (* Direct look-up of "static_hosts":  *)
            try
                let ip = Hashtbl.find static_hosts (get_domain subject) in
                [IP ip]
            with
                [Not_found -> []]

      | T_MX ->
            (* We assume that if a given domain occurs as a key in "static_
               hosts", then the corresp MX domain is the same:
            *)
            try
                let dom = get_domain subject in
                do {
                    ignore (Hashtbl.find static_hosts dom);
                    [Dom dom]
                }
            with
                [Not_found -> []]

      | T_PTR ->
            (* Inverse DNS look-up: *)
            let ip   = get_ip subject in
            Hashtbl.fold
               (fun cdom cip curr ->
                    if  ip = cip then [(Dom cdom)::curr] else curr
               )
               static_hosts []
    ];


(*------------------*)
(*  "dns_enquiry":  *)
(*------------------*)
(*  A user-callable wrapper around "dns_enquiry_iter". Also updates the stats,
    and uses static IP info if available:
*)
type  res_or_err =
[
    Res of list (ip_or_domain * ns_recs)
  | Err of exn
];

value dns_enquiry:
      dns_agent -> dns_type -> ip_or_domain -> dns_enquiry_res =

fun agent dnst subject ->
    let before = Unix.gettimeofday ()  in

    (* Try static info first, for suitable request types: *)
    let got    =
    try
        match static_dns_enquiry dnst subject with
        [
            [] ->
                (* No static info, need a real dynamic enquiry: *)
                do {
                    (* Replenish the "rem_depth" in the "agent", and re-set all
                       history:
                    *)
                    agent.rem_depth     := agent.max_depth;
                    agent.res_history   := [];

                    (* Now perform the iterative enquiry.      If this top-most
                       enquiry for MX records yields nothing  (No_More_Servers
                       exception), assume that the subject domain itself is MX:
                    *)
                    try (Res (dns_enquiry_iter agent dnst subject [])) with
                    [
                        No_More_Servers _ when dnst = T_MX  ->

                        let domain = get_domain subject in
                        Res [(Dom domain, [])]
                    ]
            }

          | stat_res ->
                (* Got some static info -- that's enough: *)
                Res (List.map (fun sr -> (sr, [])) stat_res)
        ]
    with
        [any_other_err ->
            (* Got an exception, but we defer it: *)
            Err any_other_err
        ]
    in
    let after  = Unix.gettimeofday ()  in
    do {
        (*  Update the statistics for this Agent: *)
        agent.m_total_enquiries     := agent.m_total_enquiries + 1;

        agent.m_total_resp_time_sec := agent.m_total_resp_time_sec
                                    +. after -. before;

        (*  Signal success or failure: *)
        match got with
        [
            Res oks ->
            do {
                agent.m_succ_enquiries := agent.m_succ_enquiries + 1;

                (* ONLY AT THIS POINT, we save the successful NS-records in
                   the Cache, and assemble the results:
                *)
                let (all_res, all_nsrs) = List.fold_left
                    (fun (curr_res, curr_nsrs) (ipdom, path)  ->
                         (curr_res @ [ipdom],  curr_nsrs@ path))
                    ([], []) oks
                in
                do {
                    List.iter
                    (fun [(Some rr, _) -> dns_cache_insert_rr agent rr
                          | _ -> ()]
                    )
                    all_nsrs;
                    homogenise_dns_data all_res dnst
                }
            }
          | Err any_err ->
                wrap_dns_exception any_err
        ]
    };


(*=============================*)
(*  Combined MX- and A-search: *)
(*=============================*)
(*  Combined MX- and A-records search for IPs of Mail Exchanges for a given
    Domain:  returns an initial bunch of  IPs, plus  a "continuation object"
    which may allow  the user to get more IPs if necessary. Thus, yhis is a
    LAZY IP-resolution for Mail Servers:
*)
type  mx_ips_cont =
{
    (* IPs already obtained: *)
    curr_ips : mutable list Unix.inet_addr;

    (* MX records not yet used, from which more IPs can be obtained: *)
    rem_mxs  : mutable list ip_or_domain;

    (* Mail Domain being processed: *)
    domain   : string
};

(*-----------------*)
(*  "get_mx_ips":  *)
(*-----------------*)
(*  Returns a list of IPs for a given MX record. Any exceptions are propagated
    to the caller. An empty result can also be returned:
*)
value get_mx_ips: dns_agent -> ip_or_domain -> list Unix.inet_addr =
fun agent mx  ->
    match mx with
    [   Dom _ ->
        (* Request A-records for the "mx" domain: *)
        match  dns_enquiry  agent T_A mx with
        [
            IPs ips -> ips
          | _       -> failwith "Dns_client.get_mx_ips: Must be IPs"
        ]
    | _ -> failwith "Dns_client.get_mx_ips: Must be Dom"
    ];


(*--------------------*)
(*  "get_mx_ips_ne":  *)
(*--------------------*)
(*  Given a list of MX domains, returns a non-empty list of IPs and a
    Continuation Object, or raises an exception:
*)
value rec get_mx_ips_ne:
      dns_agent -> string -> list ip_or_domain -> list exn ->
      ((list Unix.inet_addr) * mx_ips_cont)      =

fun agent domain mxs prev_errs ->
    match mxs with
    [   [] ->
            (* No valid result -- process the errors: *)
            wrap_dns_errors domain prev_errs

      | [hmx::tmxs] ->
            let (these_ips, these_errs) =
            try
                (* If "get_mx_ips" succeed, no errors were found: *)
                (get_mx_ips agent hmx, [])
            with
            [ hmm ->
                (* Error in "get_mx_ips", so no IPs found: *)
                ([], [hmm])
            ]
            in
            (* So: Have we got any IPs? *)

            match these_ips with
            [   []  ->
                (*  The first MX record yielded nothing, with or without an
                    exception. Try more:
                *)
                get_mx_ips_ne agent domain tmxs (prev_errs @ these_errs)

              | ips ->
                (* Got a non-empty list of IPs, then obviously there was no
                   exception. Return the IPs and the Continuation Object:
                *)
                let cont_obj =
                {
                    curr_ips = ips;
                    rem_mxs  = tmxs;
                    domain   = domain
                }
                in
                (ips, cont_obj)
            ]
    ];

(*------------------*)
(*  "init_mx_ips":  *)
(*------------------*)
(* Get the first bunch of IPs and the Continuation Object: *)

value init_mx_ips:
      dns_agent -> string -> ((list Unix.inet_addr) * mx_ips_cont) =
fun agent  dom  ->
try
    let mx_doms = dns_enquiry agent T_MX (Dom dom)   in

    get_mx_ips_ne agent dom (label_dns_data mx_doms) []
with
[   any_err ->
    wrap_dns_exception any_err
];


(*------------------*)
(*  "more_mx_ips":  *)
(*------------------*)
(* Tries to get more IPs from a Continuation Object. An empty result would
   indicate that no more IPs can be obtained, which is normal.  Errors are
   propagated to the caller. The Continuation Object is modified in-place:
*)
value rec more_mx_ips':
    dns_agent -> mx_ips_cont -> list exn -> list Unix.inet_addr =

fun agent cont_obj prev_errs ->

    match cont_obj.rem_mxs with
    [   [] ->
            (* Nothing more to try. What we return, depends on the errors
               encountered:
            *)
            match prev_errs with
            [   [] ->
                    (* No errors, just empty response: *)
                    []
              | _  ->
                    (* Raise appropriate exception:    *)
                    wrap_dns_errors (cont_obj.domain) prev_errs
            ]

      | [hmx::tmxs]->
        do {
            (* Whatever result "hmx" would yield, remove it from the "rem_mxs"
               list:
            *)
            cont_obj.rem_mxs := tmxs;

            (* Get IPs for the "hmx", retaining errors: *)
            let (these_ips, these_errs)  =
            try
                (* If "get_mx_ips" succeeds:  no erros: *)
                (get_mx_ips agent hmx, [])
            with
            [   hmm ->
                (* Exception: no result: *)
                ([], [hmm])
            ]
            in
            (* Check whether there are any new IPs there: *)
            let new_ips   = Misc_utils.list_minus these_ips cont_obj.curr_ips
            in
            match new_ips with
            [   [] ->
                    (* No new IPs here, remember any errors and go further: *)
                    more_mx_ips' agent cont_obj (prev_errs @ these_errs)

              | ok ->
                do {
                    (* Yes, got new IPs:  remember them in the Continuation
                       Object,  and return them. Any "prev_errors" will now
                       be ignored:
                    *)
                    cont_obj.curr_ips := cont_obj.curr_ips @ new_ips;
                    new_ips
                }
            ]
        }
    ];

(*  Now the externally-visible "more_mx_ips": *)
value more_mx_ips: dns_agent -> mx_ips_cont -> list Unix.inet_addr =
fun   agent        cont_obj  ->
    more_mx_ips'   agent cont_obj [];


(*-----------------*)
(*  "all_mx_ips":  *)
(*-----------------*)
(*  In contrast to "init_mx_ips" / "more_mx_ips", this function returns ALL IPs
    of Mail Exchanges for a given domain at once:
*)
value all_mx_ips: dns_agent -> string -> list Unix.inet_addr =
fun agent domain ->
    let  mx_doms =
    try
        label_dns_data (dns_enquiry agent T_MX (Dom domain))
    with
        (* Signal possible errors: *)
        [hmm ->  wrap_dns_exception hmm]

    in
    let (mx_ips, errs) =
        List.fold_left
        (fun (curr_ips,  curr_errs) mxd ->
            let  (these_ips, these_errs) =
            try
                (get_mx_ips agent  mxd, [])
            with
                [ hmm -> ([], [hmm])]
            in
            let new_ips = Misc_utils.list_minus these_ips curr_ips
            in
            (curr_ips @ new_ips, curr_errs @ these_errs)
        )
        ([], [])  mx_doms
    in
    (* Now analyse the result: *)
    let ips' =
        match mx_ips with
        [   [] ->
                (* No result: exception or just empty list? *)
                match errs with
                [ []  ->
                        (* Really empty response (STRANGE!) *)
                        raise (Negative_Result domain)
                  | _ ->
                        (* Yes, got errors -- signal them:  *)
                        wrap_dns_errors domain errs
                ]

          | _  ->
                (* Non-empty result: exceptions, if any, will now be ignored;
                   no re-trying necessary:
                *)
                Misc_utils.random_permutation mx_ips
        ]
    in
    (* If we get here, there must be a non-[] list of IPs: *)
    do {
        assert (ips' <> []);
        ips'
    };

(*====================*)
(*  "get_dns_stats":  *)
(*====================*)
value get_dns_stats: dns_agent -> dns_stats =
fun agent ->
    {
        total_enquiries     = agent.m_total_enquiries;
        succ_enquiries      = agent.m_succ_enquiries;

        av_resp_time_sec    =
            if   agent.m_total_enquiries = 0
            then 0.0
            else agent.m_total_resp_time_sec /.
                 (float_of_int agent.m_total_enquiries);

        av_wasted_sub_reqs  =
            if   agent.m_total_enquiries = 0
            then 0.0
            else (float_of_int agent.m_total_wasted_sub_reqs) /.
                 (float_of_int agent.m_total_enquiries);

        errors              =  agent.errs_distrib;
        cache_stats         =  Cache_simple.get_stats agent.cache
    };

