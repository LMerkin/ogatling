(*pp camlp4r *)
(* vim:ts=4:syntax=ocaml
*)
(*===========================================================================*)
(*                             "misc_utils.mli":                             *)
(*                                                                           *)
(*            Interface to the Miscellaneous Utility Functions               *)
(*                        for the "Gatling" Project                          *)
(*                                                                           *)
(*                (C) Explar Technologies Ltd, 2003--2004                    *)
(*===========================================================================*)
(*===================*)
(*  List functions:  *)
(*===================*)
(* "random_permutation":
   Permutes any list randomly (NB: not tail-recursive):
*)
value random_permutation: list 'a -> list 'a;

(* "random_insert":
   Inserts a given new element at a random position into the list.
   NB: this function is NOT tail-recursive:
*)
value random_insert     : 'a -> list 'a -> list 'a;

(* "random_element":
   Returns a random element of a list (which must be non-[]):
*)
value random_element    : list 'a -> 'a;

(* "insert_at":
   Inserts a given new element at the Nth position (N = 0 .. ListLength)
   into the list;  "N=0" means inserting before the current head of the
   list; "N=ListLength" -- after the last element.
   NB: this function is NOT tail-recursive:
*)
value insert_at         : 'a -> list 'a -> int -> list 'a;

(* "replicate_list":
   makes N (N >= 0) copies of a given list and concatentes them together:
*)
value replicate_list    : list 'a -> int -> list 'a;

(*  "list_minus":
    Removes all occurrencies of elements of "list2" from "list1". NB: this
    function is NOT tail-recursive:
*)
value list_minus: list 'a -> list 'a -> list 'a;

(*  "int_range":
    Produces a list of integers [from .. to] inclusive:
*)
value int_range:  int -> int -> list int;

(*  "list_find":
    Like the standard "List.find", but returns None rather than raises an
    exception if the required element is not found:
*)
value list_find: ('a -> bool) -> list 'a -> option 'a;


(*=====================*)
(*  Array functions:   *)
(*=====================*)
(*  "array_find":
    Returns the index of the first array element which satisfies the given
    predicat (NB: the predicate also involves the index), or raises "Not_found":
*)
value array_find: (int -> 'a -> bool) -> array 'a -> int;

(*  "array_mem":    *)
value array_mem :   'a -> array 'a -> bool;

(*  "random_entry": *)
value random_entry: array 'a -> 'a;

(*  "foldi_left"  :
    Like "Array.fold_left", but also passes the curr index to the call-back:
*)
value foldi_left  : ('a -> int -> 'b -> 'a) -> 'a -> array 'b -> 'a;

(*======================*)
(*  Hashtbl functions:  *)
(*======================*)
(*  "hashtbl_to_list":  *)
value hashtbl_to_list: Hashtbl.t 'k 'v -> list (('k * 'v));

(* "hashtbl_keys":  *)
value hashtbl_keys   : Hashtbl.t 'k 'v -> list 'k;

(* "hashtbl_vals":  *)
value hashtbl_vals   : Hashtbl.t 'k 'v -> list 'v;


(*=====================*)
(*  String functions:  *)
(*=====================*)
(*  "contains_at s1 p1 s2":
    Checks whether "s2" is a sub-string of "s1" occurring at position "p":
*)
value contains_at  : string -> int -> string -> bool;

(*  "contains_from s1 p1 s2":
    Searches for occurrences of "s2" in "s1", starting from position "p1".
    Returns the smallest index of such an occurrence if found, (-1) other-
    wise (no "Not_found" exceptuon!)
*)
value contains_from: string -> int -> string -> int;

(*  "split_by_crlfs":
    Splits a string into text lines at "\r\n" boundaries. The terminators
    themselves are removed from the lines produced.    The resulting list
    always contains at last one line (maybe ""):
*)
value split_by_crlfs : string -> list string;

(*  "split_by_nls":
    Splits a string into text lines at any new-line boundaries   ("\r\n",
    "\n", "\r"). The new-lines themselves are removed from the  results.
    The resulting list is always non-empty (at least [""]):
*)
value split_by_nls   : string -> list string;

(*  "split_by_whites":
    Splits a string into chunks at white-space boundaries ("\r\n\t "). The
    white-space chars themselves are completely removed from the chunks:
*)
value split_by_whites: string -> list string;

(*  "strip_eol":
    Removes a "\r\n", "\n" or "\r" terminator (if any) from the string:
*)
value strip_eol: string -> string;

(*  "pcre_of_glob":
    Converts a "glob-" (a.k.a. "shell-") type regular expression into the
    PCRE-type syntax:
*)
value pcre_of_glob:   string -> string;

(*  "uniq_file_name":
    Creates a uniqie file name in a given directory (arg 1), and with the
    given prefix (arg 2) and suffix (arg 3).
    XXX:  This function  is NOT thread-safe!
*)
value uniq_file_name: string -> string -> string -> string;

(*  "random_string":
    Produces a random string from the specified alphabet, of the given length:
*)
value random_string:  string -> int -> string;

(*===================*)
(*  E-Mail Parsing:  *)
(*===================*)
(*  "parse_email_addr":
    Verifies the e-mail address fot syntactic correctness, and returms a triple
    (Name, MailBox,  Domain).    "MailBox" and "Domain" (but NOT "Name"!!!) are
    normalised -- converted into lower-case.
*)
value parse_email_addr  : string -> (string * string * string);

(*  "get_email_domain":
    A simpler form of "parse_email_addr". The domain returned is converted into
    lower-case (even if the e-mail is assumed to be in lower-case anyway):
*)
value get_email_domain  : string -> string;

(*  "extract_email_addr":
    Searches the given string for the first (if any) occurrence of an e-mail
    addr, and returns it in the normalised (lower-case) form;     otherwise,
    returns None:
*)
value extract_email_addr: string -> option string;

(*========*)
(*  I/O:  *)
(*========*)
(*  "line_reader" class:
    Parametrised by the socket type ('s),  the low-level byte-reading function
    (with the type compatible with that of Tnio.read), and the internal buffer
    size. It can be instantiated to work with "normal" Tnio sockets, SSL, etc:
*)
type io_func 's = 's -> string -> int -> int -> float -> int;
                (* sock   buff   from max_need deadline  got *)

class line_reader ['s]: [io_func 's] -> [int]->
object
    (* "recv_line": 
        Args: socket, abs_deadline. Returns the line read, without EOL:
    *)
    method recv_line: 's -> float-> string;

    (* "reset":
       Clears all the pending input data from the reader:
    *)
    method reset    : unit -> unit;

    (* "leftover_data":
       Return some pending data accumulated in the reader (if any) as a single
       chunk, but no more than the size given by the Arg. These data are remo-
       ved from the reader; if any more data are pending, they are kept in:
    *)
    method leftover_data: int -> string;
end;

(*  "open_write_locked":
    Opens a file in the Append-Only or Read/Write mode   (depending on whether
    the "~append" flag is set), and tries to lock the file for exclusive use,
    If locking is successful  and the mode is Read/Write  and the file already
    exists, it is truncated to 0. If locking is unsuccessful,   the process is
    aborted (with a particular exit code if given)  and the current content of
    the file is presereved. If the file name is "", None is returned:
*)
value open_write_locked:
    ~append_mode:bool -> ?lock_fail_rc:int -> string -> option Unix.file_descr;

(*  "strict_io":
    Performs the required I/O operation (e.g. "read"/"write") on a socket
    until all the bytes are transferred, or an error occurs.   Errors are
    caught, and the number of bytes actually transferred  returned in all
    cases:
*)
value strict_io:
    io_func 's -> 's -> string -> int -> int -> float -> int;

(*  "send_str":
    Sends the whole given string via a socket. This is just a wrapper around
    "strict_io". If the whole string cannot be sent due to an error, the lat-
    ter is propagated as an exception:
*)
value send_str: io_func 's -> 's -> string -> float -> unit;

(*  "read_file":
    Reads the whole file into a string:
*)
value read_file:  string -> string;

(*  "read_lines":
    Reads a text file into a list of lines; EOLs are removed:
*)
value read_lines: string -> list string;

(*  "write_fstr":
    Writes a given text string into a file. NOT suitable for sockets, as it
    does NOT handle "short writes":
*)
value write_fstr : Unix.file_descr -> string -> unit;

(*  "save_close":
    Closes a "file_descr", disregarding any exceptions:
*)
value safe_close : Unix.file_descr -> unit;

(*  "ip4":
    Converts a "Unix.inet_addr" into a numeric quadruple:
*)
type  ip4 = (int * int * int * int);

value ip4_of_inet_addr:  Unix.inet_addr -> ip4;
value inet_addr_of_ip4:  ip4 -> Unix.inet_addr;


(*=================*)
(*  Users/Groups:  *)
(*=================*)
(*  "run_as":
    Arg1: symbolic or numeric user  ID ("": don't change!)
    Arg2: symbolic or numeric group ID ("": don't change!)
    The caller must have the required privileges to set effective UID/GID to
    the values specified:
*)
value run_as: string -> string -> unit;


(*===============*)
(*  Exceptions:  *)
(*===============*)
(*  "print_exn":
    Text msg from an exception, including detailed "Unix.error" diagnostics:
*)
value print_exn:   exn -> string;

(*  "joint_err_msgs":
    Makes a joint msg string from a list of exceptions:
*)
value joint_err_msg: list exn -> string;

(*  "fatal_error":
    Prints an error msg on "stderr" and terminates the program. This function
    never returns; nominally it returns "'a",  so it can be used in any expr:
*)
value fatal_error  : ?rc:int -> string -> 'a;

(*  "lock_or_abort":
    Locks the file given by its name and FD,  or executes "fatal_error" (with
    the given return code if specified) if the lock is not immediately avail-
    able:
*)
value lock_or_abort: ?rc:int -> string -> Unix.file_descr -> unit;

