(*pp camlp4r *)
(* vim:ts=4:syntax=ocaml
*)
(*===========================================================================*)
(*                              "http_server.mli":                           *)
(*                      A Simple HTTP(S) Server Interface                    *)
(*                      (C) Explar Technologies Ltd, 2004                    *)
(*===========================================================================*)
(*===============*)
(*  Data Types:  *)
(*===============*)
(*---------------*)
(*  "http_req":  *)
(*---------------*)
(*  Client request info:  *)
type http_req =
{
    (* TCP/IP parms: *)
    client_ip   :   Unix.inet_addr;
    client_port :   int;

    (* Parsed request line:
       METHOD Path;Parameters?Query#Fragment
    *)
    http_version:   string;  (* "1.0" or "1.1"  *)
    http_method :   string;  (* "GET" or "POST" *)
    path        :   string;
    parameters  :   string;  (* Usually "" *)
    fragment    :   string;  (* Usually "" *)

    (* Query parms are combined from the request line and (for "POST") from
       the request body, and stored in a dictionary Name => Value. The URL+
       decoding is applied automatically to the values. NB: multiple Values
       are possible for the same Name, hence the list below:
    *)
    query_parms :   Hashtbl.t string (list string);

    (* HTTP headers are also stored as a dictionary.   Header names are NOT
       case-sensitive (but the values are),    so they are always converted
       into the LOWER-CASE for easy access. Like "query_parms", this map is
       also list-valued:
    *)
    headers     :   Hashtbl.t string (list string);

    (* File uploading is also supported; uploaded files are stored in a tem-
       porary area on the server side. The following map is provided:
       Tmp_Server_FileName => (Orig_FileName, Orig_FileType):
    *)
    uploaded_files: Hashtbl.t string (string * string);

    (* Is the current connection to be kept by both client and server? *)
    keep_alive    : bool
};
                
(*--------------------*)
(*  "server_config":  *)
(*--------------------*)
(*  Similar to the "Tcp_server.server_config", except the "proc_func": *)

type server_config 'socket 'state =
{
    (* TCP Parameters: *)
    listen_on_ip        : Unix.inet_addr;
    listen_on_port      : int;
    listen_queue_len    : int;
    max_connections     : int;
    sock_buff_size      : int;

    (* Acceptor Parameters: *)
    acceptor_priority   : int;
    acceptor_stack_k    : int;

    (* Worker Pool Parameters: *)
    max_idle_threads    : int;
    min_idle_threads    : int;
    init_threads        : int;
    worker_priority     : int;
    worker_stack_k      : int;

    (* State Management: *)
    init_socket         : Tnio.tsock       -> 'state -> float  -> 'socket;
    finalise_socket     : 'socket -> float -> unit;
    init_worker_state   : unit    -> 'state;
    cleanup_state       : 'state  -> unit;
    reset_state_on_error: bool;
    log_error           : exn     -> unit;
    reject_conn         : Tnio.tsock -> unit;

    (* I/O functions for the "'socket" type, with deadlines: *)
    read_func           : Misc_utils.io_func 'socket;
    write_func          : Misc_utils.io_func 'socket;

    (* User-Level Processing:
       Request URL Paths (as in "http_req" above) are mapped to processing
       functions. NB: these functions receive DEADLINES (not timeouts) for
       each request:
    *)
    proc_funcs          : Hashtbl.t string
                          ('socket -> http_req -> 'state -> float -> 'state);

    (* Time-out for a single client connection -- NOT for "proc_funcs" invo-
       cations,   as there may be multiple requests (up to "max_pers_reqs")
       serviced over a single connection (session):
    *)
    session_timeout     : float;
    max_pers_reqs       : int;

    (* The following is the time-out for each individual handler invocation
       (of "proc_funcs") -- it should normally be no longer that  "session_
       timeout / max_pers_reqs".  Furthermore, we may allow different time-
       outs for different kinds of requests,   so this is a function pf the
       request:
    *)
    proc_timeout        : http_req -> float;

    (* User handler for HTTP errors: Args: Socket, ErrorCode, Msg: *)
    http_error          : 'socket  -> int -> string -> unit;

    (* Temporary directory for uploaded files: *)
    uploaded_dir        : string;

    (* User and Group for running this server: *)
    run_as_user         : string;
    run_as_group        : string
};

(*------------------*)
(*  "server_t":     *)
(*  "server_info":  *)
(*------------------*)
(*  They are currently directly re-used from the "Tcp_server" *)

(*======================*)
(*  Server Management:  *)
(*======================*)
(*  Only "create_server" is specific to this module; "get_server_info"
    and  "shutdown_server"  can be directly re-used from "Tcp_server":
*)
value create_server  : server_config 'socket 'state  ->
            Tcp_server.server_t      'socket 'state;

(*-------*)
(* Misc: *)
(*-------*)
(*  "display_page":
    Sends a page (Arg1) to the client. The "full_response" (Arg2) is by default
    False, and should be set to True only if this function is invoked from OUT-
    SIDE of a normal HTTP call-back handler ("proc_func" as above): if a normal
    handler is used, the server automatically sends the 1st line and the stand-
    ard headers of the response. "Content_type" (Arg3) defaults to "text/html":
*)
value display_page:
    string -> ?full_response:bool -> ?content_type:string ->
    Misc_utils.io_func  'socket   -> 'socket  ->   float  -> unit;

(*  "simple_err_handler":
    sends an error response line and empty headers to the user:
*)
value simple_err_handler:
    Misc_utils.io_func 'socket -> 'socket -> int -> unit;

