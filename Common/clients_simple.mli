(*pp camlp4r *)
(* vim:ts=4:syntax=ocaml
*)
(*===========================================================================*)
(*                          "clients_simple.mli":                            *)
(*                                                                           *)
(*         Easy-To-Use, Less-Configurable DNS, HTTP(S) Etc Clients           *)
(*                (built on top of fully-generic clients)                    *)
(*                                                                           *)
(*                   (C) Explar Technologies Ltd, 2004                       *)
(*===========================================================================*)
(*========*)
(*  DNS:  *)
(*========*)
(*  "mk_dns_agent":
    DNS AGents Factory.  Creates a new DNS Agent with some reasonable default
    configuration. The arg is a list of local DNS servers  -- should be empty
    (to make a stand-alone resolver) unless the local firewall settings don't
    pass the UDP traffic through:
*)
value mk_dns_agent: list string -> Dns_client.dns_agent;

(*  "dns_resolver": A DNS resolver for A-records, for any DNS agent (e,g. for
    one created by the above Factory). Returns a non-empty list of results in
    on success, or propagates an exception. Furthermore, if the arg is a text-
    ual form of an IP, that IP is returned in a singleton list
*)
value dns_resolver:
    Dns_client.dns_agent -> string -> list Unix.inet_addr;

(*============*)
(*  HTTP(S):  *)
(*============*)
(*  Default HTTP transport:  *)
value http_transport:  Http_client.transport Tnio.tsock;

(*  Default HTTPS transport: *)
value https_transport: Http_client.transport Ssl.socket;

(*  Misc: HTTP client settings: *)
value invalid_url:    string;
value http_buff_size: int;

(*  "mk_http_agent":
    A Factory creating combined HTTP/HTTPS Agents:
*)
type http_agent    =  Http_client.http_agent Tnio.tsock Ssl.socket;

value mk_http_agent:  Dns_client.dns_agent -> http_agent;

(*  "http_get":
    A simple HTTP/1.1 "get" method, without cookies, extra headers, etc.
    The args: URL and deadline:
*)
value http_get:
    http_agent -> string -> float -> Http_client.http_res;

