(*pp camlp4r *)
(* vim:ts=4:syntax=ocaml
*)
(*===========================================================================*)
(*                              "http_common.mli":                           *)
(*     Interface to Some Common Functions of HTTP Clients and Servers        *)
(*                      (C) Explar Technologies Ltd, 2004                    *)
(*===========================================================================*)

(* This exception  is actually  raised on the server side only
   (and then communicated to the client via the HTTP response):
*)
exception HTTP_Error of int and string;    (* HTTP_RC, Msg *)

type http_side =
    [ Server_Side               (* No extra data necessary *)
    | Client_Side of string     (* Server we are currently connected to *)
    ];

(* HTTP Cookie: *)
type http_cookie =
{
    cookie_name:    string;
    cookie_value:   string;
    cookie_domain:  string;
    cookie_path:    string;
    cookie_expires: float
};

(*  "http_err_msgs":
    Returns a dictionary of HTTP messages indexed by the HTTP response codes:
*)
value http_err_msgs : unit -> Hashtbl.t int string;

(*  "dict_install":
    Installs the "name=value" pair in the given dictionary, re-combining the
    "value" with the existing content for the "name" if applicable:
*)
value dict_install  : Hashtbl.t 'a (list 'b) -> 'a -> 'b -> unit;

(*  "parse_url":
    Returns (protocol, host, port, path_etc).
    If the URL is local (in particular, relative), e.g. does not start with a
    "protocol", the function returns ("", "", 0, url). Otherwise full parsing
    is done; "port" defaults to 80 for "http" and 443 for "https"  (any other
    "protocol"s are not allowed); "path_etc" defaults to "/":
*)
value parse_url     : string -> (string * string * int    * string);

(*  "parse_urlpath":
    Parses the "patch_etc" component of a URL,
    returns  (path, parms, query, fragment):
*)
value parse_urlpath : string -> (string * string * string * string);

(*  "parse_query":
    Fills in the "query_dict" (Arg2) by (name, value) pairs extracted and
    decoded from the "query" string (Arg1):
*)
value parse_query   : string -> Hashtbl.t string (list string) -> unit;

(*  "mk_url":
    Inverse of "parse_url": proto host port path_etc -> url:
*)
value mk_url        : string -> string -> int -> string -> string;

(*  "join_urls":
    Returns URL2 if it is absolute; otherwise, treats URL1 as the base, URL2
    as the relative extension, and returns the resulting absolute URL:
*)
value join_urls     : string -> string -> string;

(*  "mk_urlpath":
    Inverse of "parse_urlpath": path parms query fragment -> path_etc:
*)
value mk_urlpath    : string -> string -> string -> string -> string;

(*  "mk_query":
    Inverse of "parse_query": query_dict -> query_str:
*)
value mk_query      : Hashtbl.t string (list string) -> string;

(*  "get_single_val":
    Returns the value of the specified parm from the given dictionary  (e.g.
    one constructed by "read_headers" above), provided that there is exactly
    one value for that name.   For error reporting, "name" is constrained to
    the "string" type here:
*)
value get_single_val: Hashtbl.t string (list 'a) -> string -> 'a;

(*  "read_headers":
    Reads the HTTP headers and puts them into the given dictionary. Multiple
    header values can (in general) be stored under the same name in the dict:
*)
value read_headers:
    Misc_utils.line_reader 'socket -> 'socket -> float ->
    Hashtbl.t string (list string);

(*  "get_cookies":
    Extracts the cookies from the headers set. Different  header names are
    used depending on whether this function is invoked on the client or on
    the server side.   They are distinguished by the value of the 1st arg;
*)
value get_cookies:
    http_side -> Hashtbl.t string (list string) -> list http_cookie;

(*  "read_http_data":
    Read all the data following the headers  (already terminated by an empty
    line), until either the EOF is encountered, or the number of bytes given
    by the "content-length" header is reached.
    NB: this function does NOT close the socket when an error occurs -- this
    is the responsibility of the caller!
*)
type after_read =  [EOF | Still_On];

value read_http_data:
    http_side                       ->  (* Where the caller is     *)
    Misc_utils.io_func 'socket      ->  (* The "read" function     *)
    Misc_utils.line_reader 'socket  ->  (* Reader used for headers *)
    'socket                         ->  (* The socket   *)
    string                          ->  (* The input buffer *)
    Hashtbl.t string (list string)  ->  (* The headers  *)
    float                           ->  (* The deadline *)
    (string * after_read);              (* (Data, After_Status)    *)

(* "log_http_exn":
   Arg1: loggin function; Arg2: exception. Trivial exception such as client
   disconnections are NOT logged:
*)
value log_http_exn: (string -> unit) -> exn -> unit;

