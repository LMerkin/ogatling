(*pp camlp4r *)
(* vim:ts=4:syntax=ocaml
*)
(*===========================================================================*)
(*                           "server_https_test.ml":                         *)
(*          "Tcp_server" test, by creating a simple HTTPS server             *)
(*                  (C) Explar Technologies Ltd, 2004--2006                  *)
(*===========================================================================*)
Tnio.init (Tnio.Priority_Bands 2);

type state =
{
    reader : Misc_utils.line_reader Tnio.tsock
};

value finalise_socket: Tnio.tsock -> float -> unit =
fun ts _ ->
    Tnio.close ts;

value rec read_lines:
    Misc_utils.line_reader Tnio.tsock -> Tnio.tsock -> float -> unit =

fun reader sock deadline ->
    let line = reader#recv_line sock deadline in
    if  line = ""
    then ()
    else read_lines reader sock deadline;

value output_string: Tnio.tsock -> string -> float -> unit =
fun ts str deadline ->
    Misc_utils.send_str Tnio.send ts str deadline;

value page =
    "<html><head><title>Fig Vam!</title></head>\r\n" ^
    "<body bgcolor=\"white\">\r\n" ^
    "<h1>FIG VAM!</h1></body></html>\r\n";

value http_proc: Tnio.tsock -> Unix.sockaddr ->
                 state -> float -> state =
fun ts _ state deadline ->
do {
    (* Read the data until en empty line is received: *)
    read_lines  state.reader ts deadline;

    output_string ts "HTTP/1.1 200 OK\r\n"          deadline;
    output_string ts "Content-type: text/html\r\n"  deadline;
    output_string ts ("Content-length: "^
                      (string_of_int (String.length page))^
                      "\r\n")                       deadline;
    output_string ts "Connnection: close\r\n"       deadline;
    output_string ts "\r\n"                         deadline;
    output_string ts page                           deadline;

    (* Return the state: *)
    state
};

value config =
{
    Tcp_server.listen_on_ip     = Unix.inet_addr_any;
    Tcp_server.listen_on_port   = 2005;
    Tcp_server.listen_queue_len = 1280;
    Tcp_server.max_connections  = 1000;
    Tcp_server.sock_buff_size   = 256*1024;

    Tcp_server.acceptor_priority= 1;
    Tcp_server.acceptor_stack_k = 128;

    Tcp_server.max_idle_threads = 100;
    Tcp_server.min_idle_threads =  10;
    Tcp_server.init_threads     =  20;
    Tcp_server.worker_priority  =   0;
    Tcp_server.worker_stack_k   = 256;

    Tcp_server.init_socket      = fun ts _ _ -> ts;
    Tcp_server.finalise_socket  = finalise_socket;
    Tcp_server.proc_func        = http_proc;
    Tcp_server.proc_timeout     = 300.0;
    Tcp_server.init_worker_state=
        fun () -> {reader = new Misc_utils.line_reader Tnio.recv 1024};

    Tcp_server.cleanup_state    = fun state -> state.reader#reset ();
    Tcp_server.reset_state_on_error = False;

    Tcp_server.after_acceptor_bind =
                                  fun _  -> ();
    Tcp_server.log_error        = fun _  -> ();
    Tcp_server.reject_conn      = fun _  -> ()
};

value server = Tcp_server.create_server config;

prerr_endline "Server Created!";
flush stderr;

Tnio.threads_scheduler ();

