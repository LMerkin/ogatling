(*pp camlp4r *)
(* vim:ts=4:syntax=ocaml
*)
(*===========================================================================*)
(*                            "misc_utils.ml":                               *)
(*        Miscellaneous Utility Functions for the "Gatling" Project          *)
(*               (C) Explar Technologies Ltd, 2003--2004                     *)
(*===========================================================================*)
(*===================*)
(*  List functions:  *)
(*===================*)
(*------------------------------*)
(*  "random_permutation", etc:  *)
(*------------------------------*)
value rec insert_at: 'a -> list 'a -> int -> list 'a =
fun x xs p ->
    (* "p": insert position, in [0 .. Len]; "0" => before the head,
       "Len" => after the last element:
    *)
    if  p = 0
    then
        [x::xs]
    else
        match xs with
        [
            [] ->
                (* Error: non-0 insertion pos, but empty list:  *)
                failwith "Misc_utils.insert_at: Empty list"

          | [h::ts] ->
            [h:: (insert_at x ts (p-1))]
        ];

value random_insert: 'a -> list 'a -> list 'a =
fun x xs ->
    let
        p = Random.int ((List.length xs)+1)  (* "p" in [0..Len] *)
    in
        insert_at x xs p;

value rec random_permutation: list 'a -> list 'a =
fun
[ []      -> []
| [h::ts] -> random_insert h (random_permutation ts)
];

value random_element: list 'a -> 'a =
fun
[ [] ->
    failwith "Misc_utils.random_element: Empty list"

| xs ->
    (* Unfortunately, we need to know the list length before a random element
       can be selected:
    *)
    List.nth xs (Random.int (List.length xs))
];

(*---------------------*)
(*  "replicate_list":  *)
(*---------------------*)
(* Constructs a list which is "n" copies of the original list concatenated
   together:
*)
value rec replicate_list: list 'a -> int -> list 'a =
fun lst n ->
    if  n <= 0
    then  []
    else  lst @ (replicate_list lst (n-1))
;
(*-----------------*)
(*  "list_minus":  *)
(*-----------------*)
(*  Removes all occurrencies of elements of "list2" from "list1". NB: this
    function is NOT tail-recursive:
*)
value rec list_minus: list 'a -> list 'a -> list 'a =
fun list1 list2->
    match list1 with
    [
        []     -> []
      | [h::ts]->
            if  List.mem h list2
            then
                (* Drop "h": *)
                list_minus ts list2
            else
                (* Keep "h", scan the tail: *)
                [h :: (list_minus ts list2)]
    ];

(*----------------*)
(*  "int_range":  *)
(*----------------*)
value int_range: int -> int -> list int =
fun ifrom ito ->
    let res = ref []    in
    let i   = ref ifrom in
    let ()  =
    while i.val <= ito
    do {
        res.val:= res.val @ [i.val];
        i.val  := i.val + 1
    }
    in
    res.val;

(*----------------*)
(*  "list_find":  *)
(*----------------*)
value rec list_find: ('a -> bool) -> list 'a -> option 'a =
fun pred  xs ->
    match xs with
    [ []     -> None
    | [x::ts]->
        if   pred x
        then Some x
        else list_find pred ts
    ];

(*=====================*)
(*  Array functions:   *)
(*=====================*)
(*-----------------*)
(*  "array_find":  *)
(*-----------------*)
value array_find: (int -> 'a -> bool) -> array 'a -> int =
fun pred arr ->
    let curr_pos = ref 0 in
    let () =
    while (curr_pos.val < Array.length arr)   &&
          not (pred curr_pos.val arr.(curr_pos.val))
    do {
        curr_pos.val := curr_pos.val + 1
    }
    in
    if  curr_pos.val = Array.length arr
    then
        raise Not_found
    else
        curr_pos.val;  (* Found! *)


(*----------------*)
(*  "array_mem":  *)
(*----------------*)
value rec  array_mem': 'a -> array 'a -> int -> int -> bool =
fun e arr  i len ->
    if  i >= len
    then
        False
    else
    if  arr.(i) = e
    then
        True
    else
        array_mem' e arr (i+1) len;

value array_mem : 'a -> array 'a -> bool =
fun e arr ->
    array_mem' e arr 0 (Array.length arr);


(*-------------------*)
(*  "random_entry":  *)
(*-------------------*)
value random_entry: array 'a -> 'a =
fun arr ->
    let n = Array.length arr in
    if  n = 0
    then
        failwith "Misc_utils.random_entry: Empty array"
    else
        arr.(Random.int n);

(*-----------------*)
(*  "foldi_left":  *)
(*-----------------*)
value foldi_left: ('a -> int -> 'b -> 'a) -> 'a -> array 'b -> 'a =
fun cbf a0 arr ->
    let len = Array.length arr in
    let rec folder: int -> 'a -> 'a =
        fun i curr ->
            if  i >= len
            then curr
            else folder (i+1) (cbf curr i arr.(i))
    in
    folder 0 a0;

(*=====================*)
(*  Hashtbl functions: *)
(*=====================*)
(*  Obvious: *)

value hashtbl_to_list: Hashtbl.t 'k 'v -> list (('k * 'b)) =
fun ht ->
    Hashtbl.fold (fun k v curr -> [(k,v)::curr]) ht [];

value hashtbl_keys   : Hashtbl.t 'k 'v -> list 'k          =
fun ht ->
    Hashtbl.fold (fun k _ curr -> [ k  :: curr]) ht [];

value hashtbl_vals   : Hashtbl.t 'k 'v -> list 'v          =
fun ht ->
    Hashtbl.fold (fun _ v curr -> [ v  :: curr]) ht [];


(*=====================*)
(*  String functions:  *)
(*=====================*)
(*------------------*)
(*  "contains_at":  *)
(*------------------*)
(*  Checks whether "s2" is a sub-string of "s1" occurring at position "p": *)

value rec match_slice: string -> int -> string -> int -> int -> bool =
fun s1 p1 s2 p2 rem2 ->
    (* We do NOT check that "p1" and "p2" are within the valid range of
       indices  of "s1" and "s2", resp -- the caller is responsible for
       that, this is an internal function:
    *)
    if  rem2 <= 0
    then True  (* All matched! *)
    else
    if  s1.[p1] <> s2.[p2]
    then False (* A mismatch ! *)
    else
        match_slice s1 (p1+1) s2 (p2+1) (rem2-1);


value contains_at  : string -> int -> string -> bool =
fun   s1 p s2 ->
    let l1 = String.length s1 in
    let l2 = String.length s2 in
    if  p + l2 > l1
    then
        False  (* "s2" is just too long *)
    else
        match_slice s1 p s2 0 l2;

value contains_from: string -> int -> string -> int  =
fun s1 p s2 ->
    let l1   = String.length s1 in
    let l2   = String.length s2 in
    let upto = l1 - l2          in

    let rec try_from: int -> int =
    fun i ->
        if  i > upto
        then (-1) (* To indicate failure *)
        else
        let r = match_slice s1 i s2 0 l2 in
        if  r
        then i    (* Found at this "i"!  *)
        else try_from (i+1)
    in
    try_from p;

(*---------------------*)
(*  "split_by_crlfs":  *)
(*---------------------*)
(*  Splits a string into text lines at "\r\n" boundaries. The terminators
    themselves are removed from the lines produced.    The resulting list
    always contains at last one string (even ""):
*)
value crlf_re = Pcre.regexp ~study: True "\\r\\n";

value split_by_crlfs: string -> list string =
fun
[   "" -> [""]  (* Not []! *)
  | str-> Pcre.split ~rex: crlf_re str
];


(*-------------------*)
(*  "split_by_nls":  *)
(*-------------------*)
(*  Splits a string into text lines according to any possible new-line symbols
    ("\r\n", single "\n", single "\r").   The new-lines themselves are removed
    from the result. The resulting list is always non-empty:
*)
value nl_re = Pcre.regexp ~study: True "\\r\\n|\\n|\\r";

value split_by_nls: string -> list string =
fun
[   "" -> [""]  (* Not []! *)
  | str-> Pcre.split ~rex: nl_re str
];


(*----------------------*)
(*  "split_by_whites":  *)
(*----------------------*)
value ws_re = Pcre.regexp ~study: True "\\s+";

value split_by_whites: string -> list string =
fun
[   "" -> [""]  (* Not []! *)
  | str-> Pcre.split ~rex: ws_re str
];


(*----------------*)
(*  "strip_eol":  *)
(*----------------*)
(*  Removes an "\n" (UNIX format), "\r\n" (Windows format) or "\r" (Mac format)
    from the end of the string, if possible:
*)
value strip_eol: string -> string =
fun s ->
    let len = String.length s  in
    if  ExtString.String.ends_with s "\r\n"
    then
        String.sub s 0 (len-2)
    else
    if (ExtString.String.ends_with s "\n") ||
       (ExtString.String.ends_with s "\r")
    then
        String.sub s 0 (len-1)
    else
        s;


(*-------------------*)
(*  "pcre_of_glob":  *)
(*-------------------*)
(*  Converts a "glob-" (or "shell-") type  regular expression   into the PCRE
    type. Adapted from the "regexp:sh_to_awk" function of Erlang/OTP, "stdlib"
    application.
    The main difficulty is getting character sets right as the conventions in
    "Glob" and PCRE are different:
*)
value is_special_char: char -> bool =
fun c ->
    (* Test if "c" is a special character: *)
    String.contains "|*+?()\\^$.[]\"" c ;


value str_matches: string -> int -> string -> bool =
fun gre pos x ->
    (* Test safely whether "x" is a sub-string of "gre" starting at "pos": *)
    try
        String.sub gre pos (String.length x) = x
    with
    [ _ -> False ];


value rec pcre_of_glob_1: string -> int -> string =
fun gre pos ->
    if  pos < String.length gre
    then
    (
        if  str_matches gre pos "*"    (* "*" stands for any string *)
        then
            ".*" ^ (pcre_of_glob_1 gre (pos+1))
        else
        if  str_matches gre pos "?"    (* "?" stands for any single char *)
        then
            "."  ^ (pcre_of_glob_1 gre (pos+1))
        else
        if  str_matches gre pos "[^]"   (* Just trust Erlang *)
        then 
            "\\^"^ (pcre_of_glob_1 gre (pos+3))
        else
        if  str_matches gre pos "[^"
        then
            "["  ^ (pcre_of_glob_2 gre (pos+2) True )
        else
        if  str_matches gre pos "[!"
        then
            "[^" ^ (pcre_of_glob_2 gre (pos+2) False)
        else
        if  str_matches gre pos "["
        then
            "["  ^ (pcre_of_glob_2 gre (pos+1) False)
        else
            (* Unspecialise everything else which is not an escape character: *)
            let c = gre.[pos]            in
            let s = String.sub gre pos 1 in

            if  is_special_char c
            then
                "\\" ^s^ (pcre_of_glob_1 gre (pos+1))
            else
                s ^      (pcre_of_glob_1 gre (pos+1))
    )
    else
        (* Input exhausted, fix the end: *)
        ")$"


and pcre_of_glob_2:  string -> int -> bool -> string =
fun gre pos up_arrow ->
    if  str_matches gre pos "]"
    then
        "]" ^ (pcre_of_glob_3 gre (pos+1) up_arrow)
    else
        pcre_of_glob_3 gre pos up_arrow


and pcre_of_glob_3: string -> int -> bool -> string =
fun gre pos up_arrow ->
    if  pos < String.length gre
    then
    (
        if  str_matches gre pos "]"
        then
        (
            if up_arrow
            then
                "^]" ^ (pcre_of_glob_1 gre (pos+1))
            else
                "]"  ^ (pcre_of_glob_1 gre (pos+1))
        )
        else
            (String.sub gre pos 1) ^ (pcre_of_glob_3 gre (pos+1) up_arrow)
    )
    else
        if  up_arrow
        then
            "^" ^ (pcre_of_glob_1 gre pos)
        else
            pcre_of_glob_1 gre pos;


(* Now, finally, the externally-visible top-level: *)

value rec pcre_of_glob: string -> string =
fun gre ->
    "^(" ^ (pcre_of_glob_1 gre 0);  (* Fix the beginning, and proceed *)


(*---------------------*)
(*  "uniq_file_name":  *)
(*---------------------*)
(*  Similar to "Filename.temp_file_name" from the OCaml standard library, but
    allows the user to specify the directory where the temporary file will be
    located as a function parameter.
    XXX: this function relies on "Random.bits" -- it is NOT thread-safe:
*)
value rand_init = Random.self_init ();    (* Make sure PRNG is initialised *)

value uniq_file_name dir prefix suffix =
    let n1 = (Random.bits ()) land 0xFFFFFF in (* Random 24-bit number *)
    let n2 = (Random.bits ()) land 0xFFFFFF in (* Another one *)

    Filename.concat dir (Printf.sprintf "%s%06x%06x%s" prefix n1 n2 suffix);


(*--------------------*)
(*  "random_string":  *)
(*--------------------*)
(*  Generates a random string of the given alphabet and length:  *)

value random_string: string -> int -> string =
fun   alphabet len ->
    if   len < 0
    then invalid_arg "\"Misc_utils.random_string\": len < 0"
    else
    if   len = 0 || alphabet = ""
    then ""    (* Trivial case *)
    else       (* Non-trivial case: *)

    let buff = Buffer.create len in
    do {
        for i = 0 to (len-1)
        do {
            (* Select a random char from the "alphabet": *)
            let p = Random.int (String.length alphabet) in
            let c = alphabet.[p] in
            Buffer.add_char buff c
        };
        (* Get the "buff" content: *)
        let res = Buffer.contents buff in
        do {
            Buffer.reset buff;
            res
        }
    };

(*====================*)
(*  E-Mails Parsing:  *)
(*====================*)
(*-----------------------*)
(*  "parse_email_addr":  *)
(*-----------------------*)
(*  NB: we use the "Pcre" module here, not the standard "Str",   as the latter
    relies on implicit sequencing of match calls, and thus would not be thread-
    safe. This function IS thread-safe.
    Returns: (Name, MailBox, Domain)
*)
value email_re_src_search =
    ("(\"(.*)\"\\s+<)?([^\\\\<>()\\[\\],;:@\" ]+)" ^
     "@(([a-z0-9-]+\\.)*[a-z0-9-]+)>?");

value email_regexp_search =
    Pcre.regexp
    ~study:     True
    ~flags:     [`DOLLAR_ENDONLY]
    email_re_src_search;

value email_re_src_strict = "^"^email_re_src_search^"$";

value email_regexp_strict =
    Pcre.regexp
    ~study:     True
    ~flags:     [`DOLLAR_ENDONLY]
    email_re_src_strict;

value parse_email_addr: string -> (string * string * string) =
fun   email ->
    let match_res =
        try (Pcre.exec ~rex:email_regexp_strict email) with
        [   Not_found ->
                failwith
                ("Misc_utils.parse_email_addr: Invalid E-Mail: "^email)
        ]
    in
    let parts     = Pcre.get_substrings ~full_match:False  match_res  in

    let name      = ExtString.String.strip parts.(1) in
    let mbox      = String.lowercase parts.(2)       in
    let domain    = String.lowercase parts.(3)       in

    (* Matched parts to return: *)
    (name, mbox, domain);

(*-----------------------*)
(*  "get_email_domain":  *)
(*-----------------------*)
(*  Extracts the domain name from an e-mail: a simpler form of
    "parse_email_addr":
*)
value domain_regexp =
    Pcre.regexp ~study:True "@(([a-z0-9-]+\\.)*[a-z0-9-]+)";

value get_email_domain: string -> string =
fun email ->
try
    String.lowercase
        (Pcre.extract ~rex:domain_regexp ~full_match:False email).(0)
with
    [Not_found ->
    failwith ("Misc_utils.get_email_domain: Invalid EMail: "^email)];

(*-------------------------*)
(*  "extract_email_addr":  *)
(*-------------------------*)
value extract_email_addr: string -> option string =
fun line ->
    let match_res =
        try  Some (Pcre.extract ~rex:email_regexp_search ~full_match:False line)
        with [ Not_found -> None ]
    in
    match match_res with
    [ None       -> None
    | Some parts ->
        let mbox  = String.lowercase parts.(2)   in
        let domain= String.lowercase parts.(3)   in
        Some (mbox ^"@"^ domain)
    ];

(*===============*)
(*  Exceptions:  *)
(*===============*)
(*----------------*)
(*  "print_exn":  *)
(*----------------*)
value rec print_exn: exn -> string =
fun exc ->
    match exc with
    [   (* Special case: "Unix.Unix_error": *)
        Unix.Unix_error code fn arg ->
            Printf.sprintf "UNIX_Error (%s, %s, %s)"
                (Unix.error_message code)   fn  arg

      | (* Special case: "IoXML.ExcLoc": *)
        IoXML.ExcLoc (fp, tp) err ->
            Printf.sprintf "IoXML_Error in chars %d-%d: %s"
                fp tp (print_exn err)
      | _ ->
        Printexc.to_string exc
    ];

(*--------------------*)
(*  "joint_err_msg":  *)
(*--------------------*)
(*  Repeated messages are eliminated: *)

value joint_err_msg: list exn -> string =
fun errs ->
    let msgs = ExtList.List.unique (List.map print_exn errs) in
    String.concat ", " msgs;

(*-----------------*)
(*  Fatal Errors:  *)
(*-----------------*)
value fatal_error: ?rc:int -> string -> 'a =
fun ?(rc=1) err_msg ->
do {
    prerr_endline err_msg;
    flush stderr;
    exit  rc
};

(*--------------------*)
(*  "lock_or_abort":  *)
(*--------------------*)
(*  Locks the given file descr, or aborts the whole program with the given
    return code if the lock cannot be acquired immediately:
*)
value lock_or_abort: ?rc:int -> string -> Unix.file_descr -> unit =
fun ?(rc=1) file_name fd     ->
try
do {
    ignore (Unix.lseek fd 0 Unix.SEEK_SET);
    Unix.lockf fd Unix.F_TLOCK 0
}
with
[_ ->
    fatal_error
        ~rc
        ("The file \""^file_name^"\" is already in use by another program"^
         "\n\t(or another running instance of this program)." ^
         "\n\tPlease terminate that program first, "          ^
         "\n\tor specify another file name in the config.")
];


(*========*)
(*  I/O:  *)
(*========*)
(*------------------*)
(*  "line_reader":  *)
(*------------------*)
(*  Line-Reader Class:
    Parameterised by the socket type ('s) and the actual low-level reading
    function ("byte_reader"),  so it can work over e.g. both "normal" Tnio
    and SSL sockets;

    "byte_reader" args: socket buff from_pos max_needed_bytes abs_deadline;
    return value      : actually_read_bytes
*)
type io_func 's =  's  -> string -> int -> int -> float -> int;

class line_reader ['s]
    (byte_reader:  io_func 's)
    (buff_size  :  int) =

object(self)
    value buffer             : string      = String.create buff_size;
    value mutable next       : string      = "";
    value mutable ready_lines: list string = [];

    (*  "find_eol":
        Looks for "\r\n" in the specified range of the "buffer":
    *)
    method private find_eol: int -> int -> int =
    fun from until ->
        let next = from+1 in
        if  next >= until
        then
            -1   (* EOL not found: *)
        else
        if  buffer.[from] = '\r' && buffer.[next] = '\n'
        then
            from (* EOL found!     *)
        else
            self#find_eol next until; (* Try again... *)

    (*  "fill_buff":
        Internal recursive Line Reader: reads the data in from a socket,
        until an EOL is encountered. Returns (EOL_pos, total_bytes):
    *)
    method private fill_buff: 's -> int -> float -> (int * int) =
    fun sock pos deadline ->
        (* Wait for data available on the "sock", and read them until no
           more data are available:
        *)
        let max_len = buff_size - pos in
        if  max_len <= 0
        then
            (* No (more) input possible as the buffer is full, but as we
               got here, the required EOL was still not found:
            *)
            failwith "Misc_utils.line_reader: Line too long"
        else
        (* Otherwise: try to read (more) data: *)
        let (got_len, err) =
            try  (byte_reader sock buffer pos max_len deadline, None)
            with [hmm -> (-1, Some hmm)]
        in
        if  got_len <= 0
        then
            let msg =
                match err with
                [ Some hmm -> print_exn hmm
                | None     -> "peer disconnected"]
            in
            failwith ("Misc_utils.line_reader: Read failed: "^msg)
        else
        (* Is there EOL in the bytes received?  *)
        let until   = pos + got_len           in
        let eol_pos = self#find_eol pos until in
        if  eol_pos = -1
        then
            (* EOL not found -- need more data: *)
            self#fill_buff sock until deadline
        else
            (* Return what we found for further processing: *)
            (eol_pos, until);

    (* "find_lines":
       Searches  the "buffer" from the "from" position for "len" bytes,
       identifies EOLs, and puts the separate lines (without EOLs) into
       "ready_lines", and the final chunk  (if any, ie not a full line)
       into "next":
    *)
    method private parse_buff: int -> int -> unit =
    fun from until ->
        let eol_pos = self#find_eol from until in
        if  eol_pos = -1
        then
            (* No more EOLs, the rest of the "buffer" comes into "next": *)
            next := String.sub buffer from (until-from)
        else
            (* Got a line, save it and try more: *)
            let line = String.sub buffer from (eol_pos-from) in
            do {
                ready_lines := ready_lines @ [line];
                self#parse_buff   (eol_pos+2) until
            };

    (* "recv_line":
        Returns a line read from the socket, without the terminating EOL:
    *)
    method  recv_line: 's -> float -> string =
    fun sock deadline ->
    do {
        if  ready_lines = []
        then
            (* If there are no lines already pending, fill in the "buffer"
               until and EOL is found. There may already be a "next" chunk
               left from the previous reading, so put it into the "buffer"
               before all:
            *)
            let nlen = String.length next               in
            let   () = String.blit next 0 buffer 0 nlen in

            let (eol_pos, until) = self#fill_buff sock nlen deadline    in
            do {
                assert (nlen <= eol_pos  && eol_pos < until);
                (* Now: there is certainly an EOL at "eol_pos",  but there
                   could be more after that as well, so parse the input:
                *)
                ready_lines  := [String.sub buffer 0 eol_pos];
                self#parse_buff (eol_pos+2) until
            }
        else
            ();
        (* We now know that there is at least one line in "ready_lines" --
           return it:
        *)
        let hl  = List.hd ready_lines in
        let tls = List.tl ready_lines in
        do {
            ready_lines := tls;
            hl
        }
    };

    (* "reset":
       Reset the internal buffers of the reader, so it can read new data
       from scratch:
    *)
    method  reset: unit -> unit =
    fun () ->
    do {
        ready_lines  := [];
        next         := ""
    };

    (* "leftover_data":
       The user may need to stop reading lines at some point, and switch
       to the chunk mode. If there are any data accumulated  in the line
       but not yet returned, they can be returned by this method,    and 
       removed from the reader (but no more than the "limit"):
    *)
    method leftover_data: int -> string =
    fun limit ->
        let  pending = String.concat "\r\n" (ready_lines @ [next]) in
        let  plen    = String.length pending                       in

        (* Re-set the object before re=adjusting it: *)
        let  ()  = self#reset () in

        if   plen > limit
        then
            let give = String.sub pending 0 limit    in
            let klen = plen - limit                  in
            do {
                (* Be VERY CAREFUL here.  The "keep" chunk  needs to be
                   correctly re-parsed to be split between "ready_lines"
                   and the "next" chunk:
                *)
                String.blit pending limit buffer 0 klen;
                self#parse_buff  0 klen;
                give
        }
        else
            (* Return everything we got: *)
            pending;
end;

(*------------------------*)
(*  "open_write_locked":  *)
(*------------------------*)
(*  Fatal errorif cannot acquire the lock: *)
value open_write_locked:
    ~append_mode:bool   -> ?lock_fail_rc:int -> string ->
    option Unix.file_descr =

fun ~append_mode ?(lock_fail_rc=1) file_name ->
    if  file_name <> ""
    then
        (* The flags for "open". In any case, INITIALLY we do not truncate
           the file, as it may be locked by another process!  Only when we
           successfully acquire the lock, we will modify it:
        *)
        (* Open or create the file: *)
        let flags0  =
            if  append_mode
            then [Unix.O_WRONLY; Unix.O_CREAT]
            else [Unix.O_RDWR;   Unix.O_CREAT]
        in
        let fd =
            try Unix.openfile file_name flags0 0o600
            with [hmm ->
                fatal_error
                    ("ERROR: Cannot open or create \""^file_name^"\": "^
                    (print_exn hmm))
            ]
        in
        (* Lock it -- failure is fatal here: *)
        let () = lock_or_abort ~rc:lock_fail_rc file_name fd
        in
        (* Now: if the file was to be opened for appending, move to the end
           of the file; otherwise, truncate it:
        *)
        let () =
            if  append_mode
            then ignore (Unix.lseek fd 0 Unix.SEEK_END)
            else
            do {
                 (* May need to move to the beginning of the file: *)
                 ignore (Unix.lseek fd 0 Unix.SEEK_SET);
                 Unix.ftruncate fd 0
            }
        in
        Some fd
    else
        None;

(*----------------*)
(*  "strict_io":  *)
(*-----------------*)
(*  Reads or writes (depending on the "io_func") the data from/to a socket
    until the required number of bytes are received,   or an error occurs.
    Exceptions are caught, and the number of bytes which REMAIN TO BE tran-
    sferred is returned in all cases:
*)
value rec strict_io':
    io_func 's -> 's -> string -> int -> int -> float -> int =

fun io_func socket buff from len deadline ->
    (* Actually read/write the data: *)
    let n =
        try  io_func socket buff from len deadline
        with [_ -> -1]
    in
    if  n <= 0
    then
        (* I/O error occurred, return the number of untransferred bytes: *)
        len
    else
        let () = assert (n > 0 && n <= len) in
        if  n = len
        then
            0 (* All done! *)
        else
            (* Try more: *)
            strict_io' io_func socket buff (from+n) (len-n) deadline;


value   strict_io:
    io_func 's -> 's -> string -> int -> int -> float -> int =

fun io_func socket buff from len deadline  ->
    (* These checks are factored out of recursive calls: *)
    if  from + len > String.length buff || from < 0 || len < 0
    then
        invalid_arg "Misc_utils.strict_io"
    else
    if  len = 0
    then
        0 (* Nothing to do *)
    else
        let rem = strict_io' io_func socket buff from len deadline
        in
        (* Return the number of bytes transferred: *)
        len - rem;

(*---------------*)
(*  "send_str":  *)
(*---------------*)
value send_str:
    io_func 'socket -> 'socket -> string -> float -> unit =

fun write_func socket str deadline ->
    let len = String.length str
    in
    (* Do it straight through the low-level "strict_io'": *)
    let rem = strict_io' write_func socket str 0 len deadline
    in
    if  rem <> 0
    then
        failwith ("Misc_utils.send_str: I/O error: "^(string_of_int rem)^
                  " bytes not sent")
    else
        ();

(*----------------*)
(*  "read_file":  *)
(*----------------*)
(*  Reads the whole file into a string: *)

value read_file: string -> string =
fun file_name ->
    let file_size = (Unix.stat  file_name).Unix.st_size in
    let buff      = String.create   file_size in
    let inch      = open_in_bin file_name     in
    do {
        really_input inch buff  0 file_size;
        close_in     inch;
        buff
    };

(*----------------*)
(*  "read_lines": *)
(*----------------*)
(*  Reads a text file into a list of lines; EOLs are removed: *)

value rec read_lines': in_channel -> list string -> list string =
fun inch  curr_lines ->
    try
    (
      let line = strip_eol (input_line inch) in

      read_lines' inch ([line :: curr_lines])
    )
    with
    [
        End_of_file -> (* All done! *)
        curr_lines
    ];

value read_lines: string -> list string =
fun   file_name ->
    let inch  =   open_in file_name in
    let lines =   List.rev (read_lines' inch []) in
    do {
        close_in inch;
        lines
    };


(*-----------------*)
(*  "write_fstr":  *)
(*-----------------*)
(*  NB: No "short writes" handling here, so this function is for ordinary files
    only, not for sockets etc:
*)
value write_fstr: Unix.file_descr -> string -> unit =
fun fd str ->
    ignore (Unix.write fd str 0 (String.length str));


(*-----------------*)
(*  "safe_close":  *)
(*-----------------*)
value safe_close: Unix.file_descr -> unit =
fun fd ->
    try Unix.close fd with [_ -> ()];


(*-----------*)
(*  "ip4*":  *)
(*-----------*)
(*  A more efficient implementation would use the internal representation of
    "Unix.inet_addr", but we don't do it for the sake of simplicity and por-
    tability -- this function is hopefully not time-ctitical:
*)
type  ip4 = (int * int * int * int);

value ip4_of_inet_addr:  Unix.inet_addr -> ip4 =
fun addr ->
    let   octets = List.map int_of_string
                  (ExtString.String.nsplit (Unix.string_of_inet_addr addr) ".")
    in
    match octets with
    [ [a1; a2; a3; a4] -> (a1, a2, a3, a4)
    | _                -> assert False
    ];

value inet_addr_of_ip4: ip4 -> Unix.inet_addr =
fun (a1, a2, a3, a4) ->
    if  a1 < 0   || a2 < 0   || a3 < 0   || a4 < 0 ||
        a1 > 255 || a2 > 255 || a3 > 255 || a4 > 255
    then
        invalid_arg "Misc_utils.inet_addr_of_ip4"
    else
        Unix.inet_addr_of_string (Printf.sprintf "%d.%d.%d.%d" a1 a2 a3 a4);


(*===================*)
(*  Users / Groups:  *)
(*===================*)
(*  Empty value for "user" and/or "group" means they will not be cahnged *)

value run_as: string -> string -> unit =
fun user group ->
    (* Get the numeric "uid": *)
    let uid =
        if  user = ""
        then (-1)
        else
        try
            (* Is "user" a numeric ID already?  *)
            int_of_string user
        with
        [_ ->
            (* No -- need to look into "/etc/passwd"! *)
            try (Unix.getpwnam user).Unix.pw_uid
            with
            [Not_found ->
                failwith ("Misc_utils.run_as: No such user: \""^user^"\"")
            ]
        ]
    in
    (* Get the numeric "gid": *)
    let gid =
        if  group = ""
        then (-1)
        else
        try
            (* Is "group" a numeric ID already? *)
            int_of_string group
        with
        [_ ->
            (* No -- need to look into "/etc/group"!  *)
            try (Unix.getgrnam group).Unix.gr_gid
            with
            [Not_found ->
                failwith ("Misc_utils.run_as: No such group: \""^group^"\"")
            ]
        ]
    in  (* Now try to set them actually. NB: Set the GID first, as if UID
           is set to an unprivileged value, GID cannot be changed anymore!
        *)
    do {
        if  gid <> (-1)
        then
            try Unix.setgid gid
            with
            [_ -> failwith
                  ("Misc_utils.run_as: Cannot set GID to "^(string_of_int gid))
            ]
        else ();

        if  uid <> (-1)
        then
            try Unix.setuid uid
            with
            [_ -> failwith
                  ("Misc_utils.run_as: Cannot set UID to "^(string_of_int uid))
            ]
        else ()
    };

