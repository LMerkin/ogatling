// vim:ts=8:syntax=c
//===========================================================================//
//                                  "ssl.ml":                                //
//      Adapted by Explar Technologies Ltd, 2004, for use with TNIO RTS      //
//===========================================================================//
/*
   Copyright (C) 2003 Samuel Mimram

   This file is part of Ocaml-ssl.

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*/

/**
 * Libssl bindings for OCaml.
 *
 * @author Samuel Mimram
 */

/* $Id: ssl_stubs.c,v 1.19 2004/12/18 10:56:08 smimram Exp $ */

#include <caml/alloc.h>
#include <caml/callback.h>
#include <caml/fail.h>
#include <caml/memory.h>
#include <caml/misc.h>
#include <caml/mlvalues.h>

#include <openssl/ssl.h>
#include <openssl/pem.h>
#include <openssl/err.h>
#include <unistd.h>
#include <string.h>

static int client_verify_callback(int, X509_STORE_CTX *);

/*******************
 * Data structures *
 *******************/

struct ssl_socket__t
{
  SSL *handler;
  int fd;
};

typedef struct ssl_socket__t ssl_socket_t;

static ssl_socket_t* ssl_socket_of_block(value block)
{
  return (ssl_socket_t*)Field(block, 1);
}

static void finalize_ssl_socket(value block)
{
  ssl_socket_t *s = ssl_socket_of_block(block);
  SSL_free(s->handler);
  free(s);
}

static void finalize_ctx(value block)
{
  SSL_CTX *ctx = (SSL_CTX*)Field(block, 1);
  SSL_CTX_free(ctx);
}

// IDs of TNIO threads:
extern value tnio_thread_self (value);
extern value tnio_thread_id   (value);

static unsigned long get_thread_id ()
{
        value self = tnio_thread_self  (Val_unit);
        value myid = tnio_thread_id    (self);
        return (unsigned long) (Int_val(myid));
}

// Non-threading:
CAMLprim value ocaml_ssl_init(value unit)
{
  CAMLparam1(unit);

  SSL_library_init();
  SSL_load_error_strings();

  // The following is required for thread-safe opeartion:
  CRYPTO_set_id_callback (get_thread_id);

  CAMLreturn(Val_unit);
}

/*****************************
 * Context-related functions *
 *****************************/
// Non-blocking:
CAMLprim value ocaml_ssl_create_client_context(value protocol)
{
  CAMLparam1(protocol);
  SSL_METHOD *method;
  SSL_CTX *ctx;
  CAMLlocal1(block);

  switch (Int_val(protocol))
    {
    case 0:
      method = SSLv2_client_method();
      break;

    case 1:
      method = SSLv23_client_method();
      break;

    case 2:
      method = SSLv3_client_method();
      break;

    case 3:
      method = TLSv1_client_method();
      break;

    default:
      invalid_argument("Ssl.create_client_context: Unknown method (this should not have happened, please report).");
      break;
    }

  if (method == NULL)
    raise_constant(*caml_named_value("ssl_exn_method_error"));
  if ((ctx = SSL_CTX_new(method)) == NULL)
    raise_constant(*caml_named_value("ssl_exn_certificate_error"));

  block = alloc_final(2, finalize_ctx, sizeof(SSL_CTX*), sizeof(SSL_CTX*));
  Field(block, 1) = (value)ctx;
  CAMLreturn(block);
}

// Non-blocking:
CAMLprim value ocaml_ssl_create_server_context(value protocol, value cert, value privkey)
{
  CAMLparam3(protocol, cert, privkey);
  char *cert_name = String_val(cert);
  char *privkey_name = String_val(privkey);
  SSL_METHOD *method;
  SSL_CTX *ctx;
  CAMLlocal1(block);

  switch (Int_val(protocol))
    {
    case 0:
      method = SSLv2_server_method();
      break;

    case 1:
      method = SSLv23_server_method();
      break;

    case 2:
      method = SSLv3_server_method();
      break;

    case 3:
      method = TLSv1_server_method();
      break;

    default:
      invalid_argument("Ssl.create_server_context: Unknown method (this should not have happened, please report).");
      break;
    }

  if (method == NULL)
    raise_constant(*caml_named_value("ssl_exn_method_error"));
  if ((ctx = SSL_CTX_new(method)) == NULL)
    raise_constant(*caml_named_value("ssl_exn_certificate_error"));
  if (SSL_CTX_use_certificate_file(ctx, cert_name, SSL_FILETYPE_PEM) <= 0)
    {
      SSL_CTX_free(ctx);
      raise_constant(*caml_named_value("ssl_exn_certificate_error"));
    }
  if (SSL_CTX_use_PrivateKey_file(ctx, privkey_name, SSL_FILETYPE_PEM) <= 0)
    {
      SSL_CTX_free(ctx);
      raise_constant(*caml_named_value("ssl_exn_private_key_error"));
    }
  if (!SSL_CTX_check_private_key(ctx))
    {
      SSL_CTX_free(ctx);
      raise_constant(*caml_named_value("ssl_exn_unmatching_keys"));
    }

  block = alloc_final(2, finalize_ctx, sizeof(SSL_CTX*), sizeof(SSL_CTX*));
  Field(block, 1) = (value)ctx;
  CAMLreturn(block);
}

/* TODO: factorize the code with ocaml_ssl_create_server_context. */
// Non-blocking:
CAMLprim value ocaml_ssl_create_context(value protocol, value cert, value privkey)
{
  CAMLparam3(protocol, cert, privkey);
  char *cert_name = String_val(cert);
  char *privkey_name = String_val(privkey);
  SSL_METHOD *method;
  SSL_CTX *ctx;
  CAMLlocal1(block);

  switch (Int_val(protocol))
    {
    case 0:
      method = SSLv2_method();
      break;

    case 1:
      method = SSLv23_method();
      break;

    case 2:
      method = SSLv3_method();
      break;

    case 3:
      method = TLSv1_method();
      break;

    default:
      invalid_argument("Ssl.create_context: Unknown method (this should not have happened, please report).");
      break;
    }

  if (method == NULL)
    raise_constant(*caml_named_value("ssl_exn_method_error"));
  if ((ctx = SSL_CTX_new(method)) == NULL)
    raise_constant(*caml_named_value("ssl_exn_certificate_error"));
  if (SSL_CTX_use_certificate_file(ctx, cert_name, SSL_FILETYPE_PEM) <= 0)
    {
      SSL_CTX_free(ctx);
      raise_constant(*caml_named_value("ssl_exn_certificate_error"));
    }
  if (SSL_CTX_use_PrivateKey_file(ctx, privkey_name, SSL_FILETYPE_PEM) <= 0)
    {
      SSL_CTX_free(ctx);
      raise_constant(*caml_named_value("ssl_exn_private_key_error"));
    }
  if (!SSL_CTX_check_private_key(ctx))
    {
      SSL_CTX_free(ctx);
      raise_constant(*caml_named_value("ssl_exn_unmatching_keys"));
    }

  block = alloc_final(2, finalize_ctx, sizeof(SSL_CTX*), sizeof(SSL_CTX*));
  Field(block, 1) = (value)ctx;
  CAMLreturn(block);
}

// Non-blocking:
CAMLprim value ocaml_ssl_get_client_verify_callback_ptr (value unit)
{
  CAMLparam1(unit);

  CAMLreturn(copy_nativeint((int)client_verify_callback));
}

// Non-blocking (apparently):
CAMLprim value ocaml_ssl_ctx_set_verify(value context, value vmode, value vcallback)
{
  CAMLparam3(context, vmode, vcallback);
  SSL_CTX *ctx = (SSL_CTX*)Field(context, 1);
  int mode = 0;
  value mode_tl = vmode;
  int (*callback) (int, X509_STORE_CTX*) = NULL;

  if (Is_long(vmode))
    mode = SSL_VERIFY_NONE;

  while (Is_block(mode_tl))
    {
      switch(Int_val(Field(mode_tl, 0)))
        {
        case 0:
          mode |= SSL_VERIFY_PEER;
          break;

        case 1:
          mode |= SSL_VERIFY_FAIL_IF_NO_PEER_CERT | SSL_VERIFY_PEER;
          break;

        case 2:
          mode |= SSL_VERIFY_CLIENT_ONCE | SSL_VERIFY_PEER;
          break;

        default:
          invalid_argument("Ssl.ctx_set_verify: mode");
        }

      mode_tl = Field(mode_tl, 1);
    }

  if (Is_block(vcallback))
    callback = (int(*) (int, X509_STORE_CTX*))Nativeint_val(Field(vcallback, 0));

  SSL_CTX_set_verify(ctx, mode, callback);

  CAMLreturn(Val_unit);
}

// Non-blocking:
CAMLprim value ocaml_ssl_ctx_set_verify_depth(value context, value vdepth)
{
  CAMLparam2(context, vdepth);
  SSL_CTX *ctx = (SSL_CTX*)Field(context, 1);
  int depth = Int_val(vdepth);

  if (depth < 0)
    invalid_argument("Ssl.ctx_set_verify_depth: depth");

  SSL_CTX_set_verify_depth(ctx, depth);

  CAMLreturn(Val_unit);
}

// Non-blocking:
CAMLprim value ocaml_ssl_ctx_set_client_CA_list_from_file(value context, value vfilename)
{
  CAMLparam2(context, vfilename);
  SSL_CTX *ctx = (SSL_CTX*)Field(context, 1);
  char *filename = String_val(vfilename);
  STACK_OF(X509_NAME) *cert_names;

  cert_names = SSL_load_client_CA_file(filename);
  if (cert_names != 0)
    SSL_CTX_set_client_CA_list(ctx, cert_names);
  else
    raise_constant(*caml_named_value("ssl_exn_certificate_error"));

  CAMLreturn(Val_unit);
}


/****************************
 * Cipher-related functions *
 ****************************/
// Non-blocking:
CAMLprim value ocaml_ssl_ctx_set_cipher_list(value context, value ciphers_string)
{
  CAMLparam2(context, ciphers_string);
  SSL_CTX *ctx = (SSL_CTX*)Field(context, 1);
  char *ciphers = String_val(ciphers_string);

  if(*ciphers == 0)
    raise_constant(*caml_named_value("ssl_exn_cipher_error"));

  if(SSL_CTX_set_cipher_list(ctx, ciphers) != 1)
    {
      raise_constant(*caml_named_value("ssl_exn_cipher_error"));
    }
  CAMLreturn(Val_unit);
}

// Non-blocking:
CAMLprim value ocaml_ssl_get_current_cipher(value socket)
{
  CAMLparam1(socket);

  SSL_CIPHER *cipher = (SSL_CIPHER*)SSL_get_current_cipher(ssl_socket_of_block(socket)->handler);
  if (cipher == NULL)
    raise_constant(*caml_named_value("ssl_exn_cipher_error"));

  CAMLreturn(copy_nativeint((long)cipher));
}

// Non-blocking:
CAMLprim value ocaml_ssl_get_cipher_description(value vcipher)
{
  CAMLparam1(vcipher);
  char buf[1024];

  CAMLreturn(copy_string(SSL_CIPHER_description((SSL_CIPHER*)Nativeint_val(vcipher), buf, 1024)));
}

// Non-blocking:
CAMLprim value ocaml_ssl_get_cipher_name(value vcipher)
{
  CAMLparam1(vcipher);
  CAMLreturn(copy_string(SSL_CIPHER_get_name((SSL_CIPHER*)Nativeint_val(vcipher))));
}

// Non-blocking:
CAMLprim value ocaml_ssl_get_cipher_version(value vcipher)
{
  CAMLparam1(vcipher);
  CAMLreturn(copy_string(SSL_CIPHER_get_version((SSL_CIPHER*)Nativeint_val(vcipher))));
}

/*********************************
 * Certificate-related functions *
 *********************************/
// Non-blocking:
static void finalize_cert(value block)
{
  X509 *cert = (X509*)Field(block, 1);
  X509_free(cert);
}

// Non-blocking -- reads data from a file:
CAMLprim value ocaml_ssl_read_certificate(value vfilename)
{
  CAMLparam1(vfilename);
  char *filename = String_val(vfilename);
  X509 *cert = NULL;
  FILE *fh = NULL;

  if((fh = fopen(filename, "r")) == NULL)
    raise_constant(*caml_named_value("ssl_exn_certificate_error"));

  if((PEM_read_X509(fh, &cert, 0, 0)) == NULL)
    {
      fclose(fh);
      raise_constant(*caml_named_value("ssl_exn_certificate_error"));
    }

  CAMLlocal1(block);
  block = alloc_final(2, finalize_cert, sizeof(X509*), sizeof(X509*));
  Field(block, 1) = (value)cert;
  CAMLreturn(block);
}

// Non-blocking -- gets a certificate already presented by the peer:
CAMLprim value ocaml_ssl_get_certificate(value socket)
{
  CAMLparam1(socket);

  X509 *cert = SSL_get_peer_certificate(ssl_socket_of_block(socket)->handler);

  if (cert == NULL)
     raise_constant(*caml_named_value("ssl_exn_certificate_error"));

  CAMLlocal1(block);
  block = alloc_final(2, finalize_cert, sizeof(X509*), sizeof(X509*));
  Field(block, 1) = (value)cert;
  CAMLreturn(block);
}

// Non-blocking:
CAMLprim value ocaml_ssl_get_issuer(value certificate)
{
  CAMLparam1(certificate);

  X509 *cert = (X509*)Field(certificate, 1);
  char *issuer = X509_NAME_oneline(X509_get_issuer_name(cert), 0, 0);
  if (issuer == NULL) caml_raise_not_found ();

  CAMLreturn(copy_string(issuer));
}

// Non-blocking:
CAMLprim value ocaml_ssl_get_subject(value certificate)
{
  CAMLparam1(certificate);

  X509 *cert = (X509*)Field(certificate, 1);
  char *subject = X509_NAME_oneline(X509_get_subject_name(cert), 0, 0);
  if (subject == NULL) caml_raise_not_found ();

  CAMLreturn(copy_string(subject));
}

// Non-blocking -- uses a local file:
CAMLprim value ocaml_ssl_ctx_load_verify_locations(value context, value ca_file, value ca_path)
{
  CAMLparam3(context, ca_file, ca_path);
  SSL_CTX *ctx = (SSL_CTX*)Field(context, 1);
  char *CAfile = String_val(ca_file);
  char *CApath = String_val(ca_path);

  if(*CAfile == 0)
    CAfile = NULL;
  if(*CApath == 0)
    CApath = NULL;

  if(SSL_CTX_load_verify_locations(ctx, CAfile, CApath) != 1)
    invalid_argument("Ssl.ctx_load_verify_locations: ca_file or ca_path");
  CAMLreturn(Val_unit);
}

/*************************
 * Operations on sockets *
 *************************/
// In all cases, we must check explicitly that the socket is not "invalid",
// otherwise a segfault can occur:

// Non-blocking:
CAMLprim value ocaml_ssl_get_file_descr(value socket)
{
  CAMLparam1(socket);
  if ((void*)socket == NULL)
     invalid_argument ("Ssl.get_file_descr: Invalid socket");

  CAMLreturn(Val_int(ssl_socket_of_block(socket)->fd));
}

// Non-blocking:
CAMLprim value ocaml_ssl_embed_socket(value ts, value context)
{
  CAMLparam2(ts, context);
  int socket = Int_val(ts);
  SSL_CTX *ctx = (SSL_CTX*)Field(context, 1);

  ssl_socket_t *ret = malloc(sizeof(ssl_socket_t));
  CAMLlocal1(block);

  if (socket < 0)
    raise_constant(*caml_named_value("ssl_exn_invalid_socket"));
  if ((ret->handler = SSL_new(ctx)) == NULL)
    raise_constant(*caml_named_value("ssl_exn_handler_error"));
  SSL_set_fd(ret->handler, socket);
  ret->fd = socket;

  block = alloc_final(2, finalize_ssl_socket, sizeof(ssl_socket_t), sizeof(ssl_socket_t));
  Field(block, 1) = (value)ret;
  CAMLreturn(block);
}

// Modifications by LT below:
//      "ocaml_ssl_invalid_socket"
//      "tnio_set_next_deadline"
//      deadlines in all blocking opeartions

// Non-blocking:
CAMLprim value ocaml_ssl_invalid_socket (value unit)
{
  CAMLparam1 (unit);
  CAMLreturn((value)NULL); // Simply that!
}

// Non-blocking:
extern value tnio_set_next_deadline  (value);

// The following function is NOT to establish a TCP connection; it's an SSL
// handshake with the server over an existing connection:
// XXX: BLOCKING, but safe -- at the point of blocking, movable values from
// the OCaml heap are no longer used:
// 
CAMLprim value ocaml_ssl_connect(value socket, value deadline)
{
  CAMLparam1(socket);
  if  ((void*)socket == NULL)
      invalid_argument ("Ssl.connect: Invalid socket");
  
  tnio_set_next_deadline (deadline);

  int  ret;
  SSL* ssl = ssl_socket_of_block(socket)->handler;

  if ((ret = SSL_connect (ssl)) != 1)
    raise_with_arg
        (*caml_named_value("ssl_exn_connection_error"),
        Val_int (SSL_get_error(ssl, ret)));

  CAMLreturn(Val_unit);
}

// The following function is a wrapper around "SSL_get_verify_result"  which
// *APPARENTLY* does not use any network communications, it uses the locally
// available certificate data:
// So probably non-blocking:
CAMLprim value ocaml_ssl_verify(value socket)
{
  CAMLparam1(socket);
  if  ((void*)socket == NULL)
      invalid_argument ("Ssl.verify: Invalid socket");
  
  long ans = SSL_get_verify_result(ssl_socket_of_block(socket)->handler);

  if (ans != 0)
  {
     if (2 <= ans && ans <= 32)
        raise_with_arg(*caml_named_value("ssl_exn_verify_error"),
                        Val_int(ans - 2)); // Not very nice, but simple
     else
        raise_with_arg(*caml_named_value("ssl_exn_verify_error"), Val_int(31));
  }
  CAMLreturn(Val_unit);
}

// BLOCKING!
CAMLprim value ocaml_ssl_write(value socket, value buffer, value start, value length, value deadline)
{
  CAMLparam4(socket, buffer, start, length);
  if  ((void*)socket == NULL)
      invalid_argument ("Ssl.write: Invalid socket");

  tnio_set_next_deadline (deadline);

  int ret, err;

  // XXX XXX XXX DON'T pass a (char*) from "buffer" into SSL_write, as the
  // latter is blocking, and the "Buffer" can move while writing is in process!
  // Make a local copy instead -- and on the heap, as there can be stack over-
  // flow in a TNIO thread. Yes, this is inefficient...
  int   len = Int_val (length);
  int  from = Int_val (start) ;
  SSL*  ssl = ssl_socket_of_block(socket)->handler;

  if  (from < 0 || len < 0 || ssl == NULL)
      invalid_argument ("Ssl.write: from, len, or ssl");

  char*   local_buff = malloc (len);
  memcpy (local_buff, String_val (buffer) + from, len);

  ret = SSL_write     (ssl, local_buff, len); // Blocking!
  free   (local_buff);
  
  err = SSL_get_error (ssl, ret);
  if (err != SSL_ERROR_NONE)
      raise_with_arg (*caml_named_value("ssl_exn_write_error"), Val_int(err));

  CAMLreturn(Val_int(ret));
}

// BLOCKING!
CAMLprim value ocaml_ssl_read(value socket, value buffer, value start, value length, value deadline)
{
  CAMLparam4(socket, buffer, start, length);
  if  ((void*)socket == NULL)
      invalid_argument ("Ssl.read: Invalid socket");

  tnio_set_next_deadline (deadline);

  int ret, err;

  // XXX XXX XXX Again, make a local copy of the "buffer"!
  int   len = Int_val (length);
  int  from = Int_val (start) ;
  SSL*  ssl = ssl_socket_of_block(socket)->handler;

  if  (from < 0 || len < 0 || ssl == NULL)
      invalid_argument ("Ssl.read: from, len, or ssl");

  char* local_buff = malloc (len);

  ret = SSL_read      (ssl, local_buff, len); // Blocking!
  err = SSL_get_error (ssl, ret);
  if (err != SSL_ERROR_NONE)
  {
     free (local_buff);
     raise_with_arg (*caml_named_value("ssl_exn_read_error"), Val_int(err));
  }
  memcpy (String_val (buffer) + from, local_buff, ret); // Not "len"!
  free   (local_buff);
  CAMLreturn(Val_int(ret));
}

// BLOCKING, but safe, as it does not use any OCaml-heap-located data at the
// point of blocking:
CAMLprim value ocaml_ssl_accept(value socket, value deadline)
{
  CAMLparam1(socket);
  if  ((void*)socket == NULL)
      invalid_argument ("Ssl.accept: Invalid socket");

  tnio_set_next_deadline (deadline);

  int ret;
  SSL* ssl = ssl_socket_of_block(socket)->handler;

  if ((ret = SSL_accept (ssl)) <= 0)
    raise_with_arg
            (*caml_named_value("ssl_exn_accept_error"),
             Val_int(SSL_get_error(ssl, ret)));

  CAMLreturn(Val_unit);
}

// BLOCKING, but safe: no OCaml-heap-based values at the point of blocking:
CAMLprim value ocaml_ssl_flush(value socket, value deadline)
{
  CAMLparam1(socket);
  if  ((void*)socket == NULL)
      invalid_argument ("Ssl.flush: Invalid socket");
  
  tnio_set_next_deadline (deadline);
  
  ssl_socket_t *ssl = ssl_socket_of_block(socket);

  BIO *bio = SSL_get_wbio(ssl->handler); // Clean-up by LT
  if(bio)
    (void)BIO_flush(bio);

  CAMLreturn(Val_unit);
}

// BLOCKING, but again apparently safe:
CAMLprim value ocaml_ssl_shutdown(value socket, value deadline)
{
  CAMLparam1(socket);
  // Simply do nothing for an "invalid_socket":
  if  ((void*)socket != NULL)
  {
    tnio_set_next_deadline (deadline);

    ssl_socket_t *ssl = ssl_socket_of_block(socket);

    if (SSL_shutdown(ssl->handler) == 0)
        SSL_shutdown(ssl->handler);
    // NB: we ignore negative (erroneous) return values here!
    close(ssl->fd);
  }
  CAMLreturn(Val_unit);
}

/* ======================================================== */
/*
   T.F.:
   Here, we steal the client_verify_callback function from
   netkit-telnet-ssl-0.17.24+0.1/libtelnet/ssl.c

   From the original file header:

   The modifications to support SSLeay were done by Tim Hudson
   tjh@mincom.oz.au

   You can do whatever you like with these patches except pretend that
   you wrote them.

   Email ssl-users-request@mincom.oz.au to get instructions on how to
   join the mailing list that discusses SSLeay and also these patches.
*/

#define ONELINE_NAME(X) X509_NAME_oneline(X, 0, 0)

/* Quick translation ... */
#ifndef VERIFY_ERR_UNABLE_TO_GET_ISSUER
#define VERIFY_ERR_UNABLE_TO_GET_ISSUER X509_V_ERR_UNABLE_TO_GET_ISSUER_CERT
#endif
#ifndef VERIFY_ERR_DEPTH_ZERO_SELF_SIGNED_CERT
#define VERIFY_ERR_DEPTH_ZERO_SELF_SIGNED_CERT X509_V_ERR_DEPTH_ZERO_SELF_SIGNED_CERT
#endif
#ifndef VERIFY_OK
#define VERIFY_OK X509_V_OK
#endif
#ifndef VERIFY_ERR_UNABLE_TO_GET_ISSUER
#define VERIFY_ERR_UNABLE_TO_GET_ISSUER X509_V_ERR_UNABLE_TO_GET_ISSUER_CERT
#endif

/* Need to think about this mapping in terms of what the real
 * equivalent of this actually is.
 */
#ifndef VERIFY_ROOT_OK
#define VERIFY_ROOT_OK VERIFY_OK
#endif

// Apparently non-blocking...

static int client_verify_callback(int ok, X509_STORE_CTX *ctx)
{
    char *subject, *issuer;
    int depth, error;
    char *xs;

    depth = ctx->error_depth;
    error = ctx->error;
    xs = (char *)X509_STORE_CTX_get_current_cert(ctx);

    subject = issuer = NULL;

    /* First thing is to have a meaningful name for the current
     * certificate that is being verified ... and if we cannot
     * determine that then something is seriously wrong!
     */
    subject=(char*)ONELINE_NAME(X509_get_subject_name((X509*)xs));
    if (subject == NULL)
      {
        ERR_print_errors_fp(stderr);
        ok = 0;
        goto return_time;
      }
    issuer = (char*)ONELINE_NAME(X509_get_issuer_name((X509*)xs));
    if (issuer == NULL)
      {
        ERR_print_errors_fp(stderr);
        ok = 0;
        goto return_time;
      }

    /* If the user wants us to be chatty about things then this
     * is a good time to wizz the certificate chain past quickly :-)
     */
    if (1)
      {
        fprintf(stderr, "Certificate[%d] subject=%s\n", depth, subject);
        fprintf(stderr, "Certificate[%d] issuer =%s\n", depth, issuer);
        fflush(stderr);
      }

    /* If the server is using a self signed certificate then
     * we need to decide if that is good enough for us to
     * accept ...
     */
    if (error == VERIFY_ERR_DEPTH_ZERO_SELF_SIGNED_CERT)
      {
        if (1)
          {
            /* Make 100% sure that in secure more we drop the
             * connection if the server does not have a
             * real certificate!
             */
            fprintf(stderr,"SSL: rejecting connection - server has a self-signed certificate\n");
            fflush(stderr);

            /* Sometimes it is really handy to be able to debug things
             * and still get a connection!
             */
            ok = 0;
            goto return_time;
          }
        else
          {
            ok = 1;
            goto return_time;
          }
      }

    /* If we have any form of error in secure mode we reject the connection. */
    if (!((error == VERIFY_OK) || (error == VERIFY_ROOT_OK)))
      {
        if (1)
          {
            fprintf(stderr, "SSL: rejecting connection - error=%d\n", error);
            if (error == VERIFY_ERR_UNABLE_TO_GET_ISSUER)
              {
                fprintf(stderr, "unknown issuer: %s\n", issuer);
              }
            else
              {
                ERR_print_errors_fp(stderr);
              }
            fflush(stderr);
            ok = 0;
            goto return_time;
          }
        else
          {
            /* Be nice and display a lot more meaningful stuff
             * so that we know which issuer is unknown no matter
             * what the callers options are ...
             */
            if (error == VERIFY_ERR_UNABLE_TO_GET_ISSUER)
              {
                fprintf(stderr, "SSL: unknown issuer: %s\n", issuer);
                fflush(stderr);
              }
          }
      }

 return_time: ;

    /* Clean up things. */
    if (subject)
      free(subject);
    if (issuer)
      free(issuer);

    return ok;
}

