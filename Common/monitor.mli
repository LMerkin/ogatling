(*pp camlp4r *)
(* vim:ts=4:syntax=ocaml
*)
(*===========================================================================*)
(*                               "monitor.mli":                              *)
(*                                                                           *)
(*  Starts a process and guards it against crash or dead-lock. Also provides *)
(*      THE "point of management" for signals and "clean exit" policies.     *)
(*                                                                           *)
(*                  (C) Explar Technologies Ltd, 2003--2005                  *)
(*===========================================================================*)
(*  "monitor_info":
    Parameters used for monitoring of a process:
*)
type monitor_info =
{
    last_activity    : unit -> float;
        (* Returns the time ("Unix.gettimeofday()"-compatible) of the last ac-
           tivity within the process being monitored.
        *)

    infinite_proc    : bool;
        (* If True, the monitored process is ALWAYS automatically restarted,
           even if it has terminated "successfully" (with code 0)
        *)

    max_blocking_time: float;
        (* Max time (sec) the monitored process is allowed to block *)

    log_heartbeats   : bool
        (* Do logging of "liveness" events -- for testing only   *)
};

(*  "run_info":
    Parameters for running a process (daemon or attached, monitored or not --
    all 4 combinations are possible). If monitoring is used, there are two
    processes: the monitor (parent) and the worker (child). Otherwise, there
    is just one (parent) process. In any case, the parent process may or may
    not be a daemon.
    A worker process typically consists of the initialisation phase (usually
    to be done in the priviledged mode) and the operational part (non-privil-
    eged with the given UID/GID for security reasons).     Switching to those
    UID/GID will occur in between the phases:
    If monitoring is used, the monitored process is guarded against crashes
    and dead-locks. If the process terminates normally (code 0), it is NOT
    re-started unless the "infinite_proc" flag was set. If it terminates with
    the "severe_error" RC, it is NEVER re-started. It is always killed and
    re-started if blocking is detected by the monitor (ie, "last_activity()"
    shows time earlier than "max_blocking_time" ago).
*)
type run_info =
{
    init_phase  : unit -> (unit->unit);
                                     (* Closure for the init phase. Returns a
                                        "clean-up" function to be invoked be-
                                        fore any controlled exit (via exit(3)
                                        or via a catchable signal).
                                     *)
    oper_phase  : unit -> unit;      (* Closure for the operational phase *)

    daemon_mode : bool;              (* If "False", no daemon is created (but
                                         a monitor can still be -- see below!
                                     *)
    with_monitor: option monitor_info;
                                     (* If "None", monitor is not created   *)
    severe_error: int;               (* If the worker process terminates with
                                        this RC, it is never restarted
                                     *)
    exn_error   : int;               (* RC for exceptions propagated out of the
                                        worker process body
                                     *)
    pid_file    : string;            (* To save PID of the worker process   *)
    run_as_user : string;            (* Symbolic or numerical UID  *)
    run_as_group: string;            (* Symbolic or numerical GID  *)

    log         : string -> unit     (* Logging function *)
};

(*  "run_process":
    A high-level process control function, runs the process according to "run_
    info" as described above:
*)
value run_process: run_info -> unit;

(*  Auxiliary Process Group Control Functions:
    These functions are typically used internally by this module, but can be
    used independently as well:
*)
(*  "become_pg_leader":
    The current process disconnects from the controlling terminal (if any)
    and becomes the leader in its process group:
*)
value become_pg_leader: unit -> unit;

(*  "kill_pg":
    Sends the KILL signal to the whole process group of the given process:
    Arg1: PID
*)
value kill_pg: int -> unit;

