(*pp camlp4r *)
(* vim:ts=4:syntax=ocaml
*)
(*===========================================================================*)
(*                               "gen_server.mli":                           *)
(*                      A Generic  Multi-Threading Server                    *)
(*                      (C) Explar Technologies Ltd, 2004                    *)
(*===========================================================================*)
(*-------------------*)
(* "server_config":  *)
(*-------------------*)
(*  'j is the type of the data to be processed by the workers
    's is the type of the internal worker's state
    'r is the type of the result generated:
*)
type server_config 'j 's 'r =
{
    (* The maximum total number of worker threads
       (including both engaged and idle ones):
    *)
    max_all_threads     : int;

    (* The maximum number of idle threads (those waiting for jobs):   *)
    max_idle_threads    : int;

    (* The minimum number of idle (spare) threads: *)
    min_idle_threads    : int;

    (* The number of worker threads to be created on server start-up: *)
    init_threads        : int;

    (* PRE-CONDITION:
       1 <= min_idle_threads <= init_threads <= max_idle_threads <=
            max_all_threads
    *)

    (* The priority of worker threads, if priority-based scheduling is used: *)
    worker_priority     : int;

    (* The stack size of each worker thread, in KBytes: *)
    worker_stack_k      : int;

    (* The maximum length of the queue of jobs submitted by clients, but not
       yet taken for processing, must be at least 1:
    *)
    max_queue_len       : int;

    (* The initial state of newly-created worker threads.   This function is
       guaranteed to be invoked WITHIN the thread, not before, so it is safe
       to e.g. create sockets in it:
    *)
    init_worker_state   : unit -> 's;

    (* Before a worker thread terminates (if so), the following function is
       applied to clean-up its state:
    *)
    cleanup_state       : 's   -> unit;

    (* If a tolerated error occurs within a worker thread and it is re-started,
       should the "init_worker_state" be used (if the following flag is True),
       or the last state before the error (if False):
    *)
    reset_state_on_error: bool;

    (* The actual processing function applied by worker threads. It returns
       the result and the updated state:
    *)
    proc_func           : 'j   -> 's -> float -> ('r * 's)
};

(*-----------------*)
(*  "server_t":    *)
(*  "server_info": *)
(*-----------------*)
type server_t    'j 's 'r = 'x; (* Private type *)

type server_info 'j 's 'r =
{
    (* The config used by this server: *)
    server_config    : server_config 'j 's 'r;

    (* The current statistics: *)
    curr_all_threads : int;
    curr_idle_threads: int;
    curr_queue_len   : int
};

(*---------------------*)
(*  Server Management: *)
(*---------------------*)
(* Server creation: *)
value create_server  : server_config 'j 's 'r  -> server_t    'j 's 'r;

(* Server info: *)
value get_server_info: server_t      'j 's 'r  -> server_info 'j 's 'r;

(* Different modes for server shutdown: *)
type shutdown_mode =
[   Graceful
    (* The server stops taking on new jobs, but all jobs currently in
       processing AND in the queue, will be honoured.
    *)

|   Discard_Queue
    (* The currently processed jobs will be finished, but those in the
       queue will be discarded, and error propagated to sync clients.
    *)

|   Emergency
    (* All threads will be killed, and the jobs queue discarded as above.
       XXX: There is currently no way to notify those clients whose jobs
       were already taken into processing, and lost.
    *)
];

(* Server shutdown:
   NB: this function does NOT wait for all server threads to terminate:
*)
value shutdown_server: server_t 'j 's 'r -> shutdown_mode -> unit;

(*--------------------*)
(*  Client Interface: *)
(*--------------------*)
(* Exceptions which can be propagated by the "call_server_" functions: *)
exception Jobs_Queue_Full;
exception Job_Discarded;

(* "call_server_sync":
    Submits a job (Arg2) to the server (Arg1), waits for its completion with
    the deadline, given (Arg3), receives and returns the result and the
    internal server's resp time:
*)
value call_server_sync : server_t 'j 's 'r -> 'j -> float -> ('r * float);

(* "call_server_async":
    Submits a job to the server and immediately returns the buffer area for
    the result,  the cond var  which can later be used for waiting for that
    that result, and a var to get the resp time from:
*)
value call_server_async:
    server_t 'j 's 'r -> 'j -> float ->
    ((ref (option 'r)) * Tnio.cond_var_t * (ref float));

(* "from_many_servers":
   Submit asynchronous requests to multiple servers, then wait for completion
   (successful or otherwise) of all of them.   Individual errors are ignored,
    successful results assembled into a single list (paired with the corresp
    internal resp times).
    There  must be  a 1-to-1 correspondence between the jobs in Arg1  and the
    servers in Arg2:
*)
value from_many_servers:
    list 'j -> list (server_t 'j 's 'r) -> float -> list ('r * float);

