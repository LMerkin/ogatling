(*pp camlp4r pa_ioXML.cmo *)
(* vim:ts=4:syntax=ocaml
*)
(*===========================================================================*)
(*                              "tnio_mechs.mli":                            *)
(*   Declaration of possible I/O Events and Context Management Mechanisms    *)
(*                      (C) Explar Technologies Ltd, 2004                    *)
(*===========================================================================*)
(*  The following types are factored into a separate module to provide IoXML
    serialisation for them:
*)
type io_events_mech =
[    IO_Events_Default
|    IO_Events_Select
|    IO_Events_Poll
|    IO_Events_EPoll   (* Linux 2.6+ only *)
];

