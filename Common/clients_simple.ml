(*pp camlp4r *)
(* vim:ts=4:syntax=ocaml
*)
(*===========================================================================*)
(*                          "clients_simple.ml":                             *)
(*    DNS and Transport Facilities for the Gatling/Lukol MetaSearch Engine   *)
(*                  (C) Explar Technologies Ltd, 2004                        *)
(*===========================================================================*)
value default_sock_buff_size = 256*1024;

(*========*)
(*  DNS:  *)
(*========*)
(*  For the Search Engines interface, all IP addresses of configure Search
    Engines are normally pre-resolved. If Search Engines behaviour changes
    (e.g., new DNS records, new re-directions, etc), an error is generated,
    and the list of configured Search Engine domains may need to be updated
    manually.
    For the Front-End HTTP servers which need to fetch 3rd-party pages for
    search terms highlighting, DNS resolvers may be created on demand,  or
    put into the state of the worker thraeds:
*)
(*------------------*)
(*  "mk_dns_agent": *)
(*------------------*)
(* DNS Agents Factory. Just creates a new DNS agent with a default config,
   except for local DNS servers (a non-[] list of them is typically required
   for firewalling reasons):
*)
value dns_agent_conf =  Dns_client.default_dns_agent_config ();

value mk_dns_agent:     list string -> Dns_client.dns_agent =
fun local_servers ->
    Dns_client.mk_dns_agent
    {(dns_agent_conf)
        with
        Dns_client.conf_dns_servers    = local_servers;
        Dns_client.conf_sock_buff_size = default_sock_buff_size
    };

(*-----------------*)
(* "dns_resolver": *)
(*-----------------*)
(* A resolver function for A records on top of any DNS agent, with outer-level
   re-iteration of failed attempts.   On success, always returns a non-[] list
   of results; otherwise, propagates an exception.  Furthermore, if the arg is
   a textual representation of an IP, that IP is returned in a singleton list:
*)
value n_attempts = 2;

value dns_resolver:
    Dns_client.dns_agent -> string -> list Unix.inet_addr =

fun agent dom ->
    (* Is it actually an IP already? *)
    let  its_ip =
        try  [Unix.inet_addr_of_string dom]
        with [_ -> []]
    in
    if   its_ip <> []
    then its_ip (* Just return it! *)
    else
    (* Do full resolution: *)

    let rec try_resolve: int -> list string -> list Unix.inet_addr =
    fun n_rem prev_errs ->
        if  n_rem <= 0
        then
            failwith ("Client_simple.default_resolver: DNS error(s) for \""^
                      dom^"\": "^(String.concat ", " prev_errs))
        else
            let (mb_res, mb_err) =
            try
                let dns_res = Dns_client.dns_enquiry
                              agent
                              Dns_client.T_A
                             (Dns_client.Dom dom)
                in
                let got_ips =
                    match dns_res with
                    [ Dns_client.IPs ips when ips <> []     -> ips

                    | _ -> failwith ("Client_simple.default_resolver: "^
                                     "Invalid or empty DNS response")
                    ]
                in
                (Some got_ips, None)
            with
                [hmm -> (None, Some (Misc_utils.print_exn hmm))]
            in
            match mb_res with
            [ Some res ->
              do {
                assert (res <> []);
                res  (* Got it! *)
              }
            | None ->
              do {
                Tnio.thread_sleep 2.0;
                try_resolve (n_rem-1) (prev_errs @ [Option.get mb_err])
              }
            ]
    in
    (* Try serveral times: *)
    try_resolve n_attempts [];


(*============*)
(*  HTTP(S):  *)
(*============*)
type  http_agent = Http_client.http_agent Tnio.tsock Ssl.socket;

(* HTTP Transport: *)
value http_transport:  Http_client.transport Tnio.tsock =
{
    Http_client.socket          =
    fun _ ->
        Tnio.socket
        ~kern_buff_size:default_sock_buff_size Unix.PF_INET Unix.SOCK_STREAM;

    Http_client.invalid_socket  = Tnio.invalid_tsock;
    Http_client.getsockname     = Tnio.getsockname;
    Http_client.bind            = Tnio.bind;
    Http_client.connect         = Tnio.connect;
    Http_client.read            = Tnio.recv;
    Http_client.write           = Tnio.send;
    Http_client.close           = fun s _ -> Tnio.close s
};

(* HTTPS Transport: *)
value https_transport: Http_client.transport Ssl.socket =
{
    Http_client.socket          =
        fun _ -> Ssl.mk_socket
                (*(Ssl.create_context Ssl.SSLv23 Ssl.Client_context);*)
                ~kern_buff_size:65536
                (Ssl.create_client_context Ssl.SSLv23);

    Http_client.invalid_socket  = Ssl.invalid_socket;

    Http_client.getsockname     =
        fun s -> Tnio.getsockname(Ssl.file_descr_of_socket s);

    Http_client.bind            =
        fun s -> Tnio.bind       (Ssl.file_descr_of_socket s);

    Http_client.connect         = Ssl.open_connection;
    Http_client.read            = Ssl.read;
    Http_client.write           = Ssl.write;
    Http_client.close           = Ssl.shutdown_connection
};

(* Standard HTTP agent config: *)
value http_agent_conf = Http_client.default_http_agent_config ();

(* Invalid URL, a placeholder: *)
value invalid_url     = "";

(* Standard buffer size for HTTP clients: *)
value http_buff_size  = http_agent_conf.Http_client.conf_buff_size;

(* HTTP Agents Factory: *)
value mk_http_agent: Dns_client.dns_agent -> http_agent =
fun dns_agent ->
    Http_client.mk_http_agent
       (dns_resolver dns_agent)
       (fun () -> Dns_client.close_dns_agent dns_agent)
        http_transport
       (Some https_transport)
        http_agent_conf;

(* Simple HTTP/HTTPS "Get" method: *)
value http_get:
    http_agent -> string -> float -> Http_client.http_res =
fun agent url deadline ->
    let req =
    {
        Http_client.http_method     = "GET";
        Http_client.full_url        = url;
        Http_client.extra_headers   = [];
        Http_client.cookies         = [];
        Http_client.form            = []
    }
    in
    Http_client.http_transaction agent req deadline;

