(*pp camlp4r *)
(* vim:ts=4:syntax=ocaml
*)
(*===========================================================================*)
(*                            "mmoz_server.ml":                              *)
(*   Web Server Encapsulating a CDuce-Based Rewriting Engine for MusicMoz    *)
(*                (C) Explar Technologies Ltd, 2004--2006                    *)
(*===========================================================================*)
(*-----------------*)
(*  Configuration: *)
(*-----------------*)
value no_restart_rc = 255;
value version       =
    "\tMusicMoz Server version 1.1.0  (2005-12-17)\n"^
    "\t(C) Explar Technologies Ltd, 2004--2006";

(* Command-line parm: Config File: *)

if Array.length Sys.argv <> 2
then
do {
    prerr_endline version;
    prerr_endline "PARAMETER: <ConfigFile.XML>";
    exit 2
}
else ();

(* Read in and parse the Config: *)

value conf: Lib_mmoz.mmoz_config =
try
    let cnnl = open_in Sys.argv.(1) in
    let strm = Stream.of_channel cnnl   in
    let xstr = IoXML.parse_xml   strm   in
    let res  = Lib_mmoz.xparse_mmoz_config xstr in
    do {
        close_in cnnl;
        res
    }
with
[hmm ->
    Misc_utils.fatal_error ~rc:no_restart_rc
        (Printf.sprintf "ERROR: Invalid Config File: \"%s\": %s\n%!"
            Sys.argv.(1) (Misc_utils.print_exn hmm))
];

(*-----------------------*)
(*  RTS Initialisation:  *)
(*-----------------------*)
value worker_prio    = 0;
value acceptor_prio  = 1;
value prio_levels    = 2;

Tnio.init (Tnio.Priority_Bands prio_levels);

(*-----------------------*)
(*  Verify the Licence:  *)
(*-----------------------*)
(*  XXX: Temporarily configured out due to discovered non-thread-safety
    of the OpenSSL binding:

Enforcement.verify_licence
    conf.Lib_mmoz.licence_file
    (fun () -> Secure_time.gmtime 15.0);
*)

(*--------------------*)
(*  Logging Facility: *)
(*--------------------*)
value logger: out_channel =
try
    let ch = open_out_gen  [Open_creat; Open_append] 0o640
        conf.Lib_mmoz.log_file
    in
    do {
        output_string ch "\n";
        ch
    }
with
[hmm ->
    Misc_utils.fatal_error ~rc:no_restart_rc
    (Printf.sprintf
        "ERROR: Cannot open the Log File \"%s\": %s\n%!"
            conf.Lib_mmoz.log_file (Misc_utils.print_exn hmm)
    )
];

(* The logging function, to be used by all components: *)

value log: string -> unit =
fun msg ->
    let timestamp = Date_time.http_time_stamp (Unix.gettimeofday ()) in
    let log_line  = Printf.sprintf "[%s] %s\n" timestamp msg         in
    do {
        output_string logger log_line;
        flush logger
    };

(*--------------*)
(*  DB access:  *)
(*--------------*)
(*  The DB is assumed to be a fast read-only storage,   typically implemented
    through an embedded server (e.g. SQLite3). In this case, there is no need
    to provide each server thread with a separate DB connection-- just one DB
    handle is enough. DB access is NOT protected by a lock, as we use non-pre-
    emptive thread scheduling.
*)
value dbh =
    try Ocamlodbc.connect
        conf.Lib_mmoz.db_dsn
        conf.Lib_mmoz.db_user_name
        conf.Lib_mmoz.db_passwd
    with [hmm ->
        let msg = Printf.sprintf "ERROR: Cannot open the MySQL DB \"%s\": %s"
                  conf.Lib_mmoz.db_dsn (Misc_utils.print_exn hmm)
        in
        do {
            log msg;
            Misc_utils.fatal_error ~rc:no_restart_rc msg
        }];

(*--------------*)
(*  Templating: *)
(*--------------*)
value template_cache = CamlTemplate.Cache.create ();

(*------------------*)
(*  Error Handling: *)
(*------------------*)
value err_msg  =
    "Sorry, you have submitted an invalid request,<br>"^
    "or a system error has occurred. Please try again.";

value err_page =
    "<html><head><title>ERROR</title></head><body bgcolor=\"white\"><center>"^
    err_msg^
    "</center></body></html>";

value http_error: Tnio.tsock -> int -> string -> unit =
fun ts _ _ ->
    let deadline = Unix.gettimeofday() +. 10.0
    in
    Http_server.display_page  ~full_response:True
    err_page Tnio.send ts deadline;

(*------------------------*)
(*  HTTP Request Handler: *)
(*------------------------*)
(* Handles (with de-multiplexing) all the requests: *)

value handler: Tnio.tsock -> Http_server.http_req -> unit -> float -> unit =
fun   ts req _ deadline ->
    (* Is it a request for the JPG file -- only one such file is currently
       supported per single configuration:
    *)
    let path  = req.Http_server.path  in
    if  ExtString.String.ends_with path "CDucePowered.jpg"
    then
        Http_server.display_page
            Cduce_powered.logo_jpg
            ~content_type:"image/jpeg" Tnio.send ts deadline
    else
    if  ExtString.String.ends_with path "OCamlPowered.jpg"
    then
        Http_server.display_page
            Ocaml_powered.logo_jpg
            ~content_type:"image/jpeg" Tnio.send ts deadline
    else
    (* Generic case: Category Request.  The Category Name is the "path"
       without the initial and the trailing "/". The initial "/" always
       exists, the trailing one is optional:
    *)
    let path'      = ExtString.String.lchop path in
    let categ_name =
        if   ExtString.String.ends_with path' "/"
        then ExtString.String.rchop     path'
        else path'
    in
    let qname = Lib_mmoz.quote_str categ_name in
    let req   =
        Printf.sprintf "select Body from Categories where Name='%s'" qname
    in
    let html  =
    try
        (* Get the XML data for this "categ_name" from the DB: *)
        match Ocamlodbc.execute dbh req
        with
        [ (0, [[body]]) ->
            (* Got it -- do CDuce re-writing: *)
            Rewrite_mmoz.main_prod body
        | (rc, lines)   ->
            (* SQL error or improper result : *)
            let clines  =
                String.concat "\n" (List.map (String.concat "\t") lines)
            in
            let err_msg =
                Printf.sprintf "SQL ERROR on:\n%s:\nRC=%d, Res=\n%s\n"
                               req rc clines
            in
            let () = log err_msg in
            ""
        ]
    with [hmm ->
        do {
            log (Printf.sprintf
                "Category request handling error for \"%s\": %s" categ_name
                (Misc_utils.print_exn hmm));
            ""
        }]
    in
    (* Also, update the hits counter of this Category: *)
    let () = ignore
             (Ocamlodbc.execute dbh
             (Printf.sprintf "update Categories set Hits=Hits+1 where Name='%s'"
             qname))
    in
    (* So: the HTML content: *)
    let cont0 =
        (* Error message if no HTML generated: *)
        if  html = ""
        then err_msg
        else html
    in
    (* Change all occurrences of  "dmoz.org" into "lukol.com",
       and (some) of "musicmoz.org" into "AskMusicMaster.com":
    *)
    let cont1 = Lib_mmoz.post_rw cont0
    in
    (* Install it in the template. The template depends on whether it's a
       front page, or otherwise:
    *)
    let tfile =
        if  path = "/" (* i.e., categ_name="" *)
        then conf.Lib_mmoz.front_template
        else conf.Lib_mmoz.template_file
    in
    let tmpl  = CamlTemplate.Cache.get_template template_cache tfile
    in
    (* NB: the "model" must not be global -- otherwise its content can
       be replaced by another thread while being sent by this thread!
    *)
    let model = Hashtbl.create 20   in
    let buff  = Buffer.create 10240 in (* Size OK? *)
    let pcomps= ExtString.String.nsplit categ_name "/" in
    do {
        (* Instantiate the template: *)
        Hashtbl.add  model "title" (CamlTemplate.Model.Tstr
                                   (Lib_mmoz.mk_title pcomps));
        Hashtbl.add  model "data"  (CamlTemplate.Model.Tstr cont1);
        CamlTemplate.merge tmpl model buff;

        (* Send the page to the client: *)
        Http_server.display_page
            (Buffer.contents buff)
            ~content_type:"text/html" Tnio.send ts deadline
    };

(* Handler Table: *)

value proc_funcs = Hashtbl.create 10;
Hashtbl.add proc_funcs "" handler;

(*----------------*)
(* Server Config: *)
(*----------------*)
value http_config: Http_server.server_config Tnio.tsock unit =
{
    (* TCP parms: *)
    Http_server.listen_on_ip        = Unix.inet_addr_of_string
                                      conf.Lib_mmoz.listen_on_ip;
    Http_server.listen_on_port      = conf.Lib_mmoz.listen_on_port;
    Http_server.listen_queue_len    = conf.Lib_mmoz.n_threads; (* Why not? *)
    Http_server.max_connections     = conf.Lib_mmoz.n_threads;
    Http_server.sock_buff_size      = conf.Lib_mmoz.sock_buff_size;

    (* Acceptor parms:    *)
    Http_server.acceptor_priority   = acceptor_prio;
    Http_server.acceptor_stack_k    = 512; (* Normally OK *)

    (* Worker Pool parms: *)
    Http_server.max_idle_threads    = conf.Lib_mmoz.n_threads; (* All idle *)
    Http_server.min_idle_threads    = 1;   (* Hmm? *)
    Http_server.init_threads        = conf.Lib_mmoz.n_threads; (* All init *)
    Http_server.worker_priority     = worker_prio;
    Http_server.worker_stack_k      = 512; (* OK? *)

    (* State management *)
    Http_server.init_socket         = fun ts _ _ -> ts;
    Http_server.finalise_socket     = fun _ _    -> ();
    Http_server.init_worker_state   = fun _      -> ();
    Http_server.cleanup_state       = fun _      -> ();
    Http_server.reset_state_on_error= False;
    Http_server.log_error           = Http_common.log_http_exn log;
    Http_server.reject_conn         = fun _      -> ();

    (* I/O Functions to be used: *)
    Http_server.read_func           = Tnio.recv;
    Http_server.write_func          = Tnio.send;

    (* Application Logic: *)
    Http_server.proc_funcs          = proc_funcs;

    (* Temporal parms: *)
    Http_server.session_timeout     = 600.0;    (* 10 minutes -- OK? *)
    Http_server.max_pers_reqs       = 60;
    Http_server.proc_timeout        = fun _ -> 10.0;
    Http_server.http_error          = http_error;
    Http_server.uploaded_dir        = ""        (* Not used *)
};

(*--------------------*)
(* Create the Server: *)
(*--------------------*)
(* Returns a clean-up function to be used when a SIGHUP is received, to close
   the server socket properly:
*)
value init_server: unit -> (unit -> unit) =
fun () ->
    (* Initialise the HTTP server, binding to the IP/Port specified: *)
    let srv =
    try
        Http_server.create_server http_config
    with
    [hmm ->
    do {
        log (Printf.sprintf "ERROR: Cannot initialise the HTTP server: %s"
            (Misc_utils.print_exn hmm));
        Tnio.exit no_restart_rc
    }]
    in
    (* The clean-up function: *)
    fun _ ->
    do {
        Tcp_server.shutdown_server srv Gen_server.Discard_Queue;
        log "HTTP server shut down"
    };

(*----------------------------------------*)
(*  Self-Testing: The Monitor Interface:  *)
(*----------------------------------------*)
(*  Testing parms (XXX: currently non-configurable): *)
value test_url    =
    Printf.sprintf
    "http://%s:%d/Bands_and_Artists/A/Aguilera,_Christina"
    conf.Lib_mmoz.listen_on_ip
    conf.Lib_mmoz.listen_on_port;

value test_period  = 300.0;
value test_timeout =  30.0;

value test_client  =
    Clients_simple.mk_http_agent (Clients_simple.mk_dns_agent []);

value last_activity: unit -> float =
fun () ->
    (* Perform a self-request: *)
    let now     = Unix.gettimeofday ()      in
    let deadline= now +. test_timeout       in
    let ancient = now -. 1.0 -. test_period in
    try
    do {
        ignore (Clients_simple.http_get test_client test_url deadline);
        (* Request successful -- we are at the last liveness instant: *)
        deadline
    }
    with[_ -> ancient];

(*---------------*)
(*  Off we go!.. *)
(*---------------*)
value minfo =
{
    Monitor.last_activity     = last_activity;
    Monitor.infinite_proc     = True;
    Monitor.max_blocking_time = test_period;
    Monitor.log_heartbeats    = False
};

(* NB: here "daemon_mode" and "with_monitor" are equivalent: *)
value rinfo =
{
    Monitor.init_phase        = init_server;
    Monitor.oper_phase        = Tnio.threads_scheduler; (* Just that! *)
    Monitor.daemon_mode       = conf.Lib_mmoz.with_monitor;

    Monitor.with_monitor      = if  conf.Lib_mmoz.with_monitor
                                then Some minfo
                                else None;
    Monitor.severe_error      = no_restart_rc;
    Monitor.exn_error         = 1;
    Monitor.pid_file          = conf.Lib_mmoz.pid_file;
    Monitor.run_as_user       = conf.Lib_mmoz.run_as_user;
    Monitor.run_as_group      = conf.Lib_mmoz.run_as_group;
    Monitor.log               = log
};

Monitor.run_process rinfo;

