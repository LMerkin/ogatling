(*pp camlp4r *)
(* vim:ts=4:syntax=ocaml
*)
(*===========================================================================*)
(*                                "mk_mmdb":                                 *)
(*                                                                           *)
(*    Parses the complete MusicMoz XML file, inserts its Categories into     *)
(*                             an ODBC-managed DB                            *)
(*                                                                           *)
(*                     (C) Explar Technologies Ltd, 2004                     *)
(*===========================================================================*)
open ExtString;

if  Array.length Sys.argv <> 5
then
do {
    prerr_endline 
    "PARAMETERS: <MMoz_File.xml> <ODBC_DSN> <DB_UserName> <DB_PassWord>";
    exit 2
}
else ();

value xfd      = open_in   Sys.argv.(1);

value db_handle= Ocamlodbc.connect
                 Sys.argv.(2) Sys.argv.(3)Sys.argv.(4);

(* Error checking: *)
value db_error: bool -> string -> string -> unit =
fun check sql_req err_msg ->
    if  check
    then
        Misc_utils.fatal_error
        (Printf.sprintf "DB ERROR on \"%s\": %s" sql_req err_msg)
    else
        (); (* Ignore the error *)

(* SQL quieries -- no data retrieval: *)
value exec_nd: string -> ~check:bool -> unit =
fun sql_req ~check ->
    let rc = fst (Ocamlodbc.execute db_handle sql_req) in
    if  rc = 0
    then ()
    else db_error check sql_req (string_of_int rc);


(* Create the single table: "Categories": *)
exec_nd  "drop   table Categories" ~check:False;
exec_nd ("create table Categories " ^
         "(Name varbinary(255) primary key, " ^
         "Body mediumtext not null default '', Hits int not null default 0)")
         ~check:True;

(* Read the data in, and store them in the DB. The strings of each <category>
   are accumulated in the "buff":
*)
value buff     = Buffer.create 4096;
value cname_re = Pcre.regexp ~study:True " name=\"(.*?)\"";

value invalid_categ_name = "__INVALID_CATEGORY_NAME__"; (* Not ""! *)

value rec store_data: string -> unit =
fun in_categ_name ->
    let (line, eof) =
        try  (input_line xfd, False)
        with [End_of_file ->  (invalid_categ_name, True)]
    in
    if  eof
    then ()
    else
    let in_categ_name' =
        if  in_categ_name <> invalid_categ_name
        then
        do {
            Buffer.add_string buff line;
            Buffer.add_string buff "\n";

            (* Leaving the Category now? *)
            if  String.starts_with line "</category"
            then
            do {
                (* Save the "buff" content in the DB: *)
                let key  = Lib_mmoz.quote_str in_categ_name          in
                let body = Lib_mmoz.quote_str (Buffer.contents buff) in
                try
                    exec_nd
                        (Printf.sprintf
                        "insert into Categories values ('%s','%s',0)"
                        key body)
                    ~check:True
                with
                [hmm -> prerr_endline (Misc_utils.print_exn hmm)];

                Buffer.reset  buff;
                invalid_categ_name
            }
            else in_categ_name
        }   
        else
            if  String.starts_with line "<category"
            then
            do {
                Buffer.add_string  buff line;
                Buffer.add_string  buff "\n";
                (* Get the name of this Category: *)
                let cname =
                    (Pcre.extract ~rex:cname_re ~full_match:False line).(0)
                in
                do {
                    prerr_endline cname;
                    cname
                }
            }
            else invalid_categ_name
    in
    store_data in_categ_name';

store_data invalid_categ_name;

close_in  xfd;
Ocamlodbc.disconnect db_handle;
exit 0;

