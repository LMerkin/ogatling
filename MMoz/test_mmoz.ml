(*pp camlp4r *)
(* Vim:ts=4:syntax=ocaml
*)
(*===========================================================================*)
(*                              "test_mmoz.ml":                              *)
(*               Simple Test of MusicMiz Re-Writing Engine                   *)
(*                   (C) Explar Technologies Ltd, 2004                       *)
(*===========================================================================*)
if  Array.length Sys.argv <> 2
then
do {
    prerr_endline "PARAMETER: <CategoryFile.xml>";
    exit 1
}
else ();

Rewrite_mmoz.main_test Sys.argv.(1);
exit 0;

