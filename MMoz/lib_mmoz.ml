(*pp camlp4r pa_ioXML.cmo *)
(* vim:ts=4:syntax=ocaml
*)
(*===========================================================================*)
(*                                "lib_mmoz.ml":                             *)
(*  Some Auxiliary Functions for the CDuce-based MusicMoz ReWriting Engine   *)
(*                      (C) Explar Technologies Ltd, 2004                    *)
(*===========================================================================*)
(*--------------------*)
(*  The Config Type:  *)
(*--------------------*)
(*  XML serialisation functions are automatically created for this file:  *)
type mmoz_config =
{
    db_dsn          : string;   (* ODBC DSN for MusicMoz XML Categs DB    *)
    db_user_name    : string;
    db_passwd       : string;

    n_threads       : int;      (* Max number of simultaneous connections *)
    listen_on_ip    : string;   (* Server IP   to bind to *)
    listen_on_port  : int;      (* Server port to bind to *)
    sock_buff_size  : int;      (* Size of the Kernel RCV/SND buffer      *)

    run_as_user     : string;   (* If started as "root", switch to these  *)
    run_as_group    : string;   

    licence_file    : string;
    template_file   : string;   (* Template for the output page *)
    front_template  : string;   (* Template for the front  page *)
    log_file        : string;
    pid_file        : string;   (* File to save the PID of the server (NOT
                                   the monitor; if "", PID is not saved
                                *)
    with_monitor    : bool      (* Run as a daemon, and use a monitor to re-
                                   start the server in case of crash or blo-
                                   king. If not set,  neither the daemon nor
                                   the monitor are created
                                *)
};

(*--------------*)
(*  "rw_date":  *)
(*--------------*)
(*  If the date is given as "YYYY.MM.DD", it is re-written as "YYYY-Mon-DD".
    An exception is propagated for invalid data:
*)
value date_re = Pcre.regexp ~study:True
    "([0-9]{4})\\.([0-9]{1,2})(\\.([0-9]{1,2}))?";

value rw_date: string -> string =
fun dt ->
try
    let dcomps = Pcre.extract ~rex:date_re ~full_match:False dt in
    let yyyy   = dcomps.(0) in
    let mm     = dcomps.(1) in
    let mn     = int_of_string mm              in
    let ()     = assert (mn >= 1 && mn  <= 12) in
    let mon    = Date_time.months_short.(mn-1) in

    let dd     = if  Array.length dcomps = 4
                 then dcomps.(3)
                 else ""                       in
    if  dd <> ""
    then Printf.sprintf "%s-%s-%s" yyyy mon dd
    else Printf.sprintf "%s-%s"    yyyy mon
with
[_ -> dt];

(*---------------------*)
(*  "nice_path_comp":  *)
(*---------------------*)
value nice_path_comp: string -> string =
fun p ->
do {
    for i = 0 to (String.length p)-1
    do {
        if  p.[i] = '_' then p.[i]:= ' ' else ();
    };
    p
};

(*---------------*)
(*  "mk_title":  *)
(*---------------*)
value rec list_after: string -> list string -> list string =
fun key comps ->
    match comps with
    [ []     -> []
    | [h::t] ->
      if  h = key
      then t
      else list_after key t
    ];

value last2: list string -> list string =
fun ls ->
    (* Can actually return a list of length less than 2: *)
    List.rev (ExtList.List.take 2 (List.rev ls));

value mk_title:  list string -> string =
fun comps ->
    (* If it is a "Bands_and_Artists" sub-category: the title is made by
       its trailing part:
    *)
    let name_parts =
        let  ba = list_after "Bands_and_Artists" comps in
        if   List.length ba >= 2
        then List.tl ba
        else
        if  comps = [] || comps = [""]
        then ["AskMusicMaster Directory"]
        else last2  comps
    in
    String.concat " " (List.map nice_path_comp name_parts);

(*--------------*)
(*  "post_rw":  *)
(*--------------*)
(* Replace URLs as follows, also replace some encoded HTML tags by their real
   values.  After that, any special symbols INSIDE tags are replaced by their
   textual equivalents:
*)
value post_re =
    Pcre.regexp ~study:True ~flags:[`CASELESS]
    (*
    ("(www\\.dmoz\\.org)|(dmoz\\.org)|"^
     "(www\\.musicmoz\\.org)|(musicmoz\\.org)|"^
     "(&lt;)|(&gt;)|(&amp;[a-z]+;)"
    );
    *)
    ("(www\\.dmoz\\.org)|(dmoz\\.org)|"^
     "(&lt;)|(&gt;)|(&amp;[a-z]+;)"
    );

value tag_re  = Pcre.regexp ~study:True ~flags:[`CASELESS] "<[^<>]*?>";
value quot_re = Pcre.regexp ~study:True ~flags:[`CASELESS] "&[^;]+;";

value post_rw: string -> string =
fun s0 ->
    let s1 =
        Pcre.substitute ~rex:post_re
        ~subst:
        (fun s2 ->
            match String.lowercase s2 with
            [ "www.dmoz.org"
            | "dmoz.org"     -> "www.Lukol.com/Top"
            (*
            | "www.musicmoz.org"
            | "musicmoz.org" -> "www.AskMusicMaster.com"
            *)
            | "&lt;"         -> "<"
            | "&gt;"         -> ">"
            | _   ->
                (* Remove one level of quoting: *)
                "&"^(ExtString.String.slice ~first:5 s2)
            ]
        ) s0
    in
    (* XXX: however, /img URLs still need to be written with "musicmoz.org",
       as we do not yet have any images on our own site:
    let  s2 = snd (ExtString.String.replace s1
                  "<img src=\"/img/"  "<img src=\"http://musicmoz.org/img/")
    in
    let  s3 = snd (ExtString.String.replace s2
                  "www.AskMusicMaster.com/img" "musicmoz.org/img")
    in
    *)
    let s3 = s1 in
    Pcre.substitute ~rex:tag_re
    ~subst:
    (fun s4 ->
        Pcre.substitute ~rex:quot_re
        ~subst:
        (fun s5 ->
            match String.lowercase s5 with
            [ "&quot;" -> "\""
            | "&amp;"  -> "&"
            | "&nbsp;" -> " "
            | _        -> ""
            ]
        ) s4
    ) s3;

(*--------------------*)
(*  "full_res_name":  *)
(*--------------------*)
value full_res_name: string -> string =
fun
    ["amg"                -> "AMG"
    |"artistdirect"       -> "ArtistDirect"
    |"christianitytoday"  -> "ChristianityToday"
    |"cmt"                -> "CMT"
    |"dmoz"               -> "Lukol" (* !!! *)
    |"imdb"               -> "IMDB"
    |"mtv"                -> "MTV"
    |"muchmusic"          -> "MuchMusic"
    |"musicbrainz"        -> "MusicBrainz"
    |"nme"                -> "NME"
    |"rollingstone"       -> "RollingStone"
    |"songfacts"          -> "SongFacts"
    |"topix"              -> "Topix"
    |"vh1"                -> "VHL"
    |"wikipedia"          -> "Wikipedia"
    |"yahoolaunch"        -> "YahooLaunch"
    |_                    -> "" (* Impossible *)
    ];

(*-----------------*)
(*  "check_link":  *)
(*-----------------*)
value check_link: string -> string -> string =
fun name link ->
    if  name = "dmoz" && ExtString.String.ends_with link "/"
    then ExtString.String.rchop link
    else link;

(*----------------*)
(*  "quote_str":  *)
(*----------------*)
(* Quoting of strings to be used in SQL requests: *)
value quote_re = Pcre.regexp ~study:True "'";

value quote_str: string -> string =
fun s ->
    Pcre.replace ~rex:quote_re ~templ:"''" s;

(*--------------------*)
(*  "format_n_cols":  *)
(*--------------------*)
(*  Takes a single list and the number of columns to format it into. Returns
    a list of corresponding ROWS; formatting is column-first:
*)
value format_n_cols: list Cduce_lib.Value.t  ->
               list (list Cduce_lib.Value.t) =
fun xs ->
    let len = List.length xs in
    let n_cols =
        if  len <= 25
        then 1
        else
        if  len <= 59
        then 2
        else 3
    in
    let n_rows = if   len mod n_cols = 0
                 then len / n_cols
                 else len / n_cols + 1
    in
    let rows   = Array.make n_rows [] in
    let rec do_format =
    fun this_row rem_xs ->
        match rem_xs with
        [ []     -> () (* All done, "rows" has been filled in *)
        | [h::t] ->
          do {
            rows.(this_row) := [h :: rows.(this_row)];
            do_format ((this_row + 1) mod n_rows) t
          }
        ]
    in
    let () = do_format 0 xs
    in
    (* After that, all rows are done, but in the reverse order of elements;
       convert them into a list, and re-order:
    *)
    List.map (fun row -> List.rev row) (Array.to_list rows);

(*------------*)
(*  "split":  *)
(*------------*)
(*  Just a wrapper around ExtString.String.nsplit -- for some reason, the
    latter is rejected by CDuce:
*)
value split: string -> string -> list string =
    ExtString.String.nsplit;

