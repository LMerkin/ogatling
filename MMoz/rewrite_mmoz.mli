(*pp camlp4r *)
(* vim:ts=4:syntax=ocaml
*)
(*===========================================================================*)
(*                              "rewrite_mmoz.mli":                          *)
(*               OCaml Interface to the CDuce MusicMoz ReWriter              *)
(*                      (C) Explar Technologies Ltd, 2004                    *)
(*===========================================================================*)
(* The following function is invoked wfrom within the Web server;
   arg: XML string representing a MusicMoz <category>:
*)
value main_prod: string -> string;

(* The following function is for testing only. Arg: The file name containing
   the top-level <musicmoz> element:
*)
value main_test: string -> unit;

