(*pp camlp4r *)
(* vim:ts=4:syntax=ocaml
*)
(*===========================================================================*)
(*                              "red_black.mli":                             *)
(*          Purely-Functional  Maps  Based  on  Red-Black  Trees             *)
(*                L.A.Merkin, IMPARTIAL Project, 2004--2005                  *)
(*===========================================================================*)
(*-------------------------*)
(*  The signature of Keys: *)
(*-------------------------*)
module type ORDERED =
sig
    type  t = 't;
    value compare: t -> t -> int;
end;

(*-------------------------*)
(*  The signature of Maps: *)
(*-------------------------*)
(*  NB: all search operations yield an [option] result, rather than raising
    an exception if the element in question was not found.
*)
module type MAP =
sig
    (* [k]   : type of keys;
       ['a]  : type of elements;
       [m 'a]: type of maps themselves:
    *)
    type  k     = 'k;
    type  m 'a  = 'm;

    value empty     : m 'a;
    value is_empty  : m 'a -> bool;

    value singltn   : k    -> 'a -> m 'a;
    value is_singltn: m 'a -> bool;
    value get_single: m 'a -> option (k * 'a);

    value add       : m 'a -> k -> 'a -> m 'a;
    value mem       : m 'a -> k -> bool;
    value find      : m 'a -> k -> option (k * 'a);
    value remove    : m 'a -> k -> m 'a;
    value take      : m 'a -> k -> (option 'a * m 'a);

    value find_min  : m 'a -> option (k * 'a);
    value find_max  : m 'a -> option (k * 'a);
    value remove_min: m 'a -> m 'a;
    value remove_max: m 'a -> m 'a;
    value take_min  : m 'a -> (option (k * 'a) * m 'a);
    value take_max  : m 'a -> (option (k * 'a) * m 'a);

    type  trav_dir  = [Forward | Backward | Stop_Now | Left_Only | Right_Only];

    value traverse  : (k -> 'a -> 'b -> ('b * trav_dir)) -> m 'a -> 'b -> 'b;
    value fold      : (k -> 'a -> 'b ->  'b)             -> m 'a -> 'b -> 'b;
    value iter      : (k -> 'a -> unit)                  -> m 'a -> unit;

    (* Compared to the standard [Map] module, we do not provide functions
       such as [iter], [map], [mapi], [compare], [equal], as they are not
       needed for our purposes.
    *)
end;

(*------------------*)
(*  Red-Black Maps: *)
(*------------------*)
module Map (Keys: ORDERED): (MAP with type k = Keys.t);

