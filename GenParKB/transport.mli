(*pp camlp4r *)
(* vim:ts=4:syntax=ocaml
*)
(*===========================================================================*)
(*                              "transport.mli":                             *)
(*           Communication Interface for Parallel Re-Writing Systems         *)
(*                     L.A.Merkin, IMPARTIAL Project, 2005                   *)
(*===========================================================================*)
(*-------------*)
(* "comp_env": *)
(*-------------*)
(* Opaque type over the type of messages ('m) being exchanged between the
   computational nodes. For convenience, the same environment may contain
   application-specific info ('a type):
*)
type comp_env 'm 'a = 'c;
type node_id        = 'i;

(* "init": *)
value init     : Config.system_conf -> 'a -> comp_env 'm 'a;

(* "send":
   Sends a message to a single destination node:
*)
value send     : comp_env 'm 'a   -> 'm -> node_id -> unit;
                                     (* Msg  Dst *)
(* "send_all":
   Sends a multicast message to all nodes except the curr (sender) one:
*)
value send_all : comp_env 'm 'a ->  'm -> unit;

(* "recv": *)
value recv     : comp_env 'm 'a ->
                 ('m -> node_id -> bool) -> ('m * node_id);
                 (* Msg  Src    DoRecv?      Msg  Src   *)

(* "log":
   Not really a transport/communication function, but provided here for
   convenience. Logs msgs with Time and Node stamps:
*)
value log     : comp_env 'm 'a -> string -> unit;

(*----------------------*)
(*  Access to Env Data: *)
(*----------------------*)
(* "n_nodes":    *)
value n_nodes  : comp_env 'm 'a -> int;

(* "node_id" :
   "node_num":
   NodeID by number (0 .. n_nodes-1), and vice versa:
*)
value node_id  : int -> node_id;
value node_num : node_id -> int;

(* "n_threads" (for a particular node): *)
value n_threads: comp_env 'm 'a -> node_id -> int;

(* "this_node": *)
value this_node: comp_env 'm 'a -> node_id;

(* "next_node":
   Returns the ID if the "next-to-this" node (in the enumeration consistent
   with [node_id], modulo [n_nodes]):
*)
value next_node: comp_env 'm 'a -> node_id;

(* "appl_data": *)
value appl_data: comp_env 'm 'a -> 'a;

