(*pp camlp4r *)
(* vim:ts=4:syntax=ocaml
*)
(*===========================================================================*)
(*                                "kb_par.ml":                               *)
(*     Parallel Implementation of the Knuth-Bendix Completion Algorithm      *)
(*                L.A.Merkin, IMPARTIAL Project, 2003--2005                  *)
(*===========================================================================*)
module ParKB (CP: Crit_pairs.PAIRS) =
struct
    (*=====================================*)
    (*  Message Types and Communications:  *)
    (*=====================================*)
    (* To do a non-local reduction: the result is then forwarded to the next
       node;  the flag is set indicating whether the rule  has actually been
       reduced. Tuple: (Rule, OriginatingNode, ReducedFlag):
    *)
    type remote_reduction = (CP.RT.rewrite_rule * Transport.node_id * bool);

    (* With the current implementation, message size is limited  (to 64K actu-
       ally), so large aggregate types should not be wrapped into single msgs,
       nor should be function or object types:
    *)
    type req_id = int;

    type msg    =
    [ (* To propagate a new rule for construction of pairs on all nodes:   *)
      New_Rule      of CP.RT.rewrite_rule

      (* To do a remote reduction as indicated above: *)
    | Reduce_Rule   of remote_reduction

      (* Is the remote node currently idle? Req/Resp: *)
    | Is_Idle_Req   of req_id
    | Is_Idle_Resp  of req_id and bool

      (* Request all rules from the remote nodes -- at the end, Req/Resps: *)
    | Get_All_Rules of req_id
    | Return_Rule   of req_id and CP.RT.rewrite_rule
    ];

    (* Application-specific data to be held in the env -- completion strategies
       configuration:
    *)
    type appl_data =
    {
        (* Term enumeration function: *)
        enumf:     CP.RT.term_enumf;

        (* "mr_period":
           Period of mutual reductions; not used if <= 0:
        *)
        mr_period: int;

        (* "mr_thresh":
           Threshold "t" in (0.0--1.0) for triggering mutual reductions: if
           a newly-inserted rule has the enum of its LHS which is less than
           (t * MAX_CURR_LHS), then mutual reductions are performed:
        *)
        mr_thresh: float
    };

    (* The Env: *)
    type cenv = Transport.comp_env msg appl_data;

    (*==============================*)
    (*  Critical Pairs Completion:  *)
    (*==============================*)
    (*  Completion thread(s) running on each node take pairs from [curr_pairs]
        and create new rules by completing each pair.  The new rules must then
        be reduced; this is done first modulo [curr_rules], then the new rules
        are sent to other nodes (if any) for further reductions. On each node,
        a thread servicing reduction requests from other nodes is running. XXX:
        the following non-functional global vars are used -- no need to protect
        them with mutexes, as TNIO threads are non-pre-emptive:
    *)
    value curr_rules: ref CP.RT.rule_set          = ref (CP.RT.empty_rset);
    value curr_pairs: ref CP.pairs_q              = ref (CP.empty_pqueue );

    (*  A queue of newly-created rules to be reduced modulo [curr_rules] on the
        curr node. Actually contains [remote_reduction] elements, as we need to
        know where to send the rules further.   The queue is normally short (on
        average, N elements, where N is the number of nodes),  and is processed
        at once, so just a list would do here (rather than a priority queue):
    *)
    value todo_rules: ref (list remote_reduction) = ref [];

    (*-----------*)
    (*  "init":  *)
    (*-----------*)
    (* Invoked on each node (the input file is assumed to be visible  via e.g.
       NFS). Creates the communication enviroment and the initial set of rules
       (separate for each node -- extracted stripe-wise from the whole initial
       set, for load balancing). Also creates the initial set of critical pairs
       between the rules stored on this node.
       XXX: side-effect: initialising "curr_rules" and "curr_pairs": VERY BAD:
    *)
    value init: string -> cenv =
    fun fn ->
        (* Read and parse the input: *)
        let kbi = Config.read_kb_input fn in
        let (ti, rules1) =
            CP.RT.parse_rules kbi.Config.symbols kbi.Config.rules
        in
        let adt =
        {
            mr_period = kbi.Config.kb_conf.Config.mr_period;
            mr_thresh = kbi.Config.kb_conf.Config.mr_thresh
        }
        in
        let cenv = Transport.init kbi.Config.system_conf adt   in
        in
        (* We need to do inter-reductions of rules before starting the comple-
           tion process. Currently, XXX, we do it on each node -- unnecessary
           replication of effort,  but this operation is in most cases rather
           simple anyway, and it's easier to implement it this way than do it
           really with distributed communications. The result is deterministic
           and will be the same on each node, so subsequent striping should be
           correct globally:
        *)
        let enumf  = CP.RT.mk_enumf ti.CP.RT.term_order in
        let rules2 = CP.RT.reduce_wrt_each_other rules1 in

        (* Now, select the stripe of "rules" to be stored locally: rules with
           numbers (mod n) = i will be selected, where "n" is the total number
           of nodes, "i" is the index of the curr node:
        *)
        let n    = Transport.n_nodes   cenv in
        let this = Transport.this_node cenv in

        (* NB: the local slice returned by "mk_slice" should better be an
           array rather than a list -- this facilitates construction of Pairs
           below; this function also returns a list of all other (non-local)
           rules:
        *)
        let rec mk_slice:
            list CP.RT.rewrite_rule -> int ->
            list CP.RT.rewrite_rule ->
            list CP.RT.rewrite_rule ->
            ((array CP.RT.rewrite_rule) * (list CP.RT.rewrite_rule)) =
        fun rs i local other ->
            match rs with
            [ []     ->
                (* No more input, return what we got: *)
                (Array.of_list local, other)
            | [h::t] ->
                let (local', other') =
                    if   Transport.node_id (i mod n) = this
                    then ([h::local], other)
                    else (local, [h::other])
                in
                mk_slice t (i+1) local' other'
            ]
        in
        let (this_slice, other_rules) = mk_slice rules2 0 [] [] in
        let ()  =
        do {
            (* Convert "this_slice" into a set and initialise "curr_rules": *)
            curr_rules.val :=
                Array.fold_left
                (fun curr r -> CP.RT.insert_rule curr r)
                CP.RT.empty_rset this_slice;

            (* Now pairs -- first, between elements of "this_slice": *)
            let nr = Array.length this_slice in
            let rec scan_rows: int -> CP.pairs_q -> CP.pairs_q =
                fun i ci ->
                    let rec scan_cols: int -> CP.pairs_q -> CP.pairs_q =
                        fun j cj ->
                            if  j < nr
                            then
                                (* Create and insert a new pair: *)
                                let np = CP.RT.mk_pair
                                         this_slice.(i) this_slice.(j)
                                in
                                CP.insert_pair cj np
                            else
                                cj (* The col is done *)
                    in
                    (* Only do cols such that j >= i; reflexive pairs (j = i)
                       are created as well, as they may participate in genera-
                       tion of useful critical pairs:
                    *)
                    scan_cols i ci
            in
            let pairs1 = scan_rows 0 CP.empty_queue in

            (* Also, create pairs between elements of "this_slice" and "other_
               rules", i.e., between the local and non-local rules.   As each
               node will do this, we must avoid creation of duplicate pairs by
               applying the folloiwng criterion:
               LHS (Local) > RHS (NonLocal)
               before calling "mk_pair" (so that this function would NOT actu-
               ally swap the pair elements).  NB: we use > rather than >=, as
               reflexive pairs are always created locally;
               Is it more efficient to do a larger List.fold in the outer loop?
            *)
            let pairs2 =
                List.fold_left
                (fun curr1 rhs ->
                    Array.fold_left
                    (fun curr2 lhs ->
                        if   CP.cmp_rules lhs rhs > 0
                        then CP.insert_pair curr2 (CP.RT.mk_pair lhs rhs)
                        else curr2
                    )
                    curr1 this_slice
                )
                pairs1 other_rules
            in
            (* Save all the constructed pairs: *)
            curr_pairs.val := pairs2
        }
        in
        (* Just for safety -- in case if the node is re-initialised: *)
        in
        let () = todo_rules.val := [] in
        cenv;

    (*--------------*)
    (*  "is_idle":  *)
    (* "all_idle":  *)
    (*--------------*)
    (*  The curr node is idle iff it has no pending critical pairs, and
        no rules in the "todo" list:
    *)
    value is_idle : unit -> bool =
    fun () ->
        (CP.is_empty_pqueue curr_pairs.val) && (todo_rules.val = []);

    (* "all_idle":
       If the curr node is idle, makes a request to oill ther nodes, collects
       all the results:
    *)
    value all_idle: cenv -> bool =
    fun cenv ->
        let n_nodes   = Transport.n_nodes   cenv in
        let this_node = Transport.this_node cenv in
        if  is_idle ()
        then
            if  n_nodes >= 2
            then
            do {
                (* Multi-cast the request: *)
                let req_id = Random.bits ()                                  in
                let ()  = Transport.send_all cenv (Is_Idle_Req req_id)       in

                (* Collect the responses. Note that even if we receive a nega-
                   tive response, so that the answer (boolean AND) is already
                   known, we still need to get all messages, to prevent queue
                   over-flow:
                *)
                let rec get_all_resps: int -> bool -> bool =
                fun rem_count are_idle
                    if  rem_count <= 0
                    then
                        (* All responses have been received: *)
                        are_idle
                    else
                    let (msg, node_id) =
                        Transport.recv 
                        cenv
                        (fun m src -> (* Response acceptance criteria *)
                        do {
                            assert (src <> this_node);
                            match m with
                            [ Is_Idle_Resp  resp_id _
                              when resp_id = req_id -> True
                            | _                     -> False]
                        })
                    in
                    let iflag =
                        (* XXX: bad: same match second time: *)
                        match msg with
                        [ Is_Idle_Resp _ b -> b
                        |              _   -> assert False]
                    in
                    get_all_resps (rem_count-1) (are_idle && idle_flag)
                in
                (* Apply the above function: *)
                get_all_resps (n_nodes-1) True
            }
            else
                True (* There is just 1 node, and it's idle *)
        else
            (* Even this node is not idle: *)
            False;

    (*---------------*)
    (*  "complete":  *)
    (*---------------*)
    (*  The body of each Completion Thread:   *)

    value rec complete: cenv -> unit =
    fun cenv ->
    let this_node = Transport.this_node cenv in
    let next_node = Transport.next_node cenv in
    let n_nodes   = Transport.n_nodes   cenv in
    do {
        assert (n_nodes = 1 || next_node <> this_node);

        (* Try to pick a new local pair to complete: *)
        let  (mbp, curr_pairs') = CP.take_hd_pair curr_pairs.val in
        match mbp with
        [ None   ->
            () (* The local queue of pairs is empty  *)

        | Some p ->
            (* Do completion on [p]: *)
            let new_rules = CP.mk_crit_pairs p  in

            (* Don't process [new_rules] immediately -- insert the corresp
               [remote_rewrite]s into the "todo" list. The rules originate
               on [this_node], and have not yet been reduced:
            *)
            let todo_rules' =
                List.map (fun nr -> [(nr, this_node, False)]) new_rules
            in
            todo_rules.val := todo_rules'
        ];
        (* Now process [todo_rules] -- first locally,  then send the results
           to other nodes for further processing. There are typically not so
           many rules in the [todo_rules] queue  (at least one -- created at
           the previous step; on average perhaps N, where N is the number of
           computational nodes used), so we do them all at once. Irreducible
           remnants are accumulated in a list which is then sent away:
        *)
        let remnants =
            List.fold_left
            (fun curr_rems (r, orig_node, was_reduced) ->
                (* Reduce the rule [r] locally,  and if it survives, return
                   its remnant for sending it away:
                *)
                let enumf   = (Transport.appl_data cenv).enumf              in
                let red_res = CP.RT.reduce_wrt_set (curr_rules.val) r enumf in
                match red_res with
                [ None    ->
                    (* This rule was eliminated by reduction -- no remnant: *)
                    curr_rems

                | Some (r', reduced_here) ->
                    (* [r'] is the new remnant. Optimisations: if we have just
                       one node, there is no point in sending this node to our-
                       selves,  so that the Server thread would insert it into
                       [curr_rules] -- we better do it directly here:
                    *)
                    if  n_nodes cenv = 1
                    then
                    do {
                        (* Install the new rule:    *)
                        assert      (orig_node = Transport.this_node cenv);
                        insert_new_rule cenv r';
                        curr_rems
                    }
                    else
                        (* Install the new remnant: *)
                        let was_reduced' = was_reduced || reduced_here  in
                        [(r', orig_node, was_reduced') :: curr_rems]
                ]
            )
            [] todo_rules.val
        in
        (* Now send the [remnants] away for further processing.  HERE are the
           points where interleaving occurs, so the remote Server threads can
           insert the new and reduced rules into the remote "todo" lists:
        *)
        List.iter
            (fun rr ->
            do {
                (* NB: if a rule appears here,  then there is definitely more
                   than one node, otherwise it would already be saved locally:
                *)
                assert (n_nodes >= 2);
                (* This is a potentially blocking operation: *)
                Transport.send cenv (Reduce_Rule rr) next_node
            })
            remnants;

        (*  Evaluate the exit conditions. This is a blocking operation (unless
            we have just one node) again:
        *)
        if  all_idle
        then
            () (* Exit! *)
        else
            (* Next iteration. NB: we do NOT explicitly yield the CPU, as we
               have been doing blocking operations recently anyway (if there
               is more than one node;   but otherwise we don't have a thread
               to yield to, anyway):
            *)
            complete cenv
    };

    (*-------------*)
    (*  "server":  *)
    (*-------------*)
    (* The body of the Server Thread: it processes incoming msgs:
    *)
    value rec server: cenv -> unit =
    fun cenv ->
        let this_node     = Transport.this_node cenv in
        let (msg, src_id) =
            Transport.recv
            cenv
            (fun m sn ->
            do {
                assert (sn <> this_node);
                match m with
                (* The following messages are accepted, as they are to be
                   processed by THIS thread:
                *)
                [ New_Rule      _  -> True
                | Reduce_Rule   _  -> True
                | Is_Idle_Req   _  -> True
                | Get_All_Rules _  -> True
                (* All other msgs are rejected -- they are intended for
                   other threads:
                *)
                |               _  -> False
                ]
            })
        in
        (* XXX: Match again!.. *)
        match msg with
        [ New_Rule    r ->
            (* Insert this rule, and generate new local critical pairs: *)
            insert_new_rule cenv r

        | Reduce_Rule r ->
            (* The rule is simply saved for future reduction by a comple-
               tion thread:
            *)
            todo_rules.val := [r :: todo_rules.val]

        | Is_Idle_Req req_id   ->
            (* Return our status to the caller:  *)
            Transport.send cenv (Is_Idle_Resp req_id (is_idle ()))

        | Get_All_Rules req_id ->
          do {
            (* Return the BY ONE. This request only makes sense if the node
               is already idle (and all of them are idle, in fact):
            *)
            assert (is_idle ());
            CP.RT.iter_rset
                (fun r -> Transport.send cenv (Return_Rule req_id r))
                curr_rules.val
          }

        | _ ->  (* This must not happen: *)
            assert False
        ]

    (*--------------*)
    (*  "run_node": *)
    (*--------------*)
    (* Top-Level Function: *)

    (*
    value run_node  : unit -> unit =
        let cenv = init_node () in
        (* Create threads: *)
        (* All done, but we need to collect -- and possibly reduce  --
           output from all other nodes (if we are on the primary node):
        *)
        if  Transport.node_num this_node = 0
        then
            collect_remote_rules cenv
        else
            () (* Just exit *)
    *)
end;
