(*pp camlp4r *)
(* vim:ts=4:syntax=ocaml
*)
(*===========================================================================*)
(*                              "red_black.ml":                              *)
(*            Purely-Functional Maps Based on Red-Black Trees                *)
(*                                                                           *)
(*      Adapted from previous Haskell, SML and OCaml implementations         *)
(*               (C.Okasaki, S.M.Kahrs, M.Mottl, 1998--2001)                 *)
(*                                                                           *)
(*                L.A.Merkin, IMPARTIAL Project, 2004--2005                  *)
(*===========================================================================*)
(*==================*)
(*  The Interface:  *)
(*==================*)
(*-------------------------*)
(*  The signature of Keys: *)
(*-------------------------*)
module type ORDERED =
sig
    type  t = 't;
    value compare: t -> t -> int;
end;

(*-------------------------*)
(*  The signature of Maps: *)
(*-------------------------*)
(*  NB: all search operations yield an [option] result, rather than raising
    an exception if the element in question was not found.
*)
module type MAP =
sig
    (* [k]   : type of keys;
       ['a]  : type of elements;
       [m 'a]: type of maps themselves:
    *)
    type  k     = 'k;
    type  m 'a  = 'm;

    value empty     : m 'a;
    value is_empty  : m 'a -> bool;

    value singltn   : k    -> 'a -> m 'a;
    value is_singltn: m 'a -> bool;
    value get_single: m 'a -> option (k * 'a);

    value add       : m 'a -> k -> 'a -> m 'a;
    value mem       : m 'a -> k -> bool;
    value find      : m 'a -> k -> option (k * 'a);
    value remove    : m 'a -> k -> m 'a;
    value take      : m 'a -> k -> (option 'a * m 'a);

    value find_min  : m 'a -> option (k * 'a);
    value find_max  : m 'a -> option (k * 'a);
    value remove_min: m 'a -> m 'a;
    value remove_max: m 'a -> m 'a;
    value take_min  : m 'a -> (option (k * 'a) * m 'a);
    value take_max  : m 'a -> (option (k * 'a) * m 'a);

    type  trav_dir  = [Forward | Backward | Stop_Now | Left_Only | Right_Only];

    value traverse  : (k -> 'a -> 'b -> ('b * trav_dir)) -> m 'a -> 'b -> 'b;
    value fold      : (k -> 'a -> 'b ->  'b)             -> m 'a -> 'b -> 'b;
    value iter      : (k -> 'a -> unit)                  -> m 'a -> unit;

    (* Compared to the standard [Map] module, we do not provide functions
       such as [iter], [map], [mapi], [compare], [equal], as they are not
       needed for our purposes.
    *)
end;

(*===========================*)
(*  The Map Implementation:  *)
(*===========================*)

module Map (Keys: ORDERED): (MAP with type k = Keys.t) =
struct
    (*------------------------------*)
    (* The type of Red-Black Trees: *)
    (*------------------------------*)
    type  k = Keys.t;
    type  color  = [R | B];

    type  m 'a   = [E | T of color and m 'a and (k * 'a) and m 'a];

    (*----------------*)
    (* "empty":       *)
    (* "is_empty":    *)
    (*----------------*)
    value empty     : m 'a = E;
    value is_empty  : m 'a -> bool = fun [E -> True | _ -> False];

    (*-----------------*)
    (*  "singltn"   :  *)
    (*  "is_singltn":  *)
    (*  "get_single":  *)
    (*-----------------*)
    value singltn   : k -> 'a -> m 'a = fun k v -> T R E (k,v) E;

    value is_singltn: m 'a -> bool    =
    fun [T R E _ E -> True
        | _        -> False
        ];

    value get_single: m 'a -> option (k * 'a) =
    fun [T R E p E -> Some p
        | _        -> None
        ];

    (*------------*)
    (* "balance": *)
    (*------------*)
    (*  Balances two sub-trees of a given non-[E] node. First equation was
        introduced by S.M.Kahr, to make it work with a weaker invariant.
        NB: "balance" is NOT recursive, so it's of O(1) complexity!
    *)
    value balance: m 'a -> (k * 'a) -> m 'a -> m 'a =
    fun left this right ->
        match (left, this, right) with
        [ (T R a x b,           y, T R c z d) -> T R (T B a x b) y (T B c z d)
        | (T R (T R a x b) y c, z, d        ) -> T R (T B a x b) y (T B c z d)
        | (T R a x (T R b y c), z, d        ) -> T R (T B a x b) y (T B c z d)
        | (a,  x,   T R b y (T R c  z d)    ) -> T R (T B a x b) y (T B c z d)
        | (a,  x,   T R (T R b y c) z d     ) -> T R (T B a x b) y (T B c z d)
        | (a,  x,  b)                         -> T B a x b
        ];

    (*----------*)
    (*  "add":  *)
    (*----------*)
    (* An existing binding for a given key (if any) will be replaced by a
       new one:
    *)
    value add:  m 'a -> k -> 'a -> m 'a =
    fun m k v ->
        (* "ins": recursive insert function: *)
        let rec ins: m 'a -> m 'a =
            fun
            [ E           -> T R E (k,v) E

            | T B l ((yk, _) as y) r ->
                let  cmp = Keys.compare k yk in
                if   cmp < 0
                then balance (ins l) y r    (* Insert on the left  *)
                else
                if   cmp > 0
                then balance l y (ins r)    (* Insert on the right *)
                else T B l (k, v) r         (* Insert here!        *)

            | T R l ((yk, _) as y) r ->
                let  cmp = Keys.compare k yk in
                if   cmp < 0
                then T R (ins l) y r        (* Insert on the left  *)
                else
                if   cmp > 0
                then T R l y (ins r)        (* Insert on the right *)
                else T R l (k,v) r          (* Insert here!        *)
            ]
        in
        (* Do recursive insert: *)
        match ins m with
        [ T _ a z b -> T B a z b
        | _         -> assert False
        ];

    (*-----------*)
    (*  "find":  *)
    (*  "mem" :  *)
    (*-----------*)
    (* NB: If [find] is successful, it returns the ([key], [value]) pair found.
       The returned [key] may be DIFFERENT from the search argument,  both phy-
       sically and structurally,    as they are only equal up to the [compare]
       function provided:
    *)
    value rec find: m 'a -> k -> option (k * 'a) =
    fun m k ->
        match m with
        [ E               -> None
        | T _ l ((yk,_) as kv) r ->
            let  cmp = Keys.compare k yk in
            if   cmp = 0
            then Some kv
            else
            if   cmp < 0
            then find l k
            else find r k
        ];

    value rec mem : m 'a -> k -> bool =
    fun m k ->
        match m with
        [ E               -> False
        | T _ l (yk, _) r ->
            let  cmp = Keys.compare k yk in
            if   cmp = 0
            then True
            else
            if   cmp < 0
            then mem  l k
            else mem  r k
        ];

    (*---------------*)
    (*  "find_min":  *)
    (*  "find_max":  *)
    (*---------------*)
    value rec find_min: m 'a -> option (k * 'a) =
    fun
    [ E             -> None
    | T _ left y _  ->
        match  left with
        [ E -> Some y    (* Found the left-most entry *)
        | _ -> find_min left
        ]
    ];

    value rec find_max: m 'a -> option (k * 'a) =
    fun
    [ E             -> None
    | T _ _ y right ->
        match right with
        [ E -> Some y    (* Found the right-most entry *)
        | _ -> find_max right
        ]
    ];

    (*-------------*)
    (*  "take":    *)
    (*  "remove":  *)
    (*-------------*)
    (* Deletion (by S.M.Kahrs). Does nothing if the key [k] is not present in
       the map [m]. [remove] is simply a [take] which discards the taken val,
       which is slightly inefficient, but should be OK:
    *)
    value rec take: m 'a -> k -> (option 'a * m 'a) =
    fun m k ->
        (* "del": *)
        let rec del: m 'a -> (option 'a * m 'a) =
            fun
            [ E ->  (None, E)

            | T _ a ((yk,yv) as y) b ->
                let  cmp = Keys.compare k yk in
                if   cmp < 0
                then rem_from_left  a y b
                else
                if   cmp > 0
                then rem_from_right a y b
                else (Some yv,  app a b)
            ]
        and
        (* "rem_from_left": *)
        rem_from_left:  m 'a -> (k * 'a) -> m 'a -> (option 'a * m 'a) =
        fun a y b ->
            let (oa, z) = del a in
            let ba      =
                match a with
                [ T B _ _ _ -> bal_left z y b
                | _         -> T R      z y b
                ]
            in
            (oa, ba)
        and
        (* "rem_from_right": *)
        rem_from_right: m 'a -> (k * 'a) -> m 'a -> (option 'a * m 'a) =
        fun a y b ->
            let (ob, z) = del b in
            let bb  =
                match b with
                [ T B _ _ _ -> bal_right a y z
                | _         -> T R       a y z
                ]
            in
            (ob, bb)
        and
        (* "bal_left": *)
        bal_left:  m 'a -> (k * 'a) -> m 'a -> m 'a =
        fun left this right ->
            match left with
            [ T R a x b     -> T R (T B a x b) this right
            | _             ->
                match right with
                [ T B a y b             ->
                    balance left this (T R a y b)
                | T R (T B a y b) z c   ->
                    T R (T B left this a) y (balance b z  (sub c))
                | _ ->
                    assert False
                ]
            ]
        and
        (* "bal_right": *)
        bal_right: m 'a -> (k * 'a) -> m 'a -> m 'a =
        fun left this right ->
            match right with
            [ T R b y c -> T R left this (T B b y c)
            | _         ->
                match left with
                [ T B a x b             ->
                    balance (T R a x b)  this right
                | T R a x (T B b y c)   ->
                    T R (balance (sub a) x b) y (T B c this right)
                | _ ->
                    assert False
                ]
            ]
        and
        (* "app": *)
        app: m 'a -> m 'a -> m 'a =
        fun left right ->
            match (left, right) with
            [ (E, x)   -> x
            | (x, E)   -> x
            | (T R a x b, T R c y d) ->
                match app b c with
                [ T R b' z c' -> T R (T R a x b') z (T R c' y d)
                | bc          -> T R a x (T R bc y d)
                ]
            | (T B a x b, T B c y d) ->
                match app b c with
                [ T R b' z c' -> T R (T B a x b') z (T B c' y d)
                | bc          -> bal_left a x (T B bc y d)
                ]
            | (a, T R b x  c) -> T R (app a b) x c
            | (T  R a x b, c) -> T R a x (app b c)
            ]
        and
        (* "sub": *)
        sub: m 'a   -> m 'a =
        fun
        [ T B a x b -> T R a x b
        | _         -> assert False
        ]
        in
        (* Top-level "del" application: *)
        let (od, z) = del m in
        let rz =
            match z with
            [ T _ a y b -> T B a y b
            | _         -> E
            ]
        in
        (od, rz);


    value rec remove: m 'a -> k -> m 'a =
    fun m k ->
        snd (take m k);


    (*-----------------*)
    (*  "remove_min":  *)
    (*  "remove_max":  *)
    (*-----------------*)
    value remove_min:  m 'a -> m 'a =
    fun m ->
        match find_min m with
        [ None      -> m
            (* [m=E], nothing to delete: *)
        | Some (k,_)-> remove m k
        ];

    value remove_max: m 'a -> m 'a =
    fun m ->
        match find_max m with
        [ None   -> m
            (* [m=E], nothing to delete: *)
        | Some (k,_)-> remove m k
        ];

    (*---------------*)
    (*  "take_min":  *)
    (*  "take_max":  *)
    (*---------------*)
    value take_min: m 'a    -> (option (k * 'a) * m 'a) =
    fun m ->
        match find_min m with
        [ None              -> (None, m)
        | (Some (k,_)) as z -> (z, remove m k)
        ];

    value take_max: m 'a    -> (option (k * 'a) * m 'a) =
    fun m ->
        match find_max m with
        [ None              -> (None, m)
        | (Some (k,_)) as z -> (z, remove m k)
        ];

    (*-------------*)
    (* "traverse": *)
    (* "fold":     *)
    (*-------------*)
    (* This is a [fold] with explicit control over the direction of moving
       across the nodes, and the possibility  of a premature termination.
       The current node is always processed BEFORE the sub-trees. Then the
       direction of traversal  is determined by the 3-state flag  returned
       by the "action function" on the current node;  [Forward] and [Back-
       ward]  correspond to  full traversal (both branches, left first for
       [Forward]  and right first for [Backward],  that is,  from lower to
       higher keys and vice versa, resp); [Stop_Now] is to stop traversing;
       [Left_Only] and [Right_Only]  select  just one particlar  branch to
       continue:
    *)
    type  trav_dir =
        [Forward | Backward | Stop_Now | Left_Only | Right_Only];

    value rec traverse:
        (k -> 'a -> 'b -> ('b * trav_dir)) -> m 'a -> 'b -> 'b =
    fun action m b0    ->
        match  m with
        [ E                   -> b0
        | T _ left (k,v) right->

            let (b1, flag) =  action k v b0 in
            match flag with
            [   Left_Only  -> traverse action left  b1

            |   Right_Only -> traverse action right b1

            |   Forward    ->
                    let b2 =  traverse action left  b1 in
                    traverse  action right b2

            |   Backward   ->
                    let b2 =  traverse action right b1 in
                    traverse  action left  b2

            |   Stop_Now   -> b1
            ]
        ];

    (* [fold] is a simplified and more efficient version of [traverse],
       always moving [Forward]:
    *)
    value rec  fold: (k  -> 'a -> 'b -> 'b) -> m 'a -> 'b -> 'b =
    fun action m b0 ->
        match  m with
        [ E                    -> b0
        | T _ left (k,v) right ->
            let b1 = action k v   b0      in
            let b2 = fold  action left b1 in
            fold action right b2
        ];

    (* [iter]:
       can of course be implemented via [fold], but we do a stand-alone
       implementation for efficiency. The iteration direction is always
       [Forward]:
    *)
    value rec  iter: (k  -> 'a -> unit) -> m 'a -> unit =
    fun action m ->
        match  m with
        [ E                    -> ()
        | T _ left (k,v) right ->
          do {
            action k v;
            iter   action left;
            iter   action right
          }
        ];
end;
