(*pp camlp4r *)
(* vim:ts=4:syntax=ocaml
*)
(*===========================================================================*)
(*                                 "rules.ml":                               *)
(*   Terms, Re-Write Rules and Rules Storage in the Knuth-Bendix Algorithm   *)
(*                 L.A.Merkin, IMPARTIAL Project, 2004--2005                 *)
(*===========================================================================*)
(*==============================*)
(*  Atoms and Low-Level Terms:  *)
(*==============================*)
(*  If there are no more than 256 group generators, the terms can be represen-
    ted as "string"s, otherwise -- as "int" arrays. More compact  bit-wise re-
    presentations are also possible, but incur encoding/decoding overhead, so
    they are not currently implemented.
    We use a functorial interface here, rather than an algebraic type incorpo-
    rating both representations,  for the sake of avoiding  run-time overhead,
    at the expense of code growth.
*)
module type ATOMS =
sig
    (* "atoms" is a direct-access-capable sequence of "atom"s;    "atom"s are
       enumerable, i.e., can be mapped one-to-one to a range  of non-negative
       integers:
    *)
    type  atom  = 'a;
    type  atoms = 's;

    value encode  : atom -> int;
    value decode  : int  -> atom;

    (* PRE/POST-CONDITIONS of the following functions:
       length empty        = 0
       length (single   a) = 1
       length (mk_atoms x) = Array.length x
    *)
    value empty   : atoms;
    value single  : atom           -> atoms;
    value mk_atoms: array atom     -> atoms;
    value length  : atoms -> int;

    value get     : atoms -> int   -> atom; (* Direct R/O access-- no "put"! *)
    value concat  : atoms -> atoms -> atoms;
    value slice   : atoms -> int   -> int  -> atoms;
                        (* From,    Len *)
    value cmp     : atom  -> atom  -> int;
    value eqs     : atoms -> atoms -> bool;
end;

(*=====================*)
(*  High-Level Terms:  *)
(*=====================*)
module type TERMS =
sig
    (* Global Debugging Flags: *)
    value term_equality_by_enums: ref bool;
    value verify_found_lhs_enums: ref bool;

    (* Re-export the "ATOMS" over which the "TERMS" are built: *)
    module A: ATOMS;

    (* The following types are abstract, as they have integrity constraints: *)
    type kb_term       = 't;
    type rewrite_rule  = 'r;
    type rule_pair     = 'p;
    type rule_set      = 's;

    (* Term Orderings: Currently, only one term ordering is implemented: Total-
       Len Lexicographic. Internally, a given term ordering is translated into
       enumerators of terms. For the latter to be as compact as possible,   we
       notice that the range of [atom]s actually used may be smaller than that
       of the whole [atoms] type.   Thus, the user supplies the ACTUAL [atoms]
       which will appear in their terms, in the  LEXICOGRAPHICALLY  DESCENDING
       order (the 0th atom has the highest weight):
    *)
    type term_order = [TLen_Lex of A.atoms];

    (* The ordering is then converted into an internal [term_enumf] function
       which is used to actually enumerate the terms.   The user MUST ensure
       that all terms used in a single computation are created with THE SAME
       INSTANCE of this function:
    *)
    type term_enumf = 'f;

    (* Access finctions.
       IMPORTANT INVARIANT: if (l,r) = get_pair p, then l > r according to the
       lexicograpgic ordering of the rules (LHS first) on top of [term_order]:
    *)
    value get_atoms  :  kb_term      ->  A.atoms;
    value get_enum   :  kb_term      ->  Big_int.big_int;
    value get_rule   :  rewrite_rule -> (kb_term * kb_term); (* (LHS, RHS) *)
    value get_pair   :  rule_pair    -> (rewrite_rule * rewrite_rule);

    (* Comparison: *)
    value terms_equal:  kb_term      -> kb_term      -> bool;
    value terms_cmp  :  kb_term      -> kb_term      -> int;

    (* Generators: *)
    value mk_enumf   :  term_order   -> term_enumf;
    value mk_term    :  A.atoms      -> term_enumf   -> kb_term;
    value mk_rule    :  kb_term      -> kb_term      -> rewrite_rule;
    value mk_pair    :  rewrite_rule -> rewrite_rule -> rule_pair;

    (* [rule_set] operations: *)
    value empty_rset : rule_set;
    value insert_rule: rule_set -> rewrite_rule -> rule_set;
    value delete_rule: rule_set -> rewrite_rule -> rule_set;

    (* Iterators: *)
    value iter_rset  : (rewrite_rule -> unit)     -> rule_set -> unit;
    value fold_rset  : (rewrite_rule -> 'a -> 'a) -> rule_set -> 'a  -> 'a;

    (* Low-Level Reductions: *)

    value find_and_apply_rule :
        ?longest:bool -> ?min_off:int -> ?max_off:int ->
        rule_set      -> A.atoms      -> term_enumf   ->
        option A.atoms;
        (* Returns the rewritten target, or [None] if no rewriting was possi-
           ble. If [longest] is [True] (by default [False]), the longest-LHS
           rule (for each given offset in the target) will be sought;  [min_
           off] and [max_off] control the range of offsets  (by default, the
           whole [target] can be used in the matching process):
           [min_off <= offset <= max_off]. See the implementation remarks as
           well.
        *)

    value find_and_apply_rule_ext:
        ?longest:bool -> ?min_off:int -> ?max_off:int ->
        rule_set      -> A.atoms      -> term_enumf   ->
        option (rewrite_rule * A.atoms);
    (* As [find_and_apply_rule], but also returns the rule used in reduction *)

    (* High-Level Reductions: *)

    value reduce_wrt_set:
        rule_set -> rewrite_rule -> term_enumf ->
        option     (rewrite_rule * bool);

    value reduce_wrt_each_other:    rule_set -> term_enumf -> rule_set;

    (* Rules Parsing -- see the interface for more details: *)
    type terms_info =
    {
        atom_names   : Hashtbl.t A.atom string;
        atoms_by_name: Hashtbl.t string A.atom;
        term_order   : term_order;
        enumf        : term_enumf
    };
    value parse_rules:
        list string -> list string -> (terms_info * (list rewrite_rule));
end;

module Terms (A: ATOMS): TERMS =
struct
    (*----------------*)
    (*  Global Flags: *)
    (*----------------*)
    value term_equality_by_enums = ref True;
    (* Probably a good idea to set it; may use [False] for debugging. *)

    value verify_found_lhs_enums = ref False;
    (* Normally [False]; set to [True] for debugging. *)

    (*----------------------*)
    (* Terms, Rules, Pairs: *)
    (*----------------------*)
    (* A KB Term includes an aggregate of atoms, and also a "big" enumerator
       for fast comparison. A signature of the term ordering used, is stored
       in each term; for all multi-variate operations on term enums, the sig-
       natures are verified to guarantee that the enums correspond to the sa-
       me term ordering:
    *)
    module A: ATOMS   = A;
    type term_order   = [TLen_Lex  of A.atoms];
    type kb_enum      = (int    *  Big_int.big_int);   (* (order_id, enum *)
    type term_enumf   = A.atoms -> kb_enum;

    (* We can then define the types of Terms, RewriteRules and RulePairs: *)
    type kb_term      = (A.atoms      * kb_enum);
    type rewrite_rule = (kb_term      * kb_term);
    type rule_pair    = (rewrite_rule * rewrite_rule);

    (*-----------*)
    (*  Access:  *)
    (*-----------*)
    value get_atoms  :  kb_term      ->  A.atoms            =
        fun (atoms, _) -> atoms;

    value get_enum   :  kb_term      ->  Big_int.big_int    =
        fun (_, enum ) -> snd (enum);

    value get_rule   :  rewrite_rule -> (kb_term * kb_term) =
        fun r -> r;

    value get_pair   :  rule_pair    -> (rewrite_rule * rewrite_rule) =
        fun p -> p;

    (*-------------*)
    (* Generators: *)
    (*-------------*)
    (*  "mk_enumf":
        Constructs a term enumerating function, according to the given term
        ordering.
        XXX: could a true multi-stage programming with run-time code genera-
        tion be benefitial here?
    *)
    value mk_enumf: term_order -> (A.atoms -> kb_enum) =
    fun ord ->
        (* Currently, there is only one term ordering supported.
           Make a signature of the order used:
        *)
        let ord_id = Hashtbl.hash ord
        in
        match  ord with
        [ TLen_Lex all_atoms ->
            (* Convert [all_atoms] into a hash table of weights,  the 0th atom
               having the highest lexicographic weight; [n] is the cardinality
               of the  [all_atoms].
               NB: each atom is assigned a weight in (1..[n]), NOT  (0..[n-1]),
               as a weight-0 atom in the leading position of a term would then
               be indistinguishable from no atom at all! In the polynomial enu-
               merator generated below, it's still possible to use base [n] ra-
               ther than [n+1]:
            *)
            let n       = A.length all_atoms in
            let weights = Hashtbl.create   n in
            let _       =
                (* XXX: [Hashtbl] is filled in imperatively: *)
                for i = 0 to n-1
                do {
                    Hashtbl.replace weights (A.get all_atoms i) (n-i)
                }
            in
            if  Hashtbl.length weights <> n
            then
                (* There were some repeated [all_atoms]: *)
                invalid_arg "Rules.Terms.mk_enumf: Repeated atom(s) found"
            else
            (* Generate the following closure: *)
            (fun tatoms ->
                (* Again, the 0th atom has the highest weight. The enumerator
                   is computed as an [n]-adic value:
                *)
                let len = A.length tatoms in
                let rec poly: int -> Big_int.big_int -> Big_int.big_int =
                    fun i curr ->
                        if   i >= len
                        then curr
                        else
                            (* Get the current atom and its weight: *)
                            let ai = A.get tatoms i in
                            let wi =
                                try  Hashtbl.find  weights ai
                                with [Not_found -> failwith
                                     ("Rules.Terms.enumf: Undeclared atom")]
                            in
                            let curr' =
                                Big_int.add_int_big_int  wi
                               (Big_int.mult_int_big_int n curr)
                            in
                            poly (i+1) curr'
                in
                let v = poly 0 Big_int.zero_big_int in
                (ord_id, v)
            )
        ];

    (* "terms_equal":
       Equality of terms can be tested either by their enums or directly by
       comparing their atoms, depending on the global settings:
    *)
    value terms_equal: kb_term -> kb_term -> bool =
    fun t1 t2 ->
        if  term_equality_by_enums.val
        then
            let (id1, v1) = snd t1 in
            let (id2, v2) = snd t2 in
            if   id1 <> id2
            then
                failwith "Rules.Terms.terms_equal: Different enum functions"
            else
                (* For efficiency, don't use generic [=]: *)
                Big_int.eq_big_int v1 v2
        else
            (* Compare just the [atoms] -- XXX: will use the generic polymor-
               phic comparison; this is better to be avoided:
            *)
            fst t1 = fst t2;

    (* "terms_cmp":
       Returns +1 iff LHS > RHS, 0 iff LHS=RHS, -1 iff LHS < RHS:
    *)
    value terms_cmp  : kb_term -> kb_term -> int =
    fun t1 t2 ->
        let (id1, v1) = snd t1 in
        let (id2, v2) = snd t2 in
        if   id1 <> id2
        then
            failwith "Rules.Terms.terms_cmp: Different enum functions"
        else
            Big_int.compare_big_int v1 v2;

    (* "mk_term": *)
    value mk_term    :  A.atoms      -> term_enumf -> kb_term     =
        fun atoms enumf -> (atoms, enumf atoms);

    (* "mk_rule": *)
    value mk_rule    :  kb_term      -> kb_term    -> rewrite_rule =
        (* Here comparison is ALWAYS done by enum:   *)
        fun lhs rhs ->
            (* CONSTRAINT: [lhs > rhs]: *)
            let (lid, lenum) = snd lhs in
            let (rid, renum) = snd rhs in
            if   lid <> rid
            then
                failwith "Rules.Terms.mk_rule: Different enum functions"
            else
            match Big_int.compare_big_int lenum renum with
            [  1 -> (lhs, rhs)
            | -1 -> (rhs, lhs)
            |  0 -> failwith "Rules.Terms.mk_rule: Tautology" 
                    (* LHS=RHS: Tautology: Forbidden  *)
            |  _ -> assert False
            ];

    (* "mk_pair": *)
    value mk_pair    : rewrite_rule -> rewrite_rule -> rule_pair =
        (* Sort the sides so that the LHS > RHS as well: *)
        fun (((_, e1), (_, e2)) as lhs) (((_, e3), (_, e4)) as rhs) ->
            (* Pre-conditions: [e1 > e2 && e3 > e4],    and all these
               enumerators must be constructed with the same function.
               Inside rules, this should be already guaranteed:
            *)
            let (id1, v1) = e1 in
            let (id2, v2) = e2 in
            let (id3, v3) = e3 in
            let (id4, v4) = e4 in

            let _   = assert (id1 = id2 && id3 = id4) in
            if  id1 <> id3
            then
                failwith "Rules.Terms.mk_pair: Different enum functions"
            else
            match  Big_int.compare_big_int v1 v3 with
            [  1 -> (lhs, rhs)
            | -1 -> (rhs, lhs)
            |  0 ->
                if Big_int.compare_big_int v2 v4 >= 0
                then
                    (lhs, rhs)
                    (* NB: equality (=0) is still OK: a reflexive pair! *)
                else
                    (rhs, lhs)
            | _ ->  assert False
            ];

    (*--------------------*)
    (* The Rule Set Type: *)
    (*--------------------*)
    (* Provides storage for rules,  with a reasonably efficient (NFA-like)
       method of searching for rules which can reduce a given term:
    *)
    (* First of all, we need a Red-Black Map with the key type being just
       [A.atoms], compared according to the initial [atom] (assuming that
       such chunks are always non-empty).
       The reason why we map [A.atoms] rather [kb_term]s (with enums), is
       that the map will actually store SEGMENTS of [kb_term]s rather than
       monolithic terms; the corresp enum will then be stored along the
       RHS:
    *)
    module ROrd =
    struct
        type  t = A.atoms;

        value compare: t -> t -> int =
            fun as1 as2  ->
            do {
                (* Here comparison is done NOT wrt enums, but wrt the first
                   atoms of the terms:
                *)
                assert (A.length as1 > 0 && A.length as2 > 0);
                A.cmp (A.get as1 0) (A.get as2 0)
            };
    end;

    (* Now [RMap] can map [kb_term] to anything: *)
    module  RMap  = Red_black.Map (ROrd);

    (* The [rule_set] is defined as a set of branches, each containig a chunk
       of [A.atoms] (implicitly via the map key) followed by an [rs_node]
       which is either a leaf containing the RHS, or an internal node contai-
       ning more branches, and possibly a RHS (very rarely -- only if the LHS
       of some rule is a sub-term of another LHS).
       Also, enums of the LHSs are stored together with the RHSs:
    *)
    type ler       = (kb_enum * kb_term);      (* LHS Enum, RHS *)

    type rule_set  = RMap.m rs_node (* {[A.atoms] => [rs_node]} *)
    and  rs_node   =
        [   RS_Leaf  of ler
        |   RS_Intra of rule_set and option ler
        ];                          (* Further branches, maybe with a [ler] *)

    value empty_rset: rule_set = RMap.empty;

    (*=============*)
    (*  Iterators: *)
    (*=============*)
    (* The user-provided call-back requires a [rewrite_rule] as an arg, whereas
       internally we traverse [ler]s as above. Hence the following converter:
    *)
    value rule_of_ler: A.atoms   -> ler -> rewrite_rule =
    fun   latoms (lenum,  rterm) ->
        ((latoms, lenum), rterm);

    (*--------------*)
    (* "iter_rset": *)
    (*--------------*)
    value rec iter_rset: (rewrite_rule -> unit) -> rule_set -> unit =
    fun f rset0 ->
        RMap.iter
        (fun latoms node  ->
            match node with
            [ RS_Leaf ler ->
                (* Just apply [f] to this node: *)
                f (rule_of_ler latoms ler)

            | RS_Intra rset1 mb_ler ->
            do {
                (* Apply [f] to the local node, if any: *)
                match mb_ler with
                [ Some ler -> f (rule_of_ler latoms ler)
                | None     -> ()
                ];
                (* Then recurse down -- XXX: it will not be tail recursion? *)
                iter_rset f rset1
            }]
        )
        rset0;

    (*--------------*)
    (* "fold_rset": *)
    (*--------------*)
    value rec fold_rset: (rewrite_rule -> 'a -> 'a) -> rule_set -> 'a  -> 'a =
    fun f rset0 a0 ->
        RMap.fold
        (fun latoms node a1 ->
            match node with
            [ RS_Leaf ler ->
                (* Apply [f] to this node and [a1]: *)
                f (rule_of_ler latoms ler) a1

            | RS_Intra rset1 mb_ler ->
                let a2 =
                    (* Apply [f] to the local node, if any: *)
                    match mb_ler with
                    [ Some ler -> f (rule_of_ler latoms ler) a1
                    | None     -> a1
                    ]
                in
                (* Recurse down -- XXX: not tail recursion? *)
                fold_rset f rset1 a2
            ]
        )
        rset0 a0;

    (*======================*)
    (* Rule Set Navigation: *)
    (*======================*)
    (* The "navigate*" functions search  for a particular LHS in a Rule Set.
       They constitute a common part of the Insert, Delete and Find operations
       on the Rule Set:
    *)
    type oper =
    [
      Insert of A.atoms and kb_enum and kb_term
      (* [Insert latoms lenum rhs] inserts the rule [(latoms, lenum), rhs)]
         into the Rule Set.
      *)

    | Delete of A.atoms and kb_enum and kb_term
      (* [Delete latoms lenum rhs] deletes the rule [(latoms, lenum), rhs)]
         from trhe Rule Set. The RHS also needs to be specified,  as there
         can be multiple rules with the same LHS.
      *)

    | Find   of A.atoms and bool
      (* [Find atoms flag] is used for searching for a stored rule with the
          LHS such that
         [LHS.(0 : l) = atoms (fpos : fpos + l)] for some [l >= 0]
         (the upper bounds in the above ranges are inclusive).
         If the [flag] is set, the search will be performed for rules with
         LONGEST matching LHSs (for each given [fpos] -- but not across dif-
         ferent [fpos]es). This means that only results stored in [RS_Leaf]
         nodes will be returned, not [RS_Intra] results.
      *)
    ];

    (* In any case,  [oper] contains a "target" LHS  against which a matching
       stored rule is sought (such a stored rule not yet fully exist, e.g. in
       case of [Insert]). The [get_target] returns this LHS target.
       The [navigate*] functions also maintain the positional index within the
       target ([tpos]).
    *)
    value get_target: oper  -> A.atoms =
    fun
    [  Insert lhs _ _ -> lhs
    |  Delete lhs _ _ -> lhs
    |  Find   lhs _   -> lhs
    ];

    (* Result of [navigate*]: depends on the operation being performed. For
       [Insert] and [Delete], it's an updated (possibly) [rule_set] or [lhs_
       chunk]. For [Find], a successful result is [(fpos, lenum, rhs)]; the
       [fpos] is used by the caller to re-construct the LHS of the rule found,
       together with [lenum]; the RHS is taken from the result.
    *)
    type find_res  = option (int * kb_enum * kb_term);

    (* The result of [navigate_rule_set]: *)
    type set_res   =
    [ Updated_Set of rule_set             (* For [Insert] and [Delete] *)
    | Found_Set   of find_res             (* For [Find] *)
    ];

    (* The result of [navigate_branch]:   *)
    type branch_res=
    [ Updated_Br  of A.atoms and rs_node  (* For [Insert] and [Delete] *)
    | Found_Br    of find_res             (* For [Find] *)
    ];

    (*------------------------*)
    (*  "navigate_rule_set":  *)
    (*------------------------*)
    (* "BranchesRN" handling: Selects the branch to take, from a set.
       Args:
            [oper]  : the operation being performed;
            [tpos]  : the curr pos in the target of the [oper] being
                      matched against the stored data;
            [rset0] : the set of branches under consideration:
    *)
    value rec navigate_rule_set:
        oper -> int -> rule_set -> set_res =

    fun oper tpos rset0 ->
        let target = get_target oper in

        (* There MUST be some atoms in the [target] from [tpos] on;   if
           there are none, we would not be traversing this [rset]. Thus,
           [target.(tpos)] does exist:
        *)
        let _ = assert (tpos < A.length target) in

        (* Is there a branch in "rset0" starting  with  the same atom as
           [target.(tpos)]? We can just use "RMap.find" for that, as the
           branches in the set are compared just by their first atoms:
        *)
        match RMap.find rset0 (A.single (A.get target tpos)) with
        [ None ->
            (* No such branch; what it actually means, depends on the
               [oper]:
            *)
            match oper with
            [ Insert _ targ_enum rhs ->
                (* Will create a new branch (containing the rest of the
                   [target] from the [tpos] on), and insert it into the
                   [rset0]. The RHS is installed at the end of  the new
                   chunk:
                *)
                let new_branch =
                    A.slice target tpos ((A.length target) - tpos)
                in
                let new_node   = RS_Leaf (targ_enum, rhs)
                in
                Updated_Set (RMap.add rset0 new_branch new_node)

            | Delete _ _ _ ->
                (* Nothing to delete: return the curr set unchanged: *)
                Updated_Set rset0

            | Find _ _   ->
                (* Dead end: nothing found: *)
                Found_Set None
            ]

        | Some (chunk0, node0)->
            (* Yes, there is a branch starting with the current [target] atom;
               follow (and possibly modify) this branch.  If modified, rewrite
               the curr set.   NB: [chunk0] is being scanned from pos 1, as we
               already know from the set search  that the following  assertion
               holds:
            *)
            let _ = assert ((A.length chunk0 > 0) &&
                            (A.get chunk0  0 = A.get target tpos))
            in
            match navigate_branch oper (tpos+1) chunk0 node0 1 with

            [ Updated_Br chunk1 node1 ->
                (* This is a (possibly modified) branch from [Insert] or [Dele-
                   te]. Install this result in the current set, UNLESS [chunk1]
                   is empty;    in the latter case, we can SHRINK the set as a
                   result of chunk deletion.    In any case, the old branch is
                   removed first:
                *)
                let rset1 = RMap.remove rset0 chunk0 in
                let rset2 =
                    if   A.length chunk1 > 0
                    then RMap.add rset0 chunk1 node1
                    else rset1
                in
                Updated_Set rset2

            | Found_Br any ->
                (* This is a result of [Find] only; propagate it: *)
                Found_Set any
            ]
        ]
    (*----------------------*)
    (*  "navigate_branch":  *)
    (*----------------------*)
    (* Moving along the current [chunk] (from pos 0), and simultaneously along
       the [target] (from pos [tpos]), until there is a mismatch between their
       atoms, or one of the terms is finished. Then the [oper] is applied:
    *)
    and navigate_branch:
        oper -> int -> A.atoms -> rs_node -> int -> branch_res =

    fun oper tpos chunk node cpos ->

        let target = get_target oper in

        if  tpos < A.length target
        then
            (* We are still within the [target]. What about the [chunk]? *)
            if  cpos < A.length chunk
            then
                (* We are properly within BOTH the [chunk] and the [target].
                   Compare their atoms in the current position:
                *)
                if  A.get target tpos = A.get chunk cpos
                then
                    (* They are coinciding -- simply move further down: *)
                    (* XXX: update the [oper]? *)
                    navigate_branch oper (tpos+1) chunk node (cpos+1)
                else
                    (* Mismatch -- its handling depends on the [oper]:  *)
                    match oper with
                    [ Insert _ targ_enum rhs ->
                        (* Will need to introduce a new branching point here.
                           NB: a mismatch CANNOT occur at the 0th position in
                           the [chunk], due to the way we select branches:
                        *)
                        let _ = assert (cpos >= 1)                  in

                        let upper_chunk = A.slice  chunk 0 cpos
                            (* chunk.(0 : cpos-1); due to the above condition,
                               it is non-empty.
                            *)                                      in
                        let lower_chunk = A.slice  chunk cpos
                                         (A.length chunk - cpos)
                            (* chunk.(cpos : end) *)                in

                        let lower_target= A.slice  target tpos
                                         (A.length target - tpos)   in

                        (* Both [lower_chunk] and [lower_target] are non-empty:
                           they contain at least the atoms in which the [chunk]
                           and the [target] differ:
                        *)
                        let _ = assert (A.length lower_chunk  > 0   &&
                                        A.length lower_target > 0)
                        in
                        (* The new branch set: 2-element.
                           The 1st element is the [lower_chunk] with the [node]
                           which follows the original [chunk]:
                        *)
                        let brset0   = RMap.singltn lower_chunk node in

                        (* The 2nd element is made of the [lower_target] and
                           the new leaf inserted:
                        *)
                        let new_leaf = RS_Leaf (targ_enum, rhs) in
                        let brset1   = RMap.add brset0 lower_target new_leaf in

                        (* We return the [upper_chunk] followed by a new node
                           containing [brset1]. As the original [chunk] did not
                           terminate at the branching point, there is obviously
                           no [ler] to be stored here -- any existing data have
                           moved down the tree with the [node]:
                        *)
                        Updated_Br upper_chunk (RS_Intra brset1 None)

                    | Delete _ _ _ ->
                        (* The term to be deleted has not been found: *)
                        Updated_Br chunk node

                    | Find _ _ ->
                        (* Similar to [Delete] -- nothing found: *)
                        Found_Br None
                    ]
            else
                (* The [chunk] we follow has ended, but the [target] has not: *)
                match node with
                [ RS_Leaf old_ler ->
                    (* No more chunks below. Depends on the [oper]: *)
                    match oper with
                    [ Insert _ targ_enum rhs ->
                        (* Will need to replace this node by another branching
                           point. It will contain the [old_ler], as the origi-
                           nal chunk finished here.   The branching point will
                           contain a single branch   (the remaining [target]),
                           followed by the inserted Leaf:
                        *)
                        let rem_target =
                            A.slice target tpos  (A.length target - tpos)
                        in
                        let new_leaf   = RS_Leaf (targ_enum, rhs)           in
                        let new_rset   = RMap.singltn rem_target new_leaf   in
                        let new_intra  = RS_Intra new_rset  (Some old_ler)  in

                        (* Return the original [chunk], but w/ [new_intra]: *)
                        Updated_Br chunk new_intra

                    | Delete _ _ _ ->
                        (* The [target] could not be followed to the end, so
                           nothing to delete. Return the unchanged args:
                        *)
                        Updated_Br chunk node

                    | Find _ _ ->
                        (* The [target] is indeed a "super-term"  of the path
                           from the root up to this [chunk] (final) -- that's
                           what we need. The LHS of the rule found will be re-
                           constructed  by the caller  as a [target] sub-term.
                           Return the [tpos], the LHS enum and the RHS:
                        *)
                        Found_Br (Some (tpos, fst old_ler, snd old_ler))
                    ]

                | RS_Intra rset0 mb_ler  ->
                    (* For [Insert] and [Delete], continue through the [rset0].
                       For [Find], the result is successful if we can STOP here
                       (as then the [target] is a "super-term" of the path ter-
                       minating at this [chunk] -- only if the "longest search"
                       flag is NOT set), or undecided yet:
                    *)
                    match oper with
                    [ Find  _   longest when Option.is_some mb_ler  ->

                        let (lenum, rhs)   = Option.get     mb_ler  in
                        if  not longest
                        then
                            (* Yes, we can stop searching here. THUS, if there
                               is still a longer path,  we will NOT find it --
                               the shortest one is returned.
                            *)
                            Found_Br (Some (tpos, lenum, rhs))
                        else
                            (* The longest path is requested, but there may ac-
                               tually be nothing found below this point, as the
                                next chunk can diverge from the [target]. Just
                                try, and return whatever is successful:
                            *)
                            match navigate_rule_set oper tpos rset0 with
                            [ Found_Set ((Some res) as ok) ->
                                Found_Br ok

                            | _ -> (* Nothing below, take what we have here: *)
                                Found_Br (Some (tpos, lenum, rhs))
                            ]

                    | _ ->
                        (* In all other cases: continue through the [node]:  *)

                        match navigate_rule_set oper tpos rset0  with
                        [ Updated_Set rset1 ->
                            (* NB: if [rset1] is empty ([Delete] only), extra
                               care needs to be taken: either the node is eli-
                               minated completely (together with the chunk lea-
                               ding to it), or it is converted into a Leaf. A
                               special treatment is also required if [rset1] is
                               a singleton.
                               XXX: these checks are superfluous for [Insert],
                               but they are very inexpensive:
                            *)
                            if  RMap.is_empty rset1
                            then
                                match mb_ler with
                                [ None ->
                                    (* No values at all in this [node]; remove
                                       it completely,  by removing the [chunk]
                                       leading to it:
                                    *)
                                    Updated_Br A.empty node

                            | Some ol ->
                                (* Convert the [node] into a Leaf: *)
                                Updated_Br chunk (RS_Leaf ol)
                            ]
                            else
                            if  RMap.is_singltn rset1
                            then
                                match mb_ler with
                                [ None ->
                                    (* Merge the [chunk] and the single chunk
                                       stored in [rset1]:    "straightening":
                                    *)
                                    let (schunk, snode) =
                                        Option.get (RMap.get_single rset1)
                                    in
                                    Updated_Br (A.concat chunk schunk) snode

                                | _ ->
                                    (* Cannot remove the [node], even though its
                                       [rset1] is singleton, as there is a dir-
                                       ectly contained value:
                                    *)
                                    let new_node  = RS_Intra rset1 mb_ler in
                                    Updated_Br chunk new_node
                                ]
                            else
                                (* Generic case -- just update the set: *)
                                let new_node  = RS_Intra rset1 mb_ler  in
                                Updated_Br chunk new_node

                        | Found_Set  any ->
                            (* This is for [Find] when [mb_ler] is [None]; so
                               we could not stop at the [node]. Propagate any
                               result we found below:
                            *)
                            Found_Br any
                        ]
                    ]
                ]
        else
            (* The [target] has finished. What about the [chunk]? *)
            if  cpos < A.length chunk
            then
                (* We are still properly within the [chunk]. What it means,
                   depends on the [oper]:
                *)
                match oper with
                [ Insert _ targ_enum rhs ->
                    (* There will be a branching point here, with just ONE
                       branch (the remaining part of the [chunk]), and the
                       new [ler] value installed directly in the [RS_Intra]
                       node:
                    *)
                    let _ = assert (cpos >= 1)                      in

                    let upper_chunk = A.slice  chunk 0 cpos         in
                        (* chunk.(0 : cpos-1), non-empty due to the above *)

                    let lower_chunk = A.slice  chunk cpos
                                     (A.length chunk - cpos)
                        (* chunk.(cpos : end), also non-empty *)    in

                    (* The [node] will be attached to [lower_chunk]: *)
                    let rset        = RMap.singltn lower_chunk node in

                    (* The inserted value comes here: *)
                    let new_node    = RS_Intra rset (Some (targ_enum, rhs))
                    in
                    Updated_Br upper_chunk new_node

                | Delete _ _ _ ->
                    (* The [target] was not actually found -- nothing to
                       delete:
                    *)
                    Updated_Br chunk node

                | Find _ _   ->
                    (* The [target] (from the given position on) appears to be
                       a "sub-term" of the path  from the tree root up to this
                       [chunk], so the corresp rule(s) cannot reduce it:
                    *)
                    Found_Br None
                ]
            else
                (* Both [target] and [chunk] ended simultaneously. Again, the
                   meaning of this depends on the [oper]:
                *)
                match oper with
                [ Insert _ targ_enum targ_rhs ->
                    (* Insertion succeeds by over-writing the value currently
                       stored in the [node] (there may still be [None] for an
                       [RS_Intra] node):
                    *)
                    let ler      = (targ_enum, targ_rhs) in
                    let new_node =
                        match node with
                        [ RS_Leaf _       -> RS_Leaf  ler
                        | RS_Intra rset _ -> RS_Intra rset (Some ler)
                        ]
                    in
                    Updated_Br chunk new_node

                | Delete _ targ_enum targ_rhs ->
                    (* Deletion -- examine the attached [node]. The [targ_enum]
                       MUST always match the LHS enum stored; but the RHSs may
                       still be different, in which case there is no deletion:
                    *)
                    match node with
                    [ RS_Leaf (lhs_enum, rhs) ->
                        let (id1, v1) =  targ_enum in
                        let (id2, v2) =   lhs_enum in
                        if  id1 <> id2
                        then
                            failwith
                            "Rules.navigate_branch: Different enum functions"
                        else
                        let _ = assert (Big_int.eq_big_int v1 v2)
                        in
                        (* For efficiency, we can compare the RHS enums only,
                           but if they are not actually in use, we will have
                           to compare the chunks themselves:
                        *)
                        if  terms_equal targ_rhs rhs
                        then
                            (* The leaf is really deleted. We signal this by
                               returning the empty CHUNK  with  the original
                               leaf:
                            *)
                            Updated_Br A.empty node
                        else
                            (* RHSs diffwerent -- no deletion: *)
                            Updated_Br chunk node

                    | RS_Intra _ None ->
                            (* Nothing to delete: *)
                            Updated_Br chunk node

                    | RS_Intra rset (Some (_,rhs)) ->
                        (* Again, compare the enums only, if possible: *)
                        if  terms_equal targ_rhs rhs
                        then
                            (* The node is NOT to be completely deleted,  only
                               the stored value is -- as the [rset] should NOT
                               be empty:
                            *)
                            let _ =  assert (not (RMap.is_empty rset)) in
                            Updated_Br chunk (RS_Intra rset None)
                        else
                            (* Nothing to delete: *)
                            Updated_Br chunk node
                    ]

                | Find _ _ ->
                    (* [Find] successful only if we can terminate the search
                       path here, that is, if there is a value contained  in
                       the [node]:
                    *)
                    match node with
                    [ RS_Leaf          (lenum, rhs)
                    | RS_Intra _ (Some (lenum, rhs)) ->
                        Found_Br (Some (tpos, lenum, rhs))

                    | _ ->
                        Found_Br  None
                    ]
                ];

    (*================================*)
    (*  Top-Level Storage Functions:  *)
    (*================================*)
    (*------------------*)
    (*  "insert_rule":  *)
    (*------------------*)
    value insert_rule: rule_set -> rewrite_rule -> rule_set =
    fun rset (lhs, rhs) ->
        let  (latoms, lenum) = lhs in

        if  A.length latoms  = 0
        then
            invalid_arg "Rules.Terms.insert_rule: Empty LHS"
        else
        let oper = Insert latoms lenum rhs
        in
        match navigate_rule_set oper 0 rset with
        [ Updated_Set res -> res
        | _               -> assert False
        ];

    (*------------------*)
    (*  "delete_rule":  *)
    (*------------------*)
    value delete_rule: rule_set -> rewrite_rule -> rule_set =
    fun rset (lhs, rhs) ->
        let  (latoms, lenum) = lhs in

        if  A.length latoms  = 0
        then
            invalid_arg "Rules.Terms.delete_rule: Empty LHS"
        else
        let oper = Delete latoms lenum rhs
        in
        match navigate_rule_set oper 0 rset with
        [ Updated_Set res -> res
        | _               -> assert False
        ];

    (*-----------------------*)
    (*  "find_and_apply_*":  *)
    (*-----------------------*)
    (* NFA functionality: tries to find a stored rule whose LHS  is a sub-term
       of the given [target], starting from the position [tpos >= 0]. If succ-
       essful, obtains terms [(before, (lhs, rhs), after)],  where [(lhs,rhs)]
       is the rule found, and [target = before ^ lhs ^ after]. The [target] is
       then rewritten into [before ^ rhs ^ after]. If such a rule has not been
       found (and no rewriting performed), [None] is returned. Otherwise,  the
       returned data depends on the exact function used (see below).
       NB:
       It is possible  to apply some selection strategies  while searching for 
       an applicable rule: range of offsets ([min_off]..[max_off])  inclusive,
       and the [longest] flag indicating that the longest matching LHS (rather
       than the shortest one -- default) is to be returned  for each  suitable
       offset. There is still NO global (cross-offset) search  for the longest
       result:
    *)
    (* [find_and_apply_wrapper] -- internal; the "enumf" arg is only used for
       verification purposes here. Returns a tuple:
       [(tpos, fpos, latoms, lenum, rhs, rewritten_tatoms)]
    *)
    value find_and_apply_wrapper:
        ~longest:bool -> ~min_off:int -> ~max_off:int ->
        rule_set      -> A.atoms      -> term_enumf   ->
        option (int * int * A.atoms * kb_enum * kb_term * A.atoms) =

    fun ~longest ~min_off ~max_off rset tatoms enumf  ->
        let oper   = Find tatoms longest in
        (* NB: [tmax] is the inclusive upper bound for [tpos] (the initial
           matching position within the [tatoms]:
        *)
        let tmax   =
            if  max_off >= 0
            then
                (* Real [max_off] value: *)
                min ((A.length tatoms) - 1) max_off
            else
                (* [max_off] is not to be used: *)
                (A.length tatoms) - 1
        in
        let rec find_and_apply_from:
            int ->
            option (int * int * A.atoms * kb_enum * kb_term * A.atoms) =
            (*    [(tpos, fpos, latoms, lenum, rhs, rewritten_tatoms)] *)
        fun tpos ->
            (* NB: the caller is reposnsible for the validity of the [tpos] and
               [tmax] values. No consistency check is done here:
            *)
            if  tpos > tmax
            then
                (* At the end of the [tatoms] or at [max_off]: Nothing found: *)
                None
            else
            (* Perform the search: *)

            match navigate_rule_set oper tpos rset with
            [ Updated_Set  _  ->
                assert False      (* Impossible for [Find]  *)

            | Found_Set  None ->
                (* Nothing found here, try the next [tpos]: *)
                find_and_apply_from (tpos+1)

            | Found_Set (Some (fpos, lenum, rhs)) ->
                (* Yes, we found the SHORTEST-LHS rule whose LHS coincides with
                   the [tatoms] starting from [tpos] and running until [fpos]:
                *)
                let      _ = assert (fpos > tpos) in
                let latoms =
                    if  verify_found_lhs_enums.val
                    then
                        (* Re-construct the LHS, and check that its enum is the
                           same  as the found one -- this provides  an over-all
                           check of the search procedure:
                        *)
                        let lats = A.slice tatoms tpos (fpos-tpos) in

                        (* XXX: we would like to say  :
                           [assert (enumf lhs = lenum)],
                           but don't compare [Big_Int]s using generic [=]!
                           This will fail, as they are abstract type!
                        *)
                        let (id1, v1) = lenum      in
                        let (id2, v2) = enumf lats in
                        do {
                            assert (id1 = id2 && Big_int.eq_big_int v1 v2);
                            lats
                        }
                    else
                        A.empty
                in
                (* OK, apply the rule found: replace [tatoms.(tpos:fpos-1)]
                   (inclusive) by the RHS:
                *)
                let before =  A.slice tatoms 0    tpos                      in
                let after  =  A.slice tatoms fpos (A.length tatoms - fpos)  in
                let tatoms'=  A.concat (A.concat before (fst rhs))  after   in

                (* Return: *)
                Some (tpos, fpos, latoms, lenum, rhs, tatoms')
            ]
        in
        (* Do iterative search: *)
        find_and_apply_from min_off;


    (* [find_and_apply_rule]: externally-visible:
       if successful, returns just the re-written [tatoms]:
    *)
    value find_and_apply_rule:
        ?longest:bool -> ?min_off:int -> ?max_off:int ->
        rule_set      -> A.atoms      -> term_enumf   ->
        option A.atoms =

    fun ?(longest=False) ?(min_off=0) ?(max_off=(-1)) rset tatoms enumf ->
        let res =
            find_and_apply_wrapper longest min_off max_off rset tatoms enumf
        in
        match res with
        [ None -> None
        | Some (_, _, _, _, _, tatoms') -> Some tatoms'
        ];

    (* [find_and_apply_rule_ext]: externally-visible:
       if successful, returns the rule found, and the rewritten [tatoms].
       Mostly for testing purposes:
    *)
    value find_and_apply_rule_ext:
        ?longest:bool -> ?min_off:int -> ?max_off:int ->
        rule_set      -> A.atoms      -> term_enumf   ->
        option (rewrite_rule * A.atoms)  =

    fun ?(longest=False) ?(min_off=0) ?(max_off=(-1)) rset tatoms enumf ->
        let res =
            find_and_apply_wrapper longest min_off max_off rset tatoms enumf
        in
        match res with
        [ None -> None
        | Some (tpos, fpos, lats, lenum, rhs, tatoms') ->
            (* We need to return [tatoms'] along with the re-constructed rule
               used for this reduction. If the  [verify_found_lhs_enums] mode
               was set, [lats] is the LHS term which can be used to re-const-
               ruct the rule; otherwise, [lats] is empty,  and we need to use
               [tpos] and [fpos]:
            *)
            let latoms =
                if   A.length lats <> 0
                then lats
                else A.slice tatoms tpos (fpos-tpos)
            in
            let lhs    = (lats, lenum) in
            let rule   = (lhs,  rhs)   in
            Some (rule, tatoms')
        ];

    (*=========================*)
    (*  High-Level Reductions: *)
    (*=========================*)
    (*---------------------*)
    (*  "reduce_wrt_set":  *)
    (*---------------------*)
    (* Reduces a given rule wrt a set of other rules, until no more reductions
       are possible, and/or a Tautology has been derived.  In the latter case,
       the rule is eliminated, and "None" is returned.     In the former case,
       flag is also returned indicating whether there was actually a reduction:
    *)
    value reduce_wrt_set:
        rule_set  -> rewrite_rule -> term_enumf ->
        option      (rewrite_rule * bool)       =
    fun rset rule enumf ->
        (* Initially, [lhs] and [rhs] could not be equal: *)
        let  (l, r) = rule in
        if   terms_equal l r
        then failwith "Rules.Terms.reduce_wrt_set: Tautology"
        else
        (* The Reduction Loop. NB: there is an implementation choice here:
           (1) reduce LHS and RHS independently, and then compare the results
               to detect a tautology;
           (2) reduce LHS and RHS in sync, and check for a tautology after
               every step.
           We do (2), as it potentially allows us to detect a Tautology at an
           eralier stage, but for efficiency, we need to keep track of reduc-
           tions performed at the previous step,   in order not to try reduce
           again the [atoms] which are already irreducible:
        *)
        let reduce_side: A.atoms -> bool -> (A.atoms * bool) =
        fun t red ->
            if  red
            then
                (* [t] changed at the prev step -- reduce it again: *)
                match find_and_apply_rule rset t enumf with
                [ None    ->
                    (* No reduction occurred: *)
                    (t, False)
                | Some t' ->
                    (* Yes, there was a reduction: *)
                    (t', True)
                ]
            else
                (t, False) (* [t] was already irreducible *)
        in
        (* "reduce_atoms_loop":
           [lred] and [rred] indicate whether the [l] and [r] terms, resp, are
           still considered reducible  ([True] unless the resp term was really
           NOT reduced during the previous iteration),   and they are intially
           set to [True],  whereas [was_red] indicates that there was at least
           one real reduction of [l] or [r] in the past (initially [False]):
        *)
        let rec reduce_atoms_loop:
            A.atoms -> A.atoms ->  bool -> bool -> bool ->
            option    (A.atoms * A.atoms * bool) =
        fun l r lred rred was_red ->
            (* Reduce both [l] and [r] wrt the [rset]: *)
            let (l', lred') = reduce_side l lred in
            let (r', rred') = reduce_side r rred in

            (* Check whether we got a Tautology: *)
            if  A.eqs l' r'
            then None (* The rule has been eliminated! *)
            else
            (* If both LHS and RHS are now irreducible, stop. There was clearly
               no reduction at the last step,    but there may have been one in
               the past (indicated by [was_red]):
            *)
            if   not  (lred' || rred')
            then Some (l', r', was_red)
            else
                (* Try more reductions -- and set the [was_red] flag, as there
                   WAS reduction at the last step:
                *)
                reduce_atoms_loop l' r' lred' rred' True
        in
        (* Apply the above function.   Both sides are initially marked as
           reducible, but the "was-reduction" flag is initially NOT set:
        *)
        let lhs = fst (l)  in
        let rhs = fst (r)  in
        match reduce_atoms_loop lhs rhs True True False with
        [ None                       -> None
        | Some (lhs', rhs', was_red) ->
            (* Re-construct the rule,  and return it with [was_red]  flag: *)
            Some (mk_rule (mk_term lhs' enumf) (mk_term rhs' enumf), was_red)
        ];

    (*---------------------------*)
    (*  "reduce_wrt_each_other": *)
    (*---------------------------*)
    (* Reduces all elements of a given [rset0] wrt each other, until no more
       reductions are possible. This function does NOT return a flag indica-
       ting whether the set was actually modified  by at least one reduction
       (no need for that):
    *)
    value reduce_wrt_each_other: rule_set -> term_enumf -> rule_set =
    fun rset0 enumf ->
        (* "Inner loop": reduce each element of the set modulo the rest. The
           function returns  the irreducible  result:
        *)
        let rec reduce_each_wrt_rest: rule_set -> rule_set =
        fun rset1 ->
            (* Do reduction for each rule in [rset1]; [rset2] is the current
               set being transformed in this process, with reduced rules in-
               serted instead of the original ones.  NB: reductions are per-
               formed modulo this MODIFIED set, not the original one:
            *)
            let (rset_res, rflag_res) =
                fold_rset
                (fun rule1 (rset2, rflag1) ->
                    match reduce_wrt_set rset2 rule1 enumf with
                    [ None ->
                        (* [rule1] has been eliminated, so it will NOT be put
                           back into the modified set.  There was  certainy a
                           reduction:
                        *)
                        (rset2, True)

                    | Some (rule2, rflag2) ->
                        (* [rule1] became [rule2]; [rflag2] indicates whether
                           there was actually a reduction, or the rule was un-
                           changed.   In the latter case, we don't update the
                           resulting set:
                        *)
                        if  rflag2
                        then
                            (* Replace the rule.   XXX: we need a proof  or at
                               least an assertion that [rule1] indeed belonged
                               to [rset2]:
                            *)
                            (insert_rule (delete_rule rset2 rule1) rule2, True)
                        else
                            (* There was no reduction here, but keep the histo-
                               rical flag:
                            *)
                            (rset2, rflag1)
                    ]
                )
                (* Arg1 is used for traversing the set, Arg2 for accumulating
                   the result by replacing those elements of  Arg1 which were
                   taken for reduction, and actually reduced. The "was-reductn"
                   flag is initially [False]:
                *)
                rset1 (rset1, False)
            in
            if  rflag_res
            then
                (* There was at least one reduction of an element of [rset0]
                   modulo the rest, so do another round:
                *)
                reduce_each_wrt_rest rset_res
            else
                (* Full circle without a single reduction -- stop now: *)
                rset_res
        in
        reduce_each_wrt_rest rset0;

            
    (*==================*)
    (*  Parsing Rules:  *)
    (*==================*)
    (* Atoms in Terms are represented by Strings, separated by any witespace
       sequences (\s+). Rules are represented as Strings "LHS |=> RHS", each
       one being an element  of the Arg1 list of the "parse_rules" function.
       The function returns the list of rules constructed, plus two mappings:
       (Atoms => Strings) and (Strings => Atoms):
    *)
    type terms_info =
    {
        atom_names   : Hashtbl.t A.atom string;
        atoms_by_name: Hashtbl.t string A.atom;
        term_order   : term_order;
        enumf        : term_enumf
    };

    value parse_rules:
        list string -> list string -> (terms_info * (list rewrite_rule)) =
    fun symbols rlines ->
        (* The following map is used for:
           -- fast validity verification of all symbols encountered in the
              parsed rules;
           -- calculation of "frequencies" of all symbols:
        *)
        let n_atoms  = List.length symbols    in
        let map_si   = Hashtbl.create n_atoms in     (* symbol -> freq  *)
        let ()       =
            List.iter (fun s -> Hashtbl.add map_si s 0) symbols
        in
        (* Parsing a term into individual symbols <-> atoms: *)
        let split_atoms  : string -> list string =
        fun s ->
            Misc_utils.split_by_whites (ExtString.String.strip s)
        in
        (* Verifying a symbol and updating its freq: *)
        let verify_symbol: string -> unit =
        fun s ->
            let f = try  Hashtbl.find map_si s
                    with [Not_found ->
                         failwith
                         ("Rulles.Terms.parse_rules: Undeclared Atom: "^s)]
            in
            Hashtbl.replace map_si s (f+1)
        in
        (* "proto_rules": lists of verified symbols: *)
        let proto_rules =
            List.map
            (fun rline ->
                (* First of all, split "rline" into LHS and RHS: *)
                match ExtString.String.nsplit rline " |=> " with
                [ [l; r] ->
                    (* Parse both LHS and RHS:  *)
                    let proto_l = split_atoms l   in
                    let proto_r = split_atoms r   in
                    (* Verify all the symbols, and update their freqs: *)
                    let ()  = List.iter verify_symbol proto_l in
                    let ()  = List.iter verify_symbol proto_r in
                    (* Generate a "proto-rule": *)
                    (proto_l, proto_r)
                | _ ->
                    (* Invalid format: *)
                    failwith ("Rules.Terms.parse_rules: Invalid input: "^rline)
                ]
            )
            rlines
        in
        (* Heuristically optimise the term ordering: the atoms/symbols which
           occur least frequently, will be assigned higher lexicograpgic pri-
           ority, and will be put in the head of the list. For that, put the
           symbols and their freqs into an array:
        *)
        let sfreqs = Array.make n_atoms ("", 0) in
        let ()     =
        do {
            (* Also check if there are any symbols which do not occur in the
               rules at all-- if so, produce an error, as it's in most cases
               a user error indeed:
            *)
            ignore (Hashtbl.fold
                   (fun s f i ->
                   do {
                     if  f = 0
                     then failwith ("Rules.Terms.parse_rules: Unused Atom: "^s)
                     else sfreqs.(i) := (s, f);
                     i+1
                   })
                   map_si 0);

            Array.sort (fun (_,f1) (_,f2) -> compare f1 f2) sfreqs;

            (* Now re-build "map_si", installing positional codes of all sym-
               bols instead of their freqs  (though these parameters are con-
               nected by a monotonic function, due to the above construction):
            *)
            Array.iteri
                (fun i (s,_) -> Hashtbl.replace map_si s i) sfreqs
        }
        in
        (* Construct the term ordering, and 2-direction maps between symbols
           and atoms. The order is made just for the range [0..n_atoms-1] of
           atom encodings, irrespective to their mapping to Symbols:
        *)
        let map_sa  = Hashtbl.create n_atoms   (* symbol-> atom   *)      in
        let map_as  = Hashtbl.create n_atoms   (* atom  -> symbol *)      in
        let torder  =
            TLen_Lex (A.mk_atoms (Array.mapi
                     (fun i (s,_) ->
                        let at = A.decode i in
                        do {
                            (* Side-effect: fill in the maps: *)
                            Hashtbl.add map_sa s  at;
                            Hashtbl.add map_as at s;
                            at
                        }
                     ) sfreqs))
        in
        let tenum   = mk_enumf torder in

        (* Terms from arrays of symbols. The symbol in question is looked up
           in the "map_si" for the code of its corresp atom:
        *)
        let term_of_proto: list string -> kb_term =
        fun proto ->
            mk_term (A.mk_atoms (Array.of_list (List.map
                        (fun s -> A.decode (Hashtbl.find map_si s)) proto
                    )))
                    tenum
        in
        (* Construct the final rules: *)
        let rules   =
            List.map
            (fun (proto_l, proto_r) ->
                let lhs = term_of_proto proto_l in
                let rhs = term_of_proto proto_r in
                mk_rule lhs rhs
            )
            proto_rules
        in
        (* The structure to return: *)
        let info =
        {
            atom_names    = map_as;
            atoms_by_name = map_sa;
            term_order    = torder;
            enumf         = mk_enumf torder
        }
        in
        (info, rules);

end;
