(*pp camlp4r *)
(* vim:ts=4:syntax=ocaml
*)
(*===========================================================================*)
(*                            "crit_pairs.ml":                               *)
(*     Interface to the Operations on Critical Pairs  of Reduction Rules     *)
(*               L.A.Merkin, IMPARTIAL Project, 2003--2005                   *)
(*===========================================================================*)
module type PAIRS =
sig
    (* The foundation "Rules.TERMS" module, which we re-export: *)
    module RT: Rules.TERMS;

    (* Comparison of [term]s is extended to [rewrite_rule]s (wrt the LHS first,
       RHS second), and in a similar way to [rule_pair]s. This constitutes the
       Normal Strategy of ordering the Priority Queue of Rule Pairs ([pairs_q]
       type). Comparison operations are exported for use by higher-level algs;
       they return {-1, 0, 1}:
    *)
    value cmp_rules      : RT.rewrite_rule -> RT.rewrite_rule-> int;
    value cmp_pairs      : RT.rule_pair    -> RT.rule_pair   -> int;

    (* Operations on Priority Queues of [rule_pair]s,  according to the Normal
       Strategy of ordering them; NB: [take_hd_pair] returns [None] as the 1st
       element of the result tuple if the queue was empty:
    *)
    type  pairs_q      = 'p;
    value empty_pqueue   : pairs_q;
    value is_empty_pqueue: pairs_q -> bool;
    value insert_pair    : pairs_q -> RT.rule_pair           -> pairs_q;
    value take_hd_pair   : pairs_q -> ((option RT.rule_pair)  *  pairs_q);

    (* Critical Pairs Completion (and thus New Rules Generation): *)
    value complete_crit_pairs:
        RT.term_enumf -> RT.rule_pair -> list RT.rewrite_rule;
end;

module CP (RT: Rules.TERMS): PAIRS;

