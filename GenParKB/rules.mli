(*pp camlp4r *)
(* vim:ts=4:syntax=ocaml
*)
(*===========================================================================*)
(*                                 "rules.ml":                               *)
(*   Terms, Re-Write Rules and Rules Storage in the Knuth-Bendix Algorithm   *)
(*                 L.A.Merkin, IMPARTIAL Project, 2004--2005                 *)
(*===========================================================================*)
(*----------------------*)
(*  "ATOMS" Interface:  *)
(*----------------------*)
module type ATOMS =
sig
    (* "atoms"  is a direct-access data structures of "atom"s;    "atom"s are
       enumerable, i.e., can be mapped one-to-one to a range  of non-negative
       integers:
    *)
    type  atom  = 'a;
    type  atoms = 's;

    value encode  : atom -> int;
    value decode  : int  -> atom;

    (* PRE/POST-CONDITIONS of the following functions:
       length empty        = 0
       length (single a)   = 1
       length (mk_atoms x) = List.length x
    *)
    value empty   : atoms;
    value single  : atom           -> atoms;
    value mk_atoms: array atom     -> atoms;
    value length  : atoms -> int;

    value get     : atoms -> int   -> atom; (* Direct R/O access-- no "put"! *)
    value concat  : atoms -> atoms -> atoms;
    value slice   : atoms -> int   -> int  -> atoms;
                          (* From,    Len *)
    value cmp     : atom  -> atom  -> int;

    (* Also provided:  more efficient (non-polymorphic) equality on [atoms]: *)
    value eqs     : atoms -> atoms -> bool;
end;

(*----------------------*)
(*  "TERMS" Interface:  *)
(*----------------------*)
module type TERMS =
sig
    (* Global Flags -- mostly for debugging: *)

    value term_equality_by_enums: ref bool;
    (* Normally [True];  may be set to [False] for debugging. *)

    value verify_found_lhs_enums: ref bool;
    (* Normally [False]; set to [True] for debugging. *)

    (* Re-export the "ATOMS" over which these "TERMS" are built: *)
    module  A: ATOMS;

    (* The following types are abstract, as they have integrity constraints: *)
    type kb_term       = 't;
    type rewrite_rule  = 'r;
    type rule_pair     = 'p;
    type rule_set      = 's;

    (* Term Orderings: Currently, only one term ordering is implemented: Total-
       Len Lexicographic. Internally, a given term ordering is translated into
       enumerators of terms. For the latter to be as compact as possible,   we
       notice that the range of [atom]s actually used may be smaller than that
       of the whole [atoms] type.   Thus, the user supplies the ACTUAL [atoms]
       which will appear in their terms, in the  LEXICOGRAPHICALLY  DECREASING
       order (the 0th atom has the highest weight):
    *)
    type term_order = [TLen_Lex of A.atoms];

    (* The ordering is then converted into an internal [term_enumf] function
       which is used to actually enumerate the terms.   The user MUST ensure
       that all terms used in a single computation are created with THE SAME
       INSTANCE of this function, or a run-time error will occur:
    *)
    type term_enumf = 'f;

    (* Access finctions.
       IMPORTANT INVARIANT: if (l,r) = get_pair p, then l > r according to the
       lexicographic ordering of the rules (LHS first) on top of [term_order]:
    *)
    value get_atoms  :  kb_term      ->  A.atoms;
    value get_enum   :  kb_term      ->  Big_int.big_int;
    value get_rule   :  rewrite_rule -> (kb_term * kb_term); (* (LHS, RHS) *)
    value get_pair   :  rule_pair    -> (rewrite_rule * rewrite_rule);

    (* Comparison: *)
    value terms_equal:  kb_term      -> kb_term      -> bool;
    value terms_cmp  :  kb_term      -> kb_term      -> int;

    (* Generators: *)
    value mk_enumf   :  term_order   -> term_enumf;
    value mk_term    :  A.atoms      -> term_enumf   -> kb_term;
    value mk_rule    :  kb_term      -> kb_term      -> rewrite_rule;
    value mk_pair    :  rewrite_rule -> rewrite_rule -> rule_pair;

    (* [rule_set] operations: *)
    value empty_rset :  rule_set;
    value insert_rule:  rule_set -> rewrite_rule -> rule_set;
    value delete_rule:  rule_set -> rewrite_rule -> rule_set;

    (* Iterators: *)
    value iter_rset  : (rewrite_rule -> unit)     -> rule_set -> unit;
    value fold_rset  : (rewrite_rule -> 'a -> 'a) -> rule_set -> 'a  -> 'a;

    (* Low-Level Reductions : *)

    value find_and_apply_rule :
        ?longest:bool -> ?min_off:int -> ?max_off:int ->
        rule_set      -> A.atoms      -> term_enumf   ->
        option A.atoms;
        (* Returns the rewritten target, or [None] if no rewriting was possi-
           ble. If [longest] is [True] (by default [False]), the longest-LHS
           rule (for each given offset in the target) will be sought;  [min_
           off] and [max_off] control the range of offsets  (by default, the
           whole [target] can be used in the matching process):
           [min_off <= offset <= max_off]. See the implementation remarks as
           well.
        *)

    value find_and_apply_rule_ext:
        ?longest:bool -> ?min_off:int -> ?max_off:int ->
        rule_set      -> A.atoms      -> term_enumf   ->
        option (rewrite_rule * A.atoms);
    (* As [find_and_apply_rule], but also returns the rule used in reduction *)

    (* High-Level Reductions: *)

    (* "reduce_wrt_set":
       Returns [None] if the rule has been eliminated (reduced to a Tautology),
       or returns a possibly modified rule, with a flag indicating whether it
       was actually modified:
    *)
    value reduce_wrt_set:
        rule_set -> rewrite_rule -> term_enumf ->
        option     (rewrite_rule * bool);

    (* "reduce_wrt_each_other":
       Reduces all elements of the given rule set modulo the rest, until no more
       reductions are possible:
    *)
    value reduce_wrt_each_other:  rule_set -> term_enumf -> rule_set;

    (* Parsing of textual representation of rules.  The arg is a list of rules
       the form "LHS |=> RHS", where LHS and RHS are sequences of Symbols sepa-
       rated by "\s+":
    *)
    type terms_info =
    {
        atom_names    : Hashtbl.t A.atom string;
        atoms_by_name : Hashtbl.t string A.atom;
        term_order    : term_order;
        enumf         : term_enumf
    };
    value parse_rules: list string -> (terms_info * (list rewrite_rule));
end;

(*--------------------------------------*)
(*  Functorial "TERMS" implementation:  *)
(*--------------------------------------*)
module Terms (A: ATOMS): TERMS;

