(*pp camlp4r *)
(* vim:ts=4:syntax=ocaml
*)
(*===========================================================================*)
(*                            "crit_pairs.ml":                               *)
(*            Operations on Critical Pairs  of Reduction Rules               *)
(*               L.A.Merkin, IMPARTIAL Project, 2003--2005                   *)
(*===========================================================================*)
(* See the Intreface for more detail: *)
module type PAIRS =
sig
    module  RT: Rules.TERMS;

    value cmp_rules      : RT.rewrite_rule -> RT.rewrite_rule -> int;
    value cmp_pairs      : RT.rule_pair    -> RT.rule_pair    -> int;

    (* Priority Queue of Pairs to be Completed: *)
    type  pairs_q        = 'p;
    value empty_pqueue   : pairs_q;
    value is_empty_pqueue: pairs_q -> bool;
    value insert_pair    : pairs_q ->  RT.rule_pair         -> pairs_q;
    value take_hd_pair   : pairs_q -> ((option RT.rule_pair) *  pairs_q);

    (* Critical Pairs Completion: *)
    value complete_crit_pairs:
        RT.term_enumf -> RT.rule_pair -> list RT.rewrite_rule;
end;

module CP (RT: Rules.TERMS) =
struct
    (*==================================*)
    (*  Priority Queues of Rule Pairs:  *)
    (*==================================*)
    (*  Currently, implemented using Red-Black Trees as well. The pairs are
        compared according to the NORMAL STRATEGY:
    *)
    module POrd =
    struct
        type  t = RT.rule_pair;

        (* Comparison of Rules: the following function is NOT the same as the
           one used in Rule Storage, as it is used here for ordering of pairs
           according to the Normal Strategy,  rather than for fast search for
           reducers:
        *)
        value cmp_rules:  RT.rewrite_rule -> RT.rewrite_rule -> int =
        fun rr1 rr2 ->
            let (l1,r1) = RT.get_rule rr1    in
            let (l2,r2) = RT.get_rule rr2    in
            let  c1     = RT.terms_cmp l1 l2 in
            if   c1 <> 0
            then c1
            else RT.terms_cmp r1 r2;
            (* NB: it's not an error to have equal rules here! *)

        (* Similarly, comparison of Rule Pairs: *)
        value compare: t -> t -> int =
        fun p1 p2 ->
            let (l1,r1) = RT.get_pair p1 in
            let (l2,r2) = RT.get_pair p2 in
            let  c1  = cmp_rules l1 l2   in
            if   c1 <> 0
            then c1
            else       cmp_rules r1 r2;
            (* Again, equal pairs are in general OK, though of course dupli-
               cates will not be stored in the Red-Black Set!
            *)
    end;
    (* Now: the Pairs Priority Queue implementation. We are using the Red-Black
       Map: rule_pair |=> unit. This is XXX slightly sub-optimal from the memo-
       ry allocation point of view  (an extra word representing () is allocated
       for each pair stored), but the overhead  is minimal compared to the size
       of data stored:
    *)
    module RT       = RT;
    module PQueue   = Red_black.Map (POrd);
    type pairs_q    = PQueue.m unit;

    (*---------------------*)
    (*  Queue Operations:  *)
    (*---------------------*)
    (* "*empty_pqueue": *)
    value empty_pqueue   : pairs_q         = PQueue.empty;
    value is_empty_pqueue: pairs_q -> bool = PQueue.is_empty;

    (* "insert_pair":   *)
    value insert_pair    : pairs_q -> RT.rule_pair -> pairs_q =
    fun queue pair ->
        PQueue.add queue pair ();

    (* "take_hd_pair":  *)
    value take_hd_pair   : pairs_q -> ((option RT.rule_pair) * pairs_q) =
    fun queue ->
        match PQueue.take_min queue  with
        [ (None, queue')           -> (None,    queue')
        | (Some (mp, ()), queue')  -> (Some mp, queue')
        ];

    (*==============================*)
    (*  Critical Pairs Generation:  *)
    (*==============================*)
    (* Each Pair of Rules can potentially yield multiple Critical Pairs, and
       therefore a list of new Rules (to be reduced later on).
       The functions "cps1" and "cps2" return "pre-rules" (pairs of [atoms])
       constructed from the critical pairs of the 1st and the 2nd kind, resp,
       derived from a given single pair of rules:
    *)
    (*---------------------------------*)
    (* Critical Pairs of the 1st Kind: *)
    (*---------------------------------*)
    (* For rules (l1,r1), (l2,r2), find all common factors "t" such that
            l1 = t y
            l2 = x t
       In that case,
            x l1 = l2 y = x t y,
       but
            l1 ~ r1 and l2 ~ r2,
       and therefore also
            x r1 ~ r2 y
       which is the new rule to be created (for each such "t").
       Obviously, 1 <= len t <= min (len l1) (len l2);
       if len t = 0, then x = l2 and y = l1, and we have a trivial consequence
       of the original rules;
       on the other hand, empty x or y ((len t) reaching the upper limit)  are
       in general OK.
       If l1 = l2 (both x and y empty), we will have
            r1 ~ r2
        which is OK if it's not a Tautology, but would fail otherwise:
    *)
    value cps1: RT.term_enumf ->
                RT.A.atoms    ->   RT.A.atoms -> RT.A.atoms -> RT.A.atoms ->
                list (RT.A.atoms * RT.A.atoms) =
    fun enumf   l1 r1 l2 r2          ->
        let len1  = RT.A.length l1 in
        let len2  = RT.A.length l2 in
        let max_lt= min  len1 len2 in
        let rec mks1 =
            (* 0 <= i < max_lt is the offset from the left of "l1" and the rght
               of "l2", where the common factor "t" is still sought:
            *)
            fun i curr ->
                if  i >= max_lt   ||
                    RT.A.get l1 i <> RT.A.get l2 (len2-1-i)
                then
                    (* Reached the maximum possible length for "t", or got a
                       mismatch -- no more common atoms, stop now:
                    *)
                    curr
                else
                    (* (Still) equal atoms -- currently len t = i+1: *)
                    let y    = RT.A.slice  l1 (i+1) (len1-i-1) in
                    let x    = RT.A.slice  l2 0     (len2-i-1) in
                    let x_r1 = RT.A.concat x  r1      in
                    let r2_y = RT.A.concat r2 y       in
                    let curr'=
                        if  RT.A.eqs x_r1 r2_y
                        then
                            curr (* Would be a Tautology -- skip it  *)
                        else
                            [(x_r1, r2_y) :: curr]
                    in
                    mks1 (i+1) curr'  (* Try more! *)
        in
        mks1 0 [];

    (*---------------------------------*)
    (* Critical Pairs of the 2nd Kind: *)
    (*---------------------------------*)
    (* For rules (l1,r1), (l2,r2), find all prefixes "x" (and thus postfixes
       "y") such that
            l1 = x l2 y .
       This is of course only possible if (l1 >= l2) wrt any admissible term
       ordering used. This is ALREADY GUARANTEED by ordering of the rules in
       "RT.get_pair" result.
       If so, since
            l1 ~ r1 and l2 ~ r2,
       we also have
            r1 ~ x r2 y
       which is the new rule to be created (for each such "x");
       in general,
            len x = 0 and/or len y = 0 is allowed.
       However, the case len x = 0 coincides with len x = 0 in Kind 1 Pairs,
       so it will be omitted here. Thus,
            1 <= len x <= len l1 - len l2
            0 <= len y <= len l1 - len l2 - 1
            1 <= lem l1 - len l2 .
       If l1 = l2 (so both x and y are empty), we have
            r1 ~ r2
       (similar to Kind 1), which is OK iff it is not a Tautology.
       The computational comlexity here is quadratic, as opposed to linear in
       "cps1":
    *)
    value cps2: RT.term_enumf ->
                RT.A.atoms    ->   RT.A.atoms  -> RT.A.atoms -> RT.A.atoms ->
                list (RT.A.atoms * RT.A.atoms) =
    fun enumf   l1 r1 l2 r2         ->
        let len1  = RT.A.length l1 in
        let len2  = RT.A.length l2 in
        let max_i = len1 - len2    in
        (* Rules with empty LHSs are not allowed, as the RHSs must be still
           less, -- such rules do not exist. Also, len l1 >= len l2  by the
           guaranteed property of pair construction, so max_i >= 0.
           Furthermore, if len l1 = len l2, do not proceed-- it can only be
           when len x = len y = 0, which we decided to exclude:
        *)
        let ()    = assert (len1 >= 1 && len2 >= 1 && max_i >= 0) in
        if  max_i = 0
        then
            []
        else
        (* What is more efficient -- the following function, or taking a slice
           of "l1" and comparing it to "l2" by "RT.A.cmp_terms"? -- Still, not
           doing allocations is probably a good idea;
           "cmp_from" returns "True" iff "l2" is sub-sequence of "l1" starting
           from pos "i":
        *)
        let rec cmp_from: int -> int -> bool =
            (* "i" is the curr pos in "l1", "j" is the curr pos in "l2": *)
            fun i j ->
                if  j >= len2
                then True     (* Finished "l2", all atoms match! *)
                else
                let () = assert (i < len1) in
                if   RT.A.get l1 i <> RT.A.get l2 j
                then False    (* Mismatch *)
                else cmp_from (i+1) (j+1)
        in
        (* Actual generator; "i" is the curr length of the prefix "x": *)
        let rec mks2 =
            fun i curr ->
                if  i > max_i
                then
                    curr (* All done *)
                else
                let curr' =
                    if  cmp_from i 0
                    then
                        (* Yes, "l2" is a sub-sequence of "l1".
                           NB:  max_i - i = len1 - i - len2:
                        *)
                        let x      = RT.A.slice l1 0 i                in
                        let y      = RT.A.slice l1 (i+len2) (max_i-i) in
                        let x_r2_y = RT.A.concat (RT.A.concat x r2) y in
                        if  RT.A.eqs r1 x_r2_y
                        then
                            curr (* Would be a Tautology -- skip it *)
                        else
                            [(r1, x_r2_y) :: curr]
                    else
                        curr (* No match at this position *)
                in
                mks2 (i+1) curr'
        in
        mks2 1 [];   (* (len x) starts from 1! *)

    (*---------------------------*)
    (* Critical Pairs Top-Level: *)
    (*---------------------------*)
    value complete_crit_pairs:
        RT.term_enumf -> RT.rule_pair -> list RT.rewrite_rule =
    fun enumf p ->
        let (rr1, rr2) = RT.get_pair p   in
        let (a1,   b1) = RT.get_rule rr1 in
        let (a2,   b2) = RT.get_rule rr2 in
        let l1         = RT.get_atoms a1 in
        let r1         = RT.get_atoms b1 in
        let l2         = RT.get_atoms a2 in
        let r2         = RT.get_atoms b2 in
        let kind1      = cps1  enumf  l1 r1 l2 r2 in
        let kind2      = cps2  enumf  l1 r1 l2 r2 in
        let all        = kind1 @ kind2   in
        (* Is it possible that identical critical pairs  have been created?
           In general, yes  -- so use "unique" with our custom equality to
           eliminate them:
        *)
        let uniq       =
            ExtList.List.unique
            ~cmp:(fun (l1,r1) (l2,r2) -> (RT.A.eqs l1 l2) && (RT.A.eqs r1 r2))
            all
        in
        (* Make the rules from the (LHS, RHS) pairs retained. NB: tautologies
           are explicitly prevented from being constructed -- no need to catch
           the corresp exception:
        *)
        List.fold_left
            (fun curr (l, r) ->
                 if  RT.A.eqs l r
                 then curr (* Tautology -- skip it! *)
                 else
                 let  nr =
                      RT.mk_rule (RT.mk_term l enumf) (RT.mk_term r enumf)
                 in
                 [nr::curr]
            )
            [] uniq;

    (*=======*)
    (* Misc: *)
    (*=======*)
    (*--------------------------------*)
    (* Exported Comparison Functions: *)
    (*--------------------------------*)
    value cmp_rules: RT.rewrite_rule -> RT.rewrite_rule -> int =
        POrd.cmp_rules;

    value cmp_pairs: RT.rule_pair    -> RT.rule_pair    -> int =
        POrd.compare;
end;
