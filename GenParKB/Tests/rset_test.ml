(*pp camlp4r *)
(* vim:ts=4:syntax=ocaml
*)
(*===========================================================================*)
(*                             "rset_test.ml":                               *)
(*                     Unit test for the Rules Set NFA                       *)
(*                   L.A.Merkin, IMPARTIAL Project, 2004                     *)
(*===========================================================================*)
open  Rules.Atoms_Compact;
open  Rules.Terms_Compact;

value p0 = Unix.gettimeofday ();

value usage: unit -> 'a =
fun () ->
do {
    prerr_endline "PARAMETER: <N_Tests> [--realistic]";
    exit 1
};

value n_tests =
    try  int_of_string Sys.argv.(1)
    with [_ -> usage ()];

value realistic = Array.length Sys.argv >=3 && Sys.argv.(2) = "--realistic";

Rules.verify_found_lhs_enums.val := not realistic;

value alphabet = "abcdef";
value alen     = String.length alphabet;

value enumf    = mk_enumf (TLen_Lex alphabet);

value random_term: unit -> kb_term =
fun () ->
    let len = Random.int  128     in
    let str = String.make len ' ' in
    let _   =
        for i = 0 to len-1
        do {
            str.[i] := alphabet.[Random.int alen]
        }
    in
    mk_term str enumf;

value rec random_rule: unit -> rewrite_rule =
fun () ->
    let t1 = random_term () in
    let t2 = random_term () in
    if  terms_equal t1 t2
    then
        (* Try again! *)
        random_rule ()
    else
        mk_rule t1 t2;

(* Create random rules, insert them into the list first: *)

value rec create_rules: int -> list rewrite_rule -> list rewrite_rule =
fun i curr_lst ->
    if  i <= 0
    then curr_lst
    else create_rules (i-1) [(random_rule ()) :: curr_lst];

value rlist1 = create_rules n_tests [];

value p1 = Unix.gettimeofday ();

(* Now insert them into the Rule Set: *)

value rset1 = List.fold_left
    (fun curr_set r -> insert_rule curr_set r) empty_rset rlist1;

value p2 = Unix.gettimeofday ();

(* Now find and apply each of these rules: *)

value rec find_rules: list rewrite_rule -> rule_set -> unit =
fun  rlist rset ->
List.iter
    (fun r ->
        let (lhs, rhs) = get_rule r in
        let _ =
            (* Possibly check the ordering of sides in th erule: *)
            if  (not realistic) && (terms_cmp lhs rhs <> 1)
            then
                Printf.printf "ORDERING ERROR:\nLHS=%s\nRHS=%s\n\n%!"
                              (get_atoms lhs) (get_atoms rhs)
            else ()
        in
        (* The term we will re-write: *)
        let t   = mk_term ("aaaaa" ^ (get_atoms lhs) ^ "fffff") enumf in

        (* Do search and rewriting: *)
        if  realistic
        then
            let hmm = find_and_apply_rule rset t enumf in
            match hmm with
            [ None ->
                print_endline ("SEARCH FAILED: "^(get_atoms lhs))
            | Some _ ->
                ()
            ]
        else
            (* Testing mode: *)
            let hmm =
                find_and_apply_rule_ext ~min_off:5 ~longest:True rset t enumf
            in
            match hmm with
            [ None    ->
                print_endline ("SEARCH FAILED: "^(get_atoms lhs))
            | Some (rule, t') ->
                if  "aaaaa" ^ (get_atoms rhs) ^ "fffff" <> get_atoms t'
                then
                    let (rule_left, rule_right) = get_rule rule in
                    do {
                        Printf.printf
                            "HMM:\nORIG_LHS=%s\nORIG_RHS=%s\nTARGET  =%s\n%!"
                            (get_atoms lhs) (get_atoms rhs) (get_atoms t);

                        Printf.printf
                            "GOT_LHS =%s\nGOT_RHS =%s\nREWRITTEN=%s\n\n%!"
                            (get_atoms rule_left) (get_atoms rule_right)
                            (get_atoms t')
                    }
                else ()
            ]
    )
    rlist;

find_rules rlist1 rset1;

value p3 = Unix.gettimeofday ();

(* Now delete HALF of the rules by one: *)
value rec del_rules:
    int -> list rewrite_rule -> list rewrite_rule -> rule_set ->
    ((list rewrite_rule) * (list rewrite_rule) * rule_set)    =

fun count curr_lst del_lst curr_set ->
    if  count < 0
    then
        (curr_lst, del_lst, curr_set)
    else
    match curr_lst with
    [ [] ->
        (curr_lst, del_lst, curr_set)

    | [r::rs] ->
        let rset' = delete_rule curr_set r in
        del_rules (count-1) rs [r::del_lst] rset'
    ];

value (rlist2, rlist_del, rset2) = del_rules (n_tests/2) rlist1 [] rset1;

value p4 = Unix.gettimeofday ();

find_rules rlist2 rset2;

value p5 = Unix.gettimeofday ();

value total_time  = p5 -. p0;
value create_time = p1 -. p0;
value create_pct  = create_time /. total_time *. 100.0;
value store_time  = p2 -. p1;
value store_pct   = store_time  /. total_time *. 100.0;
value search1_time= p3 -. p2;
value search1_pct = search1_time/. total_time *. 100.0;
value delete_time = p4 -. p3;
value delete_pct  = delete_time /. total_time *. 100.0;
value search2_time= p5 -. p4;
value search2_pct = search2_time/. total_time *. 100.0;

Printf.printf "Creation: %.2f sec (%.2f %%)\n%!" create_time  create_pct;
Printf.printf "Storing : %.2f sec (%.2f %%)\n%!" store_time   store_pct;
Printf.printf "Search-1: %.2f sec (%.2f %%)\n%!" search1_time search1_pct;
Printf.printf "Delete  : %.2f sec (%.2f %%)\n%!" delete_time  delete_pct;
Printf.printf "Search-2: %.2f sec (%.2f %%)\n%!" search2_time search2_pct;
Printf.printf "TOTAL   : %.2f sec\n%!"           total_time;

exit 0;

