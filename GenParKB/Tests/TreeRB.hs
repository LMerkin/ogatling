-- vim:ts=4:syntax=haskell
-- ========================================================================= --
--							  	 "TreeRB.hs":								 --
--			Red-Black Trees in Haskell, by C.Okasaki and S.M.Kahrs			 --
-- 			    Adapted by L.A.Merkin for the IMPARTIAL Project			     --
--								  1998--2004								 --
-- ========================================================================= --
module TreeRB
(
	RBT,					-- Abstract type
	insert,					-- Ord a => (a -> a -> a) -> a -> RBT a -> RBT a
	member,					-- Ord a => a -> RBT a -> Bool
	delete,					-- Ord a => a -> RBT a -> RBT a
	isEmpty,  isSingleton,	-- RBT a -> Bool
	emptyRBT, singletonRBT, -- RBT a
	singleElt,				-- RBT a -> a

	Traverse_Flag (..),
	foldRBT,				-- (b -> a -> (b, Traverse_Flag)) ->
							--  b -> RBT a -> (b, Bool)

	parRBT, seqRBT,			-- Strategy a -> Strategy (RBT a)
	findEq,					-- Ord a => a -> RBT a -> Maybe a
	findMin, findMax,		-- Ord a => RBT a -> a
	deleteMin, deleteMax,	-- Ord a => RBT a -> RBT a
	takeMin, takeMax		-- Ord a => RBT a -> (a, RBT a)
)
where
import Parallel
import Strategies

-- REMARKS on PARALLELISATION:
-- Probably no point in parallelising "insert", "member", "delete", "*empty*":
-- for all function applications in them,
-- 		f x y ... z ,
-- the args  x, y, ..., z  contain no more than one function application among
-- themselves. Also, "foldRBT" is inherently sequential.
-- However, a number of evaluation strategies on "RBT"s are supported.

data Color = R | B deriving Show
data RBT a = E | T Color (RBT a) a (RBT a) deriving Show

-----------------
--	"insert":  --
-----------------
-- NB: on equal elements, the recombination function is invoked:

insert :: Ord a => (a -> a -> a) -> a -> RBT a -> RBT a
insert recomb x s =
	T B a z b
	where
	T _ a z b = ins s
	ins E = T R E x E
	ins (T B l y r)
		| x<y = balance (ins l) y r
		| x>y = balance l y (ins r)
		| otherwise = T B l (recomb x y) r
	ins (T R l y r)
		| x<y = T R (ins l) y r
		| x>y = T R l y (ins r)
		| otherwise = T R l (recomb x y) r

-----------------
--	"member":  --
-----------------
member :: Ord a => a -> RBT a -> Bool
member _ E = False
member x (T _ l y r)
	| x<y = member x l
	| x>y = member x r
	| otherwise = True

-----------------
--	"findEq":  --
-----------------
--	Tries to find an element in the Tree which is equal (in the sense of Eq,
--	not necessetily structurally equal!) to the given one:
--
findEq :: Ord a => a -> RBT a -> Maybe a
findEq _ E = Nothing
findEq x (T _ l y r)
	| x<y = findEq x l
	| x>y = findEq x r
	| otherwise = Just y

-------------------
--	"emptyRBT":  --
--	"isEmpty" :  --
-------------------
emptyRBT::  RBT a
emptyRBT =  E

isEmpty ::  RBT a -> Bool
isEmpty E = True
isEmpty _ = False

-----------------------
--	"singletonRBT":  --
--  "isSingleton" :  --
--	"singleElt"   :  --
-----------------------
singletonRBT :: a -> RBT a
singletonRBT x = T R E x E

isSingleton  :: RBT a -> Bool
isSingleton  (T R E _ E) = True
isSingleton  _			 = False

singleElt    :: RBT a -> a
singleElt    (T R E x E) = x
singleElt    _			 = error "TreeRB.singleElt: Not a singleton tree"

----------------
-- "balance": --
----------------
--	First equation is new, to make it work with a weaker invariant.
--	NB: "balance"  is NOT recursive, so it's of O(1) complexity!

balance :: RBT a -> a -> RBT a -> RBT a
balance (T R a x b) y (T R c z d) = T R (T B a x b) y (T B c z d)
balance (T R (T R a x b) y c) z d = T R (T B a x b) y (T B c z d)
balance (T R a x (T R b y c)) z d = T R (T B a x b) y (T B c z d)
balance a x (T R b y (T R c z d)) = T R (T B a x b) y (T B c z d)
balance a x (T R (T R b y c) z d) = T R (T B a x b) y (T B c z d)
balance a x b = T B a x b

-----------------
--	"delete":  --
-----------------
-- Deletion a la SMK. Deletion IS a recursive operation (as "delform*" invoke
--	"del" recursively, and also because "app" is recursive), so BEWARE of its
--  complexity!
--
delete :: Ord a => a -> RBT a -> RBT a
delete x t =
	case del t of {T _ a y b -> T B a y b; _ -> E}
	where
	del E = E
	del (T _ a y b)
	    | x<y = delformLeft a y b
	    | x>y = delformRight a y b
		| otherwise = app a b
	delformLeft a@(T B _ _ _) y b = balLeft (del a) y b
	delformLeft a y b = T R (del a) y b
	delformRight a y b@(T B _ _ _) = balRight a y (del b)
	delformRight a y b = T R a y (del b)

balLeft :: RBT a -> a -> RBT a -> RBT a
balLeft (T R a x b) y c = T R (T B a x b) y c
balLeft bl x (T B a y b) = balance bl x (T R a y b)
balLeft bl x (T R (T B a y b) z c) = T R (T B bl x a) y (balance b z (sub1 c))
balLeft _ _ _ = error "RedBlack_Tree.delete: Invariance Violation (1)"

balRight :: RBT a -> a -> RBT a -> RBT a
balRight a x (T R b y c) = T R a x (T B b y c)
balRight (T B a x b) y bl = balance (T R a x b) y bl
balRight (T R a x (T B b y c)) z bl = T R (balance (sub1 a) x b) y (T B c z bl)
balRight _ _ _ = error "RedBlack_Tree.delete: Invariance Violation (2)"

sub1 :: RBT a -> RBT a
sub1 (T B a x b) = T R a x b
sub1 _ = error "RedBlack_Tree.delete: Invariance Violation (3)"

app :: RBT a -> RBT a -> RBT a
app E x = x
app x E = x
app (T R a x b) (T R c y d) =
	case app b c of
	    T R b' z c' -> T R(T R a x b') z (T R c' y d)
	    bc -> T R a x (T R bc y d)
app (T B a x b) (T B c y d) = 
	case app b c of
	    T R b' z c' -> T R(T B a x b') z (T B c' y d)
	    bc -> balLeft a x (T B bc y d)
app a (T R b x c) = T R (app a b) x c
app (T R a x b) c = T R a x (app b c)

------------------
--	"foldRBT":  --
------------------
--  The current node is always processed BEFORE the sub-trees. The "action
--	function"  returns  a value  and a 5-state flag  which directs further
--	traversing. If the flag is "Stop_Now", no  further traversing  is done
--	at all.  The "foldRBT" function returns a Bool flag indicating whether
--	the whole structure has been traversed:
--
data Traverse_Flag =
	Left_Only
  | Right_Only
  | Left_Right
  | Right_Left
  | Stop_Now

foldRBT  :: (b -> a -> (b, Traverse_Flag)) -> b -> RBT a -> (b, Bool)

foldRBT f b0 tree = foldRBT' f (b0,True) tree


foldRBT' :: (b -> a -> (b, Traverse_Flag)) ->(b,Bool) -> RBT a -> (b,Bool)

foldRBT' _ bf E = bf

foldRBT' f (b, cont) (T _ left  a right) =
	if  cont
	then
		-- Yes, continue for completion:
		let   (b', flag) = f b a
		in
			case flag of
				Left_Only  ->
					let (b'', _) = foldRBT' f (b', True) left
					in  (b'',False)

				Right_Only ->
					let (b'', _) = foldRBT' f (b', True) right
					in  (b'',False)

				Left_Right ->
					let bl = foldRBT' f (b', True) left
					in       foldRBT' f  bl        right

				Right_Left ->
					let br = foldRBT' f (b', True) right
					in		 foldRBT' f  br		   left

				Stop_Now   ->
					(b', False)
	else
		-- No, further folding has been stopped, and did not run to completion:
		(b, False)

-------------------------
--	Head Normal Form:  --
-------------------------
--	"Very sequential" evaluation strategy:
--
instance NFData a => NFData (RBT a) where
	rnf  E = ()
	rnf (T _ left a right) = (rnf left) `seq` (rnf a) `seq` (rnf right)

-----------------
--	"parRBT":  --
-----------------
--	Parallel strategy for the tree itself,  parameterised by some strategy
--	for the nodes. We evaluate the curr node in the main thread, and spark
--	sub-threads for sub-trees:
--
parRBT :: Strategy a -> Strategy (RBT a)
parRBT  strat  =
	\ t ->
	case t of
		E -> ()
		T _ left a right ->
			(parRBT strat left ) `par`
			(parRBT strat right) `par`
			(strat  a)			 `seq`
			()

-----------------
--	"seqRBT":  --
-----------------
--	Sequential strategy for the tree itself. Unlike "rnf" in the "NFData"
--	class, the strategy for the nodes can be different from `seq`:
--
seqRBT :: Strategy a -> Strategy (RBT a)
seqRBT  strat  =
	\ t ->
	case t of
		E -> ()
		T _ left a right ->
			(seqRBT strat left ) `seq`
			(seqRBT strat right) `seq`
			(strat  a)

------------------
--	"findMin":  --
--	"findMax":  --
------------------
findMin :: RBT a -> a
findMin E = error "TreeRB.findMin: Empty tree"
findMin (T _ left a _) =
	case left of
		E ->  a  -- No way further to the left
		_ ->  findMin left

findMax :: RBT a -> a
findMax E = error "TreeRB.findMax: Empty tree"
findMax (T _ _ a right) =
	case right of
		E -> a -- No way further to the right
		_ -> findMax right

--------------------
--	"deleteMin":  --
--  "deleteMax":  --
--------------------
deleteMin :: Ord a => RBT a -> RBT a
deleteMin E = E
deleteMin t = delete (findMin t) t

deleteMax :: Ord a => RBT a -> RBT a
deleteMax E = E
deleteMax t = delete (findMax t) t

------------------
--	"takeMin":  --
--	"takeMax":  --
------------------
takeMin :: Ord a => RBT a -> (a, RBT a)
takeMin t = (a, delete a t)
	where    a = findMin t

takeMax :: Ord a => RBT a -> (a, RBT a)
takeMax t = (a, delete a t)
	where    a = findMax t

