(*---------------------*)
(*  "used_udp_addrs":  *)
(*---------------------*)
(*  XXX: currently, implementation of this function is Linux-specific. It re-
  turns a list of currently-bound UDP addressed on the local host:
*)
value used_udp_addrs: unit -> list Unix.sockaddr =
fun () ->
  let lines   = Misc_utils.read_lines "/proc/net/udp"   in

  (* We need a Hashtbl here as there may be duplicates in the addresses: *)
  let all_udp = Hashtbl.create 512 in
  let ()      =
  List.iter
    (fun line ->
    try
      (* Parse the "line": *)
      let sline  = Misc_utils.strip line        in
      let flds   = Misc_utils.split_by_whites sline in
      let addr_s = List.nth 1   flds          in

      (* Get the local address of it: *)
      let (ip4, port) =
        Scanf.sscanf addr "%02x%02x%02x%02x:%04x"
               (fun i1 i2 i3 i4 p -> ((i1,i2,i3,i4), p))
      in
      let addr   = Unix.ADDR_INET
             (Misc_utils.inet_addr_of_ip4 ip4) port
      in
      Hashtbl.replace all_udp addr ()
    with
      [_ -> ()]
    )
    lines
  in
  (* Return a list: *)
  Hashtbl.fold (fun addr _ curr -> [addr:curr]) all_udp [];

