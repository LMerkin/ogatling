(*pp camlp4r *)
(* vim:ts=4:syntax=ocaml
*)
(*===========================================================================*)
(*                            "crit_pairs.ml":                               *)
(*     Interface to the Operations on Critical Pairs  of Reduction Rules     *)
(*               L.A.Merkin, IMPARTIAL Project, 2003--2005                   *)
(*===========================================================================*)
module type PAIRS =
sig
    (* The foundation "Rules.TERMS" module, which we re-export: *)
    module RT: Rules.TERMS;

    (* Comparison of [term]s is extended to [rewrite_rule]s  (wrt the LHS first,
       RHS second), and in a similar way to [rule_pair]s.  This constitutes the
       Normal Strategy of formaling the Priority Queue of Rule Pairs ([pairs_q]
       type):
    *)
    type  pairs_q      = 'q;

    (* Operations on Priority Queues of [rule_pair]s,  according to the Normal
       Strategy of ordering them; NB: [take_hd_pair] returns [None] as the 1st
       element of the result tuple if the queue was empty:
    *)
    value empty_queue  : pairs_q;
    value insert_pair  : pairs_q    -> RT.rule_pair           -> pairs_q;
    value take_hd_pair : pairs_q    -> ((option RT.rule_pair) *  pairs_q);

    (* Critical Pairs (and thus New Rules) Generation: *)
    value mk_crit_pairs: term_enumf -> rule_pair -> list rewrite_rule;
end;

module CP (RT: Rules.TERMS):
          (PAIRS with type term_enumf   = RT.term_enumf   and
                      type rewrite_rule = RT.rewrite_rule and
                      type rule_pair    = RT.rule_pair);

