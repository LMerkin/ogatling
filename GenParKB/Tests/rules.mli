(*pp camlp4r *)
(* vim:ts=4:syntax=ocaml
*)
(*===========================================================================*)
(*                                  "rules.mli":                             *)
(*  Generative Interface to Knuth-Bendix Terms, Re-Write Rules and Rules NFA *)
(*                      L.A.Merkin, IMPARTIAL Project, 2004                  *)
(*===========================================================================*)
(*==========================*)
(*  The "ATOMS" signature:  *)
(*==========================*)
module type ATOMS =
sig
    type  atom  = 'a;
    type  atoms = 's;

    (* "atoms" is a direct-access data structures of "atom"s: *)
    value get   : atoms -> int   -> atom;
    value empty : atoms;
    value single: atom  -> atoms;
    value concat: atoms -> atoms -> atoms;
    value length: atoms -> int;
    value slice : atoms -> int   -> int  -> atoms;
                        (* From,    Len *)
    value cmp   : atom  -> atom  -> int;
end;

(*---------------------*)
(*  "Compact" "ATOMS": *)
(*---------------------*)
(*  If the cardinality of Atoms is up to 256: *)
module Atoms_Compact: ATOMS (*with type atom = char and type atoms = string*);

(*---------------------*)
(*  "General" "ATOMS": *)
(*---------------------*)
(*  For the cardinality of up to 2^30-1: *)
module Atoms_General: ATOMS with type atom = int and type atoms = array int;

(*==============================*)
(*  Terms, Rules and Rules NFA: *)
(*==============================*)
module type TERMS = functor (A: ATOMS) ->
sig
    (* The following types are abstract, as they have integrity constarints: *)
    type kb_term      = 't;
    type rewrite_rule = 'r;
    type rule_pair    = 'p;
    type rule_set     = 's;

    (* Access: *)
    value get_atoms  :  kb_term      ->  A.atoms;
    value get_rule   :  rewrite_rule -> (kb_term * kb_term); (* (LHS, RHS) *)
    value get_pair   :  rule_pair    -> (rewrite_rule * rewrite_rule);

    (* Generators: *)
    value mk_term    :  A.atoms      -> kb_term;
    value mk_rule    :  kb_term      -> kb_term      -> rewrite_rule;
    value mk_pair    :  rewrite_rule -> rewrite_rule -> rule_pair;

    (* [rule_set] operations: *)
    value empty_rs   : rule_set;
    value insert_rule: rule_set -> rewrite_rule -> rule_set;
    value delete_rule: rule_set -> rewrite_rule -> rule_set;

    value find_and_apply_rule :    rule_set     -> kb_term -> option kb_term;
        (* Returns a rewritten term, or [None] if no rewriting was possible *)
end;

module Terms: TERMS;

(*----------------------*)
(*  "Compact" "Terms":  *)
(*----------------------*)
module Terms_Compact: TERMS(Atoms_Compact);

