(*--------------*)
(*  Instances:  *)
(*--------------*)
(*  Atoms / Terms represented via Chars / Strings: *)
module Atoms_Compact: ATOMS with type atom = char and type atoms = string =
struct
  type  atom  = char;
  type  atoms = string;

  value get   : atoms -> int   -> atom        = String.get;
  value empty : atoms               = "";
  value single: atom  -> atoms          = String.make 1;
  value concat: atoms -> atoms -> atoms     = \^;
  value length: atoms -> int            = String.length;
  value slice : atoms -> int   -> int  -> atoms   = String.sub;
  value cmp   : atom  -> atom  -> int       =
    fun l r -> compare (l:char) (r:char);

  value string_of_atom:  atom  -> string      = single;
  value string_of_atoms: atoms -> string      = fun ats -> ats;
end;

(*  Atoms / Terms represented via Ints / Int Arrays: *)
module Atoms_General: ATOMS with type atom = int and type atoms = array int =
struct
  type  atom  = int;
  type  atoms = array int;

  value get   : atoms -> int   -> atom        = Array.get;
  value empty : atoms               = [| |];
  value single: atom  -> atoms          = Array.make 1;
  value concat: atoms -> atoms -> atoms     = Array.append;
  value length: atoms -> int            = Array.length;
  value slice : atoms -> int   -> int  -> atoms   = Array.sub;
  value cmp   : atom  -> atom  -> int       =
    fun l r -> compare (l:int)  (r:int);

  value string_of_atom:  atom  -> string      = string_of_int;
  value string_of_atoms: atoms -> string      =
    fun ats -> String.concat " "
          (List.map string_of_int (Array.to_list ats));
end;

(*============================*)
(*  "Terms": Instantiations:  *)
(*============================*)
module Terms_Compact = Terms Atoms_Compact;
module Terms_General = Terms Atoms_General;

