-- vim:ts=4:syntax=haskell
-- ========================================================================= --
--						Knuth-Bendix Algorithm: Top-Level					 --
--							    L.A.Merkin, 2003							 --
-- ========================================================================= --
module Main (main)
where

import KnuthBendix
import SetOP

import List
import Int
import System
import GetOpt

----------------
--	"Atom"s:  --
----------------
--	Externally, they are Chars 'a'-'z' and 'A'-'Z' (so 52 "Atom"s maximum).
--	Internally, they are just 8-bit integers:
--
type Atom = Int8

--	It's an instance of Enum, Bounded, Eq and NFData as required

-------------------
--	"fromChar":  --
-------------------
--	It uses the string of all admissible Atoms (for the problem under con-
--		sideration) in order to optimise the encoding:
--
fromChar :: Char -> String -> Atom

fromChar c  all_atoms =
	case elemIndex c all_atoms of
		Just n  -> fromIntegral n
		Nothing -> error ("Invalid Atom: '"++[c]++"'")


-----------------
--	"toChar":  --
-----------------
--	Again, conversion can only be done if we have an alphabet string of all
--		admissible Atoms:
--
toChar :: Atom -> String -> Char
toChar n  all_atoms = (all_atoms !! (fromInteger (toInteger n)))


--------------------------------
--	External Representation:  --
--------------------------------
--	ext_repr :: (all_atoms,  [rule])
--	rule	 :: (lhs, rhs)
--	all_atoms,   lhs, rhs :: String
--
type ExtRules = (String, [(String, String)])


------------------------
--	"get_ext_rules":  --
------------------------
get_ext_rules :: [String] -> IO ExtRules

get_ext_rules argv =
	case getOpt  RequireOrder  [] argv of
		([], [ext_rules_file], []) ->
			-- Read the "ext_rules_file" in:
			do
				ext_rules_str <- readFile ext_rules_file
				return (read ext_rules_str)

		_ ->
			error (usageInfo "PARAMETER: RulesFile" [])


-----------------------
--	"mk_int_rules":  --
-----------------------
--	Converts the Rules into internal representation:
--
mk_int_rules :: ExtRules -> Set (ReWrRule Atom)

mk_int_rules (all_atoms, rule_strs) =
	let
		mk_word ::  String -> KBWord Atom
		mk_word     str =     mkKBWord [fromChar c all_atoms | c <- str]

		mk_rule :: (String, String) -> ReWrRule Atom
		mk_rule    (lhs, rhs) =
			ReWrRule (mk_word lhs,  mk_word rhs)
	in
		foldl (\ curr ext_rule ->
				 addToSet (mk_rule ext_rule) curr
			  )
			  emptySet rule_strs
				

-------------------------
--	{word|rule}2str":  --
-------------------------
--	Convers a rule into external representation:
--
word2str :: (KBWord Atom) -> String -> String
word2str    kbw all_atoms =
	[toChar a all_atoms | a <- kbWordToList kbw]


rule2str ::  ReWrRule Atom -> String -> String

rule2str    (ReWrRule (l, r)) all_atoms =
	let
		ls = word2str l all_atoms
		rs = word2str r all_atoms
	in
		ls ++" => "++ rs


----------------------
--	"print_rules":  --
----------------------
print_rules :: Set (ReWrRule Atom) -> String -> IO ()

print_rules  rules  all_atoms =
	foldSet LR
	(\  io rule ->
		(io >> (putStrLn (rule2str rule all_atoms)), True)
	)
	(return ()) rules


--------------
--	"main": --
--------------
--	Command-line parm: RulesFile:
--
main :: IO ()
main =  do
	--	Get the command-line parm: RulesFileName:
	argv	  <- getArgs

	--	Get the Rules:
	ext_rules <- get_ext_rules argv

	--	Convert the rules into internal representation:
	int_rules <- return (mk_int_rules ext_rules)

	--	Run the completion algorithm!
	crules    <- return (complete int_rules)

	--	Output the result:
	print_rules  crules (fst ext_rules)

