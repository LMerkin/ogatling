-- vim:ts=4:syntax=haskell
-- ========================================================================= --
--								"KnuthBendix.hs":							 --
--	  A (parallel) implementation of the Knuth-Bendix rewriting algorithm    --
--				       L.A.Merkin, IMPARTIAL Project, 2003-2004				 --
-- ========================================================================= --
module KnuthBendix
(
	KBArr, KBWord, mkKBWord, kbWordToList, kbWordToArr,

	ReWrRule (..), CompletionMode (..), complete
)
where
import Strategies
import qualified List

-- ====================== --
--	"KBWord" Arithmetic:  --
-- ====================== --
import qualified SetOP
import qualified RuleSet

-- For arrays, it would be most efficient to use the "GHC.Arr" module,   but it
-- is still not available in GpH (which is not up to dates with the most recent
-- GHC versions), so use the standard "Array" module:
import Array
-- import Trace

--	"KBWord"s to be re-written are strings of "Atoms"; the latter can be of any
--		type with "Eq". "KBWords" are represented  as Arrays, for constant-time
--		access:
--	For efficiency of the completion algorithm,   we require the "Atom" type to
--		be "Enum"erable, and store the enumerator of each word:
--
type    KBArr  a = Array Int a
data	KBWord a = KBWord (KBArr a) Integer  -- Atoms, Enumerator

--
--	Provided that we have a valid enumeration, Eq and Ord tests are more easily
--		done on the enumerators than on the words themselves:
--
instance Eq  (KBWord a) where
	(KBWord _ enum1) == (KBWord _ enum2) = enum1 == enum2

instance Ord (KBWord a) where
	compare  (KBWord _ enum1) (KBWord _ enum2) = compare enum1 enum2

--	NB: Pairs of "KBWord"s are then natuarlly ordered as well -- but this is
--  in general NOT the ordering we need for "ReWrRule"s (see below).

----------------------------------------
--	Len-Lex Enumerator of "KBWord"s:  --
----------------------------------------
--	Corresponds to the TotalLength / Lexicographic ordering of strings:
--
lenLexEnum :: (Enum a, Bounded a, Eq a) => KBArr a -> Integer
lenLexEnum	(arr:: KBArr a) =
	let
		--  Polynomial enumerator of strings over "a":
		--	Array elements can be mapped into the range [0 .. a_max-a_min]  of
		--	the enumerator's values, so the exp base for the polynomial enume-
		--	rator is (a_max - a_min + 1):
		--
		a_min, a_max :: Int
		a_min = fromEnum (minBound :: a)
		a_max = fromEnum (maxBound :: a)

		a_base  :: Integer
		a_base  =  toInteger (a_max - a_min + 1)

		--	Array bounds:
		ind_lo,  ind_hi ::  Int
		(ind_lo, ind_hi) =  bounds arr

		--	Polynomial enumerator:
		eval_poly :: Int -> Integer -> Integer
		eval_poly ind acc =
			if   ind > ind_hi
			then acc
			else let acc' = (toInteger (fromEnum (arr ! ind) - a_min)) +
						    acc * a_base
				 in  eval_poly (ind+1) acc'
	in
		eval_poly ind_lo 0

----------------------------------
--	Recursive-Path Enumerator:  --
----------------------------------
--  Corresponds to the Recursive Path ordering of strings: TODO...

--  XXX:
--	Here we fix the ordering to be used -- is there any point in making it
--	flexible?
--
arrEnum :: (Enum a, Bounded a, Eq a) => KBArr a -> Integer
arrEnum =  lenLexEnum

--------------------------
--	"KBWord" Generator: --
--------------------------
--  Create a packed "KBWord" from a list of Atoms. The indices in the
--  "KBWord", unlike in the List, START FROM 1:
--
mkKBWord :: (Enum a, Bounded a, Eq a) => [a] -> KBWord a
mkKBWord as =
	let
		n   = length as
		arr = array (1, n) (zip [1..n] as)
		enum= arrEnum arr
	in
		KBWord arr enum

------------------------------
--	"KBWord" De-Generators: --
------------------------------
-- Convert "KBWord" into a List of a plain Array of Atoms:
--
kbWordToList :: KBWord a -> [a]
kbWordToList   (KBWord arr _) = elems arr

kbWordToArr  :: KBWord a -> KBArr a
kbWordToArr    (KBWord arr _) = arr

--------------------
--	"subKBWord":  --
--------------------
--	Returns an array slice, always starting with index 1:
--
subKBWord :: KBWord a -> Int -> Int -> KBArr a

subKBWord   (KBWord arr _) from to =
	let
		(1,len) = bounds arr
	in
		if  from < 1 || to > len
		then
			error ("\"KnuthBendix.subKBWord\": Invalid range: "++ (show from)
				   ++".."++ (show to))
		else
			let
				slen = max 0 (to-from+1)
			in
				array (1,slen) [(i-from+1, arr ! i) | i <- [from .. to]]

-------------------
--	"kbwLength": --
-------------------
kbwLength :: KBWord a -> Int
kbwLength   (KBWord arr _) =
	let
		(1,len) = bounds arr
	in
		len

-----------------------------------
--	Concatenation of "KBWord"s:  --
-----------------------------------
--	In fact, what we need is pre- and ap-pending of "KBArr"s to a "KBWord". If
--		a prefix or postfix is absent, use "empty" array:
--
attachWords :: (Enum a, Bounded a, Eq a) =>
			   KBArr a -> KBWord a -> KBArr a -> KBWord a

attachWords    before (KBWord arr _) after =
	let
		(1, len1)  = bounds before
		(1, len2)  = bounds arr
		(1, len3)  = bounds after
		len12	   = len1 + len2
		len123	   = len12+ len3

		get_elem i
			| i <= len1  = before !  i
			| i <= len12 = arr    ! (i - len1)
			| otherwise  = after  ! (i - len12)

		-- The concatenated array:
		arr123	   = array (1, len123) [(i, get_elem i) | i <- [1..len123]]

		-- The enumerator is to be re-computed:
		enum123	   = arrEnum  arr123
	in
		KBWord arr123 enum123

------------------------
--	"replaceMiddle":  --
------------------------
--	Reduction implementation. Pre-codition: term [from..to] == lhs,
--	is replaced by the rhs, where (lhs,rhs) form a re-writing rule:
--
--  NB: this is an OPTIMISED implementation. Alternatively, "subKBWord"
--	and "attachWords" could be used:
--
replaceMiddle ::
	(Enum a, Bounded a, Eq a) =>
	KBArr a -> Int -> KBArr a -> KBArr a -> KBWord a

replaceMiddle term from lhs rhs =
	let
		(1, tlen) = bounds term
		(1, llen) = bounds lhs
		(1, rlen) = bounds rhs
		to		  = from + llen - 1		-- Before substitution
		nto		  = from + rlen - 1		-- After  substutution
		nlen	  = tlen - llen + rlen  -- New full length
	in
		-- Check the boundaries, but do NOT really check -- for efficiency --
		-- that term [from..to] == lhs:
		--
		if  from <= 0 || to > tlen
		then
			error "KnuthBendix.replaceMiddle: Invalid boundaries"
		else
			let
				-- The substitution function:
				get_elem i
					| i <  from = term !  i
					| i <= nto  = rhs  ! (i - from + 1 )
					| otherwise = term ! (i - nto  + to)

				-- The new array:
				narr  = array (1, nlen) [(i, get_elem i) | i <- [1..nlen]]

				-- The new enum:
				nenum = arrEnum narr
			in
				KBWord  narr nenum

----------------
--	"empty":  --
----------------
--	Empty array (not a full "KBWord" -- no enumerator):
--
empty :: Array Int a
empty =  array (1,0) []


-- =================== --
--	Re-Writing Rules:  --
-- =================== --
--
--	Re-writing rules (from which the Critical Pairs are formed):
--		Left => Right:
--
data ReWrRule a = ReWrRule (KBWord a) (KBWord a) deriving Eq

--
--	Ordering of rules MAY  be required  for some types  of rule storage
--  (though it is NOT used by the NFA provided by the "RuleSet" module):
--
instance Ord (ReWrRule a) where
	compare  (ReWrRule l1 r1) (ReWrRule l2 r2) =
		-- Compare wrt the first (largest if the rule is ordered) term first:
		case  compare   l1 l2 of
			LT -> LT
			GT -> GT
			EQ -> compare  r1 r2

--
--	Pairs of Re-writing rules:
--	These are "critical pairs"  to be completed by the Knuth-Bendix process.
--  Their ordering may be needed for maintaining a priority queue of pending
--  critical pairs:
--
data ReWrPair a = ReWrPair (ReWrRule a) (ReWrRule a) deriving Eq

instance Ord (ReWrPair a) where
	compare  (ReWrPair p1 q1) (ReWrPair p2 q2) =
		-- Compare wrt the first rule in the pairs first:
		case compare p1 p2  of
			LT -> LT
			GT -> GT
			EQ -> compare q1 q2


-- ================ --
--	Sets of Rules:  --
-- ================ --
--	The Rules are grouped according to their LHSs, which are organised into
--	an index (acting as an NFA).  The functions of this section simply lift
--	the corresp functions of the "RuleSet" module.
--
--  In the "RSet" structure, the rules' LHSs  are spread through the index,
--	whereas RHSs are kept as single pieces. Thus, the following representa-
--  tion is used for stored Rules:
--
type StoredRule a = (Integer, KBWord a) -- LHS's Enum, full RHS

type RuleSet    a = RuleSet.RSet a (StoredRule a)

---------------------
--	"insertRule":  --
--	"deleteRule":  --
---------------------
--	NB: For efficiency, these functions do NOT use the "ReWrRule" arg type.
--	As "Enum a" does not automatically imply "Ord a", the latter constraint
--	will have to be used explicitly:
--
insertRule :: Ord a =>	KBWord a -> KBWord a -> RuleSet a -> RuleSet a

insertRule (KBWord arr enum) rhs  set =
	RuleSet.insertRule arr (enum, rhs) set


deleteRule :: Ord a => KBWord a -> KBWord a -> RuleSet a -> RuleSet a

deleteRule (KBWord arr enum) rhs  set =
	RuleSet.deleteRule arr (enum, rhs) set

----------------------
--	"foldRuleSet":  --
----------------------
--	Again, for convenience the call-back function here receives separate
--	LHS and RHS:
--
foldRuleSet:: Ord a =>
			 (KBWord a -> KBWord a -> b -> (b,Bool)) -> b -> RuleSet a -> b
--			  LHS		  RHS         Acc  Acc', ContFlag

foldRuleSet f b0 set =
	RuleSet.foldRSet
	(\ arr (enum, rhs) b ->
		let lhs = KBWord arr enum
		in  f lhs rhs  b
	)
	b0  set

-- ============================ --
--	Critical Pairs Completion:  --
-- ============================ --
--	The Critical Pairs Completion Algorithm. Takes a set of "KBWord"s, and
--		completes (and possibly reduces) it in such a way  that re-writing
--		reductions wrt to the latter set  are guaranteed  to be converging
--		(i.e., terminating and confluent).  HOWEVER, the "complete" itself
--		may not terminate!
--
-------------------------------------
--	Completion Mode Configuration: --
-------------------------------------
data CompletionMode =
	-- Sequential Mode: there is a queue of Critical Pairs to be processed,
	-- and they are processed in the order they appear in the queue:
	Seq_Mode {use_inter_reductions :: Bool}

	-- Explicit Parallel mode: controls scheduling of critical pairs to be
	-- prcessed:
  | EPar_Mode {chunk_size		   :: Int,
			   par_chunks		   :: Int,
			   use_striping		   :: Bool,
			   use_inter_reductions:: Bool}

	-- Implicit Parallel Mode: no control over the order of processing of
	-- the critical pairs.  All currently available pairs  are "exploded"
	-- at once:
  | IPar_Mode {use_inter_reductions:: Bool}

--------------------
--	"initRules":  --
--------------------
--	Pre-processor: prepares the initial set of rules so that LHS > RHS, removes
--	any tautologies, and does initial mutual reductions:
--
initRules ::
	(Ord a, Bounded a, Enum a) => RuleSet a -> ComplMode -> RuleSet a

initRules rules0 mode =
	let rules1 =
		foldRuleSet
		  (\ lhs rhs curr_rs ->
			 case compare lhs rhs of
				LT ->
					-- lhs < rhs: Reverse the order:
					(insertRule rhs lhs curr_rs, True)

				GT ->
					-- l > r: Already in the right order:
					(insertRule lhs rhs curr_rs, True)

				EQ ->
					-- A tautology: skip it:
					(curr_rs, True)
		  )
		  RuleSet.emptyRSet rules0
	in
		--	Reduce the rules wrt each other before running the completion
		--	algorithm -- possible in parallel:
		reduceWrtEachOther rules1 mode

-------------------
--	"complete":  --
-------------------
--  This is a TOP-LEVEL DRIVER which arranges the set of rules to be suitable
--		for completion, and runs the actual completion algorithm (either seq-
--		uential or parallel):
--
complete :: (Ord a, Bounded a, Enum a, NFData a) =>
			CompletionMode  -> RuleSet a -> RuleSet a

complete    mode rules0 =
	let 
		--  "rule0" is the set of rules given by the user. Pre-process them:
		rules1 = initRules   rules0  mode

		--  Construct the initial LIST of all pairs (to be then convered into
		--  a set, but also directly used in "_IPar" mode):
		--
		pairsL = mkPairsList rules1

		--	Construct a SET of all pairs of rules (for Seq_ and EPar_ Modes):
		pairs1 =  storePairsList SetOP.emptySet pairsL

		-- Run the completion process proper:
		-- NB: in the "IPar_Mode", the pairs are organised into a list, not
		--	   an ordered set:
		--
		rules2  = case mode of
			Seq_Mode  _		  ->
				completePairs_Seq  rules1 pairs1 mode

			EPar_Mode _ _ _ _ ->
				completePairs_EPar rules1 pairs1 mode

			IPar_Mode _		  ->
				completePairs_IPar rules1 pairsL mode

		-- At the end, reduce the results wrt each other once again,  except
		-- in cases when full intermediate reductions have already been done:
		--
		rules3  =
			if   use_inter_reductions mode
			then rules2
			else reduceWrtEachOther rules2 mode
	in
		--	The result:
		rules3

-- ======================================== --
--  Completion: Sequential Implementation:  --
-- ======================================== --
----------------------------
--  "completePairs_Seq":  --
----------------------------
completePairs_Seq::
	(Ord a, Bounded a, Enum a) =>
	RuleSet a -> SetOP.Set (ReWrPair a) -> CompletionMode -> RuleSet a

completePairs_Seq  rules0 pairs0 mode =
	if SetOP.isEmptySet   pairs0
	then
		-- All done, the accumulated ordered rules is what we need:
		rules0
	else
		-- There are still pairs to complete:
		let
			-- Take the smallest pair:
			(min_pair, pairs1) = SetOP.takeMinElt pairs0

			-- Compute the critical pairs (new rules) of these rules:
			--
			new_rules = completeReduceOnePair rules0 min_pair

			-- "new_rules" are those constructed as a result of completing the
			--	  "min_pair"  (actually, two different critical pairs are made
			--	  out of each one stored in the "pairs" queue).  The new rules
			--	  are appended to the existing "rules", and the pairs are made
			--	  between themselves and the old "rules":
			--
			pairs2 = updatePairs pairs1 rules0 new_rules

			--	rules1 = rules0 U new_rules, and with possible inter-reductns:
			rules1 = updateRules rules0 new_rules mode
		in
			-- Iterate until there are no new pairs to complete:
			completePairs_Seq rules1 pairs2  mode


-- =============================================== --
--	Completion: Explicit Parallel Implementation:  --
-- =============================================== --
--	This implementation uses explicit scheduling policies for critical pairs
--	to be completed:
-----------------------------
--	"completePairs_EPar":  --
-----------------------------
completePairs_EPar::
				 (Ord a, Bounded a, Enum a, NFData a) =>
				 RuleSet a -> SetOP.Set  (ReWrPair a) -> CompletionMode ->
				 RuleSet a

completePairs_EPar rules0 pairs0 mode

	| chunk_size mode <= 0 || par_chunks mode <= 0 =
	  error "\"KnuthBendix.completePairs_EPar\": Invalid chunk parameters"

	| SetOP.isEmptySet pairs0 =
	  -- All done:
	  rules0

	| otherwise =
	  -- The real work:
	  let
		-- Take CHUNKS of smallest pairs. Using striping can improve "unifor-
		-- mity" of chunks, otherwise earlier chunks would have a tendency to
		-- produce "smaller, easier" results than the later ones:
		--
		(pair_chunks, pairs1) =
			if  use_striping mode
			then
				takePairStripes (par_chunks mode) (chunk_size mode) pairs0
			else
				takePairChunks  (par_chunks mode) (chunk_size mode) pairs0

		-- The chunks are processed in parallel, each yielding a list of new
		-- rules. The results are fed back into "rules" and "pairs":
		--
		new_rules = concat
					(parMap chunkStrat (processChunk rules0) pair_chunks)

		-- The rest is the same as in the sequential implementation:
		--
		pairs2 = updatePairs pairs1 rules0 new_rules

		-- New set of rules, modulo which "pairs2" will then be reduced:
		rules2 = updateRules rules0 new_rules mode
	  in
		-- Iterate until there are no new pairs to complete:
		completePairs_EPar rules2 pairs2 mode

-----------------------
--	"processChunk":  --
-----------------------
--	Each chunk of critical pairs is processed by a single thread using the
--	"chunkStrat" defined below,    so "seqList" is needed to achieve full
--	evaluation of the resulting list and its components:
--
processChunk :: (Ord a, Bounded a, Enum a, NFData a) =>
				 RuleSet a -> [ReWrPair a] -> [ReWrRule a]
processChunk old_rules pairs =
	-- Each of the "pairs" is completed and reduced wrt "old_rules" (which
	--   can yield multiple results,  as in "completeReduceOnePair").  The
	--   results are merged together:
	--
	(concat (map (\ pair -> completeReduceOnePair old_rules pair) pairs))
	`using` chunkStrat

-------------------------
--	"takePairChunks":  --
-------------------------
--	Takes "p" chunks of size "s" of smallest elements from "pairs". Each chunk
--	is a list of pairs.
--	Result fst:  outer dim: chunks; inner dim: pairs in each chunk
--	Result snd:  remaining pairs in the queue
--
takePairChunks :: Int -> Int  ->   SetOP.Set (ReWrPair a) ->
				  ([[ReWrPair a]], SetOP.Set (ReWrPair a))

takePairChunks p s pairs = takePairChunks' p s pairs []


takePairChunks':: Int -> Int  ->   SetOP.Set (ReWrPair a) -> [[ReWrPair a]] ->
				  ([[ReWrPair a]], SetOP.Set (ReWrPair a))

takePairChunks' p s pairs curr =
	if p <= 0 || SetOP.isEmptySet pairs
	then
		-- All chunks already taken:
		(curr, pairs)
	else
		let (chunk, pairs') = takePairChunk s pairs   []
		in  takePairChunks' (p-1) s pairs' (curr++[chunk])

--
--	"takePairChunk":
--	Takes one chunk of size "s" from "pairs":
--
takePairChunk  :: Int -> SetOP.Set (ReWrPair a) -> [ReWrPair a] ->
					([ReWrPair a], SetOP.Set (ReWrPair a))

takePairChunk s pairs curr =
	if  s <= 0 || SetOP.isEmptySet pairs
	then
		--  This chunk is done:
		(curr, pairs)
	else
		let (next_pair, pairs') = SetOP.takeMinElt pairs
		in  takePairChunk (s-1) pairs' (curr++[next_pair])

----------------------------
--	"takePairStripes":  --
----------------------------
--	Similar to "takePairChunks", but in the striping mode:
--
takePairStripes ::  Int -> Int ->     SetOP.Set (ReWrPair a) ->
					 ([[ReWrPair a]], SetOP.Set (ReWrPair a))

takePairStripes  p s pairs =
	-- Create an array of "p" lists  (initially empty, then growing to length
	-- up to "s"), and fill each of them in by traversing the leading "pairs":
	let
		stripes = array (1,p) [(i, []) | i <- [1..p]]
	in
		takePairStripes' (p*s) pairs stripes


takePairStripes':: Int -> SetOP.Set  (ReWrPair a) ->
					 Array  Int  [ReWrPair a] ->
					 ([[ReWrPair a]], SetOP.Set (ReWrPair a))

takePairStripes' rem_ps pairs stripes =
	--
	-- "rem_ps" pairs are to be taken sequentially from the "pairs" set, and
	-- distributed across the "stripes":
	--
	if  rem_ps == 0 || SetOP.isEmptySet pairs
	then
		-- No more pairs to be taken:
		(elems stripes, pairs)
	else
		-- Take the next pair. In each stripe, pairs come in the same relative
		-- order as they appear in the "pairs" queue;  the ordering of stripes
		-- themselves does not matter since they will be processed in parallel:
		let
			(next_pair, pairs') = SetOP.takeMinElt pairs

			-- "arr_ind" is the stripe index where this pair is going into:
			(1, p)		= bounds     stripes
			arr_ind 	= (rem_ps `mod` p) + 1

			-- Append the "next_pair" to the destination stripe:
			to_stripe	= stripes   !  arr_ind
			to_stripe'	= to_stripe ++ [next_pair]
			stripes'	= stripes   // [(arr_ind, to_stripe')]
		in
			takePairStripes' (rem_ps-1) pairs' stripes'


-- ============= --
--	Strategies:  --
-- ============= --
--	"chunkStrat":
--	The strategy to process a chunk of critical pairs, to build a list of new
--	rules. We currently require that the chunk is completely processed by its
--	thread, so the result is fully evaluated:
--
chunkStrat :: NFData a => Strategy [ReWrRule a]
chunkStrat =  seqList  ruleStrat

--	"ruleStrat":
--	The strategy to evaluate a single new rule when it is built from a critical
--	pair: also sequential:
--
ruleStrat  :: NFData a => Strategy (ReWrRule a)
ruleStrat =
	\ (ReWrRule l r) -> (wordStrat l) `seq` (wordStrat r) `seq` ()

--	"wordStart":
--	Sequential strategy for evaluating "KBWord"s to RNF:
--
wordStrat  :: NFData a => Strategy (KBWord a)
wordStrat =
	\ (KBWord arr enum) -> ((seqArr atomStrat) arr) `seq` enum `seq` ()

--	"atomStrat":
--	Atoms are polymorphic, but they must admin an RNF:
--
atomStrat  :: NFData a => Strategy a
atomStrat = rnf

-- =============================================== --
--	Completion: Implicit Parallel Implementation:  --
-- =============================================== --
--	We define the complete set of Rules as the initial set of rules ("rules1")
--		plus the closure under the criticl pairs completion operation:
--
completePairs_IPar ::
	(Enum a, Bounded a, Eq a, NFData a) =>
	RuleSet a ->  [ReWrPair a] -> CompletionMode ->  RuleSet a

completePairs_IPar rules0 pairs0  mode  = rules0
	if SetOP.isEmptySet   pairs0
	then
		-- All done, the accumulated ordered rules is what we need:
		rules0
	else
		-- There are still pairs to complete:
		let
			-- "Explode" the whole list of pending critical pairs at once. This
			-- can also be done in the "_EPar" algorithm by selecting   "chunk_
			-- size" to be 1 and "par_chunks" to be equal to the length of the
			-- queue, but in that case we don't really need a priority queue --
			-- hence a list here.
			-- Each new rule is completely evaluated, including full reduction
			-- wrt "rules0":
			new_rules = (map completeReduceOnePair rules0) `using`
						(parList ruleStrat)

			-- All current pairs have been consumed, so a new set is to be
			-- constructed:
			pairs1    = updatePairs RulesSet.emptyRSet rules0 new_rules

			-- New rules for future reductions wrt them:
			rules1	  = updateRules rules0   new_rules mode
		in
			-- Iterate until there are no new pairs to complete:
			completePairs_IPar rules1 pairs1 mode


-- ====================================================== --
--	Common functions used by "complete_{Seq,EPar,IPar}":  --
-- ====================================================== --
---------------------
--	"mkPairsList":  --
---------------------
--  Constructs a list of all pairs of given rules, including reflexive ones,
--		e.g. (r,r).
--  NB: reflexive pairs are not the same as reflexive rules! --  the latter
--		are obviously NOT included in the result if constructed during  the
--		the completion process. Reflexive pairs are needed because their LHSs
--		can still participate in critical pairs with other rules:
--
mkPairsList ::
	(Enum a, Ord a) => RuleSet a -> [ReWrPair a]

mkPairsList rules =
	 foldRuleSet
	 (\ l1 r1 curr_pairs1 ->
		let
			rule1 = ReWrRule l1 r1
			new_list1 =
				foldRuleSet
		 		(\  l2 r2 curr_pairs2 ->
					let rule2 = ReWrRule l2 r2
					in  ((ReWrPair rule1 rule2) : curr_pairs2, True)
					-- NB: There is no need to order rules inside a pair,
					-- since both symmetric pairs wil be included anyway!
				)
				curr_pairs1 rules
		in (new_list1, True)
	 )
	 [] rules

-------------------------
--	"storePairsList":  --
-------------------------
--	Takes a LIST of critical pairs (possibly constructed by "MkPairsList"
--		above), and inserts each of them into a given set (possibly empty)
--		of "old_pairs":
--
storePairsList ::
	(Enum a, Ord a) =>
	SetOP.Set (ReWrPair a) -> [ReWrPair a] -> SetOP.Set (ReWrPair a)

storePairsList old_pairs new_pairs =
	foldl
	(\ curr_set pair ->
		SetOP.addToSet
			-- No recombination is needed here:
			SetOP.trc
			pair curr_set
	)
	old_pairs
	new_pairs

----------------------
--	"updatePairs":  --
----------------------
--	This function inserts new pairs into the set of pairs to be completed, as
--		a result of new rules having been produced:
--
updatePairs ::  (Enum a, Ord a) =>
				SetOP.Set (ReWrPair a) -> RuleSet a -> [ReWrRule a] ->
				SetOP.Set (ReWrPair a)

updatePairs old_pairs old_rules new_rules =
	let
		--	Construct symmetric pairs between the "new_" and "old_rules",
		--  inserting the results into "old_pairs":
		pairs1 =
			--	For each "nrule" in "new_rules":
			foldl
			(\  cpairs1 nrule ->
				--	For each "orule = (olhs,orhs)" in "old_rules":
				foldRuleSet
				(\  olhs orhs cpairs2 ->
					let
						orule = ReWrRule olhs orhs
						--	pairs' =
						--	pairs U {(orule, nrule)} U {(nrule, orule)}:
						--
						cpairs3 = SetOP.addToSet
								  SetOP.trc (ReWrPair nrule orule) cpairs2

						cpairs4 = SetOP.addToSet
								  SetOP.trc (ReWrPair orule nrule) cpairs3
					in
						(cpairs4, True)
				)
				cpairs1 old_rules
			)
			old_pairs new_rules

		--	Construct SYMMETRIC pairs between the "new_rules" themselves,
		--  inserting the results into "pairs1":
		--
		pairs2 = storePairsSet pairs1 (mkPairsList new_rules)
	in
		pairs2

----------------------
--	"updateRules":  --
----------------------
--	Adds elements of the "new_rules" list to the "old_rules" set:
--
updateRules ::
	Ord a => RuleSet a -> [ReWrRule a] -> CompleMode -> RuleSet a

updateRules old_rules new_rules =
	if  use_inter_reductions mode
	then
		-- Need inter-reductions (possibly parallel, depending on the "mode").
		-- Have to do them efficiently, talking into account that "new_rules"
		-- are already reduced wrt "old_rules", but not other way round:
		let
			-- Convert "new_rules" into a set, so reductions can be made wrt
			-- to it:
			new_rset = foldl
				(\ cset (ReWrRule lhs rhs) -> insertRule lhs rhs cset)
				RuleSet.emptyRSet
				new_rules

			-- Reduce "old_rules" wrt "new_rset" -- possibly in parallel:
		
	else
		-- No inter-reductions -- just insert "new_rules" into the
		-- "old_rules" set:
		--
		foldl
			(\  crules (ReWrRule lhs rhs)  -> insertRule lhs rhs crules)
			old_rules
			new_rules

--------------------------------
--	"completeReduceOnePair":  --
--------------------------------
--  NB: there is no need to sort new rules in the result before inserting them
--		into the general rules set, so a list (rather than an Ordered Set)  is
--		OK as an output here. Possible tautologies are removed by "reduceEach",
--		identical new rules are removed by "nub":
--
completeReduceOnePair :: 
	(Ord a, Bounded a, Enum a) => RuleSet a -> ReWrPair a -> [ReWrRule a]

completeReduceOnePair  old_rules pair =
	List.nub (reduceEach ((critPairs1 pair) ++ (critPairs2 pair)) old_rules)

---------------------
--	"critPairs1":  --
---------------------
--	Given two rules, (l1,r1) and (l2,r2), it computes a critical pair for
--		them by checking if there is a non-empty common "t" such that
--		l1 = t y
--		l2 = x t
--	If so, we have x l1 = l2 y, but l1 <-> r1, l2 <-> r2,
--	then x r1 and  r2 y are two derivations from the same term,  and thus
--		they are equivalent themselves:
--		x r1 <-> r2 y,
--	so a new rule like
--		x r1  -> r2 y
--	can be introduced (it will then be sorted and reduced module existing
--		rules).
--	NB: the "t" above may not be unique;   we make critical pairs for all
--		suitable "t"s:
--
critPairs1:: (Ord a, Bounded a, Enum a) => ReWrPair a -> [ReWrRule a]

critPairs1   (ReWrPair (ReWrRule l1 r1)   (ReWrRule l2 r2)) =
	let
		--	The list of (x,y) factors, as above:
		xys   = factors1 l1 l2

		--	The new rules:
	in
		[ReWrRule (attachWords x r1 empty) (attachWords empty r2 y)
		|(x,y) <- xys]

-------------------
--	"factors1":  --
-------------------
--	For "l1", "l2" as above, computes lists of (x, y) factors.
--  NB: There is no "factors2"!
--
factors1 :: Eq a => KBWord a -> KBWord a -> [(KBArr a, KBArr a)]

factors1 l1 l2 =
	let
		len1   = kbwLength l1
		len2   = kbwLength l2

		factors1'    tlen   curr =
			if  (tlen  > len1)  || (tlen > len2)
			then
				--	Input exhausted:
				curr
			else
				--  Do the sub-words
				--  	l1 [1..tlen],		and
				--		l2 [len2-tlen+1 .. len2]
				--  match?
				let
					t1 = subKBWord l1 1 tlen
					t2 = subKBWord l2 (len2-tlen+1) len2
				in
					if  t1 == t2	-- Array Equality!
					then
						-- Match found, so get the factors, and then look
						--		for more:
						let
							y = subKBWord l1 (tlen+1) len1
							x = subKBWord l2 1		 (len2-tlen)
						in
							factors1' (tlen+1) ((x,y) : curr)
					else
						-- No match here, try longer sub-words:
						factors1' (tlen+1) curr
	in
		factors1' 1 []

---------------------
--	"critPairs2":  --
---------------------
--	Given two rules, (l1,r1) and (l2,r2), it computes a critical pair for
--		them by checking if there exist "x" and "y" such that
--		l1 = x l2 y
--	If so, as l1 <-> r1, l2 <-> r2,
--	then r1 and x r2 y are two derivations from the same term,  and thus
--		they are equivalent themselves:
--		r1 <-> x r2 y
--	so a new rule like
--		r1  -> x r2 y
--	can be introduced (it will then be sorted and reduced modulo existing
--		rules).
--	NB: the "x" and "y" above may not be unique;   we make critical pairs
--		for all suitable "x" and "y":
--
critPairs2:: (Ord a, Bounded a, Enum a) => ReWrPair a -> [ReWrRule a]

critPairs2   (ReWrPair (ReWrRule l1 r1)   (ReWrRule l2 r2)) =
	let
		xys  = findSubWords1 l1 l2
	in
		map (\ (x, y) -> ReWrRule r1 (attachWords x r2 y)) xys


-- ============= --
--	Reductions:  --
-- ============= --
--	Reduction of a "KBWord" modulo a set of "ReWrRule"s. Reductions are per-
--		formed until no more reductions are possible.  The order (and indeed
--		the result, unless the rules set is already complete) of such reduc-
--		tions  is non-determnistic in the parallel environment:
--
reduceWrtSet :: (Ord a, Bounded a, Enum a) =>
				KBWord a -> RuleSet a -> KBWord a

reduceWrtSet kbw  rules  =
	-- Find a rule in the "kbset" which can be used for reduction; that is,
	--	the LHS of that rule must be a sub-word of "kbw".
	--
	case findDoReductions kbw rules of
		[] ->
			-- No (more) reducers found in "rules" --  all reductions done:
			kbw

		kbw' : _ ->
			-- There were some reducers, and the corresp reductions have been
			--		done. We do not try to find the "best" reduction results,
			--		but simply take the FIRST one. With lazy evaluation, all
			--		other reductions will not even be computed.
			-- XXX: Or, should we do all the reductions and  find the minimal
			--		result, for example?
			--
			reduceWrtSet kbw' rules

----------------------------
--	"findDoReductions1":  --
----------------------------
--	Returns the list of all possible reduction results of "kbw" wrt "rules".
--	Order is important here: if a "rule" reduces the "kbw", then LHS(rule)
--	is a sub-word of "kbw", and thus LHS(rule) <= kbw in our term ordering.
--
--  XXX: This may be inefficient, as if a current LHS is not at all a sub-
--	word of the "kbw" arg, the former would still need to be traversed:
--
--	NB: This function  produces a LIST of results,  but this list is lazily
--	pattern-matched by the caller ("reduceWrtSet"), so only 1 reduction is
--	actually applied:
--
findDoReductions1 :: (Ord a, Bounded a, Enum a) =>
					  KBWord a -> RuleSet a -> [KBWord a]

findDoReductions1 kbw rules =
	foldRuleSet
	(\ lhs rhs curr ->
		let
			redress  =
				if  kbw > lhs
				then
					-- Certainly no reductions here. NB: This is the ONLY
					-- place where ordering of Rules is used:
					[]
				else
					-- Try to find reducers:
					let
						reducers = findSubWords1 kbw lhs
					in
						-- Do the reductions!
						map (\ (before, after) ->
							attachWords before rhs after)  reducers
		in
			(curr ++ redress, True)
	)
	[] rules

------------------------
--	"findSubWords1":  --
------------------------
--	"e": reduceE; "r": potential reduceR (if occurs as a sub-word in "e").
--	Returns a list of tuples (before, after),  corresponding to possible
--		reductions (e == before r after):
--
findSubWords1 :: Eq a =>  KBWord a -> KBWord a -> [(KBArr a, KBArr a)]

findSubWords1 e@(KBWord _ eenum) r@(KBWord rarr renum) =
	let
		elen = kbwLength e
		rlen = kbwLength r

		findSubWords' from subs =
			if	from + rlen - 1 > elen
			then
				-- Went too far: return what we got already:
				subs
			else
				-- Check if the slice of "e" starting at "from" is the same as
				--		the reducer:
				--
				if  (subKBWord e from (from+rlen-1)) == rarr
				then
					-- Yes, it is found. Get the prefix and the postfix, and
					-- SEARCH FOR MORE:
					let
						before = subKBWord e 1           (from-1)
						after  = subKBWord e (from+rlen) elen

						subs'  = (before, after) : subs
					in
						findSubWords'  (from+1)  subs'
				else
					-- Sub-word not equal to "rarr" -- try the next position:
					findSubWords'		 (from+1)  subs
	in
		--	First of all, check the enumerators: "r" can only be a reducer for
		--		"e" if eenum <= renum:
		if  renum > eenum
		then
			[]
		else
			--	Go through "e", trying to find sub-words equal to "r".   ALL
			--		occurrencies of "r" in "e" are used for reductions, even
			--		overlapping ones -- we get multiple results:
			--
			findSubWords' 1 []

----------------------------
--	"findDoReductions2":  --
----------------------------
--	A more efficient implementation, which uses directed indexed search for
--  reducers in a "RuleSet". Still, the search term  (the reducible part of
--	the LHS) might not start at the beginning of the given "kbw";   we need
--  to try all possible positions.   With the curr implementation, the VERY
--  FIRST reducer found is applied; alternative stratrgies (TODO ?) may use
--  the shortest or the longest reducer, the minimum reduction result, etc:
--
findDoReductions2 :: (Ord a, Bounded a, Enum a) =>
					  KBWord a -> RuleSet a -> [KBWord a]

findDoReductions2    (KBWord term _) rules =
	let
		(1,ubound) = bounds  term

		runNFA pos =
			-- Check whether there is an indexed rule  with the LHS being a
			-- sub-word of "term", beginning at "pos". Stop after the first
			-- sub-word found:
			if pos > ubound
			then
				[]
			else
				case RuleSet.findRule term pos rules of
					Nothing ->
						-- Try next "pos":
						runNFA (pos+1)

					Just (larr, (_, KBWord rarr _)) ->
						-- Found something, do reduction:
						[replaceMiddle term pos larr rarr]
	in
		runNFA 1

--
--	So: which implementation do we use?
--
findDoReductions :: (Ord a, Bounded a, Enum a) =>
					 KBWord a -> RuleSet a -> [KBWord a]

findDoReductions =  findDoReductions2

-- ======================== --
--	High-Level Reductions:  --
-- ======================== --
-----------------------------
--	"reduceWrtEachOther":  --
-----------------------------
--	Performs all mutual reductions for a set of rules, until no more reductions
--		are possible:
--
reduceWrtEachOther ::
	(Ord a, Bounded a, Enum a) => RuleSet a -> RuleSet a

reduceWrtEachOther rules0 =
	--	Do reductions of all elements wrt the rest. Repeat the process until we
	--		complete a cycle without any reductions:
	let
		(rules1, red_flag) = reductionCycle rules0
	in
		if  red_flag
		then
			--  There were reductions, so do it again:
			reduceWrtEachOther rules1
		else
			--  No more reductions -- all done (rules1 == rules0):
			rules0

-----------------------------
--	"reductionCycle_Seq":  --
-----------------------------
--	The "inner loop" of "reduceWrtEachOther".
--	For each element of the initial "rules", reduce it wrt the rest;   "constr_
--		rules" is the set of reduced rules under construction during the cycle.
--		At the end, it will be returned to replace the original "rules" set:
--
reductionCycle :: (Ord a, Bounded a, Enum a) =>
				   RuleSet a ->  (RuleSet a, Bool)

reductionCycle rules =
	foldRuleSet
	( \ lhs rhs (constr_rules, flag) ->
		-- "flag" indicates  whether there was  at least  one reduction
		-- during this cycle;     it is reported to the caller (such as
		-- "reduceWrtEachOther")  to decide the termination condition:
		let
			this_rule   = ReWrRule   lhs rhs
			other_rules = deleteRule lhs rhs rules

			red_rule    = reduceEach [this_rule] other_rules
			res			=
				-- New state:
				case red_rule of
					[]   ->
						-- The rule has disappeared, as it was reduced to
						--		a tautology. Thus, there WAS a reduction:
						(constr_rules, True)

					[rr] ->
						if  rr == this_rule
						then
							-- There was actually no reduction.  Keep the
							--	original "flag" (DON'T re-set it to False!)
							--
							(insertRule lhs rhs constr_rules, flag)
						else
							-- There was a reduction:
							(insertRule lhs rhs constr_rules, True)

					_ ->
						error "\"KnuthBendix.reductionCycle\": Impossible"
		in (res, True)
	)
	(RuleSet.emptyRSet, False) rules

---------------------
--	"reduceEach":  --
---------------------
--	Post-processing of "new_rules" (typically generated by completing ONE cri-
--		tical pair). Each of these rules is reduced wrt "old_rules",  but NOT
--		wrt each other.  The reduced rules are ordered so that LHS > RHS, and
--		tautologies are removed:
--
reduceEach   :: (Ord a, Bounded a, Enum a) =>
				[ReWrRule a] -> RuleSet a -> [ReWrRule a]

reduceEach      new_rules old_rules =
	foldl
	(\ curr (ReWrRule l r) ->
		let
			l' = reduceWrtSet l old_rules
			r' = reduceWrtSet r old_rules
		in
			case compare l' r' of
				GT ->
					-- l' > r': put them in this order:
					(ReWrRule l' r') : curr

				LT ->
					-- l' < r': reverse the order:
					(ReWrRule r' l') : curr

				EQ ->
					-- l'== r': tautology: skip it:
					curr
	)
	[] new_rules

