(*pp camlp4r *)
(* vim:ts=4:syntax=ocaml
*)
(*===========================================================================*)
(*                            "crit_pairs.ml":                               *)
(*            Operations on Critical Pairs  of Reduction Rules               *)
(*                L.A.Merkin, IMPARTIAL Project, 2003--2005                  *)
(*===========================================================================*)
(* See the Intreface for more detail: *)
module type PAIRS =
sig
    type  term_enumf   = 'f;
    type  rewrite_rule = 'r;
    type  rule_pair    = 'p;
    type  pairs_q      = 'q;

    value empty_queue  : pairs_q;
    value insert_pair  : pairs_q    ->  rule_pair          -> pairs_q;
    value take_hd_pair : pairs_q    -> ((option rule_pair) *  pairs_q);

    value mk_crit_pairs: term_enumf -> rule_pair -> list rewrite_rule;
end;

module CP (RT: Rules.TERMS) =
struct
    (*==================================*)
    (*  Priority Queues of Rule Pairs:  *)
    (*==================================*)
    (*  Currently, implemented using Red-Black Trees as well: *)
    module POrd =
    struct
        type   t = RT.rule_pair;

        (* Normal Strategy: First of all, comparison or Rules:  wrt the LHS,
           then wrt the RHS. Tautologies (rules with LHS=RHS) are forbidden,
           so we will check for that:
        *)
        value rules_cmp:  RT.rewrite_rule -> RT.rewrite_rule -> int =
        fun rr1 rr2 ->
            let (l1,r1) = RT.get_rule rr1    in
            let (l2,r2) = RT.get_rule rr2    in
            let  c1     = RT.terms_cmp l1 l2 in
            if   c1 <> 0
            then c1
            else
            let  c2     = RT.terms_cmp r1 r2 in
            if   c2 <> 0
            then c2
            else failwith "Rules.POrd: Tautology found";

        (* Similarly, comparison of Rule Pairs. However, unlike tautologies
           in Rules which are not allowed,  here it DOES make sense to have
           reflexive rules (with LHS=RHS):
        *)
        value compare: t -> t -> int =
        fun p1 p2 ->
            let (l1,r1) = RT.get_pair p1 in
            let (l2,r2) = RT.get_pair p2 in
            let  c1  = rules_cmp l1 l2   in
            if   c1 <> 0
            then c1
            else       rules_cmp r1 r2;
    end;
    (* Now: the Pairs Priority Queue implementation. We are using the Red-Black
       Map: rule_pair |=> unit. This is XXX slightly sub-optimal from the memo-
       ry allocation point of view  (an extra word representing () is allocated
       for each pair stored), but the overhead  is minimal compared to the size
       of data stored:
    *)
    module PQueue     = Red_black.Map (POrd);

    type term_enumf   = RT.term_enumf;
    type rewrite_rule = RT.rewrite_rule;
    type rule_pair    = RT.rule_pair;
    type pairs_q      = PQueue.m unit;

    (*---------------------*)
    (*  Queue Operations:  *)
    (*---------------------*)
    (* "empty_queue":  *)
    value empty_queue : pairs_q = PQueue.empty;

    (* "insert_pair":  *)
    value insert_pair : pairs_q -> rule_pair -> pairs_q =
    fun queue pair ->
        PQueue.add queue pair ();

    (* "take_hd_pair": *)
    value take_hd_pair: pairs_q -> ((option rule_pair) * pairs_q) =
    fun queue ->
        match PQueue.take_min queue  with
        [ (None, queue')          -> (None,    queue')
        | (Some (mp, ()), queue') -> (Some mp, queue')
        ];

    (*==============================*)
    (*  Critical Pairs Generation:  *)
    (*==============================*)
    (* Each Pair of Rules can potentially yield multiple Critical Pairs, and
       therefore a list of new Rules (to be reduced later on):
    *)
    (*---------------------------------*)
    (* Critical Pairs of the 1st Kind: *)
    (*---------------------------------*)
    (* For rules (l1,r1), (l2,r2), find all common factors "t" such that
            l1 = t y
            l2 = x t
       In that case,
            x l1 = l2 y = x t y,
       but
            l1 ~ r1 and l2 ~ r2,
       and therefore also
            x r1 ~ r2 y
       which is the new rule to be created (for each such "t").
       Obviously, 1 <= len t <= min (len l1) (len l2);
       if len t = 0, then x = l2 and y = l1, and we have a trivial consequence
       of the original rules;
       on the other hand, empty x or y ((len t) reaching the upper limit)  are
       OK:
    *)
    value cps1: term_enumf ->
                RT.A.atoms -> RT.A.atoms -> RT.A.atoms -> RT.A.atoms ->
                list rewrite_rule =
    fun enumf   l1 r1 l2 r2      ->
        let len1  = RT.A.length l1 in
        let len2  = RT.A.length l2 in
        let max_i = min  len1 len2 in
        let rec mks1 =
            (* 0 <= i <= max_i is the offset from the left of "l1" and the rght
               of "l2", where the common factor "t" is sought to continue:
            *)
            fun i curr ->
                if  RT.A.get l1 i = RT.A.get l2 (len2-1-i)
                then
                    (* (Still) equal atoms -- currently len t = i+1: *)
                    let y    = RT.A.slice  l1 (i+1) (len1-i-1) in
                    let x    = RT.A.slice  l2 0     (len2-i-1) in
                    let x_r1 = RT.A.concat x  r1      in
                    let r2_y = RT.A.concat r2 y       in
                    let lhs  = RT.mk_term  x_r1 enumf in
                    let rhs  = RT.mk_term  r2_y enumf in
                    let nrule= RT.mk_rule  lhs   rhs  in
                    (* Try more! *)
                    mks1 (i+1) [nrule::curr]
                else
                    curr (* No more common symbols -- stop now *)
        in
        mks1 0 [];

    (*---------------------------------*)
    (* Critical Pairs of the 2nd Kind: *)
    (*---------------------------------*)
    (* For rules (l1,r1), (l2,r2), find all prefixes "x" (and thus postfixes
       "y") such that
            l1 = x l2 y .
       This is of course only possible if (l1 >= l2) wrt any admissible term
       ordering used. This is ALREADY GUARANTEED by ordering of the rules in
       "RT.get_pair" result.
       If so, since
            l1 ~ r1 and l2 ~ r2,
       we also have
            r1 ~ x r2 y
       which is the new rule to be created (for each such "x");
       in general,
            len x = 0 and/or len y = 0 is allowed.
       However, the case len x = 0 coincides with len x = 0 in Kind 1 Pairs,
       so it will be omitted here. Thus,
            1 <= len x <= len l1 - len l2
            0 <= len y <= len l1 - len l2 - 1
            1 <= lem l1 - len l2
       The computational comlexity here is quadratic, as opposed to linear in
       "cps1":
    *)
    value cps2: term_enumf ->
                RT.A.atoms -> RT.kb_term -> RT.A.atoms -> RT.A.atoms ->
                list rewrite_rule =
    fun enumf   l1 r1 l2 r2      ->
        let len1  = RT.A.length l1 in
        let len2  = RT.A.length l2 in
        let max_i = len1 - len2    in
        (* Rules with empty LHSs are not allowed, as the RHSs must be still
           less, -- such rules do not exist. Also, len l1 >= len l2  by the
           guaranteed property of pair construction, so max_i >= 0.
           Furthermore, if len 1 = len 2, do not proceed --  it can only be
           len x = len y = 0 then, which we decided to exclude
        *)
        let ()    = assert (len1 >= 1 && len2 >= 1 && max_i >= 0) in
        if  max_i = 0
        then []
        else
        (* What is more efficient -- the following function, or taking a slice
           of "l1" and comparing it to "l2" by "RT.A.cmp_terms"? -- Still, not
           doing allocations is probably a good idea;
           "cmp_from" returns "True" iff "l2" is sub-sequence of "l1" starting
           from pos "i":
        *)
        let rec cmp_from: int -> int -> bool =
            (* "i" is the curr pos in "l1", "j" is the curr pos in "l2": *)
            fun i j ->
                let () = assert (i < len1) in
                if  j >= len2
                then True     (* Finished "l2", all atoms match! *)
                else
                if   RT.A.get l1 i <> RT.A.get l2 j
                then False    (* Mismatch *)
                else cmp_from (i+1) (j+1)
        in
        (* Actual generator; "i" is the curr length of the prefix "x": *)
        let rec mks2 =
            fun i curr ->
                if  i > max_i
                then curr (* All done *)
                else
                let curr' =
                    if  cmp_from i 0
                    then
                        (* Yes, "l2" is a sub-sequence of "l1".
                           NB:  max_i - i = len1 - i - len2:
                        *)
                        let x      = RT.A.slice l1 0 i                in
                        let y      = RT.A.slice l1 (i+len2) (max_i-i) in
                        let x_r2_y = RT.A.concat (RT.A.concat x r2) y in
                        let rhs    = RT.mk_term x_r2_y enumf          in
                        let nrule  = RT.mk_rule r1     rhs            in
                        [nrule::curr]
                    else
                        curr (* No match at this position *)
                in
                mks2 (i+1) curr'
        in
        mks2 1 [];   (* (len x) starts from 1! *)

    (*---------------------------*)
    (* Critical Pairs Top-Level: *)
    (*---------------------------*)
    value mk_crit_pairs: term_enumf -> rule_pair -> list RT.rewrite_rule =
    fun enumf p ->
        let (lhs, rhs) = RT.get_pair p in
        [];

end;
