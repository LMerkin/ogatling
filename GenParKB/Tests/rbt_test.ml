(*pp camlp4r *)
(* vim:ts=4:syntax=ocaml
*)
(*===========================================================================*)
(*                               "rbt_test.ml":                              *)
(*                  Unit test for Red-Black trees in OCaml                   *)
(*                    L.A.Merkin, IMPARTIAL Project, 2004                    *)
(*===========================================================================*)
if  Array.length Sys.argv <> 2
then
do {
    prerr_endline "PARAMETER: <N_Iters>";
    exit 1
}
else
    ();
value n    = int_of_string Sys.argv.(1);

module IntOrd =
struct
    type  t       = int;
    value compare = 
        fun l r -> compare (l:int) (r:int);
end;

module IntRBM = Red_black.Map (IntOrd);

value from_option: option 'a -> 'a =
fun
    [ None   -> failwith "from_option: None"
    | Some a -> a
    ];

value print_rbm: IntRBM.m int -> unit =
    IntRBM.iter
    (fun k v ->
     do {
        print_int k;
        print_int v;
        print_string " "
    });

value h: Hashtbl.t int unit  = Hashtbl.create n;

value rec fill_by_one: int -> IntRBM.m int -> IntRBM.m int =
fun i rbm ->
    if  i < 0
    then rbm
    else
        let k    = Random.bits()      in
        let _    = Hashtbl.replace h k () in
        let rbm' = IntRBM.add rbm k k in
        fill_by_one (i-1) rbm';

value full: IntRBM.m int = fill_by_one (n-1) IntRBM.empty;

value empty = Hashtbl.fold
    (fun k _ rbm ->
        let (ok, rbm') = IntRBM.take rbm k   in
        let _ = assert (k  = from_option ok) in
        rbm'
    )
    h full;

assert (IntRBM.is_empty empty);
exit 0;
