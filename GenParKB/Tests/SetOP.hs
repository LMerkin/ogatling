-- vim:ts=4:syntax=haskell
-- ========================================================================= --
--								 "SetOP.hs":								 --
-- 			       Ordered Sets based on Red-Black Trees,					 --
--						with some Parallel Features							 --
--				  L.A.Merkin, IMPARTIAL Project, 2003--2004				     --
-- ========================================================================= --
module SetOP
(
	-- * The Set type:
	Set,            -- abstract, instance of: Eq
	IterDir (..),   -- iteration strategy -- may be useful for infinite sets

	-- * Construction:
	emptySet,       -- :: Set a
	mkSet,          -- :: Ord a  => [a]   -> Set a
	mkSet_Par,		-- :: Ord a  => Strategy a -> [a] -> Set a
	setToList,      -- :: IterDir-> Set a -> [a] 
	singletonSet,   -- :: a -> Set a
	singleElt,		-- :: Set a -> a

	-- * Inspection:
	elementOf,      -- :: Ord a => a -> Set a -> Bool
	isEmptySet,     -- :: Set a -> Bool
	isSingletonSet, -- :: Set a -> Bool
	cardinality,    -- :: Set a -> Int

	-- * Operations:
	union,          -- :: Ord a => IterDir  -> (a -> a -> a) ->
					--			  	  Set a ->  Set a -> Set a

	unionManySets,  -- :: Ord a => IterDir  -> (a -> a -> a) ->
					--						   [Set a]-> Set a

	minusSet,       -- :: Ord a => IterDir  -> (a -> a -> a) ->
					--				  Set a ->  Set a -> Set a

	intersect,      -- :: Ord a => IterDir  ->  Set a -> Set a -> Set a
	addToSet,      	-- :: Ord a => (a -> a -> a) -> a -> Set a -> Set a
	delFromSet,    	-- :: Ord a => a-> Set a->  Set a
	mapSet,         -- :: Ord a => IterDir -> (a -> b)->   Set a -> Set b
	mapSet_Par,		-- :: Ord a => Strategy a ->
					--			   IterDir -> (a -> b) ->  Set a -> Set b
	foldSet,		-- :: Ord a => IterDir ->
					--			   (b -> a -> (b,Bool))-> b -> Set a -> b
	trc,			--    a -> a -> a


	-- * Some Ordering:
	minElt,			-- :: Ord a => Set a -> a
	maxElt, 		-- :: Ord a => Set a -> a
	eqElt,			-- :: Ord a => a -> Set a -> Maybe a
	deleteMinElt,	-- :: Ord a => Set a -> Set a
	deleteMaxElt,	-- :: Ord a => Set a -> Set a
	takeMinElt,		-- :: Ord a => Set a -> (a, Set a)
	takeMaxElt,		-- :: Ord a => Set a -> (a, Set a)

	-- * Strategies:
	parSet,			-- :: Strategy a -> Strategy (Set a)
	seqSet 			-- :: Strategy a -> Strategy (Set a)

) where

import Strategies
import qualified TreeRB

------------
-- "Set": --
------------
-- This can't be a type synonym if we want to use constructor classes:
newtype Set a = MkSet (TreeRB.RBT a)

-- Iteration direction for the Set (actually, for the underlying "TreeRB"):
-- Left-to-Right or Right-to-Left:
--
data IterDir = LR | RL

mkFlag :: IterDir  -> TreeRB.Traverse_Flag
mkFlag	  LR	   =  TreeRB.Left_Right
mkFlag	  RL	   =  TreeRB.Right_Left

------------------
-- "emptySet":  --
------------------
emptySet :: Set a
emptySet =  MkSet TreeRB.emptyRBT

---------------------
-- "singletonSet": --
-- "singleELt"   : --
---------------------
singletonSet :: a -> Set a
singletonSet    x =  MkSet (TreeRB.singletonRBT x)

singleElt    :: Set a -> a
singleElt (MkSet rep) =  TreeRB.singleElt rep
-- Error if "rep" is not actually a singleton tree

------------------
-- "setToList": --
------------------
setToList :: IterDir -> Set a -> [a]

setToList idir (MkSet rep) =
	fst (TreeRB.foldRBT (\ acc x -> (x:acc, tflag)) [] rep)
	where tflag = mkFlag idir

--------------------
--	"mkSet":      --
--  "mkSet_Par":  --
--------------------
--	An inverse of "setToList". It admits a parallel implementation
--	("mkSet_Par") which is parameterised by the elements evaluation strategy:
--
--  Trivial recombination function:
--
trc :: a -> a -> a
trc    x   _  =  x

mkSet     :: Ord a => [a] -> Set a
mkSet as = MkSet
		   (foldl (\ acc x -> TreeRB.insert trc x acc) TreeRB.emptyRBT as)

mkSet_Par :: Ord a => Strategy a -> [a] -> Set a
mkSet_Par strat as = (mkSet as) `using` (parSet strat)


--------------
-- "union": --
--------------
-- NB: the LHS set operand  is traversed, and its elements inserted  into the
-- RHS. Normally, one would like to put the smaller set  (if finite) into the
-- LHS. The user can also specify explicitly the traversing direction for the
-- LHS set being iterated over:
--
union :: Ord a  => IterDir  -> (a -> a -> a) -> Set a -> Set a -> Set a

union  idir recomb (MkSet rep1) (MkSet rep2) =
	MkSet   (fst
			(TreeRB.foldRBT
			(\ acc x ->(TreeRB.insert recomb x acc, tflag)) rep2 rep1))
	where
	tflag = mkFlag idir

----------------------
-- "unionManySets": --
----------------------
-- Processed from left to right in the list, and each set itself is
-- traversed in the direction specified:
--
unionManySets :: Ord a => IterDir -> (a -> a -> a) -> [Set a]-> Set a

unionManySets _  _ []  =   emptySet
unionManySets idir recomb (set:sets) = foldl (union idir recomb) set sets

-----------------
-- "minusSet": --
-----------------
-- The result is formed by all elements in "rep1" which are NOT in "rep2".
-- Again, we specify the order of iterating over "rep1":
--
minusSet  :: Ord a => IterDir -> Set a -> Set a -> Set a

minusSet  idir (MkSet rep1) (MkSet rep2) =
	MkSet
		(fst 
			(TreeRB.foldRBT
			(\ acc x ->
				(if   TreeRB.member x rep2
				 then acc 
				 else TreeRB.insert trc x acc,
				tflag)
			)
			TreeRB.emptyRBT rep1)
		)
	where
	tflag = mkFlag idir

------------------
-- "intersect": --
------------------
-- Selects all elements which are in both sets. The LHS set is iterated
-- over, in the specified direction:
--
intersect :: Ord a => IterDir -> Set a  -> Set a -> Set a
intersect idir (MkSet rep1) (MkSet rep2) =
	MkSet
		(fst
			(TreeRB.foldRBT
			(\ acc x ->
				(if   TreeRB.member x rep2
				 then TreeRB.insert trc x acc
				 else acc,
				 tflag)
			)
			TreeRB.emptyRBT rep1)
		)
	where
	tflag = mkFlag idir

-----------------
-- "addToSet": --
-----------------
-- Insert a single element, re-combining it with a possibly already existing
-- equal one:
--
addToSet :: Ord a => (a -> a -> a) -> a -> Set a -> Set a
addToSet recomb x (MkSet rep) =
	MkSet  (TreeRB.insert recomb x rep)

-------------------
-- "delFromSet": --
-------------------
-- Does nothing if "x" is not actually in the "rep":
--
delFromSet :: Ord a => a -> Set a -> Set a
delFromSet x (MkSet rep) =
	MkSet	 (TreeRB.delete x rep)

------------------
-- "elementOf": --
------------------
elementOf :: Ord a => a -> Set a -> Bool
elementOf x (MkSet rep) = TreeRB.member x rep

-----------------------
-- "isEmptySet": 	 --
-- "isSingletonSet": --
-----------------------
isEmptySet     :: Set a -> Bool
isEmptySet		  (MkSet rep) = TreeRB.isEmpty     rep

isSingletonSet :: Set a -> Bool
isSingletonSet    (MkSet rep) = TreeRB.isSingleton rep

---------------
-- "mapSet": --
---------------
-- Sequential "map" implementation:
--
mapSet :: Ord a => IterDir -> (b -> a) -> Set b -> Set a

mapSet idir f (MkSet rep) =
	MkSet
	(fst
		(TreeRB.foldRBT
		(\ acc x -> (TreeRB.insert trc (f x) acc, tflag))
		TreeRB.emptyRBT rep)
	)
	where
	tflag = mkFlag idir

---------------
-- "parSet": --
-- "seqSet": --
---------------
-- Lifts "parRBT" and "seqRBT" strategies to Sets. The arg is the strategy
-- for elements:
--
parSet :: Strategy a -> Strategy (Set a)
parSet strat = \ (MkSet rep) -> (TreeRB.parRBT strat) rep

seqSet :: Strategy a -> Strategy (Set a)
seqSet strat = \ (MkSet rep) -> (TreeRB.seqRBT strat) rep

---------------------
--	"mapSet_Par":  --
---------------------
--	Parallel "map" implementation, using "parSet". Parameterised by the
--	evaluation strategy for individual elements:
--
mapSet_Par ::   Ord a => Strategy a -> IterDir -> (b -> a) -> Set b -> Set a

mapSet_Par strat idir f rep = (mapSet idir f rep) `using` (parSet strat)

----------------
-- "foldSet": --
----------------
--	It cannot be parallelised unless we know the structure of the "b" type.
--	NB: the folding function also returns a Bool flag indicating whether to
--		continue the "fold".     Same flag is retirned by "foldSet" itself:
--		True <=> traversed the set to the end,   False <=> stopped by user:
--
foldSet:: Ord a => IterDir -> (b -> a -> (b,Bool)) -> b -> Set a -> (b, Bool)
foldSet idir f b0 (MkSet rep) =
	let
		tflag	  = mkFlag idir

		-- The function used to traverse the underlying tree: different from
		--    "f" in the flag returned:
		trav_func b x =
			let (b', cont_flag) = f b x
			in	if   cont_flag
				then (b', tflag)
				else (b', TreeRB.Stop_Now)
	in
		TreeRB.foldRBT trav_func b0 rep

---------------------
--	"cardinality": --
---------------------
--	Obviously, does not terminate for infinite set. In general, this function
--	is expensive, as it re-computes  the number of elements from scratch,  --
--	storing them in the set would preclude infinite sets:
--
cardinality :: Set a -> Int
cardinality (MkSet rep) =
	fst (TreeRB.foldRBT
		(\ c _ -> (c+1, TreeRB.Left_Right)) 0 rep)

---------------------
-- "Eq" instance:  --
---------------------
-- Equality between sets is difficult to test, as their internal representation
-- may not be unique, and they may not be finite.  The following function tests
-- mutual inclusion of two sets. It may, however, fail to terminate:
--
instance (Ord a) => Eq (Set a) where
	(==) (MkSet rep1) (MkSet rep2) =
		-- NB: no way to use a custom traversing parameter here, so always do
		--	   "Left_Right".   The following function tests that the LHS is a
		--	   sub-set of the RHS:
		let test_incl s1 s2  =
			fst
			(TreeRB.foldRBT
			(\ _ x ->
				if  TreeRB.member x s2
				then
					(True,  TreeRB.Left_Right)  -- Continue
				else
					(False, TreeRB.Stop_Now)	   -- Fails
			)
			True s1)
		in
		(test_incl rep1 rep2) && (test_incl rep2 rep1)

----------------
-- "*minElt": --
-- "*maxElt": --
----------------
minElt :: Ord a => Set a -> a
minElt (MkSet rep) = TreeRB.findMin rep

maxElt :: Ord a => Set a -> a
maxElt (MkSet rep) = TreeRB.findMax rep

deleteMinElt :: Ord a => Set a -> Set a
deleteMinElt (MkSet rep) = MkSet (TreeRB.deleteMin rep)

deleteMaxElt :: Ord a => Set a -> Set a
deleteMaxElt (MkSet rep) = MkSet (TreeRB.deleteMax rep)

--
--	"take*" functions return and delete the requested set elements:
--
takeMinElt :: Ord a => Set a -> (a, Set a)
takeMinElt (MkSet rep) =
	let (min_a,   rep')  = TreeRB.takeMin rep
	in  (min_a,   MkSet rep')

takeMaxElt :: Ord a => Set a -> (a, Set a)
takeMaxElt (MkSet rep) =
	let (max_a,   rep')  = TreeRB.takeMax rep
	in  (max_a,   MkSet rep')

----------------
--	"eqElt":  --
----------------
--	Similar to "elementOf":
--	Tries to find an element in the Set which is equal (in the sense of Eq,
--	not necessarily structurally equal!) to the given one:
--
eqElt :: Ord a => a -> Set a -> Maybe a
eqElt x (MkSet rep) = TreeRB.findEq x rep

