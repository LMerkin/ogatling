(*pp camlp4r *)
(* vim:ts=4:syntax=ocaml
*)
(*===========================================================================*)
(*                                "bench.ml":                                *)
(*                       BenchMark Test for GenParKB                         *)
(*                   L.A.Merkin, IMPARTIAL Project, 2004                     *)
(*===========================================================================*)
(*======================*)
(* The "Test" Functor:  *)
(*======================*)
module Test (P: Crit_pairs.PAIRS) =
struct
    open P;
    open P.RT;
    open P.RT.A;

    value random_term: array atom -> term_enumf -> kb_term =
    fun alphabet enumf ->
        let alen= Array.length alphabet in
        let len = Random.int  128       in
        let str = Array.init  len (fun i -> alphabet.(Random.int alen)) in
        let ats = mk_atoms    str       in
        mk_term ats enumf;

    value rec random_rule: array atom -> term_enumf -> rewrite_rule =
    fun alphabet enumf ->
        let t1 = random_term alphabet enumf in
        let t2 = random_term alphabet enumf in
        if  terms_equal t1 t2
        then
            (* Try again! *)
            random_rule alphabet enumf
        else
            mk_rule t1 t2;

    (* Create random rules, insert them into the list: *)

    value rec create_rules:
        array atom -> term_enumf ->
        int -> list rewrite_rule -> list rewrite_rule =
    fun alphabet enumf i curr_lst ->
        if  i <= 0
        then curr_lst
        else create_rules  alphabet enumf (i-1)
             [(random_rule alphabet enumf) :: curr_lst];

    (* Searching for Reducers: *)

    value mk_fixes: array atom -> (atoms * atoms) =
    fun alphabet ->
        let () = assert (alphabet <> [||]) in
        let af = alphabet.(0) in
        let al = alphabet.(Array.length alphabet - 1)        in
        let prefix_ats = mk_atoms (Array.make 5 af) in
        let postfx_ats = mk_atoms (Array.make 5 al) in
        (prefix_ats, postfx_ats);

    value find_rules:
        array atom -> term_enumf   -> list rewrite_rule -> rule_set -> unit =
    fun  alphabet enumf rlist rset ->
        let (prefix, postfx) = mk_fixes alphabet in
        List.iter
        (fun r ->
            let (lhs, rhs) = get_rule r  in

            (* The term we will re-write: *)
            let t   = concat prefix (concat (get_atoms lhs) postfx)
            in
            (* Do search and rewriting:   *)
            match find_and_apply_rule rset t enumf with
            [ None   -> print_endline "SEARCH FAILED"
            | Some _ -> ()
            ]
        )
        rlist;

    value rec del_rules:
        int -> list rewrite_rule -> list rewrite_rule -> rule_set ->
        ((list rewrite_rule) * (list rewrite_rule) * rule_set)    =

    fun count curr_lst del_lst curr_set ->
        if  count < 0
        then
            (curr_lst, del_lst, curr_set)
        else
        match curr_lst with
        [ [] ->
            (curr_lst, del_lst, curr_set)

        | [r::rs] ->
            let rset' = delete_rule curr_set r in
            del_rules (count-1) rs [r::del_lst] rset'
        ];

    value rec do_crit_pairs:
        term_enumf  -> rule_set ->
            list rewrite_rule -> list rewrite_rule -> unit =
    fun enumf rset list1 list2 ->
        match (list1,  list2) with
        [ ([h1::t1], [h2::t2]) ->
          do {
                let new_rules = complete_crit_pairs enumf (mk_pair h1 h2)  in
                List.iter (fun r ->
                          ignore (reduce_wrt_set rset r enumf)) new_rules;

                do_crit_pairs enumf rset t1 t2
          }
        | _ ->  ()
        ];

    (*----------------*)
    (* Run All Tests: *)
    (*----------------*)
    value run_tests: array atom -> int -> unit =
    fun alphabet n_tests ->
        let ()       = verify_found_lhs_enums.val := False      in

        let enumf    = mk_enumf (TLen_Lex (mk_atoms alphabet))  in

        (* Create the initial random rules: *)
        let p0       = Unix.gettimeofday    ()      in
        let rlist1   = create_rules alphabet enumf n_tests []   in

        let p1       = Unix.gettimeofday    ()      in

        (* Now insert them into the Rule Set: *)
        let rset1    = List.fold_left
            (fun curr_set r -> insert_rule curr_set r) empty_rset rlist1
        in
        let p2       = Unix.gettimeofday    ()      in

        (* Now find and apply each of these rules: *)
        let ()       = find_rules alphabet enumf rlist1 rset1   in
        let p3       = Unix.gettimeofday    ()      in

        (* Now delete HALF of the rules by one: *)
        let (rlist2, rlist_del, rset2) = del_rules (n_tests/2) rlist1 [] rset1
        in
        let p4       = Unix.gettimeofday    ()      in

        (* Now create critical pairs between "rlist2" and "rlist_del": *)
        let ()       = do_crit_pairs enumf rset2 rlist2 rlist_del       in
        let p5       = Unix.gettimeofday   ()       in

        let total_time  = p5 -. p0 in
        let create_time = p1 -. p0 in
        let create_pct  = create_time /. total_time *. 100.0 in
        let store_time  = p2 -. p1 in
        let store_pct   = store_time  /. total_time *. 100.0 in
        let search_time = p3 -. p2 in
        let search_pct  = search_time /. total_time *. 100.0 in
        let delete_time = p4 -. p3 in
        let delete_pct  = delete_time /. total_time *. 100.0 in
        let crit_p_time = p5 -. p4 in
        let crit_p_pct  = crit_p_time /. total_time *. 100.0 in
        do {
            Printf.printf   "Creation:  %.2f sec (%.2f %%)\n%!"
                            create_time  create_pct;
            Printf.printf   "Storing :  %.2f sec (%.2f %%)\n%!"
                            store_time   store_pct;
            Printf.printf   "Search  :  %.2f sec (%.2f %%)\n%!"
                            search_time  search_pct;
            Printf.printf   "Delete/2:  %.2f sec (%.2f %%)\n%!"
                            delete_time  delete_pct;
            Printf.printf   "CritPairs: %.2f sec (%.2f %%)\n%!"
                            crit_p_time  crit_p_pct;
            Printf.printf   "TOTAL   : %.2f sec\n%!"
                            total_time
        };
end;

(*==============*)
(*  Instances:  *)
(*==============*)
(*  Atoms / Terms represented via Chars / Strings: *)
module Atoms8 (*: Rules.ATOMS with type atom = char and type atoms = string*) =
struct
    type  atom    = char;
    type  atoms   = string;

    value encode  : atom  -> int                    = Char.code;
    value decode  : int   -> atom                   = Char.chr;

    value empty   : atoms                           = "";
    value single  : atom  -> atoms                  = String.make 1;
    value mk_atoms: array atom     -> atoms         =
                    fun ats        ->
                        let l = Array.length ats in
                        let s = String.create  l in
                        do {
                            for i = 0 to l-1
                            do {
                                s.[i]:= ats.(i)
                            };
                            s
                        };
    value length  : atoms -> int                    = String.length;

    value get     : atoms -> int   -> atom          = String.get;
    value concat  : atoms -> atoms -> atoms         = \^;
    value slice   : atoms -> int   -> int  -> atoms = String.sub;

    value cmp     : atom  -> atom  -> int           =
        fun l r -> compare (l:char) (r:char);

    value eqs     : atoms -> atoms -> bool          =
        fun l r -> \=      (l:string) (r:string);
end;

(*  Atoms / Terms represented via Ints / Int Arrays: *)
module Atoms30 =
struct
    type  atom    = int;
    type  atoms   = array int;

    value encode  : atom  -> int                    = fun a -> a;
    value decode  : int   -> atom                   = fun i -> i;

    value empty   : atoms                           = [| |];
    value single  : atom  -> atoms                  = Array.make 1;
    value mk_atoms: array atom -> atoms             = fun ats -> ats;
    value length  : atoms -> int                    = Array.length;

    value get     : atoms -> int   -> atom          = Array.get;
    value concat  : atoms -> atoms -> atoms         = Array.append;
    value slice   : atoms -> int   -> int  -> atoms = Array.sub;

    value cmp     : atom  -> atom  -> int           =
        fun l r ->  compare (l:int)  (r:int);
    value eqs     : atoms -> atoms -> bool          =
        fun l r -> \=      (l:array int)  (r:array int);
end;

(*  Atoms / Terms using a typical "polymorphic" representation: *)
module AtomsP =
struct
    type  atom  = {a: int};
    type  atoms = array atom;

    value encode: atom  -> int                      = fun a -> a.a;
    value decode: int   -> atom                     = fun i -> {a=i};

    value empty : atoms                             = [| |];
    value single: atom  -> atoms                    = Array.make 1;
    value mk_atoms: array atom -> atoms             = fun ats -> ats;
    value length: atoms -> int                      = Array.length;

    value get   : atoms -> int   -> atom            = Array.get;
    value concat: atoms -> atoms -> atoms           = Array.append;
    value slice : atoms -> int   -> int  -> atoms   = Array.sub;

    value cmp   : atom  -> atom  -> int             = compare;
    value eqs   : atoms -> atoms -> bool            = \=;
end;

(*=============================*)
(*  Instances and Top-Level:   *)
(*=============================*)
value usage: unit -> 'a =
fun () ->
do {
    prerr_endline "PARAMETER: <M_Alphabet> <N_Tests>";
    exit 1
};

value (m_alph, n_tests) =
    try  (int_of_string Sys.argv.(1), int_of_string Sys.argv.(2))
    with [_ -> usage ()];

if  m_alph <= 0 || n_tests < 0
then usage ()
else ();

value int_alph =  Array.init m_alph (fun i -> i);

module Terms8  = Rules.Terms   (Atoms8 );
module CP8     = Crit_pairs.CP (Terms8);
module Test8   = Test          (CP8);

if  m_alph < 256
then
do {
    print_endline "\n***** TEST-8    *****\n";
    Test8.run_tests  (Array.map CP8.RT.A.decode  int_alph) n_tests
}
else ();

module Terms30 = Rules.Terms   (Atoms30);
module CP30    = Crit_pairs.CP (Terms30);
module Test30  = Test          (CP30);
print_endline "\n***** TEST-30   *****\n";
Test30.run_tests (Array.map CP30.RT.A.decode int_alph) n_tests;

module TermsP  = Rules.Terms   (AtomsP);
module CPP     = Crit_pairs.CP (TermsP);
module TestP   = Test          (CPP);
print_endline "\n***** TEST-Poly *****\n";
TestP.run_tests  (Array.map CPP.RT.A.decode  int_alph) n_tests;

exit 0;

