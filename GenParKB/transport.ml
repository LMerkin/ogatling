(*pp camlp4r *)
(* vim:ts=4:syntax=ocaml
*)
(*===========================================================================*)
(*                              "transport.ml":                              *)
(*       Communication Infrastructure for Parallel Re-Writing Systems        *)
(*                   L.A.Merkin, IMPARTIAL Project, 2005                     *)
(*===========================================================================*)
(*================*)
(* Configuration: *)
(*================*)
(*----------------*)
(*  "node_id"  :  *)
(*  "node_info":  *)
(*----------------*)
(*  A "node" is a communicating process running on a particular host. As we
    use the TNIO RTS, it can't take advantage of SMP architectures, so more
    than one process may need to run on a single host. Thus, all such procs
    will have different ports for communication.    They are comprised of a
    given number of TNIO threads.
    The nodes are identified simply by their integer numbers:
*)
type node_id   = int;
type node_info =
{
    addr       : Unix.sockaddr;   (* Host IP & Port     *)
    n_threads  : int              (* Number of TNIO threads to be created *)
};

(*---------------*)
(*  "comp_env":  *)
(*---------------*)
(*  "comp_env" is parameterised by the message type, as it contains the
    msg queue internally:
*)
type comp_env 'm 'a =
{
    (* The following fields are from the config: *)
    all_nodes  : array node_info; (* All configured nodes   *)

    (* The following fields are dynamically initialised; "msg_q" is the
       msg queue on receiving, containing msgs and the indices of their
       originating nodes:
    *)
    this_node  : int;             (* Index of the curr node *)
    socket     : Tnio.tsock;      (* Communication socket   *)
    buff       : string;             (* Receiving buffer    *)
    msg_q      : Dirq.t ('m * int);  (* Receiving queue     *)

    (* All ports used by nodes: when sending a multi-cast message,    it is
       sent (although multi-cast) to each port individually, hence multiple
       multi-cast addrs:
    *)
    mcast_addrs: list Unix.sockaddr;

    (* Reverse mapping for identifying the source nodes:    *)
    rev_nodes  : Hashtbl.t Unix.sockaddr int;

    (* Logging: *)
    log_ch     : out_channel;

    (* Application-specific data: *)
    appl_data  : 'a
};

(*-------------------------*)
(*  Access to "comp_env":  *)
(*-------------------------*)
(*  "n_nodes":
    The total number "n" of nodes configured (0..n-1):
*)
value n_nodes  : comp_env 'm 'a -> int =
fun ce ->
    Array.length ce.all_nodes;

(*  "n_threads":
    The number of threads to be created on a particular node:
*)
value n_threads: comp_env 'm 'a -> int -> int =
fun ce i ->
    ce.all_nodes.(i).n_threads;

(*  "this_node":  *)
value this_node: comp_env 'm 'a -> node_id =
fun ce ->
    ce.this_node;

(*  "next_node":  *)
value next_node: comp_env 'm 'a -> node_id =
fun ce ->
    (ce.this_node + 1) mod (n_nodes ce);

(*  "appl_data":  *)
value appl_data: comp_env 'm 'a -> 'a  =
fun ce ->
    ce.appl_data;

(*  "node_id"  :  *)
value node_id  : int -> node_id =
fun i ->
    i;

(*  "node_num" :  *)
value node_num : node_id -> int =
fun i ->
    i;

(*---------*)
(* "init": *)
(*---------*)
(* Initialises communications and application-specific data for the current
   node:
*)
value init: Config.system_conf -> 'a -> comp_env 'm 'a =
fun sc appl_data ->
    (* Log file: *)
    let log_ch    =
        try  open_out  sc.Config.log_file
        with
        [_ -> Misc_utils.fatal_error
             ("Transport.init: cannot open log: \""^sc.Config.log_file^"\"")
        ]
    in
    (* Translate "all_nodes", and construct "rev_nodes" as well: *)
    let src_nodes = Array.of_list  sc.Config.all_nodes      in
    let rev_nodes = Hashtbl.create (Array.length src_nodes) in
    let ports_hash= Hashtbl.create (Array.length src_nodes) in
    let all_nodes =
        Array.mapi
        (fun i ni ->
            let addr =
                Unix.ADDR_INET
               (Unix.inet_addr_of_string ni.Config.ip) ni.Config.port
            in
            do {
                (* Update the reverse mapping:       *)
                Hashtbl.add rev_nodes addr  i;

                (* Save the port number in the hash: *)
                Hashtbl.replace ports_hash  ni.Config.port ();

                (* The value for the direct mapping: *)
                {
                    addr      = addr;
                    n_threads = ni.Config.n_threads
                }
            }
        )
        src_nodes
    in
    (* Determine "this_node" automatically.  NB: this causes problems if there
       if more than one node is running on a given host, as we then don't know
       to which address we need to bind (such nodes have different IPs  and/or
       Ports). Solution: bind to each of them (re-using the address, so we will
       not be blocked on TIME-WAIT (XXX: is there a danger of getting old msgs
       in this case?), until we succeed for some address:
    *)
    let our_ips = Tnio.get_local_ips () in

    (* "find_this_node" returns the list of ALL indices of nodes in "all_nodes"
       which include an IP of the current node:
    *)
    let get_ip: Unix.sockaddr -> Unix.inet_addr =
    fun
        [ Unix.ADDR_INET ip _ -> ip
        | _                   -> assert False
        ]
    in
    let rec find_this_node: int -> list int -> list int =
    fun i curr ->
        if  i < 0
        then curr
        else
        let ith_ip = get_ip all_nodes.(i).addr in
        let curr'  =
            if  List.mem ith_ip our_ips
            then [i::curr]  (* Got it! *)
            else curr
        in
        find_this_node (i-1) curr' (* Try next... *)
    in
    let is = find_this_node (Array.length all_nodes - 1) [] in
    if  is = []
    then (* Our own node was not found: *)
        failwith "Transport.init: Cannot determine the local node"
    else
    (* Create the communication socket (UDP):   *)
    let ts        = Tnio.socket Unix.PF_INET Unix.SOCK_DGRAM  in

    (* Bind this socket to some local address. Returns the index of a
       successful bind; that becomes "this_node":
    *)
    let ()        = Tnio.setsockopt ts Unix.SO_REUSEADDR True in
    let rec try_bind: list int -> int =
        fun
        [ []     ->
            failwith "Transport.init: Cannot bind to any local address"
        | [h::t] ->
            let ok = try do{Tnio.bind ts all_nodes.(h).addr; True}
                     with  [Unix.Unix_error Unix.EADDRINUSE _ _ -> False]
            in
            if  ok   then h else try_bind t
        ]
    in
    let this_node  = try_bind is in

    (* If we got here, binding was successful. Make the socket a member of the
       multi-cast group:
    *)
    let this_ip    = get_ip all_nodes.(this_node).addr           in
    let mcast_ip   = Unix.inet_addr_of_string sc.Config.mcast_ip in
    let mcast_addrs=
        Hashtbl.fold
        (fun p _ curr ->
            let ma = Unix.ADDR_INET mcast_ip p in
            [ma::curr]
        )
        ports_hash  []
    in
    let ()         =
        Tnio.join_multicast ts ~iface_ip:this_ip ~mcast_ip:mcast_ip
    in
    let ()         = Tnio.set_multicast_ttl ts sc.Config.mcast_ttl in

    (* Return the env: *)
    {
        all_nodes  = all_nodes;
        this_node  = this_node;
        socket     = ts;
        mcast_addrs= mcast_addrs;
        buff       = String.create 65535;
        msg_q      = Dirq.empty ();
        rev_nodes  = rev_nodes;
        log_ch     = log_ch;
        appl_data  = appl_data
    };

    (*-------------*)
    (* "send":     *)
    (* "send_all": *)
    (*-------------*)
    (* Sending a message to a single destination, given by its index in the
       "comp_env.all_nodes", or to all nodes except the sender itself:
    *)
    value send':    comp_env 'm 'a -> 'm -> Unix.sockaddr -> unit =
    fun ce msg addr ->
        let ser      = Marshal.to_string msg [Marshal.No_sharing] in
        let len      = String.length ser                          in

        if  Tnio.sendto ce.socket ser 0 len Tnio.infinite_deadline addr <> len
        then failwith "Transport.Msgs.send*: Short send"
        else ();

    value send:     comp_env 'm 'a -> 'm -> int -> unit =
    fun ce msg node ->
        send' ce msg ce.all_nodes.(node).addr;

    value send_all: comp_env 'm 'a -> 'm -> unit =
    fun ce msg ->
        List.iter (fun addr -> send' ce msg addr) ce.mcast_addrs;

    (*-----------*)
    (*  "recv":  *)
    (*-----------*)
    value recv: comp_env 'm 'a -> ('m -> int -> bool) -> ('m * int) =
    fun ce test ->
        (* The actual receiving function: *)
        let rec recv_msg =
        fun ()  ->
            let (len, src) =
                Tnio.recvfrom
                    ce.socket ce.buff 0 (String.length ce.buff)
                    Tnio.infinite_deadline
            in
            (* Message, if received, must be non-empty:  *)
            let () = assert (len > 0) in

            (* Decode it, and determine its source node: *)
            let m  = Marshal.from_string ce.buff 0 in
            let si = Hashtbl.find ce.rev_nodes src in

            (* Is it what we are waiting for? *)
            if test m si
            then
                (* Yes, don't queue it, just return it: *)
                (m, si)
            else
            do {
                (* This msg is to be queued; we will wait for another one: *)
                Dirq.append ce.msg_q (m, si);
                recv_msg ()
            }
        in
        (* Now, first of all, check the messages already in the queue: *)
        match Dirq.take ce.msg_q (fun (m,i) -> test m i) with
        [ Some res ->
            res  (* Got it! *)
        | _ ->
            (* No readily-available msgs; try to receive one directly: *)
            recv_msg ()
        ];

(*----------*)
(*  "log":  *)
(*----------*)
(*  No log file locking is performed (it may not work when the file is on NFS).
    Writes are committed by flushing.   Multiple nodes can write into the same
    (eg NFS-shared) file:
*)
value log: comp_env 'm 'a -> string -> unit =
fun ce msg  ->
    let tst  = Date_time.http_time_stamp (Unix.gettimeofday ())  in
    Printf.fprintf ce.log_ch "Node %d [%s]: %s\n%!" ce.this_node tst msg;

