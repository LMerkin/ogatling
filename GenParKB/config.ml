(*pp camlp4r pa_ioXML.cmo *)
(* vim:ts=4:syntax=ocaml
*)
(*===========================================================================*)
(*                               "config.ml":                                *)
(*          Top-Level Source/Configuration Data with XML-Based I/O           *)
(*                  L.A.Merkin, IMPARTIAL Project, 2005                      *)
(*===========================================================================*)
(*-----------------*)
(*  "node_info":   *)
(*  "system_conf": *)
(*-----------------*)
(*  Configuration of processing nodes and communications between them. Directly
    used by the "Transport" module:
*)
type node_info =
{
    ip         :  string;
    port       :  int;
    n_threads  :  int
};

type system_conf =
{
    (* Nodes and communications: *)
    all_nodes  :  list node_info;

    (* Multi-cast parms:    *)
    mcast_ip   :  string;   (* Multi-Cast IP to join         *)
    mcast_ttl  :  int;      (* Normally 1 on local sub-net   *)

    (* Log file.  Currently, ONE file name (usually a single file on NFS,   or
       per-node local file)) is specified for all nodes; messages written into
       it are endowed with Node/Time stamps:
    *)
    log_file   :  string
};

(*--------------*)
(*  "kb_conf":  *)
(*--------------*)
(*  Application-specific configuration:  *)
type kb_conf =
{
    (* Completion strategy. Currently, it includes parms for triggering mutual
       reductions periodically after a certain number of insertions,  and if a
       rule with the LHS being smaller than the given fraction of the curr MAX
       enumerator of the LHSs is inserted:
    *)
    mr_period  : int;   (* Not used if <= 0 *)
    mr_thresh  : float  (* If < 0.0, then NEVER; if >= 1.0, then ALWAYS *)
};

(*---------------*)
(*  "kb_input":  *)
(*---------------*)
(*  Over-all config and data input for the algorithm, read from an external XML
    file (see "read_kb_input" below):
*)
type kb_input =
{
    symbols    : list string;
    rules      : list string;  (* As "l1 ... lN |=> r1 ... rM " *)
    kb_conf    : kb_conf;
    system_conf: system_conf
};

(*  "read_kb_input":
    Reads "kb_input" from a file (presumably, visible on each node via eg NFS):
*)
value read_kb_input: string -> kb_input =
fun fn ->
    let cnnl = open_in fn in
    try
        let strm = Stream.of_channel cnnl in
        let xstr = IoXML.parse_xml   strm in
        let res  = xparse_kb_input   xstr in
        do {close_in cnnl; res}
    with
    [hmm ->
        do {close_in cnnl; raise hmm}
    ];

